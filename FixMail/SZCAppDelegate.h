//
//  SZCAppDelegate.h
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SZCUIWindow.h"
#import "TestFlight.h"

@class SZCViewController;
@class SZCGuardScreenController;
@protocol SZLApplicationContainer;

@class SZLConcreteApplicationContainer;

@interface SZCAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>
{
    BOOL freshLaunch;
//    SZCGuardScreenController *_lockScreenController;
    UINavigationController *_lockScreenController;
    SZLConcreteApplicationContainer *_rootViewController;
    NSArray *_savedPasteboardItems;
}

@property (strong, nonatomic) SZCUIWindow *window;
//@property (strong, nonatomic) SZCViewController *viewController;

-(id <SZLApplicationContainer>)container;
-(void)wipeContainer;
-(void)lockContainer;
-(void)lockContainer:(BOOL)animated;
-(void)unlockContainer;
-(void)closePopUps:(NSArray *)subviews;

@end
