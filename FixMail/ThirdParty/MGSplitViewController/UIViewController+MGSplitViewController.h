//
//  UIViewController+MGSplitViewController.h
//  FixMail
//
//  Created by Sean Langley on 2013-04-23.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MGSplitViewController)
@property (nonatomic, retain) id mgSplitViewController;

@end
