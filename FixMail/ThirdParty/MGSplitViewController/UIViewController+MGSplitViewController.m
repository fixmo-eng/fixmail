//
//  UIViewController+MGSplitViewController.m
//  FixMail
//
//  Created by Sean Langley on 2013-04-23.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "UIViewController+MGSplitViewController.h"
#import <objc/runtime.h>

static char const * const mgSplitViewControllerKey = "mgSplitViewControllerKey";


@implementation UIViewController (MGSplitViewController)
@dynamic mgSplitViewController;

- (id)mgSplitViewController {
    return objc_getAssociatedObject(self, mgSplitViewControllerKey);
}

- (void)setMgSplitViewController:(id)mgSplitViewController {
    objc_setAssociatedObject(self, mgSplitViewControllerKey, mgSplitViewController, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


@end
