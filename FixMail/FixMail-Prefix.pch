
//
// Prefix header for all source files of the 'FixMail' target in the 'FixMail' project
//

#ifdef __OBJC__
    #import <Foundation/Foundation.h>
    #import <UIKit/UIKit.h>
	#import <QuartzCore/QuartzCore.h>

	#import "UIStoryboard+FixmoExtensions.h"

	#import "ContainedApplicationConstants.h"

// If defined use old reMail database methodology with a dbNum and pk(Primary Key) email database indexing
#define FIXMAIL_CONTAINER	1

// FIXME: should be checked in as DEBUG
// If not defined, for ActiveSync provisioning the server and port will be fetched from
//   the container enrollment
#define showServerFields    DEBUG

#import "FXLSafeZone.h"

#define IS_IPAD() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define IS_IPHONE() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
#define IS_IPHONE5() (IS_IPHONE() && [[UIScreen mainScreen] bounds].size.height == 568.0f)

#define IPAD_BACKGROUND_PORTRAIT @"Fixmo_SafeZone_Launch.png"
#define IPAD_BACKGROUND_LANDSCAPE @"Fixmo_SafeZone_Launch_landscape.png"
#define IPAD_LOCK_PORTRAIT @"Fixmo_SafeZone_Lock.png"
#define IPAD_LOCK_LANDSCAPE @"Fixmo_SafeZone_Lock_landscape.png"

#define ICON_SPACING() (IS_IPAD() ? 24 : 9)
#define ICONS_PER_ROW() (IS_IPAD() ? 4 : 3)

#define SAFEZONE_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]

#define FXLLocalizedStringFromTable(key, tbl, comment) \
[[FXLSafeZone getResourceBundle] localizedStringForKey:(key) value:@"" table:(tbl)]

#define FXLLocalizedString(key, comment) \
[[FXLSafeZone getResourceBundle] localizedStringForKey:(key) value:@"" table:@"Localizable"]

#define DEFAULT_ANIMATION_SPEED 0.3f

//
// Localization
//
#define TRANSLATION_NEEDED							"TRANSLATION_NEEDED"

//
// Storyboards
//
#define kStoryboard_Contacts_iPhone					@"contacts_iPhone"
#define kStoryboard_Contacts_iPad					@"contacts_iPad"

#define kStoryboard_Accounts_iPhone					@"accounts_Both"
#define kStoryboard_Accounts_iPad					@"accounts_Both"

#define kStoryboard_Settings_iPhone					@"settings_iPhone"
#define kStoryboard_Settings_iPad					@"settings_iPad"

#define kStoryboard_ComposeMail_iPhone				@"composeMail_iPhone"
#define kStoryboard_ComposeMail_iPad				@"composeMail_iPhone"

#define kStoryboard_Mailbox_iPhone					@"mailbox_Both"
#define kStoryboard_Mailbox_iPad					@"mailbox_Both"

#define kStoryboard_MailViewer_iPhone				@"mailViewer_Both"
#define kStoryboard_MailViewer_iPad					@"mailViewer_Both"

#define kStoryboard_Previewers_iPhone				@"previewers_Both"
#define kStoryboard_Previewers_iPad					@"previewers_Both"

//
//Nibs
//

#define kNib_FolderView_iPhone                      @"FolderListView"
#define kNib_FolderView_iPad                        @"FolderListView"

//
// User defaults
#define kUserDefaultsConfigurePIMLaterKey				@"userDefaultsConfigurePIMLaterKey"

//
// Notifications
#define kProgressViewStringDidChangeNotification @"progressViewStringDidChangeNotification"
#define kHttpRequestFailureNotification @"httpRequestFailureNotification"
#define kSZCContactListSelectedNotification		@"kSZCContactListSelectedNotification"

//
// Timers
#define kTypingPauseInterval			0.8

// Screen Size
#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGHT ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.height : [[UIScreen mainScreen] bounds].size.width)

//
// FXLog defines
//
#define kFXLogAll					0xFFFFFFFFFFFF

#define kFXLogTrace					0x000000000001
#define kFXLogFIXME					0x000000000002
#define kFXLogPKMime				0x000000000004
#define kFXLogSZLSecurity			0x000000000008

#define kFXLogActiveSync			0x000000000010
#define kFXLogWBXML					0x000000000020
#define kFXLogASEmail				0x000000000040
#define kFXLogASFolder				0x000000000080

#define kFXLogCustom1				0x000000001000
#define kFXLogCustom2				0x000000002000
#define kFXLogCustom3				0x000000004000
#define kFXLogCustom4				0x000000008000
#define kFXLogCustom5				0x000000010000
#define kFXLogCustom6				0x000000020000
#define kFXLogCustom7				0x000000040000
#define kFXLogCustom8				0x000000080000

#define kFXLogVCLoadUnload			0x000000100000
#define kFXLogVCAppearDisappear		0x000000200000
#define kFXLogVCDealloc				0x000000400000

#define kFXLogSettings				0x000001000000
#define kFXLogContacts				0x000002000000
#define kFXLogCalendar				0x000004000000
#define kFXLogNotes					0x000008000000

#define kFXLogComposeMailVC			0x000010000000
#define kFXLogMailViewerVC			0x000020000000

#define kFXLogSZCAddressView		kFXLogCustom1
#define kFXLogSZCTextEntryView		kFXLogCustom2
#define kFXLogSZCAttachmentView		kFXLogCustom3
#define kFXLogAttachmentManager		kFXLogCustom4
#define kFXLogCertificateManager	kFXLogCustom5

#ifdef DEBUG

#if __cplusplus
extern "C" {
void FXDebugLog(long long flags, NSString *format, ...);
void FXSetFlags(long long inFlags);
long long FXGetFlags(void);
}
#else
void FXDebugLog(long long flags, NSString *format, ...);
void FXSetFlags(long long inFlags);
long long FXGetFlags(void);
#endif

#else
#define FXDebugLog(...)
#define FXSetFlags(...)
inline long long FXGetFlags() {return 0;}
#endif

#endif


//
// Global Header remnants
//
#define SuppressPerformSelectorLeakWarning(x) \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
x; \
_Pragma("clang diagnostic pop")
