//
//  UIWebView+FixmoExtensions.m
//
//  Created by Colin Biggin on 2013-07-03.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "UIWebView+FixmoExtensions.h"

@implementation UIWebView (Fixmo_Extensions)

- (void) makeEditable
{
	[self stringByEvaluatingJavaScriptFromString:@"document.body.contentEditable=true;document.designMode='on';void(0)"];
}

- (void) encloseBodyInQuoteStyle
{
	// give it a nice blue inset
	NSString *jsScript = @"document.body.style.color = 'blue'; " \
						@"document.body.style.textIndent = '5px';" \
						@"document.body.style.paddingLeft = '5px';" \
						@"document.body.style.borderLeft = '2px ridge blue';";

	[self stringByEvaluatingJavaScriptFromString:jsScript];
}

- (NSString *) bodyHTML
{
	return [self stringByEvaluatingJavaScriptFromString: @"document.body.innerHTML"];
}

- (NSString *) allHTML
{
	return [self stringByEvaluatingJavaScriptFromString: @"document.documentElement.outerHTML"];
}

- (NSString *) bodyText
{
	return [self stringByEvaluatingJavaScriptFromString: @"document.body.textContent"];
}

- (NSString *) bodyTextInQuoteStyle
{
	static NSString *bodyPrefix = @"<div class=\"QuotedBody\" style=\"color: blue; text-indent: 5px; padding-left: 5px; border-left-width: 2px; border-left-style: ridge; border-left-color: blue; \"><br>";
	static NSString *bodySuffix = @"<br></div>";

	NSString *bodyString = [NSString stringWithFormat:@"\n%@\n", [self stringByEvaluatingJavaScriptFromString: @"document.body.textContent"]];
	return [NSString stringWithFormat:@"%@%@%@", bodyPrefix, bodyString, bodySuffix];
}

- (NSString *) allText
{
	return [self stringByEvaluatingJavaScriptFromString: @"document.documentElement.textContent"];
}

- (int) bodyHeight
{
	return [[self stringByEvaluatingJavaScriptFromString: @"document.height"] intValue];
}

- (void) setBodyTextSizePercent:(int)inPercent
{
	[self stringByEvaluatingJavaScriptFromString:[[NSString alloc] initWithFormat:@"documentbody.style.webkitTextSizeAdjust='%d%%'", inPercent]];
}

- (NSString *) prependTextToBody:(NSString *)inString
{
	NSString *theString = [NSString stringWithFormat:@"<p>%@</p>", inString];
	NSString *jsScript = [NSString stringWithFormat:@"document.body.insertAdjacentHTML(\"afterbegin\", \"%@\");", theString];
	NSString *theResult = [self stringByEvaluatingJavaScriptFromString:jsScript];
	return theResult;
}

- (NSString *) appendTextToBody:(NSString *)inString
{
	NSString *theString = [NSString stringWithFormat:@"<p>%@</p>", inString];
	NSString *jsScript = [NSString stringWithFormat:@"document.body.insertAdjacentHTML(\"beforeend\", \"%@\");", theString];
	NSString *theResult = [self stringByEvaluatingJavaScriptFromString:jsScript];
	return theResult;
}

@end