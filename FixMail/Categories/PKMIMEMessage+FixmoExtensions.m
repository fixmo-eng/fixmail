//
//  PKMIMEMessage+FixmoExtensions.m
//
//  Created by Colin Biggin on 2013-03-28.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "PKMIMEMessage+FixmoExtensions.h"
#import "UIView-ViewFrameGeometry.h"
#import "ASEmail.h"
#import "AttachmentManager.h"

// Constants
static const int kDefaultAudioAttachmentHeight          = 24;
static const int kDefaultApplicationAttachmentHeight    = 800;
static const int kDefaultVideoAttachmentHeight          = 800;

@implementation PKMIMEMessage (Fixmo_Extensions)

+ (NSString*)bodyAsString:(NSData *)inData
{
	if (inData.length == 0) return @"";

    NSString* aBodyString = NULL;
    
    PKMIMEData* aPKMIMEData         = [PKMIMEData dataFromDataWithHeaders:inData];
    PKContentType* aPKContentType   = [aPKMIMEData contentType];
    if([aPKContentType isMultipart]) {
        PKMIMEMessage* aMIMEMessage = [PKMIMEMessage messageWithData:aPKMIMEData];
        if([aMIMEMessage hasAlternatives]) {
            // Multipart alternative
            //
            aBodyString = [PKMIMEMessage stringFromMultipartAlternative:aMIMEMessage contentType:@"text/plain"];
            if(!aBodyString) {
                 aBodyString = [PKMIMEMessage stringFromMultipartAlternative:aMIMEMessage contentType:@"text/plain"];
                if(aBodyString) {
                    aBodyString = [PKMIMEMessage textFromHTML:aBodyString];
                }
            }
        }else{
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME bodyAsString multipart");
            if([[aPKContentType string] hasPrefix:@"multipart/related"]) {
            }else if([[aPKContentType string] hasPrefix:@"multipart/signed"]) {
            }else{
            }
        }
    }else{
        aBodyString = [self mimeBodyAsString:aPKContentType mimeData:aPKMIMEData];
    }
    
    return aBodyString;
}

#pragma mark - Internal methods

+ (NSMutableString*)stringFromMultipartAlternative:(PKMIMEMessage*)aMIMEMessage contentType:(NSString*)aContentType
{
    NSMutableString* aString = NULL;
    NSArray* anAlternatives = [aMIMEMessage alternatives];
    int aCount = [anAlternatives count];
    for(int i = aCount-1 ; i >= 0 ; --i) {
        PKContentType* anAlternativeContentType = [anAlternatives objectAtIndex:i];
        PKMIMEData* aAlternativeData = [aMIMEMessage dataForAlternativeNumber:i];
        if([anAlternativeContentType isMultipart]) {
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME stringFromMultipartAlternative isMultipart");
        }else if([anAlternativeContentType isText]) {
            if([[anAlternativeContentType string] hasPrefix:aContentType]) {
                aString = [NSMutableString stringWithString:[aAlternativeData decodedString]];
                break;
            }else{
            }
        }
    }
    return aString;
}

+ (NSString *)textFromHTML:(NSString *)anHtml
{    
    // This isn't a very good HTML flattener, may need to use DTCoreText instead?
    //
    NSScanner* aScanner;
    NSString* aText = nil;
    aScanner = [NSScanner scannerWithString:anHtml];
    
    while ([aScanner isAtEnd] == NO) {
        [aScanner scanUpToString:@"<" intoString:NULL] ; 
        [aScanner scanUpToString:@">" intoString:&aText] ;
        anHtml = [anHtml stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", aText] withString:@""];
    }

    anHtml = [anHtml stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return anHtml;
}

+ (NSString *)textFromRTF:(NSString *)inRTF
{
	return @"";
}

+ (NSString*) mimeBodyAsString:(PKContentType*)aPKContentType mimeData:(PKMIMEData*)aMimeData
{
    FXDebugLog(kFXLogPKMime, @"addMIMEToView: %@", [aPKContentType string]);

    NSString* aString = @"";
	NSString* aContentTypeString = [aPKContentType string];
    if([aPKContentType isText]) {
        if([aContentTypeString hasPrefix:@"text/plain"]) {
            aString = [aMimeData decodedString];
        }else if([aContentTypeString hasPrefix:@"text/html"]) {
            aString = [PKMIMEMessage textFromHTML:[aMimeData decodedString]];    
        }else if([aContentTypeString hasPrefix:@"text/calendar"]) {
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME calendar MIME part: %@\n%@", aContentTypeString, [aMimeData decodedString]);
        }else{
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME text unknown MIME part: %@", aContentTypeString);
        }
    }else if([aPKContentType isApplication]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        if([aFileName length] > 0) {
            if([aContentTypeString hasPrefix:@"application/pdf"]
               || [aContentTypeString hasPrefix:@"application/msword"]
               || [aContentTypeString hasPrefix:@"application/vnd.ms-excel"]) {
            }else{
                FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME unknown application MIME part may not work in web view: %@", aContentTypeString);
            }
        }else{
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME application MIME has no file name");
        }
    }else if([aPKContentType isImage]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME image MIME part: %@", aContentTypeString);
    }else if([aPKContentType isAudio]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME audio MIME part: %@", aContentTypeString);
    }else if([aPKContentType isVideo]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME video MIME part: %@", aContentTypeString);
    }else if([aPKContentType isData]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME data MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMessage]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME message MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMultipart]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME multipart MIME shouldnt get here: %@", aContentTypeString);
    }else{
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME shouldnt get here: %@", aContentTypeString);
    }
    
    return aString;
}

+ (void)_addTextToView:(UIWebView*)anWebView body:(NSString*)aBody top:(int)aTop
{
    [anWebView loadData:[aBody dataUsingEncoding:NSUTF8StringEncoding] MIMEType:@"text/plain" textEncodingName:@"UTF-8" baseURL:nil];
}

+ (void)_addHTMLToView:(UIWebView*)anWebView body:(NSString*)aBody top:(int)aTop
{
	NSString* aWebViewSizeString = [NSString stringWithFormat:@"<meta name=\"viewport\" id=\"iphone-viewport\" content=\"minimum-scale=1.0, maximum-scale=4.0 width=%f\">", anWebView.frame.size.width];

    [anWebView loadHTMLString:[NSString stringWithFormat:@"%@%@",  aWebViewSizeString, aBody] baseURL:nil];
}

+ (void)_addFileToWebView:(UIWebView*)anAddToView
                    fileName:(NSString*)aFileName 
                        data:(NSData*)aData 
                         top:(int)aTop
                      height:(int)aHeight   // FIXME should try to get this from the DOM
{
    // Write the file to attachment directory
    //
    NSString* aFilePath = [[AttachmentManager attachmentsPathWithEmail:nil] stringByAppendingPathComponent:aFileName];
    [aData writeToFile:aFilePath atomically:FALSE];
    NSURL* aURL = [NSURL URLWithString:aFilePath];
    
    /*
    // Create a WebView
    //
    CGFloat aContentWidth = anAddToView.size.width;
    
    WebViewController* aWebController = [[WebViewController alloc] initWithNibName:@"WebView" bundle:[FXLSafeZone getResourceBundle]];
    
    UIView* aView = [aWebController view];
	aView.autoresizingMask    = UIViewAutoresizingFlexibleWidth;
    
    aView.frame = CGRectMake(-3, aTop, aContentWidth, aHeight+24);
    
    [anAddToView addSubview:aView];
    
    UIWebView* aWebView = [aWebController webView];
    [bodyViews addObject:aWebView];
    
    // Load the file in to the web view
    //
    [aWebController loadURL:aURL];
    
    return aWebView;
     */
    [anAddToView loadRequest:[NSURLRequest requestWithURL:aURL]];
}

+ (UIView*)_addImageToView:(UIWebView*)anAddToView data:(NSData*)aData top:(int)aTop
{        
    UIImage* anImage = [[UIImage alloc] initWithData:aData];
    
    CGFloat height  = anImage.size.height;
    CGFloat width   = anImage.size.width;
    if(width <= 0.0f || height <= 0.0f) {
        FXDebugLog(kFXLogPKMime, @"_addImageToView image invalid");
    }
    
    UIImageView* anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, aTop, width, height)];    
    [anImageView setImage:anImage];
    [anAddToView.scrollView addSubview:anImageView];
    
    return anImageView;
}

+ (NSString*)cleanContentID:(NSString*)aContentID
{
    NSString* aCleanContentID = aContentID;
    NSRange atRange = [aContentID rangeOfString:@"@" options:0 range:NSMakeRange(0, [aContentID length])];
    if(atRange.location != NSNotFound) {
        aCleanContentID  = [aContentID substringToIndex:atRange.location];
    }
    return aCleanContentID;
}

+ (int)handleMultipartRelated:(PKMIMEMessage*)aMIMEMessage
                  contentType:(PKContentType*)aPKContentType                    
                          top:(int)aTop 
                    addToView:(UIWebView*)aView
{
    int aBottom = aTop;
    
    // multipar/related implemented assuming an text/HTML first part which will reference ContentID's in subsequent parts, like images
    // Other variations will probably fail
    //
    NSMutableArray* aContentIDs     = [NSMutableArray arrayWithCapacity:5];
    NSString* anAttachementsPath    = [AttachmentManager attachmentsPathWithEmail:nil];
    NSMutableString* anHtmlString   = NULL;
    
    NSUInteger numParts = [aMIMEMessage numParts];
    for(NSUInteger i = 0 ; i < numParts ; ++i) {
        PKMIMEMessage* aPartMessage     = [aMIMEMessage partAtIndex:i];            
        PKContentType* aPKContentType   = [aPartMessage contentType];
        NSString* aContentTypeString    = [aPKContentType string];

        FXDebugLog(kFXLogPKMime, @"handleMIMEPart %d: %@", i, aContentTypeString);
        
        if([aContentTypeString hasPrefix:@"text/html"]) {
            PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
            anHtmlString = [NSMutableString stringWithString:[aPartData decodedString]];
            
        }else if([aContentTypeString hasPrefix:@"multipart"]) {
            if([aPartMessage hasAlternatives]) {
                anHtmlString = [self stringFromMultipartAlternative:aPartMessage contentType:@"text/html"];
            }else{
                FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME Multipart in Multipart related: %@", aContentTypeString);
            }
        }else {
            PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
            
            // Get the ContentID for this part
            //
            NSString* aContentID = [aPartData contentID];
            [aContentIDs addObject:aContentID];
            NSString* aCleanContentID = [self cleanContentID:aContentID];
            
            
            // Write the CID part's rawData to the attachment directory with a cleaned contentID
            //
            NSString* aFilePath = [anAttachementsPath stringByAppendingPathComponent:aCleanContentID];
            NSData* aRawData = [aPartData rawData];
            FXDebugLog(kFXLogActiveSync, @"Multipart related contentID = %d %@", [aRawData length], aFilePath);
            
            [aRawData writeToFile:aFilePath atomically:FALSE];
            
            // Debug image
            //NSData* anImageData = [NSData dataWithContentsOfFile:aFilePath];
            //[self _addImageToView:aView data:anImageData top:aTop];
        }
    }
    
    if([anHtmlString length] > 0) {
        // Replace all of the CID src strings in the HTML string with local file references in attachements directory
        //
        for(NSString* aContentID in aContentIDs) {
            NSString* aCIDString = [@"cid:" stringByAppendingString:aContentID];
            NSRange aCIDRange = [anHtmlString rangeOfString:aCIDString options:0 range:NSMakeRange(0, [anHtmlString length])];
            if(aCIDRange.location != NSNotFound) {
                FXDebugLog(kFXLogPKMime, @"Replace CID: %@", aCIDString);
                NSString* aCleanContentID = [self cleanContentID:aContentID];
                NSString* aFileURL = aCleanContentID;
                [anHtmlString replaceOccurrencesOfString:aCIDString withString:aFileURL options:0 range:aCIDRange];
            }else{
                FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME CID not found: %@", aCIDString);
            }
        }
        
        // Create a WebView and load the HTML string with attachments
        //
        [self _addHTMLToView:aView body:NULL top:aTop];
        NSURL* aURL = [NSURL fileURLWithPath:anAttachementsPath];
		FXDebugLog(kFXLogPKMime, @"multipart/related %@ html: %@", [aURL absoluteString], anHtmlString);

        [aView loadHTMLString:anHtmlString baseURL:aURL];
        
    }else{
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME handleMultipartRelated no HTML");
    }
#warning FIXME - Need to clean up attachments directory when done

    return aBottom;
}

+ (void)addMIMEToView:(PKContentType*)aPKContentType
                mimeData:(PKMIMEData*)aMimeData 
                    view:(UIWebView*)aWebView
                     top:(int)aTop
{
    FXDebugLog(kFXLogPKMime, @"addMIMEToView: %@", [aPKContentType string]);

    NSString* aContentTypeString = [aPKContentType string];
    if([aPKContentType isText]) {

        if([aContentTypeString hasPrefix:@"text/plain"]) {
            
            [aWebView loadData:[[aMimeData decodedString] dataUsingEncoding:NSUTF8StringEncoding] MIMEType:@"text/plain" textEncodingName:@"UTF-8" baseURL:nil];
        }else if([aContentTypeString hasPrefix:@"text/html"]) {
            [self _addHTMLToView:aWebView body:[aMimeData decodedString] top:aTop];
//            NSString* aWebViewSizeString = [NSString stringWithFormat:@"<meta name=\"viewport\" id=\"iphone-viewport\" content=\"minimum-scale=1.0, maximum-scale=4.0 width=%f\">", aWebView.frame.size.width];
//            
//            [aWebView loadHTMLString:[NSString stringWithFormat:@"%@%@",  aWebViewSizeString, [aMimeData decodedString]] baseURL:nil];
        }else if([aContentTypeString hasPrefix:@"text/calendar"]) {
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME calendar MIME part: %@\n%@", aContentTypeString, [aMimeData decodedString]);
        }else{
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME text unknown MIME part: %@", aContentTypeString);
        }

    }else if([aPKContentType isApplication]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        if([aFileName length] > 0) {
            if([aContentTypeString hasPrefix:@"application/pdf"]
            || [aContentTypeString hasPrefix:@"application/msword"]
            || [aContentTypeString hasPrefix:@"application/vnd.ms-excel"]) {
                [PKMIMEMessage _addFileToWebView:aWebView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultApplicationAttachmentHeight];
            }else if([aContentTypeString hasPrefix:@"application/x-pkcs7-mime"]) {
                [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@ not implemented", @"ErrorAlert", @"Error alert view title when something not implemented"), @"x-pkcs7-mime"]];
            }else if([aContentTypeString hasPrefix:@"application/x-pkcs7-signature"]) {
                [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@ not implemented", @"ErrorAlert", nil), @"x-pkcs7-signature"]];
            }else if([aContentTypeString hasPrefix:@"application/ics"]) {
                FXDebugLog(kFXLogASEmail, @"ics attachment not rendered");
            }else{
                FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME unknown application MIME part may not work in web view: %@", aContentTypeString);
                [PKMIMEMessage _addFileToWebView:aWebView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultApplicationAttachmentHeight];
            }
        }else{
            FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME application MIME has no file name");
        }
    }else if([aPKContentType isImage]) {
        [PKMIMEMessage _addImageToView:aWebView data:[aMimeData rawData] top:aTop];
    }else if([aPKContentType isAudio]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        [PKMIMEMessage _addFileToWebView:aWebView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultAudioAttachmentHeight];
    }else if([aPKContentType isVideo]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME video MIME part: %@", aContentTypeString);
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        [PKMIMEMessage _addFileToWebView:aWebView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultVideoAttachmentHeight];
    }else if([aPKContentType isData]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME data MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMessage]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME message MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMultipart]) {
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME multipart MIME shouldnt get here: %@", aContentTypeString);
    }else{
        FXDebugLog(kFXLogFIXME + kFXLogPKMime, @"FIXME shouldnt get here: %@", aContentTypeString);
    }
}

+ (int)handleMultipart:(PKMIMEMessage*)aMIMEMessage
           contentType:(PKContentType*)aPKContentType                    
                   top:(int)aTop 
             addToView:(UIWebView*)aWebView
{
    int aBottom = aTop;
    
    FXDebugLog(kFXLogPKMime, @"handleMultipart:\n\tContentType\n%@\n%@", [aPKContentType string], [aPKContentType parameters]);
    
    if([aMIMEMessage hasAlternatives]) {
        // Multipart alternative
        //
        NSArray* anAlternatives = [aMIMEMessage alternatives];
        int aCount = [anAlternatives count];
        for(int i = aCount-1 ; i >= 0 ; --i) {
            PKContentType* anAlternativeContentType = [anAlternatives objectAtIndex:i];
            FXDebugLog(kFXLogPKMime, @"handleMultipart alternatives %d: %@", i, [anAlternativeContentType string]);
            PKMIMEData* aAlternativeData = [aMIMEMessage dataForAlternativeNumber:i];
            if([anAlternativeContentType isMultipart]) {
                // Warning: this is recursing
                //
                PKMIMEMessage* anAlternativeMIMEMessage = [PKMIMEMessage messageWithData:aAlternativeData];
                aBottom = [self handleMultipart:anAlternativeMIMEMessage contentType:anAlternativeContentType top:aTop addToView:aWebView];
            }else{
                [self addMIMEToView:anAlternativeContentType mimeData:aAlternativeData view:aWebView top:aTop];
                    aBottom = aWebView.scrollView.bottom;
            }
            if(aBottom > aTop) {
                break;
            }
        }
    }else{
        NSString* aContentTypeString = [aPKContentType string];
        if([aContentTypeString hasPrefix:@"multipart/related"]) {
            // Multipart related
            //
            aBottom = [PKMIMEMessage handleMultipartRelated:aMIMEMessage contentType:aPKContentType top:aTop addToView:aWebView];
        }else if([aContentTypeString hasPrefix:@"multipart/mixed"]) {
            // Multipart mixed
            //
            NSUInteger numParts = [aMIMEMessage numParts];
            for(NSUInteger i = 0 ; i < numParts ; ++i) {
                PKMIMEMessage* aPartMessage = [aMIMEMessage partAtIndex:i];            
                PKContentType* aPKContentType = [aPartMessage contentType];
                NSString* aContentTypeString = [aPKContentType string];
                FXDebugLog(kFXLogPKMime, @"handleMIMEPart %d: %@", i, aContentTypeString);
                if([aContentTypeString hasPrefix:@"multipart"]) {
                    aBottom = [self handleMultipart:aPartMessage contentType:aPKContentType top:aTop addToView:aWebView];
                }else {
                    PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
                    [self addMIMEToView:aPKContentType mimeData:aPartData view:aWebView top:aTop];

                        aBottom = aWebView.scrollView.bottom;
                }
                aTop = aBottom;
            }
        }else if([aContentTypeString hasPrefix:@"multipart/signed"]) {
            // Multipart signed
            //
            [PKMIMEMessage handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@ not implemented", @"ErrorAlert", nil), @"multipart/signed"]];
        }else{
            [PKMIMEMessage handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@ not implemented", @"ErrorAlert", nil), aContentTypeString]];
        }
    }
    return aBottom;
}

+ (int)mimeBodyBlock:(NSData *)aData
             top:(int)aTop 
       addToView:(UIWebView*)inWebView
{
    @try {
		PKMIMEData* aPKMIMEData         = [PKMIMEData dataFromDataWithHeaders:aData];
		PKContentType* aPKContentType   = [aPKMIMEData contentType];
		PKMIMEMessage* aMimeMessage     = [PKMIMEMessage messageWithData:aPKMIMEData];
		if ([aMimeMessage hasAlternatives]) {
			PKMIMEData *bestGuess = [aMimeMessage dataForBestAlternative];
			if([[[bestGuess contentType] string] isEqualToString:@"text/calendar"]) {
				[PKMIMEMessage addMIMEToView:[bestGuess contentType] mimeData:bestGuess view:inWebView top:aTop];
			}else{
				PKMIMEData* anHTMLData = [aMimeMessage dataForMediaType:@"text/html"];
				if(anHTMLData) {
					[self _addHTMLToView:inWebView body:[anHTMLData decodedString] top:aTop];
//                            [PKMIMEMessage addMIMEToView:[anHTMLData contentType] mimeData:anHTMLData view:inWebView top:aTop];
				}else{
					[PKMIMEMessage addMIMEToView:[bestGuess contentType] mimeData:bestGuess view:inWebView top:aTop];
				}
			}
		}else if([aPKContentType isMultipart]) {
			[self handleMultipart:aMimeMessage contentType:aPKContentType top:aTop addToView:inWebView];
		}else{
			[self addMIMEToView:aPKContentType mimeData:aPKMIMEData view:inWebView top:aTop];
		}

		[inWebView.scrollView setContentOffset:CGPointMake(0.0f, 0.0f)];

    }@catch (NSException* e) {
        [PKMIMEMessage _logException:@"mimeBodyBlock" exception:e];
    }

	return inWebView.scrollView.bottom;
}

+ (int)webBodyBlock:(NSData *)aData
             top:(int)aTop 
       addToView:(UIWebView*)inWebView
{
    @try {

		NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
		aDataString = [aDataString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
		if([aDataString isEqualToString:@"{no text body}"]) {
			// Attachments only
		}else{
			FXDebugLog(kFXLogPKMime, @"bodyBlock2 HTML: %@", aDataString);
			[self _addHTMLToView:inWebView body:aDataString top:aTop];    
		}

		[inWebView.scrollView setContentOffset:CGPointMake(0.0f, 0.0f)];

    }@catch (NSException* e) {
        [PKMIMEMessage _logException:@"webBodyBlock" exception:e];
    }

	return inWebView.scrollView.bottom;
}

+ (int) unknownBodyBlock:(NSData *)aData
             top:(int)aTop 
       addToView:(UIWebView*)inWebView
{
    @try {

		NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
		if([aDataString isEqualToString:@"{no text body}"]) {
			// Attachments only
		}else{
			FXDebugLog(kFXLogPKMime, @"bodyBlock Text or RTF: %@", aDataString);
			[self _addTextToView:inWebView body:aDataString top:aTop];
		}

		[inWebView.scrollView setContentOffset:CGPointMake(0.0f, 0.0f)];

    }@catch (NSException* e) {
        [PKMIMEMessage _logException:@"unknownBodyBlock" exception:e];
    }

	return inWebView.scrollView.bottom;
}

+ (int)bodyBlock:(ASEmail*)anEmail
             top:(int)aTop 
       addToView:(UIWebView*)inWebView
{
    @try {
        NSData* aData = anEmail.body;
        if([aData length] > 0) {
            if(anEmail.isMIME) {
//                PKMIMEData* aPKMIMEData         = [PKMIMEData dataFromDataWithHeaders:aData];
//                PKContentType* aPKContentType   = [aPKMIMEData contentType];
//                PKMIMEMessage* aMimeMessage     = [PKMIMEMessage messageWithData:aPKMIMEData];
//                if ([aMimeMessage hasAlternatives]) {
//                    PKMIMEData *bestGuess = [aMimeMessage dataForBestAlternative];
//                    if([[[bestGuess contentType] string] isEqualToString:@"text/calendar"]) {
//                        [PKMIMEMessage addMIMEToView:[bestGuess contentType] mimeData:bestGuess view:inWebView top:aTop];
//                    }else{
//                        PKMIMEData* anHTMLData = [aMimeMessage dataForMediaType:@"text/html"];
//                        if(anHTMLData) {
//							[self _addHTMLToView:inWebView body:[anHTMLData decodedString] top:aTop];
////                            [PKMIMEMessage addMIMEToView:[anHTMLData contentType] mimeData:anHTMLData view:inWebView top:aTop];
//                        }else{
//                            [PKMIMEMessage addMIMEToView:[bestGuess contentType] mimeData:bestGuess view:inWebView top:aTop];
//                        }
//                    }
//                }else if([aPKContentType isMultipart]) {
//                    [self handleMultipart:aMimeMessage contentType:aPKContentType top:aTop addToView:inWebView];
//                }else{
//                    [self addMIMEToView:aPKContentType mimeData:aPKMIMEData view:inWebView top:aTop];
//                }
				[PKMIMEMessage mimeBodyBlock:aData top:aTop addToView:inWebView];
            }else if(anEmail.isHTML) {
//                NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
//                aDataString = [aDataString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
//                if([aDataString isEqualToString:@"{no text body}"]) {
//                    // Attachments only
//                }else{
//                    FXDebugLog(kFXLogPKMime, @"bodyBlock2 HTML: %@", aDataString);
//                    [self _addHTMLToView:inWebView body:aDataString top:aTop];    
//                }
				[PKMIMEMessage webBodyBlock:aData top:aTop addToView:inWebView];
            }else{
//                NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
//                if([aDataString isEqualToString:@"{no text body}"]) {
//                    // Attachments only
//                }else{
//                    FXDebugLog(kFXLogPKMime, @"bodyBlock Text or RTF: %@", aDataString);
//                    [self _addTextToView:inWebView body:aDataString top:aTop];
//                }
				[PKMIMEMessage unknownBodyBlock:aData top:aTop addToView:inWebView];
            }
            
//            [inWebView.scrollView setContentOffset:CGPointMake(0.0f, 0.0f)];
        }else{
//			[self startActivity:@"Parsing..." animated:true];
//            [self _loadingActivityIndicator:aView];
		
        }
    }@catch (NSException* e) {
        [PKMIMEMessage _logException:@"bodyBlock" exception:e];
    }  
    
	return inWebView.scrollView.bottom;
}

+ (void)handleError:(NSString*)anErrorMessage
{
//    [ErrorAlert alert:FXLLocalizedStringFromTable(@"PKMIMEMESSAGE", @"ErrorAlert", @"PKMIMEMessage error alert view title) message:anErrorMessage];
}

+ (void)_logException:(NSString*)where exception:(NSException*)e
{
//    [ErrorAlert exception:FXLLocalizedStringFromTable(@"PKMIMEMESSAGE", @"ErrorAlert", nil) where:where exception:e];
}

@end