//
//  UIWebView+FixmoExtensions.h
//
//  Created by Colin Biggin on 2013-07-03.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

@interface UIWebView (Fixmo_Extensions)

- (void) makeEditable;
- (void) encloseBodyInQuoteStyle;

- (NSString *) bodyHTML;
- (NSString *) allHTML;

- (NSString *) bodyText;
- (NSString *) bodyTextInQuoteStyle;
- (NSString *) allText;

- (int) bodyHeight;
- (void) setBodyTextSizePercent:(int)inPercent;

- (NSString *) prependTextToBody:(NSString *)inString;
- (NSString *) appendTextToBody:(NSString *)inString;

@end

