//
//  UIImageView+FixmoExtensions.h
//  FixMail
//
//  Created by Colin Biggin on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

@interface UIImageView (FixmoExtensions)
- (void) rotateAndReplace:(int)inDegrees withImage:(UIImage *)inImage;
@end
