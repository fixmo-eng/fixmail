//
//  PKMIMEMessage+FixmoExtensions.h
//
//  Created by Colin Biggin on 2013-03-28.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "PKMIMEMessage.h"

@class ASEmail;

@interface PKMIMEMessage (Fixmo_Extensions)

+ (NSString *) bodyAsString:(NSData *)inData;
+ (NSString *) textFromHTML:(NSString *)anHtml;
+ (NSString *) textFromRTF:(NSString *)inRTF;
+ (NSString *) mimeBodyAsString:(PKContentType *)aPKContentType mimeData:(PKMIMEData *)aMimeData;

+ (int)bodyBlock:(ASEmail*)anEmail
             top:(int)aTop 
       addToView:(UIWebView*)inWebView;

+ (int)mimeBodyBlock:(NSData *)aData
             top:(int)aTop 
       addToView:(UIWebView*)inWebView;

+ (int)webBodyBlock:(NSData *)aData
             top:(int)aTop 
       addToView:(UIWebView*)inWebView;

@end

