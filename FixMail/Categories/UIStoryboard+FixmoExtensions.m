//
//  UIStoryboard+FixmoExtensions.m
//
//  Created by Colin Biggin on 2012-11-16.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//
#import "UIStoryboard+FixmoExtensions.h"
#import "FXLSafeZone.h"

@implementation UIStoryboard (Fixmo_Extensions)

+ (UIStoryboard *) storyboardWithName:(NSString *)inName
{
	if (inName.length == 0) return nil;
	return [UIStoryboard storyboardWithName:inName bundle:[FXLSafeZone getResourceBundle]];
}

+ (id) storyboardWithName:(NSString *)inName identifier:(NSString *)inIdentifier
{
	if (inName.length == 0) return nil;
	if (inIdentifier.length == 0) return nil;

	UIStoryboard *theStoryboard = [UIStoryboard storyboardWithName:inName];
	return [theStoryboard instantiateViewControllerWithIdentifier:inIdentifier];
}

@end