//
//  UIButton+FixmoExtensions.h
//
//  Created by Colin Biggin on 2013-07-18.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

@interface UIButton (Fixmo_Extensions)

+ (UIButton *) createArrowUpWithTitle:(NSString *)inTitle;
+ (UIButton *) createArrowDownWithTitle:(NSString *)inTitle;

@end

