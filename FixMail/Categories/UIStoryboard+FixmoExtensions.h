//
//  UIStoryboard+FixmoExtensions.h
//
//  Created by Colin Biggin on 2012-11-16.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

@interface UIStoryboard (Fixmo_Extensions)
+ (UIStoryboard *) storyboardWithName:(NSString *)inName;
+ (id) storyboardWithName:(NSString *)inName identifier:(NSString *)inIdentifier;
@end

