//
//  UIButton+FixmoExtensions.m
//
//  Created by Colin Biggin on 2013-07-18.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "UIButton+FixmoExtensions.h"

@implementation UIButton (Fixmo_Extensions)

+ (UIButton *) createWithTitle:(NSString *)inTitle
		image:(NSString *)inImageName
		normal:(UIColor *)inNormalColor
		highlight:(UIColor *)inHighlightColor
{
	NSParameterAssert(inImageName != nil);
	NSParameterAssert(inNormalColor != nil);
	NSParameterAssert(inHighlightColor != nil);

	UIImage *arrowImage = [UIImage imageNamed:inImageName];

	UIButton *theButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[theButton setImage:arrowImage forState:UIControlStateNormal];
	[theButton setTitle:inTitle forState:UIControlStateNormal];
	[theButton setTitleColor:inNormalColor forState:UIControlStateNormal];
	[theButton setTitleColor:inHighlightColor forState:UIControlStateHighlighted];
	theButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
	theButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

	// this will size it perfectly
	[theButton sizeToFit];

	// now have to add some separation so that it is:
	//
	// |__________image_____title__________|
	//
	// ie, 10 pixels from the edge and 5 pixels between the image and title
	//
	theButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 15.0, 0.0, 0.0);
	theButton.imageEdgeInsets = UIEdgeInsetsMake(3.0, 10.0, 0.0, 0.0);
	theButton.layer.cornerRadius = 5.0;
	theButton.layer.borderWidth = 0.5;
	theButton.layer.borderColor = [inNormalColor CGColor];

	// re-adjust the size
	CGRect frame = theButton.frame;
	frame.size.width += 25.0;
	frame.size.height = 28.0;
	theButton.frame = frame;

	return theButton;
}

+ (UIButton *) createArrowUpWithTitle:(NSString *)inTitle
{
	return [UIButton createWithTitle:inTitle
							   image:@"FixMailRsrc.bundle/arrowWhiteUp.png"
							  normal:[UIColor whiteColor]
						   highlight:[UIColor grayColor]
			];
}

+ (UIButton *) createArrowDownWithTitle:(NSString *)inTitle
{
	return [UIButton createWithTitle:inTitle
							   image:@"FixMailRsrc.bundle/arrowWhiteDown.png"
							  normal:[UIColor whiteColor]
						   highlight:[UIColor grayColor]
			];
}

@end