//
//  UIColor+FixmoExtensions.m
//
//  Created by Colin Biggin on 2013-04-25.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "UIColor+FixmoExtensions.h"

@implementation UIColor (Fixmo_Extensions)

+ (UIColor *) settingsSelectedColor
{
	return [UIColor colorWithHue:(218.0/360.0) saturation:0.57 brightness:0.52 alpha:1.0];
}

+ (UIColor *) mailTableViewSeparatorColor
{
    return [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.878 alpha:1];
}

@end