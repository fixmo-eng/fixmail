//
//  UIImageView+FixmoExtensions.m
//  FixMail
//
//  Created by Colin Biggin on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "UIImageView+FixmoExtensions.h"

#define RADIANS(__X) ((float) ((__X * M_PI) / 180.0))

@implementation UIImageView (FixmoExtensions)

- (void) rotateAndReplace:(int)inDegrees withImage:(UIImage *)inImage
{
	self.transform = CGAffineTransformIdentity;
 	[UIView animateWithDuration:0.2
		animations:^{
			self.transform = CGAffineTransformRotate(self.transform, RADIANS(inDegrees));
		}
		completion:^(BOOL inFinished) {
			if ((inFinished) && (inImage != nil)) {
				self.transform = CGAffineTransformIdentity;
				self.image = inImage;
			}
		}
	];
}

@end
