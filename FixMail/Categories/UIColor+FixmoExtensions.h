//
//  UIColor+FixmoExtensions.h
//
//  Created by Colin Biggin on 2013-04-25.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

@interface UIColor (Fixmo_Extensions)

+ (UIColor *) settingsSelectedColor;
+ (UIColor *) mailTableViewSeparatorColor;

@end

