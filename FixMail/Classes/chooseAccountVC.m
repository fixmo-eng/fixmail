//
//  AccountConfigureViewController.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/15/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "chooseAccountVC.h"
#import "ASConfigViewController.h"
#import "AppSettings.h"

@implementation chooseAccountVC

// Properties
@synthesize newAccount;
@synthesize accountNum;
@synthesize firstSetup;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
	[super viewDidUnload];
}

-(void)viewDidAppear:(BOOL)animated 
{
}

-(void)viewWillAppear:(BOOL)animated 
{
	[super viewWillAppear:animated];		
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	
	self.title = FXLLocalizedStringFromTable(@"CONFIGURE_ACCOUNT", @"chooseAccountVC", @"Title of view controller for new account configuration");

	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	self.navigationController.navigationBarHidden = false;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	if (IS_IPAD()) return YES;
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark - IBActions

-(IBAction) gmailActiveSyncClicked 
{
	ASConfigViewController* vc = [self getViewController];
	[self.navigationController pushViewController:vc animated:YES];

    [vc.hostnameField       setText:@"m.google.com"];
    [vc.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconGmail.png"];
    [vc.imageView           setImage:anImage];
    
	vc.firstSetup = self.firstSetup;
    vc.account    = nil;
	vc.accountNum = self.accountNum;
	vc.newAccount = YES;
    vc.accountSubType = AccountSubTypeGmail;

	vc.title = FXLLocalizedStringFromTable(@"GMAIL",  @"chooseAccountVC", @"Title of button for configuring Gmail account");
    
}

-(IBAction) exchangeClicked 
{
	ASConfigViewController* vc = [self getViewController];
    [self.navigationController pushViewController:vc animated:YES];

    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconExchange.png"];
    [vc.imageView setImage:anImage];
    
    [vc.portField setText:[NSString stringWithFormat:@"%d", 443]];
	vc.firstSetup = self.firstSetup;
    vc.account    = nil;
	vc.accountNum = self.accountNum;
	vc.newAccount = YES;
    vc.accountSubType = AccountSubTypeExchange;

	vc.title = FXLLocalizedStringFromTable(@"MS_EXCHANGE", @"chooseAccountVC", @"Title of button for configuring Gmail account");
    
}

-(IBAction) fixmoClicked_stk08
{
	ASConfigViewController* vc = [self getViewController];
	[self.navigationController pushViewController:vc animated:YES];
    
    [vc.hostnameField       setText:@"stk08-cas02.fixmo.corp"]; // FIXME for debug only, must remove
    [vc.domainField         setText:@"etp"];
    [vc.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconFixmo.png"];
    [vc.imageView setImage:anImage];
    
	vc.firstSetup = self.firstSetup;
    vc.account    = nil;
	vc.accountNum = self.accountNum;
	vc.newAccount = YES;
    vc.accountSubType = AccountSubTypeExchange;
    
	vc.title = FXLLocalizedStringFromTable(@"FIXMO_STK08",  @"chooseAccountVC",  @"Title of button for configuring Fixmo stk09");
    
}

-(IBAction) fixmoClicked_stk09
{
	ASConfigViewController* vc = [self getViewController];
	[self.navigationController pushViewController:vc animated:YES];
    
    [vc.hostnameField       setText:@"stk09.fixmo.com"]; // FIXME for debug only, must remove
    [vc.domainField         setText:@"szdev.local"];
    [vc.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconFixmo.png"];
    [vc.imageView setImage:anImage];
    
	vc.firstSetup = self.firstSetup;
    vc.account    = nil;
	vc.accountNum = self.accountNum;
	vc.newAccount = YES;
    vc.accountSubType = AccountSubTypeExchange;
    
	vc.title = FXLLocalizedStringFromTable(@"FIXMO_STK09", @"chooseAccountVC", @"Title of button for configuring Fixmo stk09");
    
}

#pragma mark - Private methods

- (ASConfigViewController *) getViewController
{
	ASConfigViewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Accounts_iPad identifier:@"ASConfigViewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Accounts_iPhone identifier:@"ASConfigViewController"];
	}

	return theController;
}
@end
