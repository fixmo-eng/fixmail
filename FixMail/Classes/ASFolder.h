/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseFolder.h"
#import "ASSync.h"

@class CommitObjects;
@class FastWBXMLParser;

// Notifications
#define kFolderSyncingNotification      @"kFolderSyncingNotification"
#define kFolderSyncedNotification       @"kFolderSyncedNotification"
#define kFolderNewObjectsNotification   @"kFolderNewObjectsNotification"
#define kFolderChangeObjectNotification @"kFolderChangeObjectNotification"
#define kFolderDeleteUidNotification    @"kFolderDeleteUidNotification"

#define kFolderObjects                  @"kFolderObjects"
#define kFolderObject                   @"kFolderObject"
#define kFolderObjectUid                @"kFolderObjectUid" 

@interface ASFolder : BaseFolder

// Properties
@property (nonatomic, strong)           NSString*   parentId;
@property (nonatomic, strong)           NSString*   syncKey;
@property (nonatomic, strong)           NSDate*     syncDate;
@property (nonatomic, readonly)         ESyncStatus syncStatus;
@property                               NSInteger   dbaseCommits;

// Construct/Destruct
- (id)initWithAccount:(BaseAccount*)anAccount;

// ActiveSync Parser
- (void)parser:(FastWBXMLParser*)aParser;

// ASFolder Virtual Methods
- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType;
- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;
- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;
- (void)resyncFolder;

// Setters
- (void)setSyncKey:(NSString*)aSyncKey syncDate:(NSDate*)aSyncDate;
+ (void)displaySyncStatus:(ESyncStatus)aSyncStatus;

// Getters
- (NSString*)ASClass;

// Sync
- (void)setNeedsInitialSync;
- (void)folderSyncing;
- (void)folderSynced;
- (void)sendFolderSynced;

- (void)folderCommitCompleted;
- (void)commitWithCallback:(CommitObjects*)aCommitObjects action:(SEL)anAction;

// Load/Store
- (void)loadFromStore:(NSDictionary*)aDictionary;
- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary;

// BaseFolder Overrides
- (void)commit;

@end
