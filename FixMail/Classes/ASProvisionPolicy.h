/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"

// FIXME - Needs to be stored someplace so we dont have to force a provision handshake at startup every time
//
@interface ASProvisionPolicy : BaseObject {
    // Password
    unsigned int            maxInactivityTimeDeviceLock;
    unsigned int            maxDevicePasswordFailedAttempts;
    unsigned int            minDevicePasswordLength;
    unsigned int            devicePasswordHistory;
    unsigned int            devicePasswordExpiration;
    
    BOOL                    allowSimpleDevicePassword;
    BOOL                    devicePasswordEnabled;
    BOOL                    passwordRecoveryEnabled;
    BOOL                    alphanumericDevicePasswordRequired;
    
    // Encryption
    BOOL                    deviceEncryptionEnabled;
    
    // Attachments
    unsigned int            maxAttachmentSize;
    BOOL                    attachmentsEnabled;
}

@end
