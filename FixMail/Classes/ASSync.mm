/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASSync.h"
#import "ASAccount.h"
#import "ASContact.h"
#import "ASFolder.h"
#import "ASSyncFolders.h"
#import "ASPing.h"
#import "ASSyncDelete.h"
#import "Event.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASSync

// Properties
@synthesize folder;
@synthesize syncDelegate;
@synthesize syncObjectDelegate;
@synthesize eventDelegate;
@synthesize contactDelegate;
@synthesize moreAvailableFolders;
@synthesize syncFolders;
@synthesize pingFolders;
@synthesize isChangeRequest;
@synthesize object;
@synthesize bodyType;
@synthesize bodyFetchType;

// Types
typedef enum {
    kMIMESupportForNone                     = 0,
    kMIMESupportForSMIME                    = 1,
    kMIMESupportForAll                      = 2,
} EMIMESupport;

typedef enum {
    kMIMETruncationAll                      = 0,
    kMIMETruncationOver4K                   = 1,
    kMIMETruncationOver5K                   = 2,
    kMIMETruncationOver7K                   = 3,
    kMIMETruncationOver10K                  = 4,
    kMIMETruncationOver20K                  = 5,
    kMIMETruncationOver50K                  = 6,
    kMIMETruncationOver100K                 = 7,
    kMIMETruncationNo                       = 8,
} EMIMETruncation;

// Constants
static NSString* kCommand               = @"Sync";
static const ASTag kCommandTag          = SYNC_SYNC;

static NSString* kTruncate              = @"0";
static NSString* kPreviewTruncationSize = @"256";
static NSString* kMaxTruncationSize     = @"4294967295";

static int sWindowSize                  = 100;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
        self.moreAvailableFolders   = [NSMutableArray arrayWithCapacity:2];
        self.pingFolders            = nil;
        
        syncDelegate        = nil;
        eventDelegate       = nil;
		contactDelegate		= nil;
        folder              = nil;
        message             = nil;
        pingFolders         = nil;
        syncFolders         = nil;
        object              = nil;
        //..
        // Options
        bodyType        = [anAccount bodyType];
        [super setIsQueued];
    }
    return self;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Sync
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Sync

+ (ASSync*)sendSync:(ASAccount*)anAccount
     syncFolders:(NSArray*)aSyncFolders 
     pingFolders:(NSArray*)aPingFolders
      bodyFetchType:(EBodyFetchType)aBodyFetchType
        delegate:(NSObject<ASSyncDelegate>*)aDelegate
{
    ASSync* aSync = nil;
    @try {
        NSUInteger aCount = aSyncFolders.count;
        if(aCount > 0) {

            aSync = [[ASSync alloc] initWithAccount:anAccount];
            aSync.syncDelegate      = aDelegate;
            aSync.syncFolders       = aSyncFolders;
            aSync.pingFolders       = aPingFolders;
            aSync.bodyFetchType     = aBodyFetchType;
            
            NSData* aWbxml = [aSync wbxmlSync:aSyncFolders fetchBody:FALSE/*fetchBody*/];
//            NSData* aWbxml = [aSync wbxmlSync:aSyncFolders fetchBody:TRUE];

            NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
            [anAccount send:aURLRequest httpRequest:aSync];
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME sync with no folders");
            return nil;
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendSync" exception:e];
    }
    return aSync;
}

- (NSData*)wbxmlSync:(NSArray*)aFolders fetchBody:(BOOL)fetchBody
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            NSUInteger aCount = [aFolders count];
            for(int i = 0 ; i < aCount ; ++i) {
                ASFolder* aFolder = [aFolders objectAtIndex:i];
                if(![aFolder isSyncing]) {
                    [aFolder setIsSyncing:TRUE];
                }else{
                    FXDebugLog(kFXLogActiveSync, @"Folder already syncing: %@", aFolder.displayName);
                }
                //FXDebugLog(kFXLogActiveSync, @"sync folder: %@", aFolder);
                
                wbxml.start(SYNC_COLLECTION); {
                    // Reference MS-ASCMD 2.2.3.29.2 The order of elements in Collection is STRICTLY enforced
                    //
                    //  1. SyncKey           Required
                    //  2. CollectionId      Required
                    //  3. Supported         Required for Calendar and Contacts maybe
                    //  4. DeletesAsMoves    Optional
                    //  5. GetChanges        Required on 12.0, optional on 12.1 where defaults should be correct
                    //  6. WindowSize        Optional
                    //  7. ConversationMode  Optional - only applicable to emails
                    //  8. Options           Optional
                    //  9. Commands          Required for command operations
                    //
                    NSString* aSyncKey = [aFolder syncKey];
					NSAssert(aSyncKey != nil, @"aSyncKey != nil");

                    if([aSyncKey isEqualToString:kInitialSyncKey] && ![aFolder isInitialSynced]) {
                        //FXDebugLog(kFXLogActiveSync, @"Starting initial sync on: %@", [aFolder displayName]);
                        [aFolder setIsInitialSync:TRUE];
                    }
                    wbxml.keyValue(SYNC_SYNC_KEY, aSyncKey);
                    wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                    
                    if(![aSyncKey isEqualToString:kInitialSyncKey]) {
                        wbxml.keyValue(SYNC_DELETES_AS_MOVES, @"1");
                        wbxml.keyValue(SYNC_GET_CHANGES, @"1");
                        wbxml.keyValue(SYNC_WINDOW_SIZE, [NSString stringWithFormat:@"%d", sWindowSize]);
                        //wbxml.keyValue(SYNC_CONVERSATION_MODE, @"1");
                        
                        switch([aFolder folderType]) {
                            case kFolderTypeContacts:
							case kFolderTypeUserContacts:
                            case kFolderTypeRecipientInfo:
								break;
                            case kFolderTypeCalendar:
                            case kFolderTypeUserCalendar:
                                [self calendarOptions:wbxml];
                                break;
                            case kFolderTypeTasks:
                               break;
                            default:
                                [self emailOptions:wbxml fetchBody:fetchBody];
                                break;
                        }
                    }else{
                        // Initial sync
                        //
                        //FXDebugLog(kFXLogActiveSync, @"Initial Sync: %@", [aFolder displayName]);
                        switch([aFolder folderType]) {
                            case kFolderTypeContacts:
							case kFolderTypeUserContacts:
                                [self contactsSupported:wbxml];
                               break;
                            case kFolderTypeRecipientInfo:
//                                [self contactsSupported:wbxml];
                               break;
                            case kFolderTypeCalendar:
                                //[self calendarSupported:wbxml];
                                break;
                            default:
                                break;
                        }
                        // May need to increase connection timeout for this, it might take a while
                    }
                }wbxml.end();
            }
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (void)emailOptions:(WBXMLDataGenerator&)wbxml fetchBody:(BOOL)fetchBody
{
    wbxml.start(SYNC_OPTIONS); {
        // Not clear you really need this unless you are doing options on different classes, like SMS
        //wbxml.keyValue(SYNC_CLASS, @"Email");
        
        // Time Filter
        ASAccount* anAccount = self.account;
        wbxml.keyValueInt(SYNC_FILTER_TYPE, [anAccount emailFilterType]);
        
        // MIME, HTML, RTF, or PlainText
        if([anAccount ASVersion] >= kASVersion2007) {
            wbxml.start(BASE_BODY_PREFERENCE); {
                switch(self.bodyFetchType) {
                    case kBodyFetchPreview:
                        // NOTE: estimated size will be for plain text, not MIME, etc.
                        wbxml.keyValueInt(BASE_TYPE, kBodyTypePlainText);
                        wbxml.keyValue(BASE_TRUNCATION_SIZE, kPreviewTruncationSize);
                        break;
                    case kBodyFetchFull:
                        // Deprecated, use ASItemOperation unless you know the body is short
                        wbxml.keyValueInt(BASE_TYPE, [anAccount bodyType]);
                        wbxml.keyValue(BASE_TRUNCATION_SIZE, kMaxTruncationSize);
                        break;
                    case kBodyFetchNone:
                        // NOTE: body type is required even if you are truncating, the estimated size in
                        // the response is based on this
                        wbxml.keyValueInt(BASE_TYPE, [anAccount bodyType]);
                        wbxml.keyValue(BASE_TRUNCATION_SIZE, kTruncate);
                        break;
                }
            }wbxml.end();
        }else{
            if(fetchBody) {
                wbxml.keyValueInt(SYNC_MIME_TRUNCATION, kMIMETruncationNo);
            }else{
                wbxml.keyValueInt(SYNC_MIME_TRUNCATION, kMIMETruncationAll);
            }
        }
        
        if([anAccount bodyType] == kBodyTypeMIME && fetchBody) {
            wbxml.keyValueInt(SYNC_MIME_SUPPORT, kMIMESupportForAll);
		}
    }wbxml.end();
}

- (void)calendarOptions:(WBXMLDataGenerator&)wbxml
{    
    wbxml.start(SYNC_OPTIONS); {
        // Not clear you really need this unless you are doing options on different classes, like SMS
        //wbxml.keyValue(SYNC_CLASS, @"Email");
        
        // Time Filter
        ASAccount* anAccount = self.account;
        wbxml.keyValueInt(SYNC_FILTER_TYPE, [anAccount calendarFilterType]);
        
        // MIME, HTML, RTF, or PlainText
        if([anAccount ASVersion] >= kASVersion2007) {
            wbxml.start(BASE_BODY_PREFERENCE); {
                wbxml.keyValueInt(BASE_TYPE, kBodyTypePlainText);
                //wbxml.keyValueInt(BASE_TYPE, kBodyTypeHTML);
                wbxml.keyValue(BASE_TRUNCATION_SIZE, kMaxTruncationSize);
            }wbxml.end();
        }else{
            wbxml.keyValueInt(SYNC_MIME_TRUNCATION, kMIMETruncationNo);
        }
        
        //if([anAccount bodyType] == kBodyTypeMIME) {
        //    wbxml.keyValueInt(SYNC_MIME_SUPPORT, kMIMESupportForAll);
		//}
    }wbxml.end();
}

///////////////////////////////////////////////////////////////////////////////////////////
// Delete UID
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Delete UID

+ (void)queueDeleteUid:(NSString*)aUid folder:(ASFolder*)aFolder
{
    ASSyncDelete* aSyncDelete = [[ASSyncDelete alloc] initWithFolder:aFolder uid:aUid];
    [aSyncDelete queue];
}

+ (ASSync*)sendDeleteUid:(NSString*)aUid folder:(ASFolder*)aFolder
{
    ASSync* aSync = nil;
    @try {
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders         = [NSArray arrayWithObject:aFolder];
        NSData* aWbxml = [aSync wbxmlDeleteUid:aUid folder:(ASFolder*)aFolder];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendDeleteUid" exception:e];
    }
    return aSync;
} 

- (NSData*)wbxmlDeleteUid:(NSString*)aUid folder:(ASFolder*)aFolder
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, aFolder.syncKey);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.tag(SYNC_GET_CHANGES);
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_DELETE); {
                        wbxml.keyValue(SYNC_SERVER_ID, aUid);
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML Utilities
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Utilities

- (void)contactsSupported:(WBXMLDataGenerator&)wbxml
{
    // Must be sent on initial sync of calendar folder
    // MS-ASCMD 2.2.3.164
    //
    wbxml.start(SYNC_SUPPORTED); {
//        wbxml.tag(CONTACTS_PAGE);
//        wbxml.tag(CONTACTS_ANNIVERSARY);
//        wbxml.tag(CONTACTS_ASSISTANT_NAME);
//        wbxml.tag(CONTACTS_ASSISTANT_TELEPHONE_NUMBER);
//		wbxml.tag(CONTACTS_BIRTHDAY);
//		wbxml.tag(CONTACTS_BODY);
//        wbxml.tag(CONTACTS_BODY_SIZE);
//        wbxml.tag(CONTACTS_BODY_TRUNCATED);
		wbxml.tag(CONTACTS_BUSINESS2_TELEPHONE_NUMBER);
		wbxml.tag(CONTACTS_BUSINESS_ADDRESS_CITY);
		wbxml.tag(CONTACTS_BUSINESS_ADDRESS_COUNTRY);
		wbxml.tag(CONTACTS_BUSINESS_ADDRESS_POSTAL_CODE);
		wbxml.tag(CONTACTS_BUSINESS_ADDRESS_STATE);
		wbxml.tag(CONTACTS_BUSINESS_ADDRESS_STREET);
		wbxml.tag(CONTACTS_BUSINESS_FAX_NUMBER);
		wbxml.tag(CONTACTS_BUSINESS_TELEPHONE_NUMBER);
//		wbxml.tag(CONTACTS_CAR_TELEPHONE_NUMBER);
//        wbxml.tag(CONTACTS_CATEGORIES);
//        wbxml.tag(CONTACTS_CATEGORY);
//        wbxml.tag(CONTACTS_CHILDREN);
//        wbxml.tag(CONTACTS_CHILD);
		wbxml.tag(CONTACTS_COMPANY_NAME);
//		wbxml.tag(CONTACTS_DEPARTMENT);
		wbxml.tag(CONTACTS_EMAIL1_ADDRESS);
		wbxml.tag(CONTACTS_EMAIL2_ADDRESS);
		wbxml.tag(CONTACTS_EMAIL3_ADDRESS);
//        wbxml.tag(CONTACTS_FILE_AS);
		wbxml.tag(CONTACTS_FIRST_NAME);
		wbxml.tag(CONTACTS_HOME2_TELEPHONE_NUMBER);
		wbxml.tag(CONTACTS_HOME_ADDRESS_CITY);
		wbxml.tag(CONTACTS_HOME_ADDRESS_COUNTRY);
		wbxml.tag(CONTACTS_HOME_ADDRESS_POSTAL_CODE);
		wbxml.tag(CONTACTS_HOME_ADDRESS_STATE);
		wbxml.tag(CONTACTS_HOME_ADDRESS_STREET);
		wbxml.tag(CONTACTS_HOME_FAX_NUMBER);
		wbxml.tag(CONTACTS_HOME_TELEPHONE_NUMBER);
//		wbxml.tag(CONTACTS_JOB_TITLE);
		wbxml.tag(CONTACTS_LAST_NAME);
		wbxml.tag(CONTACTS_MIDDLE_NAME);
//		wbxml.tag(CONTACTS_MOBILE_TELEPHONE_NUMBER);
//		wbxml.tag(CONTACTS_OFFICE_LOCATION);
		wbxml.tag(CONTACTS_OTHER_ADDRESS_CITY);
		wbxml.tag(CONTACTS_OTHER_ADDRESS_COUNTRY);
		wbxml.tag(CONTACTS_OTHER_ADDRESS_POSTAL_CODE);
		wbxml.tag(CONTACTS_OTHER_ADDRESS_STATE);
		wbxml.tag(CONTACTS_OTHER_ADDRESS_STREET);
//		wbxml.tag(CONTACTS_PAGER_NUMBER);
//		wbxml.tag(CONTACTS_RADIO_TELEPHONE_NUMBER);
//		wbxml.tag(CONTACTS_SPOUSE);
//        wbxml.tag(CONTACTS_SUFFIX);
//		wbxml.tag(CONTACTS_TITLE);
		wbxml.tag(CONTACTS_WEBPAGE);
//        wbxml.tag(CONTACTS_YOMI_COMPANY_NAME);
//        wbxml.tag(CONTACTS_YOMI_FIRST_NAME);
//        wbxml.tag(CONTACTS_YOMI_LAST_NAME);
//        wbxml.tag(CONTACTS_COMPRESSED_RTF);
		wbxml.tag(CONTACTS_PICTURE);
//		wbxml.tag(CONTACTS_ALIAS);
//        wbxml.tag(CONTACTS_WEIGHTED_RANK);
    }wbxml.end();
}

- (void)calendarSupported:(WBXMLDataGenerator&)wbxml
{
    // Must be sent on initial sync of calendar folder
    // MS-ASCMD 2.2.3.164
    //
    wbxml.start(SYNC_SUPPORTED); {
        // Required 
        wbxml.tag(CALENDAR_DTSTAMP);
        wbxml.tag(CALENDAR_CATEGORIES);
        wbxml.tag(CALENDAR_SENSITIVITY);
        wbxml.tag(CALENDAR_BUSY_STATUS);
        wbxml.tag(CALENDAR_UID);
        wbxml.tag(CALENDAR_TIME_ZONE);
        wbxml.tag(CALENDAR_START_TIME);
        wbxml.tag(CALENDAR_SUBJECT);
        wbxml.tag(CALENDAR_LOCATION);
        wbxml.tag(CALENDAR_END_TIME);
        wbxml.tag(CALENDAR_RECURRENCE);
        wbxml.tag(CALENDAR_ALL_DAY_EVENT);
        wbxml.tag(CALENDAR_REMINDER);
        wbxml.tag(CALENDAR_EXCEPTIONS);
        // Optional
        wbxml.tag(CALENDAR_ATTENDEES);
        wbxml.tag(CALENDAR_ORGANIZER_NAME);
        wbxml.tag(CALENDAR_ORGANIZER_EMAIL);
        wbxml.tag(CALENDAR_MEETING_STATUS);
        wbxml.tag(CALENDAR_RESPONSE_REQUESTED);
        wbxml.tag(CALENDAR_DISALLOW_NEW_TIME_PROPOSAL);
    }wbxml.end();
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser

- (void)_commandsParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:SYNC_COMMANDS]) != AS_END) {
            switch(tag) {
                case SYNC_ADD:
                    [folder addParser:aParser tag:SYNC_ADD object:self.object bodyType:bodyType];
                    if(self.eventDelegate) {
                        [self.eventDelegate addEvent:(Event*)self.object];
                        self.eventDelegate = nil;
                    }
                    if(self.contactDelegate) {
                        [self.contactDelegate addContact:(ASContact*)self.object];
                        self.contactDelegate = nil;
                    }
                    if(self.syncObjectDelegate) {
                        [self.syncObjectDelegate addObject:self.object];
                        self.syncObjectDelegate = nil;
                    }
                    break;
                case SYNC_DELETE:
                    [folder deleteParser:aParser tag:SYNC_DELETE];
                    if(self.eventDelegate) {
                        FXDebugLog(kFXLogActiveSync, @"event delete");
                    }
                    if(self.contactDelegate) {
                        FXDebugLog(kFXLogActiveSync, @"contact delete");
                    }
                    if(self.syncObjectDelegate) {
                        FXDebugLog(kFXLogActiveSync, @"sync object delete");
                    }
                    break;
                case SYNC_SOFT_DELETE:
                    [folder deleteParser:aParser tag:SYNC_SOFT_DELETE];
                    break;
                case SYNC_CHANGE:
                    [folder changeParser:aParser tag:SYNC_CHANGE];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Sync Responses" exception:e];
    }
   
#warning FIXME MUST NOT store sync key until database store is complete or emails will be lost
    // This will store objects to the database and send to
    // UI clients
    //
    [folder commitObjects];
}

- (void)_responsesParser:(FastWBXMLParser*)aParser
{
    BOOL needsCommit = FALSE;
    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:SYNC_RESPONSES]) != AS_END) {
            switch(tag) {
#if 0
                case SYNC_FETCH:
                {
                    [folder addParser:aParser tag:SYNC_FETCH object:self.object bodyType:bodyType];
                    if(self.object) {
                        // Send to view controller client, typically the Mail viewer
                        //
                        NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:messageDelegate selector:@selector(fetchedMessage:) object:self.object];
                        [[NSOperationQueue mainQueue] addOperation:op];
                    }
                    break;
                }
#endif
                case SYNC_ADD:
                {
                    needsCommit = TRUE;
                    [folder addParser:aParser tag:tag object:self.object bodyType:bodyType];
                    if(self.eventDelegate) {
                        [eventDelegate addEvent:(Event*)self.object];
                        self.eventDelegate = nil;
                    }
                    if(self.contactDelegate) {
                        [contactDelegate addContact:(ASContact*)self.object];
                        self.contactDelegate = nil;
                    }
                    if(self.syncObjectDelegate) {
                        [syncObjectDelegate addObject:self.object];
                        self.syncObjectDelegate = nil;
                    }
                    break;
                }
                case SYNC_CHANGE:
                {
                    needsCommit = TRUE;
                    [folder changeParser:aParser tag:tag];
                    if(self.eventDelegate) {
                        Event* anEvent = (Event*)self.object; 
                        [anEvent.folder changedObject:anEvent];
                        self.eventDelegate = nil;
                    }
                    if(self.contactDelegate) {
                        ASContact* anContact = (ASContact*)self.object; 
                        [anContact.folder changedObject:anContact];
                        self.contactDelegate = nil;
                    }
                    if(self.syncObjectDelegate) {
                        [folder changedObject:self.object];
                        self.syncObjectDelegate = nil;
                    }
                    break;
                }
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Sync Responses" exception:e];
    }
    
    // This will store objects to the database and send to
    // UI clients
    //
    if(needsCommit) {
        [folder commitObjects];
    }
}

- (BOOL)_collectionParser:(FastWBXMLParser*)aParser
{
    BOOL isUpdating = TRUE;
    
    NSString* aSyncKey = NULL;
    folder = NULL;

    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:SYNC_COLLECTION]) != AS_END) {
            switch(tag) {
                case SYNC_SYNC_KEY:
                {
                    // We have to cache this until the folder collectionID arrives, they should
                    // have sent collectionID first
                    //
                    aSyncKey = [aParser getStringTraceable];
                    break;
                }
                case SYNC_COLLECTION_ID:
                {
                    NSString* aCollectionID = [aParser getStringTraceable];
                    folder = (ASFolder*)[self.account folderForServerId:aCollectionID];
                    if(folder) {
                        // Clear more available and it will be set TRUE below if there is more
                        [folder setIsMoreAvailable:FALSE];
                    }else{
                        FXDebugLog(kFXLogActiveSync, @"ASSync SYNC_COLLECTION_ID invalid: %@", aCollectionID); 
                    }
                    break;
                }
                case SYNC_CLASS:
                {
                    NSString* aClass = [aParser getStringTraceable]; 
                    if([aClass isEqualToString:@"Email"]) {
                    }else{
                        FXDebugLog(kFXLogActiveSync, @"ASSync FIXME unhandled class type: %@", aClass);
                    }
                    break;
                }
                case SYNC_COMMANDS:
                    [self _commandsParser:aParser]; 
                    break;
                case SYNC_RESPONSES:
                    [self _responsesParser:aParser]; 
                    break;
                case SYNC_MORE_AVAILABLE:
                    [folder setIsMoreAvailable:TRUE];
                    [self.moreAvailableFolders addObject:folder];
                    break;
                case SYNC_STATUS:
                {
                    ESyncStatus aSyncStatus = (ESyncStatus)[aParser getIntTraceable];
                    self.statusCodeAS = aSyncStatus;
                    switch(aSyncStatus) {
                        case kSyncStatusOK:
                            break;
                        case kSyncStatusInvalidSyncKey:
                            aSyncKey = nil;
                            [folder resyncFolder];
                            folder = nil;
                            isUpdating = FALSE;
                            break;
                        case kSyncStatusFolderHierarchyChanged:
                            [self.account folderSync];
#warning FIXME need to retry the sync when folder sync finished
                            break;
                        default:
                            [ASFolder displaySyncStatus:aSyncStatus];
                            break;
                    }
                    break;
                }
                case SYNC_PARTIAL:
                    // the current Sync request does not include all of the settings for all the collections to be synchronized, 
                    // and that the server is to use the values from the previous Sync request
                    FXDebugLog(kFXLogFIXME, @"FIXME SYNC_PARTIAL");
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Sync Collection" exception:e];
    }
    
    if(folder) {
        [folder setIsSyncing:FALSE];
        
        if([aSyncKey length] > 0) {
            if(![aSyncKey isEqualToString:folder.syncKey]) {
                [folder setSyncKey:aSyncKey syncDate:[NSDate date]];
#warning FIXME optimize sync key commit
                [folder commit];
            }else{
				// for contacts, the syncKey doesn't change so we have
				// to watch this
				if ((folder.folderType == kFolderTypeContacts) ||
					(folder.folderType == kFolderTypeUserContacts) ||
					(folder.folderType == kFolderTypeRecipientInfo)) {

					[folder setSyncKey:aSyncKey syncDate:[NSDate date]];
					[folder commit];
				} else {
					[syncDelegate syncNotUpdating:syncFolders];
					isUpdating = FALSE;
				}
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"ASSync sync key invalid: %@", [folder displayName]); 
        }
    }
    return isUpdating;
}

- (void)fastParser:(FastWBXMLParser*)aParser
{  
    ESyncStatus aSyncStatus = kSyncStatusOK;
    BOOL isUpdating = TRUE;

    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case SYNC_COLLECTIONS:
                    break;
                case SYNC_COLLECTION:
                    isUpdating = [self _collectionParser:aParser];
                    break;
                case SYNC_STATUS:
                    aSyncStatus = (ESyncStatus)[aParser getInt];
                    [ASFolder displaySyncStatus:aSyncStatus];
                    break;
                default:
                    [aParser skipTag]; 
                    break;
            }
        }
        
    }@catch (NSException* e) {
        [HttpRequest logException:@"Sync response" exception:e];
    }

    if(aSyncStatus == kSyncStatusOK) {
        if(isChangeRequest) {
            //FXDebugLog(kFXLogActiveSync, @"do nothing on change request");
        }else if(self.moreAvailableFolders.count > 0 && isUpdating) {
            [syncDelegate syncMoreAvailable:self.moreAvailableFolders pingFolders:self.pingFolders];
        }else if(self.pingFolders.count > 0) {
            [syncDelegate syncSuccess:syncFolders isPinging:TRUE];
            [ASPing sendPing:self.account folders:self.pingFolders];
        }else if(syncDelegate) {
            if(isUpdating) {
                [syncDelegate syncSuccess:syncFolders isPinging:FALSE];
            }
        }
        
        if(self.isChangeRequest) {
            //FXDebugLog(kFXLogActiveSync, @"Change request complete");
            if(self.eventDelegate) {
                [self.eventDelegate changedEvent:(Event*)self.object];
            }
            if(self.contactDelegate) {
                [self.contactDelegate changedContact:(ASContact*)self.object];
            }
            if(self.syncObjectDelegate) {
                [self.syncObjectDelegate changedObject:self.object];
            }
        }
    }else {
        if(syncDelegate) {
            [syncDelegate syncFailed:syncFolders httpStatus:self.statusCode syncStatus:aSyncStatus];
        }
        if(eventDelegate) {
            [eventDelegate addEventFailed:(Event*)self.object httpStatus:self.statusCode syncStatus:aSyncStatus];
        }
        if(contactDelegate) {
			if (self.isChangeRequest) {
				[contactDelegate changeContactFailed:(ASContact*)self.object httpStatus:self.statusCode syncStatus:aSyncStatus];
			} else {
				[contactDelegate addContactFailed:(ASContact*)self.object httpStatus:self.statusCode syncStatus:aSyncStatus];
			}
        }
        if(syncObjectDelegate) {
            [syncObjectDelegate addObjectFailed:self.object httpStatus:self.statusCode syncStatus:aSyncStatus];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode         = (EHttpStatusCode)[resp statusCode];
    NSDictionary* aHeaders  = [resp allHeaderFields];
    self.contentType        = [aHeaders objectForKey:@"Content-Type"];

    if(self.statusCode == kHttpOK) {
        if([self.contentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
        }else if([self.account ASVersion] >= kASVersion2007SP1) {
            // Version 12.1 will return empty responses
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, self.statusCode, aHeaders);
            FXDebugLog(kFXLogActiveSync, @"%@ Response is not WBXML: %@", kCommand, self.contentType);
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
	switch(self.statusCode) {
        case kHttpOK:
        {
            NSUInteger aLength = [data length];
            if(aLength > 0) {
                if([self.contentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
                    [self parseWBXMLData:data command:kCommandTag];
                }else{
                    FXDebugLog(kFXLogActiveSync, @"sync response content type invalid: %@", self.contentType);
                }
            }else if([self.account ASVersion] >= kASVersion2007SP1) {
                FXDebugLog(kFXLogActiveSync, @"Empty response to sync");
                
                // Version 12.1 will return empty responses implying you are an idiot for
                // asking for sync when there are no changes :)
                for(ASFolder* aFolder in syncFolders) {
                    [aFolder setIsSyncing:FALSE];
                }
                [syncDelegate syncSuccess:syncFolders isPinging:(self.pingFolders.count > 0)];
                if(self.pingFolders.count > 0) {
                    [ASPing sendPing:self.account folders:self.pingFolders];
                }
            }

            break;
        }
        case kHttpServiceUnavailable:
            // This will retry up to a point
            return;
        default:
            FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
#ifdef DEBUG
            if([data length] > 0) {
                NSString* aString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                FXDebugLog(kFXLogActiveSync, @"withData:\n%@", aString);
            }
#endif
            if(syncDelegate) {
                [syncDelegate syncFailed:syncFolders httpStatus:self.statusCode syncStatus:kSyncStatusUnknown];
            }
            break;
    }
    [super connectionDidFinishLoading:connection];
}
@end