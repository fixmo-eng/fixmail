/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASAccount.h"

@interface ASAccountExchange : ASAccount {
    
}

// Construct/Destruct
- (id)initWithAccountNum:(int)anAccountNum;
- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUsername
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(EEncryptionMethod)anEncryption
    authentication:(EAuthenticationMethod)anAuthentication
       folderNames:(NSArray *)aFolderNames;

// Overrides
- (NSArray*)foldersToDisplay;
- (NSArray*)foldersToSync;
- (BOOL)isFilterTypeSupported:(EFilterType)aFilterType folderType:(EFolderType)aFolderType;

@end
