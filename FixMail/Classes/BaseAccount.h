/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"
#import "AccountDelegate.h"
#import "ASConfigViewDelegate.h"
#import "ASProvisionPolicy.h"
#import "BaseFolder.h"
#import "TraceDelegate.h"

@class BaseContact;
@class Event;
@class ASEmail;
@class Reachability;
@class WBXMLRequest;

typedef enum {
	AccountTypeImap         = 1,	
    AccountTypeActiveSync   = 2
} email_account_type_enum;

typedef enum {
	AccountSubTypeGmail     = 1,	
    AccountSubTypeExchange  = 2,
    AccountSubTypeDomino    = 3
} email_account_sub_type_enum;

@interface BaseAccount : BaseObject {
    double                      ASVersion;
    NSMutableArray*             folders;
    NSMutableDictionary*        folderDictionary;    
    NSMutableArray*             accountDelegates;
    BOOL                        observingReachability;
}

// Properties
@property (strong) NSMutableArray*  folders;

// Persistent Store Properties
@property email_account_type_enum       accountType;
@property email_account_sub_type_enum   accountSubType;

@property int                       accountNum;
@property (strong) NSString*        name;
@property (strong) NSString*        pcc;
@property (strong) NSString*        userName;
@property (strong) NSString*        emailAddress;
@property (strong) NSString*        password;
@property (strong) NSString*        hostName;
@property (strong) NSString*        path;
@property (strong) NSString*        domain;

@property int                       port;
@property int                       encryption;
@property int                       authentication;
@property BOOL                      deleted;
@property (nonatomic) BOOL          isCreated;

@property BOOL                      isReachable;
@property (strong) Reachability*    reachability;

@property (nonatomic, weak)   NSObject<ASConfigViewDelegate>* delegate;
@property (nonatomic, weak)   NSObject<TraceDelegate>*      traceDelegate;

@property (nonatomic, strong)   NSString*                   deviceID;
@property (nonatomic, strong)   NSString*                   deviceModel;

@property (strong)              ASProvisionPolicy*          provisionPolicy;
@property (strong)              NSString*                   policyKey;
@property (strong)              NSString*                   syncKey;
@property (strong)              WBXMLRequest*               requestToResend;

@property (nonatomic,strong)    NSString*                   protocolVersion;
@property (nonatomic,readonly)  double                      ASVersion;
@property (nonatomic,strong)    NSArray*                    versionSupport;
@property (nonatomic,strong)    NSArray*                    commands;

@property (nonatomic, strong)   NSString*                   userAgent;

@property (nonatomic)           int                         heartBeatIntervalSeconds;

@property (strong)              NSMutableArray*             initialSyncFolders;

// Factory

+ (BaseAccount*)currentLoggedInAccount;
+ (NSUInteger)numAccounts;
+ (NSArray*)accounts;
+ (BaseAccount*)accountForAccountNumber:(int)anAccountNumber;
+ (BaseFolder*)folderForAccountNum:(int)anAccountNum folderNum:(int)aFolderNum;
+ (BaseFolder*)folderForCombinedFolderNum:(int)anAccountNum;
+ (void)deleteAllAccounts;

// Construct/Destruct
- (id)initWithAccountNum:(int)anAccountNum;
- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUserName
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(int)anEncryption
        authentication:(int)anAuthentication
       folderNames:(NSArray *)aFolderNames;

// Error Handling
- (void)handleError:(NSString*)anErrorMessage;
- (void)logException:(NSString*)where exception:(NSException*)e;

// Load/Store
- (void)createAccount:(NSSet*)aFoldersSelected folderPaths:(NSArray*)aFolderPaths firstSetup:(BOOL)aFirstSetup;
- (void)create;

- (void)loadFolders:(NSDictionary*)aFolderDictionary;
- (void)removeAllFolders;

- (void)loadSettings;
- (void)commitSettings;

- (void)fromDictionary:(NSDictionary*)aDictionary;
- (void)asDictionary:(NSMutableDictionary*)aDictionary;

// BaseObject Overrides
- (void)fromJSON:(NSString*)aString;
- (NSDictionary*)asDictionary; 

// Delegate
- (void)addDelegate:(NSObject<AccountDelegate>*)anAccountDelegate;
- (void)removeDelegate:(NSObject<AccountDelegate>*)anAccountDelegate;

// Setters
- (void)storeEmailAddress:(NSString *)anEmailAddress;

// Getters
- (BaseFolder*)folderForNum:(int)aNum;
- (BaseFolder*)folderForServerId:(NSString*)aServerID;
- (BaseFolder*)folderForName:(NSString*)aName;
- (NSMutableArray*)foldersForFolderType:(EFolderType)aFolderType;
- (NSArray*)folderNames;
- (void)folderHierarchyFromFolders:(NSArray*)aFromFolders toFolders:(NSMutableArray*)aToFolders;

// Overrides
- (BaseContact*)createContact;
- (Event*)createEvent;
- (BaseFolder*)createFolderForFolderType:(EFolderType)aFolderType;
- (ASEmail*)createEmailForFolder:(BaseFolder*)aFolder;

- (UIImage*)icon;

- (void)setupFolderForName:(NSString*)aFolderPath;
- (void)setupFolder:(BaseFolder*)aFolder;
- (void)addFolder:(BaseFolder*)aFolder;
- (void)deleteAllFolders;
- (NSArray*)foldersToDisplay;
- (BOOL)emailCanBeInMultipleFolders;

- (void)creationComplete;

- (void)sendEmail:(ASEmail*)anEmail;

// Database
+ (void)tableCheck;
+ (void)clearPreparedStatements;
+ (BaseAccount*)createAccountFromSQL:(int)anAccountNum;
+ (int)accountsConfigured;

- (void)commitSyncKey;
- (void)commitPolicyKey;
- (void)commitEmailAddress;

// Debug
- (void)description:(NSMutableString*)aString;

@end
