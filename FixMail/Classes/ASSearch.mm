/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASSearch.h"
#import "ASAccount.h"
#import "ASEmail.h"
#import "ASFolder.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASSearch

@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"Search";
static const ASTag kCommandTag  = SEARCH_SEARCH;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

// Email search
//
+ (ASSearch*)sendSearchMailbox:(ASFolder*)aFolder
                         query:(NSString*)aQuery
                         range:(NSRange)aRange
                 displayErrors:(BOOL)aDisplayErrors
{
    ASAccount* anAccount = (ASAccount*)[aFolder account];
    ASSearch* aSearch = [[ASSearch alloc] initWithAccount:anAccount];
    aSearch.cmd     = kSearchCmdMailbox;
    aSearch.folder  = aFolder;
    aSearch.query   = aQuery;
    aSearch.range   = aRange;
    aSearch.displayErrors = aDisplayErrors;
    [aSearch send];
    
    return aSearch;
}

+ (ASSearch*)queueSearchMailbox:(ASFolder*)aFolder
                          query:(NSString*)aQuery
                          range:(NSRange)aRange
                  displayErrors:(BOOL)aDisplayErrors
{
    ASAccount* anAccount = (ASAccount*)[aFolder account];
    ASSearch* aSearch = [[ASSearch alloc] initWithAccount:anAccount];
    aSearch.cmd     = kSearchCmdMailbox;
    aSearch.folder  = aFolder;
    aSearch.query   = aQuery;
    aSearch.range   = aRange;
    aSearch.displayErrors = aDisplayErrors;
    [aSearch queue];
    
    return aSearch;
}

- (void)send
{
    NSData* aWbxml = nil;
    switch(self.cmd) {
        case kSearchCmdMailbox:
            aWbxml = [self wbxmlSearchMailbox:self.folder query:self.query];
            break;
    }
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator
    
- (NSData*)wbxmlSearchMailbox:(ASFolder*)aFolder query:(NSString*)aQuery
{
    WBXMLDataGenerator wbxml;
    
    wbxml.start(kCommandTag); {
        wbxml.start(SEARCH_STORE); {
            wbxml.keyValue(SEARCH_NAME, @"Mailbox");
            wbxml.start(SEARCH_QUERY); {
                wbxml.start(SEARCH_AND); {
                    wbxml.keyValue(SEARCH_FREE_TEXT, aQuery);
                    wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                }wbxml.end();
            }wbxml.end();
            wbxml.start(SEARCH_OPTIONS); {
                NSUInteger aLocation = self.range.location;
                wbxml.keyValue(SEARCH_RANGE, [NSString stringWithFormat:@"%d-%d", aLocation, aLocation+self.range.length]);
                wbxml.tag(SEARCH_REBUILD_RESULTS);
                //wbxml.tag(SEARCH_DEEP_TRAVERSAL);
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
#if 0
    <Search xmlns="Search" xmlns:airsync="AirSync">
    <Store>
    <Name>Mailbox</Name>
    <Query>
    <And>
    <airsync:CollectionId>7</airsync:CollectionId>
    <FreeText>Presentation</FreeText>
    </And>
    </Query>
    <Options>
    <RebuildResults />
    <Range>0-4</Range>
    <DeepTraversal/>
    </Options>
    </Store>
    </Search>
#endif
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kSearchSuccess:
            break;
#ifdef DEBUG
        case kSearchRequstInvalid:
            [HttpRequest handleASError:@"Search request invalid"]; break;
        case kSearchServerError:
            [HttpRequest handleASError:@"Search server error"]; break;
        case kSearchBadLink:
            [HttpRequest handleASError:@"Search bad link"]; break;
        case kSearchAccessDenied:
            [HttpRequest handleASError:@"Search access denied"]; break;
        case kSearchNotFound:
            [HttpRequest handleASError:@"Search not found"]; break;
        case kSearchConnectionFailed:
            [HttpRequest handleASError:@"Search connection failed"]; break;
        case kSearchTooComplex:
            [HttpRequest handleASError:@"Search too complex"]; break;
        case kSearchTimedOut:
            [HttpRequest handleASError:@"Search timed out"]; break;
        case kSearchFolderSyncRequired:
            [HttpRequest handleASError:@"Search folder sync required"]; break;
        case kSearchEndOfRetrivableRange:
            [HttpRequest handleASError:@"Search end of retrievable range"]; break;
        case kSearchAccessBlocked:
            [HttpRequest handleASError:@"Search access blocked"]; break;
        case kSearchCredentialsRequired:
            [HttpRequest handleASError:@"Search credentials required"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"SEARCH_ERROR", @"ErrorAlert", @"Error alert message for Search error"), self.statusCodeAS]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)_propertiesParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:SEARCH_PROPERTIES]) != AS_END) {
        switch(tag) {
            case GAL_DISPLAY_NAME:
                FXDebugLog(kFXLogActiveSync, @"GAL_DISPLAY_NAME = %@", [aParser getString]);
                break;
            case GAL_ALIAS:
                FXDebugLog(kFXLogActiveSync, @"GAL_ALIAS = %@", [aParser getString]);
                break;
            case GAL_FIRST_NAME:
                FXDebugLog(kFXLogActiveSync, @"GAL_FIRST_NAME = %@", [aParser getString]);
                break;
            case GAL_LAST_NAME:
                FXDebugLog(kFXLogActiveSync, @"GAL_LAST_NAME = %@", [aParser getString]);
                break;
            case GAL_EMAIL_ADDRESS:
                FXDebugLog(kFXLogActiveSync, @"GAL_EMAIL_ADDRESS = %@", [aParser getString]);
                break;
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    BOOL isEmail = FALSE;
    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case SEARCH_STATUS:
                    self.statusCodeAS = (ESearchStatus)[aParser getInt];
                    if(self.statusCodeAS == kSearchSuccess) {
                        self.results = [[NSMutableArray alloc] initWithCapacity:100];
                    }
                    break;
                case SEARCH_RESPONSE:
                    break;
                case SEARCH_STORE:
                    break;
                case SEARCH_RESULT:
                    break;
                case SEARCH_PROPERTIES:
                {
                    if(isEmail && self.folder) {
                        ASEmail* anEmail = [[ASEmail alloc] initWithFolder:self.folder];
                        [anEmail parser:aParser tag:tag];
                        //FXDebugLog(kFXLogActiveSync, @"search result email: %@", anEmail);
                        [self.results addObject:anEmail];
                    }else{
                        [self _propertiesParser:aParser];
                    }
                    break;
                }
                case SEARCH_RANGE:
                    //FXDebugLog(kFXLogActiveSync, @"SEARCH_RANGE = %@", [aParser getString]);
                    break;
                case SEARCH_TOTAL:
                    self.total = [aParser getUnsignedInt];
                    break;
                case SEARCH_LONG_ID:
                    //FXDebugLog(kFXLogActiveSync, @"SEARCH_LONG_ID = %@", [aParser getString]);
                    break;
                case SYNC_CLASS:
                {
                    NSString* aClass = [aParser getString];
                    if([aClass isEqualToString:@"Email"]) {
                        isEmail = TRUE;
                    }else{
                        isEmail = FALSE;
                        FXDebugLog(kFXLogActiveSync, @"SYNC_CLASS = %@", aClass);
                    }
                    break;
                }
                case SYNC_COLLECTION_ID:
                    //FXDebugLog(kFXLogActiveSync, @"SYNC_COLLECTION_ID = %@", [aParser getString]);
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        if(self.statusCodeAS == kSearchSuccess) {
            [self success];
        }else{
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Search response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end