/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"
#import "ASItemOperationsDelegate.h"

@class ASEmail;
@class ASFolder;
@class SZLSecureFileHandle;

typedef enum {
    kItemOperationsSuccess                      = 1,
    kItemOperationsProtocolError                = 2,
    kItemOperationsServerError                  = 3,
    kItemOperationsDocumentURIBad               = 4,
    kItemOperationsDocumentAccessDenied         = 5,
    kItemOperationsDocumentNotFound             = 6,
    kItemOperationsDocumentFailedServerConnect  = 7,
    kItemOperationsByteRangeInvalid             = 8,
    kItemOperationsStoreUnknown                 = 9,
    kItemOperationsFileEmpty                    = 10,
    kItemOperationsRequestedSizeTooLarge        = 11,
    kItemOperationsInputOutputFailure           = 12,
    kItemOperationsItemFailedConversion         = 14,
    kItemOperationsAttachmentInvalid            = 15,
    kItemOperationsAccessDenied                 = 16,
    kItemOperationsPartialSuccess               = 17,
    kItemOperationsCredentialsRequired          = 18,
    kItemOperationsProtocolErrorOptionsMissing  = 155,
    kItemOperationsFolderMustBeIPFNote          = 156
} EItemOperationsStatus;

typedef enum {
    kItemOperationsCmdAttachment            = 1,
    kItemOperationsCmdAttachmentWithRange   = 2,
    kItemOperationsCmdFetchEmailBody        = 3,
    kItemOperationsCmdEmptyFolder           = 4
} EitemOperationsCmd;

@interface ASItemOperations : WBXMLRequest {
    NSMutableArray*                     partRanges;
    NSRange                             wbxmlRange;
    NSRange                             partRange;
}

@property (nonatomic,strong) NSObject<ASItemOperationsDelegate>*    delegate;

@property (nonatomic) EitemOperationsCmd                            cmd;
@property (nonatomic,strong) id                                     target;
@property (nonatomic) SEL                                           selector;
@property (nonatomic) EItemOperationsStatus                         statusCodeAS;
@property (nonatomic) BOOL                                          isBodyFetch;
@property (nonatomic) BOOL                                          isMultipart;
@property (nonatomic) BOOL                                          isOctetStream;

@property (nonatomic,strong) NSString*                              fileReference;
@property (nonatomic,strong) ASEmail*                               email;
@property (nonatomic,strong) ASFolder*                              folder;
@property (nonatomic,strong) NSString*                              filePath;
@property (nonatomic,strong) NSFileHandle*                          fileHandle;
@property (nonatomic,strong) NSMutableData*                         fileData;
@property (nonatomic)        NSUInteger                             percentReported;
@property (nonatomic)        NSUInteger                             expectedSize;
@property (nonatomic)        NSUInteger                             bytesDownloaded;
@property (nonatomic)        NSRange                                range;
@property (nonatomic)        BOOL									smimeMessage;

// Factory
+ (ASItemOperations*)sendRequest:(ASAccount*)anAccount
                   fileReference:(NSString*)aFileReference
                        filePath:(NSString*)aFilePath
                           range:(NSRange)aRange
                        delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;
+ (ASItemOperations*)queueRequest:(ASAccount*)anAccount
                    fileReference:(NSString*)aFileReference
                         filePath:(NSString*)aFilePath
                            range:(NSRange)aRange
                         delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;

+ (ASItemOperations*)sendRequest:(ASAccount*)anAccount
                   fileReference:(NSString*)aFileReference 
                        filePath:(NSString*)aFilePath       
                    expectedSize:(NSUInteger)anExpectedSize
                        delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;
+ (ASItemOperations*)queueRequest:(ASAccount*)anAccount
                    fileReference:(NSString*)aFileReference
                         filePath:(NSString*)aFilePath
                     expectedSize:(NSUInteger)anExpectedSize
                         delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;

+ (ASItemOperations*)sendFetchEmailBody:(ASEmail*)anEmail
								  smime:(BOOL)anSMIMEMessage
                               delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;
+ (ASItemOperations*)queueFetchEmailBody:(ASEmail*)anEmail
                                   smime:(BOOL)anSMIMEMessage
                                delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;

+ (ASItemOperations*)sendEmptyFolderContents:(ASFolder*)aFolder
                                    delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;
+ (ASItemOperations*)queueEmptyFolderContents:(ASFolder*)aFolder
                                     delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end