/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASPing.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASSync.h"
#import "ASSyncFolders.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASPing

// Properties
@synthesize folders;
@synthesize changedFolders;
@synthesize statusCodeAS;
@synthesize emptyPing;

// Constants
static NSString* kCommand       = @"Ping";
static const ASTag kCommandTag  = PING_PING;

static const int kSecondsPerMinute              = 60;
static const int kMinHeartBeatIntervalSeconds   = 240;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSMutableArray* sPings = NULL;

+ (int)heartBeatIntervalSeconds:(ASAccount*)anAccount
{
    int aHearbeatIntervalSeconds = [anAccount heartBeatIntervalSeconds];
    if(aHearbeatIntervalSeconds < kMinHeartBeatIntervalSeconds) {
        FXDebugLog(kFXLogActiveSync, @"heartBeatIntervalSeconds %d too small, setting to %d seconds", aHearbeatIntervalSeconds, kMinHeartBeatIntervalSeconds);
        aHearbeatIntervalSeconds = kMinHeartBeatIntervalSeconds;
    }
    return aHearbeatIntervalSeconds;
}

+ (void)sendPing:(ASAccount*)anAccount folders:(NSArray*)aFolders
{
    int aHeartbeatIntervalSeconds = [ASPing heartBeatIntervalSeconds:anAccount];
    
    ASPing* anASPing = [[ASPing alloc] initWithAccount:anAccount folders:aFolders];
    NSData* aWbxml = [anASPing wbxmlPingWithHeartbeatIntervalSeconds:aHeartbeatIntervalSeconds];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    
    // Set the network connection to timeout to 1 minute more than the heartbeat interval, we really dont want it to time out
    [aURLRequest setTimeoutInterval:aHeartbeatIntervalSeconds+kSecondsPerMinute];
    [anAccount send:aURLRequest httpRequest:anASPing];
}

+ (void)sendEmptyPing:(ASAccount*)anAccount folders:(NSArray*)aFolders
{
    int aHeartbeatIntervalSeconds = [ASPing heartBeatIntervalSeconds:anAccount];

    ASPing* anASPing = [[ASPing alloc] initWithAccount:anAccount folders:aFolders];   
    anASPing.emptyPing = TRUE;
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:nil];
    
    // Set the network connection to timeout to 1 minute more than the heartbeat interval, we really dont want it to time out
    [aURLRequest setTimeoutInterval:aHeartbeatIntervalSeconds+kSecondsPerMinute];
    [anAccount send:aURLRequest httpRequest:anASPing];
}

+ (BOOL)accountHasPing:(ASAccount*)anAccount
{
    BOOL aHasPing = FALSE;
    
    for(ASPing* aPing in sPings) {
        if(aPing.account == anAccount) {
            aHasPing = TRUE;
            break;
        }
    }
    
    return aHasPing;
}
 
+ (void)removePingsForAccount:(ASAccount*)anAccount
{
    NSUInteger aCount = [sPings count];
    for(int i = aCount-1 ; i >= 0 ; --i) {
        ASPing* aPing = [sPings objectAtIndex:i];
        if([aPing account] == anAccount) {
            [aPing cancel];
            [sPings removeObject:aPing];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlPingWithHeartbeatIntervalSeconds:(int)aHeartbeatIntervalSeconds
{
    NSData* aData = NULL;
    
    if(self.account && [folders count] > 0) {
        WBXMLDataGenerator wbxml;
        
        wbxml.start(kCommandTag); {
            wbxml.keyValue(PING_HEARTBEAT_INTERVAL, [NSString stringWithFormat:@"%d", aHeartbeatIntervalSeconds]);
            wbxml.start(PING_FOLDERS); {
                NSUInteger aCount = [folders count];
                for(NSUInteger i = 0 ; i < aCount ; ++i) {
                    wbxml.start(PING_FOLDER); {
                        ASFolder* aFolder = [folders objectAtIndex:i];
                        wbxml.keyValue(PING_ID,     [aFolder uid]);
                        wbxml.keyValue(PING_CLASS,  [aFolder ASClass]);
                    }wbxml.end();
                }
            }wbxml.end();
        }wbxml.end();

        aData = wbxml.encodedData(self, kCommandTag); 
    }else{
        FXDebugLog(kFXLogActiveSync, @"Ping invalid");
    }
    
    return aData;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount folders:(NSArray*)aFolders
{
    if(!sPings) {
        sPings = [[NSMutableArray alloc] initWithCapacity:2];
    }
    if (self = [super initWithAccount:anAccount]) {
        [sPings addObject:self];
        self.folders        = aFolders;
        self.changedFolders = [NSMutableArray arrayWithCapacity:1];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setPingStatus:(EPingStatus)aStatus
{
    self.statusCodeAS = aStatus;
    
    switch(aStatus) {
        // Normal occurrences
        //
        case kPingNoStatus:
            FXDebugLog(kFXLogActiveSync, @"Ping no status"); break;
        case kPingNoChanges:
            FXDebugLog(kFXLogActiveSync, @"Ping no changes"); break;
        case kPingChangesOccurred:
            FXDebugLog(kFXLogActiveSync, @"Ping changes occurred"); break;
        case kPingFolderHierarchySyncRequired:
            FXDebugLog(kFXLogActiveSync, @"Ping folder hierarchy sync required"); break;
            
        // May or may not be actual errors
        //
        case kPingOmittedParameters:
            FXDebugLog(kFXLogActiveSync, @"Ping ommitted parameters"); break;
        case kPingInvalidWBXML:
            FXDebugLog(kFXLogActiveSync, @"Ping invalid WBXML"); break;
            
        // Errors follow so produce an error alert
        //
#ifdef DEBUG
        case kPingSyntaxError:
            [HttpRequest handleASError:@"Ping syntax error"]; break;
        case kPingIntervalOutOfRange:
            [HttpRequest handleASError:@"Ping interval out of range"]; break;
        case kPingTooManyFolders:
            [HttpRequest handleASError:@"Ping too many folders"]; break;
        case kPingServerError:
            [HttpRequest handleASError:@"Ping server error"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aStatus]; break;
#else
		default:
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"PING_ERROR", @"ErrorAlert", @"Error alert message for Ping error"), aStatus]];
            break;
#endif
			
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    EPingStatus aPingStatus = kPingNoStatus;
    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case PING_FOLDERS:
                    break;
                case PING_FOLDER:
                {
                    NSString* aFolderID = [aParser getString];
                    ASFolder* aFolder = (ASFolder*)[self.account folderForServerId:aFolderID];
                    if(aFolder){
                        //FXDebugLog(kFXLogActiveSync, @"PingFolder %@:\n%@", aFolderID, aFolder);
                        if(aPingStatus == kPingChangesOccurred) {
                            [changedFolders addObject:aFolder];
                        }
                    }else{
                        FXDebugLog(kFXLogActiveSync, @"Ping folder invalid: %@", aFolderID);
                    }
                    break;
                }
                case PING_STATUS:
                    aPingStatus = (EPingStatus)[aParser getInt];
                    [self setPingStatus:aPingStatus];
                    break;
                case PING_HEARTBEAT_INTERVAL:
                    FXDebugLog(kFXLogActiveSync, @"Ping Hearbeat interval out of range: %d", [aParser getInt]);
                    break;
                default:
                    [aParser skipTag]; break;
            }
        } 
    }@catch (NSException* e) {
        [HttpRequest logException:@"Ping response" exception:e];
    }
    
    @try {
		if(changedFolders.count > 0) {
			ASSyncFolders* aSyncFolders = [[ASSyncFolders alloc] initWithAccount:self.account
                                                                     syncFolders:changedFolders
                                                                     pingFolders:folders
                                                                   bodyFetchType:kBodyFetchPreview];
			[aSyncFolders queue];
		}else{
			switch(aPingStatus) {
                // Not error status
				case kPingNoChanges:
					[ASPing sendEmptyPing:self.account folders:folders];
					break;
				case kPingChangesOccurred:
					FXDebugLog(kFXLogFIXME, @"FIXME kPingChangesOccurred shouldn'thappen");
					break;
                case kPingFolderHierarchySyncRequired:
                    [self.account folderSync];
                    break;
                case kPingOmittedParameters:
                    if(self.account && self.folders.count > 0) {
                        [ASPing sendPing:self.account folders:folders];
                    }else{
                        [HttpRequest handleASError:[NSString stringWithFormat:@"Ping ommitted parameters fc=%d", self.folders.count]];
                    }
                    break;
                 case kPingInvalidWBXML:
                    if(self.emptyPing && self.account && self.folders.count > 0) {
                        [ASPing sendPing:self.account folders:folders];
                    }else{
                        [HttpRequest handleASError:@"Ping invalid WBXML"];
                    }
                    break;
				default:
					FXDebugLog(kFXLogActiveSync, @"Ping error occurred, unwise to carry on");
					break;
			}
		}
    }@catch (NSException* e) {
        [HttpRequest logException:@"Ping error occurred, unwise to carry on" exception:e];
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML: %@", kCommand, aContentType);
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connection:(HttpConnection*)connection didFailWithError:(NSError *)anError
{
    //NSDictionary* aUserInfo = [anError userInfo];
    switch([anError code]) {
        case NSURLErrorTimedOut:
            [ASPing sendPing:self.account folders:folders];
            break;
        case NSURLErrorCannotFindHost:
        case NSURLErrorCannotConnectToHost:
        case NSURLErrorNetworkConnectionLost:
        case NSURLErrorDNSLookupFailed:
        case NSURLErrorNotConnectedToInternet:
            [self handleReachability];
            return;
        default:
            [sPings removeObject:self];
            [super connection:connection didFailWithError:anError];
            break;
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
    [sPings removeObject:self];
    [super connectionDidFinishLoading:connection];
}

@end
