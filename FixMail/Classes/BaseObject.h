/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

@interface BaseObject : NSObject {
    
}

// Overrides
- (NSString*)asJSON;
- (void)fromJSON:(NSString*)aString;
- (NSDictionary*)asDictionary;

// Dictionary/JSON Utilities
- (void)addString:(NSString*)aString toDictionary:(NSMutableDictionary*)aDictionary key:(NSString*)aKey;

- (int)intFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey;
- (NSUInteger)unsignedIntFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey;
- (BOOL)boolFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey;
- (double)doubleFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey;

// Description Utilities
- (void)writeString:(NSMutableString*)string tag:(NSString*)tag value:(NSString*)value;
- (void)writeStringIndented:(NSMutableString*)string tag:(NSString*)tag value:(NSString*)value;
- (void)writeBoolean:(NSMutableString*)string tag:(NSString*)tag value:(BOOL)value;
- (void)writeInt:(NSMutableString*)string tag:(NSString*)tag value:(int)value;
- (void)writeUnsigned:(NSMutableString*)string tag:(NSString*)tag value:(unsigned int)value;
- (void)writeLongLong:(NSMutableString*)string tag:(NSString*)tag value:(long long)value;
- (void)writePointer:(NSMutableString*)string tag:(NSString*)tag pointer:(void*)pointer;
- (void)writeDataAsString:(NSMutableString*)string tag:(NSString*)tag data:(NSData*)data;
;
@end
