/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpRequest.h"
#import "ASAccount.h"
#import "HttpEngine.h"
#import "HttpConnection.h"
#import "Reachability.h"
#import "UIAlertView+Modal.h"
#import <Security/SecureTransport.h>

@implementation HttpRequest

@synthesize account;
@synthesize httpConnection;
@synthesize statusCode;
@synthesize contentType;
@synthesize thread;
@synthesize retries;

typedef enum 
{
    kDeny           = 0,
    kAllow          = 1,
} ECertUntrusted;

static const NSUInteger kMaxRetries = 32;
static const double     kRetryInterval = 5.0;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super init]) {
        account = anAccount;
        thread = false;
    }
    
    return self;
}

- (void)dealloc
{
    if(observingReachability) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    account         = nil;
    httpConnection  = nil;
}

- (void)cancel
{
    if(httpConnection) {
        [httpConnection cancel];

        NSString *connectionIdentifier = [httpConnection identifier];
        [[HttpEngine engine] removeConnection:connectionIdentifier];
        
        httpConnection = nil;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setURLRequest:(NSMutableURLRequest*)aURLRequest
{
    if(aURLRequest != URLrequest) {
        URLrequest = aURLRequest; 
    }
}

- (NSMutableURLRequest*)URLrequest
{
    return URLrequest;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

+ (void)logException:(NSString*)where exception:(NSException*)e
{
    [self handleASError:[NSString stringWithFormat:@"Exception: HTTPRequest %@ %@: %@", where, [e name], [e reason]]];
}

+ (NSString*)handleASError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"ACTIVESYNC_ERROR", @"HttpRequest", @"Alert view title for ActiveSync error") 
                                                          message:anErrorMessage 
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"ACTIVESYNC_ERROR_CANCEL", @"HttpRequest", @"Cancel button title for alert view for ActiveSync error")
                                                otherButtonTitles:nil];
    [anAlertView show];
    return anErrorMessage;
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (void)userAbortedWithMessage:(NSString*)aMesg;
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// Certificate trust for SSL
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Certificate trust for SSL

- (bool)evaluateTrust:(NSURLAuthenticationChallenge*)aChallenge secTrustRef:(SecTrustRef)aSecTrustRef
{
    BOOL retry = false;
    
    //NSURLProtectionSpace* aProtectionSpace = [aChallenge protectionSpace];
    //NSString* aHost = [aProtectionSpace host];
    
	NSURLCredential* aURLCredential = nil;
    SecTrustResultType aResult;
    OSStatus status = SecTrustEvaluate(aSecTrustRef, &aResult);
    if(status == 0) {
        switch(aResult) {
            case kSecTrustResultInvalid:
			{
                FXDebugLog(kFXLogActiveSync, @"Trust result invalid");
                [self handleCertificateError:@"Certificate invalid"];
                [[aChallenge sender] rejectProtectionSpaceAndContinueWithChallenge:aChallenge];
                break;
			}
            case kSecTrustResultProceed:
            {
                FXDebugLog(kFXLogActiveSync, @"Trust result proceed");
                aURLCredential = [NSURLCredential credentialForTrust:aSecTrustRef];
                [[aChallenge sender] useCredential:aURLCredential forAuthenticationChallenge:aChallenge];
                break;
            }
            case kSecTrustResultConfirm:
            {
			    FXDebugLog(kFXLogActiveSync, @"Trust result confirm");
                [self handleCertificateError:@"Confirm certificate"];
                aURLCredential = [NSURLCredential credentialForTrust:aSecTrustRef];
                [[aChallenge sender] useCredential:aURLCredential forAuthenticationChallenge:aChallenge];
                break;
			}
            case kSecTrustResultDeny:
             {
			    FXDebugLog(kFXLogActiveSync, @"Trust result deny");
                [self handleCertificateError:@"Deny certificate"];
                [[aChallenge sender] rejectProtectionSpaceAndContinueWithChallenge:aChallenge];
                break;
			}
            case kSecTrustResultUnspecified:
            {
                FXDebugLog(kFXLogActiveSync, @"Trust result unspecified");
                aURLCredential = [NSURLCredential credentialForTrust:aSecTrustRef];
                [[aChallenge sender] useCredential:aURLCredential forAuthenticationChallenge:aChallenge];
                break;
            }
            case kSecTrustResultRecoverableTrustFailure:
            {
                FXDebugLog(kFXLogActiveSync, @"Recoverable trust failure");
                [self handleCertificateError:@"Certificate invalid - Recoverable trust failure"];
                [[aChallenge sender] rejectProtectionSpaceAndContinueWithChallenge:aChallenge];
                break;
            }
            case kSecTrustResultFatalTrustFailure:
			{
			    FXDebugLog(kFXLogActiveSync, @"Fatal trust failure");
                [self handleCertificateError:@"Fatal trust failure"];
                [[aChallenge sender] rejectProtectionSpaceAndContinueWithChallenge:aChallenge];
                break;
			}
            case kSecTrustResultOtherError:
            {
				FXDebugLog(kFXLogActiveSync, @"Trust result other error");
                [self handleCertificateError:@"Trust unknown error"];
                [[aChallenge sender] rejectProtectionSpaceAndContinueWithChallenge:aChallenge];
                break;
			}
			default:
				break;
        }
    }
    return retry;
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)handleBasicAuthenticationChallenge:(NSURLAuthenticationChallenge*)aChallenge
{
    NSString* aTitle;
    
    NSError* anError = [aChallenge error];
    if(anError) {
        aTitle = [anError localizedDescription];
    }else{
        aTitle = FXLLocalizedStringFromTable(@"AUTHENTICATION_CHALLENGE", @"HttpRequest", @"AlertView title for authentication challenge");
    }
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:aTitle
                                                          message:FXLLocalizedStringFromTable(@"ENTER_NEW_PASSWORD", @"HttpRequest", @"Alert view message for authentication challenge, prompting user to enter a new password")
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"AUTHENTICATION_CHALLENGE_CANCEL", @"HttpRequest", @"Cancel button title for alert view for authentication challenge")
                                                otherButtonTitles:FXLLocalizedStringFromTable(@"AUTHENTICATION_CHALLENGE_OK", @"HttpRequest", @"OK button title for alert view for authentication challenge"), nil];
    anAlertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    int aButtonIndex = [anAlertView showModal];
    if(aButtonIndex == 0) {
        [[aChallenge sender] performDefaultHandlingForAuthenticationChallenge:aChallenge];
    }else{
        UITextField* aTextField = [anAlertView textFieldAtIndex:0];
        NSString* aString = aTextField.text;
        if(aString.length > 0) {
            self.account.password = aString;
            NSURLCredential *credential = [NSURLCredential credentialWithUser:self.account.userName password:aString
                                                                  persistence:NSURLCredentialPersistenceForSession];
            [[aChallenge sender] useCredential:credential forAuthenticationChallenge:aChallenge];
            [self.account commitSettings];
        }else{
            [[aChallenge sender] performDefaultHandlingForAuthenticationChallenge:aChallenge];
        }
    }
}

- (void)connection:(NSURLConnection*)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge*)aChallenge
{
    //NSURLProtectionSpace * aProtectionSpace = [aChallenge protectionSpace];
    NSURLCredential* aProposedCredential = [aChallenge proposedCredential];
    NSError* anError = [aChallenge error];
    FXDebugLog(kFXLogActiveSync, @"HTTP connection didReceiveAuthenticationChallenge: %@ %@", [anError localizedDescription], aProposedCredential);

    switch(self.account.authentication) {
        case kAuthenticationBasic:
            [self handleBasicAuthenticationChallenge:aChallenge];
            break;
        case kAuthenticationToken:
#warning FIXME kAuthenticationToken
            FXDebugLog(kFXLogFIXME, @"FIXME didReceiveAuthenticationChallenge kAuthenticationToken");
            break;
        case kAuthenticationCertificate:
#warning FIXME kAuthenticationToken
            FXDebugLog(kFXLogFIXME, @"FIXME didReceiveAuthenticationChallenge kAuthenticationToken");
            break;
    }
}

#if 0 // Old debug version with some cert handling stuff
- (void)connection:(NSURLConnection*)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge*)aChallenge
{
    NSURLProtectionSpace * aProtectionSpace = [aChallenge protectionSpace];
#ifdef DEBUG
    //[self debugChallenge:aChallenge];
    //[self debugProtectionSpace:aProtectionSpace];
    NSURLCredential* aProposedCredential = [aChallenge proposedCredential];
    NSError* anError = [aChallenge error];
    FXDebugLog(kFXLogActiveSync, @"HTTP connection didReceiveAuthenticationChallenge: %@ %@", anError, aProposedCredential);
#endif

    if([aProtectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodDefault]) {
        FXDebugLog(kFXLogActiveSync, @"default challenge");
    }

    // Ordinary credentials challenge
#if 1
    [[aChallenge sender] performDefaultHandlingForAuthenticationChallenge:aChallenge];
#else
	if ([aUserName length] > 0 && [aPassword length] > 0) {
        if([aChallenge previousFailureCount] == 0 /* }&& ![aChallenge proposedCredential]*/) {
            NSURLCredential *credential = [NSURLCredential credentialWithUser:aUserName password:aPassword 
                                                                  persistence:NSURLCredentialPersistenceForSession];
            [[aChallenge sender] useCredential:credential forAuthenticationChallenge:aChallenge];
        }else{
            [[aChallenge sender] cancelAuthenticationChallenge:aChallenge];
        }
        return;
	} else {
        [[aChallenge sender] performDefaultHandlingForAuthenticationChallenge:aChallenge];
        //[[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
        //[[challenge sender] rejectProtectionSpaceAndContinueWithChallenge:challenge];
        //[[challenge sender] [cancelAuthenticationChallenge:challenge];
		//[[aChallenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
	}
#endif
}
#endif

-(NSURLRequest *)connection:(HttpConnection *)connection
            willSendRequest:(NSURLRequest *)urlRequest
           redirectResponse:(NSURLResponse *)redirectResponse
{
    NSURLRequest *newUrlRequest = urlRequest;
    if (redirectResponse) {
        FXDebugLog(kFXLogActiveSync, @"HTTP connection willSendResponse: %@ redirectResponse: %@", urlRequest, redirectResponse);
    }
    return newUrlRequest;
}

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [connection resetData];
    
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    statusCode = [resp statusCode];
#ifdef DEBUG	
	NSDictionary* allHeaders = [resp allHeaderFields];
	FXDebugLog(kFXLogActiveSync, @"HTTP didReceiveResponse: %d headers: %@", statusCode, allHeaders);
#endif
    
	if(statusCode >= 200 && statusCode < 300) {
        // HTTP Success
        //
	}else if(statusCode >= 400) {
		// HTTP Error
		//
    }else if(statusCode == 304) {
        // Not modified, or generic success
		//    
        // Destroy the connection
        //
        //[connection cancel];
        //[self connectionRelease:connection];
    }else if (statusCode >= 300) {
        // Redirect
        //
        NSDictionary* allHeaders = [resp allHeaderFields];
        NSString* location = [allHeaders objectForKey:@"Location"];
        if(location) {
            FXDebugLog(kFXLogActiveSync, @"HTTP Redirect to %@", location);
        }else{
            FXDebugLog(kFXLogActiveSync, @"HTTP Redirect error: %d %@", statusCode, allHeaders);
        }
    }
}

- (void)connection:(HttpConnection*)connection didReceiveData:(NSData *)data
{
	HttpRequest* httpRequest = [connection httpRequest];
#ifdef DEBUG
	NSString* string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	FXDebugLog(kFXLogActiveSync, @"didReceiveData: %@", string);
#endif
	if([httpRequest statusCode] < 400) {

	}else{
		[connection appendData:data];
	}
}

- (void)_httpError:(HttpConnection*)connection
{
	NSData* receivedData = [connection data];
	NSMutableDictionary* dictionary = [NSMutableDictionary dictionaryWithCapacity:1];
	[dictionary setObject:receivedData forKey:@"data"];
#ifdef DEBUG
    HttpRequest* request = [connection httpRequest];
	NSError* error = [NSError errorWithDomain:@"HTTP" code:[request statusCode] userInfo:dictionary];
    FXDebugLog(kFXLogActiveSync, @"HTTP error: %@", error);
#endif
}

- (void)connectionRelease:(HttpConnection*)connection
{
	NSString *connectionIdentifier = [connection identifier];
	
#if 0
	// Notify HttpRequest the connection finished
	//
	HttpRequest* request = [connection query];
	if(request) {
		[request setConnectionClosed:connectionIdentifier];
	}
#endif
	
	// Remove connection from dictionary, this should cause the HttpConnection to release
	//
    [[HttpEngine engine] removeConnection:connectionIdentifier];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
	
	// If HTTP status code is an error special handle the error data
	//
	//HttpRequest* httpRequest = [connection httpRequest];
	//if([httpRequest statusCode] >= 400) {
		//[self _httpError:connection];
	//}
    
	[self connectionRelease:connection];
}

- (void)connection:(HttpConnection*)connection didFailWithError:(NSError *)anError
{
    //NSDictionary* aUserInfo = [anError userInfo];
    NSInteger anErrorCode = [anError code];
    switch(anErrorCode) {
        case EPFNOSUPPORT:      /* Protocol family not supported */
        case EAFNOSUPPORT:      /* Address family not supported by protocol family */
        case EADDRINUSE:        /* Address already in use */
        case EADDRNOTAVAIL:     /* Can't assign requested address */
            
            /* ipc/network software -- operational errors */
        case ENETDOWN:          /* Network is down */
        case ENETUNREACH:       /* Network is unreachable */
        case ENETRESET:         /* Network dropped connection on reset */
        case ECONNABORTED:      /* Software caused connection abort */
        case ECONNRESET:        /* Connection reset by peer */
        case ENOBUFS:           /* No buffer space available */
        case EISCONN:           /* Socket is already connected */
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleGenericError:anError];
            break;
        case ENOTCONN:          /* Socket is not connected */
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleGenericError:anError];
            break;
        case ESHUTDOWN:         /* Can't send after socket shutdown */
        case ETOOMANYREFS:      /* Too many references: can't splice */
        case ETIMEDOUT:         /* Operation timed out */
        case ECONNREFUSED:      /* Connection refused */
            
        case EHOSTDOWN:         /* Host is down */
        case EHOSTUNREACH:      /* No route to host */
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleGenericError:anError];
            break;
        case NSURLErrorTimedOut:
        case NSURLErrorCannotFindHost:
        case NSURLErrorCannotConnectToHost:
        case NSURLErrorNetworkConnectionLost:
        case NSURLErrorDNSLookupFailed:
        case NSURLErrorNotConnectedToInternet:
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleReachability];
            break;
        case NSURLErrorServerCertificateHasBadDate:
        case NSURLErrorServerCertificateUntrusted:
        case NSURLErrorServerCertificateHasUnknownRoot:
        case NSURLErrorServerCertificateNotYetValid:
        case NSURLErrorClientCertificateRejected:
        case NSURLErrorClientCertificateRequired:
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleCertificateError:[anError localizedDescription]];
            break;
            
        case NSNetServicesUnknownError:
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleGenericError:anError];
            break;
            
        case errSSLXCertChainInvalid:
        {
            NSURL* aURL = [[anError userInfo] valueForKey:@"NSErrorFailingURLKey"];
            [self handleCertificateError:
             [NSString stringWithFormat:@"Unable to validate the certificate for %@. This container must lock. Please see your IT Administrator.",
                aURL.host]];
            break;
        }
        case errSSLProtocol:
        case errSSLNegotiation:
        case errSSLFatalAlert: 
        case errSSLWouldBlock: 
        case errSSLSessionNotFound: 
        case errSSLClosedGraceful: 
        case errSSLClosedAbort: 

        case errSSLBadCert:
        case errSSLCrypto:
        case errSSLInternal:
        case errSSLModuleAttach:
        case errSSLUnknownRootCert:
        case errSSLNoRootCert:
        case errSSLCertExpired:
        case errSSLCertNotYetValid:
        case errSSLClosedNoNotify:
        case errSSLBufferOverflow:
        case errSSLBadCipherSuite:
            
        case errSSLPeerUnexpectedMsg:
        case errSSLPeerBadRecordMac:
        case errSSLPeerDecryptionFail:
        case errSSLPeerRecordOverflow:
        case errSSLPeerDecompressFail:
        case errSSLPeerHandshakeFail:
        case errSSLPeerBadCert:
        case errSSLPeerUnsupportedCert:
        case errSSLPeerCertRevoked:
        case errSSLPeerCertExpired:
        case errSSLPeerCertUnknown:
        case errSSLIllegalParam:
        case errSSLPeerUnknownCA:
        case errSSLPeerAccessDenied:
        case errSSLPeerDecodeError:
        case errSSLPeerDecryptError:
        case errSSLPeerExportRestriction:
        case errSSLPeerProtocolVersion:
        case errSSLPeerInsufficientSecurity:
        case errSSLPeerInternalError:
        case errSSLPeerUserCancelled:
        case errSSLPeerNoRenegotiation:
            
        //case errSSLPeerAuthCompleted:
        //case errSSLClientCertRequested:
            
        case errSSLHostNameMismatch:
        case errSSLConnectionRefused:
        case errSSLDecryptionFail:
        case errSSLBadRecordMac:
        case errSSLRecordOverflow:
        case errSSLBadConfiguration:
        case errSSLUnexpectedRecord:
        {
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleCertificateError:[anError localizedDescription]];
            break;
        }
        default:
            FXDebugLog(kFXLogActiveSync, @"HTTP connection didFailWithError: %@", [anError localizedDescription]);
            [self handleGenericError:anError];
            break;
    }
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kHttpRequestFailureNotification object:nil];
    
	[self connectionRelease:connection];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Retry
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Retry

- (void)doRetry
{
    FXDebugLog(kFXLogActiveSync, @"retry %d: %@", self.retries, self.URLrequest);
    [[HttpEngine engine] sendRequest:URLrequest httpRequest:self];
}

- (void)sendRetry
{
    [NSTimer scheduledTimerWithTimeInterval:kRetryInterval*self.retries
                                     target:self
                                   selector:@selector(doRetry)
                                   userInfo:nil
                                    repeats:NO];
    self.retries++;
}

- (void)retry
{
    [httpConnection cancel];
    Reachability* aReachability = self.account.reachability;
    if(aReachability) {
        NetworkStatus aHostStatus = aReachability.currentReachabilityStatus;
        switch(aHostStatus) {
            case ReachableViaWWAN:
            case ReachableViaWiFi:
            {
                if(self.retries == 1) {
                    [self sendRetry];
                }else if(retries < kMaxRetries) {
                    [self sendRetry];
                }else{
                    FXDebugLog(kFXLogActiveSync, @"handleRetry max retries: %d", self.retries);
                    if(self.statusCode > 200) {
                        [self handleHttpError];
                    }else{
#warning FIXME Need to post error here, especially if its ActiveSync status error
                    }
                }
                break;
            }
            case NotReachable:
                [self handleReachability];
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)handleCertificateError:(NSString*)aMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"SSL_CERTIFICATE_ERROR", @"HttpRequest", @"Alert view title for SSL certificate error")
                                                          message:aMessage 
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"SSL_CERTIFICATE_ERROR_OK", @"HttpRequest", @"Alert view OK button title for SSL certificate error")
                                                otherButtonTitles:nil];
    /*ECertUntrusted aButtonIndex =*/ [anAlertView showModal];
    [self userAbortedWithMessage:aMessage];
}

- (void)handleGenericError:(NSError*)anError
{
    NSString* aMessage = [anError localizedDescription];
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"NETWORK_ERROR", @"HttpRequest", @"Alert view title for network error")
                                                          message:aMessage 
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"NETWORK_ERROR_CANCEL", @"HttpRequest", @"Alert view cancel button title for network error")
                                                otherButtonTitles:FXLLocalizedStringFromTable(@"NETWORK_ERROR_RETRY", @"HttpRequest", @"Alert view retry button title for network error"), nil];
    int aButtonIndex = [anAlertView showModal];
    if(aButtonIndex == 0) {
        [self userAbortedWithMessage:aMessage];
    }else{
        [self retry];
    }
}

- (void)postHttpError
{
    [httpConnection cancel];
    
    NSString* aMessage = [NSHTTPURLResponse localizedStringForStatusCode:self.statusCode];
    FXDebugLog(kFXLogAll, @"HTTP Error: %@\n%@", aMessage, self);
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"HTTP_ERROR", @"HttpRequest", @"Alert view title for HTTP error")
                                                          message:aMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"HTTP_ERROR_CANCEL", @"HttpRequest", @"Alert view cancel button title for HTTP error")
                                                otherButtonTitles:FXLLocalizedStringFromTable(@"HTTP_ERROR_RETRY", @"HttpRequest", @"Alert view cancel button title for HTTP error"), nil];
    int aButtonIndex = [anAlertView showModal];
    if(aButtonIndex == 0) {
        [self userAbortedWithMessage:aMessage];
    }else{
        [self retry];
    }
}

- (void)handleHttpError
{
    switch(self.statusCode) {
        case kHttpCreated:
        case kHttpAccepted:
        case kHttpNonAuthorativeInformation:
        case kHttpNoContent:
        case kHttpResetContent:
        case kHttpPartialContent:
        case kHttpForbidden:
        case kHttpPaymentRequired:
        case kHttpBadRequest:
        case kHttpUnauthorized: // Should be handled in didReceiveAuthenticationChallenge
        case kHttpNotFound:
        case kHttpMethodNotAllowed:
        case kHttpNotAcceptable:
        case kHttpProxyAuthenticationRequired:
        case kHttpRequestTimeout:
        case kHttpConflict:             // Domino will send this if sync and ping in flight at same time
        case kHttpGone:
        case kHttpLengthRequired:
        case kHttpPreconditionFailed:
        case kHttpRequestEntityTooLarge:
        case kHttpRequestURITooLong:
        case kHttpUnsupportedMediaType:
        case kHttpRequestedRangeNotSatisfied:
        case kHttpExpectationFailed:
        case kHttpInternalServerError:
        case kHttpNotImplemented:
        case kHttpBadGateway:
        case kHttpGatewayTimeout:
        case kHttpVersionNotSupported:
            [self postHttpError]; break;
           
        case kHttpServiceUnavailable:   // GMail sends a lot of these, probably as load control
            [self retry]; break;
            
        case kHttpNeedsProvisioning:
            // Should be handled in WBXMLRequest
            [self postHttpError]; break;

        default:
            [self postHttpError]; break;
            break;
    }
}

- (void)handleReachability
{
    Reachability* aReachability = self.account.reachability;
    if(aReachability) {
        NetworkStatus aHostStatus = aReachability.currentReachabilityStatus;
        switch(aHostStatus) {
            case ReachableViaWWAN:
            case ReachableViaWiFi:
            {
                [self retry];
                break;
            }
            case NotReachable:
            {
                if(observingReachability) {
                    FXDebugLog(kFXLogActiveSync, @"FIXME already observing reachability");
                }else{
                    observingReachability = TRUE;
                    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification
                                                                      object:nil queue:nil
                                                                  usingBlock:^(NSNotification *inNotification) {
                                                                      Reachability* aReachability = inNotification.object;
                                                                      NetworkStatus aHostStatus = aReachability.currentReachabilityStatus;
                                                                      switch(aHostStatus) {
                                                                          case ReachableViaWWAN:
                                                                          case ReachableViaWiFi:
                                                                          {
                                                                              if(aHostStatus == ReachableViaWWAN) {
                                                                                  FXDebugLog(kFXLogActiveSync, @"Server reachable via mobile: %@", self.account.hostName);
                                                                              }else{
                                                                                  FXDebugLog(kFXLogActiveSync, @"Server reachable via WiFi: %@", self.account.hostName);
                                                                              }
                                                                              [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
                                                                              observingReachability = FALSE;
                                                                              [self retry];
                                                                              break;
                                                                          }
                                                                          case NotReachable:
                                                                              FXDebugLog(kFXLogActiveSync, @"Server not reachable: %@", self.account.hostName);
                                                                              break;
                                                                      }
                                                                  }
                     ];
                }
                break;
            }
        }
    }
}

@end
