/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "WBXMLRequest.h"
#import "ASComposeDelegate.h"

@class ASAccount;
@class ASFolder;

@interface ASCompose : WBXMLRequest {
    NSObject<ASComposeDelegate>*     delegate;
    NSString*                       clientID;
    
    ASTag                           commandTag;
}

// Properties
@property (strong) NSObject<ASComposeDelegate>* delegate;
@property (strong) NSString*                    clientID;

// Factory
+ (void)sendMail:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate;
+ (void)sendMail:(ASAccount*)anAccount string:(NSString*)aMimeString delegate:(NSObject<ASComposeDelegate>*)aDelegate;

+ (void)sendSmartForward:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate;
+ (void)sendSmartReply:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount tag:(ASTag)aTag;

@end