 //
//  ASConfigViewController.m
//
//  Displays credential entry screen to the user ...
//
//  Created by Gabor Cselle on 1/22/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "ASConfigViewController.h"
#import "AppSettings.h"
#import "ASAccountDomino.h"
#import "ASAccountExchange.h"
#import "ASAccountGmail.h"
#import "ASOptions.h"
#import "ASTypes.h"
#import "ErrorAlert.h"
#import "HttpRequest.h"
#import "ProgressView.h"
#import "Reachability.h"
#import "SyncManager.h"
#import "StringUtil.h"
#import "Reachability.h"
#import "TraceViewController.h"
#import "UIAlertView+Modal.h"
#import "SZLConcreteApplicationContainer.h"
#import "SVProgressHUD.h"

#import "SZCAppDelegate.h"
#import "SZLCAC.h"


#define personalCodeFieldTag 200

#define ActiveSync_PORT                 443
#define ActiveSync_CONNECTION_TYPE      kEncryptionHttps
#define ActiveSync_AUTH_TYPE            kAuthenticationBasic

@interface ASConfigViewController()
{
    UITextField* activeField;
}
@property (weak, nonatomic) IBOutlet UILabel *accountOptionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *stk08AccountButton;
@property (weak, nonatomic) IBOutlet UIButton *stk09AccountButton;
@property (weak, nonatomic) IBOutlet UIButton *stk01AccountButton;
@property (weak, nonatomic) IBOutlet UIButton *gmailAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *gmailFixmoAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *microsoftExchangeAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *configureButton;

@property (nonatomic) int numberOfSections;

@end

@implementation ASConfigViewController

@synthesize hostName;
@synthesize path;
@synthesize reachability;
@synthesize accountOptionsLabel;
@synthesize stk08AccountButton;
@synthesize stk09AccountButton;
@synthesize stk01AccountButton;
@synthesize gmailAccountButton;
@synthesize gmailFixmoAccountButton;
@synthesize microsoftExchangeAccountButton;

// Globals
extern BOOL gIsFixTrace;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

+(UINavigationController *)accountConfigurationControllerWithDelegate:(id <SZLConfigurationDelegate>)configurationDelegate
{
	ASConfigViewController *configurationController = nil;
	if (IS_IPAD()) {
		configurationController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPad identifier:@"ASConfigViewController"];
	} else {
		configurationController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPhone identifier:@"ASConfigViewController"];
	}
	if (configurationController) {
		if (configurationDelegate) {
			configurationController.configurationDelegate = configurationDelegate;
		}
		UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:configurationController];
		navigationController.modalPresentationStyle = UIModalPresentationPageSheet;
		return navigationController;
	}
	
	return nil;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];    
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
	[super viewDidUnload];

}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    [self setupView];
    [self setupFields];
    [self setupAccount];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return self.numberOfSections;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];	
}

- (void)viewDidAppear:(BOOL)animated 
{
	[super viewDidAppear:animated];

}

- (void)removeReachabilityNotifier
{
    if(observingReachability) {
        [self.reachability stopNotifier];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
        self.reachability = nil;
        observingReachability = FALSE;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self removeReachabilityNotifier];
	
	// Hide any visual information
	if ([SVProgressHUD isVisible]) {
		[SVProgressHUD dismiss];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	FXDebugLog(kFXLogActiveSync, @"ActiveSyncConfig received memory warning");
}

////////////////////////////////////////////////////////////////////////////////
// Setup methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setup View

-(void)setupFields
{
    if ([SZLCAC isContainerCACEnabled]) {
        self.passwordField.hidden = YES;
        self.PCCField.hidden = NO;
        self.passwordLabel.text = @"PCC";
    } else {
        self.PCCField.hidden = YES;
        self.passwordField.hidden = NO;
        self.passwordLabel.text = @"Password";
    }
    
    // Setup text fields
    //
	self.nameField.delegate = self;
	self.userNameField.delegate = self;
	self.passwordField.delegate = self;
    self.PCCField.delegate = self;
	if(self.domainField) {
        self.domainField.delegate = self;
    }
    
#if showServerFields
    if(self.hostnameField) {
        self.hostnameField.delegate = self;
    }
    if(self.portField) {
        self.portField.delegate = self;
    }
	
	self.numberOfSections = 3;
#else
	[self.tableView beginUpdates];
	self.numberOfSections = 1;
	[self.tableView endUpdates];
	
	self.accountOptionsLabel.hidden = YES;
	self.stk08AccountButton.hidden = YES;
	self.stk09AccountButton.hidden = YES;
	self.gmailAccountButton.hidden = YES;
	self.gmailFixmoAccountButton.hidden = YES;
	self.microsoftExchangeAccountButton.hidden = YES;
#endif
	
}

-(void)setupView
{
    UIBarButtonItem *configureLaterButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"CONFIGURE_LATER", @"ASConfigViewController", @"Button title for skipping email configuration and going to application springboard") style:UIBarButtonItemStyleDone target:self action:@selector(configureLaterClicked:)];
	
	UIBarButtonItem *configureButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"CONFIGURE", @"ASConfigViewController", @"Button title for starting email configuration") style:UIBarButtonItemStyleBordered target:self action:@selector(accountConfigurationClicked:)];
    [self.configureButton setBackgroundImage:[[UIImage imageNamed:@"FixMailRsrc.bundle/redButton.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];


    if (IS_IPAD()) {
		self.title = FXLLocalizedStringFromTable(@"CONFIGURE_ACCOUNT",  @"ASConfigViewController", @"Title of view controller for new account configuration");
	} else {
		self.title = @"";
		configureButton.title = FXLLocalizedStringFromTable(@"CONFIGURE_ACCOUNT",  @"ASConfigViewController", @"Title of view controller for new account configuration");
	}
    
    // let us navigate back to the main page
	self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
	self.navigationItem.leftBarButtonItem = configureLaterButton;
	self.navigationItem.rightBarButtonItem = configureButton;
    
    [self.selectFoldersButton setEnabled:YES];
    
    [self.scrollView setContentSize:CGSizeMake(320, 1300)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
	// setup a gesture on the view to get rid fo the keyboard
	UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
	tapGestureRecognizer.delegate = self;
 	[self.scrollView addGestureRecognizer:tapGestureRecognizer];
}

-(void)setupAccount
{
    // preset emailAddress, password if they exist
    //
	if(!self.newAccount && self.account) {
        if ([SZLCAC isContainerCACEnabled]) {
            self.PCCField.text = account.pcc;
            self.userNameField.text = account.userName;
        } else {
            self.passwordField.text = account.password;
            self.userNameField.text = account.userName;
        }
        self.nameField.text     = account.name;
		self.domainField.text   = account.domain;
#if showServerFields
		self.hostnameField.text = account.hostName;
        self.portField.text     = [NSString stringWithFormat:@"%d", account.port];
#endif
	} else {
		// if we're adding a new account, we shouldn't show select folders button
		[self.selectFoldersButton setHidden:YES];
        
        if ([SZLCAC isContainerCACEnabled]) {
            [SZLCAC getEDIPI:^(NSString *EDIPI) {
                self.userNameField.text = EDIPI;
                [self.userNameField setEnabled:NO];
            }];
        }
	}
    
}


////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception: FXLLocalizedStringFromTable(@"CONFIGURE_VIEW", @"ErrorAlert", @"Error alert view title in ASConfigViewController") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CONFIGURE_VIEW", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
}

////////////////////////////////////////////////////////////////////////////////
// Methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark Methods

- (void)saveSettings:(ASAccount*)anAccount 
{
	// The user was editing the account and clicked "Check and Save", and it validated OK
	
    [anAccount commitSettings];
	
	[self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)_openHomeView 
{
//	if (self.navigationController != nil) {
//		[self.navigationController popToRootViewControllerAnimated:YES];
//	} else {
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//	}
}

-(void)_openTraceView
{
	if (self.navigationController != nil) {
		[self.navigationController popToRootViewControllerAnimated:YES];
	} else {
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)closeConfigurationView
{	
    if(!gIsFixTrace) {
        [self _openHomeView];
    }else{
        [self _openTraceView];
    }
}

- (void)showFolders:(ASAccount*)anAccount
{
    // The user was adding a new account and clicked "Check and Save", now let him/her select folders
	[self closeConfigurationView];
}

- (void)showFolderSelect:(ASAccount*)anAccount
{
/*
	// The user was adding a new account and clicked "Check and Save", now let him/her select folders
	
	// display home screen
	FolderSelectViewController *vc = [[FolderSelectViewController alloc] initWithNibName:@"FolderSelect" bundle:[FXLSafeZone getResourceBundle]];
    [vc setAccount:anAccount];
	vc.folderPaths      = [anAccount folderNames];
	
	vc.name             = [anAccount name];
	vc.emailAddress     = [anAccount emailAddress];
	vc.password         = [anAccount password];
	vc.server           = [anAccount hostName];
	
	vc.encryption       = [anAccount encryption];
	vc.port             = [anAccount port];
	vc.authentication   = [anAccount authentication];

	vc.newAccount       = self.newAccount;
	vc.firstSetup       = self.firstSetup;
	vc.accountNum       = self.accountNum;
    vc.accountType      = AccountTypeActiveSync;
	
	[self.navigationController pushViewController:vc animated:YES];
*/
}


- (void)failedLoginWithMessage:(NSString*)message 
{
	[self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
	
	[self handleError:message];
	
	[self.selectFoldersButton setEnabled:YES];
	
	[BaseAccount deleteAllAccounts];
}

- (NSString*)massageEmailAddress:(NSString*)anAddress
{
    NSString* aMassagedEmailAddress;
    
    //if(![StringUtil stringContains:anAddress subString:@"@"]) {
    //    aMassagedEmailAddress = [NSString stringWithFormat:@"%@@%@", anAddress, server];
	//}else{
        aMassagedEmailAddress = anAddress;
    //}
    
    return aMassagedEmailAddress;
}

- (BOOL)validateFields
{
    name            = self.nameField.text; 
    
    if (![SZLCAC isContainerCACEnabled]) {
        if(self.passwordField && [self.passwordField.text length] > 0) {
            password        = self.passwordField.text; 
        } else {
            [self handleError: FXLLocalizedStringFromTable(@"NO_PASSWORD", @"ErrorAlert", @"Error alert view message when validating account configuration fields and no password was entered")];	
            return FALSE;
        }
    } else {
        password = @"";
        PCC = self.PCCField.text;
    }
    
#if showServerFields
    if(self.hostnameField && [self.hostnameField.text length] > 0) {
        server = [self parseHostName];
    }else{
        [self handleError: FXLLocalizedStringFromTable(@"NO_SERVER", @"ErrorAlert", @"Error alert view message when validating account configuration fields and no server was entered")];
        return FALSE;
    }
	
    if(self.portField && [self.portField.text length] > 0) {
        port        = [self.portField.text intValue];
    }else{
        port        = ActiveSync_PORT;
    }
#endif
    
    if(self.userNameField && [self.userNameField.text length] > 0) {
        userName    = self.userNameField.text;       
    }else{
        [self handleError: FXLLocalizedStringFromTable(@"NO_USERNAME", @"ErrorAlert", @"Error alert view message when validating account configuration fields and no username was entered")];
        return FALSE;
    }
	
	if(self.domainField && [self.domainField.text length] > 0) {
        domain      = self.domainField.text;
    }else{
    }
    
    encryption      = ActiveSync_CONNECTION_TYPE;
    authentication  = ActiveSync_AUTH_TYPE;
    
    return TRUE;
}

- (void)doLogin:(NSNumber*)forceSelectFolders
{
    @autoreleasepool {
        FXDebugLog(kFXLogActiveSync, @"Logging in with user name %@", userName);
        
        if(!account) {
            // Create new account unless we are editing an existing account
            if(accountSubType == AccountSubTypeGmail) {
                account = [[ASAccountGmail alloc] initWithName:name
                                                      userName:userName
                                                      password:password
                                                      hostName:server
                                                          path:self.path
                                                          port:port
                                                    encryption:encryption
                                                authentication:authentication
                                                   folderNames:NULL];
            }else if(accountSubType == AccountSubTypeDomino) {
                account = [[ASAccountDomino alloc] initWithName:name
                                                       userName:userName
                                                       password:password
                                                       hostName:server
                                                           path:self.path
                                                           port:port
                                                     encryption:encryption
                                                 authentication:authentication
                                                    folderNames:NULL];
            }else{
                accountSubType = AccountSubTypeExchange;
                account = [[ASAccountExchange alloc] initWithName:name
                                                         userName:userName 
                                                         password:password
                                                         hostName:server
                                                             path:self.path
                                                             port:port 
                                                       encryption:encryption
                                                   authentication:authentication
                                                      folderNames:NULL];
            }
        }else{
            // Editing an existing account
            account.name            = name;
            account.userName        = userName;
            account.pcc             = PCC;
            account.password        = password;
            account.hostName        = server;
            account.path            = self.path;
            account.encryption      = encryption;
            account.authentication  = authentication;
        }
        
        if(account) {
            [account setDomain:domain];
            [account setPcc:PCC];
            
            //store CAC username in secureSettings for other apps' use. Only if CAC enabled.    
            if ([SZLCAC isContainerCACEnabled]) {
                NSString* cacUserName;
                if (PCC) {
                    cacUserName = [userName stringByAppendingString:PCC];
                } else {
                    cacUserName = userName;
                }
                [AppSettings setCacUserName:cacUserName];
            }
            
            ASAccount* anASAccount = (ASAccount*)account;
            [anASAccount setDelegate:self];
			
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(httpRequestFailed:) name:kHttpRequestFailureNotification object:nil];
            
            // This ActiveSync Options command will initiate a Provision followed by a FolderSync etc. if successful
            //
            [anASAccount options:self];
        }
    }
}

- (void)httpRequestFailed:(NSNotification *)notification
{
	[SVProgressHUD dismiss];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kHttpRequestFailureNotification object:nil];
}


////////////////////////////////////////////////////////////////////////////////
// UITextFieldDelegate Methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITextFieldDelegate methods

//	fields are ordered as follows:
//	nameField
//	userNameField
//	passwordField
//	hostnameField
//	domainField
//	portField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == self.nameField) {
		[self.userNameField becomeFirstResponder];
	} else if (textField == self.userNameField) {
		[self.passwordField becomeFirstResponder];
	} else if (textField == self.passwordField) {

		// make sure the others are filled in, and if so, then do a login
		if (self.hostnameField.text.length == 0) [self.hostnameField becomeFirstResponder];
		else if (self.portField.text.length == 0) [self.portField becomeFirstResponder];
		else if (self.nameField.text.length == 0) [self.nameField becomeFirstResponder];
		else if (self.userNameField.text.length == 0) [self.userNameField becomeFirstResponder];
		else [self loginClick];
		return NO;

	} else if (textField == self.hostnameField) {
		[self.domainField becomeFirstResponder];
	} else if (textField == self.domainField) {
		[self.portField becomeFirstResponder];
	} else if (textField == self.portField) {
		[self.nameField becomeFirstResponder];
	} else {
		return NO;
	}

	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (NSString*)parseHostName
{
    NSString* aHostName = nil;
    
    NSString* aString;
    NSString* aFieldString = hostnameField.text;
    if([aFieldString hasPrefix:@"http:"] || [aFieldString hasPrefix:@"https:"]) {
        aString = aFieldString;
    }else{
        aString = [NSString stringWithFormat:@"https://%@",aFieldString];
    }
    NSURL* aURL = [NSURL URLWithString:aString];
    aHostName   = aURL.host;
    self.path   = aURL.path;
    FXDebugLog(kFXLogActiveSync, @"host: %@ path: %@", aHostName, self.path);
    
    return aHostName;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;

    if(textField == hostnameField) {
        // If this is the hostname field, test reachability of the server
        //
        [self isReachableHost:[self parseHostName]];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == personalCodeFieldTag) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 3) ? NO : YES;
    }
    
    return YES;
}

#pragma mark - UIGestureRecognizerDelegate methods

- (void) handleTap:(UITapGestureRecognizer *)inRecognizer
{
	[activeField resignFirstResponder];
}

////////////////////////////////////////////////////////////////////////////////
// ASConfigDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark - ASConfigDelegate

- (void)configureSuccess:(NSArray*)aFolders;
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kProgressViewStringDidChangeNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kHttpRequestFailureNotification object:nil];
	
	dispatch_async(dispatch_get_main_queue(), ^(void) {
		[SVProgressHUD showSuccessWithStatus:FXLLocalizedStringFromTable(@"ACCOUNT_SUCCESSFULLY_CONFIGURED", @"ASConfigViewController", @"Alert view message when account successfully configured") ];
	});
	
	__block id progressObserver = [[NSNotificationCenter defaultCenter] addObserverForName:SVProgressHUDDidDisappearNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
		if(self.newAccount /* || [forceSelectFolders boolValue]*/) {
			[account setAccountType:AccountTypeActiveSync];
			//FXDebugLog(kFXLogActiveSync, @"account type: %d subType: %d", account.accountType, account.accountSubType);
			[AppSettings resetUserSettings];
			[account create];
#if 1
			[self performSelectorOnMainThread:@selector(showFolders:) withObject:account waitUntilDone:NO];
#else
			[self performSelectorOnMainThread:@selector(showFolderSelect:) withObject:account waitUntilDone:NO];
#endif
		} else {
			[self performSelectorOnMainThread:@selector(saveSettings:) withObject:account waitUntilDone:NO];
		}
		configureInProgress = FALSE;
		
		if (self.configurationDelegate) {
			[self.configurationDelegate configurationDidSucceed:nil];
		} else {
			[self closeConfigurationView];
			[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
		}
		[[NSNotificationCenter defaultCenter] removeObserver:progressObserver];
	}];
	
	double delayInSeconds = 5;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		
		

	});	
}

- (void)configureFailed:(EHttpStatusCode)aStatusCode
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kHttpRequestFailureNotification object:nil];
	[SVProgressHUD dismiss];
    NSString* aMesg = @"";

    NetworkStatus status = self.reachability.currentReachabilityStatus;
    if(status == NotReachable) {
        aMesg = FXLLocalizedStringFromTable(@"EMAIL_SERVER_UNREACHABLE", @"ASConfigViewController", @"Progress view error message when configure failed because email server was unreachable");
    } else if(status == ReachableViaWWAN) {
        aMesg = FXLLocalizedStringFromTable(@"ERROR_CONNECTING_TO_SERVER_TRY_OVER_WIFI", @"ASConfigViewController", @"Progress view error message when confgure failed because of error while attempting to connect to server through carrier data network");
    } else {
        aMesg = FXLLocalizedStringFromTable(@"ERROR_CONNECTING_TO_SERVER", @"ASConfigViewController", @"Progress view error message when configure failed because of other error connecting to server.");
    }

    [self performSelectorOnMainThread:@selector(failedLoginWithMessage:) withObject:aMesg waitUntilDone:NO];
    configureInProgress = FALSE;
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kProgressViewStringDidChangeNotification object:nil];
}

- (void)_progressMessage:(NSString*)aMesg
{
    [[ProgressView progressView] didChangeProgressStringTo:aMesg];
}

- (void)progressMessage:(NSString*)aMesg
{
    [self performSelectorOnMainThread:@selector(_progressMessage:) withObject:aMesg waitUntilDone:NO];
}
	 
-(void)progressMessageDidChange:(NSNotification *)aNotification
{
	if ([SVProgressHUD isVisible]) {
		[SVProgressHUD setStatus:[ProgressView progressView].updatedLabel.text];
	}
}

- (void)configureAbortedWithMessage:(NSString*)aMesg
{
    [self performSelectorOnMainThread:@selector(failedLoginWithMessage:) withObject:aMesg waitUntilDone:NO];
    configureInProgress = FALSE;
}

////////////////////////////////////////////////////////////////////////////////
// ASOptionsDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASOptionsDelegate

- (void)authenticationSuccess:(ASOptions*)anOptions
{
#ifdef DEBUG
    self.userNameStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/greenCheck.png"];
    self.passwordStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/greenCheck.png"];
#endif
}

- (void)authenticationChallenge:(NSURLAuthenticationChallenge*)aChallenge
                        options:(ASOptions*)anOptions
{
	//FIXME: It might be the case that the domain name or username/server combination is actually invalid
    NSString* aMesg = FXLLocalizedStringFromTable(@"INVALID_USERNAME_OR_PASSWORD", @"ASConfigViewController", @"Alert view message when user entered invalid username or password");
    [self performSelectorOnMainThread:@selector(failedLoginWithMessage:) withObject:aMesg waitUntilDone:NO];
    configureInProgress = FALSE;
	[SVProgressHUD dismiss];
#ifdef DEBUG
    self.userNameStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/redX.png"];
    self.passwordStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/redX.png"];
#endif

    [anOptions cancel];
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

#ifdef DEBUG
-(IBAction) gmailActiveSyncClicked
{
    [self.hostnameField       setText:@"m.google.com"];
    [self.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconGmail.png"];
    [self.imageView           setImage:anImage];
    
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeGmail;    
}

-(IBAction)gmailFixmoClicked
{
    [self.hostnameField       setText:@"m.google.com"];
    [self.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconGmail.png"];
    [self.imageView           setImage:anImage];
    
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeGmail;

	if (self.userNameField.text.length == 0) {
		self.userNameField.text = @"XXXX@fixmo.com";
	} else {
		NSRange theRange = [self.userNameField.text rangeOfString:@"@"];
		if (theRange.length == 0) {
			self.userNameField.text = [self.userNameField.text stringByAppendingString:@"@fixmo.com"];
		}
	}

	return;
}

-(IBAction) exchangeClicked
{	
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconExchange.png"];
    [self.imageView setImage:anImage];
    
    [self.portField setText:[NSString stringWithFormat:@"%d", 443]];
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeExchange;   
}

-(IBAction) fixmoClickedStk08
{
    [self.hostnameField       setText:@"stk08-cas02.fixmo.corp"]; // FIXME for debug only, must remove
    [self.domainField         setText:@"etp"];
    [self.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconFixmo.png"];
    [self.imageView setImage:anImage];
    
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeExchange;    
}

-(IBAction) fixmoClickedStk09
{
    [self.hostnameField       setText:@"stk09.fixmo.com"]; // FIXME for debug only, must remove
    [self.domainField         setText:@"szdev.local"];
    [self.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconFixmo.png"];
    [self.imageView setImage:anImage];
    
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeExchange;
}

-(IBAction) fixmoClickedStk01
{
#ifdef DEBUG
    [self.nameField           setText:@"Matthew Dominici"];
    [self.userNameField       setText:@"matthew.dominici"];
    [self.passwordField       setText:@"FIXmo123"];
#endif
    [self.hostnameField       setText:@"stk01-dom01.fixmo.corp/servlet/traveler"];
    [self.portField           setText:[NSString stringWithFormat:@"%d", 443]];
    
    UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconFixmo.png"];
    [self.imageView setImage:anImage];
    
    self.account    = nil;
	self.newAccount = YES;
    self.accountSubType = AccountSubTypeDomino;
}
#endif

-(void)configureLaterClicked:(id)sender
{
	if (self.configurationDelegate) {
		[self.configurationDelegate configurationWasCancelled:nil];
	} else {
		[self closeConfigurationView];
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}
}

-(BOOL)accountExists:(NSString*)anEmailAddress server:(NSString*)aServer 
{
    NSArray* anAccounts = [BaseAccount accounts];
    NSUInteger aCount = [anAccounts count];
    for(NSUInteger i = 0 ; i < aCount ; ++i) {
        BaseAccount* anAccount = [anAccounts objectAtIndex:i];
		if(anAccount.deleted) {
			continue;
		}
		
		if([anAccount.hostName isEqualToString:aServer] 
        && [anAccount.userName isEqualToString:anEmailAddress]) {
			return YES;
		}
	}
	
	return NO;
}

-(void)updateReachability:(Reachability*)aReachability
{
    NetworkStatus aHostStatus = aReachability.currentReachabilityStatus;
    switch(aHostStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
        {
            if(aHostStatus == ReachableViaWWAN) {
                FXDebugLog(kFXLogActiveSync, @"Server reachable via mobile: %@", self.hostName);
            }else{
                FXDebugLog(kFXLogActiveSync, @"Server reachable via WiFi: %@", self.hostName);
            }
            isReachableHost = TRUE;
#ifdef DEBUG
            self.hostStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/greenCheck.png"];
#endif
            break;
        }
        case NotReachable:
            FXDebugLog(kFXLogActiveSync, @"Server not reachable: %@", self.hostName);
#ifdef DEBUG
            self.hostStatusImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/redX.png"];
#endif
            break;
    }
}

-(BOOL)isReachableHost:(NSString*)aHostName
{
    // Validate hostname
    // FIXME should I do other validity tests here?
    //
    if(aHostName.length > 3) {
		if(![aHostName isEqualToString:self.hostName]) {
			
			observingReachability = TRUE;
			self.hostName = aHostName;
			self.reachability = [Reachability reachabilityWithHostname:self.hostName];
			[self.reachability startNotifier];
			[[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification
															  object:self.reachability queue:nil
														  usingBlock:^(NSNotification *inNotification) {
															  [self updateReachability:inNotification.object];
														  }
			 ];
			
			[self updateReachability:self.reachability];
		}
    }else{
        // Invalid host name
        //
        if(observingReachability) {
            [self removeReachabilityNotifier];
            isReachableHost = FALSE;
            self.hostStatusImageView.image = nil;
        }
    }
    return isReachableHost;
}

-(void)notReachableHost
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"SERVER_NOT_REACHABLE", @"ASConfigViewController", @"Alert view mesage title when host server is not reachable")
                                                          message:self.hostName
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL", @"ASConfigViewController", @"Alert view cancel button title when host server is not reachable")
                                                otherButtonTitles:FXLLocalizedStringFromTable(@"RETRY", @"ASConfigViewController", @"Alert view retry button title when host server is not reachable"), nil];
    int aButtonIndex = [anAlertView showModal];
    switch(aButtonIndex) {
        case 0:
        default:
            // Cancel
            break;
        case 1:
        {
            // Retry
            self.reachability = [Reachability reachabilityWithHostname:self.hostName];
            [self loginClick];
        }
    }
}

-(IBAction)loginClick
{
	[self accountConfigurationClicked:nil];
}

-(void)accountConfigurationClicked:(id)sender
{
    self.account    = nil;
    
    self.newAccount = YES;

	if(![self validateFields]) {
        return;
    }
	
    if(configureInProgress) {
        return;
    }else{
        configureInProgress = TRUE;
    }
    
	[self backgroundClick]; // to deactivate all keyboards
	
#if showServerFields
    server = [self parseHostName];
#else
	server = [[FXLSafeZone secureStandardUserDefaults] stringForKey:kSZCGuardServerSettingsDataHostKey];
	port = [[[FXLSafeZone secureStandardUserDefaults] stringForKey:kSZCGuardServerSettingsDataPortKey] intValue];
#endif
    
    NSString* aUserName = [self.userNameField text];
	
	// check if account already exists
    //
	if(self.newAccount) {
		if([self accountExists:aUserName server:server]) {
			UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"ACCOUNT_EXISTS", @"ASConfigViewController", @"Alert view title when attempting to set up new account and account already exists") message:FXLLocalizedStringFromTable(@"ALREADY_AN_ACCOUNT_FOR_THIS_EMAIL_ADDRESS/SERVER_COMBINATION", @"ASConfigViewController", @"Alert view message when attempting to set up new account and account already exists") delegate:self cancelButtonTitle:FXLLocalizedStringFromTable(@"OK_ACCOUNT_EXISTS", @"ASConfigViewController", @"Alert view cancel button title when attempting to set up new account and account already exists") otherButtonTitles: nil];
			[alertView show];
			return;
		}
	}
    
    // Vertify host name is a reachable host
    //
	if([self isReachableHost:server]) {
        // Host is reachable, start account provisioning
        //
        [self progressMessage:FXLLocalizedStringFromTable(@"VALIDATING_LOGIN", @"ASConfigViewController", @"Progress view message while host is validating login")];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(progressMessageDidChange:)
													 name:kProgressViewStringDidChangeNotification object:nil];
		
		[SVProgressHUD showWithStatus:FXLLocalizedStringFromTable(@"VALIDATING_LOGIN", @"ASConfigViewController", @"Progress view message while host is validating login") maskType:SVProgressHUDMaskTypeGradient];
        
        NSThread *driverThread = [[NSThread alloc] initWithTarget:self selector:@selector(doLogin:) object:[NSNumber numberWithBool:NO]];
        [driverThread start];
    }else{
        configureInProgress = FALSE;
        [self notReachableHost];
    }
}

- (void)resignResponders
{
    [self.nameField         resignFirstResponder];
	[self.userNameField     resignFirstResponder];
	[self.passwordField     resignFirstResponder];
	[self.hostnameField     resignFirstResponder];
    [self.domainField       resignFirstResponder];
	[self.portField         resignFirstResponder];
}

-(IBAction)backgroundClick 
{
    [self resignResponders];
}

-(IBAction)selectFoldersClicked 
{
	[self.selectFoldersButton setEnabled:NO];
    [self resignResponders];
	
	NSThread *driverThread = [[NSThread alloc] initWithTarget:self selector:@selector(doLogin:) object:[NSNumber numberWithBool:YES]];
	[driverThread start];
}

#pragma mark - UIKeyboardNotifications
// Called when the UIKeyboardWillHideNotification is sent
// From: http://karthik-prabhu.blogspot.ca/2012/09/scrollup-content-when-uitextfield.html
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if (!IS_IPAD())
    {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        self.scrollView.contentOffset = CGPointZero;
    }
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if (!IS_IPAD())
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
     
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        self.scrollView.contentInset = contentInsets;
        self.scrollView.scrollIndicatorInsets = contentInsets;
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        
        aRect.size.height -= kbSize.height+(activeField.frame.size.height*2); // add textfield height when the UITextField slightly outside from keyboard view
        
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) )
        {
            CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y- kbSize.height);
            [self.scrollView setContentOffset:scrollPoint animated:YES];
        }
        self.scrollView.contentSize = self.view.frame.size;
    }
}


@end
