/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASSettings.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASOutOfOffice.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASSettings

@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"Settings";
static const ASTag kCommandTag  = SETTINGS_SETTINGS;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASSettings*)sendGetUserInfo:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd = kSettingsCmdGetUserInfo;
    aSettings.displayErrors = aDisplayErrors;
    [aSettings send];
    return aSettings;
}

+ (ASSettings*)queueGetUserInfo:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd = kSettingsCmdGetUserInfo;
    aSettings.displayErrors = aDisplayErrors;
    [aSettings queue];
    return aSettings;
}

+ (ASSettings*)sendGetOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd = kSettingsCmdGetOOF;
    aSettings.displayErrors = aDisplayErrors;
    [aSettings send];
    return aSettings;
}

+ (ASSettings*)queueGetOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd = kSettingsCmdGetOOF;
    aSettings.displayErrors = aDisplayErrors;
    [aSettings queue];
    return aSettings;
}

// Set Out Of Office - Global
//
+ (ASSettings*)sendSetOutOfOffice:(ASAccount*)anAccount
                         internal:(ASOutOfOffice*)anOofInternal
                    externalKnown:(ASOutOfOffice*)anOofExternalKnown
                  externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                    displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdSetOOF;
    aSettings.oofState              = kSettingsOOFStateGlobal;
    aSettings.oofInternal           = anOofInternal;
    aSettings.oofExternalKnown      = anOofExternalKnown;
    aSettings.oofExternalUnknown    = anOofExternalUnknown;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings send];
    return aSettings;
}

+ (ASSettings*)queueSetOutOfOffice:(ASAccount*)anAccount
                          internal:(ASOutOfOffice*)anOofInternal
                     externalKnown:(ASOutOfOffice*)anOofExternalKnown
                   externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                     displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdSetOOF;
    aSettings.oofState              = kSettingsOOFStateGlobal;
    aSettings.oofInternal           = anOofInternal;
    aSettings.oofExternalKnown      = anOofExternalKnown;
    aSettings.oofExternalUnknown    = anOofExternalUnknown;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings queue];
    return aSettings;
}

// Set Out Of Office - Time based
//
+ (ASSettings*)sendSetOutOfOfficeForTime:(ASAccount*)anAccount
                                   start:(NSDate*)aStartDate
                                     end:(NSDate*)anEndDate
                                internal:(ASOutOfOffice*)anOofInternal
                           externalKnown:(ASOutOfOffice*)anOofExternalKnown
                         externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                           displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdSetOOF;
    aSettings.oofState              = kSettingsOOFStateTimeBased;
    aSettings.startDate             = aStartDate;
    aSettings.endDate               = anEndDate;
    aSettings.oofInternal           = anOofInternal;
    aSettings.oofExternalKnown      = anOofExternalKnown;
    aSettings.oofExternalUnknown    = anOofExternalUnknown;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings send];
    return aSettings;
}

+ (ASSettings*)queueSetOutOfOfficeForTime:(ASAccount*)anAccount
                                    start:(NSDate*)aStartDate
                                      end:(NSDate*)anEndDate
                                 internal:(ASOutOfOffice*)anOofInternal
                            externalKnown:(ASOutOfOffice*)anOofExternalKnown
                          externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                            displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdSetOOF;
    aSettings.oofState              = kSettingsOOFStateTimeBased;
    aSettings.startDate             = aStartDate;
    aSettings.endDate               = anEndDate;
    aSettings.oofInternal           = anOofInternal;
    aSettings.oofExternalKnown      = anOofExternalKnown;
    aSettings.oofExternalUnknown    = anOofExternalUnknown;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings queue];
    return aSettings;
}

// Disable Out Of Office
//
+ (ASSettings*)sendDisableOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdDisableOOF;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings send];
    return aSettings;
}

+ (ASSettings*)queueDisableOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors
{
    ASSettings* aSettings = [[ASSettings alloc] initWithAccount:anAccount];
    aSettings.cmd                   = kSettingsCmdDisableOOF;
    aSettings.displayErrors         = aDisplayErrors;
    [aSettings queue];
    return aSettings;
}

- (void)send
{
    NSData* aWbxml = nil;
    switch(self.cmd) {
        case kSettingsCmdGetUserInfo:
            aWbxml = [self wbxmlGetUserInfo:self.account];
            break;
        case kSettingsCmdGetOOF:
            aWbxml = [self wbxmlGetOOF:self.account];
            break;
        case kSettingsCmdSetOOF:
            aWbxml = [self wbxmlSetOOF:self.account];
            break;
        case kSettingsCmdDisableOOF:
            aWbxml = [self wbxmlDisableOOF:self.account];
            break;

    }
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlGetUserInfo:(ASAccount*)anAccount
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SETTINGS_USER_INFORMATION); {
            wbxml.tag(SETTINGS_GET);
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlGetOOF:(ASAccount*)anAccount
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SETTINGS_OOF); {
            wbxml.start(SETTINGS_GET); {
                wbxml.keyValue(SETTINGS_BODY_TYPE, @"Text");
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlSetOOF:(ASAccount*)anAccount
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SETTINGS_OOF); {
            wbxml.start(SETTINGS_SET); {
                wbxml.keyValueInt(SETTINGS_OOF_STATE, self.oofState);
                if(self.startDate) {
                     wbxml.keyValue(SETTINGS_START_TIME, [[ASEmail dateFormatter] stringFromDate:self.startDate]);
                }
                if(self.endDate) {
                    wbxml.keyValue(SETTINGS_END_TIME, [[ASEmail dateFormatter] stringFromDate:self.endDate]);
                }

                if(self.oofInternal) {
                    wbxml.start(SETTINGS_OOF_MESSAGE); {
                        wbxml.tag(SETTINGS_APPLIES_TO_INTERNAL);
                        wbxml.keyValueInt(SETTINGS_ENABLED, self.oofInternal.enabled);
                        if(self.oofInternal.enabled) {
                            wbxml.keyValue(SETTINGS_REPLY_MESSAGE, self.oofInternal.message);
                            wbxml.keyValue(SETTINGS_BODY_TYPE, self.oofInternal.bodyType);
                        }
                    }wbxml.end();
                }
                if(self.oofExternalKnown) {
                    wbxml.start(SETTINGS_OOF_MESSAGE); {
                        wbxml.tag(SETTINGS_APPLIES_TO_EXTERNAL_KNOWN);
                        wbxml.keyValueInt(SETTINGS_ENABLED, self.oofExternalKnown.enabled);
                        if(self.oofExternalKnown.enabled) {
                            wbxml.keyValue(SETTINGS_REPLY_MESSAGE, self.oofExternalKnown.message);
                            wbxml.keyValue(SETTINGS_BODY_TYPE, self.oofExternalKnown.bodyType);
                        }
                    }wbxml.end();
                }
                if(self.oofExternalUnknown) {
                    wbxml.start(SETTINGS_OOF_MESSAGE); {
                        wbxml.tag(SETTINGS_APPLIES_TO_EXTERNAL_UNKNOWN);
                        wbxml.keyValueInt(SETTINGS_ENABLED, self.oofExternalUnknown.enabled);
                        if(self.oofExternalUnknown.enabled) {
                            wbxml.keyValue(SETTINGS_REPLY_MESSAGE, self.oofExternalUnknown.message);
                            wbxml.keyValue(SETTINGS_BODY_TYPE, self.oofExternalUnknown.bodyType);
                        }
                    }wbxml.end();
                }
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlDisableOOF:(ASAccount*)anAccount
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SETTINGS_OOF); {
            wbxml.start(SETTINGS_SET); {
                wbxml.keyValueInt(SETTINGS_OOF_STATE, kSettingsOOFStateDisabled);
                wbxml.start(SETTINGS_OOF_MESSAGE); {
                    wbxml.tag(SETTINGS_APPLIES_TO_INTERNAL);
                    wbxml.keyValueInt(SETTINGS_ENABLED, 0);
                }wbxml.end();
                wbxml.start(SETTINGS_OOF_MESSAGE); {
                    wbxml.tag(SETTINGS_APPLIES_TO_EXTERNAL_KNOWN);
                    wbxml.keyValueInt(SETTINGS_ENABLED, 0);
                }wbxml.end();
                wbxml.start(SETTINGS_OOF_MESSAGE); {
                    wbxml.tag(SETTINGS_APPLIES_TO_EXTERNAL_UNKNOWN);
                    wbxml.keyValueInt(SETTINGS_ENABLED, 0);
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kSettingsSuccess:
            break;
#ifdef DEBUG
        case kSettingsProtocolError:
            [HttpRequest handleASError:@"Settings protocol error"]; break;
        case kSettingsAccessDenied:
            [HttpRequest handleASError:@"Settings access denied"]; break;
        case kSettingsServerUnavailable:
            [HttpRequest handleASError:@"Settings server unavailable"]; break;
        case kSettingsInvalidArguments:
            [HttpRequest handleASError:@"Settings invalid arguments"]; break;
        case kSettingsConflictingArguments:
            [HttpRequest handleASError:@"Settings conflicting arguments"]; break;
        case kSettingsDeniedByPolicy:
            [HttpRequest handleASError:@"Settings denied by policy"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
           [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"SETTINGS_ERROR", @"ErrorAlert", @"Error alert message for Settings error"), self.statusCodeAS]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)_userInformationParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case SETTINGS_STATUS:
                self.statusCodeAS = (ESettingsStatus)[aParser getInt];
                break;
            case SETTINGS_GET:
                break;
            case SETTINGS_ACCOUNTS:
                break;
            case SETTINGS_ACCOUNT:
                break;
            case SETTINGS_EMAIL_ADDRESSES:
                break;
            case SETTINGS_SMTP_ADDRESS:
                if (!self.account.emailAddress && ![self.account.emailAddress isEqualToString:@""])
                {
                    [self.account storeEmailAddress:[aParser getString]];
                    FXDebugLog(kFXLogActiveSync, @"SETTINGS_SMTP_ADDRESS: %@", self.account.emailAddress);
                }
                else
                {
                    FXDebugLog(kFXLogActiveSync, @"Skipping SETTINGS_SMTP_ADDRESS");

                }
                break;
            case SETTINGS_PRIMARY_SMTP_ADDRESS:
                [self.account storeEmailAddress:[aParser getString]];
                FXDebugLog(kFXLogActiveSync, @"SETTINGS_PRIMARY_SMTP_ADDRESS: %@", self.account.emailAddress);
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)_oofParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    ASOutOfOffice* anOof = nil;
    
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case SETTINGS_STATUS:
                self.statusCodeAS = (ESettingsStatus)[aParser getInt];
                break;
            case SETTINGS_GET:
                break;
            case SETTINGS_OOF_STATE:
                self.oofState = (ESettingsOOFState)[aParser getInt];
                break;
            case SETTINGS_OOF_MESSAGE:
                break;
            case SETTINGS_START_TIME:
                self.startDate = [[ASEmail dateFormatter] dateFromString:[aParser getString]];
                break;
            case SETTINGS_END_TIME:
                self.endDate = [[ASEmail dateFormatter] dateFromString:[aParser getString]];
                break;
            case SETTINGS_BODY_TYPE:
                anOof.bodyType = [aParser getString];
                break;
            case SETTINGS_REPLY_MESSAGE:
                anOof.message = [aParser getString];
                break;
            case SETTINGS_ENABLED:
                anOof.enabled = [aParser getBool];
                break;
            case SETTINGS_APPLIES_TO_INTERNAL:
                anOof = self.oofInternal = [[ASOutOfOffice alloc] init];
                break;
            case SETTINGS_APPLIES_TO_EXTERNAL_KNOWN:
                anOof = self.oofExternalKnown = [[ASOutOfOffice alloc] init];
                break;
            case SETTINGS_APPLIES_TO_EXTERNAL_UNKNOWN:
                anOof = self.oofExternalUnknown = [[ASOutOfOffice alloc] init];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case SETTINGS_STATUS:
                    self.statusCodeAS = (ESettingsStatus)[aParser getInt];
                    break;
                case SETTINGS_USER_INFORMATION:
                    [self _userInformationParser:aParser tag:tag];
                    break;
                case SETTINGS_OOF:
                    [self _oofParser:aParser tag:tag];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        
        if(self.statusCodeAS == kSettingsSuccess) {
            //if(self.delegate) {
                //[self.delegate settingsSuccess:self];
            //}
            [self success];
        }else{
            //if(self.delegate) {
                //[self.delegate settingsFailed:self];
            //}
            [self displayError];    // FIXME temporary
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Settings response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end