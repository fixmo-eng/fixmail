/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpRequest.h"

typedef enum {
    kSyncStatusUnknown                      = 0,
    kSyncStatusOK                           = 1,
    kSyncStatusInvalidSyncKey               = 3,
    kSyncStatusProtocolError                = 4,
    kSyncStatusServerError                  = 5,
    kSyncStatusConversionError              = 6,
    kSyncStatusConflict                     = 7,
    kSyncStatusObjectNotFound               = 8,
    kSyncStatusCommandNotComplete           = 9,
    kSyncStatusFolderHierarchyChanged       = 12,
    kSyncStatusIncompleteCommand            = 13,
    kSyncStatusInvalidWaitOrHeartbeat       = 14,
    kSyncStatusInvalidCommand               = 15,
    kSyncStatusRetry                        = 16
} ESyncStatus;

@protocol ASSyncDelegate <NSObject>

@required
- (void)syncSuccess:(NSArray*)aSyncFolders isPinging:(BOOL)isPinging;
- (void)syncFailed:(NSArray*)aSyncFolders
        httpStatus:(EHttpStatusCode)aStatusCode
        syncStatus:(ESyncStatus)aSyncStatus;

@optional
- (void)syncMoreAvailable:(NSArray*)aMoreAvailableFolders pingFolders:(NSArray*)aPingFolders;
- (void)syncNotUpdating:(NSArray*)aSyncFolders;

@end
