/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"
#import "ASMoveItemsDelegate.h"

@class ASFolder;

typedef enum {
    kMoveItemsInvalidSourceID                   = 1,
    kMoveItemsInvalidDestinationID              = 2,
    kMoveItemsSuccess                           = 3,
    kMoveItemsSameSourceAndDestination          = 4,
    kMoveItemsLockedOrMoreThanOneDestination    = 5,
    kMoveItemsLocked                            = 7
} EMoveItemsStatus;

@interface ASMoveItems : WBXMLRequest {
    NSObject<ASMoveItemsDelegate>*     delegate;
}

@property (nonatomic,strong)    NSArray*                        emails;
@property (nonatomic,strong)    ASFolder*                       toFolder;
@property (nonatomic,strong)    NSObject<ASMoveItemsDelegate>*  delegate;
@property (nonatomic)           EMoveItemsStatus                statusCodeAS;


// Factory
+ (ASMoveItems*)sendMove:(ASAccount*)anAccount
                  emails:(NSArray*)anEmails
                toFolder:(ASFolder*)aFolder
           displayErrors:(BOOL)aDisplayErrors;

+ (ASMoveItems*)queueMove:(ASAccount*)anAccount
                   emails:(NSArray*)anEmails
                 toFolder:(ASFolder*)aFolder
            displayErrors:(BOOL)aDisplayErrors;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end