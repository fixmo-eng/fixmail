/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncObject.h"
#import "ASSyncDelegate.h"

@class ASEmail;
@class ASFolder;

#define kReadStatusSyncedNotification       @"kReadStatusSyncedNotification"
#define kReadStatusSyncFailedNotification   @"kReadStatusSyncFailedNotification"
#define kReadStatusEmails                   @"kReadStatusEmails"
#define kReadStatusStatusCode               @"kReadStatusStatusCode"

@interface ASSyncReadStatus : ASSyncObject <ASSyncDelegate> {
}

@property (nonatomic,strong) ASFolder*  folder;
@property (nonatomic,strong) NSArray*  emails;

- (id)initWithFolder:(ASFolder*)aFolder email:(ASEmail*)anEmail;
- (id)initWithFolder:(ASFolder*)aFolder emails:(NSArray*)anEmail;

- (void)send;

@end