//
//  DateUtil.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 3/17/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import <Foundation/Foundation.h>


@interface DateUtil : NSObject {

}

@property (nonatomic, strong) NSDate *today;
@property (nonatomic, strong) NSDate *yesterday;
@property (nonatomic, strong) NSDate *lastWeek;
@property (nonatomic, strong) NSDateComponents* todayComponents;
@property (nonatomic, strong) NSDateComponents* yesterdayComponents;
@property (nonatomic,readonly) NSCalendar* currentCalendar;

+(id)getSingleton;
-(BOOL)is24HourClock;
-(NSString *)amSymbol;
-(NSString *)pmSymbol;
-(NSString*)shortDate:(NSDate*)date;

-(NSString*)humanDate:(NSDate*)date;
+(NSDate *) datetimeInLocal:(NSDate *)utcDate;
-(NSDate *) dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime;

-(NSDate *) dateFromString24HourSystemHourAndMinuteOnly:(NSString *)time;
+(NSDate*) dateFromString:(NSString*)date withDateFormat:(NSString*)dateFormat;

-(NSString*) stringFromDateTimeOnlyShort:(NSDate*)date;
-(NSString*) stringFromDate24HourSystemHourAndMinuteOnly:(NSDate*)date;
-(NSString*) stringFromDate12HourSystemHourAndAMOrPMSymbolOnly:(NSDate*)date;
-(NSString*) stringFromDateDayOfWeekOnly:(NSDate*)date;
-(NSString*) stringFromDateShortDayOfWeekOnly:(NSDate*)date;
-(NSString*) stringFromDateOnlyMedium:(NSDate*)date;
-(NSString*) stringFromDateMonthAndDayOnly:(NSDate *)date;
-(NSString*) stringFromDateMonthAndYear:(NSDate *)date;
-(NSString*) stringFromDateMonthOnly:(NSDate *)date;
-(NSString*) stringFromDateYearOnly:(NSDate *)date;
-(NSString*) stringFromDateWeekdayAndDateLongStyle:(NSDate *)date;
-(NSString*) stringFromDateWeekdayAndDateTimeShortStyle:(NSDate *)date;
-(NSString*) rfcStringWithDate:(NSDate *)date;
-(NSArray *) arrayOfStringsShortWeekdaySymbols;
+(NSString*) stringFromDate:(NSDate*)date withDateFormat:(NSString*)dateFormat;

@end
