//
//  FXDebugLog.m
//  FixMail
//
//  Created by Colin Biggin on 2013-01-23.
//
#import "FXDebugLog.h"

#ifdef DEBUG

static long long sFlags = kFlags;

void FXSetFlags(long long inFlags)
{
	sFlags = inFlags;
}

long long FXGetFlags(void)
{
	return sFlags;
}

void FXDebugLog(long long flags, NSString *format, ...)
{
	// to print any bit that matches
#if defined(kAnyBitSet)
	if (!(flags & sFlags)) return;
#elif defined(kAllBitsSet)
	if ((flags & sFlags) != flags) return;
#else
	return;
#endif

    va_list ap;
    va_start (ap, format);
    if (![format hasSuffix: @"\n"]) {
      format = [format stringByAppendingString: @"\n"];
    }
    NSString *body =  [[NSString alloc] initWithFormat: format arguments: ap];
    va_end (ap);
    fprintf(stderr,"%s",[body UTF8String]);
}

#endif

