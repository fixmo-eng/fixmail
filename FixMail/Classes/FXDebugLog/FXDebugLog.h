//
//  FXDebugLog.h
//  FixMail
//
//  Created by Colin Biggin on 2013-01-23.
//
// Description:
// FXDebugLog is to be used in place of NSLog, DebugLog, or ALog, or DLog
//
// Essentially, we can categorize our calls and selectively turn on/off the printing
// of the message to the console. We do this by defining bits for these categories:
//
// For example:
//		#define kFXLogSettings				0x00010000
//		#define kFXLogContacts				0x00020000
//		#define kFXLogCalendar				0x00040000
//		#define kFXLogNotes					0x00080000
//		#define kFXLogActiveSync			0x00100000
//
// So to log some info on Contacts, we would just have:
//
//		FXDebugLog(kFXLogContacts, @"Here is a contact: %@", theContact.name);
//
// And to enable the printing of these messages to the console, we would define kFlags
// as following:
//					#define kFlags (kFXLogContacts)
//
// You can also set multiple categories on logging:
//
//		FXDebugLog(kFXLogContacts | kFXLogActiveSync, @"Here is a contact: %@", theContact.name);
//
// So you can selectively log just contact stuff, or just Active Sync.
//
// There are two other flags that you can set below: kAnyBitSet and kAllBitsSet
// If you want any bit that is And'ed to log, then define kAnyBitSet
// If you want all bits And'ed to be set, then define kAllBitsSet
//
// So, a little bit better description, if you wanted ONLY Contacts AND ActiveSync log messages
// to appear, then you should define the following:
//
//		#define kFlags			(kFXLogContacts | kFXLogActiveSync)
//		#define kAllBitsSet		1
//		// and kAnyBitSet is undefined

#ifdef DEBUG
#define kFlags          kFXLogAll
//#define kFlags		(kFXLogContacts)
//#define kFlags		(kFXLogVCLoadUnload	 | kFXLogVCAppearDisappear | kFXLogVCDealloc)
//#define kFlags        (kFXLogActiveSync | kFXLogWBXML)
//#define kFlags        (kFXLogSZCAttachmentView | kFXLogAttachmentManager)
#else
#define kFlags          0
#endif

#define kAnyBitSet		1
//#define kAllBitsSet	1

