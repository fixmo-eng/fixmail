 /*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASFolderCreate.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASFolderCreateDelegate.h"
#import "ASPing.h"
#import "WBXMLDataGenerator.h"

@implementation ASFolderCreate

// Properties
@synthesize folderType;
@synthesize name;
@synthesize parentID;
@synthesize delegate;
@synthesize folder;
@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"FolderCreate";
static const ASTag kCommandTag  = FOLDER_FOLDER_CREATE;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASFolderCreate*)queueFolderCreate:(ASAccount*)anAccount
                          folderType:(EFolderType)aFolderType
                                name:(NSString*)aName
                            parentID:(NSString*)aParentID
                       displayErrors:(BOOL)aDisplayErrors
{
    ASFolderCreate* aFolderCreate = [[ASFolderCreate alloc] initWithAccount:anAccount];
    aFolderCreate.folderType    = aFolderType;
    aFolderCreate.name          = aName;
    aFolderCreate.parentID      = aParentID;
    aFolderCreate.displayErrors = aDisplayErrors;
    [aFolderCreate queue];
    
    return aFolderCreate;
}

- (void)send
{
    NSData* aWbxml = [self wbxmlFolderCreate:self.account folderType:self.folderType name:self.name parentID:self.parentID];
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlFolderCreate:(ASAccount*)anAccount
                  folderType:(EFolderType)aFolderType
                        name:(NSString*)aName
                    parentID:(NSString*)aParentID
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.keyValue(FOLDER_SYNC_KEY, anAccount.syncKey);
        wbxml.keyValue(FOLDER_PARENT_ID, aParentID);
        wbxml.keyValue(FOLDER_DISPLAY_NAME, aName);
        wbxml.keyValueInt(FOLDER_TYPE, aFolderType);
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kFolderCreateSuccess:
            break;
#ifdef DEBUG
        case kFolderCreateFolderExists:
            [HttpRequest handleASError:@"Folder create folder exists"]; break;
        case kFolderCreateFolderIsSpecial:
            [HttpRequest handleASError:@"Folder create folder is special"]; break;
        case kFolderCreateParentNotFound:
            [HttpRequest handleASError:@"Folder create parent not found"]; break;
        case kFolderCreateServerError:
            [HttpRequest handleASError:@"Folder create server error"]; break;
        case kFolderCreateSyncKeyInvalid:
            [HttpRequest handleASError:@"Folder create sync key invalid"]; break;
        case kFolderCreateMalformedRequest:
            [HttpRequest handleASError:@"Folder create malformed request"]; break;
        case kFolderCreateUnknownError:
            [HttpRequest handleASError:@"Folder create unknown error"]; break;
        case kFolderCreateUnusualBackendIssue:
            [HttpRequest handleASError:@"Folder create unusual backend issue"]; break;
        default:
            [super handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"FOLDER_CREATE_ERROR", @"ErrorAlert", @"Error alert message for Folder Create error"), self.statusCodeAS]];
            break;
			
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {        
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case FOLDER_STATUS:
                    self.statusCodeAS = (EFolderCreateStatus)[aParser getInt];
                    if(self.statusCodeAS == kFolderCreateSuccess) {
                        ASFolder* aFolder = (ASFolder*)[self.account createFolderForFolderType:self.folderType];
                        aFolder.folderType  = self.folderType;
                        aFolder.displayName = self.name;
                        aFolder.parentId    = self.parentID;
                        self.folder = aFolder;
                    }else{
                        if(self.delegate) {
                            [self.delegate folderCreateFailed:self];
                        }else{
                            [self displayError];
                        }
                    }
                    break;
                case FOLDER_SYNC_KEY:
                    self.account.syncKey = [aParser getStringTraceable];
                    [self.account commitSyncKey];
                    break;
                case FOLDER_SERVER_ID:
                    self.folder.uid = [aParser getStringTraceable];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        ASFolder* aFolder = self.folder;
        if(self.statusCodeAS == kFolderCreateSuccess && aFolder) {
            [self.account addFolder:aFolder];
            [self.account setupFolder:aFolder];
            if(self.delegate) {
                [self.delegate folderCreated:aFolder];
            }
            [self success];
        }else{
            if(self.delegate) {
                [self.delegate folderCreateFailed:self];
            }
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Folder create response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end