/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASEmailChange.h"
#import "ASEmail.h"

@implementation ASEmailChange

// Properties
@synthesize uid;
@synthesize email;

// Flags transient
static const NSUInteger kReadChanged        = 0x00000001;
static const NSUInteger kReadState          = 0x00000002;

static const NSUInteger kFlagChanged        = 0x00000004;
static const NSUInteger kFlagState          = 0x00000008;

static const NSUInteger kCategoryChanged    = 0x00000010;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithUid:(NSString*)aUid
{
    if (self = [super init]) {
        self.uid = aUid;
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////
// Flags Transient
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Transient

- (BOOL)readChanged
{
	return (flags & kReadChanged) != 0;
}

- (BOOL)readState
{
	return (flags & kReadState) != 0;
}

- (void)setReadState:(BOOL)state
{
    flags |= kReadChanged;
	if(state)	flags |= kReadState;
	else		flags &= ~kReadState;
}

- (BOOL)flagChanged
{
	return (flags & kFlagChanged) != 0;
}

- (BOOL)flagState
{
	return (flags & kFlagState) != 0;
}

- (void)setFlagState:(BOOL)state
{
    flags |= kFlagChanged;
	if(state)	flags |= kFlagState;
	else		flags &= ~kFlagState;
}

- (BOOL)categoryChanged
{
	return (flags & kCategoryChanged) != 0;
}

- (void)setCategoryChanged:(BOOL)state
{
	if(state)	flags |= kCategoryChanged;
	else		flags &= ~kCategoryChanged;
}

@end
