/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASEmailThread.h"

#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation ASEmailThread

// Constants
static NSString* kUid                 = @"uid";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (id)initWithJSON:(NSString*)aJSON
{
    if (self = [super init]) {
        NSDictionary* aDictionary = [aJSON JSONValue];
                
        self.uid           = [aDictionary objectForKey:kUid];

    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// JSON
////////////////////////////////////////////////////////////////////////////////
#pragma mark JSON

- (NSString*)asJSON
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:6];
    
    if(self.uid.length > 0) {
        [aDictionary setObject:self.uid        forKey:kUid];
    }

    return[aDictionary JSONRepresentation];
}

@end
