/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "AccountDelegate.h"
#import "SZCComposeDelegate.h"

@class FolderSelectController;
@protocol FolderSelectDelegate <NSObject>

-(void)folderViewController:(FolderSelectController*)controller didSelectFolder:(BaseFolder*)folder;

@end

@class BaseAccount;

@interface FolderSelectController : UITableViewController <AccountDelegate>

@property (nonatomic,strong) BaseFolder			*currentFolder;
@property (nonatomic,strong) NSMutableArray		*mainFolders;
@property (nonatomic,strong) NSMutableArray		*secondaryFolders;
@property (nonatomic,assign) id <FolderSelectDelegate> delegate;

@end
