//
//  FMSplitViewDelegate.m
//  FixMail
//
//  Created by Sean Langley on 11/14/12.
//  Based on Apple MultipleDetailViews iOS Sample
//  
//
//

#import "FMSplitViewDelegate.h"
#import "mailViewerVC.h"
#import "UIViewController+MGSplitViewController.h"
#import "SZLConcreteApplicationContainer.h"


@implementation FMSplitViewDelegate

-(void) setMasterViewController:(UIViewController *)masterViewController
{
    _masterViewController.mgSplitViewController = nil;
    _masterViewController = masterViewController;
    _masterViewController.mgSplitViewController = self.splitViewController;

}

- (void)setDetailViewController:(UIViewController*)detailViewController
{
    _detailViewController.mgSplitViewController = nil;
    _detailViewController = detailViewController;
    _detailViewController.mgSplitViewController = self.splitViewController;

    if (!self.detailNavController) {
        _detailNavController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
	}

    self.detailNavController.viewControllers = @[detailViewController];
    [self.detailNavController popToRootViewControllerAnimated:NO];
    
    NSArray *viewControllers = @[_masterViewController, _detailNavController];
    self.splitViewController.viewControllers = viewControllers;
        
    // Dismiss the navigation popover if one was present.  This will
    // only occur if the device is in portrait.
    if (self.navigationPopoverController) {
        [self.navigationPopoverController dismissPopoverAnimated:YES];
    }
	return;
}


- (BOOL)splitViewController:(MGSplitViewController *)svc
   shouldHideViewController:(UIViewController *)vc
              inOrientation:(UIInterfaceOrientation)orientation
{
    return UIInterfaceOrientationIsPortrait(orientation);
}


- (void)splitViewController:(MGSplitViewController *)svc
     willHideViewController:(UIViewController *)aViewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)pc
{
    // If the barButtonItem does not have a title (or image) adding it to a toolbar
    // will do nothing.
    barButtonItem.title = FXLLocalizedString(@"NAVIGATION", @"Navigation button title for split view delegate");

    _navigationPaneButtonItem = barButtonItem;
    _navigationPopoverController = pc;
    [self updateNavigationButton];
}

- (void)splitViewController:(MGSplitViewController *)svc
     willShowViewController:(UIViewController *)aViewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    _navigationPaneButtonItem = nil;
    _navigationPopoverController = nil;
    [self updateNavigationButton];
}

-(void) updateNavigationButton
{
    if ([self.detailViewController respondsToSelector:@selector(setSplitViewBarButtonItem:)]) {
        [self.detailViewController performSelector:@selector(setSplitViewBarButtonItem:) withObject:_navigationPaneButtonItem];
    }
}

@end
