/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpRequest.h"

@class ASFolder;

// Types
//
typedef enum {
    kFolderSyncStatusUnknown            = 0,
    kFolderSyncStatusOK                 = 1,
    kFolderSyncStatusExists             = 2,    // Obsolete?
    kFolderSyncStatusSpecialFolder      = 3,    // Obsolete?
    kFolderSyncStatusDoesNotExist       = 4,    // Obsolete?
    kFolderSyncStatusParentNotFound     = 5,    // Obsolete?
    kFolderSyncStatusServerError        = 6,
    kFolderSyncStatusInvalidKey         = 9,
    kFolderSyncStatusIncorrectFormat    = 10,
    kFolderSyncStatusUnknownError       = 11,
    kFolderSyncStatusCodeUnknown        = 12
} EFolderSyncStatus;


@protocol ASFolderSyncDelegate <NSObject>

- (void)needsFolderResync;

- (void)folderSyncSuccess:(NSMutableArray*)aFolders;
- (void)setFolderSyncKey:(NSString*)aSyncKey;
- (void)folderSyncFailed:(EHttpStatusCode)aStatusCode folderSyncStatus:(EFolderSyncStatus)aFolderSyncStatus;

- (void)folderAdded:(ASFolder*)aFolder;
- (void)folderDeleted:(NSString*)aServerId;
- (void)folderUpdated:(ASFolder*)aFolder;

@end
