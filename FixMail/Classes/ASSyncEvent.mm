/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncEvent.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "ASSync.h"
#import "Attendee.h"
#import "Calendar.h"
#import "Event.h"
#import "EventException.h"
#import "EventRecurrence.h"
#import "EventEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASSyncEvent

@synthesize event;
@synthesize delegate;

// Constants
static NSString* kCommand               = @"Sync";
static const ASTag kCommandTag          = SYNC_SYNC;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithEvent:(Event*)anEvent delegate:(NSObject<ASSyncEventDelegate>*)aDelegate isAdd:(BOOL)anIsAdd
{
    if (self = [super init]) {
        self.event      = anEvent;
        self.delegate   = aDelegate;
        isAdd           = anIsAdd;
    }
    return self;
}


- (void)send
{
    ASSync* aSync = nil;
    
    if(isAdd) {
        aSync = [self sendAddEvent:self.event delegate:self.delegate];
    }else{
        aSync = [self sendChangeEvent:self.event delegate:self.delegate];
    }
    
    if(aSync) {
        [super sendAndWait:aSync];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Event sync failed: %@", self);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
// WBXML Generators
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Generators

- (ASSync*)sendAddEvent:(Event*)anEvent delegate:(NSObject<ASSyncEventDelegate>*)anEventDelegate
{
    ASSync* aSync = nil;
    @try {
        ASCalendarFolder* aFolder = (ASCalendarFolder*)anEvent.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders       = [NSArray arrayWithObject:aFolder];
        aSync.object            = (BaseObject*)anEvent;
        aSync.eventDelegate     = anEventDelegate;
        
        NSData* aWbxml = [self wbxmlAddEvent:anEvent request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendAddEvent" exception:e];
    }
    return aSync;
}

- (void)recurrenceForEvent:(Event*)anEvent wbxml:(WBXMLDataGenerator&)wbxml
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];
    
    wbxml.start(CALENDAR_RECURRENCE); {
        wbxml.keyValueInt(CALENDAR_RECURRENCE_TYPE, anEvent.recurrence.recurrenceType);
        if(anEvent.recurrence.interval > 1) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_INTERVAL, anEvent.recurrence.interval);
        }
        
        if(anEvent.recurrence.firstDayOfWeek) {
            wbxml.keyValueInt(CALENDAR_FIRST_DAY_OF_WEEK, anEvent.recurrence.firstDayOfWeek);
        }
        if(anEvent.recurrence.dayOfWeek) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_DAYOFWEEK, anEvent.recurrence.dayOfWeek);
        }
        if(anEvent.recurrence.dayOfMonth) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_DAYOFMONTH, anEvent.recurrence.dayOfMonth);
        }
        if(anEvent.recurrence.weekOfMonth) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_WEEKOFMONTH, anEvent.recurrence.weekOfMonth);
        }
        if(anEvent.recurrence.monthOfYear) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_MONTHOFYEAR, anEvent.recurrence.monthOfYear);
        }
        
        // Expiration
        //
        if(anEvent.recurrence.occurences) {
            wbxml.keyValueInt(CALENDAR_RECURRENCE_OCCURRENCES, anEvent.recurrence.occurences);
        }
        if(anEvent.recurrence.untilDate) {
            wbxml.keyValue(CALENDAR_RECURRENCE_UNTIL, [aDateFormatter stringFromDate:anEvent.recurrence.untilDate]);
        }
        
        // Causes HTTP 400 bad request errors on Exchange 2007, reenable with caution if needed
        //
        //ASAccount* anAccount = (ASAccount*)self.event.folder.account;
        //if(anAccount.ASVersion >= kASVersion2010) {
           // wbxml.keyValueInt(CALENDAR_FIRST_DAY_OF_WEEK, 0);
        //}

        //wbxml.keyValueInt(CALENDAR_CALENDAR_TYPE, kCalendarTypeGregorian);
    }wbxml.end();
}

- (void)exceptionsForEvent:(Event*)anEvent wbxml:(WBXMLDataGenerator&)wbxml
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];
    
    NSArray* anExceptions = anEvent.recurrence.exceptions;
    if(anExceptions.count > 0) {
        wbxml.start(CALENDAR_EXCEPTIONS); {
            for(EventException* anException in anExceptions) {
                if(anException.exceptionIsDeleted) {
                    wbxml.keyValueInt(CALENDAR_EXCEPTION_IS_DELETED, anException.exceptionIsDeleted);
                }
                if(anException.exceptionStartTime) {
                    wbxml.keyValue(CALENDAR_EXCEPTION_START_TIME, [aDateFormatter stringFromDate:anException.exceptionStartTime]);
                }
                if(anException.startDate) {
                    wbxml.keyValue(CALENDAR_START_TIME, [aDateFormatter stringFromDate:anException.startDate]);
                }
                if(anException.endDate) {
                    wbxml.keyValue(CALENDAR_END_TIME, [aDateFormatter stringFromDate:anException.endDate]);
                }
            }
        }wbxml.end();
    }
}

- (void)wbxmlEvent:(Event*)anEvent wbxml:(WBXMLDataGenerator&)wbxml
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];
    
    wbxml.start(SYNC_APPLICATION_DATA); {
        
        // Title/Location
        //
        if(anEvent.title.length > 0) {
            wbxml.keyValue(CALENDAR_SUBJECT, anEvent.title);
        }
        if(anEvent.location.length > 0) {
            wbxml.keyValue(CALENDAR_LOCATION, anEvent.location);
        }
        
#warning FIXME Hardwired timezone element, replace with a generared time zone for the time zone of the event
        // Google Calendar and maybe AS 12.x requires this time zone element,  The AS 14.x docs say its optional
        //
        ASAccount* anAccount = (ASAccount*)self.event.folder.account;
        if(anAccount.ASVersion < kASVersion2007SP1) {
            wbxml.keyValue(CALENDAR_TIME_ZONE, @"pAEAAE0AUwBUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAsAAAABAAIAAAAAAAAAAAAAAE0ARABUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAACAAIAAAAAAAAAxP///w==");
        }
        
        // Time
        //
        wbxml.keyValue(CALENDAR_DTSTAMP,    [aDateFormatter stringFromDate:[NSDate date]]);
        wbxml.keyValue(CALENDAR_START_TIME, [aDateFormatter stringFromDate:anEvent.startDate]);
        wbxml.keyValue(CALENDAR_END_TIME,   [aDateFormatter stringFromDate:anEvent.endDate]);
        
        // Notes
        //
        if(anEvent.notes.length > 0) {
            wbxml.start(BASE_BODY); {
                wbxml.keyValueInt(BASE_TYPE, kBodyTypePlainText);
                wbxml.keyValue(BASE_DATA, anEvent.notes);
            }wbxml.end();
        }
        
        // Options
        //
        wbxml.keyValueInt(CALENDAR_SENSITIVITY, anEvent.sensitivity);
        wbxml.keyValueInt(CALENDAR_BUSY_STATUS, anEvent.busyStatus);
        wbxml.keyValueInt(CALENDAR_ALL_DAY_EVENT, [anEvent isAllDay]);
        
        // Organizer
        if(anEvent.organizerEmail.length > 0) {
            wbxml.keyValue(CALENDAR_ORGANIZER_EMAIL, anEvent.organizerEmail);
        }
        
        if(anEvent.organizerName.length > 0) {
            wbxml.keyValue(CALENDAR_ORGANIZER_NAME, anEvent.organizerName);
        }
      

        // Reminder
        //
        if(anEvent.reminderMinutes > 0) {
            wbxml.keyValueInt(CALENDAR_REMINDER, anEvent.reminderMinutes);
        }
        
        // Recurrence
        //
        if(anEvent.recurrence > 0) {
            [self recurrenceForEvent:anEvent wbxml:wbxml];
        }
     
        // Recurrence exceptions
        //
        if(anEvent.recurrence.exceptions.count > 0) {
            [self exceptionsForEvent:anEvent wbxml:wbxml];
        }
    
        // Meeting
        //
        if(anEvent.attendees.count > 0) {
            wbxml.start(CALENDAR_ATTENDEES); {
                for(Attendee* anAttendee in anEvent.attendees) {
                    wbxml.start(CALENDAR_ATTENDEE); {
                        wbxml.keyValue(CALENDAR_ATTENDEE_NAME,      anAttendee.asNameString);
                        wbxml.keyValue(CALENDAR_ATTENDEE_EMAIL,     anAttendee.address);
                        wbxml.keyValueInt(CALENDAR_ATTENDEE_STATUS, anAttendee.attendeeStatus);
                        wbxml.keyValueInt(CALENDAR_ATTENDEE_TYPE,   anAttendee.attendeeType);
                    }wbxml.end();
                }
            }wbxml.end();
        }
    }wbxml.end();
}

- (NSData*)wbxmlAddEvent:(Event*)anEvent request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASCalendarFolder* aFolder = (ASCalendarFolder*)anEvent.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.keyValue(SYNC_GET_CHANGES, @"1");
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_ADD); {
                        ASAccount* anAccount = (ASAccount*)aFolder.account;
                        wbxml.keyValue(SYNC_CLIENT_ID, [anAccount getClientID]);
                        [self wbxmlEvent:anEvent wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

- (NSData*)wbxmlChangeEvent:(Event*)anEvent request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASCalendarFolder* aFolder = (ASCalendarFolder*)anEvent.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                //wbxml.keyValue(SYNC_CLASS, @"Calendar");
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_CHANGE); {
                        wbxml.keyValue(SYNC_SERVER_ID, [anEvent uid]);
                        [self wbxmlEvent:anEvent wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

- (ASSync*)sendChangeEvent:(Event*)anEvent delegate:(NSObject<ASSyncEventDelegate>*)anEventDelegate
{
    ASSync* aSync = nil;
    @try {
        ASCalendarFolder* aFolder = (ASCalendarFolder*)anEvent.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders       = [NSArray arrayWithObject:aFolder];
        aSync.isChangeRequest   = TRUE;
        aSync.object            = (BaseObject*)anEvent;
        aSync.eventDelegate     = anEventDelegate;
        NSData* aWbxml = [self wbxmlChangeEvent:anEvent request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendChangeEvent" exception:e];
    }
    return aSync;
}

@end