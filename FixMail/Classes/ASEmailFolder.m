/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASEmailFolder.h"
#import "ASAccount.h"
#import "ASEmailChange.h"
#import "ASSync.h"
#import "ASSyncReadStatus.h"
#import "CommitObjects.h"
#import "EmailProcessor.h"
#import "SearchRunner.h"
#import "TraceEngine.h"
#import "MGSplitViewController.h"
#import "UIViewController+MGSplitViewController.h"

@implementation ASEmailFolder

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (ASEmail*)emailForUID:(NSString*)aUid
{
    ASEmail* anEmail = [dictionary objectForKey:aUid];
    if(anEmail) {
    }else{
        anEmail = [[SearchRunner getSingleton] emailForUid:aUid folder:self];
    }
    return anEmail;
}

////////////////////////////////////////////////////////////////////////////////
// Change Handlers
////////////////////////////////////////////////////////////////////////////////
#pragma mark Change Handlers

- (void)changeObject:(BaseObject*)aBaseObject
          changeType:(EChange)aChangeType
        updateServer:(BOOL)updateServer
      updateDatabase:(BOOL)updateDatabase
{
    @try {
        ASEmail* anEmail = (ASEmail*)aBaseObject;
        
        // Notify clients of change
        //
        //FXDebugLog(kFXLogActiveSync, @"folder %@ changeObject delegates=%d", self.displayName, delegates.count);
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(changeObject:) withObject:anEmail waitUntilDone:FALSE];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kFolderChangeObjectNotification
                                                            object:self
                                                          userInfo:[NSDictionary dictionaryWithObject:anEmail
                                                                                               forKey:kFolderObject]];
        switch(aChangeType) {
            case kChangeReadStatus:
                if(updateServer) {
                    if(self.account.isReachable) {
                        [[[ASSyncReadStatus alloc] initWithFolder:self  email:anEmail] queue];
                    }else{
                        anEmail.hasOfflineChange    = YES;
                        self.hasOfflineChanges      = YES;
                    }
                }
                if(updateDatabase) {
                    [[EmailProcessor getSingleton] queueUpdateFlags:anEmail];
                }
                break;
            case kChangeMeetingUserResponse:
                if(updateDatabase) {
                    [[EmailProcessor getSingleton] queueUpdateMeetingRequest:anEmail];
                }
                if(updateServer) {}
                break;
            default:
                break;
        }
    }@catch (NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
}

- (void)deleteUid:(NSString*)aUid updateServer:(BOOL)updateServer
{
    @try {
        // Notify clients of deletion
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(deleteUid:) withObject:aUid waitUntilDone:FALSE];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kFolderDeleteUidNotification
                                                            object:self
                                                          userInfo:[NSDictionary dictionaryWithObject:aUid
																							   forKey:kFolderObjectUid]];
        
        if(!objectsDeleted) {
            objectsDeleted = [[NSMutableArray alloc] init];
        }
        [objectsDeleted addObject:aUid];

        if(updateServer) {
            [ASSync queueDeleteUid:aUid folder:self];
        }
    }@catch (NSException* e) {
        [self logException:@"deleteUid" exception:e];
    }
}
    
////////////////////////////////////////////////////////////////////////////////
// ASFolder Parser Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolder Parser Overrides

- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType
{
    @try {
        ASEmail* anEmail;
        if(anObject) {
            anEmail = (ASEmail*)anObject;
        }else{
            anEmail = [[ASEmail alloc] initWithFolder:self]; 
        }
        anEmail.bodyType = aBodyType;
        if(aTag != SYNC_FETCH) {
            [anEmail setIsNew:TRUE];
        }
        
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [anEmail setUid:[aParser getStringTraceable]];
                    break;
                case SYNC_APPLICATION_DATA:
                    [anEmail parser:aParser tag:tag];
                    break;
                case SYNC_STATUS:
                    [ASFolder displaySyncStatus:(ESyncStatus)[aParser getIntTraceable]];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        
        if(aTag == SYNC_FETCH) {
            [[EmailProcessor getSingleton] queueUpdateBodyAndFlags:anEmail];
        }else if(anEmail.uid.length > 0) {
            [self addObject:anEmail];
        }
        
    }@catch (NSException* e) {
        [self logException:@"Email Add" exception:e];
    }
}

- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [self deleteUid:[aParser getStringTraceable] updateServer:FALSE];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Email Delete" exception:e];
    }
}

- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        if(!self.objectsChanged) {
            self.objectsChanged = [[NSMutableArray alloc] initWithCapacity:20];
        }
        ASEmailChange* aChange = nil;
        
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                {
                    NSString* aUid = [aParser getStringTraceable];
                    aChange = [[ASEmailChange alloc] initWithUid:aUid];
                    [self.objectsChanged addObject:aChange]; 
                    break;
                }
                case SYNC_APPLICATION_DATA:
                    break;
                case EMAIL_READ:
                    [aChange setReadState:[aParser getBool]];
                    break;
                case EMAIL_CATEGORIES:
                    break;
                case EMAIL_CATEGORY:
#warning FIXME changeParser EMAIL_CATEGORY
                    //[aChange setCategoryChanged:TRUE];
                    break;
                case EMAIL_FLAG:
                {
#warning FIXME changeParser EMAIL_FLAG
                    // Note this is a temp object for the parser, need to either get the
                    // existing ASEmail object or load it from database by UID
                    // or at least merge the temp with the existing object and figure out
                    // what changed
                    //
                    //ASEmail* anEmail = [[ASEmail alloc] initWithFolder:self];
                    //[anEmail flagParser:aParser tag:tag];
                    //[aChange setFlagState:TRUE];
                    break;
                }
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Sync Change" exception:e];
    }

}

////////////////////////////////////////////////////////////////////////////////
// BaseFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark BaseFolder Overrides

- (DBaseEngine*)databaseEngine
{
    return [EmailProcessor getSingleton];
}

- (void)commitComplete:(CommitObjects*)aCommitObjects
{
    [super folderCommitCompleted];
}

- (void)commitObjects
{
    if(self.objectsAdded.count > 0 || self.objectsChanged.count > 0 || self.objectsDeleted.count > 0) {
        @try {
            CommitObjects* aCommitObjects = [[CommitObjects alloc] initWithFolder:self];
        
            // New objects added
            //
            if([self.objectsAdded count] > 0) {
                NSArray* anObjects = aCommitObjects.objectsAdded = self.objectsAdded;
                self.objectsAdded = [[NSMutableArray alloc] initWithCapacity:30]; // FIXME capacity should match sync window size?
                
                if(![self isInitialSync]) {
                    // Store new objects on the folder
                    //
                    for(ASEmail* anEmail in anObjects) {
                        if(anEmail.uid.length > 0) {
                            [dictionary setObject:anEmail forKey:anEmail.uid];
                        }else{
                            FXDebugLog(kFXLogActiveSync, @"commitObjects email invalid: %@", anEmail);
                        }
                    }
                }else{
                    [super commitWithCallback:aCommitObjects action:@selector(commitComplete:)];
                }

                //
                // And send them to delegate, calendar folder for meetings and .ics, mailbox, etc.
                //
                if(self.isInitialSynced || !self.isSyncable) {
                    for(NSObject<FolderDelegate>* delegate in delegates) {
                        [delegate newObjects:anObjects];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFolderNewObjectsNotification
                                                                        object:self
                                                                      userInfo:[NSDictionary dictionaryWithObject:anObjects
                                                                                                           forKey:kFolderObjects]];
                }
            }
            
            // Objects Changed
            //
            if(self.objectsChanged.count > 0) {
                aCommitObjects.objectsChanged = self.objectsChanged;
                self.objectsChanged = nil;
            }
            
            // Objects Deleted
            //
            if([self.objectsDeleted count] > 0) {
                aCommitObjects.objectsDeleted = self.objectsDeleted;
                self.objectsDeleted = nil;
            }
            
            [[self databaseEngine] queueCommitObjects:aCommitObjects];
        }@catch (NSException* e) {
            [self logException:@"commitObjects" exception:e];
        }
    }
}

- (void)commitChanges:(NSArray*)anObjects
{
    // Get the email for the UID, may be cached, may have to load from the database
    //
    for(ASEmailChange* aChange in anObjects) {
        aChange.email = [self emailForUID:aChange.uid];
    }
    
    // Make the changes
    //
    for(ASEmailChange* aChange in anObjects) {
        ASEmail* anEmail = aChange.email;
        if(anEmail) {
            BOOL anUpdateDatabase = FALSE;
            if([aChange readChanged]) {
                BOOL isRead = [aChange readState];
                if([anEmail isRead] != isRead) {
                    [anEmail setIsRead:isRead];
                    anUpdateDatabase = TRUE;
                }
            }
            if([aChange flagChanged]) {
#warning FIXME changeParser EMAIL_FLAG
                FXDebugLog(kFXLogFIXME, @"FIXME change EMAIL_FLAG");
            }
            if([aChange categoryChanged]) {
#warning FIXME changeParser EMAIL_CATEGORY
                FXDebugLog(kFXLogFIXME, @"FIXME change EMAIL_CATEGORY");
            }
            [self changeObject:anEmail
                    changeType:kChangeReadStatus
                  updateServer:FALSE
                updateDatabase:anUpdateDatabase];
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME changeParser no email for change");
        }
    }
}

- (void)deleteDatabaseForFolder
{
#warning FIXME deleteDatabaseForFolder
    FXDebugLog(kFXLogFIXME, @"FIXME deleteDatabaseForFolder: %@", self.displayName);
}

- (void)syncOfflineChanges
{
    if(!self.isSyncingOfflineChanges) {
        self.isSyncingOfflineChanges = YES;
        EmailSearch* anEmailSearch = [[EmailSearch alloc] initWithFolder:self delegate:self];
        [[SearchRunner getSingleton] offlineChangesSearch:anEmailSearch];
    }
}

//////////////////////////////////////////////////////////////////////////////
// SearchManagerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark SearchManagerDelegate

- (BOOL)shouldDeliverObject:(NSString*)aUid
{
    return YES;
}

- (void)deliverSearchResults:(NSArray *)aResults
{
    ASSyncReadStatus* aSyncReadStatus = [[ASSyncReadStatus alloc] initWithFolder:self emails:aResults];
    [[NSNotificationCenter defaultCenter] addObserverForName:kReadStatusSyncedNotification object:aSyncReadStatus queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      NSDictionary* aUserInfo = inNotification.userInfo;
                                                      NSArray* anEmails = [aUserInfo objectForKey:kReadStatusEmails];
                                                      for(ASEmail* anEmail in anEmails) {
                                                          anEmail.hasOfflineChange = FALSE;
                                                      }
                                                      [[EmailProcessor getSingleton] queueUpdateFlagsForEmails:anEmails];
                                                      self.hasOfflineChanges        = NO;
                                                      self.isSyncingOfflineChanges  = NO;
                                                      [self commit];
                                                      [[NSNotificationCenter defaultCenter] removeObserver:self name:kReadStatusSyncedNotification object:inNotification.object];
                                                      [[NSNotificationCenter defaultCenter] removeObserver:self name:kReadStatusSyncFailedNotification object:inNotification.object];
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:kReadStatusSyncFailedNotification object:aSyncReadStatus queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      self.isSyncingOfflineChanges  = NO;
#warning FIXME what to do if sync fails
                                                      [[NSNotificationCenter defaultCenter] removeObserver:self name:kReadStatusSyncedNotification object:inNotification.object];
                                                      [[NSNotificationCenter defaultCenter] removeObserver:self name:kReadStatusSyncFailedNotification object:inNotification.object];
                                                  }];
    [aSyncReadStatus queue];
}

- (void) deliverAdditionalResults:(NSNumber *)availableResults
{
}

@end
