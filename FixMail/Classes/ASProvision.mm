/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASProvision.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "HttpEngine.h"
#import "ASSettings.h"
#import "WBXMLDataGenerator.h"

static const unsigned int kMaxUnsignedInt       = 0xffffffff;

@implementation ASProvision

// Properties
@synthesize provisionActionType;
@synthesize policyKey;
@synthesize provisionPolicy;

// Constants
static NSString* kCommand                       = @"Provision";
static const ASTag kCommandTag                  = PROVISION_PROVISION;
static NSString* kProvisionType                 = @"MS-EAS-Provisioning-WBXML";
static NSString* kProvisionStatusOKString       = @"1";
//static NSString* kProvisionStatusPartial        = @"2";

typedef enum {
    kProvisionStatusOK                  = 1,
    kProvisionProtcolError              = 2,
    kProvisionServerError               = 3,
    kProvisionClientCantComply          = 139,
    kProvisionDeviceNotProvisionable    = 141,
    kProvisionClientExternallyManaged   = 145,
    kProvisionDeviceInformationRequired = 165,
    kProvisionMaximumDevicesReached     = 177
} EProvisionStatus;

typedef enum {
    kPolicyStatusOK                     = 1,
    kPolicyNotDefined                   = 2,
    kPolicyTypeUnknown                  = 3,
    kPolicyDataCorrupt                  = 4,
    kPolicyKeyMismatch                  = 5
} EPolicyStatus;


////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendRequest:(ASAccount*)anAccount 
    provisionAction:(EProvisionActionType)aProvisionAction 
          policyKey:(NSString*)aPolicyKey
    provisionPolicy:(ASProvisionPolicy*)aProvisionPolicy
{
    ASProvision* anASProvision = [[ASProvision alloc] initWithAccount:anAccount 
                                                   provisionActionType:aProvisionAction
                                                       provisionPolicy:aProvisionPolicy];
    NSData* aWbxml = NULL;
    switch(aProvisionAction) {
        case kProvisionRequestPolicy:
            aWbxml = [anASProvision wbxmlProvisionPolicy]; 
            break;
        case kProvisionPolicyAcknowledge:
            if([aPolicyKey length] > 0) {
                aWbxml = [anASProvision wbxmlPolicyAcknowledge:aPolicyKey]; 
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME %@ policyKey invalid", kCommand); break;
            }
            break;
        default:
            FXDebugLog(kFXLogFIXME, @"FIXME %@ sendRequest", kCommand); break;
    }
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:anASProvision];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlProvisionPolicy
{
	WBXMLDataGenerator wbxml;
    
    ASAccount* anAccount = self.account;
	wbxml.start(kCommandTag); {
        if(anAccount.ASVersion >= kASVersion2010) {
            wbxml.start(SETTINGS_DEVICE_INFORMATION); {
                wbxml.start(SETTINGS_SET); {
                    wbxml.keyValue(SETTINGS_MODEL, anAccount.deviceModel);
                    wbxml.keyValue(SETTINGS_IMEI, anAccount.deviceID);        // FIXME this is a random UUID, not the IMEI
                    wbxml.keyValue(SETTINGS_FRIENDLY_NAME, @"FriendlyName");// FIXME
                    wbxml.keyValue(SETTINGS_OS, @"IOS");                    // FIXME
                    wbxml.keyValue(SETTINGS_USER_AGENT, anAccount.userAgent);
                    wbxml.keyValueInt(SETTINGS_ENABLE_OUTBOUND_SMS, 0);
                    wbxml.keyValue(SETTINGS_MOBILE_OPERATOR, @"MobileOperator"); // FIXME
                }wbxml.end();
            }wbxml.end();
        }
        wbxml.start(PROVISION_POLICIES); {
            wbxml.start(PROVISION_POLICY); {
                wbxml.keyValue(PROVISION_POLICY_TYPE, kProvisionType);
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);;
}

- (NSData*)wbxmlPolicyAcknowledge:(NSString*)aPolicyKey
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(PROVISION_POLICIES); {
            wbxml.start(PROVISION_POLICY); {
                wbxml.keyValue(PROVISION_POLICY_TYPE, kProvisionType);
                wbxml.keyValue(PROVISION_POLICY_KEY, aPolicyKey);
                
                // This says client will fully comply with policy
                wbxml.keyValue(PROVISION_STATUS, kProvisionStatusOKString);
                
                // This says client will only partially comply with policy
                //wbxml.keyValue(PROVISION_STATUS, kProvisionStatusPartial);

            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount 
  provisionActionType:(EProvisionActionType)aProvisionActionType
      provisionPolicy:(ASProvisionPolicy*)aProvisionPolicy
{
    if (self = [super initWithAccount:anAccount]) {
        policyKey           = NULL;
        provisionActionType = aProvisionActionType;
        [self setProvisionPolicy:aProvisionPolicy];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setProvisionStatus:(EProvisionStatus)aProvisionStatus
{
    switch(aProvisionStatus) {
        case kProvisionStatusOK:
            break;
#ifdef DEBUG
        case kProvisionProtcolError:
            [HttpRequest handleASError:@"Provision protocol error"]; break;
        case kProvisionServerError:
            [HttpRequest handleASError:@"Provision server error"]; break;
        case kProvisionClientCantComply:
            [HttpRequest handleASError:@"Provision client can not comply"]; break;
        case kProvisionDeviceNotProvisionable:
            [HttpRequest handleASError:@"Provision device not provisionable"]; break;
        case kProvisionClientExternallyManaged:
            [HttpRequest handleASError:@"Provision client externally managed"]; break;
        case kProvisionDeviceInformationRequired:
            [HttpRequest handleASError:@"Provision device information required"]; break;
        case kProvisionMaximumDevicesReached:
            [HttpRequest handleASError:@"Provision too many devices partnered, delete some from server"]; break;
        default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"Provision status unknown: %d", aProvisionStatus]]; break;
            break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"PROVISIONING_ERROR", @"ErrorAlert", @"Error alert message when provisioning error occurred"), aProvisionStatus]]; break;
            break;
#endif
    }
}

- (void)setPolicyStatus:(EPolicyStatus)aPolicyStatus
{
    switch(aPolicyStatus) {
        case kPolicyStatusOK:
            break;
#ifdef DEBUG
        case kPolicyNotDefined:
            // Stop sending policy information.  No policy is implemented
            [HttpRequest handleASError:@"Provision policy not defined"]; break;
        case kPolicyTypeUnknown:
            // Issue a request with a value of "MS-EAS- Provisioning-WBXML" in the PolicyType
            [HttpRequest handleASError:@"Policy type unknown"]; break;
        case kPolicyDataCorrupt:
            // Server administrator intervention is required
            [HttpRequest handleASError:@"Policy data corrupt, call admin"]; break;
        case kPolicyKeyMismatch:
            // Issue a new Provision request to obtain a valid policy key
            [HttpRequest handleASError:@"Policy key mismatch"]; break;
        default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"Provision policy status unknown: %d", aPolicyStatus]]; break;
            break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"PROVISIONING_POLICY_ERROR", @"ErrorAlert", @"Error alert message when provisioning policy error occurred"), aPolicyStatus]]; break;
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)provisionDocParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:PROVISION_EAS_PROVISION_DOC]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case PROVISION_DEVICE_PASSWORD_ENABLED:
                devicePasswordEnabled = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_DEVICE_PASSWORD_ENABLED: %d", devicePasswordEnabled); break;
            case PROVISION_ALPHA_DEVICE_PASSWORD_ENABLED:
                alphanumericDevicePasswordRequired = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALPHA_DEVICE_PASSWORD_ENABLED: %d", alphanumericDevicePasswordRequired); break;
            case PROVISION_PASSWORD_RECOVERY_ENABLED:
                passwordRecoveryEnabled = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_PASSWORD_RECOVERY_ENABLED: %d", passwordRecoveryEnabled); break;

            case PROVISION_ATTACHMENTS_ENABLED:
                deviceEncryptionEnabled = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ATTACHMENTS_ENABLED: %d", [aParser getInt]); break;
            case PROVISION_ALLOW_SIMPLE_DEVICE_PASSWORD:
                allowSimpleDevicePassword = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_SIMPLE_DEVICE_PASSWORD: %d", allowSimpleDevicePassword); break;
            case PROVISION_DEVICE_PASSWORD_EXPIRATION:
            {
                BOOL anIsEmpty;
                devicePasswordExpiration = [aParser getIntIsEmpty:&anIsEmpty];
                if(anIsEmpty) {
                    devicePasswordExpiration = 0;
                }
                FXDebugLog(kFXLogActiveSync, @"PROVISION_DEVICE_PASSWORD_EXPIRATION: %d", devicePasswordExpiration); 
                break;
            }
            case PROVISION_DEVICE_PASSWORD_HISTORY:
                devicePasswordHistory = [aParser getUnsignedInt];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_DEVICE_PASSWORD_HISTORY: %d", devicePasswordHistory); break;
            case PROVISION_MIN_DEVICE_PASSWORD_LENGTH:
                minDevicePasswordLength = [aParser getUnsignedInt];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MIN_DEVICE_PASSWORD_LENGTH: %d", minDevicePasswordLength); break;
            case PROVISION_MAX_INACTIVITY_TIME_DEVICE_LOCK:
                maxInactivityTimeDeviceLock = [aParser getUnsignedInt];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_INACTIVITY_TIME_DEVICE_LOCK: %i", maxInactivityTimeDeviceLock); break;
            case PROVISION_MAX_DEVICE_PASSWORD_FAILED_ATTEMPTS:
                maxDevicePasswordFailedAttempts = [aParser getUnsignedInt];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_DEVICE_PASSWORD_FAILED_ATTEMPTS: %d", maxDevicePasswordFailedAttempts); break;
          
            case PROVISION_DEVICE_ENCRYPTION_ENABLED:
                deviceEncryptionEnabled = [aParser getBool];
                FXDebugLog(kFXLogActiveSync, @"PROVISION_DEVICE_ENCRYPTION_ENABLED: %d", deviceEncryptionEnabled); break;
            case PROVISION_MAX_ATTACHMENT_SIZE:
            {
                BOOL anIsEmpty;
                maxAttachmentSize = [aParser getUnsignedIntIsEmpty:&anIsEmpty];
                if(anIsEmpty) {
                    maxAttachmentSize = kMaxUnsignedInt;
                }
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_ATTACHMENT_SIZE: %d", maxAttachmentSize); 
                break;
            }
                
            // ActiveSync 12.1
            //
            case PROVISION_ALLOW_STORAGE_CARD:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MIN_DEVICE_PASSWORD_LENGTH: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_CAMERA:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_CAMERA: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_UNSIGNED_APPLICATIONS:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_UNSIGNED_APPLICATIONS: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_UNSIGNED_INSTALLATION_PACKAGES:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_UNSIGNED_INSTALLATION_PACKAGES: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_WIFI:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_WIFI: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_TEXT_MESSAGING:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_TEXT_MESSAGING: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_POP_IMAP_EMAIL:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_POP_IMAP_EMAIL: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_BLUETOOTH:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_BLUETOOTH: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_IRDA:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_IRDA: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_DESKTOP_SYNC:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_DESKTOP_SYNC: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_HTML_EMAIL:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_HTML_EMAIL: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_SMIME_ENCRYPTION_NEGOTIATION:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_SMIME_ENCRYPTION_NEGOTIATION: %d", [aParser getInt]); break;
            case PROVISION_ALLOW_SMIME_SOFT_CERTS:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_SMIME_SOFT_CERTS: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_BROWSER:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_BROWSER: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_CONSUMER_EMAIL:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_CONSUMER_EMAIL: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_REMOTE_DESKTOP:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_REMOTE_DESKTOP: %d", [aParser getBool]); break;
            case PROVISION_ALLOW_INTERNET_SHARING:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_ALLOW_INTERNET_SHARING: %d", [aParser getBool]); break;
                
            case PROVISION_REQUIRE_DEVICE_ENCRYPTION:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_DEVICE_ENCRYPTION: %d", [aParser getBool]); break;
            case PROVISION_REQUIRE_MANUAL_SYNC_WHEN_ROAMING:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_MANUAL_SYNC_WHEN_ROAMING: %d", [aParser getBool]); break;
            case PROVISION_REQUIRE_SIGNED_SMIME_MESSAGES:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_SIGNED_SMIME_MESSAGES: %d", [aParser getBool]); break;  
            case PROVISION_REQUIRE_ENCRYPTED_SMIME_MESSAGES:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_ENCRYPTED_SMIME_MESSAGES: %d", [aParser getBool]); break;  
            case PROVISION_REQUIRE_SIGNED_SMIME_ALGORITHM:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_SIGNED_SMIME_ALGORITHM: %d", [aParser getBool]); break;  
            case PROVISION_REQUIRE_ENCRYPTION_SMIME_ALGORITHM:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_REQUIRE_ENCRYPTION_SMIME_ALGORITHM: %d", [aParser getBool]); break; 

            case PROVISION_MIN_DEVICE_PASSWORD_COMPLEX_CHARS:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MIN_DEVICE_PASSWORD_COMPLEX_CHARS: %d", [aParser getInt]); break;
            case PROVISION_MAX_CALENDAR_AGE_FILTER:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_CALENDAR_AGE_FILTER: %d", [aParser getInt]); break;
            case PROVISION_MAX_EMAIL_AGE_FILTER:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_EMAIL_AGE_FILTER: %d", [aParser getInt]); break;
            case PROVISION_MAX_EMAIL_BODY_TRUNCATION_SIZE:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_EMAIL_BODY_TRUNCATION_SIZE: %d", [aParser getInt]); break;
            case PROVISION_MAX_EMAIL_HTML_BODY_TRUNCATION_SIZE:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_MAX_EMAIL_HTML_BODY_TRUNCATION_SIZE: %d", [aParser getInt]); break;  
            case PROVISION_UNAPPROVED_IN_ROM_APPLICATION_LIST:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_UNAPPROVED_IN_ROM_APPLICATION_LIST"); break;
            case PROVISION_APPROVED_APPLICATION_LIST:
                FXDebugLog(kFXLogActiveSync, @"PROVISION_APPROVED_APPLICATION_LIST"); break;
              
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)setSettingsStatus:(ESettingsStatus)aSettingsStatus
{
    switch(aSettingsStatus) {
        case kSettingsSuccess:
            break;
#ifdef DEBUG
        case kSettingsProtocolError:
            [HttpRequest handleASError:@"Settings protocol error"]; break;
        case kSettingsAccessDenied:
            [HttpRequest handleASError:@"Settings access denied"]; break;
        case kSettingsServerUnavailable:
            [HttpRequest handleASError:@"Settings server unavailable"]; break;
        case kSettingsInvalidArguments:
            [HttpRequest handleASError:@"Settings invalid arguments"]; break;
        case kSettingsConflictingArguments:
            [HttpRequest handleASError:@"Settings conflicting arguments"]; break;
        case kSettingsDeniedByPolicy:
            [HttpRequest handleASError:@"Settings denied by policy"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aSettingsStatus]; break;
#else
		default:
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"SETTINGS_ERROR", @"ErrorAlert", @"Error alert message for Settings error"), aSettingsStatus]];
            break;
#endif
    }
}

- (void)policyParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:PROVISION_POLICY]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case PROVISION_POLICY_TYPE:
            {
                // Must  be "MS-EAS-Provisioning-WBXML"
                //
                NSString* aPolicyType = [aParser getString];
                if(![aPolicyType isEqualToString:@"MS-EAS-Provisioning-WBXML"]) {
                    FXDebugLog(kFXLogActiveSync, @"Invalid policy type: %@", aPolicyType);
                }
                break;
            }
            case PROVISION_STATUS:
                [self setPolicyStatus:(EPolicyStatus)[aParser getInt]]; break;
            case PROVISION_POLICY_KEY:
                [self setPolicyKey:[aParser getString]]; break;
            case PROVISION_DATA:
                break;
            case PROVISION_EAS_PROVISION_DOC:
                [self provisionDocParser:aParser]; break;
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    [self setPolicyKey:@""];

    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case PROVISION_POLICIES:
                     break;
                case PROVISION_STATUS:
                    [self setProvisionStatus:(EProvisionStatus)[aParser getInt]];
                    break;
                case PROVISION_POLICY:
                    [self policyParser:aParser];
                    break;
                case SETTINGS_DEVICE_INFORMATION:
                    break;
                case SETTINGS_STATUS:
                    [self setSettingsStatus:(ESettingsStatus)[aParser getInt]];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        } 
    }@catch (NSException* e) {
        [HttpRequest logException:@"Provision response" exception:e];
    }

    if(provisionActionType == kProvisionRequestPolicy) {
        if([policyKey length] > 0) {
            [self.account setProvisionReceived:policyKey];
            [ASProvision sendRequest:self.account
                     provisionAction:kProvisionPolicyAcknowledge 
                           policyKey:[self.account policyKey]
                     provisionPolicy:provisionPolicy];
        }else{
            [self.account setProvisionFailed];
        }
    }else if(provisionActionType == kProvisionPolicyAcknowledge) {
        if([self.policyKey length] > 0) {
            [self.account setProvisionAcknowledged:self.policyKey];
        }else{
            [self.account setProvisionFailed];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
            
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", kCommand);
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
    [super connectionDidFinishLoading:connection];
}

@end
