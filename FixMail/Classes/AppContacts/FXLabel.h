//
//  FXLabel.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXLabel : UIView

@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *textString;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, assign) NSLineBreakMode lineBreakMode;

+ (CGFloat) measureString:(NSString *)inString font:(UIFont *)inFont width:(CGFloat)inWidth;

@end
