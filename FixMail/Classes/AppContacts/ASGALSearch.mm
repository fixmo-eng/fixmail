/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASGALSearch.h"
#import "ASAccount.h"
#import "ASContact.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"
#import "FXContactsHeader.h"
#import "FXDatabase.h"
#import "ASContact+FMDatabase.h"

@implementation ASGALSearch

@synthesize statusCodeAS;

// Constants
static NSString* kCommand	   = @"Search";
static const ASTag kCommandTag  = SEARCH_SEARCH;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendSearch:(ASAccount*)anAccount query:(NSString*)aQuery
{
	ASGALSearch* aSearch = [[ASGALSearch alloc] initWithAccount:anAccount];
	NSData* aWbxml = [aSearch wbxmlSearch:anAccount query:aQuery];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kGALSearchStartedNotification object:nil];

	NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
	[anAccount send:aURLRequest httpRequest:aSearch];

}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlSearch:(ASAccount*)anAccount query:(NSString*)aQuery
{
	WBXMLDataGenerator wbxml;
	
	wbxml.start(kCommandTag); {
		wbxml.start(SEARCH_STORE); {
			wbxml.keyValue(SEARCH_NAME, @"GAL");
			wbxml.keyValue(SEARCH_QUERY, aQuery);

			wbxml.start(SEARCH_OPTIONS); {
				wbxml.keyValue(SEARCH_RANGE, @"0-100");
				wbxml.tag(SEARCH_REBUILD_RESULTS);
				wbxml.tag(SEARCH_DEEP_TRAVERSAL);
			}wbxml.end();
		}wbxml.end();
	}wbxml.end();
	
	return wbxml.encodedData(self, kCommandTag);
}
	
////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
	if (self = [super initWithAccount:anAccount]) {
		_contactsAdded = 0;
	}
	return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setSettingsStatus:(ESearchStatus)aSearchStatus
{
    self.statusCodeAS = aSearchStatus;
    
    switch(aSearchStatus) {
        case kSearchSuccess:
            break;
#ifdef DEBUG
        case kSearchRequstInvalid:
            [HttpRequest handleASError:@"Search request invalid"]; break;
        case kSearchServerError:
            [HttpRequest handleASError:@"Search server error"]; break;
        case kSearchBadLink:
            [HttpRequest handleASError:@"Search bad link"]; break;
        case kSearchAccessDenied:
            [HttpRequest handleASError:@"Search access denied"]; break;
        case kSearchNotFound:
            [HttpRequest handleASError:@"Search not found"]; break;
        case kSearchConnectionFailed:
            [HttpRequest handleASError:@"Search connection failed"]; break;
        case kSearchTooComplex:
            [HttpRequest handleASError:@"Search too complex"]; break;
        case kSearchTimedOut:
            [HttpRequest handleASError:@"Search timed out"]; break;
        case kSearchFolderSyncRequired:
            [HttpRequest handleASError:@"Search folder sync required"]; break;
        case kSearchEndOfRetrivableRange:
            [HttpRequest handleASError:@"Search end of retrievable range"]; break;
        case kSearchAccessBlocked:
            [HttpRequest handleASError:@"Search access blocked"]; break;
        case kSearchCredentialsRequired:
            [HttpRequest handleASError:@"Search credentials required"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aSearchStatus]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"SEARCH_ERROR", @"ErrorAlert", @"Error alert message for Search error"), aSearchStatus]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

	
//GAL_DISPLAY_NAME
//GAL_PHONE	   
//GAL_OFFICE	  
//GAL_TITLE	   
//GAL_COMPANY	 
//GAL_ALIAS	   
//GAL_FIRST_NAME  
//GAL_LAST_NAME   
//GAL_HOME_PHONE  
//GAL_MOBILE_PHONE
//GAL_EMAIL_ADDRES

- (void)_propertiesParser:(FastWBXMLParser*)inParser
{
	NSAssert(inParser != nil, @"inParser != nil");

	ASContact *theContact = [[ASContact alloc] init];
	theContact.contactType = contactGAL;

	ASTag tag;
	while ((tag = [inParser nextTag:SEARCH_PROPERTIES]) != AS_END) {
		switch(tag) {
			case GAL_DISPLAY_NAME:
				FXDebugLog(kFXLogActiveSync, @"GAL_DISPLAY_NAME = %@", [inParser getString]);
				break;
			case GAL_ALIAS:
				FXDebugLog(kFXLogActiveSync, @"GAL_ALIAS = %@", [inParser getString]);
				break;
			case GAL_FIRST_NAME:
				theContact.firstName = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_FIRST_NAME = %@", theContact.firstName);
				break;
			case GAL_LAST_NAME:
				theContact.lastName = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_LAST_NAME = %@", theContact.lastName);
				break;
			case GAL_EMAIL_ADDRESS:
				theContact.emailAddress1 = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_EMAIL_ADDRESS = %@", theContact.emailAddress1);
			   break;
			case GAL_COMPANY:
 				theContact.company = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_COMPANY = %@", theContact.company);
				break;
			case GAL_TITLE:
				theContact.title = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_TITLE = %@", theContact.title);
				break;
			case GAL_PHONE:
				theContact.companyPhone1 = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_PHONE = %@", theContact.companyPhone1);
				break;
			 case GAL_HOME_PHONE:
				theContact.homePhone1 = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_HOME_PHONE = %@", theContact.homePhone1);
				break;
			 case GAL_MOBILE_PHONE:
				theContact.mobilePhone = [inParser getString];
				FXDebugLog(kFXLogActiveSync, @"GAL_MOBILE_PHONE = %@", theContact.mobilePhone);
				break;
		   default:
				[inParser skipTag]; break;
		}
	}

	// now add it to the GAL database
	_contactsAdded++;
	theContact.isFavorite = true;
	[theContact addToContactsDB:contactGAL];
}

- (void)fastParser:(FastWBXMLParser*)aParser
{
	// reset the gal database and count
	_contactsAdded = 0;
	[FXDatabase deleteContactList:contactGAL];

	bool errorOccurred = false;
	@try {
		ASTag tag;
		while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
			switch(tag) {
				case SEARCH_STATUS:
					[self setSettingsStatus:(ESearchStatus)[aParser getInt]];
					break;
				case SEARCH_RESPONSE:
					break;
				case SEARCH_STORE:
					break;
				case SEARCH_RESULT:
					break;
				case SEARCH_PROPERTIES:
					[self _propertiesParser:aParser];
					break;
				case SEARCH_RANGE:
					FXDebugLog(kFXLogActiveSync, @"SEARCH_RANGE = %@", [aParser getString]);
					break;
				case SEARCH_TOTAL:
					FXDebugLog(kFXLogActiveSync, @"SEARCH_TOTAL = %@", [aParser getString]);
					break;
				case SEARCH_LONG_ID:
					FXDebugLog(kFXLogActiveSync, @"SEARCH_LONG_ID = %@", [aParser getString]);
					break;
				case SYNC_CLASS:
					FXDebugLog(kFXLogActiveSync, @"SYNC_CLASS = %@", [aParser getString]);
					break;
				case SYNC_COLLECTION_ID:
					FXDebugLog(kFXLogActiveSync, @"SYNC_COLLECTION_ID = %@", [aParser getString]);
					break;
				default:
					[aParser skipTag]; break;
			}
		} 
	}

	@catch (NSException* e) {
		[HttpRequest logException:@"Search response" exception:e];
		errorOccurred = true;
	}

	@finally {
		if (errorOccurred) {
			[[NSNotificationCenter defaultCenter] postNotificationName:kGALSearchFinishedErrorNotification object:nil];
		} else if (_contactsAdded == 0) {
			[[NSNotificationCenter defaultCenter] postNotificationName:kGALSearchFinishedNoResultsNotification object:nil];
		} else {
			[[NSNotificationCenter defaultCenter] postNotificationName:kGALSearchFinishedResultsNotification object:nil];
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
	NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
	self.statusCode = (EHttpStatusCode)[resp statusCode];
	
	if(self.statusCode == kHttpOK) {
		NSDictionary* aHeaders = [resp allHeaderFields];
		//FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
		NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
		if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
			
		}else{
			FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", kCommand);
		}
	}else{
		[super handleHttpErrorForAccount:self.account connection:connection response:response];
	}
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
	NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
		[self parseWBXMLData:data command:kCommandTag];
	}else{
		FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
	}
	[super connectionDidFinishLoading:connection];
}

@end