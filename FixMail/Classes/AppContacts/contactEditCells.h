//
//  contactEditCell.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-09.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

@interface contactEditCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *editField;

- (void) applyEmailField;
- (void) applyPhoneField;
- (void) applyURLField;

+ (CGFloat) rowHeight;

@end

@interface contactEditAddressCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextField *streetField;
@property (nonatomic, strong) UITextField *cityField;
@property (nonatomic, strong) UITextField *stateField;
@property (nonatomic, strong) UITextField *postalCodeField;
@property (nonatomic, strong) UITextField *countryField;

+ (CGFloat) rowHeight;

@end
