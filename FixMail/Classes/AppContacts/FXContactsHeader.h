//
//  FXContactsHeader.h
//  FixMail
//
//  Created by Colin Biggin on 2012-11-27.
//
//

#ifndef FixMail_FXContactsHeader_h
#define FixMail_FXContactsHeader_h


//
// Notifications
//
#define kDeleteContactNotification					@"kDeleteContactNotification"
#define kSaveContactNotification					@"kSaveContactNotification"
#define kContactUpdatedFromServerNotification		@"kContactUpdatedFromServerNotification"
#define kContactAddedFromServerNotification			@"kContactAddedFromServerNotification"
#define kContactDeletedFromServerNotification		@"kContactDeletedFromServerNotification"

#define kGALSearchStartedNotification				@"kGALSearchStartedNotification"
#define kGALSearchFinishedResultsNotification		@"kGALSearchFinishedResultsNotification"
#define kGALSearchFinishedNoResultsNotification		@"kGALSearchFinishedNoResultsNotification"
#define kGALSearchFinishedErrorNotification			@"kGALSearchFinishedErrorNotification"

#define kUserInfoContact							@"kUserInfoContact"
#define kUserInfoContactServerID					@"kUserInfoContactServerID"

//
// Typedefs
//
typedef enum {

	contactUnknown = 0,

	// base types
	contactUser = 10,
	contactGAL,
	contactEmailRecipients,

	// possibly used for searching WITHIN the base types
	contactEmails,
	contactPhones,

	contactAll = 999

} tContactType;

#endif
