//
//  contactDescriptionCell.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import "contactDescriptionCell.h"
#import "FXLabel.h"

#import "UIView-ViewFrameGeometry.h"

#define kCellMinimumHeight		44.0
#define kCellHeightPadding		18.0

@implementation contactDescriptionCell

- (void) setTitleString:(NSString *)inString
{
	_titleString = inString;
	self.titleLabel.text = _titleString;
}

- (void) setDescriptionString:(NSString *)inString
{
	_descriptionString = inString;
	self.descriptionLabel.textString = _descriptionString;
	[self setNeedsLayout];
}

- (void) layoutSubviews
{
	[super layoutSubviews];

	self.titleLabel.left = 10.0;
	self.titleLabel.width = 70.0;

	self.descriptionLabel.left = 90.0;
	self.descriptionLabel.width = self.contentView.bounds.size.width - 90.0 - 10.0;
	self.descriptionLabel.top = 10.0;
	[self.descriptionLabel sizeToFit];
	[self.descriptionLabel setNeedsDisplay];
}

#pragma mark -

+ (CGFloat) rowHeight
{
	return kCellMinimumHeight;
}

+ (CGFloat) rowHeightWithString:(NSString *)inString font:(UIFont *)inFont landscape:(bool)inLandscape
{
	CGFloat theWidth = 470.0;
	if (IS_IPAD()) {
		theWidth = inLandscape ? 470.0 : 470.0;
	} else {
		theWidth = inLandscape ? 360.0 : 200.0;
	}
	CGFloat theHeight = [FXLabel measureString:inString font:inFont width:theWidth];
	theHeight += kCellHeightPadding;
	if (theHeight < kCellMinimumHeight) theHeight = kCellMinimumHeight;

//	NSLog([NSString stringWithFormat:@"rowHeightWithString: %@ [%d]", inString, (int)theHeight], @"");
	return theHeight;
}

@end
