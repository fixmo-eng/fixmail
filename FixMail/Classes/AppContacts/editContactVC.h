/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASContact.h"

/*

CustomCells:
- contactEditEmailCell
- contactEditPhoneCell
- contactEditAdressCell

TableHeader
 /------\
 |		|	First Name
 |		|	Last Name
 |		|	Company
 \------/

Section 0 - Phones
0: Mobile
1: Pager

Section 1 - Email
0: Email 1
1: Email 2
2: Email 3

Section 2 - Company Address Info
0: street, city, state, country postal code -> contactEditAdressCell
1: company phone 1
2: company phone 2
3: company fax

Section 3 - Home Address Info
0: street, city, state, country postal code -> contactEditAdressCell
1: Home phone 1
2: Home phone 2
3: Home fax

Section 4 - Other Address Info
0: street, city, state, country postal code -> contactEditAdressCell

Section 5 - Other info
0: url

TableFooter

 Notes:
 /--------------\
 |				|
 |				|
 |				|
 \--------------/

*/

@class editContactVC;

@protocol FXAEditContactVCDelegate <NSObject>

@required
- (void)editContactVC:(editContactVC*)editController didSaveContact:(ASContact*)contact;
- (void)editContactVC:(editContactVC*)editController didDeleteContact:(ASContact*)contact;
- (void)editContactVC:(editContactVC *)editController didFailAction:(ASContact *)contact withKey:(NSString*)key;

@end

@interface editContactVC : UITableViewController <UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) id<FXAEditContactVCDelegate> delegate;

// header view
@property (strong, nonatomic) IBOutlet UIImageView *headerView;
@property (strong, nonatomic) IBOutlet UIImageView *contactImage;
@property (strong, nonatomic) IBOutlet UITextField *firstNameField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameField;
@property (strong, nonatomic) IBOutlet UITextField *companyField;

// phones
@property (strong, nonatomic)  UITextField *mobileField;
@property (strong, nonatomic)  UITextField *pagerField;

// email
@property (strong, nonatomic)  UITextField *email1Field;
@property (strong, nonatomic)  UITextField *email2Field;
@property (strong, nonatomic)  UITextField *email3Field;

// company address
@property (strong, nonatomic)  UITextField *companyStreet;
@property (strong, nonatomic)  UITextField *companyCity;
@property (strong, nonatomic)  UITextField *companyState;
@property (strong, nonatomic)  UITextField *companyPostalCode;
@property (strong, nonatomic)  UITextField *companyCountry;
@property (strong, nonatomic)  UITextField *companyPhone1;
@property (strong, nonatomic)  UITextField *companyPhone2;
@property (strong, nonatomic)  UITextField *companyFax;

// home address
@property (strong, nonatomic)  UITextField *homeStreet;
@property (strong, nonatomic)  UITextField *homeCity;
@property (strong, nonatomic)  UITextField *homeState;
@property (strong, nonatomic)  UITextField *homePostalCode;
@property (strong, nonatomic)  UITextField *homeCountry;
@property (strong, nonatomic)  UITextField *homePhone1;
@property (strong, nonatomic)  UITextField *homePhone2;
@property (strong, nonatomic)  UITextField *homeFax;

// other address
@property (strong, nonatomic)  UITextField *otherStreet;
@property (strong, nonatomic)  UITextField *otherCity;
@property (strong, nonatomic)  UITextField *otherState;
@property (strong, nonatomic)  UITextField *otherPostalCode;
@property (strong, nonatomic)  UITextField *otherCountry;

// url
@property (strong, nonatomic) UITextField *webPage;

// footer view
@property (strong, nonatomic) IBOutlet UIImageView *footerView;
@property (strong, nonatomic) IBOutlet UITextView *notesTextView;
@property (strong, nonatomic) IBOutlet UIButton *deleteContactButton;

@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property (strong, nonatomic) UIBarButtonItem *saveButton;

- (IBAction) toggleDeleteContact:(id)inSender;

- (void) setContact:(ASContact *)inContact newContact:(BOOL)newContact;

@end
