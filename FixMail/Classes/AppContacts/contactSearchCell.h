//
//  contactSearchCell.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-08.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

@class ASContact;

@interface contactSearchCell : UITableViewCell
{
	ASContact *_contact;
}

@property (nonatomic, strong) ASContact *contact;

@property (nonatomic, strong) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *lastNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *criteriaLabel;
@property (nonatomic, strong) IBOutlet UILabel *matchedStringLabel;
@property (nonatomic, strong) IBOutlet UIImageView *characteristicImage;

+ (CGFloat) rowHeight;

@end
