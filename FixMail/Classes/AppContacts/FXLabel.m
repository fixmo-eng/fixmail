//
//  FXLabel.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//
#import "FXLabel.h"

#define kFXLabelFontSize		14.0
#define kFXLabelMinimumWidth	200.0
#define kFXLabelMaximumWidth	200.0
#define kFXLabelMinimumHeight	26.0
#define kFXLabelMaximumHeight	2000.0

@implementation FXLabel

- (void) initDefaults
{
	self.backgroundColor = [UIColor clearColor];
	self.font = [UIFont boldSystemFontOfSize:kFXLabelFontSize];
	self.textColor = [UIColor blackColor];
	self.lineBreakMode = NSLineBreakByCharWrapping;
}

- (id) initWithCoder:(NSCoder *)inDecoder
{
	self = [super initWithCoder:inDecoder];
	if (self) {
		[self initDefaults];
	}
	return self;
}

- (id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		[self initDefaults];
    }
    return self;
}

- (void) drawRect:(CGRect)inRect
{
	[self.textColor set];
	[self.textString drawInRect:inRect withFont:self.font lineBreakMode:self.lineBreakMode];
}

- (CGSize) sizeThatFits:(CGSize)inSize
{
	// always give a size that varies the height, but not the width
	CGSize maxSize = CGSizeMake(self.frame.size.width, kFXLabelMaximumHeight);
	CGSize newSize = [self.textString sizeWithFont:self.font constrainedToSize:maxSize lineBreakMode:self.lineBreakMode];
	if (newSize.width < self.frame.size.width) newSize.width = self.frame.size.width;
	if (newSize.height < self.frame.size.height) newSize.height = self.frame.size.height;

	return newSize;
}

#pragma mark -

+ (CGFloat) measureString:(NSString *)inString font:(UIFont *)inFont width:(CGFloat)inWidth
{
	if ([inString length] == 0) return kFXLabelMinimumHeight;

	UIFont *theFont = inFont;
	if (theFont == nil) theFont = [UIFont systemFontOfSize:kFXLabelFontSize];
	
	CGSize maxSize = CGSizeMake(inWidth, kFXLabelMaximumHeight);
	CGSize newSize = [inString sizeWithFont:theFont constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
	//	CGSize newSize = [inString sizeWithFont:theFont constrainedToSize:maxSize];
	
	if (newSize.height < kFXLabelMinimumHeight) newSize.height = kFXLabelMinimumHeight;
	
//	NSLog([NSString stringWithFormat:@"measureString: %@ [%d]", inString, (int)newSize.height], @"");
	return newSize.height;
}

@end
