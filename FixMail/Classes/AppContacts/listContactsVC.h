/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASSyncContactDelegate.h"
#import "FXMSearchTimerManager.h"

@class ASSyncContact;
@class showContactVC;
@class MGSplitViewController;

@interface listContactsVC : UITableViewController <UIActionSheetDelegate, FXMSearchTimerManagerDelegate>

@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSMutableArray *syncRequests;
@property (strong, nonatomic) NSIndexPath *savedIndexPath;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) UIBarButtonItem *addButtonItem;

@property (strong, nonatomic) showContactVC *detailViewController;
@property (strong, nonatomic) MGSplitViewController *splitViewController;

@end
