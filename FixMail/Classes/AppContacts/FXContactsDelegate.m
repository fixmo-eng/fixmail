//
//  FXContactsDelegate.m
//  FixMail
//
//  Created by Colin Biggin on 2012-12-05.
//
//

#import "FXContactsDelegate.h"
#import "ASAccount.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
#import "FXContactsHeader.h"
#import "listContactsVC.h"
#import "showContactVC.h"
#import "ASConfigViewController.h"

@interface FXContactsDelegate ()
@property (nonatomic, strong) UIViewController *rootViewController;
@end

@implementation FXContactsDelegate
@synthesize rootViewController = _rootViewController;

+ (NSString*) applicationIcon
{
	return @"FixMailRsrc.bundle/SafeGuard_Contacts_Icon";
}

+ (NSString*) applicationName
{
	return FXLLocalizedString(@"Contacts", @"Contacts app name displayed to the user");
}

- (UIViewController *) rootViewControllerIPhone
{
	// need to do a couple things here
	// 1: grab a listContactsVC controller object
	// 2: give it an exit button to get out the app
	// 3: embed within a nav-controller.
	// ====================================================================

	// 1
	// ====================================================================
	UIViewController *theController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPhone identifier:@"listContactsVC"];

	// 2
	// ====================================================================
	UIBarButtonItem *exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
	theController.navigationItem.leftBarButtonItem = exitButton;

	// 3 - we could extract from the storyboard an already embedded listContactsVC within a navController
	// but we don't to keep it fairly modular. Eg, the non-container app pushes the listContactsVC on an already existing
	// navController... you cannot push a navController onto a navController
	// ====================================================================
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:theController];

	return navController;
}

- (UIViewController *) rootViewControllerIPad
{
	// Our iPad will have the following layout:
	//
	// MGSplitViewController (instead of UISplitViewController)
	// -> UINavigationController -> listContactsVC (Master)
	// -> UINavigationController (Detail)
	//		-> showContactVC (no contact selected will show blank fields)
	//		-> showContactVC (contact selected will show fields filled in)
	//		-> editContactVC (pushed when edit button hit)
	//
	// have to do the following:
	// 1: allocate a splitViewController
	// 2: allocate the master portion (listContactsVC)
	// 3: allocate the detail portion (showContactVC)
	// 4: embed them each in navControllers and put them in the splitViewController
	// ====================================================================

	// 1
	// ====================================================================
	MGSplitViewController *contactController = [[MGSplitViewController alloc] initWithNibName:@"MGSplitViewController" bundle:[FXLSafeZone getResourceBundle]];
	[contactController setup];

	// 2
	// ====================================================================
	listContactsVC *listController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPad identifier:@"listContactsVC"];
	listController.splitViewController = contactController;

	// 3
	// ====================================================================
	showContactVC *showController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPad identifier:@"showContactVC"];
	contactController.delegate = showController;

	// 4
	// ====================================================================
	contactController.masterViewController = [[UINavigationController alloc] initWithRootViewController:listController];
	contactController.detailViewController = [[UINavigationController alloc] initWithRootViewController:showController];

	return contactController;
}

- (UIViewController *) rootViewController
{
	if (!_rootViewController) {
		if (IS_IPHONE()) {
			_rootViewController = [self rootViewControllerIPhone];
		} else if (IS_IPAD()) {
			_rootViewController = [self rootViewControllerIPad];
		}
	}
	return _rootViewController;
}

- (BOOL) containedApplication:(id <SZLApplicationContainer>)applicationContainer didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    // Start ping for this account
    
    if([BaseAccount accounts].count == 0) {
        [ASAccount loadExistingEmailAccounts];
    }
    
    ASAccount* anASAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    [anASAccount startPing];
    
    return YES;
}

- (void) containedApplicationWillTerminate:(id <SZLApplicationContainer>)applicationContainer
{
    [ASAccount removePings];
}

- (void) containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer
{
	if ([BaseAccount accountsConfigured] < 1) {
		UINavigationController *configurationController = [ASConfigViewController accountConfigurationControllerWithDelegate:self];
		[self.rootViewController presentViewController:configurationController animated:YES completion:nil];
	}
}

#pragma mark - SZLConfigurationDelegate Methods
-(void)configurationDidFail:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationWasCancelled:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationDidSucceed:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

@end
