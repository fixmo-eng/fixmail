/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMContactsManager.m
 *
 *  Created by Colin Biggin on 2013-06-21.
 */
#import "FXMContactsManager.h"
#import "FXDatabase.h"
#import "ASContact.h"
#import "ASContact+FMDatabase.h"
#import "ASFolder.h"
#import "ASSyncContact.h"
#import "BaseAccount.h"

@interface contactsOperation  : NSObject
@property (nonatomic, strong) ASSyncContact *syncOperation;
@property (nonatomic, strong) ASContact *contact;
@property (nonatomic, strong) NSString *contactKey;
@end

@implementation contactsOperation
@end

#pragma mark -

@implementation FXMContactsManager

static NSMutableDictionary *sCurrentOperations = nil;

+ (FXMContactsManager *) sharedInstance
{
	static FXMContactsManager *_sharedInstance;
	if(!_sharedInstance) {
		static dispatch_once_t oncePredicate;
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
			sCurrentOperations = [[NSMutableDictionary alloc] initWithCapacity:2];
		});
	}

	return _sharedInstance;
}

+ (void) performContactOperation:(ASContact *)inContact add:(BOOL)inAddOperation
{
	NSParameterAssert(inContact != nil);

	// make sure that our contact folder is pointing to the Contacts folder
	if (inContact.folder == nil) {
		BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
		NSAssert(currentAccount != nil, @"currentAccount != nil");
		NSArray *folders = [currentAccount foldersForFolderType:kFolderTypeContacts];
		if (folders.count > 0) {
			inContact.folder = (ASFolder *)folders[0];
		}
	}

	ASSyncContact *theSyncRequest = [[ASSyncContact alloc] initWithContact:inContact delegate:[self sharedInstance] isAdd:inAddOperation];

	// make an object to reference later on
	contactsOperation *cOp = [[contactsOperation alloc] init];
	cOp.contact = inContact;
	cOp.contactKey = inContact.uniqueKey;
	cOp.syncOperation = theSyncRequest;

	[sCurrentOperations setObject:cOp forKey:cOp.contactKey];

	NSDictionary *userDictionary = @{
			kContactManagerASContactUserInfo : inContact,
	};
	if (inAddOperation) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kContactAddStartedNotification object:nil userInfo:userDictionary];
	} else {
		[[NSNotificationCenter defaultCenter] postNotificationName:kContactChangeStartedNotification object:nil userInfo:userDictionary];
	}

	[theSyncRequest queue];

	return;
}

+ (void) addNewContact:(ASContact *)inContact
{
	[FXMContactsManager performContactOperation:inContact add:YES];
}

+ (void) changeExistingContact:(ASContact *)inContact
{
	[FXMContactsManager performContactOperation:inContact add:NO];
}

+ (void) deleteExistingContact:(ASContact *)inContact
{
	NSParameterAssert(inContact != nil);

	if (inContact.isInLocalDB) {
		[FXDatabase deleteContact:inContact];
	}
	if (inContact.isOnServer) {
		// it's on the server, so queue it up
		[inContact.folder deleteUid:inContact.serverId updateServer:TRUE];
		[inContact.folder commitObjects];
	}

}

#pragma mark - ASSyncContactDelegate methods

- (void) addContact:(ASContact *)inContact
{
	FXDebugLog(kFXLogActiveSync, @"FXMContactsManager: addContact");

	[FXDatabase commitChanges:@[inContact]];

	[FXMContactsManager removeDownloadForContact:inContact];
	NSDictionary *userDictionary = @{
			kContactManagerASContactUserInfo : inContact,
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kContactAddSuccessNotification object:nil userInfo:userDictionary];

}

- (void) addContactFailed:(ASContact *)inContact
            httpStatus:(EHttpStatusCode)aStatusCode
            syncStatus:(ESyncStatus)aSyncStatus
{
	FXDebugLog(kFXLogActiveSync, @"FXMContactsManager: addContactFailed");

	[FXMContactsManager removeDownloadForContact:inContact];
	NSDictionary *userDictionary = @{
			kContactManagerASContactUserInfo : inContact,
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kContactAddErrorNotification object:nil userInfo:userDictionary];
}

- (void) changedContact:(ASContact *)inContact
{
	FXDebugLog(kFXLogActiveSync, @"FXMContactsManager: changedContact");

	[FXDatabase commitChanges:@[inContact]];

	[FXMContactsManager removeDownloadForContact:inContact];
	NSDictionary *userDictionary = @{
			kContactManagerASContactUserInfo : inContact,
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kContactChangeSuccessNotification object:nil userInfo:userDictionary];
}

- (void) changeContactFailed:(ASContact *)inContact
            httpStatus:(EHttpStatusCode)aStatusCode
            syncStatus:(ESyncStatus)aSyncStatus
{
	FXDebugLog(kFXLogActiveSync, @"FXMContactsManager: changeContactFailed");

	[FXMContactsManager removeDownloadForContact:inContact];
	NSDictionary *userDictionary = @{
			kContactManagerASContactUserInfo : inContact,
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kContactChangeErrorNotification object:nil userInfo:userDictionary];
}

#pragma mark - Internal routines

+ (contactsOperation *) getOperationForContact:(ASContact *)inContact
{
	NSParameterAssert(inContact != nil);
	return [sCurrentOperations objectForKey:inContact];
}

+ (contactsOperation *) getDownloadForSyncOperation:(ASSyncContact *)inSyncOperation
{
	NSParameterAssert(inSyncOperation != nil);

	NSArray *syncOperations = [sCurrentOperations allValues];
	for (contactsOperation *theOperation in syncOperations) {
		if (theOperation.syncOperation == inSyncOperation) return theOperation;
	}

	return nil;
}

+ (void) removeDownloadForContact:(ASContact *)inContact
{
	NSParameterAssert(inContact != nil);

	// the email address is the 'key'
	[sCurrentOperations removeObjectForKey:inContact.uniqueKey];
}

@end
