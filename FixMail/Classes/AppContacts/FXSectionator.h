//
//  FXSectionator.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

@interface FXSectionator : NSObject
{
	bool		_searchFlag;
}

@property (nonatomic, strong) NSMutableDictionary *sectionDictionary;
@property (nonatomic, strong) NSMutableArray *allObjectsArray;
@property (nonatomic, strong) NSArray *sortedSectionArray;
@property (nonatomic, strong) NSMutableArray *searchArray;
@property (nonatomic, strong) NSString *searchString;

@property (nonatomic, readonly) int sectionCount;
@property (nonatomic, readonly) bool isSearching;

- (void) addSectionObject:(id)inObject;
- (void) addSectionObjects:(NSArray *)inArray;
- (bool) removeSectionObjectAtIndex:(NSIndexPath *)inIndexPath;
- (bool) removeSectionObject:(id)inObject;

- (int) rowCountForSection:(int)inSection;
- (id) objectForIndex:(NSIndexPath *)inIndexPath;

- (NSString *) titleForSection:(int)inSection;
- (int) sectionTitleCount;
- (NSArray *) sectionTitleArray;

- (void) resetKeys;
- (void) reloadKeys;

@end
