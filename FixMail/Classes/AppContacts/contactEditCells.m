//
//  contactEditCell.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-09.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//
#import "contactEditCells.h"

#import "UIView-ViewFrameGeometry.h"


@interface contactCellUtils : NSObject 
+ (UILabel *) titleLabel:(CGRect)inRect;
+ (UITextField *) textField:(CGRect)inRect;
+ (UIView *) separator:(CGRect)inRect;
@end

@implementation contactCellUtils

+ (UILabel *) titleLabel:(CGRect)inRect
{
	UILabel *theLabel = [[UILabel alloc] initWithFrame:inRect];
	theLabel.backgroundColor = [UIColor clearColor];
	theLabel.font = [UIFont systemFontOfSize:12];
	theLabel.textAlignment = NSTextAlignmentRight;
	theLabel.textColor = [UIColor colorWithRed:(80.0/255.0) green:(103.0/255.0) blue:(142.0/255.0) alpha:1.0];
	theLabel.autoresizingMask = UIViewAutoresizingNone;

	return theLabel;
}

+ (UITextField *) textField:(CGRect)inRect
{
	UITextField *theField = [[UITextField alloc] initWithFrame:inRect];
	theField.borderStyle = UITextBorderStyleNone;
	theField.backgroundColor = [UIColor clearColor];
	theField.font = [UIFont boldSystemFontOfSize:18];
	theField.textAlignment = NSTextAlignmentLeft;
	theField.textColor = [UIColor blackColor];
	theField.minimumFontSize = 14.0;
	theField.adjustsFontSizeToFitWidth = true;
	theField.clearButtonMode = UITextFieldViewModeWhileEditing;
//	theField.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
	theField.autoresizingMask = UIViewAutoresizingNone;

	return theField;
}

+ (UIView *) separator:(CGRect)inRect
{
	UIView *theView = [[UIView alloc] initWithFrame:inRect];
	theView.backgroundColor = [UIColor lightGrayColor];

	if (inRect.size.height == 1) {
		theView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
	} else if (inRect.size.width == 1) {
		theView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
	}

	return theView;
}

@end

#pragma mark -

@implementation contactEditCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;

	self.titleLabel = [contactCellUtils titleLabel:CGRectMake(10.0, 16.0, 70.0, 15.0)];
	self.editField = [contactCellUtils textField:CGRectMake(90.0, 11.0, 200.0, 22.0)];

	[self.contentView addSubview:self.titleLabel];
	[self.contentView addSubview:self.editField];
	[self.contentView addSubview:[contactCellUtils separator:CGRectMake(85.0, 0.0, 1.0, 45.0)]];

	self.autoresizesSubviews = true;
	self.clipsToBounds = true;
	self.accessoryType = UITableViewCellAccessoryNone;
	self.selectionStyle = UITableViewCellSelectionStyleNone;

    return self;
}

- (void) applyEmailField
{
	self.editField.keyboardType = UIKeyboardTypeEmailAddress;
	self.editField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.editField.placeholder = FXLLocalizedStringFromTable(@"EMAIL", @"contactEditCells", @"Placeholder for email field in contact address");
}

- (void) applyPhoneField
{
    self.editField.keyboardType = IS_IPHONE() ? UIKeyboardTypeNumberPad : UIKeyboardTypeNumbersAndPunctuation;
    self.editField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.editField.placeholder = FXLLocalizedStringFromTable(@"PHONE", @"contactEditCells", @"Placeholder for phone field in contact address");
}

- (void) applyURLField
{
	self.editField.keyboardType = UIKeyboardTypeURL;
	self.editField.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.editField.placeholder = FXLLocalizedStringFromTable(@"URL", @"contactEditCells", @"Placeholder for URL field in contact address");
}

- (void) layoutSubviews
{
	[super layoutSubviews];

	self.editField.left = 90.0;
	self.editField.width = self.contentView.width - 90.0 - 10.0;
}

+ (CGFloat) rowHeight
{
	return 45.0;
}

@end

#pragma mark -

@implementation contactEditAddressCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;

	self.titleLabel = [contactCellUtils titleLabel:CGRectMake(10.0, 16.0, 70.0, 15.0)];
	if (IS_IPHONE()) {
		self.streetField = [contactCellUtils textField:CGRectMake(90.0, 10.0, 200.0, 22.0)];
		self.cityField = [contactCellUtils textField:CGRectMake(90.0, 45.0, 100.0, 22.0)];
		self.stateField = [contactCellUtils textField:CGRectMake(195.0, 45.0, 95.0, 22.0)];
		self.postalCodeField = [contactCellUtils textField:CGRectMake(90.0, 80.0, 200.0, 22.0)];
		self.countryField = [contactCellUtils textField:CGRectMake(90.0, 115.0, 200.0, 22.0)];
	} else {
		self.streetField = [contactCellUtils textField:CGRectMake(90.0, 10.0, 600.0, 22.0)];
		self.cityField = [contactCellUtils textField:CGRectMake(90.0, 45.0, 290, 22.0)];
		self.stateField = [contactCellUtils textField:CGRectMake(390, 45.0, 290, 22.0)];
		self.postalCodeField = [contactCellUtils textField:CGRectMake(90.0, 80.0, 290, 22.0)];
		self.countryField = [contactCellUtils textField:CGRectMake(390, 80.0, 290, 22.0)];
	}

	self.streetField.placeholder = FXLLocalizedStringFromTable(@"STREET", @"contactEditCells", @"Placeholder for street field in contact address");
	self.cityField.placeholder = FXLLocalizedStringFromTable(@"CITY", @"contactEditCells", @"Placeholder for city field in contact address");
	self.stateField.placeholder = FXLLocalizedStringFromTable(@"STATE/PROVINCE", @"contactEditCells", @"Placeholder for province/state field in contact address");
	self.postalCodeField.placeholder = FXLLocalizedStringFromTable(@"CODE", @"contactEditCells", @"Placeholder for postal/zip code field in contact address");
	self.countryField.placeholder = FXLLocalizedStringFromTable(@"COUNTRY", @"contactEditCells", @"Placeholder for country field in contact address");

	[self.contentView addSubview:self.titleLabel];
	[self.contentView addSubview:self.streetField];
	[self.contentView addSubview:self.cityField];
	[self.contentView addSubview:self.stateField];
	[self.contentView addSubview:self.postalCodeField];
	[self.contentView addSubview:self.countryField];
	[self.contentView addSubview:[contactCellUtils separator:CGRectMake(85.0, 0.0, 1.0, 115.0)]];
	[self.contentView addSubview:[contactCellUtils separator:CGRectMake(85.0, 37.0, 235.0, 1.0)]];
	[self.contentView addSubview:[contactCellUtils separator:CGRectMake(85.0, 73.0, 235.0, 1.0)]];
	if (IS_IPHONE()) {
		[self.contentView addSubview:[contactCellUtils separator:CGRectMake(85.0, 107.0, 235.0, 1.0)]];
	}

	self.accessoryType = UITableViewCellAccessoryNone;
	self.selectionStyle = UITableViewCellSelectionStyleNone;

    return self;
}

+ (CGFloat) rowHeight
{
	if (IS_IPHONE()) {
		return 148.0;
	} else {
		return 113.0;
	}
}

@end
