//
//  FXContactUtils.h
//  FixMail
//
//  Created by Colin Biggin on 2012-12-06.
//
//

@interface FXContactUtils : NSObject

+ (NSData *) graduatedImage:(UIImage *)inImage quality:(float)inQuality;
+ (NSData *) getImage:(UIImage *)inImage maxSize:(int)inMaxSize;

+ (NSString *) extractEmailAddress:(NSString *)inString;
+ (NSString *) makeFullName:(NSString *)inFirstName middle:(NSString *)inMiddleName last:(NSString *)inLastName;

@end
