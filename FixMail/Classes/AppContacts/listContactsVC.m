/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "listContactsVC.h"
#import "contactAttributedCell.h"
#import "contactSearchCell.h"
#import "editContactVC.h"
#import "showContactVC.h"
#import "ASAccount.h"
#import "ASContact.h"
#import "BaseAccount.h"
#import "FXContactsHeader.h"
#import "FXDatabase.h"
#import "FXSectionator.h"
#import "MGSplitViewController.h"
#import "Reachability.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
#import "FXMContactsManager.h"
#import "ASGALSearch.h"
#import "FXMSearchTimerManager.h"

#define kMinimumRowCountForTitles		20

@interface listContactsVC () <FXAEditContactVCDelegate>
@property (nonatomic, strong) FXSectionator *sectionator;
@property (nonatomic, strong) FXMSearchTimerManager *searchTimerManager;
@property (nonatomic) BOOL serverReachable;
@end

#pragma mark -


@implementation listContactsVC
@synthesize sectionator = _sectionator;
@synthesize searchTimerManager = _searchTimerManager;


- (void) viewDidLoad
{
	FXDebugLog(kFXLogContacts | kFXLogVCLoadUnload, @"listContactsVC: viewDidLoad");
    [super viewDidLoad];

	// Do any additional setup after loading the view, typically from a nib.
	self.addButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addButtonPushed:)];
	self.navigationItem.rightBarButtonItem = self.addButtonItem;
	
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	self.searchDisplayController.searchBar.placeholder = FXLLocalizedStringFromTable(@"SEARCH_CONTACTS_AND_GAL", @"listContactsVC", @"Placeholder string for contact search bar");

	self.syncRequests = [[NSMutableArray alloc] init];

	[self setupControls];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"listContactsVC: viewWillAppear");
	[super viewWillAppear:inAnimated];
    
    [self setupSearchNotifications];
    
	if (self.searchDisplayController.searchBar.text.length > 0) {
		self.sectionator.searchString = self.searchDisplayController.searchBar.text;
		[self.tableView reloadData];
	} else {
		[self reloadTableview];
		
		// Add a line and display GAL contacts underneath
		//NSString *galString = FXLLocalizedStringFromTable(@"GAL_CONTACTS", @"listContactsVC", @"Display title for the list of GAL contacts");
	}
    
    [self startReachablility];
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"listContactsVC: viewDidAppear");
	[super viewDidAppear:inAnimated];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"listContactsVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];
    [self stopReachability];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"listContactsVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
    
    [self removeSearchNotifications];
}

- (void) dealloc
{
	FXDebugLog(kFXLogContacts | kFXLogVCDealloc, @"listContactsVC: dealloc");
    [self removeSearchNotifications];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)inDuration
{
	return;
}

#pragma mark - Getters and Setters

-(FXSectionator *)sectionator
{
	if (!_sectionator) {
		_sectionator = [FXSectionator new];
	}
	return _sectionator;
}

-(FXMSearchTimerManager *)searchTimerManager
{
	if (!_searchTimerManager) {
		_searchTimerManager = [[FXMSearchTimerManager alloc] initWithDelegate:self invalidateAfterTwoCharacters:NO];
	}
	return _searchTimerManager;
}

#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            self.addButtonItem.enabled = TRUE;
			self.serverReachable = YES;
            break;
        case NotReachable:
            self.addButtonItem.enabled = FALSE;
			self.serverReachable = NO;
            break;
    }
}

- (void)startReachablility
{
    ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
    Reachability* aReachability = currentAccount.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)inTableView
{
	return [self.sectionator sectionTitleCount];
}

- (NSInteger) tableView:(UITableView *)inTableView numberOfRowsInSection:(NSInteger)inSection
{
	return [self.sectionator rowCountForSection:inSection];
}

// Customize the appearance of table view cells.
- (UITableViewCell *) tableView:(UITableView *)inTableView cellForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	UITableViewCell *theCell = nil;

	if ((self.sectionator.isSearching)){
		contactSearchCell *searchCell = [self.tableView dequeueReusableCellWithIdentifier:@"contactSearchCell"];
		if (searchCell == nil) {
			searchCell = [[contactSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contactSearchCell"];
		}
		searchCell.contact = [self.sectionator objectForIndex:inIndexPath];
		theCell = searchCell;
	} else {
		contactAttributedCell *attributedCell = [self.tableView dequeueReusableCellWithIdentifier:@"contactAttributedCell"];
		if (attributedCell == nil) {
			attributedCell = [[contactAttributedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"contactAttributedCell"];
		}
		attributedCell.contact = [self.sectionator objectForIndex:inIndexPath];
		theCell = attributedCell;
	}

    return theCell;
}

- (BOOL) tableView:(UITableView *)inTableView canEditRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	if (self.sectionator.isSearching) return NO;
	return YES;
}

- (void) tableView:(UITableView *)inTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		self.savedIndexPath  = inIndexPath;
		UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
																	delegate:self
														   cancelButtonTitle:nil
													  destructiveButtonTitle:FXLLocalizedStringFromTable(@"DELETE_CONTACT", @"listContactsVC", @"Action sheet delete contact button title")
														   otherButtonTitles:FXLLocalizedStringFromTable(@"CANCEL_DELETE_CONTACT", @"listContactsVC", @"Action Sheet cancel button when deleting contact"), nil
										 ];

		[theActionSheet showInView:self.view];
	}
}

- (CGFloat) tableView:(UITableView *)inTableView heightForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	CGFloat rowHeight = 44.0;

	if (self.sectionator.isSearching) rowHeight = [contactSearchCell rowHeight];
	else rowHeight = [contactAttributedCell rowHeight];

	return rowHeight;
}

- (NSArray *) sectionIndexTitlesForTableView:(UITableView *)inTableView
{
	return [self.sectionator sectionTitleArray];
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
    ASContact *theContact = [self.sectionator objectForIndex:inIndexPath];
	NSAssert(theContact != nil, @"theContact != nil");

    if (IS_IPHONE()) {

	    showContactVC *theController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPhone identifier:@"showContactVC"];
		theController.contact = theContact;
        [self.navigationController pushViewController:theController animated:YES];

    } else if (IS_IPAD()) {

		NSAssert(self.splitViewController.detailViewController , @"self.splitViewController.detailViewController");
		UINavigationController *detailNavController = (UINavigationController *)self.splitViewController.detailViewController;
		showContactVC *showController = (showContactVC *)detailNavController.viewControllers[0];
		showController.contact = theContact;

	}

	return;
}

#pragma mark - UIActionSheetDelegate methods

- (void) actionSheet:(UIActionSheet *)inActionSheet clickedButtonAtIndex:(NSInteger)inButtonIndex
{
	// the destructive button is the affirmative for deleting
	if (inButtonIndex != inActionSheet.destructiveButtonIndex) return;

	// get the object to delete
	ASContact *theContact = [self.sectionator objectForIndex:self.savedIndexPath];
	NSAssert(theContact != nil, @"theContact != nil");

	bool sectionDeleted = [self.sectionator removeSectionObjectAtIndex:self.savedIndexPath];
	if (sectionDeleted) {
		[self.tableView deleteSections:[NSIndexSet indexSetWithIndex:self.savedIndexPath.section] withRowAnimation:UITableViewRowAnimationFade];
	} else {
		[self.tableView deleteRowsAtIndexPaths:@[self.savedIndexPath] withRowAnimation:UITableViewRowAnimationFade];
	}

	// and now actually delete it
	[self deleteContact:theContact];
}

#pragma mark - UISearchBarDelegate methods
- (void) searchBar:(UISearchBar *)inSearchBar textDidChange:(NSString *)inSearchText
{
	[self.searchTimerManager updateTimerForSearchText:inSearchText];
	self.sectionator.searchString = inSearchText;
	[self.searchDisplayController.searchResultsTableView reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)inSearchBar
{
	if (self.searchDisplayController.searchBar.text.length == 0) return;

	[self beginGALSearch];
	return;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)inSearchBar
{
	inSearchBar.text = @"";
	_sectionator.searchString = nil;
	[self reloadTableview];
}

#pragma mark - FXMSearchTimerManagerDelegate methods
-(void)searchTimerFired:(NSTimer *)searchTimer
{
	[self beginGALSearch];
}

#pragma mark - Actions & IBActions

- (void) doneButtonPushed:(id)inSender
{
    if (IS_IPHONE()) {

		[self.splitViewController dismissViewControllerAnimated:true completion:nil];

   } else if (IS_IPAD()) {
		[self.splitViewController dismissViewControllerAnimated:true completion:nil];

		// we might be called from a UIPopoverController so dismiss
		NSAssert(self.splitViewController.detailViewController != nil, @"self.splitViewController.detailViewController != nil");
		UINavigationController *detailNavController = (UINavigationController *)self.splitViewController.detailViewController;
		showContactVC *showController = (showContactVC *)detailNavController.viewControllers[0];
		if (showController.masterPopoverController != nil) {
			[showController.masterPopoverController dismissPopoverAnimated:true];
		}

		NSAssert(self.splitViewController != nil, @"self.splitViewController != nil");
		[self.splitViewController dismissViewControllerAnimated:true completion:nil];
	}

	return;
}

- (void) addButtonPushed:(id)inSender
{
	if (self.sectionator.isSearching) return;

	ASContact *newContact = [[ASContact alloc] init];
	newContact.contactType = contactUser;
	newContact.isNew = true;

	editContactVC *editController = nil;
    if (IS_IPHONE()) {
	    editController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPhone identifier:@"editContactVC"];
    } else if (IS_IPAD()) {
		editController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPad identifier:@"editContactVC"];
	}
    
    editController.delegate = self;
	
	[editController setContact:newContact newContact:YES];
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:editController];
	navController.modalPresentationStyle = UIModalPresentationPageSheet;
	[self.navigationController presentViewController:navController animated:YES completion:nil];

	return;
}

#pragma mark - FXAEditContactVCDelegate
- (void)editContactVC:(editContactVC *)editController didSaveContact:(ASContact *)contact
{
    [self reloadTableview];
}

- (void)editContactVC:(editContactVC *)editController didDeleteContact:(ASContact *)contact
{
    NSParameterAssert(contact != nil);
    
	// have to be wary of a couple things here
	// 2: delete from self.sectionator
	// 2: if existing contact, check
	//    a) on server?
	//    b) just local
	// 3: if iPad, blank out the detail screen
	// -------------------------------------------------------------------------------
    
	// 2:
	// -------------------------------------------------------------------------------
	[self.sectionator removeSectionObject:contact];
	[self.tableView reloadData];
    
	// 3:
	// -------------------------------------------------------------------------------
    if (IS_IPAD()) {
		NSAssert(self.splitViewController.detailViewController , @"self.splitViewController.detailViewController");
		UINavigationController *detailNavController = (UINavigationController *)self.splitViewController.detailViewController;
		showContactVC *showController = (showContactVC *)detailNavController.viewControllers[0];
		showController.contact = nil;
	}
    
    [self reloadTableview];
}

- (void)editContactVC:(editContactVC *)editController didFailAction:(ASContact *)contact withKey:(NSString *)key
{
    [self reloadTableview];
}

# pragma mark - Notifications
- (void)notifiedSearch:(NSNotification*)inNotification
{
    NSString *name = [inNotification name];
    if([name isEqualToString:kGALSearchStartedNotification]) {
        [self.activityIndicator startAnimating];
    } else if([name isEqualToString:kGALSearchFinishedResultsNotification]) {
        [self.activityIndicator stopAnimating];
        [self reloadTableview];
    } else if([name isEqualToString:kGALSearchFinishedNoResultsNotification]) {
        [self.activityIndicator stopAnimating];
        [self reloadTableview];
    } else if([name isEqualToString:kGALSearchFinishedErrorNotification]) {
        [self.activityIndicator stopAnimating];
        [self reloadTableview];
    }
}

- (void)setupSearchNotifications
{
	// GAL queries
	// -------------------------------------------------------------------------------
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedSearch:) name:kGALSearchStartedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedSearch:) name:kGALSearchFinishedResultsNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedSearch:) name:kGALSearchFinishedNoResultsNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedSearch:) name:kGALSearchFinishedErrorNotification object:nil];
}

- (void)removeSearchNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kGALSearchStartedNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kGALSearchFinishedResultsNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kGALSearchFinishedNoResultsNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kGALSearchFinishedErrorNotification object:nil];
}

#pragma mark - Private methods

- (void) setupControls
{
	// set a few things for the table
	self.tableView.sectionIndexMinimumDisplayRowCount = kMinimumRowCountForTitles;
	self.tableView.sectionIndexTrackingBackgroundColor = [UIColor lightGrayColor];
    
	return;
}

- (void) dismissKeyboard
{
	[self.view endEditing:YES];
}

- (void) reloadTableview
{
	[self.sectionator resetKeys];
	[self.sectionator addSectionObjects:[FXDatabase contactList:contactUser]];
	if (self.sectionator.isSearching) {
		NSArray *gal = [FXDatabase contactList:contactGAL];
		[self.sectionator addSectionObjects:gal];		
		[self.searchDisplayController.searchResultsTableView reloadData];
	} else {
		[self.tableView reloadData];
	}

	return;
}

-(void)beginGALSearch
{
	if (self.serverReachable) {
		ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
		[ASGALSearch sendSearch:currentAccount query:self.searchDisplayController.searchBar.text];
	}
}

- (void) saveContact:(ASContact *)inContact
{
	NSParameterAssert(inContact != nil);

	if (inContact.isNew) {
		[FXMContactsManager addNewContact:inContact];
	} else {
		[FXMContactsManager changeExistingContact:inContact];
	}

	return;
}

- (void) deleteContact:(ASContact *)inContact
{
	NSParameterAssert(inContact != nil);

	// have to be wary of a couple things here
	// 2: delete from self.sectionator
	// 2: if existing contact, check
	//    a) on server?
	//    b) just local
	// 3: if iPad, blank out the detail screen
	// -------------------------------------------------------------------------------
	[FXMContactsManager deleteExistingContact:inContact];

	// 2:
	// -------------------------------------------------------------------------------
	[self.sectionator removeSectionObject:inContact];
	[self.tableView reloadData];

	// 3:
	// -------------------------------------------------------------------------------
    if (IS_IPAD()) {
		NSAssert(self.splitViewController.detailViewController , @"self.splitViewController.detailViewController");
		UINavigationController *detailNavController = (UINavigationController *)self.splitViewController.detailViewController;
		showContactVC *showController = (showContactVC *)detailNavController.viewControllers[0];
		showController.contact = nil;
	}

	return;
}

- (void) makeExitButton
{
	UIBarButtonItem *exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
	self.navigationItem.leftBarButtonItems = [NSArray arrayWithObject:exitButton];
}

@end
