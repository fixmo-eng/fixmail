/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncContact.h"
#import "ASAccount.h"
#import "ASContactFolder.h"
#import "ASSync.h"
#import "WBXMLDataGenerator.h"

@implementation ASSyncContact

@synthesize contact;
@synthesize delegate;
@synthesize mySyncRequest;

// Constants
static NSString* kCommand               = @"Sync";
static const ASTag kCommandTag          = SYNC_SYNC;
//static NSOperationQueue *sContactQueue	= nil;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithContact:(ASContact*)inContact delegate:(NSObject<ASSyncContactDelegate>*)aDelegate isAdd:(BOOL)anIsAdd
{
    if (self = [super init]) {
        self.contact      = inContact;
        self.delegate   = aDelegate;
        isAdd           = anIsAdd;
    }
    return self;
}

- (void)dealloc
{
	FXDebugLog(kFXLogActiveSync, @"ASSyncContact: dealloc");

}

- (void)send
{
	FXDebugLog(kFXLogActiveSync, @"ASSyncContact: send -->");

    if (isAdd) {
        self.syncRequest = [self sendAddContact:self.contact delegate:self.delegate];
    } else {
        self.syncRequest = [self sendChangeContact:self.contact delegate:self.delegate];
    }
	self.mySyncRequest = self.syncRequest;

	FXDebugLog(kFXLogActiveSync, @"ASSyncContact: send <--");
	return;
}

///////////////////////////////////////////////////////////////////////////////////////////
// WBXML Generators
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Generators

- (ASSync*)sendAddContact:(ASContact *)inContact delegate:(NSObject<ASSyncContactDelegate> *)inContactDelegate
{
    ASSync* aSync = nil;
    @try {
        ASContactFolder* aFolder = (ASContactFolder *)inContact.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders       = [NSArray arrayWithObject:aFolder];
        aSync.isChangeRequest   = FALSE;
        aSync.object            = (BaseObject*)inContact;
        aSync.contactDelegate     = inContactDelegate;
        
        NSData* aWbxml = [self wbxmlAddContact:inContact request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendAddContact" exception:e];
    }
    return aSync;
}

- (ASSync*)sendChangeContact:(ASContact *)inContact delegate:(NSObject<ASSyncContactDelegate> *)inContactDelegate
{
    ASSync* aSync = nil;
    @try {
        ASContactFolder* aFolder = (ASContactFolder *)inContact.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders       = [NSArray arrayWithObject:aFolder];
        aSync.isChangeRequest   = TRUE;
        aSync.object            = (BaseObject*)inContact;
        aSync.contactDelegate     = inContactDelegate;
        NSData* aWbxml = [self wbxmlChangeContact:inContact request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendChangeContact" exception:e];
    }
    return aSync;
}

- (void)wbxmlContact:(ASContact *)inContact wbxml:(WBXMLDataGenerator&)wbxml
{
    wbxml.start(SYNC_APPLICATION_DATA); {

        // OWA seems to require CONTACTS_FILE_AS instead of first/last name so use display name
        //
        NSString* aFileAs = inContact.displayName;
        if(aFileAs.length > 0) {
            wbxml.keyValue(CONTACTS_FILE_AS, aFileAs);
        }
        
        if(inContact.firstName.length > 0) wbxml.keyValue(CONTACTS_FIRST_NAME, inContact.firstName);
        if(inContact.middleName.length > 0) wbxml.keyValue(CONTACTS_MIDDLE_NAME, inContact.middleName);
        if(inContact.lastName.length > 0) wbxml.keyValue(CONTACTS_LAST_NAME, inContact.lastName);
        if(inContact.alias.length > 0) wbxml.keyValue(CONTACTS_ALIAS, inContact.alias);

        if(inContact.company.length > 0) wbxml.keyValue(CONTACTS_COMPANY_NAME, inContact.company);
        if(inContact.department.length > 0) wbxml.keyValue(CONTACTS_DEPARTMENT, inContact.department);
        if(inContact.title.length > 0) wbxml.keyValue(CONTACTS_JOB_TITLE, inContact.title);
        if(inContact.companyLocation.length > 0) wbxml.keyValue(CONTACTS_OFFICE_LOCATION, inContact.companyLocation);
        if(inContact.webPage.length > 0) wbxml.keyValue(CONTACTS_WEBPAGE, inContact.webPage);

        if(inContact.emailAddress1.length > 0) wbxml.keyValue(CONTACTS_EMAIL1_ADDRESS, inContact.emailAddress1);
        if(inContact.emailAddress2.length > 0) wbxml.keyValue(CONTACTS_EMAIL2_ADDRESS, inContact.emailAddress2);
        if(inContact.emailAddress3.length > 0) wbxml.keyValue(CONTACTS_EMAIL3_ADDRESS, inContact.emailAddress3);

        if(inContact.mobilePhone.length > 0) wbxml.keyValue(CONTACTS_MOBILE_TELEPHONE_NUMBER, inContact.mobilePhone);
        if(inContact.pager.length > 0) wbxml.keyValue(CONTACTS_PAGER_NUMBER, inContact.pager);
        if(inContact.otherPhone1.length > 0) wbxml.keyValue(CONTACTS_RADIO_TELEPHONE_NUMBER, inContact.otherPhone1);
        if(inContact.otherPhone2.length > 0) wbxml.keyValue(CONTACTS_CAR_TELEPHONE_NUMBER, inContact.otherPhone2);

        if(inContact.companyStreet.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_ADDRESS_STREET, inContact.companyStreet);
        if(inContact.companyCity.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_ADDRESS_CITY, inContact.companyCity);
        if(inContact.companyState.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_ADDRESS_STATE, inContact.companyState);
        if(inContact.companyCountry.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_ADDRESS_COUNTRY, inContact.companyCountry);
        if(inContact.companyPostalCode.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_ADDRESS_POSTAL_CODE, inContact.companyPostalCode);
        if(inContact.companyPhone1.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_TELEPHONE_NUMBER, inContact.companyPhone1);
        if(inContact.companyPhone2.length > 0) wbxml.keyValue(CONTACTS_BUSINESS2_TELEPHONE_NUMBER, inContact.companyPhone2);
        if(inContact.companyFax.length > 0) wbxml.keyValue(CONTACTS_BUSINESS_FAX_NUMBER, inContact.companyFax);

        if(inContact.homeStreet.length > 0) wbxml.keyValue(CONTACTS_HOME_ADDRESS_STREET, inContact.homeStreet);
        if(inContact.homeCity.length > 0) wbxml.keyValue(CONTACTS_HOME_ADDRESS_CITY, inContact.homeCity);
        if(inContact.homeState.length > 0) wbxml.keyValue(CONTACTS_HOME_ADDRESS_STATE, inContact.homeState);
        if(inContact.homeCountry.length > 0) wbxml.keyValue(CONTACTS_HOME_ADDRESS_COUNTRY, inContact.homeCountry);
        if(inContact.homePostalCode.length > 0) wbxml.keyValue(CONTACTS_HOME_ADDRESS_POSTAL_CODE, inContact.homePostalCode);
        if(inContact.homePhone1.length > 0) wbxml.keyValue(CONTACTS_HOME_TELEPHONE_NUMBER, inContact.homePhone1);
        if(inContact.homePhone2.length > 0) wbxml.keyValue(CONTACTS_HOME2_TELEPHONE_NUMBER, inContact.homePhone2);
        if(inContact.homeFax.length > 0) wbxml.keyValue(CONTACTS_HOME_FAX_NUMBER, inContact.homeFax);

        if(inContact.otherStreet.length > 0) wbxml.keyValue(CONTACTS_OTHER_ADDRESS_STREET, inContact.otherStreet);
        if(inContact.otherCity.length > 0) wbxml.keyValue(CONTACTS_OTHER_ADDRESS_CITY, inContact.otherCity);
        if(inContact.otherState.length > 0) wbxml.keyValue(CONTACTS_OTHER_ADDRESS_STATE, inContact.otherState);
        if(inContact.otherCountry.length > 0) wbxml.keyValue(CONTACTS_OTHER_ADDRESS_COUNTRY, inContact.otherCountry);
        if(inContact.otherPostalCode.length > 0) wbxml.keyValue(CONTACTS_OTHER_ADDRESS_POSTAL_CODE, inContact.otherPostalCode);

        if(inContact.spouse.length > 0) wbxml.keyValue(CONTACTS_SPOUSE, inContact.spouse);
        if(inContact.birthday.length > 0) wbxml.keyValue(CONTACTS_BIRTHDAY, inContact.birthday);

		if (inContact.pictureString.length > 0) {
			wbxml.keyValue(CONTACTS_PICTURE, inContact.pictureString);
//			wbxml.keyOpaque(CONTACTS_PICTURE, inContact.picture);
		}

     }wbxml.end();
}

- (NSData*)wbxmlAddContact:(ASContact *)inContact request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASContactFolder* aFolder = (ASContactFolder*)inContact.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.keyValue(SYNC_GET_CHANGES, @"1");
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_ADD); {
                        ASAccount* anAccount = (ASAccount*)aFolder.account;
                        wbxml.keyValue(SYNC_CLIENT_ID, [anAccount getClientID]);
                        [self wbxmlContact:inContact wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

- (NSData*)wbxmlChangeContact:(ASContact *)inContact request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASContactFolder* aFolder = (ASContactFolder*)inContact.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_CHANGE); {
                        wbxml.keyValue(SYNC_SERVER_ID, inContact.serverId);
                        [self wbxmlContact:inContact wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

@end