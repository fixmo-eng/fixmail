/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "editContactVC.h"
#import "ASAccount.h"
#import "FXContactsHeader.h"
#import "contactEditCells.h"
#import "listContactsVC.h"
#import "Reachability.h"
#import "FXMContactsManager.h"

#import "UIView-ViewFrameGeometry.h"

@interface editContactVC () {
    UITextField* _focusedTextField;
}

@property (strong, nonatomic) ASContact *contact;
@property (nonatomic, strong) UIPopoverController *imageSelectionPopover;
@property (nonatomic) BOOL networkEnabled;
@property (nonatomic) BOOL newContact;
@property (nonatomic, strong) NSArray *textFieldArray;
- (void) setupControls;
- (void) redoControls;
- (void) dismissImageSelector:(UIImagePickerController *)inPicker;
@end

@implementation editContactVC

@synthesize contact = _contact;
@synthesize textFieldArray = _textFieldArray;

- (void) viewDidLoad
{
	FXDebugLog(kFXLogContacts | kFXLogVCLoadUnload, @"editContactVC: viewDidLoad");
	[super viewDidLoad];
	
	self.networkEnabled = YES;

	if (!self.navigationItem.title) {
		self.navigationItem.title = FXLLocalizedStringFromTable(@"INFO", @"editContactVC", "Navigation bar title when displaying contact");
	}
	
	self.cancelButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"CANCEL_EDIT_CONTACT", @"editContactVC", "Cancel bar button item title when editing contact") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleCancel:)];
	if (!self.navigationItem.leftBarButtonItem) {
		self.navigationItem.leftBarButtonItem = self.cancelButton;
	}
	
	self.saveButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"SAVE", @"editContactVC", "Save bar button item title when displaying contact") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleSave:)];
	self.navigationItem.rightBarButtonItem = self.saveButton;
	
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(redoButtons:) name:UITextFieldTextDidChangeNotification object:nil];
    
    [self setupContactNotifications];
	[self setupControls];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"editContactVC: viewWillAppear");
	[super viewWillAppear:inAnimated];
    [self startReachablility];
    
    if(IS_IPHONE()) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidShow:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
    }
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"editContactVC: viewDidAppear");
	[super viewDidAppear:inAnimated];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"editContactVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];
    [self stopReachability];
    
    if(IS_IPHONE()) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"editContactVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
    [self removeContactNotifications];
}

- (void) dealloc
{
	FXDebugLog(kFXLogContacts | kFXLogVCDealloc, @"editContactVC: dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[self.tableView setNeedsLayout];
	[self.tableView reloadData];
}

#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
			self.networkEnabled = YES;
            break;
        case NotReachable:
			self.networkEnabled = YES;
            break;
    }
}

- (void)startReachablility
{
    ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
    Reachability* aReachability = currentAccount.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - Getters/Setters
- (void) setContact:(ASContact *)inContact newContact:(BOOL)newContact
{
	if (_contact == inContact) return;
	
	_contact = inContact;
	self.newContact = newContact;
	[self redoControls];
}

#pragma mark - UITableview Delegate/Datasource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)inTableView
{
	return 6;
}

- (NSInteger) tableView:(UITableView *)inTableView numberOfRowsInSection:(NSInteger)inSection
{
	NSInteger rowCount = 1;
	switch (inSection) {
		case 0: rowCount = 2; break;		// phones
		case 1: rowCount = 3; break;		// email
		case 2: rowCount = 4; break;		// company address
		case 3: rowCount = 4; break;		// home address
		case 4: rowCount = 1; break;		// other address
		case 5: rowCount = 1; break;		// webpage
	}

	return rowCount;
}

- (UITableViewCell *) tableView:(UITableView *)inTableView cellForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	UITableViewCell *theCell = nil;

	if (inIndexPath.section == 0) {
		contactEditCell *editCell = [[contactEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		NSAssert(editCell != nil, @"editCell != nil");

		switch (inIndexPath.row) {
			case 0:
				editCell.titleLabel.text = FXLLocalizedStringFromTable(@"MOBILE", @"ASContact", nil);
				editCell.editField.text = self.contact.mobilePhone;
				if (self.mobileField.text) {
					editCell.editField.text = self.mobileField.text;
				}
				self.mobileField = editCell.editField;
				self.mobileField.delegate = self;
				self.mobileField.keyboardType = UIKeyboardTypePhonePad;
				break;
			case 1:
				editCell.titleLabel.text = FXLLocalizedStringFromTable(@"PAGER", @"ASContact", nil);
				editCell.editField.text = self.contact.pager;
				if (self.pagerField.text) {
					editCell.editField.text = self.pagerField.text;
				}
				self.pagerField = editCell.editField;
				self.pagerField.delegate = self;
				self.pagerField.keyboardType = UIKeyboardTypePhonePad;
				break;
			default:
				break;
		}
		[editCell applyPhoneField];
		theCell = editCell;

	} else if (inIndexPath.section == 1) {
		contactEditCell *editCell = [[contactEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		NSAssert(editCell != nil, @"editCell != nil");

		switch (inIndexPath.row) {
			case 0:
				editCell.titleLabel.text = FXLLocalizedStringFromTable(@"EMAIL_1", @"ASContact", nil);
				editCell.editField.text = self.contact.emailAddress1;
				if (self.email1Field.text) {
					editCell.editField.text = self.email1Field.text;
				}
				self.email1Field = editCell.editField;
				self.email1Field.delegate = self;
				break;
			case 1:
				editCell.titleLabel.text = FXLLocalizedStringFromTable(@"EMAIL_2", @"ASContact", nil);
				editCell.editField.text = self.contact.emailAddress2;
				if (self.email2Field.text) {
					editCell.editField.text = self.email2Field.text;
				}
				self.email2Field = editCell.editField;
				self.email2Field.delegate = self;
				break;
			case 2:
				editCell.titleLabel.text = FXLLocalizedStringFromTable(@"EMAIL_3", @"ASContact", nil);
				editCell.editField.text = self.contact.emailAddress3;
				if (self.email3Field.text) {
					editCell.editField.text = self.email3Field.text;
				}
				self.email3Field = editCell.editField;
				self.email3Field.delegate = self;
				break;
			default:
				break;
		}
		[editCell applyEmailField];
		theCell = editCell;

	} else if (inIndexPath.section == 2) {
		if (inIndexPath.row == 0) {
			contactEditAddressCell *addressCell = [[contactEditAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			NSAssert(addressCell != nil, @"addressCell != nil");

			addressCell.titleLabel.text = FXLLocalizedStringFromTable(@"COMPANY", @"ASContact", nil);

			addressCell.streetField.text = self.contact.companyStreet;
			if (self.companyStreet.text) {
				addressCell.streetField.text = self.companyStreet.text;
			}
			 self.companyStreet = addressCell.streetField;
			 self.companyStreet.delegate = self;
			
			addressCell.cityField.text = self.contact.companyCity;
			if (self.companyCity.text) {
				addressCell.cityField.text = self.companyCity.text;
			}
			 self.companyCity = addressCell.cityField;
			 self.companyCity.delegate = self;
			
			addressCell.stateField.text = self.contact.companyState;
			if (self.companyState.text) {
				addressCell.stateField.text = self.companyState.text;
			}
			 self.companyState = addressCell.stateField;
			 self.companyState.delegate = self;
			
			addressCell.postalCodeField.text = self.contact.companyPostalCode;
			if (self.companyPostalCode.text) {
				addressCell.postalCodeField.text = self.companyPostalCode.text;
			}
			 self.companyPostalCode = addressCell.postalCodeField;
			 self.companyPostalCode.delegate = self;
			
			addressCell.countryField.text = self.contact.companyCountry;
			if (self.companyCountry.text) {
				addressCell.countryField.text = self.companyCountry.text;
			}
			 self.companyCountry = addressCell.countryField;
			 self.companyCountry.delegate = self;

			theCell = addressCell;

		} else {
			contactEditCell *editCell = [[contactEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			NSAssert(editCell != nil, @"editCell != nil");

			switch (inIndexPath.row) {
				case 1:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", nil);
					editCell.editField.text = self.contact.companyPhone1;
					if (self.companyPhone1.text) {
						editCell.editField.text = self.companyPhone1.text;
					}
					self.companyPhone1 = editCell.editField;
					self.companyPhone1.delegate = self;
					self.companyPhone1.keyboardType = UIKeyboardTypePhonePad;
					break;
				case 2:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", nil);
					editCell.editField.text = self.contact.companyPhone2;
					if (self.companyPhone2.text) {
						editCell.editField.text = self.companyPhone2.text;
					}
					self.companyPhone2 = editCell.editField;
					self.companyPhone2.delegate = self;
					self.companyPhone2.keyboardType = UIKeyboardTypePhonePad;
					break;
				case 3:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"FAX", @"ASContact", nil);
					editCell.editField.text = self.contact.companyFax;
					if (self.companyFax.text) {
						editCell.editField.text = self.companyFax.text;
					}
					self.companyFax = editCell.editField;
					self.companyFax.delegate = self;
					self.companyFax.keyboardType = UIKeyboardTypePhonePad;
					break;
				default:
					break;
			}
			[editCell applyPhoneField];
			theCell = editCell;

		}
	} else if (inIndexPath.section == 3) {
		if (inIndexPath.row == 0) {
			contactEditAddressCell *addressCell = [[contactEditAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			NSAssert(addressCell != nil, @"addressCell != nil");

			addressCell.titleLabel.text = FXLLocalizedStringFromTable(@"HOME", @"ASContact", nil);

			addressCell.streetField.text = self.contact.homeStreet;
			if (self.homeStreet.text) {
				addressCell.streetField.text = self.homeStreet.text;
			}
			 self.homeStreet = addressCell.streetField;
			 self.homeStreet.delegate = self;
			
			addressCell.cityField.text = self.contact.homeCity;
			if (self.homeCity.text) {
				addressCell.cityField.text = self.homeCity.text;
			}
			 self.homeCity = addressCell.cityField;
			 self.homeCity.delegate = self;
			
			addressCell.stateField.text = self.contact.homeState;
			if (self.homeState.text) {
				addressCell.stateField.text = self.homeState.text;
			}
			 self.homeState = addressCell.stateField;
			 self.homeState.delegate = self;
			
			addressCell.postalCodeField.text = self.contact.homePostalCode;
			if (self.homePostalCode.text) {
				addressCell.postalCodeField.text = self.homePostalCode.text;
			}
			 self.homePostalCode = addressCell.postalCodeField;
			 self.homePostalCode.delegate = self;
			
			addressCell.countryField.text = self.contact.homeCountry;
			if (self.homeCountry.text) {
				addressCell.countryField.text = self.homeCountry.text;
			}
			 self.homeCountry = addressCell.countryField;
			 self.homeCountry.delegate = self;

			theCell = addressCell;

		} else {
			contactEditCell *editCell = [[contactEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
			NSAssert(editCell != nil, @"editCell != nil");

			switch (inIndexPath.row) {
				case 1:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", nil);
					editCell.editField.text = self.contact.homePhone1;
					if (self.homePhone1.text) {
						editCell.editField.text = self.homePhone1.text;
					}
					self.homePhone1 = editCell.editField;
					self.homePhone1.delegate = self;
					self.homePhone1.keyboardType = UIKeyboardTypePhonePad;
					break;
				case 2:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", nil);
					editCell.editField.text = self.contact.homePhone2;
					if (self.homePhone2.text) {
						editCell.editField.text = self.homePhone2.text;
					}
					self.homePhone2 = editCell.editField;
					self.homePhone2.delegate = self;
					self.homePhone2.keyboardType = UIKeyboardTypePhonePad;
					break;
				case 3:
					editCell.titleLabel.text = FXLLocalizedStringFromTable(@"FAX", @"ASContact", nil);
					editCell.editField.text = self.contact.homeFax;
					if (self.homeFax.text) {
						editCell.editField.text = self.homeFax.text;
					}
					self.homeFax = editCell.editField;
					self.homeFax.delegate = self;
					self.homeFax.keyboardType = UIKeyboardTypePhonePad;
					break;
				default:
					break;
			}
			[editCell applyPhoneField];
			theCell = editCell;

		}
	} else if ((inIndexPath.section == 4) && (inIndexPath.row == 0)) {
		contactEditAddressCell *addressCell = [[contactEditAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		NSAssert(addressCell != nil, @"addressCell != nil");

		addressCell.titleLabel.text = FXLLocalizedStringFromTable(@"OTHER", @"ASContact", nil);

		addressCell.streetField.text = self.contact.otherStreet;
		if (self.otherStreet.text) {
			addressCell.streetField.text = self.otherStreet.text;
		}
		 self.otherStreet = addressCell.streetField;
		 self.otherStreet.delegate = self;
		
		addressCell.cityField.text = self.contact.otherCity;
		if (self.otherCity.text) {
			addressCell.cityField.text = self.otherCity.text;
		}
		 self.otherCity = addressCell.cityField;
		 self.otherCity.delegate = self;
		
		addressCell.stateField.text = self.contact.otherState;
		if (self.otherState.text) {
			addressCell.stateField.text = self.otherState.text;
		}
		 self.otherState = addressCell.stateField;
		 self.otherState.delegate = self;
		
		addressCell.postalCodeField.text = self.contact.otherPostalCode;
		if (self.otherPostalCode.text) {
			addressCell.postalCodeField.text = self.otherPostalCode.text;
		}
		 self.otherPostalCode = addressCell.postalCodeField;
		 self.otherPostalCode.delegate = self;
		
		addressCell.countryField.text = self.contact.otherCountry;
		if (self.otherCountry.text) {
			addressCell.countryField.text = self.otherCountry.text;
		}
		 self.otherCountry = addressCell.countryField;
		 self.otherCountry.delegate = self;

		theCell = addressCell;

	} else if (inIndexPath.section == 5) {
		contactEditCell *editCell = [[contactEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		NSAssert(editCell != nil, @"editCell != nil");

		editCell.titleLabel.text = FXLLocalizedStringFromTable(@"URL", @"ASContact", nil);
		editCell.editField.text = self.contact.webPage;
		if (self.webPage.text) {
			editCell.editField.text = self.webPage.text;
		}
		self.webPage = editCell.editField;
		self.webPage.delegate = self;

		[editCell applyURLField];
		theCell = editCell;
	}

	return theCell;
}

- (BOOL) tableView:(UITableView *)inTableView canEditRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	return NO;
}

- (CGFloat) tableView:(UITableView *)inTableView heightForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	CGFloat rowHeight = [contactEditCell rowHeight];
	if (((inIndexPath.section == 2) && (inIndexPath.row == 0)) ||
		((inIndexPath.section == 3) && (inIndexPath.row == 0)) ||
		((inIndexPath.section == 4) && (inIndexPath.row == 0)) )
	{
		rowHeight = [contactEditAddressCell rowHeight];
	}

	return rowHeight;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - IBActions

- (IBAction) toggleCancel:(id)inSender
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) toggleSave:(id)inSender
{
	self.contact.firstName = self.firstNameField.text;
	self.contact.lastName = self.lastNameField.text;
	self.contact.company = self.companyField.text;

	self.contact.mobilePhone = self.mobileField.text;
	self.contact.pager = self.pagerField.text;

	self.contact.emailAddress1 = self.email1Field.text;
	self.contact.emailAddress2 = self.email2Field.text;
	self.contact.emailAddress3 = self.email3Field.text;

	self.contact.companyStreet = self.companyStreet.text;
	self.contact.companyCity = self.companyCity.text;
	self.contact.companyState = self.companyState.text;
	self.contact.companyPostalCode = self.companyPostalCode.text;
	self.contact.companyCountry = self.companyCountry.text;
	self.contact.companyPhone1 = self.companyPhone1.text;
	self.contact.companyPhone2 = self.companyPhone2.text;
	self.contact.companyFax = self.companyFax.text;

	self.contact.homeStreet = self.homeStreet.text;
	self.contact.homeCity = self.homeCity.text;
	self.contact.homeState = self.homeState.text;
	self.contact.homePostalCode = self.homePostalCode.text;
	self.contact.homeCountry = self.homeCountry.text;
	self.contact.homePhone1 = self.homePhone1.text;
	self.contact.homePhone2 = self.homePhone2.text;
	self.contact.homeFax = self.homeFax.text;

	self.contact.otherStreet = self.otherStreet.text;
	self.contact.otherCity = self.otherCity.text;
	self.contact.otherState = self.otherState.text;
	self.contact.otherPostalCode = self.otherPostalCode.text;
	self.contact.otherCountry = self.otherCountry.text;

	self.contact.webPage = self.webPage.text;
	self.contact.notes = self.notesTextView.text;

	// this will alert the db to be updated
	self.contact.pushToLocalDB = true;

    [self saveContact:self.contact];
}

- (IBAction) toggleDeleteContact:(id)inSender
{
	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
																delegate:self
													   cancelButtonTitle:nil
												  destructiveButtonTitle:FXLLocalizedStringFromTable(@"DELETE_CONTACT", @"editContactVC", "Action sheet button title for deleting contact when clicked to delete contact")
													   otherButtonTitles:FXLLocalizedStringFromTable(@"CANCEL_DELETE_CONTACT", @"editContactVC", "Action sheet button title for cancelling when clicked to delete contact"), nil
									 ];

	[theActionSheet showInView:self.view];
	return;
}

#pragma mark Keyboard Notifications
- (void)keyboardDidShow:(NSNotification *)note
{
    if(_focusedTextField)
        [self shouldAddDoneButton:YES];
}

- (void)shouldAddDoneButton:(BOOL)add
{
    UIButton *doneButton;
    if(add) {
        // create custom button
        doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [doneButton setTag:1111];
        doneButton.frame = CGRectMake(0, 163, 106, 53);
        doneButton.adjustsImageWhenHighlighted = NO;
        [doneButton setImage:[UIImage imageNamed:@"FixMailRsrc.bundle/KeyboardNumberPadDoneUp.png"]
                    forState:UIControlStateNormal];
        [doneButton setImage:[UIImage imageNamed:@"FixMailRsrc.bundle/KeyboardNumberPadDoneDown.png"]
                    forState:UIControlStateHighlighted];
        [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    // locate keyboard view
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++) {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
            if([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES) {
                if(add) {
                    UIButton* existBtn = (UIButton*)[keyboard viewWithTag:1111];
                    if(!existBtn)
                        [keyboard addSubview:doneButton];
                } else {                    
                    [(UIButton*)[keyboard viewWithTag:1111] removeFromSuperview];
                }
            }
        }
        else {
            if(add) {
                UIButton* existBtn = (UIButton*)[keyboard viewWithTag:1111];
                if(!existBtn)
                    [keyboard addSubview:doneButton];
            } else {
                [(UIButton*)[keyboard viewWithTag:1111] removeFromSuperview];
            }
        }
    }
}

- (void)doneButton:(id)sender
{
    [_focusedTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{    
    return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(IS_IPHONE()) {
        if(textField.keyboardType == UIKeyboardTypeNumberPad) {
            if(![_focusedTextField isEqual:textField]) {
                _focusedTextField = textField;
                [self shouldAddDoneButton:YES];
            }
        } else {
            [self shouldAddDoneButton:NO];
        }
    }
}

- (void) textFieldDidEndEditing:(UITextField *)inTextField
{
    if(IS_IPHONE() && inTextField.keyboardType == UIKeyboardTypeNumberPad) {
        [self shouldAddDoneButton:NO];
        _focusedTextField = nil;
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)inTextField
{
	[inTextField resignFirstResponder];
	return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(IS_IPAD()) {
        if(textField.keyboardType == UIKeyboardTypeNumbersAndPunctuation) {
            NSString *validRegEx =@"^[0-9-]*$";
            NSPredicate *regExPredicate =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", validRegEx];
            BOOL myStringMatchesRegEx = [regExPredicate evaluateWithObject:string];
            if (myStringMatchesRegEx)
                return YES;
            else
                return NO;
        }
    }
    
	return YES;
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)inActionSheet clickedButtonAtIndex:(NSInteger)inButtonIndex
{
	// the destructive button is the affirmative for deleting
	if (inButtonIndex != inActionSheet.destructiveButtonIndex) return;

	// this will let whatever list do whatever it needs to do
//	[[NSNotificationCenter defaultCenter] postNotificationName:kDeleteContactNotification
//														object:nil
//													  userInfo:[NSDictionary dictionaryWithObject:self.contact forKey:kUserInfoContact]];
    [self deleteContact:self.contact];
	
	return;
}

#pragma mark - Notifications
- (void)notifiedContact:(NSNotification*)inNotification
{
    NSString *name = [inNotification name];
    ASContact *asContact = [inNotification userInfo][kContactManagerASContactUserInfo];
    
    if([name isEqualToString:kContactAddSuccessNotification] ||
       [name isEqualToString:kContactChangeSuccessNotification] ||
       [name isEqualToString:kContactAddedFromServerNotification] ||
       [name isEqualToString:kContactUpdatedFromServerNotification]) {
        [self.delegate editContactVC:self didSaveContact:asContact];
        
    } else if([name isEqualToString:kContactDeleteSuccessNotification] ||
              [name isEqualToString:kContactDeletedFromServerNotification]) {
        [self.delegate editContactVC:self didDeleteContact:asContact];
        
    } else { // errors
        [self.delegate editContactVC:self didFailAction:asContact withKey:name];
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private methods
- (void)setupContactNotifications
{
    // FXMContactManager messages
	// -------------------------------------------------------------------------------
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactAddSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactChangeSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactDeleteSuccessNotification object:nil];
    
	// if things change on the server
	// -------------------------------------------------------------------------------
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactUpdatedFromServerNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactAddedFromServerNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifiedContact:) name:kContactDeletedFromServerNotification object:nil];
}

- (void)removeContactNotifications
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactAddSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactChangeSuccessNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactDeleteSuccessNotification object:nil];
    
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactUpdatedFromServerNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactAddedFromServerNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kContactDeletedFromServerNotification object:nil];
}

- (void)saveContact:(ASContact*)contact
{    
    if (contact.isNew) {
		[FXMContactsManager addNewContact:contact];
	} else {
		[FXMContactsManager changeExistingContact:contact];
	}
}

- (void)deleteContact:(ASContact*)contact
{
    NSParameterAssert(contact != nil);
    
	// have to be wary of a couple things here
	// 2: delete from self.sectionator
	// 2: if existing contact, check
	//    a) on server?
	//    b) just local
	// 3: if iPad, blank out the detail screen
	// -------------------------------------------------------------------------------
	[FXMContactsManager deleteExistingContact:contact];
    
//	// 2:
//	// -------------------------------------------------------------------------------
//	[self.sectionator removeSectionObject:contact];
//	[self.tableView reloadData];
//    
//	// 3:
//	// -------------------------------------------------------------------------------
//    if (IS_IPAD()) {
//		NSAssert(self.splitViewController.detailViewController , @"self.splitViewController.detailViewController");
//		UINavigationController *detailNavController = (UINavigationController *)self.splitViewController.detailViewController;
//		showContactVC *showController = (showContactVC *)detailNavController.viewControllers[0];
//		showController.contact = nil;
//	}
}

- (void) setupControls
{
	self.headerView.image = [[UIImage imageNamed:@"FixMailRsrc.bundle/editContactHeader.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0];
	self.footerView.image = [[UIImage imageNamed:@"FixMailRsrc.bundle/notesFooter.png"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];

	self.firstNameField.placeholder = FXLLocalizedStringFromTable(@"FIRST_NAME_PLACEHOLDER", @"ASContact", nil);
	self.lastNameField.placeholder = FXLLocalizedStringFromTable(@"LAST_NAME_PLACEHOLDER", @"ASContact", nil);
	self.companyField.placeholder = FXLLocalizedStringFromTable(@"COMPANY_PLACEHOLDER", @"ASContact", nil);

	[self.deleteContactButton setBackgroundImage:[[UIImage imageNamed:@"FixMailRsrc.bundle/redButton.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];

	[self redoControls];
}

- (void) redoControls
{
	self.contactImage.image = self.contact.image;

	self.firstNameField.text = self.contact.firstName;
	self.lastNameField.text = self.contact.lastName;
	self.companyField.text = self.contact.company;

	self.notesTextView.text = self.contact.notes;
	
	[self redoButtons:nil];

	return;
}

-(void)redoButtons:(id)sender
{
	if (!self.networkEnabled) {
		self.saveButton.enabled = NO;
		self.deleteContactButton.enabled = NO;
	} else {

		if (sender && [sender isKindOfClass:[UITextField class]] && ((UITextField *)sender).text.length > 0) {
			self.saveButton.enabled = YES;
		} else if (self.firstNameField.text.length > 0 || self.lastNameField.text.length > 0
			|| self.companyField.text.length > 0 || self.mobileField.text.length > 0
			|| self.pagerField.text.length > 0 || self.email1Field.text.length > 0
			|| self.email2Field.text.length > 0 || self.email3Field.text.length > 0
			|| self.companyStreet.text.length > 0 || self.companyPostalCode.text.length > 0
			|| self.companyCountry.text.length > 0 || self.companyPhone1.text.length > 0
			|| self.companyPhone2.text.length > 0 || self.companyFax.text.length > 0
			|| self.homeStreet.text.length > 0 || self.homeCity.text.length > 0
			|| self.homeState.text.length > 0 || self.homePostalCode.text.length > 0
			|| self.homeCountry.text.length > 0 || self.homePhone1.text.length > 0
			|| self.homePhone2.text.length > 0 || self.homeFax.text.length > 0
			|| self.otherStreet.text.length > 0 || self.otherCity.text.length > 0
			|| self.otherState.text.length > 0 || self.otherPostalCode.text.length > 0
			|| self.otherCountry.text.length > 0 || self.webPage.text.length > 0
			|| self.notesTextView.text.length > 0) {
			self.saveButton.enabled = YES;
		} else {
			self.saveButton.enabled = NO;
		}
		
		if (self.newContact) {
			self.deleteContactButton.hidden = YES;
			self.deleteContactButton.enabled = NO;
		} else {
			self.deleteContactButton.hidden = NO;
			self.deleteContactButton.enabled = YES;
		}
	}
}

- (void) dismissImageSelector:(UIImagePickerController *)inPicker
{
	if (IS_IPAD()) {
		[self.imageSelectionPopover dismissPopoverAnimated:true];
		self.imageSelectionPopover = nil;
	} else {
		[inPicker dismissViewControllerAnimated:true completion:nil];
	}

}

@end
