/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASContact.h"
#import "MGSplitViewController.h"

@interface showContactVC : UITableViewController <MGSplitViewControllerDelegate>
{
	bool		_showPopdown;
}

@property (strong, nonatomic) ASContact *contact;
@property (strong, nonatomic) NSArray *tableArrayStrings;

// toolbar
@property (strong, nonatomic) UIBarButtonItem *editButton;
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) MGSplitViewController *masterSplitViewController;

// header view
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIImageView *contactImage;
@property (strong, nonatomic) IBOutlet UILabel *fullnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *companyLabel;

@end
	