//
//  contactAttributedCell.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-05.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import "contactAttributedCell.h"
#import "ASContact.h"
#import "AppSettings.h"

#import "UIView-ViewFrameGeometry.h"

#define kNamePadding	5.0
#define kLabelX			10.0

@interface contactAttributedCell()
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *firstToLastNameLabelConstraint;
//@property (strong, nonatomic) NSArray *lastToFirstNameLabelConstraints;

@end

@implementation contactAttributedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //NSDictionary *labelDictionary = NSDictionaryOfVariableBindings(self.firstNameLabel, self.lastNameLabel);
        //self.lastToFirstNameLabelConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[self.lastNameLabel]-5-[self.firstNameLabel]" options:0 metrics:nil views:labelDictionary];
        
    }
    
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	
	// make sure our image is always to the right by a fixed distance
	// -------------------------------------------------------------------------------
	self.characteristicImage.right = self.contentView.right - 10.0;
}

#pragma mark - Static methods

+ (CGFloat) rowHeight
{
	return 44.0;
}

#pragma mark - Getters/Setters

- (void) setContact:(ASContact *)inContact
{
	// we COULD just return, but some of the content might have changed
	// when the table gets redrawn and if we just returned, this wouldn't
	// get updated... so we have to re-check
	// -------------------------------------------------------------------------------
	if (inContact != _contact) {
		_contact = inContact;
	}

	[self redoControls];
}

#pragma mark - Private methods

- (void) redoControls
{
	if (_contact == nil) {
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = true;
		self.characteristicImage.hidden = true;
		return;
	}

	if (_contact.isCompany) {

		// only use the lastNameLabel
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = false;

		self.lastNameLabel.text = self.contact.company;
		[self.lastNameLabel sizeToFit];
		self.lastNameLabel.left = kLabelX;

		self.characteristicImage.image = nil;

	} else if (self.contact.hasName) {
		self.firstNameLabel.hidden = false;
		self.lastNameLabel.hidden = false;

		self.firstNameLabel.text = self.contact.firstAndMiddleName;
		self.lastNameLabel.text = self.contact.lastName;
		[self.firstNameLabel sizeToFit];
		[self.lastNameLabel sizeToFit];

        
		if ([AppSettings contactsDisplayOrder] == sortOrderFirstLast) {
			self.firstNameLabel.left = kLabelX;
            if (self.firstNameLabel.text) {
                self.lastNameLabel.left = self.firstNameLabel.right + kNamePadding;
            } else {
                self.lastNameLabel.left = kLabelX;
            }
		} else {
			self.lastNameLabel.left = kLabelX;
            if (self.lastNameLabel.text) {
                self.firstNameLabel.left = self.lastNameLabel.right + kNamePadding;
            } else {
                self.firstNameLabel.left = kLabelX;
            }
		}

		if (self.contact.isMe) self.characteristicImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/meIcon.png"];
		else if (self.contact.isFavorite) self.characteristicImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/faveIcon.png"];
		else self.characteristicImage.image = nil;
	} else {		
		
		self.lastNameLabel.hidden = true;
		self.firstNameLabel.hidden = false;
		
		NSString *email = [self firstAvailableEmailAddress];
		if (email.length > 0) {
			self.firstNameLabel.text = email;
		} else {
			self.firstNameLabel.text = FXLLocalizedStringFromTable(@"NO_NAME", @"ASContact", @"Placeholder in list of contacts when contact has neither first, last, nor company name");
			self.firstNameLabel.font = [UIFont italicSystemFontOfSize:20.0];
		}
		[self.firstNameLabel sizeToFit];
		self.firstNameLabel.left = kLabelX;
		
	}

	return;
}

-(NSString *)firstAvailableEmailAddress
{
	if (self.contact.emailAddress1.length > 0) return self.contact.emailAddress1;
	if (self.contact.emailAddress2.length > 0) return self.contact.emailAddress2;
	if (self.contact.emailAddress3.length > 0) return self.contact.emailAddress3;
	
	return @"";
}

@end
