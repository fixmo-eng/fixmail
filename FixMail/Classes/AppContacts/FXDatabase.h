/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "DBaseEngine.h"
#import "ASContact.h"
#import "FXContactsHeader.h"

@class FMDatabaseQueue;

@interface FXDatabase : DBaseEngine

+ (FXDatabase*)sharedManager;
+ (void) initializeDB;
+ (void) terminateDB;
+ (void) deleteContactDatabases;

+ (NSString *) tableNameForContactType:(int)inContactType;

+ (NSMutableArray *) contactList:(int)inContactType;
+ (NSMutableArray *) contactListForEmailAddress:(NSString *)emailAddress;
+ (NSMutableArray *) contactListForName:(NSString *)name;

+ (void) commitChanges:(NSArray *)inList;
+ (void) deleteContactList:(int)inContactType;

+ (void) addContactWithEmail:(NSString *)inEmail name:(NSString *)inName contact:(int)inContactType;
+ (ASContact *) getContactWithServerID:(NSString *)inServerID;
+ (void) deleteContactWithServerID:(NSString *)inServerID;
+ (void) deleteContact:(ASContact *)inContact;

+ (FMDatabaseQueue *) defaultFMDatabaseQueue;

+ (NSMutableArray *) findContacts:(NSString *)inEmailAddress;
@end

