//
//  FXSectionator.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//
#import "FXSectionator.h"
#import "ASContact.h"

@interface FXSection : NSObject

@property (nonatomic, strong) NSMutableArray *rowArray;
@property (nonatomic, strong) NSString *sortKey;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, readonly) int rowCount;
- (void) redoSortedArray;
@end

@implementation FXSection

- (id) init
{
	self = [super init];
	if (self) {
		self.rowArray = [[NSMutableArray alloc] init];
	}
	return self;
}

- (NSString *) description
{
	return self.sortKey;
}

- (NSComparisonResult) sortBySortKey:(FXSection *)inSection
{
	return [self.sortKey compare:inSection.sortKey];
}

- (int) rowCount
{
	return [self.rowArray count];
}

- (void) redoSortedArray
{
	NSArray *theArray = [self.rowArray sortedArrayUsingComparator:^(id inContact1, id inContact2) {
		if ([inContact1 respondsToSelector:@selector(sortKey)] && [inContact2 respondsToSelector:@selector(sortKey)]) {
			
			return [[inContact1 sortKey] compare:[inContact2 sortKey]];
		} else {
			return [[inContact1 description] compare:[inContact2 description]];
		}
			
	}];
	[self.rowArray removeAllObjects];
	[self.rowArray addObjectsFromArray:theArray];
}

@end

#pragma mark -

@implementation FXSectionator

- (id) init
{
	self = [super init];
	if (self) {
		self.sectionDictionary = [[NSMutableDictionary alloc] init];
		self.allObjectsArray = [[NSMutableArray alloc] init];
		self.searchArray = [[NSMutableArray alloc] init];
		_searchFlag = false;
	}
	return self;
}

- (void) setSearchString:(NSString *)inString
{
	if ([inString length] > 0) {
		_searchString = inString;
		_searchFlag = true;
	} else {
		_searchString = nil;
		_searchFlag = false;
	}
	[self redoSearchArray];

	return;
}

- (void) addSectionObject:(id)inObject
{
	if (inObject == nil) return;

	NSString *theSortKey = nil;
	if ([inObject respondsToSelector:@selector(sortKey)]) {
		theSortKey = [inObject sortKey];
	} else {
		theSortKey = [inObject description];
	}
	if (theSortKey.length == 0) return;

	// we'll only use the first letter from the sortKey for our side-index
	theSortKey = [[theSortKey substringToIndex:1] uppercaseString];

	FXSection *theSection = (self.sectionDictionary)[theSortKey];
	if (theSection == nil) {
		theSection = [[FXSection alloc] init];
		theSection.sortKey = theSortKey;
		theSection.title = theSortKey;
		(self.sectionDictionary) [theSortKey] = theSection;
		[self redoSortedArray];
	}
	[theSection.rowArray addObject:inObject];
	[theSection redoSortedArray];
	[self.allObjectsArray addObject:inObject];

	return;
}

- (void) addSectionObjects:(NSArray *)inArray
{
	if (inArray == nil) return;

	[inArray enumerateObjectsUsingBlock:^(id inObject, NSUInteger inIndex, BOOL *inStop)
		{
			[self addSectionObject:inObject];
		}
	];
	
	[self redoSearchArray];
}

- (bool) removeSectionObjectAtIndex:(NSIndexPath *)inIndexPath
{	bool sectionDeleted = false;

	NSAssert(inIndexPath.section < self.sortedSectionArray.count, @"inIndexPath.section < self.sortedArray.count");

	FXSection *theSection = (self.sortedSectionArray)[inIndexPath.section];
	NSAssert(theSection != nil, @"theSection != nil");
	NSAssert(inIndexPath.row < theSection.rowArray.count, @"inIndexPath.row < theSection.rowArray.count");

	// if there's nothing left in the array, then delete this section entirely
	id theObject = theSection.rowArray[inIndexPath.row];

	[self.allObjectsArray removeObject:theObject];
	[theSection.rowArray removeObject:theObject];

	if (theSection.rowArray.count == 0) {
		[self.sectionDictionary removeObjectForKey:theSection.sortKey];
		[self redoSortedArray];
		sectionDeleted = true;
	}


	return sectionDeleted;
}

- (bool) removeSectionObject:(id)inObject
{	bool sectionDeleted = false;

	for (FXSection *theSection in self.sortedSectionArray) {
		if (![theSection.rowArray containsObject:inObject]) continue;

		// found it
		[theSection.rowArray removeObject:inObject];
		[self.allObjectsArray removeObject:inObject];

		if (theSection.rowArray.count == 0) {
			[self.sectionDictionary removeObjectForKey:theSection.sortKey];
			[self redoSortedArray];
			sectionDeleted = true;
		}
		break;
	}

	return sectionDeleted;
}

- (int) sectionCount
{
	if (_searchFlag) return 1;

	return [self.sortedSectionArray count];
}

- (bool) isSearching
{
	return _searchFlag;
}

- (int) rowCountForSection:(int)inSection
{
	if (_searchFlag) {
		NSAssert(inSection == 0, @"inSection == 0");
		return self.searchArray.count;
	}

	NSAssert(inSection < self.sortedSectionArray.count, @"inSection < sortedSectionArray");

	FXSection *theSection = (self.sortedSectionArray)[inSection];
	return theSection.rowCount;
}


- (id) objectForIndex:(NSIndexPath *)inIndexPath
{
	if (_searchFlag) {
		NSAssert(inIndexPath.section == 0, @"inIndexPath.section == 0");
		NSAssert(inIndexPath.row < self.searchArray.count, @"inIndexPath.row < self.searchArray.count");
		return (self.searchArray)[inIndexPath.row];
	}

	NSAssert(inIndexPath.section < self.sortedSectionArray.count, @"inIndexPath.section < self.sortedArray.count");

	FXSection *theSection = (self.sortedSectionArray)[inIndexPath.section];
	NSAssert(theSection != nil, @"theSection != nil");
	NSAssert(inIndexPath.row < theSection.rowArray.count, @"inIndexPath.row < theSection.rowArray.count");

	return (theSection.rowArray)[inIndexPath.row];
}

- (NSString *) titleForSection:(int)inSection
{
	if (_searchFlag) return @"";

	NSAssert(inSection < self.sortedSectionArray.count, @"inSection < self.sortedArray.count");	

	FXSection *theSection = (self.sortedSectionArray)[inSection];
	NSAssert(theSection != nil, @"theSection != nil");

	return theSection.title;
}

- (int) sectionTitleCount
{
	if (_searchFlag) return 1;

	return self.sortedSectionArray.count;
}

- (NSArray *) sectionTitleArray
{
	if (_searchFlag) return nil;

	// only return the title if enough sections and/or entries
//	if (self.allObjectsArray.count < 20) return nil;

	NSMutableArray *theArray = [[NSMutableArray alloc] initWithCapacity:self.sortedSectionArray.count];
	for (FXSection *theSection in self.sortedSectionArray) {
		[theArray addObject:theSection.sortKey];
	}

//	[theArray replaceObjectAtIndex:0 withObject:@"🔎"];

	return theArray;
}

- (void) resetKeys
{
	// get rid of everything
	[self.sectionDictionary removeAllObjects];
	[self.allObjectsArray removeAllObjects];
	self.sortedSectionArray = nil;
	[self.searchArray removeAllObjects];
}

- (void) reloadKeys
{
	// have to do a couple things
	// 1) get a copy of allObjects array go iterate over
	// 2) delete all the FXSections and allObjcts
	// 3) re-iterate over all the objects and add them to the sections
	//
	// ** IMPORTANT **
	// we DON'T use the allObjects array because addSectionObject re-inserts back into it
	// so you could be potentially doubling up on objects
	// -------------------------------------------------------------------------------

	// 1
	// -------------------------------------------------------------------------------
	NSArray *theArray = [NSArray arrayWithArray:self.allObjectsArray];

	// 2
	// -------------------------------------------------------------------------------
	[self.sectionDictionary removeAllObjects];
	[self.allObjectsArray removeAllObjects];

	// 3
	// -------------------------------------------------------------------------------
	[theArray enumerateObjectsUsingBlock:^(id inObject, NSUInteger inIndex, BOOL *inStop)
		{
			[self addSectionObject:inObject];
		}
	];

	return;
}

#pragma mark - Private methods

- (void) redoSortedArray
{
	NSArray *theArray = [self.sectionDictionary allValues];
	self.sortedSectionArray = [theArray sortedArrayUsingComparator:^(FXSection *inSection1, FXSection *inSection2) {
				return [inSection1.sortKey compare:inSection2.sortKey];
			}
	];
}

- (void) redoSearchArray
{
	[self.searchArray removeAllObjects];
	if ([self.searchString length] == 0) return;

	for (FXSection *theSection in self.sortedSectionArray) {
		for (id theObject in theSection.rowArray) {

			if ([theObject isKindOfClass:[ASContact class]]) {
				ASContact *theContact = (ASContact *)theObject;
				if ([theContact matchesSearchString:self.searchString]) {
					[self.searchArray addObject:theObject];
				}
			} else {
				NSString *theSearchString = [theObject description];
				if (theSearchString != nil) {
					NSRange theRange = [theSearchString rangeOfString:self.searchString options:NSCaseInsensitiveSearch];
					if (theRange.length > 0) [self.searchArray addObject:theObject];
				}
			}

		}  // for id

	} // for FXSection

	return;
}

@end
