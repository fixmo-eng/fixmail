/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMCertificateManager.h
 *
 *  Created by Colin Biggin on 2013-06-21.
 */

#import "ASSyncContactDelegate.h"

#define kContactAddStartedNotification			@"kContactAddStartedNotification"
#define kContactAddErrorNotification			@"kContactAddErrorNotification"
#define kContactAddSuccessNotification			@"kContactAddSuccessNotification"

#define kContactChangeStartedNotification		@"kContactChangeStartedNotification"
#define kContactChangeErrorNotification			@"kContactChangeErrorNotification"
#define kContactChangeSuccessNotification		@"kContactChangeSuccessNotification"

#define kContactDeleteStartedNotification		@"kContactDeleteStartedNotification"
#define kContactDeleteErrorNotification			@"kContactDeleteErrorNotification"
#define kContactDeleteSuccessNotification		@"kContactDeleteSuccessNotification"

#define kContactManagerASContactUserInfo		@"kContactManagerASContactUserInfo"

@class ASContact;

@interface FXMContactsManager : NSObject <ASSyncContactDelegate>

+ (void) addNewContact:(ASContact *)inContact;
+ (void) changeExistingContact:(ASContact *)inContact;
+ (void) deleteExistingContact:(ASContact *)inContact;

@end
