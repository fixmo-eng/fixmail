//
//  FXMRecipientSearchCell.h
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-06-20.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ASContact;

@interface FXMRecipientSearchCell : UITableViewCell
@property (nonatomic, strong) ASContact *contact;
@property (nonatomic, strong) NSString *matchedSearchString;

@property (nonatomic, strong) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *lastNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *emailLabel;

+ (CGFloat) rowHeight;
@end
