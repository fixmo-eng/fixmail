/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "showContactVC.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "FXContactsHeader.h"
#import "FXDatabase.h"
#import "editContactVC.h"
#import "contactDescriptionCell.h"
#import "MGSplitViewController.h"
#import "Reachability.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"

#import "composeMailVC.h"
#import "BaseAccount.h"

@interface showContactVC () <FXAEditContactVCDelegate>
- (void) setupControls;
- (void) setupNotifications;
- (void) redoControls;
- (void) reloadTable;

@property (strong, nonatomic) id<FXAEditContactVCDelegate> delegate;
@property (nonatomic, strong) UIBarButtonItem *exitButton;
@end

@implementation showContactVC
@synthesize exitButton = _exitButton;

-(UIBarButtonItem *)exitButton
{
	if (!_exitButton) {
		_exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
	}
	return _exitButton;
}

- (void) viewDidLoad
{
	FXDebugLog(kFXLogContacts | kFXLogVCLoadUnload, @"showContactVC: viewDidLoad");
	[super viewDidLoad];

	if (!self.navigationItem.title) {
		self.navigationItem.title = FXLLocalizedStringFromTable(@"INFO", @"showContactVC", "Navigation bar title when displaying contact");
	}
	
	self.editButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"EDIT", @"showContactVC", "Edit button title when displaying contact") style:UIBarButtonItemStyleBordered target:self action:@selector(editButtonPushed:)];
	self.navigationItem.rightBarButtonItem = self.editButton;

	self.tableView.tableHeaderView = self.headerView;
	
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;

	[self setupNotifications];
	[self setupControls];

	// so, if we're on iPad and we first show up in portrait mode then
	// we get this ugly blank screen. So to make it better, we'll display
	// the popup... but ONLY on load
	_showPopdown = false;
	if (IS_IPAD()) {
		if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)) {
			_showPopdown = true;
		}
	}

	return;
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"showContactVC: viewWillAppear");
	[super viewWillAppear:inAnimated];

	[self redoControls];
	[self reloadTable];
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"showContactVC: viewDidAppear");
	[super viewDidAppear:inAnimated];

	if (_showPopdown) {
		[self.masterSplitViewController showMasterPopover:self];
		_showPopdown = false;
	}
    
    [self startReachablility];

	return;
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"showContactVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];
    
    [self stopReachability];

	// get rid of any popover controller
	[self.masterPopoverController dismissPopoverAnimated:true];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"showContactVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
}

- (void) dealloc
{
	FXDebugLog(kFXLogContacts | kFXLogVCDealloc, @"showContactVC: dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[self.tableView setNeedsLayout];
	[self.tableView reloadData];
}

#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            self.editButton.enabled = TRUE;
            break;
        case NotReachable:
            self.editButton.enabled = FALSE;
            break;
    }
}

- (void)startReachablility
{
    ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
    Reachability* aReachability = currentAccount.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - Getters/Setters

- (void) setContact:(ASContact *)inContact
{
	if (_contact == inContact) return;

	_contact = inContact;
	self.tableArrayStrings = _contact.tableArrayStrings;
	[self redoControls];
	[self reloadTable];

	if (self.masterPopoverController != nil) {
		[self.masterPopoverController dismissPopoverAnimated:YES];
	}

	return;
}

#pragma mark -
#pragma mark Split view support

- (void) splitViewController:(MGSplitViewController *)splitController
	  willHideViewController:(UIViewController *)viewController
		   withBarButtonItem:(UIBarButtonItem *)barButtonItem
		forPopoverController:(UIPopoverController *)popoverController
{
	if (self.masterSplitViewController == nil) {
		self.masterSplitViewController = splitController;
	}

	barButtonItem.title = FXLLocalizedStringFromTable(@"ALL_CONTACTS", @"showContactVC", "Left bar button item for showing master view controller listing all contacts from split view controller when master view controller hidden");
	self.navigationItem.leftBarButtonItems = @[self.exitButton, barButtonItem];
	self.masterPopoverController = popoverController;
}

- (void) splitViewController:(MGSplitViewController *)splitController
	  willShowViewController:(UIViewController *)viewController
   invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	self.navigationItem.leftBarButtonItems = [NSArray arrayWithObject:self.exitButton];
	self.masterPopoverController = nil;
}

- (void)splitViewController:(MGSplitViewController*)svc 
		  popoverController:(UIPopoverController*)pc 
  willPresentViewController:(UIViewController *)aViewController
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (void)splitViewController:(MGSplitViewController*)svc willChangeSplitOrientationToVertical:(BOOL)isVertical
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (void)splitViewController:(MGSplitViewController*)svc willMoveSplitToPosition:(float)position
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
}


- (float)splitViewController:(MGSplitViewController *)svc constrainSplitPosition:(float)proposedPosition splitViewSize:(CGSize)viewSize
{
	//NSLog(@"%@", NSStringFromSelector(_cmd));
	return proposedPosition;
}

#pragma mark - UITableview Delegate/Datasource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)inTableView
{
	return self.tableArrayStrings.count;
}

- (NSInteger) tableView:(UITableView *)inTableView numberOfRowsInSection:(NSInteger)inSection
{
	NSArray *theArray = self.tableArrayStrings[inSection];
	return theArray.count;
}

- (UITableViewCell *) tableView:(UITableView *)inTableView cellForRowAtIndexPath:(NSIndexPath *)inIndexPath
{	static NSString *reuseIdentifier = @"contactDescriptionCell";

	contactDescriptionCell *theCell = [inTableView dequeueReusableCellWithIdentifier:reuseIdentifier];
	if (theCell == nil) {
		theCell = [[contactDescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
	}

	NSArray *theArray = self.tableArrayStrings[inIndexPath.section];
	NSDictionary *theDictionary = theArray[inIndexPath.row];

//	NSLog([NSString stringWithFormat:@"tableView:cellForRowAtIndexPath: %@", inIndexPath.description], @"");
	theCell.titleString = [[theDictionary allKeys] objectAtIndex:0];
	theCell.descriptionString = [[theDictionary allValues] objectAtIndex:0];

	return theCell;
}

- (BOOL) tableView:(UITableView *)inTableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Return NO if you do not want the specified item to be editable.
	return NO;
}

- (CGFloat) tableView:(UITableView *)inTableView heightForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	NSArray *theArray = self.tableArrayStrings[inIndexPath.section];
	NSDictionary *theDictionary = theArray[inIndexPath.row];

	NSString *theString = [[theDictionary allValues] objectAtIndex:0];

	return [contactDescriptionCell rowHeightWithString:theString font:nil landscape:UIInterfaceOrientationIsLandscape(self.interfaceOrientation)];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{	
	NSDictionary *cellStrings = self.tableArrayStrings[indexPath.section][indexPath.row];
	NSString *cellTitleString = [[cellStrings allKeys] objectAtIndex:0];
	NSString *cellDescriptionString = [[cellStrings allValues] objectAtIndex:0];
	if ([cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"EMAIL_1", @"ASContact", nil)] || [cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"EMAIL_2", @"ASContact", nil)] || [cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"EMAIL_3", @"ASContact", nil)]) {
		
		// Should be using mail API for this
		UINavigationController *navController = [composeMailVC createNavigationMailComposerWithTo:[NSArray arrayWithObject:cellDescriptionString] cc:nil bcc:nil attachments:nil account:[BaseAccount accountForAccountNumber:0]];
		[self presentViewController:navController animated:true completion:nil];
		
	} else if ([cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"URL", @"ASContact", nil)]) {

		[[SZLConcreteApplicationContainer sharedApplicationContainer] launchBrowserWithURL:[NSURL URLWithString:cellDescriptionString] closeCompletionHandler:nil];
		
	} else if  ([cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"MOBILE", @"ASContact", nil)] || [cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", nil)] || [cellTitleString isEqualToString:FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", nil)] ) {
		
		if (IS_IPHONE()){
			BOOL didOpen = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", [cellDescriptionString stringByReplacingOccurrencesOfString:@" " withString:@"-"]]]];
			if (!didOpen) {
				FXDebugLog(kFXLogContacts, @"Phone could not be launched");
			}
			// If no sim card, shows popup and launches native contact app. How to stop this?
		}
	}
}


#pragma mark - Actions

- (void) editButtonPushed:(id)inSender
{
	editContactVC *theController = nil;
    if (IS_IPHONE()) {
	    theController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPhone identifier:@"editContactVC"];
    } else if (IS_IPAD()) {
	    theController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPad identifier:@"editContactVC"];
	}
    theController.delegate = self;
	[theController setContact:self.contact newContact:NO];
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:theController];
	navController.modalPresentationStyle = UIModalPresentationPageSheet;
	[self.navigationController presentViewController:navController animated:YES completion:nil];

	return;
}

#pragma mark - FXAEditContractVCDelegate
- (void)editContactVC:(editContactVC *)editController didSaveContact:(ASContact *)contact
{
}

- (void)editContactVC:(editContactVC *)editController didDeleteContact:(ASContact *)contact
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)editContactVC:(editContactVC *)editController didFailAction:(ASContact *)contact withKey:(NSString *)key
{
    if([key isEqualToString:kContactDeletedFromServerNotification]) {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
    }
}

#pragma mark - Private methods

- (void) setupControls
{
	[self redoControls];
	[self setupButtonsForOrientation:self.interfaceOrientation];
}

- (void) setupNotifications
{
	// we just want to see if the contact just updated is this one, and if so, reload it
	// -------------------------------------------------------------------------------
	[[NSNotificationCenter defaultCenter] addObserverForName:kContactUpdatedFromServerNotification object:nil queue:nil
		usingBlock:^(NSNotification *inNotification) {
			NSString *theServerID = inNotification.userInfo[kUserInfoContactServerID];
			if ([theServerID caseInsensitiveCompare:self.contact.serverId] == NSOrderedSame) {
				ASContact *reloadedContact = [FXDatabase getContactWithServerID:theServerID];
				self.contact = reloadedContact;
			}
		}
	];

	return;
}

- (void) redoControls
{
	if (self.contact != nil) {
		UIImage *theImage = self.contact.image;
		if (theImage == nil) {
			if (self.contact.isCompany) {
				theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/blankCompany.png"];
			} else {
				theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/blankContact.png"];
			}
		}
		self.contactImage.image = theImage;

		if (self.contact.isCompany) {
			self.fullnameLabel.text = self.contact.company;
			self.companyLabel.text = @"";
		} else {
			self.fullnameLabel.text = self.contact.displayName;
			self.companyLabel.text = self.contact.company;
		}
		self.editButton.enabled = true;
	} else {
		self.contactImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/blankContact.png"];
		self.fullnameLabel.text = @"";
		self.companyLabel.text = @"";
		self.editButton.enabled = false;
	}

//	NSString *theMessage = @"Add to Favorites";
//	if (self.contact.isFavorite) theMessage = @"Remove Favorite";
//	[self.favoritesButton setTitle:theMessage forState:UIControlStateNormal];

	return;
}

- (void) setupButtonsForOrientation:(UIInterfaceOrientation)orientation
{
	if (IS_IPHONE()) return;

	if (UIInterfaceOrientationIsLandscape(orientation)) {
		self.navigationItem.leftBarButtonItems = @[self.exitButton];
	}

	return;
}

- (void) reloadTable
{
	self.tableArrayStrings = _contact.tableArrayStrings;
	[self.tableView reloadData];
}

@end

