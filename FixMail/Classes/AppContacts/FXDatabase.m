//
//  FXDatabase.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-12.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import "FXDatabase.h"
#import "FXContactsHeader.h"
#import "ASContact.h"
#import "ASContact+FMDatabase.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabaseQueue.h"
#import "FXContactUtils.h"

#define kTableNameGAL			@"galContacts"
#define kTableNameUser			@"userContacts"
#define kTableNameRecipients	@"recipientContacts"

// from SearchRunner - fields used to return contact data back
static const NSString* kEmailAddresses  = @"emailAddresses";
static const NSString* kName            = @"name";
static const NSString* kDbMin           = @"dbMin";
static const NSString* kDbMax           = @"dbMax";

static bool sInitialized = false;

#define FMDBQuickCheck(SomeBool) { if (!(SomeBool)) { NSLog(@"Failure on line %d", __LINE__); abort(); } }

//										@"dbNum integer, dbMin integer, dbMax integer,"\

#define kFullContactTableCreationSQL	@"contactID integer primary key autoincrement not null,"\
										@"contactType integer,"\
										@"serverID text,"\
										@"firstName text, middleName text, lastName text, company text, mobilePhone text, pager text,"\
										@"emailAddress1 text, emailAddress2 text, emailAddress3 text,"\
										@"companyStreet text, companyCity text, companyState text, companyPostalCode text, companyCountry text,"\
										@"companyPhone1 text, companyPhone2 text, companyFax text,"\
										@"homeStreet text, homeCity text, homeState text, homePostalCode text, homeCountry text,"\
										@"homePhone1 text, homePhone2 text, homeFax text,"\
										@"otherStreet text, otherCity text, otherState text, otherPostalCode text, otherCountry text,"\
										@"webPage text, notes text, pictureData blob"

@implementation FXDatabase

// Constants
static NSString* kDatabaseName = @"contacts2.tdb";

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static FMDatabaseQueue *sFMDatabaseQueue = nil;

+ (FXDatabase *)sharedManager
{
    static FXDatabase *sSharedSQLiteManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sSharedSQLiteManager = [[FXDatabase alloc] init];
        // Do any other initialisation stuff here
    });
    return sSharedSQLiteManager;
}

+ (FMDatabaseQueue *) defaultFMDatabaseQueue
{
	[FXDatabase initializeDB];
	return sFMDatabaseQueue;
}

+ (NSString*)path
{
	NSString *documentsDirectory = [FXLSafeZone documentsDirectory];
    return [documentsDirectory stringByAppendingPathComponent:kDatabaseName];
}

//
// 1) make sure the file exists (Obviously)
// 2) check that each table exists within the file
//		-> if not, then create it
//
+ (void) initializeDB
{
	if (sInitialized) return;

	NSString *dbPath = [FXDatabase path];

    sFMDatabaseQueue = [FXLSafeZone secureDatabaseQueueWithPath:dbPath];
	NSAssert(sFMDatabaseQueue != nil, @"sFMDatabaseQueue != nil");
	FMDBRetain(sFMDatabaseQueue);

	// create the tables:
	[sFMDatabaseQueue inDatabase:^(FMDatabase *inDB) {

			[inDB open];

			[inDB executeUpdate:@"CREATE TABLE IF NOT EXISTS " kTableNameGAL " (" kFullContactTableCreationSQL ");"];
			[inDB executeUpdate:@"CREATE TABLE IF NOT EXISTS " kTableNameUser " (" kFullContactTableCreationSQL ");"];
			[inDB executeUpdate:@"CREATE TABLE IF NOT EXISTS " kTableNameRecipients " (" kFullContactTableCreationSQL ");"];

			[inDB close];
		}
	];

	sInitialized = true;
	return;
}

+ (void) terminateDB
{
    if(sFMDatabaseQueue) {
        FMDBRelease(sFMDatabaseQueue);
        sFMDatabaseQueue = nil;
    }
    return;
}

+ (void) deleteContactDatabases
{
	[[FXLSafeZone secureFileManager] removeItemAtPath:[FXDatabase path] error:nil];
    
    sInitialized = false;
}

+ (NSMutableArray *) contactList:(int)inContactType
{
	NSString *theTableName = [FXDatabase tableNameForContactType:inContactType];

	[FXDatabase initializeDB];

	NSMutableArray *theArray = [NSMutableArray array];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
			NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM %@", theTableName];

			[inDB open];
			FMResultSet *theResults = [inDB executeQuery:queryString];

			while ([theResults next]) {
				ASContact *theContact = [[ASContact alloc] initWithFMResults:theResults];
				[theArray addObject:theContact];
			}

			[inDB close];
		}
	];

	return theArray;
}

+ (NSMutableArray *) contactListForName:(NSString *)name
{
	// Similar to findContacts, which returns contacts as a dictionary.
	// This searches in firstName, middleName, lastName, company name, and all email addresses.
	
	// get rid of any wild-cards... I'll handle those myself
	NSString *thePattern = [name stringByReplacingOccurrencesOfString:@"*" withString:@""];
	
	NSMutableArray *theArray = [NSMutableArray array];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
		
		[inDB open];
		
		NSString *queryString = nil;
		FMResultSet *theResults = nil;
		
		// Search all 3 contacts tables
		for (NSString *tableName in @[kTableNameUser, kTableNameRecipients, kTableNameGAL]) {
			if (thePattern.length > 0) {
				
				queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE firstName LIKE '%%%@%%' OR lastName LIKE '%%%@%%' OR middleName LIKE '%%%@%%' OR company LIKE '%%%@%%' OR emailAddress1 LIKE '%%%@%%' OR emailAddress2 LIKE '%%%@%%' OR emailAddress3 LIKE '%%%@%%'", tableName, thePattern, thePattern, thePattern, thePattern, thePattern, thePattern, thePattern];
			} else {
				queryString = [NSString stringWithFormat:@"SELECT * FROM %@", kTableNameUser];
			}
			theResults = [inDB executeQuery:queryString];
			while ([theResults next]) {
				ASContact *theContact = [[ASContact alloc] initWithFMResults:theResults];
				[theArray addObject:theContact];
			}
		}		
		[inDB close];
	}
	 ];
	
	return theArray;
}


+ (NSMutableArray *) contactListForEmailAddress:(NSString *)emailAddress
{
	// Similar to findContacts, which returns contacts as a dictionary.
	
	// get rid of any wild-cards... I'll handle those myself
	NSString *thePattern = [emailAddress stringByReplacingOccurrencesOfString:@"*" withString:@""];
	
	NSMutableArray *theArray = [NSMutableArray array];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
		
		[inDB open];
		
		NSString *queryString = nil;
		FMResultSet *theResults = nil;
		if (thePattern.length > 0) {
			NSString *tmpPattern = [NSString stringWithFormat:@"%%%@%%", thePattern];
			queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE emailAddress1 LIKE ? OR emailAddress2 LIKE ? OR emailAddress3 LIKE ?", kTableNameUser];
			theResults = [inDB executeQuery:queryString, tmpPattern, tmpPattern, tmpPattern];
		} else {
			queryString = [NSString stringWithFormat:@"SELECT * FROM %@", kTableNameUser];
			theResults = [inDB executeQuery:queryString];
		}
		while ([theResults next]) {
			ASContact *theContact = [[ASContact alloc] initWithFMResults:theResults];
			[theArray addObject:theContact];
		}

		if (thePattern.length > 0) {
			NSString *tmpPattern = [NSString stringWithFormat:@"%%%@%%", thePattern];
			queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE emailAddress1 LIKE ? OR emailAddress2 LIKE ? OR emailAddress3 LIKE ?", kTableNameRecipients];
			theResults = [inDB executeQuery:queryString, tmpPattern, tmpPattern, tmpPattern];
		} else {
			queryString = [NSString stringWithFormat:@"SELECT * FROM %@", kTableNameUser];
			theResults = [inDB executeQuery:queryString];
		}
		while ([theResults next]) {
			ASContact *theContact = [[ASContact alloc] initWithFMResults:theResults];
			[theArray addObject:theContact];
		}

		[inDB close];
	}
	 ];
	
	return theArray;
}

+ (ASContact *) createContact:(NSString *)inFirstName last:(NSString *)inLastName email:(NSString *)inEmail phone:(NSString *)inPhone list:(NSMutableArray *)inList
{
	if (inList == nil) return nil;

	ASContact *theContact = [[ASContact alloc] init];
	if (inFirstName.length > 0) theContact.firstName = inFirstName;
	if (inLastName.length > 0) theContact.lastName = inLastName;
	if (inEmail.length > 0) theContact.emailAddress1 = inEmail;
	if (inPhone.length > 0) theContact.mobilePhone = inPhone;
	theContact.company = @"Fixmo Inc.";
	theContact.isFavorite = false;
	theContact.isNew = false;

	// add it to the sectionator
	[inList addObject:theContact];

	return theContact;
}

+ (void) commitChanges:(NSArray *)inList
{
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
			[inDB open];
			[inDB beginTransaction];

			NSString *theTableName = [FXDatabase tableNameForContactType:contactUser];
			for (ASContact *theContact in inList) {
				// check if already in there
				NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE serverID = ?", theTableName];
				FMResultSet *theResults = [inDB executeQuery:queryString, theContact.serverId];

				if ([theResults next]) {
					[theContact updateIntoFMDatabase:inDB table:theTableName];
				} else {
					[theContact insertIntoFMDatabase:inDB table:theTableName];
				}
			}

			[inDB commit];
			[inDB close];
		}
	];

	return;
}

+ (void) deleteContactList:(int)inContactType
{
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {

			NSString *theTableName = [FXDatabase tableNameForContactType:inContactType];
			NSString *dropString = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@;", theTableName];
			NSString *createString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ ( %@ );", theTableName, kFullContactTableCreationSQL];

			[inDB open];
			[inDB executeUpdate:dropString];
			[inDB executeUpdate:createString];
			[inDB close];
		}
	];

	return;
}

+ (void) addContactWithEmail:(NSString *)inEmail name:(NSString *)inName contact:(int)inContactType
{
	[FXDatabase initializeDB];

	// break up the name into first and last name
	NSString *emailAddress = inEmail;
	NSString *firstName = nil;
	NSString *lastName = nil;

	NSArray *args = [inName componentsSeparatedByString:@" "];
	if (args.count == 1) {
		firstName = [args[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	} else {
		firstName = [args[0] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		lastName = [args[args.count-1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	}
	NSString *theTableName = [FXDatabase tableNameForContactType:inContactType];
	if (emailAddress == nil) emailAddress = @"";
	if (firstName == nil) firstName = @"";
	if (lastName == nil) lastName = @"";

	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
			bool addEmailAddress = true;

			// first check if it's already in the DB
            [inDB open];
			if (emailAddress.length > 0) {
				NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE emailAddress1 = ?", theTableName];
				FMResultSet *theResults = [inDB executeQuery:queryString, emailAddress];
				if ([theResults next]) addEmailAddress = false;
			}
			if (addEmailAddress) {
				NSString *insertStatement = [NSString stringWithFormat:@"INSERT INTO %@ (contactType, firstName, lastName, emailAddress1) values (?, ?, ?, ?)", theTableName];
				[inDB beginTransaction];
				[inDB executeUpdate:insertStatement, [NSNumber numberWithInt:inContactType], firstName, lastName, emailAddress];
				[inDB commit];
			}
            [inDB close];
		}
	];

	return;
}

+ (ASContact *) getContactWithServerID:(NSString *)inServerID
{
	// will only be in userContacts since email recipients are created on the fly and GAL contacts are retrieved from server
	NSString *theTableName = [FXDatabase tableNameForContactType:contactUser];
	NSMutableArray *theArray = [NSMutableArray array];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
			// first check if it's already in the DB
			NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE serverID = ?", theTableName];

			[inDB open];
			FMResultSet *theResults = [inDB executeQuery:queryString, inServerID];
			while ([theResults next]) {
				ASContact *theContact = [[ASContact alloc] initWithFMResults:theResults];
				[theArray addObject:theContact];
			}

			[inDB close];
		}
	];

	// just get the first one
	NSAssert(theArray.count <= 1, @"theArray.count <= 1");
	if (theArray.count == 0) return nil;

	return (ASContact *)theArray[0];
}

+ (void) deleteContactWithServerID:(NSString *)inServerID
{
	// will only be in userContacts since email recipients are created on the fly and GAL contacts are retrieved from server
	NSString *theTableName = [FXDatabase tableNameForContactType:contactUser];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
			// first check if it's already in the DB
			NSString *queryString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE serverID = ?", theTableName];

			[inDB open];

			[inDB executeUpdate:queryString, inServerID];

			[inDB close];
		}
	];

	return;
}

+ (void) deleteContact:(ASContact *)inContact
{
	// two ways to delete here:
	// 1: check if serverID non-null
	// 2: use the contactID (assigned inside the db)

	if (inContact.serverId.length > 0) {
		[FXDatabase deleteContactWithServerID:inContact.serverId];
	} else {
		NSString *theTableName = [FXDatabase tableNameForContactType:inContact.contactType];
		[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {
				// first check if it's already in the DB
				NSString *queryString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE contactID = ?", theTableName];

				[inDB open];

				[inDB executeUpdate:queryString, [NSNumber numberWithInt:inContact.contactID]];

				[inDB close];
			}
		];
	}

	return;
}

+ (NSMutableArray *) findContacts:(NSString *)inEmailAddress
{
	// get rid of any wild-cards... I'll handle those myself
	NSString *thePattern = [inEmailAddress stringByReplacingOccurrencesOfString:@"*" withString:@""];

	NSMutableArray *theArray = [NSMutableArray array];
	[[FXDatabase defaultFMDatabaseQueue] inDatabase:^(FMDatabase *inDB) {

			[inDB open];

			NSString *queryString = nil;
			FMResultSet *theResults = nil;
			if (thePattern.length > 0) {
				queryString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE emailAddress1 LIKE '%%%@%%'", kTableNameUser, thePattern];
			} else {
				queryString = [NSString stringWithFormat:@"SELECT * FROM %@", kTableNameUser];
			}
			theResults = [inDB executeQuery:queryString];
			while ([theResults next]) {
				NSString *firstName = [theResults stringForColumn:@"firstName"];
				NSString *middleName = [theResults stringForColumn:@"middleName"];
				NSString *lastName = [theResults stringForColumn:@"lastName"];
				NSString *fullName = [FXContactUtils makeFullName:firstName middle:middleName last:lastName];
				NSString *emailAddress = [theResults stringForColumn:@"emailAddress1"];
//				int dbMin = [theResults intForColumn:@"dbMin"];
//				int dbMax = [theResults intForColumn:@"dbMax"];
				int dbMin = 0;
				int dbMax = 0;
				NSDictionary *theDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
											   fullName,						kName,
											   emailAddress,					kEmailAddresses,
											   [NSNumber numberWithInt:dbMin],	kDbMin,
											   [NSNumber numberWithInt:dbMax],	kDbMax,
											   nil];
				[theArray addObject:theDictionary];
			}

			[inDB close];
		}
	];

	return theArray;
}

+ (NSString *) tableNameForContactType:(int)inContactType
{
	NSString *theString = kTableNameUser;
	switch (inContactType) {
		case contactGAL: theString = kTableNameGAL; break;
		case contactEmailRecipients: theString = kTableNameRecipients; break;
		default:
			break;
	}

	return theString;
}

////////////////////////////////////////////////////////////////////////////////////////////
// DBaseEngine Virtual overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Virtual overrides

- (void)open:(BaseAccount*)anAccount
{
	[FXDatabase initializeDB];
}

- (unsigned long long)spaceUsed
{
    unsigned long long aSpaceUsed = 0;
    
    aSpaceUsed += [DBaseEngine sizeForPath:[FXDatabase path]];
    
    return aSpaceUsed;
}

- (void)shutDown
{
    [FXDatabase terminateDB];
}

- (void)deleteAccount
{
	[FXDatabase deleteContactDatabases];
}

- (NSString*)displayName
{
    return @"Contacts";
}

@end
