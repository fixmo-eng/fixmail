//
//  contactDescriptionCell.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-06.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

@class FXLabel;

@interface contactDescriptionCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet FXLabel *descriptionLabel;

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSString *descriptionString;

+ (CGFloat) rowHeight;
+ (CGFloat) rowHeightWithString:(NSString *)inString
						   font:(UIFont *)inFont
					  landscape:(bool)inLandscape;

@end
