//
//  contactSearchCell.m
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-05.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import "contactSearchCell.h"
#import "ASContact.h"
#import "UIView-ViewFrameGeometry.h"
#import "AppSettings.h"

#define kNamePadding	5.0
#define kLabelX			10.0

@implementation contactSearchCell

- (void) layoutSubview
{
	[super layoutSubviews];
	
	// make sure our image is always to the right by a fixed distance
	self.characteristicImage.right = self.contentView.right - 10.0;
}

#pragma mark - Static methods

+ (CGFloat) rowHeight
{
	return 50.0;
}

#pragma mark - Getters/Setters

- (void) setContact:(ASContact *)inContact
{
	// we COULD just return, but some of the content might have changed
	// when the table gets redrawn and if we just returned, this wouldn't
	// get updated... so we have to re-check
	// -------------------------------------------------------------------------------
	if (inContact != _contact) {
		_contact = inContact;
	}

	[self redoControls];
}

- (ASContact *) contact
{
	return _contact;
}

#pragma mark - Private methods

- (void) redoControls
{
	if (_contact == nil) {
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = true;
		self.characteristicImage.hidden = true;
		return;
	}

	if (_contact.isCompany) {
		// only use the lastNameLabel
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = false;

		self.lastNameLabel.text = self.contact.company;
		[self.lastNameLabel sizeToFit];
		self.lastNameLabel.left = kLabelX;

		self.characteristicImage.image = nil;

	} else {
		self.firstNameLabel.hidden = false;
		self.lastNameLabel.hidden = false;

		self.firstNameLabel.text = self.contact.firstAndMiddleName;
		self.lastNameLabel.text = self.contact.lastName;
		[self.firstNameLabel sizeToFit];
		[self.lastNameLabel sizeToFit];

		if ([AppSettings contactsDisplayOrder] == sortOrderFirstLast) {
			self.firstNameLabel.left = kLabelX;
			self.lastNameLabel.left = self.firstNameLabel.right + kNamePadding;
		} else {
			self.lastNameLabel.left = kLabelX;
			self.firstNameLabel.left = self.lastNameLabel.right + kNamePadding;
		}

		if (self.contact.isMe) self.characteristicImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/meIcon.png"];
		else if (self.contact.isFavorite) self.characteristicImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/faveIcon.png"];
		else self.characteristicImage.image = nil;
	}

	if (_contact.matchedString == nil) {
		self.criteriaLabel.hidden = false;
		self.matchedStringLabel.hidden = false;
		self.criteriaLabel.text = FXLLocalizedStringFromTable(@"UNKNOWN", @"ASContact", @"Unknown is displayed in criteria label when matched string is nil");
		self.matchedStringLabel.text = FXLLocalizedStringFromTable(@"MATCHED_STRING_NIL", @"ASContact", @"?? is displayed in matched string label when matched string is nil");
	} else {
		self.criteriaLabel.hidden = false;
		self.matchedStringLabel.hidden = false;
		self.criteriaLabel.text = _contact.matchedCriteria;
		self.matchedStringLabel.text = _contact.matchedString;
	}
	[self.criteriaLabel sizeToFit];
	[self.matchedStringLabel sizeToFit];
	self.matchedStringLabel.left = self.criteriaLabel.right + kNamePadding;
	
	return;
}

@end
