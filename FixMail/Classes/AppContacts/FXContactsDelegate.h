//
//  FixContactsSafeZoneDelegate.h
//  FixMail
//
//  Created by Colin Biggin on 2012-12-05.
//
//

#import "SZLApplicationContainer.h"

@interface FXContactsDelegate : NSObject <SZLApplicationContainerDelegate, SZLConfigurationDelegate>

@property (nonatomic, assign) id <SZLApplicationContainer> container;

@end
