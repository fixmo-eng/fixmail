//
//  FXContactUtils.m
//  FixMail
//
//  Created by Colin Biggin on 2012-12-06.
//
//
#import "FXContactUtils.h"

@implementation FXContactUtils

// our image has to be a maximum file size of 36Kb (so 48Kb in base64). This translates to
// a maximum image of approximately (assuming 32bits per pixel):
// sqrt(36 x 1024 / 4) = 96

+ (NSData *) graduatedImage:(UIImage *)inImage quality:(float)inQuality
{
	float theQuality = inQuality;
	if (theQuality > 1.0) theQuality = 1.0;
	else if (theQuality < 0.1) theQuality = 0.1;
	CGRect resizeRect = CGRectMake(0.0f, 0.0f, inImage.size.width * theQuality, inImage.size.height * theQuality);

	UIGraphicsBeginImageContext(resizeRect.size);
	[inImage drawInRect:resizeRect];
	UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(resizedImage)];

	FXDebugLog(kFXLogActiveSync, @"graduatedImage (%.1f): %d x %d, %d bytes", theQuality, (int)resizeRect.size.width, (int)resizeRect.size.height, imageData.length);

	return imageData;
}

+ (NSData *) getImage:(UIImage *)inImage maxSize:(int)inMaxSize
{
	NSData *imageData = nil;
	for (float theQuality = 1.0; theQuality > 0.005;) {
		imageData = [FXContactUtils graduatedImage:inImage quality:theQuality];
		if (imageData.length < inMaxSize) break;
		imageData = nil;
		if (theQuality > 0.1) theQuality -= 0.1;
		else theQuality -= 0.01;
	}

	return imageData;
}

+ (NSString *) extractEmailAddress:(NSString *)inString
{	NSString *theEmailAddress = nil;

	if (inString == nil) return nil;
	if (inString.length == 0) return nil;

	// from MSExchange servers, we get an address like this:
	// "Foghorn Leghorn" <foghorn.leghorn@fixmo.com>
	// so obviously, we only want whats between the brackets

	NSRange startRange = [inString rangeOfString:@"<"];
	if (startRange.length > 0) {
		theEmailAddress = [inString substringFromIndex:(startRange.location + startRange.length)];
		NSRange endRange = [theEmailAddress rangeOfString:@">"];
		if (endRange.length > 0) {
			theEmailAddress = [theEmailAddress substringToIndex:endRange.location];
		}
	} else {
		theEmailAddress = inString;
	}

	// get rid of any extraneous spaces & quotes
	theEmailAddress = [theEmailAddress stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	theEmailAddress = [theEmailAddress stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\""]];

	return theEmailAddress;
}

+ (NSString *) makeFullName:(NSString *)inFirstName middle:(NSString *)inMiddleName last:(NSString *)inLastName;
{
	NSString *theString = @"";
	if ((inFirstName.length > 0) && (inMiddleName.length > 0) && (inLastName.length > 0)) {
		theString = [NSString stringWithFormat:@"%@ %@ %@", inFirstName, inMiddleName, inLastName];
	} else if ((inFirstName.length > 0) && (inLastName.length > 0)) {
		theString = [NSString stringWithFormat:@"%@ %@", inFirstName, inLastName];
	} else if (inFirstName.length > 0) {
		theString = inFirstName;
	} else if (inLastName.length > 0) {
		theString = inLastName;
	}

	return theString;
}

@end
