//
//  contactAttributedCell.h
//  FXContacts
//
//  Created by Colin Biggin on 2012-11-05.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//
#import "FXContactsHeader.h"

@class ASContact;

@interface contactAttributedCell : UITableViewCell

@property (nonatomic, strong) ASContact *contact;

@property (nonatomic, strong) IBOutlet UILabel *firstNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *lastNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *characteristicImage;

+ (CGFloat) rowHeight;

@end
