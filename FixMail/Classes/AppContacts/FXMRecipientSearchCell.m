//
//  FXMRecipientSearchCell.m
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-06-20.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMRecipientSearchCell.h"
#import "ASContact.h"
#import "AppSettings.h"
#import "UIView-ViewFrameGeometry.h"

#define kNamePadding	5.0
#define kLabelX			10.0

@implementation FXMRecipientSearchCell
@synthesize contact = _contact;

- (void) layoutSubview
{
	[super layoutSubviews];
	
}

#pragma mark - Static methods

+ (CGFloat) rowHeight
{
	return 50.0;
}

#pragma mark - Getters/Setters

- (void) setContact:(ASContact *)inContact
{
	// we COULD just return, but some of the content might have changed
	// when the table gets redrawn and if we just returned, this wouldn't
	// get updated... so we have to re-check
	// -------------------------------------------------------------------------------
	if (inContact != _contact) {
		_contact = inContact;
	}
	
	[self redoControls];
}

- (ASContact *) contact
{
	return _contact;
}

#pragma mark - Private methods

- (void) redoControls
{
	if (_contact == nil) {
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = true;
		return;
	}
	
	if (self.contact.isCompany) {
		// only use the lastNameLabel
		self.firstNameLabel.hidden = true;
		self.lastNameLabel.hidden = false;
		
		self.lastNameLabel.text = self.contact.company;
		[self.lastNameLabel sizeToFit];
		//self.lastNameLabel.left = kLabelX;
		
	} else {
		self.firstNameLabel.hidden = false;
		self.lastNameLabel.hidden = false;
		
		self.firstNameLabel.text = self.contact.firstAndMiddleName;
		self.lastNameLabel.text = self.contact.lastName;
		[self.firstNameLabel sizeToFit];
		[self.lastNameLabel sizeToFit];
		
		if ([AppSettings contactsDisplayOrder] == sortOrderFirstLast) {
			self.firstNameLabel.left = kLabelX;
			self.lastNameLabel.left = self.firstNameLabel.right + kNamePadding;
		} else {
			self.lastNameLabel.left = kLabelX;
			self.firstNameLabel.left = self.lastNameLabel.right + kNamePadding;
		}
	}
	
	ERecipientSearchMatch match = [self.contact matchEmailOrNameSearchString:self.matchedSearchString];
	switch (match) {
		case kEmailAddress1Match:
			self.emailLabel.text = self.contact.emailAddress1; 
			break;
		case kEmailAddress2Match:
			self.emailLabel.text = self.contact.emailAddress2;
			break;
		case kEmailAddress3Match:
			self.emailLabel.text = self.contact.emailAddress3;
			
		// if name matches should add contacts for each email address
			
		default:
			self.emailLabel.text = self.contact.emailAddress1; 
			break;
	}
	
	if (self.contact.matchedString == nil) {
		
	} else {
		
	}
	
	return;
}

@end
