//
//  ASContact+FMDatabase.m
//  FXContactsApp
//
//  Created by Colin Biggin on 2012-11-27.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//
#import "ASContact+FMDatabase.h"
#import "BaseAccount.h"
#import "ErrorAlert.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMResultSet.h"
#import "FXDatabase.h"

@implementation ASContact (FMDatabase)

- (ASContact *) initWithFMResults:(FMResultSet *)inResults
{
	NSAssert(inResults != nil, @"inResults != nil");

	self.contactID = [inResults intForColumn:@"contactID"];
	self.contactType = [inResults intForColumn:@"contactType"];
	self.serverId = [inResults stringForColumn:@"serverID"];
	self.firstName = [inResults stringForColumn:@"firstName"];
	self.middleName = [inResults stringForColumn:@"middleName"];
	self.lastName = [inResults stringForColumn:@"lastName"];
	self.company = [inResults stringForColumn:@"company"];
	self.mobilePhone = [inResults stringForColumn:@"mobilePhone"];
	self.pager = [inResults stringForColumn:@"pager"];
	self.emailAddress1 = [inResults stringForColumn:@"emailAddress1"];
	self.emailAddress2 = [inResults stringForColumn:@"emailAddress2"];
	self.emailAddress3 = [inResults stringForColumn:@"emailAddress3"];

	self.companyStreet = [inResults stringForColumn:@"companyStreet"];
	self.companyCity = [inResults stringForColumn:@"companyCity"];
	self.companyState = [inResults stringForColumn:@"companyState"];
	self.companyPostalCode = [inResults stringForColumn:@"companyPostalCode"];
	self.companyCountry = [inResults stringForColumn:@"companyCountry"];
	self.companyPhone1 = [inResults stringForColumn:@"companyPhone1"];
	self.companyPhone2 = [inResults stringForColumn:@"companyPhone2"];
	self.companyFax = [inResults stringForColumn:@"companyFax"];

	self.homeStreet = [inResults stringForColumn:@"homeStreet"];
	self.homeCity = [inResults stringForColumn:@"homeCity"];
	self.homeState = [inResults stringForColumn:@"homeState"];
	self.homePostalCode = [inResults stringForColumn:@"homePostalCode"];
	self.homeCountry = [inResults stringForColumn:@"homeCountry"];
	self.homePhone1 = [inResults stringForColumn:@"homePhone1"];
	self.homePhone2 = [inResults stringForColumn:@"homePhone2"];
	self.homeFax = [inResults stringForColumn:@"homeFax"];

	self.otherStreet = [inResults stringForColumn:@"otherStreet"];
	self.otherCity = [inResults stringForColumn:@"otherCity"];
	self.otherState = [inResults stringForColumn:@"otherState"];
	self.otherPostalCode = [inResults stringForColumn:@"otherPostalCode"];
	self.otherCountry = [inResults stringForColumn:@"otherCountry"];

	self.webPage = [inResults stringForColumn:@"webPage"];
	self.notes = [inResults stringForColumn:@"notes"];
	self.picture = [inResults dataForColumn:@"pictureData"];

	// if we have a serverID, then we need the folder to point to
	BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
	if (currentAccount != nil) {
		if (self.serverId.length > 0) {
			NSArray *folders = [currentAccount foldersForFolderType:kFolderTypeContacts];
			if (folders.count > 0) {
				self.folder = (ASFolder *)folders[0];
			}
		}
	}

	return self;
}

- (void)_reportError:(FMDatabase *)aDatabase
{
    NSError* anError = [aDatabase lastError];
    NSString* anErrorMessage = [anError localizedDescription];
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CONTACT_DATABASE", @"ErrorAlert", @"ASContact+FMDatabase error alert view title") message:anErrorMessage];
}

- (bool) insertIntoFMDatabase:(FMDatabase *)inDB table:(NSString *)inTableName
{
	if (inDB == nil) return false;
	if (inTableName.length == 0) return false;

	// use the contactID to determine whether an insertion or and update
	NSString *insertStatement;
	if (!self.isInLocalDB) {
		FXDebugLog(kFXLogContacts, @"New contactID(%d) table:(%@) serverID(%@)", self.contactID, inTableName, self.serverId);
		insertStatement = [NSString stringWithFormat:@"INSERT INTO %@ ("\
			@"contactType, serverID, "\
			@"firstName, middleName, lastName, company, mobilePhone, pager, "\
			@"emailAddress1, emailAddress2, emailAddress3, "\
			@"companyStreet, companyCity, companyState, companyPostalCode, companyCountry, "\
			@"companyPhone1, companyPhone2, companyFax, "\
			@"homeStreet, homeCity, homeState, homePostalCode, homeCountry, "\
			@"homePhone1, homePhone2, homeFax, "\
			@"otherStreet, otherCity, otherState, otherPostalCode, otherCountry, "\
			@"webPage, notes, pictureData ) VALUES ("\
			@"?, ?, "\
			@"?, ?, ?, ?, ?, ?, "\
			@"?, ?, ?, "\
			@"?, ?, ?, ?, ?, ?, ?, ?, "\
			@"?, ?, ?, ?, ?, ?, ?, ?, "\
			@"?, ?, ?, ?, ?, "\
			@"?, ?, ?"\
			@")", inTableName];
	} else {
		FXDebugLog(kFXLogContacts, @"Updating contactID(%d) table:(%@) serverID(%@)", self.contactID, inTableName, self.serverId);
		insertStatement = [NSString stringWithFormat:@"UPDATE %@ SET "\
				@"contactType = ?, serverID = ?, firstName = ?, middleName = ?, lastName = ?, company = ?, mobilePhone = ?, pager = ?, "\
				@"emailAddress1 = ?, emailAddress2 = ?, emailAddress3 = ?, "\
				@"companyStreet = ?, companyCity = ?, companyState = ?, companyPostalCode = ?, companyCountry = ?, "\
				@"companyPhone1 = ?, companyPhone2 = ?, companyFax = ?, "\
				@"homeStreet = ?, homeCity = ?, homeState = ?, homePostalCode = ?, homeCountry = ?, "\
				@"homePhone1 = ?, homePhone2 = ?, homeFax = ?, "\
				@"otherStreet = ?, otherCity = ?, otherState = ?, otherPostalCode = ?, otherCountry = ?, "\
				@"webPage = ?, notes = ?, pictureData = ?"\
				@" WHERE contactID = %d", inTableName, self.contactID];
	}

	[inDB executeUpdate:insertStatement,
		[NSNumber numberWithInt:self.contactType], self.serverId,
		self.firstName, self.middleName, self.lastName, self.company, self.mobilePhone, self.pager,
		self.emailAddress1,self.emailAddress2, self.emailAddress3,
		self.companyStreet, self.companyCity, self.companyState, self.companyPostalCode, self.companyCountry,
		self.companyPhone1, self.companyPhone2, self.companyFax,
		self.homeStreet, self.homeCity, self.homeState, self.homePostalCode, self.homeCountry,
		self.homePhone1, self.homePhone2, self.homeFax,
		self.otherStreet, self.otherCity, self.otherState, self.otherPostalCode, self.otherCountry,
		self.webPage, self.notes, self.picture
	];

	if([inDB hadError]) {
        [self _reportError:inDB];
    }

	return true;
}

- (bool) updateIntoFMDatabase:(FMDatabase *)inDB table:(NSString *)inTableName
{
	if (inDB == nil) return false;
	if (inTableName.length == 0) return false;

	// use the contactID to determine whether an insertion or and update
	FXDebugLog(kFXLogContacts, @"Updating contactID(%d) table:(%@) serverID(%@)", self.contactID, inTableName, self.serverId);
	NSString *insertStatement = [NSString stringWithFormat:@"UPDATE %@ SET "\
		@"contactType = ?, "\
		@"firstName = ?, middleName = ?, lastName = ?, company = ?, mobilePhone = ?, pager = ?, "\
		@"emailAddress1 = ?, emailAddress2 = ?, emailAddress3 = ?, "\
		@"companyStreet = ?, companyCity = ?, companyState = ?, companyPostalCode = ?, companyCountry = ?, "\
		@"companyPhone1 = ?, companyPhone2 = ?, companyFax = ?, "\
		@"homeStreet = ?, homeCity = ?, homeState = ?, homePostalCode = ?, homeCountry = ?, "\
		@"homePhone1 = ?, homePhone2 = ?, homeFax = ?, "\
		@"otherStreet = ?, otherCity = ?, otherState = ?, otherPostalCode = ?, otherCountry = ?, "\
		@"webPage = ?, notes = ?, pictureData = ?"\
		@" WHERE serverID = \"%@\"", inTableName, self.serverId];

	[inDB executeUpdate:insertStatement,
		[NSNumber numberWithInt:self.contactType],
		self.firstName, self.middleName, self.lastName, self.company, self.mobilePhone, self.pager,
		self.emailAddress1,self.emailAddress2, self.emailAddress3,
		self.companyStreet, self.companyCity, self.companyState, self.companyPostalCode, self.companyCountry,
		self.companyPhone1, self.companyPhone2, self.companyFax,
		self.homeStreet, self.homeCity, self.homeState, self.homePostalCode, self.homeCountry,
		self.homePhone1, self.homePhone2, self.homeFax,
		self.otherStreet, self.otherCity, self.otherState, self.otherPostalCode, self.otherCountry,
		self.webPage, self.notes, self.picture
	];

	if([inDB hadError]) {
        [self _reportError:inDB];
    }
	return true;
}

- (void) addToContactsDB:(int)inContactType
{
	FMDatabaseQueue *theQueue = [FXDatabase defaultFMDatabaseQueue];
	NSString *theTable = [FXDatabase tableNameForContactType:inContactType];
	[theQueue inDatabase:^(FMDatabase *inDB) {
			[inDB open];
			[inDB beginTransaction];

			self.contactType = inContactType;
			[self insertIntoFMDatabase:inDB table:theTable];

			[inDB commit];
			[inDB close];
		}
	];

	return;
}

@end
