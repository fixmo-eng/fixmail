//
//  ASContact+FMDatabase.h
//  FXContactsApp
//
//  Created by Colin Biggin on 2012-11-27.
//  Copyright (c) 2012 Colin Biggin. All rights reserved.
//

#import "ASContact.h"

@class FMDatabase;
@class FMResultSet;

@interface ASContact (FXDatabase)

- (bool) insertIntoFMDatabase:(FMDatabase *)inDB table:(NSString *)inTableName;
- (bool) updateIntoFMDatabase:(FMDatabase *)inDB table:(NSString *)inTableName;
- (ASContact *) initWithFMResults:(FMResultSet *)inResults;
- (void) addToContactsDB:(int)inContactType;

@end
