// Obsolete since switch to ASItemOperations for body fetch
#if 0
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncObject.h"
#import "ASEmail.h"

@class ASFolder;
@protocol ASSyncMessageDelegate;

@interface ASSyncBody : ASSyncObject {
    ASFolder*                           folder;
    ASEmail*                           email;
    EBodyType                           bodyType;
    NSObject<ASSyncMessageDelegate>*    delegate;
}

@property (nonatomic,strong) ASFolder*  folder;
@property (nonatomic,strong) ASEmail*   email;
@property (nonatomic) EBodyType         bodyType;
@property (nonatomic,strong) NSObject<ASSyncMessageDelegate>*  delegate;


- (id)initWithFolder:(ASFolder*)aFolder email:(ASEmail*)anEmail bodyType:(EBodyType)aBodyType delegate:(NSObject<ASSyncMessageDelegate>*)aDelegate;

- (void)send;

@end
#endif