/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAutoTest.h"
#import "ASAccount.h"
#import "ASAutoTestFolder.h"
#import "ASAutoTestSearch.h"
#import "ASAutoTestSettings.h"
#import "ErrorAlert.h"


@implementation ASAutoTest

@synthesize account;
@synthesize autoTestFolder;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super init]) {
        self.account = anAccount;
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:@"Autotest" where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:@"AutoTest" message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Test Harness
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Test Harness

- (void)start
{
    if(!self.inProgress) {
        self.inProgress = YES;
        //self.autoTestFolder = [[ASAutoTestFolder alloc] initWithAutoTest:self];
        //[self.autoTestFolder start];
        
        self.autoTestSettings = [[ASAutoTestSettings alloc] initWithAutoTest:self];
        [self.autoTestSettings start];
        
        //self.autoTestSearch = [[ASAutoTestSearch alloc] initWithAutoTest:self];
        //[self.autoTestSearch start];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// ASAutoTestFolderDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASAutoTestFolderDelegate

- (void)autoTestSuccess:(ASAutoTestObject*)anAutoTestObject
{
    self.inProgress = NO;
}

- (void)autoTestFailed:(ASAutoTestObject*)anAutoTestObject;
{
    self.inProgress = NO;

}

@end
