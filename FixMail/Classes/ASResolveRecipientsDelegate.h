/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 *
 *  ASResolveRecipientsDelegate.h
 *
 *  Created by Colin Biggin on 2013-05-14.
 */

@class ASResolveRecipients;

@protocol ASResolveRecipientsDelegate <NSObject>

- (void)deliverCertificate:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp;
- (void)deliverMiniCertificate:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp;
- (void)deliverPicture:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp;
- (void)deliverFinished:(NSString*)inMessage resolveOp:(ASResolveRecipients*)inResolveOp;
- (void)deliverError:(NSString*)inMessage resolveOp:(ASResolveRecipients*)inResolveOp;

@end
