//
//  settingsSelectVC.m
//  FXContacts
//
//  Created by Colin Biggin on 2013-02-01.
//  Copyright (c) 2013 Colin Biggin. All rights reserved.
//
#import "settingsSelectVC.h"
#import "UIColor+FixmoExtensions.h"
#import "UIView-ViewFrameGeometry.h"
#import "EmailAddress.h"
#import "ErrorAlert.h"

@implementation settingsSelectVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"settingsSelectVC: viewDidLoad");
	[super viewDidLoad];

}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsSelectVC: viewWillAppear");
	[super viewWillAppear:inAnimated];
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsSelectVC: viewDidAppear");
	[super viewDidAppear:inAnimated];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsSelectVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"settingsSelectVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
	FXDebugLog(kFXLogVCDealloc, @"settingsSelectVC: dealloc");
}

#pragma mark UITableViewDataSource & UITableViewDelegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)inTableView
{
	return 1;
}

- (NSInteger) tableView:(UITableView *)inTableView numberOfRowsInSection:(NSInteger)inSection
{
	return self.rowLabels.count;
}

- (UITableViewCell *) tableView:(UITableView *)inTableView cellForRowAtIndexPath:(NSIndexPath *)inIndexPath
{	static NSString *reuseIdentifier = @"inputPlaceCellIdentifier";

	UITableViewCell *theCell = [inTableView dequeueReusableCellWithIdentifier:reuseIdentifier];
	if (theCell == nil) {
		theCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }

	theCell.textLabel.text = self.rowLabels[inIndexPath.row];
	theCell.textLabel.textColor = (self.rowSelected == inIndexPath.row) ? [UIColor settingsSelectedColor] : [UIColor blackColor];
	theCell.accessoryType = (self.rowSelected == inIndexPath.row) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
	theCell.selectionStyle = UITableViewCellSelectionStyleNone;

	return theCell;
}

- (UITableViewCellEditingStyle) tableView:(UITableView *)inTableView editingStyleForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	return UITableViewCellEditingStyleNone;
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[inTableView deselectRowAtIndexPath:inIndexPath animated:false];
	self.rowSelected = inIndexPath.row;
	[inTableView reloadData];

	[[NSNotificationCenter defaultCenter] postNotificationName:kSettingsSavedNotification object:[NSNumber numberWithInt:self.rowSelected]];
}

@end

#pragma mark -

@implementation emailEntrySettingsVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"emailEntrySettingsVC: viewDidLoad");
	[super viewDidLoad];

	self.emailField.delegate = self;
	[[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:self.emailField queue:NULL
				usingBlock:^(NSNotification *inNotification) {
					[self checkEmailAddress];
				}
	];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"emailEntrySettingsVC: viewWillAppear");
	[super viewWillAppear:inAnimated];

	if (self.helpText.length > 0) {
		self.helpTextLabel.text = self.helpText;
		[self.helpTextLabel sizeToFit];
		self.tableView.tableHeaderView.height = self.helpTextLabel.height + 20;
	} else {
		self.tableView.tableHeaderView = nil;
	}

	if (self.emailAddress.length > 0) {
		self.emailField.text = self.emailAddress;
		[self checkEmailAddress];
	}

	return;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"emailEntrySettingsVC: viewDidAppear");
	[super viewDidAppear:inAnimated];

	[self.emailField becomeFirstResponder];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"emailEntrySettingsVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];

	NSString *theEmailField = self.emailField.text;
	[[NSNotificationCenter defaultCenter] postNotificationName:kSettingsEmailSavedNotification object:theEmailField];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"emailEntrySettingsVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
	FXDebugLog(kFXLogVCDealloc, @"emailEntrySettingsVC: dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark UITextFieldDelegate methods

- (BOOL) textFieldShouldBeginEditing:(UITextField *)inTextField
{
	[self checkEmailAddress];
	return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)inTextField
{
	[self checkEmailAddress];
}

- (BOOL) textFieldShouldReturn:(UITextField *)inTextField
{
	[inTextField resignFirstResponder];
	return YES;
}

#pragma mark Private methods

- (void) checkEmailAddress
{
	NSString *theEmailField = self.emailField.text;
	if ([EmailAddress isProperRFCEmailAddress:theEmailField]) self.emailField.textColor = [UIColor blackColor];
	else self.emailField.textColor = [UIColor redColor];
}

@end

#pragma mark -

@implementation textEntrySettingsVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"textEntrySettingsVC: viewDidLoad");
	[super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"textEntrySettingsVC: viewWillAppear");
	[super viewWillAppear:inAnimated];

	if (self.helpText.length > 0) {
		self.helpTextLabel.text = self.helpText;
		[self.helpTextLabel sizeToFit];
		self.tableView.tableHeaderView.height = self.helpTextLabel.height + 20;
	} else {
		self.tableView.tableHeaderView = nil;
	}

	if (self.text.length > 0) {
		self.textView.text = self.text;
	}

	return;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"textEntrySettingsVC: viewDidAppear");
	[super viewDidAppear:inAnimated];

	[self.textView becomeFirstResponder];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"textEntrySettingsVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];

	NSString *theText = self.textView.text;
	[[NSNotificationCenter defaultCenter] postNotificationName:kSettingsTextSavedNotification object:theText];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"textEntrySettingsVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
	FXDebugLog(kFXLogVCDealloc, @"textEntrySettingsVC: dealloc");
}

@end