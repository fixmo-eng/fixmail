/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#define kCurrentAccountDeletedNotification		@"kCurrentAccountDeletedNotification"

@class ASAutoTest;

@interface settingsVC : UITableViewController
@end

@interface baseSettingsVC : UIViewController
@property (nonatomic, assign) int settingBeingEdited;
@end

@interface baseTableSettingsVC : UITableViewController
@property (nonatomic, assign) int settingBeingEdited;
@end

@interface accountSettingsVC : baseTableSettingsVC <UIActionSheetDelegate>//<UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordTitle;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UILabel *serverLabel;
@property (strong, nonatomic) IBOutlet UILabel *domainLabel;
@property (strong, nonatomic) IBOutlet UILabel *portLabel;
@property (strong, nonatomic) IBOutlet UIImageView *serviceImage;

@property (strong, nonatomic) IBOutlet UIButton *deleteAccountButton;

- (IBAction) toggleDeleteThisAccount:(id)inSender;

@end

@interface emailSettingsVC : baseTableSettingsVC

@property (strong, nonatomic) IBOutlet UILabel *emailRecentMessagesLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailPreviewLinesLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailReplyToLabel;
@property (strong, nonatomic) IBOutlet UISwitch *emailAskBeforeDeletingSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *emailSMIMESwitch;
@property (strong, nonatomic) IBOutlet UILabel *emailSignatureLabel;

- (IBAction) toggleSMIMESwitch:(id)inSender;
- (IBAction) toggleAskBeforeDeletingSwitch:(id)inSender;

@end

@interface calendarSettingsVC : baseTableSettingsVC

@property (strong, nonatomic) IBOutlet UISwitch *calendarReminders;
@property (strong, nonatomic) IBOutlet UILabel *calendarEventsLabel;

- (IBAction) toggleCalendarReminders:(id)inSender;

@end

@interface contactsSettingsVC : baseTableSettingsVC

@property (strong, nonatomic) IBOutlet UILabel *contactsSortOrderLabel;
@property (strong, nonatomic) IBOutlet UILabel *contactsDisplayOrderLabel;

@end

@interface developerSettingsVC : baseTableSettingsVC

@property (strong, nonatomic) IBOutlet UILabel *calendarStartsOnLabel;
@property (strong, nonatomic) IBOutlet UISegmentedControl *emailBodyType;

@property (strong, nonatomic) ASAutoTest* autoTest;

- (IBAction) toggleDumpSettings:(id)inSender;
- (IBAction) toggleDeleteAllAttachments:(id)inSender;
- (IBAction) toggleTraceActiveSyncStart:(id)inSender;
- (IBAction) toggleTraceActiveSyncInitialSync:(id)inSender;
- (IBAction) toggleTraceActiveSyncStop:(id)inSender;
- (IBAction) toggleAutoTest:(id)inSender;
- (IBAction) toggleEmailBodyType:(id)inSender;

@property (strong, nonatomic) IBOutlet UIButton *traceSyncButton;

@end

@interface developerSystemVC : baseSettingsVC
@property (weak, nonatomic) IBOutlet UITextView *textView;
@end

@interface developerFXDebugLogVC : baseTableSettingsVC
@property (strong, nonatomic) IBOutlet UISwitch *traceSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *fixmeSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *pkMIMESwitch;
@property (strong, nonatomic) IBOutlet UISwitch *szlSecuritySwitch;

@property (strong, nonatomic) IBOutlet UISwitch *activeSyncSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *wbxmlSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *asEmailSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *asFolderSwitch;

@property (strong, nonatomic) IBOutlet UISwitch *viewDidLoadUnloadSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *viewDidAppearDisappearSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *deallocSwitch;

@property (strong, nonatomic) IBOutlet UISwitch *settingsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *contactsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *calendarSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *notesSwitch;

@property (strong, nonatomic) IBOutlet UISwitch *composeMailSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *mailViewerSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *addressViewSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *textEntryViewSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *attachmentViewSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *attachmentManagerSwitch;

- (IBAction) toggleTurnOnAllFlags:(id)inSender;
- (IBAction) toggleTurnOffAllFlags:(id)inSender;
- (IBAction) toggleSwitch:(id)inSender;
@end

@interface developerCertificateVC : baseSettingsVC

@property (strong, nonatomic) IBOutlet UITextView *attributesView;

- (IBAction) toggleDeleteContainerCertificate:(id)inSender;

@end
