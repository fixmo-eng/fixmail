//
//  FXDatePickerVC.m
//  FixMail
//
//  Created by dexter.kim on 2013-07-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXDatePickerVC.h"

@implementation FXDatePickerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [_datepickerToolbar setBarStyle:UIBarStyleBlackTranslucent];
    [_segmntChangeDateMode setSelectedSegmentIndex:(NSInteger)_kDateMode];
    
    _datepicker.timeZone = [NSTimeZone localTimeZone];
    if(_strDates) {
        [_datepicker setMinimumDate:_kDateMode==kDatePickerVCStartDate ? [NSDate date] : [self dateFromString:_strDates[kDatePickerVCStartDate] withDateFormat:_dateFormat] ];
        [_datepicker setDate:[self dateFromString:_strDates[_kDateMode] withDateFormat:_dateFormat] animated:YES];
    } else {
        [_datepicker setMinimumDate:[NSDate date]];
        [_datepicker setDate:[NSDate date] animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSDate*)dateFromString:(NSString*)date withDateFormat:(NSString*)dateFormat
{
    if(!date) {
        return nil;
    }
    
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter dateFromString:date];
}

- (NSString*)stringFromDate:(NSDate*)date withDateFormat:(NSString*)dateFormat
{
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter stringFromDate:date];
}

- (IBAction)dateModeChange:(id)sender {
    _kDateMode = _segmntChangeDateMode.selectedSegmentIndex;
    NSDate *sdate = [self dateFromString:_strDates[kDatePickerVCStartDate] withDateFormat:_dateFormat];
    NSDate *edate = [self dateFromString:_strDates[kDatePickerVCEndDate] withDateFormat:_dateFormat];
    
    if([sdate compare:edate] == NSOrderedDescending) {
        _strDates[_kDateMode==kDatePickerVCStartDate] = [self stringFromDate:_kDateMode==kDatePickerVCStartDate?sdate:edate withDateFormat:_dateFormat];
    }
    
    [_datepicker setMinimumDate:(_kDateMode==kDatePickerVCStartDate ? [NSDate date] : sdate)];
    [_datepicker setDate:(_kDateMode==kDatePickerVCStartDate?sdate:edate) animated:YES];
}

- (IBAction)today:(id)sender {
    _kDateMode = _segmntChangeDateMode.selectedSegmentIndex = kDatePickerVCStartDate;
    [_datepicker setMinimumDate:[NSDate date]];
    [_datepicker setDate:[NSDate date] animated:YES];
    _strDates[kDatePickerVCEndDate] = _strDates[kDatePickerVCStartDate] = [self stringFromDate:_datepicker.date withDateFormat:_dateFormat];
    [self.delegate datePickerVC:self didChangedDates:_strDates withMode:_kDateMode];
}

- (IBAction)dateChanged:(id)sender {
    NSDate *changedDate = _datepicker.date;
    NSDate *today = [NSDate date];
    NSInteger interval = [[[NSCalendar currentCalendar] components: NSDayCalendarUnit
                                                          fromDate: today
                                                            toDate: changedDate
                                                           options: 0] day];
    
    NSLog(@"%@ - %@, %@", _kDateMode==kDatePickerVCStartDate ? @"kStartDate" : @"kEndDate", changedDate, _datepicker.minimumDate);
    _strDates[_kDateMode] = [self stringFromDate:changedDate withDateFormat:_dateFormat];
    NSDate *another_date = [self dateFromString:_strDates[_kDateMode==kDatePickerVCStartDate] withDateFormat:_dateFormat]; // xor
    if(interval>=0) {
        switch (_kDateMode) {
            case kDatePickerVCStartDate: {
                NSComparisonResult result = [_datepicker.minimumDate compare:changedDate];
                if(result != NSOrderedAscending) {
                    [_datepicker setDate:_datepicker.minimumDate animated:YES];
                } else {
                    result = [changedDate compare:another_date];
                    if(result != NSOrderedAscending) {
                        _strDates[kDatePickerVCEndDate] = _strDates[kDatePickerVCStartDate];
                        [_datepicker setDate:changedDate animated:YES];
                    }
                }
            } break;
                
            case kDatePickerVCEndDate: {
                NSComparisonResult result = [another_date compare:changedDate];
                if(result != NSOrderedAscending) {
                    if(result == NSOrderedSame) {
                        NSDate *thedaybefore = [changedDate dateByAddingTimeInterval:60*60*24*(-1)];
                        [_datepicker setDate:thedaybefore animated:YES];
                        [_datepicker setDate:_datepicker.minimumDate animated:YES];
                    } else {
                        [_datepicker setDate:changedDate animated:YES];
                    }
                }
            } break;
                
            default:
                break;
        }
        [self.delegate datePickerVC:self didChangedDates:_strDates withMode:_kDateMode];
    } else if(interval<0) {
        [_datepicker setMinimumDate:_kDateMode==kDatePickerVCStartDate ? today : another_date];
        [_datepicker setDate:[self dateFromString:_strDates[_kDateMode] withDateFormat:_dateFormat] animated:YES];
        NSString *errMsg = [NSString stringWithFormat:@"%@ Date is invalid.", _kDateMode==kDatePickerVCStartDate ? @"Start" : @"End"];
        [[[UIAlertView alloc] initWithTitle:@"Invalid Date"
                                    message:errMsg
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }

}
@end
