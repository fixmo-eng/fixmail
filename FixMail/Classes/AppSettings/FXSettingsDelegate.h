//
//  FXSettingsDelegate.h
//  FixMail
//
//  Created by Colin Biggin on 2013-01-25.
//
//

#import "SZLApplicationContainer.h"

@interface FXSettingsDelegate : NSObject <SZLApplicationContainerDelegate, SZLConfigurationDelegate>

@property (nonatomic, assign) id <SZLApplicationContainer> container;
@property (nonatomic, assign) id <SZLConfigurationDelegate> configurationDelegate;

-(BOOL)isConfigured;
-(BOOL)requiresImmediateConfiguration;
-(void)presentConfigurationController;

@end
