/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "settingsVC.h"
#import "AppSettings.h"
#import "ASAccount.h"
#import "ASAutoTest.h"
#import "ASConfigViewController.h"
#import "chooseAccountVC.h"
#import "settingsSelectVC.h"
#import "SZLConcreteApplicationContainer.h"
#import "UIViewController+MGSplitViewController.h"
#import "UIColor+FixmoExtensions.h"
#import "UIStoryboard+FixmoExtensions.h"
#import "MGSplitViewController.h"
#import "FXMEmailSecurity.h"
#import "AttachmentManager.h"
#import "TraceEngine.h"
#include <objc/runtime.h>
#import "FXSettingsDelegate.h"
#import "FXSOutOfOfficeVC.h"
#import "FXCNotificationsManager.h"
#import "SZLCAC.h"

#define kSectionAbout					0
#define kSectionApplications			1
#define kSectionDeveloper				2

#define kApplicationAbout				0
#define kApplicationLicensing			1

#define kApplicationAccount				0
#define kApplicationEmail				1
#define kApplicationCalendar			2
#define kApplicationContacts			3

#define kApplicationDeveloper			0

#define kEmailSettingShowMessages		10
#define kEmailSettingPreviewLines		11
#define kEmailSettingReplyto			12
#define kEmailSettingSignature			13
#define kEmailSettingOutofOffice        14

#define kCalendarEvents					13
#define kCalendarStartsOn				14
#define kContactsSortOrder				15
#define kContactsDisplayOrder			16

#define kAccountDeleteAccountAlertViewTag 100

#pragma mark - Settings VC


@implementation settingsVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"settingsVC: viewDidLoad");
	[super viewDidLoad];

	[self setupNotifications];
	[self restoreSettings];

	return;
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsVC: viewWillAppear");
	[super viewWillAppear:inAnimated];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
	if (IS_IPAD()) {
		NSIndexPath *savedIndexPath = [NSIndexPath indexPathForRow:[AppSettings settingsRow] inSection:[AppSettings settingsSection]];
		[self.tableView selectRowAtIndexPath:savedIndexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
	}

	return;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsVC: viewDidAppear");
	[super viewDidAppear:inAnimated];

	
	
	if (IS_IPAD()) {
		UIBarButtonItem *exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
		self.navigationItem.leftBarButtonItem = exitButton;
	}

	return;
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"settingsVC: viewWillDisappear");
	[super viewWillDisappear:inAnimated];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"settingsVC: viewDidDisappear");
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
	FXDebugLog(kFXLogSettings | kFXLogVCDealloc, @"settingsVC: dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	if (!IS_IPAD()) {
		[inTableView deselectRowAtIndexPath:inIndexPath animated:YES];
	}

	[AppSettings setSettingsSection:inIndexPath.section];
	[AppSettings setSettingsRow:inIndexPath.row];
	[self displayViewControllerWithIndexPath:inIndexPath animated:YES];
}

#pragma mark - Private methods

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kCurrentAccountDeletedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			[self showAccountChooser:YES];
		}
	];

	return;
}

- (void) showAccountChooser:(BOOL)inAnimated
{
	FXSettingsDelegate *settingsDelegate = (FXSettingsDelegate *)[SZLConcreteApplicationContainer sharedApplicationContainer].delegate;
	[settingsDelegate presentConfigurationController];
}

- (void) restoreSettings
{
	// on iPhone, this doesn't really make sense... feels weird
	if (!IS_IPAD()) return;

	NSIndexPath *savedIndexPath = [NSIndexPath indexPathForRow:[AppSettings settingsRow] inSection:[AppSettings settingsSection]];
	[self displayViewControllerWithIndexPath:savedIndexPath animated:NO];

	return;
}

- (void) displayViewControllerWithIndexPath:(NSIndexPath *)inIndexPath animated:(BOOL)inAnimated
{
	UIViewController *theController = nil;
	NSString *storyboardName;
	if (IS_IPAD()) {
		storyboardName = kStoryboard_Settings_iPad;
	} else {
		storyboardName = kStoryboard_Settings_iPhone;
	}
	
	if (inIndexPath.section == kSectionAbout) {
		switch (inIndexPath.row) {
			case kApplicationAbout:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"aboutSettingsVC"];
				break;
			case kApplicationLicensing:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"licensingSettingsVC"];
				break;
		}
	} else if (inIndexPath.section == kSectionApplications) {
		switch (inIndexPath.row) {
			case kApplicationAccount:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"accountSettingsVC"];
				break;
			case kApplicationEmail:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"emailSettingsVC"];
				break;
			case kApplicationCalendar:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"calendarSettingsVC"];
				break;
			case kApplicationContacts:
				theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"contactsSettingsVC"];
				break;
			default:
				break;
		}
	} else if (inIndexPath.section == kSectionDeveloper) {
		theController = [UIStoryboard storyboardWithName:storyboardName identifier:@"developerSettingsVC"];
	}

	if (theController != nil) {
		if (IS_IPAD()) {
			MGSplitViewController *mgController = self.mgSplitViewController;
			UINavigationController *navController = (UINavigationController *)mgController.detailViewController;
			navController.viewControllers = @[theController];
            navController.navigationBar.barStyle = UIBarStyleBlack;
		} else {
			[self.navigationController pushViewController:theController animated:inAnimated];
		}
	}

	return;
}

@end

#pragma mark - Base Settings VC

@implementation baseSettingsVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"%s: viewDidLoad", class_getName([self class]));
	[super viewDidLoad];

	[self setupNotifications];
	[self setupControls];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewWillAppear", class_getName([self class]));
	[super viewWillAppear:inAnimated];

	[self redoControls];

	return;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewDidAppear", class_getName([self class]));
	[super viewDidAppear:inAnimated];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewWillDisappear", class_getName([self class]));
	[super viewWillDisappear:inAnimated];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"%s: viewDidDisappear", class_getName([self class]));
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) dealloc
{
	FXDebugLog(kFXLogSettings | kFXLogVCDealloc, @"%s: dealloc", class_getName([self class]));
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Private methods

- (void) setupNotifications
{
}

- (void) setupControls
{
}

- (void) redoControls
{
}

@end

#pragma mark - Base Table Settings VC

@implementation baseTableSettingsVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"%s: viewDidLoad", class_getName([self class]));
	[super viewDidLoad];

	[self setupNotifications];
	[self setupControls];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewWillAppear", class_getName([self class]));
	[super viewWillAppear:inAnimated];

	[self redoControls];

	return;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewDidAppear", class_getName([self class]));
	[super viewDidAppear:inAnimated];
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"%s: viewWillDisappear", class_getName([self class]));
	[super viewWillDisappear:inAnimated];
}

- (void) viewDidDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogVCAppearDisappear, @"%s: viewDidDisappear", class_getName([self class]));
	[super viewDidDisappear:inAnimated];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) dealloc
{
	FXDebugLog(kFXLogSettings | kFXLogVCDealloc, @"%s: dealloc", class_getName([self class]));
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Private methods

- (void) setupNotifications
{
}

- (void) setupControls
{
}

- (void) redoControls
{
}

@end

#pragma mark - Account Settings VC

@implementation accountSettingsVC

- (void) setupControls
{
	[self.deleteAccountButton setBackgroundImage:[[UIImage imageNamed:@"FixMailRsrc.bundle/redButton.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:0] forState:UIControlStateNormal];
}

- (void) redoControls
{
	BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
	if (currentAccount == nil) {
		self.emailLabel.textColor = [UIColor redColor];
		self.emailLabel.text = FXLLocalizedStringFromTable(@"<NO_CURRENT_ACCOUNT>", @"settingsVC", @"Name displayed in account view when no account configured");
		self.usernameLabel.text = @"";
		self.passwordLabel.text = @"";
		self.serverLabel.text = @"";
		self.domainLabel.text = @"";
		self.portLabel.text = @"";
		self.serviceImage.image = nil;
	} else {
		self.emailLabel.textColor = [UIColor settingsSelectedColor];
		self.emailLabel.text = currentAccount.name;
        self.usernameLabel.text = currentAccount.userName;
        if ([SZLCAC isContainerCACEnabled]) {
            self.passwordTitle.text = @"PCC";
            self.passwordLabel.text = currentAccount.pcc;
        } else {
            self.passwordTitle.text = @"Password";
            self.passwordLabel.text = [@"•" stringByPaddingToLength:currentAccount.password.length withString:@"•" startingAtIndex:0];
        }
		self.serverLabel.text = currentAccount.hostName;
		self.domainLabel.text = currentAccount.domain;
		self.portLabel.text = [NSString stringWithFormat:@"%d", currentAccount.port];
 
		UIImage *theImage = nil;
		switch (currentAccount.accountSubType) {
			case AccountSubTypeExchange:
            case AccountSubTypeDomino:
				theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconExchange.png"];
				break;
			case AccountSubTypeGmail:
				theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/accountTypeIconGmail.png"];
				break;
			default:
				break;
		}
		self.serviceImage.image = theImage;
	}

	return;
}

- (IBAction) toggleDeleteThisAccount:(id)inSender
{	
	UIActionSheet *deleteSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"ARE_YOU_SURE_YOU_WANT_TO_DELETE_THIS_ACCOUNT", @"settingsVC", @"Message when Delete Account button is pressed")
															 delegate:self
													cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_DELETE_ACCOUNT", @"settingsVC", @"Cancel button title for alert view when Delete Account button is pressed")
											   destructiveButtonTitle:FXLLocalizedStringFromTable(@"DELETE_ACCOUNT", @"settingsVC", @"Delete Account (OK) button title for alert view when Delete Account button is pressed")
													otherButtonTitles:nil];
	[deleteSheet showInView:self.view];
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView
{
#if showServerFields
	return 3;
#else
	return 2;
#endif
}

#pragma mark UIActionSheetDelegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == actionSheet.destructiveButtonIndex) {
		self.emailLabel.textColor = [UIColor redColor];
		self.emailLabel.text = FXLLocalizedStringFromTable(@"<NO_CURRENT_ACCOUNT>", @"settingsVC", @"Name displayed in account view when no account configured");
		self.usernameLabel.text = @"";
		self.passwordLabel.text = @"";
		self.serverLabel.text = @"";
		self.domainLabel.text = @"";
		self.portLabel.text = @"";
		self.serviceImage.image = nil;
		[BaseAccount deleteAllAccounts];
		[[NSNotificationCenter defaultCenter] postNotificationName:kCurrentAccountDeletedNotification object:self];
	}
}


@end

#pragma mark - Email Settings VC

@implementation emailSettingsVC

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			int theRow = [inNotification.object integerValue];
			if (self.settingBeingEdited == kEmailSettingShowMessages) {
				switch (theRow) {
					default:
					case 0: [AppSettings setEmailRecentMessages:50]; break;
					case 1: [AppSettings setEmailRecentMessages:100]; break;
					case 2: [AppSettings setEmailRecentMessages:200]; break;
					case 3: [AppSettings setEmailRecentMessages:500]; break;
					case 4: [AppSettings setEmailRecentMessages:1000]; break;
				}
			} else if (self.settingBeingEdited == kEmailSettingPreviewLines) {
				switch (theRow) {
					default:
					case 0: [AppSettings setEmailPreviewLines:0]; break;
					case 1: [AppSettings setEmailPreviewLines:1]; break;
					case 2: [AppSettings setEmailPreviewLines:2]; break;
					case 3: [AppSettings setEmailPreviewLines:3]; break;
					case 4: [AppSettings setEmailPreviewLines:4]; break;
					case 5: [AppSettings setEmailPreviewLines:5]; break;
				}
			}
		}
	];

	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsEmailSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			NSString *theEmailAddress = inNotification.object;
			[AppSettings setEmailReplyTo:theEmailAddress];
		}
	];

	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsTextSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			NSString *theSignature = inNotification.object;
			[AppSettings setEmailSignature:theSignature];
		}
	];

	return;
}

- (void) redoControls
{
	switch ([AppSettings emailRecentMessages]) {
		default:
		case 50: self.emailRecentMessagesLabel.text = FXLLocalizedStringFromTable(@"50_MESSAGES", @"settingsVC", nil); break;
		case 100: self.emailRecentMessagesLabel.text = FXLLocalizedStringFromTable(@"100_MESSAGES", @"settingsVC", nil); break;
		case 200: self.emailRecentMessagesLabel.text = FXLLocalizedStringFromTable(@"200_MESSAGES", @"settingsVC", nil); break;
		case 500: self.emailRecentMessagesLabel.text = FXLLocalizedStringFromTable(@"500_MESSAGES", @"settingsVC", nil); break;
		case 1000: self.emailRecentMessagesLabel.text = FXLLocalizedStringFromTable(@"1000_MESSAGES", @"settingsVC", nil); break;
	}

	switch ([AppSettings emailPreviewLines]) {
		default:
		case 0: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"0_LINES", @"settingsVC", nil); break;
		case 1: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"1_LINE", @"settingsVC", nil); break;
		case 2: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"2_LINES", @"settingsVC", nil); break;
		case 3: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"3_LINES", @"settingsVC", nil); break;
		case 4: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"4_LINES", @"settingsVC", nil); break;
		case 5: self.emailPreviewLinesLabel.text = FXLLocalizedStringFromTable(@"5_LINES", @"settingsVC", nil); break;
	}

	self.emailReplyToLabel.text = [AppSettings emailReplyTo];
	self.emailAskBeforeDeletingSwitch.on = [AppSettings emailAskBeforeDeleting];
	self.emailSignatureLabel.text = [AppSettings emailSignature];

	if ([FXMEmailSecurity hasUserCertificate]) {
		self.emailSMIMESwitch.enabled = YES;
		self.emailSMIMESwitch.on = [AppSettings emailSMIME];
	} else {
		self.emailSMIMESwitch.enabled = NO;
		self.emailSMIMESwitch.on = NO;
	}

	return;
}

- (IBAction) toggleSMIMESwitch:(id)inSender;
{
	[AppSettings setEmailSMIME:self.emailSMIMESwitch.isOn];
}

- (IBAction) toggleAskBeforeDeletingSwitch:(id)inSender
{
	[AppSettings setEmailAskBeforeDeleting:self.emailAskBeforeDeletingSwitch.isOn];
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[inTableView deselectRowAtIndexPath:inIndexPath animated:NO];

	if (inIndexPath.row == 0) {
		settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
		theController.rowLabels = @[ FXLLocalizedStringFromTable(@"50_MESSAGES", @"settingsVC", nil),
                                        FXLLocalizedStringFromTable(@"100_MESSAGES", @"settingsVC", nil),
                                        FXLLocalizedStringFromTable(@"200_MESSAGES", @"settingsVC", nil),
                                        FXLLocalizedStringFromTable(@"500_MESSAGES", @"settingsVC", nil),
                                        FXLLocalizedStringFromTable(@"1000_MESSAGES", @"settingsVC", nil) ];
		switch ([AppSettings emailRecentMessages]) {
			default:
			case 50: theController.rowSelected = 0; break;
			case 100: theController.rowSelected = 1; break;
			case 200: theController.rowSelected = 2; break;
			case 500: theController.rowSelected = 3; break;
			case 1000: theController.rowSelected = 4; break;
		}
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kEmailSettingShowMessages;
	} else if (inIndexPath.row == 1) {
		settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
		theController.rowLabels = @[ FXLLocalizedStringFromTable(@"0_LINES", @"settingsVC", nil),
                                    FXLLocalizedStringFromTable(@"1_LINE", @"settingsVC", nil),
                                    FXLLocalizedStringFromTable(@"2_LINES", @"settingsVC", nil),
                                    FXLLocalizedStringFromTable(@"3_LINES", @"settingsVC", nil),
                                    FXLLocalizedStringFromTable(@"4_LINES", @"settingsVC", nil),
                                    FXLLocalizedStringFromTable(@"5_LINES", @"settingsVC", nil) ];
		switch ([AppSettings emailPreviewLines]) {
			default:
			case 0: theController.rowSelected = 0; break;
			case 1: theController.rowSelected = 1; break;
			case 2: theController.rowSelected = 2; break;
			case 3: theController.rowSelected = 3; break;
			case 4: theController.rowSelected = 4; break;
			case 5: theController.rowSelected = 5; break;
		}
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kEmailSettingPreviewLines;
	} else if (inIndexPath.row == 4) {
        FXSOutOfOfficeVC *theController = nil;
		if (IS_IPAD()) theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPad identifier:@"FXSOutOfOfficeVC"];
		else theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPhone identifier:@"FXSOutOfOfficeVC"];
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kEmailSettingOutofOffice;
	} else if (inIndexPath.row == 5) {
		textEntrySettingsVC *theController = nil;
		if (IS_IPAD()) theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPad identifier:@"textEntrySettingsVC"];
		else theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPhone identifier:@"textEntrySettingsVC"];
		theController.text = [AppSettings emailSignature];
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kEmailSettingSignature;
	}

	return;
}
@end

#pragma mark - Calendar Settings VC

@interface calendarSettingsVC ()
@property NSTimer* calendarNotificationsToggleTimer;
@end


@implementation calendarSettingsVC

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			int theRow = [inNotification.object integerValue];
			if (self.settingBeingEdited == kCalendarEvents) {
				switch (theRow) {
					default:
					case 0: [AppSettings setCalendarEvents:calendarEventsForever]; break;
					case 1: [AppSettings setCalendarEvents:calendarEvents1Day]; break;
					case 2: [AppSettings setCalendarEvents:calendarEvents3Days]; break;
					case 3: [AppSettings setCalendarEvents:calendarEvents1Week]; break;
					case 4: [AppSettings setCalendarEvents:calendarEvents2Weeks]; break;
					case 5: [AppSettings setCalendarEvents:calendarEvents1Month]; break;
					case 6: [AppSettings setCalendarEvents:calendarEvents3Months]; break;
					case 7: [AppSettings setCalendarEvents:calendarEvents6Months]; break;
				}
			}
		}
	];

	return;
}

- (void) redoControls
{
	switch ([AppSettings calendarEvents]) {
		default:
		case calendarEventsForever: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"FOREVER", @"settingsVC", nil); break;
		case calendarEvents1Day: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"1_DAY", @"settingsVC", nil); break;
		case calendarEvents3Days: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"3_DAYS", @"settingsVC", nil); break;
		case calendarEvents1Week: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"1_WEEK", @"settingsVC", nil); break;
		case calendarEvents2Weeks: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"2_WEEKS", @"settingsVC", nil); break;
		case calendarEvents1Month: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"1_MONTH", @"settingsVC", nil); break;
		case calendarEvents3Months: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"3_MONTHS", @"settingsVC", nil); break;
		case calendarEvents6Months: self.calendarEventsLabel.text = FXLLocalizedStringFromTable(@"6_MONTHS", @"settingsVC", nil); break;
	}

	return;
}

- (IBAction) toggleCalendarReminders:(id)inSender
{
    [AppSettings setCalendarReminders:self.calendarReminders.isOn];
    
    if (self.calendarNotificationsToggleTimer)
    {
        [self.calendarNotificationsToggleTimer invalidate];
        self.calendarNotificationsToggleTimer = nil;
    }
    
    self.calendarNotificationsToggleTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCalendarNotifications:) userInfo:nil repeats:NO];
}

-(void) updateCalendarNotifications:(NSTimer*)timer
{
    [FXCNotificationsManager.instance removeLocalNotifications];
    [FXCNotificationsManager.instance populateLocalNotifications];
    [timer invalidate];
    self.calendarNotificationsToggleTimer = nil;
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[inTableView deselectRowAtIndexPath:inIndexPath animated:NO];

	if (inIndexPath.row == 1) {
		settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
		theController.rowLabels = @[ FXLLocalizedStringFromTable(@"FOREVER", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"1_DAY", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"3_DAYS", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"1_WEEK", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"2_WEEKS", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"1_MONTH", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"3_MONTHS", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"6_MONTHS", @"settingsVC", nil) ];
		switch ([AppSettings calendarEvents]) {
			default:
			case calendarEventsForever: theController.rowSelected = 0; break;
			case calendarEvents1Day: theController.rowSelected = 1; break;
			case calendarEvents3Days: theController.rowSelected = 2; break;
			case calendarEvents1Week: theController.rowSelected = 3; break;
			case calendarEvents2Weeks: theController.rowSelected = 4; break;
			case calendarEvents1Month: theController.rowSelected = 5; break;
			case calendarEvents3Months: theController.rowSelected = 6; break;
			case calendarEvents6Months: theController.rowSelected = 7; break;
		}
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kCalendarEvents;
	}

	return;
}

-(void)dealloc
{
    if (self.calendarNotificationsToggleTimer)
    {
        [self.calendarNotificationsToggleTimer invalidate];
        self.calendarNotificationsToggleTimer = nil;
    }
}
@end

#pragma mark - Contacts Settings VC

@implementation contactsSettingsVC

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			int theRow = [inNotification.object integerValue];
			if (self.settingBeingEdited == kContactsSortOrder) {
				switch (theRow) {
					default:
					case 0: [AppSettings setContactsSortOrder:sortOrderFirstLast]; break;
					case 1: [AppSettings setContactsSortOrder:sortOrderLastFirst]; break;
				}
			} else if (self.settingBeingEdited == kContactsDisplayOrder) {
				switch (theRow) {
					default:
					case 0: [AppSettings setContactsDisplayOrder:sortOrderFirstLast]; break;
					case 1: [AppSettings setContactsDisplayOrder:sortOrderLastFirst]; break;
				}
			}
		}
	];

	return;
}

- (void) redoControls
{
	switch ([AppSettings contactsSortOrder]) {
		case sortOrderFirstLast: self.contactsSortOrderLabel.text = FXLLocalizedStringFromTable(@"FIRST_NAME", @"settingsVC", nil); break;
		case sortOrderLastFirst: self.contactsSortOrderLabel.text = FXLLocalizedStringFromTable(@"LAST_NAME", @"settingsVC", nil); break;
	}
	switch ([AppSettings contactsDisplayOrder]) {
		case sortOrderFirstLast: self.contactsDisplayOrderLabel.text = FXLLocalizedStringFromTable(@"FIRST_NAME", @"settingsVC", nil); break;
		case sortOrderLastFirst: self.contactsDisplayOrderLabel.text = FXLLocalizedStringFromTable(@"LAST_NAME", @"settingsVC", nil); break;
	}

	return;
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[inTableView deselectRowAtIndexPath:inIndexPath animated:NO];

	if (inIndexPath.row == 0) {
		settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
		theController.rowLabels = @[ FXLLocalizedStringFromTable(@"FIRST_LAST", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"LAST_FIRST", @"settingsVC", nil) ];
		switch ([AppSettings contactsSortOrder]) {
			default:
			case sortOrderFirstLast: theController.rowSelected = 0; break;
			case sortOrderLastFirst: theController.rowSelected = 1; break;
		}
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kContactsSortOrder;
	} else if (inIndexPath.row == 1) {
		settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
		theController.rowLabels = @[ FXLLocalizedStringFromTable(@"FIRST_LAST", @"settingsVC", nil),
									FXLLocalizedStringFromTable(@"LAST_FIRST", @"settingsVC", nil) ];
		switch ([AppSettings contactsDisplayOrder]) {
			default:
			case sortOrderFirstLast: theController.rowSelected = 0; break;
			case sortOrderLastFirst: theController.rowSelected = 1; break;
		}
		[self.navigationController pushViewController:theController animated:YES];
		self.settingBeingEdited = kContactsDisplayOrder;
	}

	return;
}

@end

#pragma mark - Developer Settings VC

@implementation developerSettingsVC

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSettingsSavedNotification object:NULL queue:NULL
		usingBlock:^(NSNotification *inNotification) {
			int theRow = [inNotification.object integerValue];
			if (self.settingBeingEdited == kCalendarStartsOn) {
				switch (theRow) {
					default:
					case 0: [AppSettings setCalendarStartDayOn:calendarStartOnSunday]; break;
					case 1: [AppSettings setCalendarStartDayOn:calendarStartOnMonday]; break;
					case 2: [AppSettings setCalendarStartDayOn:calendarStartOnTuesday]; break;
					case 3: [AppSettings setCalendarStartDayOn:calendarStartOnWednesday]; break;
					case 4: [AppSettings setCalendarStartDayOn:calendarStartOnThursday]; break;
					case 5: [AppSettings setCalendarStartDayOn:calendarStartOnFriday]; break;
					case 6: [AppSettings setCalendarStartDayOn:calendarStartOnSaturday]; break;
				}
			}
		}
	];

	return;
}

- (void) redoControls
{
	switch ([AppSettings calendarStartDayOn]) {
		default:
		case calendarStartOnSunday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"SUNDAY", @"settingsVC", nil); break;
		case calendarStartOnMonday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"MONDAY", @"settingsVC", nil); break;
		case calendarStartOnTuesday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"TUESDAY", @"settingsVC", nil); break;
		case calendarStartOnWednesday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"WEDNESDAY", @"settingsVC", nil); break;
		case calendarStartOnThursday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"THURSDAY", @"settingsVC", nil); break;
		case calendarStartOnFriday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"FRIDAY", @"settingsVC", nil); break;
		case calendarStartOnSaturday: self.calendarStartsOnLabel.text = FXLLocalizedStringFromTable(@"SATURDAY", @"settingsVC", nil); break;
	}

	switch ([AppSettings emailBodyType]) {
		case emailBodyText: self.emailBodyType.selectedSegmentIndex = 0; break;
		case emailBodyHTML: self.emailBodyType.selectedSegmentIndex = 1; break;
		case emailBodyTextHTML: self.emailBodyType.selectedSegmentIndex = 2; break;
		default:
			break;
	}
	return;
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	[inTableView deselectRowAtIndexPath:inIndexPath animated:NO];

	if (inIndexPath.section == 0) {
		NSString *theStoryboardFile = @"";
		if (IS_IPAD()) theStoryboardFile = kStoryboard_Settings_iPad;
		else theStoryboardFile = kStoryboard_Settings_iPhone;
		UIViewController *theController = nil;
		switch (inIndexPath.row) {
			case 0: theController = [UIStoryboard storyboardWithName:theStoryboardFile identifier:@"developerSystemVC"]; break;
			case 1: theController = [UIStoryboard storyboardWithName:theStoryboardFile identifier:@"developerFXDebugLogVC"]; break;
			case 2: theController = [UIStoryboard storyboardWithName:theStoryboardFile identifier:@"developerCertificateVC"]; break;
		}
		if (theController != nil) {
			[self.navigationController pushViewController:theController animated:YES];
		}
	} else if (inIndexPath.section == 1) {
		if (inIndexPath.row == 0) {
			settingsSelectVC *theController = [[settingsSelectVC alloc] initWithStyle:UITableViewStyleGrouped];
			theController.rowLabels = @[ FXLLocalizedStringFromTable(@"SUNDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"MONDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"TUESDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"WEDNESDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"THURSDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"FRIDAY", @"settingsVC", nil),
										FXLLocalizedStringFromTable(@"SATURDAY", @"settingsVC", nil) ];
			switch ([AppSettings calendarStartDayOn]) {
				default:
				case calendarStartOnSunday: theController.rowSelected = 0; break;
				case calendarStartOnMonday: theController.rowSelected = 1; break;
				case calendarStartOnTuesday: theController.rowSelected = 2; break;
				case calendarStartOnWednesday: theController.rowSelected = 3; break;
				case calendarStartOnThursday: theController.rowSelected = 4; break;
				case calendarStartOnFriday: theController.rowSelected = 5; break;
				case calendarStartOnSaturday: theController.rowSelected = 6; break;
			}
			[self.navigationController pushViewController:theController animated:YES];
			self.settingBeingEdited = kCalendarStartsOn;
		}
	}

	return;
}

- (IBAction) toggleDumpSettings:(id)inSender
{
	[FXMEmailSecurity dumpSettingsDB];
}

- (IBAction) toggleDeleteAllAttachments:(id)inSender
{
	[AttachmentManager deleteAllAttachments];
}

- (IBAction) toggleTraceActiveSyncStart:(id)inSender
{
    TraceEngine* aTraceEngine = [TraceEngine engine];
    [aTraceEngine setTraceType:kTraceTypeActiveSync];
    [aTraceEngine createTraceLog];
}

- (IBAction) toggleTraceActiveSyncInitialSync:(id)inSender
{
    ASAccount *aCurrentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];

    if(aCurrentAccount) {
        NSArray* aFolders = [aCurrentAccount foldersForFolderType:kFolderTypeInbox];
        ASFolder* aFolder = nil;
        if(aFolders.count > 0) {
            aFolder = [aFolders objectAtIndex:0];
            [aFolder resyncFolder];
        }
    }
}

- (IBAction) toggleTraceActiveSyncStop:(id)inSender
{
    TraceEngine* aTraceEngine = [TraceEngine engine];
    [aTraceEngine close];
    NSData* aData = [aTraceEngine getData];
    if(aData.length > 0) {
        NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
        FXDebugLog(kFXLogActiveSync, @"%@\n", aDataString);
#if 0
        ASAccount *currentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
        composeMailVC* aComposeViewController = [composeMailVC createMailComposerWithTo:[NSArray arrayWithObject:kLogEmailAddress] cc:nil bcc:nil attachments:nil account:self.account];
        NSString* aSubject = [TraceEngine stringForTraceType:[[TraceEngine engine] traceType]];
        [aComposeViewController setSubject:aSubject];
        [aComposeViewController setBody:aTraceLog];
        [self.navigationController pushViewController:aComposeViewController animated:YES];
#endif
    }else{
        FXDebugLog(kFXLogActiveSync, @"Trace log is empty");
    }
    [aTraceEngine removeTraceLog];
}

- (IBAction) toggleAutoTest:(id)inSender
{
    ASAccount *currentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    self.autoTest = [[ASAutoTest alloc] initWithAccount:currentAccount];
    [self.autoTest start];
}

- (IBAction) toggleEmailBodyType:(id)inSender
{
	switch (self.emailBodyType.selectedSegmentIndex) {
		case 0: // text
			[AppSettings setEmailBodyType:emailBodyText];
			break;
		default:
		case 1: // html
			[AppSettings setEmailBodyType:emailBodyHTML];
			break;
		case 2: // both
			[AppSettings setEmailBodyType:emailBodyTextHTML];
			break;
	}

	return;
}

@end

#pragma mark - Developer System VC

@implementation developerSystemVC

- (void) redoControls
{
	NSString *theString = @"No Account\nWhat's goin' on??\n";
	BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
	if (currentAccount != nil) {
		theString = [NSString stringWithFormat:@"Device ID: %@\n", currentAccount.deviceID];
		theString = [theString stringByAppendingFormat:@"Device Model: %@\n\n", currentAccount.deviceModel];

		theString = [theString stringByAppendingFormat:@"Protocol Version: %@\n", currentAccount.protocolVersion];
		theString = [theString stringByAppendingFormat:@"ActiveSync Version: %.4f\n\n", currentAccount.ASVersion];

		theString = [theString stringByAppendingString:@"AS Commands supported:\n"];
		for (NSString *tmpString in currentAccount.commands) {
			theString = [theString stringByAppendingFormat:@"%@\n", tmpString];
		}
	}

	self.textView.text = theString;
}

@end

#pragma mark - Developer FXDebugLog VC

@implementation developerFXDebugLogVC

- (void) redoControls
{
	long long debugFlags = FXGetFlags();

	self.traceSwitch.on = (debugFlags & kFXLogTrace) != 0;
	self.fixmeSwitch.on = (debugFlags & kFXLogFIXME) != 0;
	self.pkMIMESwitch.on = (debugFlags & kFXLogPKMime) != 0;
	self.szlSecuritySwitch.on = (debugFlags & kFXLogSZLSecurity) != 0;

	self.activeSyncSwitch.on = (debugFlags & kFXLogActiveSync) != 0;
	self.wbxmlSwitch.on = (debugFlags & kFXLogWBXML) != 0;
	self.asEmailSwitch.on = (debugFlags & kFXLogASEmail) != 0;
	self.asFolderSwitch.on = (debugFlags & kFXLogASFolder) != 0;

	self.viewDidLoadUnloadSwitch.on = (debugFlags & kFXLogVCLoadUnload) != 0;
	self.viewDidAppearDisappearSwitch.on = (debugFlags & kFXLogVCAppearDisappear) != 0;
	self.deallocSwitch.on = (debugFlags & kFXLogVCDealloc) != 0;

	self.settingsSwitch.on = (debugFlags & kFXLogSettings) != 0;
	self.contactsSwitch.on = (debugFlags & kFXLogContacts) != 0;
	self.calendarSwitch.on = (debugFlags & kFXLogCalendar) != 0;
	self.notesSwitch.on = (debugFlags & kFXLogNotes) != 0;

	self.composeMailSwitch.on = (debugFlags & kFXLogComposeMailVC) != 0;
	self.mailViewerSwitch.on = (debugFlags & kFXLogMailViewerVC) != 0;
	self.addressViewSwitch.on = (debugFlags & kFXLogSZCAddressView) != 0;
	self.textEntryViewSwitch.on = (debugFlags & kFXLogSZCTextEntryView) != 0;
	self.attachmentViewSwitch.on = (debugFlags & kFXLogSZCAttachmentView) != 0;
	self.attachmentManagerSwitch.on = (debugFlags & kFXLogAttachmentManager) != 0;
}

- (IBAction) toggleTurnOnAllFlags:(id)inSender
{
	FXSetFlags(kFXLogAll);
	[self redoControls];
}

- (IBAction) toggleTurnOffAllFlags:(id)inSender
{
	FXSetFlags(0x0000);
	[self redoControls];
}

- (IBAction) toggleSwitch:(id)inSender
{
	long long debugFlags = FXGetFlags();

	if (inSender == self.traceSwitch) debugFlags |= kFXLogTrace;
	else if (inSender == self.fixmeSwitch) debugFlags |= kFXLogFIXME;
	else if (inSender == self.pkMIMESwitch) debugFlags |= kFXLogPKMime;
	else if (inSender == self.szlSecuritySwitch) debugFlags |= kFXLogSZLSecurity;

	else if (inSender == self.activeSyncSwitch) debugFlags |= kFXLogActiveSync;
	else if (inSender == self.wbxmlSwitch) debugFlags |= kFXLogWBXML;
	else if (inSender == self.asEmailSwitch) debugFlags |= kFXLogASEmail;
	else if (inSender == self.asFolderSwitch) debugFlags |= kFXLogASFolder;

	else if (inSender == self.viewDidLoadUnloadSwitch) debugFlags |= kFXLogVCLoadUnload;
	else if (inSender == self.viewDidAppearDisappearSwitch) debugFlags |= kFXLogVCAppearDisappear;
	else if (inSender == self.deallocSwitch) debugFlags |= kFXLogVCDealloc;

	else if (inSender == self.settingsSwitch) debugFlags |= kFXLogSettings;
	else if (inSender == self.contactsSwitch) debugFlags |= kFXLogContacts;
	else if (inSender == self.calendarSwitch) debugFlags |= kFXLogCalendar;
	else if (inSender == self.notesSwitch) debugFlags |= kFXLogNotes;

	else if (inSender == self.composeMailSwitch) debugFlags |= kFXLogComposeMailVC;
	else if (inSender == self.mailViewerSwitch) debugFlags |= kFXLogMailViewerVC;
	else if (inSender == self.addressViewSwitch) debugFlags |= kFXLogSZCAddressView;
	else if (inSender == self.textEntryViewSwitch) debugFlags |= kFXLogSZCTextEntryView;
	else if (inSender == self.attachmentViewSwitch) debugFlags |= kFXLogSZCAttachmentView;
	else if (inSender == self.attachmentManagerSwitch) debugFlags |= kFXLogAttachmentManager;

	FXSetFlags(debugFlags);
	[self redoControls];
}

@end

#pragma mark - Developer Certificate VC

@implementation developerCertificateVC

- (void) redoControls
{
	NSString *theString = @"";

	NSArray *theAttributes = [FXMEmailSecurity getAllUserAttributes];
	if (theAttributes.count > 0) {
		theString = [theAttributes componentsJoinedByString:@"\n"];
	}

	self.attributesView.text = theString;
}

- (IBAction) toggleDeleteContainerCertificate:(id)inSender
{
	[FXMEmailSecurity deleteUserCertificate];
}

@end
