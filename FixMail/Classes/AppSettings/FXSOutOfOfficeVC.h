//
//  FXSOutOfOfficeVC.h
//  FixMail
//
//  Created by dexter.kim on 2013-07-16.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXDatePickerView.h"
#import "FXDatePickerVC.h"

@interface FXSOutOfOfficeVC : UITableViewController <UITextViewDelegate, FXDatePickerViewDelegate, FXDatePickerVCDelegate>

@property (strong, nonatomic) IBOutlet FXDatePickerView *fxDatePickerView;

@end
