//
//  settingsCells.m
//  FXSettings
//
//  Created by Colin Biggin on 2013-01-24.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "settingsCells.h"
#import "UIView-ViewFrameGeometry.h"

@implementation settingsTitleCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;

	UILabel *theLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, 200.0, 21.0)];
	theLabel.backgroundColor = [UIColor clearColor];
	theLabel.font = [UIFont boldSystemFontOfSize:17];
	theLabel.textAlignment = NSTextAlignmentLeft;
	theLabel.textColor = [UIColor blackColor];
	theLabel.autoresizingMask = UIViewAutoresizingNone;
	self.titleLabel = theLabel;

	theLabel = [[UILabel alloc] initWithFrame:CGRectMake(70.0, 10.0, 200.0, 21.0)];
	theLabel.backgroundColor = [UIColor clearColor];
	theLabel.font = [UIFont systemFontOfSize:17];
	theLabel.textAlignment = NSTextAlignmentRight;
	theLabel.textColor = [UIColor colorWithRed:(80.0/255.0) green:(103.0/255.0) blue:(142.0/255.0) alpha:1.0];
	theLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
	self.subtitleLabel = theLabel;

	[self.contentView addSubview:self.titleLabel];
	[self.contentView addSubview:self.subtitleLabel];

	self.autoresizesSubviews = true;
	self.clipsToBounds = true;
	self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	self.selectionStyle = UITableViewCellSelectionStyleBlue;

    return self;
}

+ (CGFloat) rowHeight
{
	return 44.0;
}

@end

#pragma mark -

@implementation settingsSwitchCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;

	UILabel *theLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, 200.0, 21.0)];
	theLabel.backgroundColor = [UIColor clearColor];
	theLabel.font = [UIFont boldSystemFontOfSize:17];
	theLabel.textAlignment = NSTextAlignmentLeft;
	theLabel.textColor = [UIColor blackColor];
	theLabel.autoresizingMask = UIViewAutoresizingNone;
	self.titleLabel = theLabel;

	UISwitch *theSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
	theSwitch.right = 270;
	theSwitch.top = 10;
	self.switchField = theSwitch;

	[self.contentView addSubview:self.titleLabel];
	[self.contentView addSubview:self.switchField];

	self.autoresizesSubviews = true;
	self.clipsToBounds = true;
	self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	self.selectionStyle = UITableViewCellSelectionStyleBlue;

    return self;
}

+ (CGFloat) rowHeight
{
	return 44.0;
}

@end
