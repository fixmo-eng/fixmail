//
//  settingsCells.h
//  FXSettings
//
//  Created by Colin Biggin on 2013-01-24.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

@interface settingsTitleCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;

+ (CGFloat) rowHeight;

@end

@interface settingsSwitchCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UISwitch *switchField;

+ (CGFloat) rowHeight;

@end
