//
//  FXDatePickerView.m
//  FixMail
//
//  Created by dexter.kim on 2013-07-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXDatePickerView.h"

#define tagDimView      1000
#define kDimViewAlpha   0.7f

@interface FXDatePickerView() {
}

@property (strong, nonatomic) id<FXDatePickerViewDelegate> delegate;

@end

@implementation FXDatePickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSDate*)dateFromString:(NSString*)date withDateFormat:(NSString*)dateFormat
{
    if(!date) {
        return nil;
    }
    
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter dateFromString:date];
}

- (NSString*)stringFromDate:(NSDate*)date withDateFormat:(NSString*)dateFormat
{
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter stringFromDate:date];
}

#pragma mark Setup Date Picker View
- (void)setupWithFrame:(CGRect)rect
                target:(id<FXDatePickerViewDelegate>)target
 shouldDimOnBackground:(BOOL)shouldDim
           minimumDate:(NSDate *)minDate
{
    [self setFrame:rect];
    _delegate = target;
    _shouldDimOnBackground = shouldDim;
}

- (void)presentViewWithAnimation:(BOOL)animate
                      completion:(void (^)(BOOL))completion
{
    if(_delegate) {        
        [_datepickerToolbar setBarStyle:UIBarStyleBlackTranslucent];
        [_segmntChangeDateMode setSelectedSegmentIndex:(NSInteger)_kDateMode];
        
        _datepicker.timeZone = [NSTimeZone localTimeZone];
        if(_strDates) {
            [_datepicker setMinimumDate:_kDateMode==kDatePickerStartDate ? [NSDate date] : [self dateFromString:_strDates[kDatePickerStartDate] withDateFormat:_dateFormat] ];
            [_datepicker setDate:[self dateFromString:_strDates[_kDateMode] withDateFormat:_dateFormat] animated:YES];
        } else {
            [_datepicker setMinimumDate:[NSDate date]];
            [_datepicker setDate:[NSDate date] animated:YES];
        }
        
        CGRect rect = self.frame;
        rect.origin.x = 0.0f;
        if(!IS_IPAD()) {
            rect.origin.y = SCREEN_HEIGHT - self.frame.size.height;
        } else {
            rect.origin.y = SCREEN_HEIGHT - self.frame.size.height;
            rect.size.width = SCREEN_WIDTH;
        }
        [UIView animateWithDuration:animate ? 0.35f : 0.0f
                         animations:^{
                             [self setFrame:rect];
                         }
        ];
        
        if(_shouldDimOnBackground)
            [self shouldDimOnBackground:YES];
        
        if(completion) {
            completion(YES);
        }
    } else {
        NSLog(@"This view isn't setup yet.");
    }
}

- (void)dismissWithAnimation:(BOOL)animate completion:(void (^)(BOOL))completion
{
    CGRect rect = self.frame;
    rect.origin.y = [UIScreen mainScreen].bounds.size.height;
    [UIView animateWithDuration:animate ? 0.35f : 0.0f
                     animations:^{
                         [self setFrame:rect];
                     }
                     completion:^(BOOL inFinished) {                         
                         [self removeFromSuperview];
                     }
    ];
    
    if(_shouldDimOnBackground)
        [self shouldDimOnBackground:NO];
    
    if(completion) {
        completion(YES);
    }
}

- (void)shouldDimOnBackground:(BOOL)shouldDim
{
    if(shouldDim) {
        CGRect rect = [UIScreen mainScreen].bounds;
        UIView *dimView = [[UIView alloc] initWithFrame:rect];
        [dimView setTag:tagDimView];
        [dimView setBackgroundColor:[UIColor clearColor]];
        [dimView setAlpha:0.0f];
        NSLog(@"FXDatePickerView's superview: %p", self.superview);
        [self.superview addSubview:dimView];
        [self.superview bringSubviewToFront:self];
        
        [UIView animateWithDuration:0.35f
                         animations:^{
                             [dimView setBackgroundColor:[UIColor blackColor]];
                             [dimView setAlpha:kDimViewAlpha];
                         }
                         completion:^(BOOL isFinished){
                             NSLog(@"Dimmed");
                         }];
        
    } else {
        __block UIView *dimView = [self.superview viewWithTag:tagDimView];
        if(dimView) {
            [UIView animateWithDuration:0.35f
                             animations:^{                             
                                 [dimView setBackgroundColor:[UIColor clearColor]];
                                 [dimView setAlpha:0.0f];
                             }
                             completion:^(BOOL isFinished){
                                 [dimView removeFromSuperview];
                                 dimView = nil;
                                 NSLog(@"Lighted");
                             }];
        }
    }
}

#pragma mark Action Events
- (IBAction)dateModeChanged:(id)sender {
    _kDateMode = (EDateMode)_segmntChangeDateMode.selectedSegmentIndex;
    NSDate *sdate = [self dateFromString:_strDates[kDatePickerStartDate] withDateFormat:_dateFormat];
    NSDate *edate = [self dateFromString:_strDates[kDatePickerEndDate] withDateFormat:_dateFormat];

    if([sdate compare:edate] == NSOrderedDescending) {
        _strDates[_kDateMode==kDatePickerStartDate] = [self stringFromDate:_kDateMode==kDatePickerStartDate?sdate:edate withDateFormat:_dateFormat];
    }
    
    [_datepicker setMinimumDate:(_kDateMode==kDatePickerStartDate ? [NSDate date] : sdate)];
    [_datepicker setDate:(_kDateMode==kDatePickerStartDate?sdate:edate) animated:YES];
}

- (IBAction)dateChanged:(id)sender {
    NSDate *changedDate = _datepicker.date;
    NSDate *today = [NSDate date];  
    NSInteger interval = [[[NSCalendar currentCalendar] components: NSDayCalendarUnit
                                                          fromDate: today
                                                            toDate: changedDate
                                                           options: 0] day];

    NSLog(@"%@ - %@, %@", _kDateMode==kDatePickerStartDate ? @"kStartDate" : @"kEndDate", changedDate, _datepicker.minimumDate);
    _strDates[_kDateMode] = [self stringFromDate:changedDate withDateFormat:_dateFormat];
    NSDate *another_date = [self dateFromString:_strDates[_kDateMode==kDatePickerStartDate] withDateFormat:_dateFormat]; // xor
    if(interval>=0) {
        switch (_kDateMode) {
            case kDatePickerStartDate: {
                NSComparisonResult result = [_datepicker.minimumDate compare:changedDate];
                if(result != NSOrderedAscending) {
                    [_datepicker setDate:_datepicker.minimumDate animated:YES];
                } else {
                    result = [changedDate compare:another_date];
                    if(result != NSOrderedAscending) {
                        _strDates[kDatePickerEndDate] = _strDates[kDatePickerStartDate];
                        [_datepicker setDate:changedDate animated:YES];                        
                    }
                }
            } break;
                
            case kDatePickerEndDate: {
                NSComparisonResult result = [another_date compare:changedDate];
                if(result != NSOrderedAscending) {
                    if(result == NSOrderedSame) {
                        NSDate *thedaybefore = [changedDate dateByAddingTimeInterval:60*60*24*(-1)];
                        [_datepicker setDate:thedaybefore animated:YES];
                        [_datepicker setDate:_datepicker.minimumDate animated:YES];
                    } else {
                        [_datepicker setDate:changedDate animated:YES];
                    }
                }
            } break;
                
            default:
                break;
        }
        [self.delegate datePicker:self didChangedDates:_strDates withMode:_kDateMode];
    } else if(interval<0) {
        [_datepicker setMinimumDate:_kDateMode==kDatePickerStartDate ? today : another_date];
        [_datepicker setDate:[self dateFromString:_strDates[_kDateMode] withDateFormat:_dateFormat] animated:YES];
        NSString *errMsg = [NSString stringWithFormat:@"%@ Date is invalid.", _kDateMode==kDatePickerStartDate ? @"Start" : @"End"];
        [[[UIAlertView alloc] initWithTitle:@"Invalid Date"
                                    message:errMsg
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

- (IBAction)today:(id)sender {
    _kDateMode = _segmntChangeDateMode.selectedSegmentIndex = kDatePickerStartDate;
    [_datepicker setMinimumDate:[NSDate date]];
    [_datepicker setDate:[NSDate date] animated:YES];
    _strDates[kDatePickerEndDate] = _strDates[kDatePickerStartDate] = [self stringFromDate:_datepicker.date withDateFormat:_dateFormat];
    [self.delegate datePicker:self didChangedDates:_strDates withMode:_kDateMode];
}

- (IBAction)closeDatePicker:(id)sender {
    [self.delegate datePickerDidClosed:self];
}
@end
