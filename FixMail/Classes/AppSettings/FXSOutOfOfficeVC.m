//
//  FXSOutOfOfficeVC.m
//  FixMail
//
//  Created by dexter.kim on 2013-07-16.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXSOutOfOfficeVC.h"
#import "BaseAccount.h"
#import "ASAccount.h"
#import "ASSettings.h"
#import "ASOutOfOffice.h"
#import "UIColor+FixmoExtensions.h"
#import "UIView-ViewFrameGeometry.h"
#import "ErrorAlert.h"
#import "DateUtil.h"
#import "Calendar.h"
#import "SZCAlertView.h"
#import "Reachability.h"
#import "FXDatePickerVC.h"

// define values
#define switchWidth             100.0f
#define switchHeight            27.0f
#define datepickerviewHeight    260.0f
#define kLoadingViewAlpha       0.5f
#define SPLIT_MASTER_WIDTH      320.0f
#define groupCellSideGapPort    50.0f
#define groupCellSideGapLand    50.0f

// Date Format
#define DATE_FORMAT             @"EdMMMyyyy"

// define tags
#define tagSubjectField         100
#define tagMessageField         101
#define tagLoadingView          102

// define strings
#define OUTOFOFFICE_LOADING     FXLLocalizedStringFromTable(@"OUTOFOFFICE_LOADING",     @"settingsVC",  @"Loading...\nOut of Office Data")
#define TITLE_ONOFF             FXLLocalizedStringFromTable(@"TITLE_ONOFF",             @"settingsVC",  @"Out of Office")
#define TITLE_START_DATE        FXLLocalizedStringFromTable(@"TITLE_START_DATE",        @"settingsVC",  @"Start Date")
#define TITLE_END_DATE          FXLLocalizedStringFromTable(@"TITLE_END_DATE",          @"settingsVC",  @"End Date")
#define TITLE_DELETE_BTN        FXLLocalizedStringFromTable(@"TITLE_DELETE_BTN",        @"settingsVC",  @"Delete")
#define PLACEHOLDER_OF_MESSAGE  FXLLocalizedStringFromTable(@"PLACEHOLDER_OF_MESSAGE",  @"settingsVC",  @"Please type your Out of Office message.")

// Section and Row Types
enum {
    kSectionState      = 0,
    kSectionDate       = 1,
    kSectionMessage    = 2
};

enum {
    kRowStartDate      = 0,
    kRowEndDate        = 1
};

@interface FXSOutOfOfficeVC () {
    CGFloat _msgBaseHeight;
    SZCAlertView *_progressView;
    BOOL _isShownKeyboard;
}

@property (strong, nonatomic) UISwitch          *sw;
@property (strong, nonatomic) UITextField       *subject;
@property (strong, nonatomic) UITextView        *message;
@property (strong, nonatomic) UIPopoverController *popoverDatePicker;

@property (readwrite, nonatomic)    BOOL        outOfOfficeState;
@property (strong, nonatomic)       NSString    *outOfOfficeStartDate;
@property (strong, nonatomic)       NSString    *outOfOfficeEndDate;
@property (strong, nonatomic)       NSString    *outOfOfficeMessage;

@end

@implementation FXSOutOfOfficeVC

- (void)viewDidLoad
{
    FXDebugLog(kFXLogSettings | kFXLogVCLoadUnload, @"outOfficeSettingsVC: viewDidLoad");
	[super viewDidLoad];
    
    // Initialize Controlls
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated
{
    FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"outOfficeSettingsVC: viewWillAppear");
	[super viewDidLoad];    
    
    [self shouldShowProgressView:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"outOfficeSettingsVC: viewDidAppear");
	[super viewDidLoad];
    
    if(![self isReachable])
        [self.navigationController popViewControllerAnimated:YES];
    else {
        [self.tableView reloadData];
        [self requestOOFData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    FXDebugLog(kFXLogSettings | kFXLogVCAppearDisappear, @"outOfficeSettingsVC: viewWillDisappear");
	[super viewDidLoad];
    
    if([self isReachable])
        [self updateOOFData];
    [_message resignFirstResponder];
    [self shouldShowProgressView:NO];
}

- (void)viewDidDisappear:(BOOL)animated
{
    FXDebugLog(kFXLogVCAppearDisappear, @"outOfficeSettingsVC: viewDidDisappear");
	[super viewDidLoad];
    
    [self uninitialize];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    FXDebugLog(kFXLogVCDealloc, @"outOfficeSettingsVC: dealloc");
	[super viewDidLoad];
    
    [self uninitialize];
}

- (void)initialize
{
    _isShownKeyboard = NO;
    
    // Initialize OOF Data Array
    _outOfOfficeState = NO;
    _outOfOfficeStartDate = [DateUtil stringFromDate:[NSDate date] withDateFormat:DATE_FORMAT];
    _outOfOfficeEndDate = [DateUtil stringFromDate:[NSDate date] withDateFormat:DATE_FORMAT];
    _outOfOfficeMessage = @"";
    
    if(!IS_IPAD()) {
        // FXDatePicker
        CGRect rect = _fxDatePickerView.frame;
        rect.origin.y = [UIScreen mainScreen].bounds.size.height;
        [_fxDatePickerView setupWithFrame:rect
                                   target:self
                    shouldDimOnBackground:YES
                              minimumDate:[NSDate date]];
    }
    
    [self registerForKeyboardNotifications];
}

- (void)uninitialize
{
    [self removeForKeyboardNotification];
}


#pragma mark Reachability
- (BOOL)isReachable
{
    ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
    Reachability* aReachability = currentAccount.reachability;
    return aReachability.currentReachabilityStatus != NotReachable;
}

#pragma mark Loading Indicator
-(void)shouldShowProgressView:(BOOL)shouldShow
{
    if(shouldShow) {
        CGRect rect = [UIScreen mainScreen].bounds;
        UIView *loadingView = [[UIView alloc] initWithFrame:rect];
        [loadingView setTag:tagLoadingView];
        [loadingView setBackgroundColor:[UIColor clearColor]];
        [loadingView setAlpha:0.0f];
        [self.navigationController.view addSubview:loadingView];
        
        if (!_progressView) {
            _progressView = [[SZCAlertView alloc] initWithFrame:CGRectMake(0, 0, 180, 120)];
            _progressView.backgroundColor = [UIColor blackColor];
            [_progressView setHidden:YES];
            _progressView.title.text = OUTOFOFFICE_LOADING;
        }
        
        if (_progressView.superview != self.view) {
            [self.view addSubview:_progressView];
            [self.view bringSubviewToFront:_progressView];
        }
        
        [_progressView.spinner startAnimating];
        _progressView.center = CGPointMake(CGRectGetMidX(self.view.bounds),CGRectGetMidY(self.view.bounds));
        _progressView.alpha = 0.0f;
        _progressView.hidden = NO;
        [UIView animateWithDuration:DEFAULT_ANIMATION_SPEED animations:^{
            _progressView.alpha = 1.0f;
            [loadingView setBackgroundColor:[UIColor blackColor]];
            [loadingView setAlpha:kLoadingViewAlpha];
        }];
    } else {
        __block UIView *loadingView = [self.navigationController.view viewWithTag:tagLoadingView];
        [UIView animateWithDuration:DEFAULT_ANIMATION_SPEED animations:^{
            _progressView.alpha = 0.0f;
            [loadingView setBackgroundColor:[UIColor clearColor]];
            [loadingView setAlpha:0.0f];
        } completion:^(BOOL finished) {
            _progressView.hidden = YES;
            [_progressView.spinner stopAnimating];
            [_progressView removeFromSuperview];
            _progressView = nil;
            [loadingView removeFromSuperview];
            loadingView = nil;
        }];
    }
}

#pragma mark ActiveSync Communication
- (void)requestOOFData
{    
    // sendGetOutOfOffice
    ASAccount *currentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    ASSettings* aSettings = [ASSettings queueGetOutOfOffice:currentAccount
                                            displayErrors:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfGetOutOfOffice:) name:kActiveSyncSuccessNotification object:aSettings];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfGetOutOfOffice:) name:kActiveSyncFailNotification object:aSettings];
}

- (void)responseOfGetOutOfOffice:(NSNotification*)notification
{
    if([[notification name] isEqualToString:kActiveSyncSuccessNotification]) {
        ASSettings* aSettings = (ASSettings*)notification.object;
        FXDebugLog(kFXLogActiveSync, @"GetOutOfOffice success");
        
        // Out of Office - State
        _outOfOfficeState = aSettings.oofInternal.enabled;
        
        if(_outOfOfficeState) {
            NSDate *sdate=aSettings.startDate, *edate=aSettings.endDate, *today=[NSDate date];
            
            // Out of Office - Start && End Date
            NSComparisonResult result = [edate compare:today];
            BOOL isAfterToday = result != NSOrderedAscending;
            if(isAfterToday) {
                _outOfOfficeStartDate = [DateUtil stringFromDate:sdate withDateFormat:DATE_FORMAT];
                _outOfOfficeEndDate = [DateUtil stringFromDate:edate withDateFormat:DATE_FORMAT];
            } else {
                _outOfOfficeStartDate = [DateUtil stringFromDate:today withDateFormat:DATE_FORMAT];
                _outOfOfficeEndDate = [DateUtil stringFromDate:today withDateFormat:DATE_FORMAT];
            }
            
            // Out of Office - Auto Reply Message
            NSString* message = aSettings.oofInternal.message;
            if(message.length==0 || message==nil || [message isEqualToString:@"\r\n"])
                _outOfOfficeMessage = @"";
            else
                _outOfOfficeMessage = message;
            
            _sw.on = YES;
            [self toggleOutofOfficeSwitch:_sw];
        }
        [self shouldShowProgressView:NO];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncSuccessNotification object:nil];
        
    } else if([[notification name] isEqualToString:kActiveSyncFailNotification]) {
        ASSettings *aSettings = (ASSettings*)notification.object;
        [aSettings displayError];
        [self shouldShowProgressView:NO];
        [self.navigationController popViewControllerAnimated:YES];
		[[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncFailNotification object:nil];
    }
}

- (void)updateOOFData
{    
    // sendGetOutOfOffice
    ASAccount *currentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    
    NSDate* aStartDate  = [DateUtil dateFromString:_outOfOfficeStartDate withDateFormat:DATE_FORMAT];
    NSDate* anEndDate   = [DateUtil dateFromString:_outOfOfficeEndDate withDateFormat:DATE_FORMAT];
    aStartDate  = [[Calendar calendar] addToDate:aStartDate hours:00 minutes:00];
    anEndDate   = [[Calendar calendar] addToDate:anEndDate hours:23 minutes:59];
    
    ASOutOfOffice* asOofInternal = [[ASOutOfOffice alloc] init];
    asOofInternal.type      = kOutOfOfficeInternal;
    asOofInternal.enabled   = _outOfOfficeState;
    asOofInternal.bodyType  = kOOFBodyText;
    asOofInternal.message   = _outOfOfficeMessage;
    
    ASOutOfOffice* asOofExternalKnown = [[ASOutOfOffice alloc] init];
    asOofExternalKnown.type      = kOutOfOfficeExternalKnown;
    asOofExternalKnown.enabled   = _outOfOfficeState;
    asOofExternalKnown.bodyType  = kOOFBodyText;
    asOofExternalKnown.message   = _outOfOfficeMessage;
    
    ASOutOfOffice* asOofExternalUnknown = [[ASOutOfOffice alloc] init];
    asOofExternalUnknown.type      = kOutOfOfficeExternalUnknown;
    asOofExternalUnknown.enabled   = _outOfOfficeState;
    asOofExternalUnknown.bodyType  = kOOFBodyText;
    asOofExternalUnknown.message   = _outOfOfficeMessage;
    
    ASSettings* aSettings = [ASSettings queueSetOutOfOfficeForTime:currentAccount
                                                             start:aStartDate
                                                               end:anEndDate
                                                          internal:asOofInternal
                                                     externalKnown:asOofExternalKnown
                                                   externalUnknown:asOofExternalUnknown
                                                     displayErrors:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfSetOutOfOffice:) name:kActiveSyncSuccessNotification object:aSettings];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfSetOutOfOffice:) name:kActiveSyncFailNotification object:aSettings];
}

- (void)responseOfSetOutOfOffice:(NSNotification*)notification
{
    [self.navigationController popViewControllerAnimated:YES];
    
    if([[notification name] isEqualToString:kActiveSyncSuccessNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncSuccessNotification object:nil];
    } else if([[notification name] isEqualToString:kActiveSyncFailNotification]) {
        ASSettings* aSettings = (ASSettings*)notification.object;
        [aSettings displayError];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncFailNotification object:nil];
    }
}

- (void)disableOOFData
{
    ASAccount *currentAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    ASSettings *aSettings = [ASSettings queueDisableOutOfOffice:currentAccount
                                                  displayErrors:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfDisableOutOfOffice:) name:kActiveSyncSuccessNotification object:aSettings];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseOfDisableOutOfOffice:) name:kActiveSyncFailNotification object:aSettings];
}

- (void)responseOfDisableOutOfOffice:(NSNotification*)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[notification name] object:nil];
    if([[notification name] isEqualToString:kActiveSyncSuccessNotification]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncSuccessNotification object:nil];
    } else if([[notification name] isEqualToString:kActiveSyncFailNotification]) {
        ASSettings* aSettings = (ASSettings*)notification.object;
        [aSettings displayError];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kActiveSyncFailNotification object:nil];
    }
}

#pragma mark UITableViewDataSource & UITableViewDelegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSection = 1;
    
    if(_sw && _sw.on) // After ViewDdidAppear
        numOfSection = 3;
    else if(_outOfOfficeState) // Before ViewDidAppear
        numOfSection = 3;
    
    return numOfSection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = 0;
    
    switch (section) {
        case kSectionState:
            rows = 1;
            break;
            
        case kSectionDate:
            rows = 2;
            break;
            
        case kSectionMessage:
            rows = 1;
            break;
    }
    
    return rows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == kSectionMessage) {
        if(IS_IPAD()) {
            _msgBaseHeight = 298.0f;
        } else if(IS_IPHONE5()) {
            _msgBaseHeight = 298.0f;
        } else if(IS_IPHONE()) {
            _msgBaseHeight = 210.0f;
        }
        return _msgBaseHeight;
    }
    
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"outofOfficeCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"outofOfficeCell"];
    }
    
    int section = indexPath.section;
    
    switch (section) {
        case kSectionState: { // switch
            cell.textLabel.text = TITLE_ONOFF;
            cell.detailTextLabel.text = @"";
            if(!_sw) {
                _sw = [[UISwitch alloc] init];
                CGRect rect;
                if(!IS_IPAD()) {
                    rect = cell.bounds;
                    rect.size.width = switchWidth;
                    rect.size.height = switchHeight;
                    rect.origin.x = cell.bounds.size.width - rect.size.width;
                    rect.origin.y = (cell.bounds.size.height - rect.size.height)/2;
                    [_sw setFrame:rect];
                } else {
                    rect = cell.frame;
                    rect.size.width = switchWidth;
                    rect.size.height = switchHeight;
                    UIDevice* aDevice = [UIDevice currentDevice];
                    if(aDevice.orientation == UIDeviceOrientationLandscapeLeft
                       || aDevice.orientation == UIDeviceOrientationLandscapeRight) {
                        rect.origin.x = cell.bounds.size.width - switchWidth - groupCellSideGapLand;
                    } else {
                        rect.origin.x = cell.bounds.size.width - switchWidth - groupCellSideGapPort;
                    }
                    rect.origin.y = (cell.bounds.size.height - rect.size.height)/2;
                    [_sw setFrame:rect];                    
                    [_sw setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
                }
                [_sw addTarget:self action:@selector(toggleOutofOfficeSwitch:) forControlEvents:UIControlEventValueChanged];
                [cell addSubview:_sw];
            }
            _sw.on = _outOfOfficeState;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        } break;
            
        case kSectionDate: { // Start & End Date            
            if(indexPath.row == kRowStartDate) {
                cell.textLabel.text = TITLE_START_DATE;
                
                if(_outOfOfficeStartDate) {
                    cell.detailTextLabel.text = _outOfOfficeStartDate;
                } else {
                    cell.detailTextLabel.text = [DateUtil stringFromDate:[NSDate date] withDateFormat:DATE_FORMAT];
                }
            } else if(indexPath.row == kRowEndDate) {
                cell.textLabel.text = TITLE_END_DATE;
                
                if(_outOfOfficeEndDate) {
                    cell.detailTextLabel.text = _outOfOfficeEndDate;
                } else {
                    cell.detailTextLabel.text = [DateUtil stringFromDate:[NSDate date] withDateFormat:DATE_FORMAT];
                }
            }
        } break;
            
        case kSectionMessage: { // Message
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"outofOfficeCell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            CGFloat insetX = 14.0f;
            if(IS_IPAD()) {
                insetX = 20.0f;
            }
            _message = [[UITextView alloc]initWithFrame:CGRectInset(cell.frame, insetX, 5.0f)];
            [_message setTag:tagMessageField];
            [_message setDelegate:self];
            [_message setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
            [_message setBackgroundColor:[UIColor clearColor]];
            [_message setKeyboardType:UIKeyboardTypeAlphabet];
            
            UIToolbar *toolbar = [[UIToolbar alloc] init];
            [toolbar setBarStyle:UIBarStyleBlackTranslucent];
            [toolbar sizeToFit];
            UIBarButtonItem *delButton =[[UIBarButtonItem alloc] initWithTitle:TITLE_DELETE_BTN style:UIBarButtonItemStyleDone target:self action:@selector(deleteMessage)];
            [delButton setTintColor:[UIColor redColor]];
            UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
            UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard)];
            NSArray *itemsArray = [NSArray arrayWithObjects:delButton, flexButton, doneButton, nil];
            [toolbar setItems:itemsArray];
            [_message setInputAccessoryView:toolbar];
            
            [_message setFont:[UIFont systemFontOfSize:15.0f]];
            if(_outOfOfficeMessage)
                _message.text = _outOfOfficeMessage;
            else
                _message.text = @"";
            if(_message.text.length == 0){
                _message.textColor = [UIColor lightGrayColor];
                _message.text = PLACEHOLDER_OF_MESSAGE;
            }
            
            [cell addSubview:_message];
        } break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1) {
        [_message resignFirstResponder];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        if(!IS_IPAD()) {
            [self toggleShowDatePickerView:YES withDateMode:indexPath.row];
        } else {
            FXDatePickerVC* popoverContent = [UIStoryboard storyboardWithName:@"settings_iPad"
                                                                   identifier:@"FXDatePickerVC"];
            
            popoverContent.delegate = self;
            NSMutableArray* dates = [NSMutableArray new];
            for(int i=0 ; i<2 ; i++) {
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i
                                                                                                 inSection:kSectionDate]];
                [dates addObject:cell.detailTextLabel.text];
            }
            popoverContent.strDates = dates;
            popoverContent.kDateMode = indexPath.row;
            popoverContent.dateFormat = DATE_FORMAT;
            
            if(self.popoverDatePicker) {
                [self.popoverDatePicker dismissPopoverAnimated:NO];
                self.popoverDatePicker = nil;
            }
            self.popoverDatePicker = [[UIPopoverController alloc] initWithContentViewController:popoverContent];
            
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:kDatePickerVCEndDate
                                                                                             inSection:kSectionDate]];
            CGRect popRect = popoverContent.datepicker.frame;
            CGFloat popHei = popRect.size.height + popoverContent.datepickerToolbar.height;
            [self.popoverDatePicker setPopoverContentSize:CGSizeMake(320.0f, popHei)];
            [self.popoverDatePicker presentPopoverFromRect:cell.frame
                                                    inView:self.tableView
                                  permittedArrowDirections:UIPopoverArrowDirectionUp
                                                  animated:YES];
        }
    }
}

#pragma mark Action Events
- (void)toggleOutofOfficeSwitch:(id)sender
{
    UISwitch *sw = (UISwitch*)sender;
    if(sw.on) {
    } else {
        [self disableOOFData];
    }
    _outOfOfficeState = sw.on;
    [self.tableView reloadData];
}

- (void)toggleShowDatePickerView:(BOOL)shouldShow withDateMode:(EDateMode)kDateMode
{    
    if(shouldShow) {
        BOOL isExist = [[self.navigationController.view subviews] containsObject:_fxDatePickerView];
        if(!isExist) {
            [self.navigationController.view addSubview:_fxDatePickerView];
            
            NSMutableArray *dates = [NSMutableArray new];
            for(int i=0 ; i<2 ; i++) {
                UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i
                                                                                                 inSection:kSectionDate]];
                [dates addObject:cell.detailTextLabel.text];
            }
            
            _fxDatePickerView.strDates = dates;
            _fxDatePickerView.kDateMode = kDateMode;
            _fxDatePickerView.dateFormat = DATE_FORMAT;
            [_fxDatePickerView presentViewWithAnimation:YES
                                             completion:^(BOOL isFinished){
                                                 CGPoint pos = self.tableView.contentOffset;
                                                 if(IS_IPHONE()) {
                                                     pos.y += 20.0f;
                                                 }
                                                 [self.tableView setContentOffset:pos
                                                                         animated:YES];
                                             }];
        }
    } else {
        BOOL isExist = [[self.navigationController.view subviews] containsObject:_fxDatePickerView];
        if(isExist) {
            CGPoint pos = self.tableView.contentOffset;
            if(IS_IPHONE()) {
                pos.y -= 20.0f;
            }
            [self.tableView setContentOffset:pos animated:YES];
            
            [_fxDatePickerView dismissWithAnimation:YES completion:nil];
        }
    }
}

#pragma mark FXDatePickerViewDelegate
- (void)datePicker:(FXDatePickerView *)picker didChangedDates:(NSArray *)strDates withMode:(EDateMode)kDateMode
{    
    for(int i=0 ; i<[strDates count] ; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:kSectionDate];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = strDates[i];
        
        if(i == kDateMode) {
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            [UIView animateWithDuration:0.35f
                             animations:^{                             
                                 [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                             }
             ];
        }
        
        if(i==0) {
            _outOfOfficeStartDate = strDates[i];
        } else {
            _outOfOfficeEndDate = strDates[i];
        }
    }
}

- (void)datePickerDidClosed:(FXDatePickerView *)picker
{
    [self toggleShowDatePickerView:NO withDateMode:-1];
}

#pragma mark FXDatePickerVCDelegate
- (void)datePickerVC:(FXDatePickerVC *)pickerVC didChangedDates:(NSArray *)strDates withMode:(EDateModeVC)kDateMode
{
    for(int i=0 ; i<[strDates count] ; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:kSectionDate];
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.detailTextLabel.text = strDates[i];
        
        if(i == kDateMode) {
            [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
            [UIView animateWithDuration:0.35f
                             animations:^{
                                 [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                             }
             ];
        }
        
        if(i==0) {
            _outOfOfficeStartDate = strDates[i];
        } else {
            _outOfOfficeEndDate = strDates[i];
        }
    }
}

#pragma mark UITextViewDelegate for Message
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if (_message.textColor == [UIColor lightGrayColor]) {
        _message.text = @"";
        _message.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"] && _message.text.length == 0){
        _message.textColor = [UIColor lightGrayColor];
        _message.text = PLACEHOLDER_OF_MESSAGE;
        [_message resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{    
    _outOfOfficeMessage = _message.text;
    
    if(_message.text.length == 0){
        _message.textColor = [UIColor lightGrayColor];
        _message.text = PLACEHOLDER_OF_MESSAGE;
    }
    
    [_message setContentOffset:CGPointZero];
}

- (void)deleteMessage
{
    _message.text = @"";
    _message.textColor = [UIColor blackColor];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSectionMessage]];
    CGFloat diff = cell.bounds.size.height - _msgBaseHeight;
    [cell setHeight:_msgBaseHeight];
    CGSize tsize = self.tableView.contentSize;
    tsize.height -= diff;
    [self.tableView setContentSize:tsize];
    
    _outOfOfficeMessage = @"";
}

-(void)resignKeyboard
{
    [_message resignFirstResponder];
}

#pragma mark Keyboard Notifications
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidChangeFrame:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
}

- (void)removeForKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidChangeFrameNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if(IS_IPHONE()) {
        UITableViewCell *msgcell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSectionMessage]];
        
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        CGFloat bottom = 20.0f;
        if([msgcell isEditing])
            bottom = 0.0f;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height + bottom, 0.0);
        if(!UIEdgeInsetsEqualToEdgeInsets(self.tableView.contentInset, contentInsets)) {
            self.tableView.contentInset = contentInsets;
            self.tableView.scrollIndicatorInsets = contentInsets;
        }   
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    if(IS_IPHONE()) {
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        self.tableView.contentInset = contentInsets;
        self.tableView.scrollIndicatorInsets = contentInsets;
        [self.tableView setContentOffset:CGPointZero];
    }    
}

- (void)keyboardDidChangeFrame:(NSNotification *)aNotification
{
    CGRect keyboardEndFrame;
    [[aNotification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame fromView:nil];
    
    if (CGRectIntersectsRect(keyboardFrame, self.view.frame)) {
        self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 396.0f + 20.0f, 0.0);
        
        UIDevice* aDevice = [UIDevice currentDevice];
        if(aDevice.orientation == UIDeviceOrientationLandscapeLeft
           || aDevice.orientation == UIDeviceOrientationLandscapeRight
           || (aDevice.orientation == UIDeviceOrientationFaceUp && SCREEN_WIDTH == 1024.0f)) {
            UITableViewCell *msgcell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:kSectionMessage]];
            [self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:msgcell]
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
        }
    } else {
        self.tableView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
        
        UIDevice* aDevice = [UIDevice currentDevice];
        if(aDevice.orientation == UIDeviceOrientationLandscapeLeft
           || aDevice.orientation == UIDeviceOrientationLandscapeRight
           || (aDevice.orientation == UIDeviceOrientationFaceUp && SCREEN_WIDTH == 1024.0f)) {
            [self.tableView setContentOffset:CGPointZero animated:YES];
        }
    }
}

@end
