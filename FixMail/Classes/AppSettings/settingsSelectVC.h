//
//  settingsSelectVC.h
//  FXSettings
//
//  Created by Colin Biggin on 2013-01-01.
//  Copyright (c) 2013 Colin Biggin. All rights reserved.
//

#define kSettingsSavedNotification			@"kSettingsSavedNotification"
#define kSettingsEmailSavedNotification		@"kSettingsEmailSavedNotification"
#define kSettingsTextSavedNotification		@"kSettingsTextSavedNotification"

@interface settingsSelectVC : UITableViewController

@property (nonatomic, strong) NSArray *rowLabels;
@property (nonatomic, assign) int rowSelected;

@end

@interface emailEntrySettingsVC : UITableViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UILabel *helpTextLabel;
@property (nonatomic, weak) IBOutlet UITextField *emailField;
@property (nonatomic, strong) NSString *helpText;
@property (nonatomic, strong) NSString *emailAddress;

@end

@interface textEntrySettingsVC : UITableViewController

@property (nonatomic, weak) IBOutlet UILabel *helpTextLabel;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, strong) NSString *helpText;
@property (nonatomic, strong) NSString *text;

@end