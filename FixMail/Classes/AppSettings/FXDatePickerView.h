//
//  FXDatePickerView.h
//  FixMail
//
//  Created by dexter.kim on 2013-07-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kDatePickerStartDate = 0,
    kDatePickerEndDate = 1
}EDateMode;

@class FXDatePickerView;

@protocol FXDatePickerViewDelegate <NSObject>

@required
- (void)datePicker:(FXDatePickerView*)picker didChangedDates:(NSArray*)strDates withMode:(EDateMode)kDateMode;
- (void)datePickerDidClosed:(FXDatePickerView*)picker;

@end

@interface FXDatePickerView : UIView

@property (weak, nonatomic) IBOutlet UIToolbar *datepickerToolbar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmntChangeDateMode;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;

@property (strong, nonatomic) NSMutableArray *strDates; // 0: StartDate, 1: EndDate
@property (readwrite, nonatomic) EDateMode kDateMode;
@property (strong, nonatomic) NSString *dateFormat;
@property (readwrite, nonatomic) BOOL shouldDimOnBackground;

- (IBAction)dateModeChanged:(id)sender;
- (IBAction)dateChanged:(id)sender;
- (IBAction)today:(id)sender;
- (IBAction)closeDatePicker:(id)sender;

- (void)setupWithFrame:(CGRect)rect
                target:(id<FXDatePickerViewDelegate>)target
 shouldDimOnBackground:(BOOL)shouldDim
           minimumDate:(NSDate*)minDate;

- (void)presentViewWithAnimation:(BOOL)animate
                      completion:(void (^)(BOOL finished))completion;

- (void)dismissWithAnimation:(BOOL)animate
                  completion:(void (^)(BOOL finished))completion;

@end
