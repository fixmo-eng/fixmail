//
//  FXDatePickerVC.h
//  FixMail
//
//  Created by dexter.kim on 2013-07-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kDatePickerVCStartDate = 0,
    kDatePickerVCEndDate = 1
}EDateModeVC;

@class FXDatePickerVC;

@protocol FXDatePickerVCDelegate <NSObject>

- (void)datePickerVC:(FXDatePickerVC*)pickerVC didChangedDates:(NSArray*)strDates withMode:(EDateModeVC)kDateMode;

@end

@interface FXDatePickerVC : UIViewController

@property (weak, nonatomic) IBOutlet UIToolbar *datepickerToolbar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmntChangeDateMode;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;

@property (strong, nonatomic) id<FXDatePickerVCDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *strDates; // 0: StartDate, 1: EndDate
@property (readwrite, nonatomic) EDateModeVC kDateMode;
@property (strong, nonatomic) NSString *dateFormat;

- (IBAction)dateModeChange:(id)sender;
- (IBAction)today:(id)sender;
- (IBAction)dateChanged:(id)sender;

@end
