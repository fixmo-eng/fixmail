//
//  FXSettingsDelegate.m
//  FixMail
//
//  Created by Colin Biggin on 2013-01-02.
//
//

#import "FXSettingsDelegate.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
#import "PlaceholderSplitviewDetailViewController.h"
#import "MGSplitViewController.h"
#import "UIViewController+MGSplitViewController.h"
#import "settingsVC.h"
#import "BaseAccount.h"
#import "ASAccount.h"

#import "ASConfigViewController.h"


@interface FXSettingsDelegate ()
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) MGSplitViewController *mgController;
@property (nonatomic, strong) UIViewController *rootViewController;
@end

@implementation FXSettingsDelegate
@synthesize rootViewController = _rootViewController;

+ (void) initialize
{
	return;
}

+ (NSString*) applicationIcon
{
	return @"FixMailRsrc.bundle/SafeGuard_Settings_Icon";
}

+ (NSString*) applicationName
{
	return FXLLocalizedString(@"Settings", @"Settings app name displayed to the user");
}

- (BOOL) containedApplication:(id <SZLApplicationContainer>)applicationContainer didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    return YES;
}

- (void) containedApplicationWillTerminate:(id <SZLApplicationContainer>)applicationContainer
{
	self.navController = nil;
}

/*
- (void) containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer
{
	settingsVC *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPad identifier:@"settingsVC"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPhone identifier:@"settingsVC"];
	}

	UIBarButtonItem *exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
	theController.navigationItem.leftBarButtonItem = exitButton;

	[self.navController pushViewController:theController animated:false];
}

- (UIViewController *) rootViewController
{
	self.navController = [[UINavigationController alloc] init];
	self.navController.navigationBar.barStyle = UIBarStyleBlack;

	return self.navController;
}
*/

- (void) containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer
{
	[self presentConfigurationController];
}

- (UIViewController *) rootViewControllerIPhone
{
	// need to do a couple things here
	// 1: grab a listContactsVC controller object
	// 2: give it an exit button to get out the app
	// 3: embed within a nav-controller.
	// ====================================================================

	// 1
	// ====================================================================
	UIViewController *theController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPhone identifier:@"settingsVC"];

	// 2
	// ====================================================================
	UIBarButtonItem *exitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
	theController.navigationItem.leftBarButtonItem = exitButton;

	// 3 - we could extract from the storyboard an already embedded listContactsVC within a navController
	// but we don't to keep it fairly modular. Eg, the non-container app pushes the listContactsVC on an already existing
	// navController... you cannot push a navController onto a navController
	// ====================================================================
	self.navController = [[UINavigationController alloc] initWithRootViewController:theController];

	return self.navController;
}

- (UIViewController *) rootViewControllerIPad
{
	// Our iPad will have the following layout:
	//
	// MGSplitViewController (instead of UISplitViewController)
	// -> UINavigationController -> settingsVC (Master)
	// -> UINavigationController (Detail)
	//
	// have to do the following:
	// 1: allocate an MGSplitViewController
	// 2: allocate the master portion (settingsVC)
	// 3: allocate the detail portion (PlaceholderSplitviewDetailViewController)
	// 4: embed them each in navControllers and put them in the splitViewController
	// ====================================================================

	// 1
	// ====================================================================
	self.mgController = [[MGSplitViewController alloc] initWithNibName:@"MGSplitViewController" bundle:[FXLSafeZone getResourceBundle]];
	[self.mgController setup];

	// 2
	// ====================================================================
	settingsVC *settingsController = [UIStoryboard storyboardWithName:kStoryboard_Settings_iPad identifier:@"settingsVC"];
	settingsController.mgSplitViewController = self.mgController;

	// 3
	// ====================================================================
	UINavigationController *detailController = [[UINavigationController alloc] init];

	// 4
	// ====================================================================
	self.mgController.masterViewController = [[UINavigationController alloc] initWithRootViewController:settingsController];
	self.mgController.detailViewController = detailController;
	self.mgController.showsMasterInPortrait = true;

	return self.mgController;
}

- (UIViewController *) rootViewController
{
	if (!_rootViewController) {
		if (IS_IPHONE()) {
			_rootViewController = [self rootViewControllerIPhone];
		} else if (IS_IPAD()) {
			_rootViewController = [self rootViewControllerIPad];
		}
        
        // Load existing email accounts and folders
		//
		if([BaseAccount accounts].count == 0) {
			[ASAccount loadExistingEmailAccounts];
		}
	}
	return _rootViewController;
}

#pragma mark - Configuration

-(void)presentConfigurationController
{
	if (![self isConfigured]) {
		UIViewController *configurationController = [self configurationController];
		[self.rootViewController presentViewController:configurationController animated:YES completion:nil];
	}
}

-(BOOL)isConfigured
{
	if ([BaseAccount accountsConfigured] < 1) {
		return NO;
	}
	return YES;
}

-(BOOL)requiresImmediateConfiguration
{
	if (![self isConfigured] && ![[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultsConfigurePIMLaterKey]) {
		return YES;
	}
	return NO;
}

-(UIViewController *)configurationController
{
	if (self.configurationDelegate) {
		return [ASConfigViewController accountConfigurationControllerWithDelegate:self.configurationDelegate];
	}
	return [ASConfigViewController accountConfigurationControllerWithDelegate:self];
}

#pragma mark - SZLConfigurationDelegate Methods
-(void)configurationDidFail:(id <SZLApplicationContainerDelegate>)application
{
	if (self.configurationDelegate) {
		[self.configurationDelegate configurationDidFail:self];
	}  else {
		[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
			[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
		}];
	}
}

-(void)configurationWasCancelled:(id <SZLApplicationContainerDelegate>)application
{
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kUserDefaultsConfigurePIMLaterKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	if (self.configurationDelegate) {
		[self.configurationDelegate configurationWasCancelled:self];
	} else {
		[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
			[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
		}];
	}
}

-(void)configurationDidSucceed:(id <SZLApplicationContainerDelegate>)application
{
	if (self.configurationDelegate) {
		[self.configurationDelegate configurationDidSucceed:self];
	} else {
		[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
			[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
		}];
	}
}

@end
