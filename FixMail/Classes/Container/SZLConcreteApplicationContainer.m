 //
//  SZLApplicationContainer.m
//  SZContainer
//
//  Created by Anluan O'Brien on 12-03-16.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//
#import "ContainedApplicationConstants.h"
#import "SZLConcreteApplicationContainer.h"
#import "SZCAppDelegate.h"
#import "FMSplitViewDelegate.h"
#import "sqlite3.h"
//#import "SZLSecureFileManager_SZL.h"

//#import "SZCPushManager.h"

#import "ASConfigViewController.h"
#import "BaseAccount.h"

#define APPLICATION_OPEN_ANIMATION_SPEED 0.2f

//#define FORCEBACKBUTTON

static SZLConcreteApplicationContainer *_sharedInstance;
static UIBarButtonItem *_exitButton;


@interface SZLConcreteApplicationContainer () {
    NSInvocation *_exitInvocation;
}

@property (strong, nonatomic) SZCViewController *containerBoard;
@property (strong, nonatomic) UIViewController *containedApplicationRootViewController;

-(void)handleContainerNotifications:(NSNotification*)notification;
-(void)exitToContainerWithCompletion:(void (^)(void))completionBlock;
@end

@implementation SZLConcreteApplicationContainer

@synthesize delegate;
@synthesize window;
@synthesize containerBoard = _containerBoard;
@synthesize containedApplicationRootViewController = _containedApplicationRootViewController;

+(SZLConcreteApplicationContainer *)sharedApplicationContainer
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[SZLConcreteApplicationContainer alloc] init];
        _sharedInstance.window = [[[UIApplication sharedApplication] delegate] window];
    });
    return _sharedInstance;
}

-(void)viewDidLoad
{
}

-(void)viewWillAppear:(BOOL)animated
{
    if (!_sharedInstance) {
        _sharedInstance = self;
    }
#ifdef DEBUG
    self.view.backgroundColor = [UIColor yellowColor];
#endif
    [self setupContainerBoard];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sbFun:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

-(void)setupContainerBoard
{
    if (!_containerBoard) {
        self.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self setContainerBoard:[[SZCViewController alloc] initWithNibName:@"SZCViewController" bundle:nil]];
        [_containerBoard.view setFrame:[self.view bounds]];
        [_containerBoard willMoveToParentViewController:self];
        [self addChildViewController:_containerBoard];
        [self.view addSubview:_containerBoard.view];
		[_containerBoard didMoveToParentViewController:self];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleContainerNotifications:) name:SZCContainerScreenWillAppearNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleContainerNotifications:) name:SZCContainerScreenDidAppearNotification object:nil];
}

#if defined(FIXMAIL_SIDEMENU)
- (MFSideMenu *)sideMenu:(UINavigationController *)inController
{
    SideMenuViewController *sideMenuController = [[SideMenuViewController alloc] init];
    
    MFSideMenuOptions options = MFSideMenuOptionMenuButtonEnabled|MFSideMenuOptionBackButtonEnabled
                                                                 |MFSideMenuOptionShadowEnabled;
    MFSideMenuPanMode panMode = MFSideMenuPanModeNavigationBar|MFSideMenuPanModeNavigationController;
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:inController
                                                 sideMenuController:sideMenuController
                                                           location:MFSideMenuLocationLeft
                                                            options:options
                                                            panMode:panMode];
    
    sideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}
#endif

-(void)requestToSwitchToContainedApplication:(Class)containedAppClass
{
	[self requestToSwitchToContainedApplication:containedAppClass options:nil];
}

-(void)requestToSwitchToContainedApplication:(Class)containedAppClass options:(NSDictionary *)launchOptions
{   
	
    if ([containedAppClass conformsToProtocol:@protocol(SZLApplicationContainerDelegate)] && [containedAppClass instancesRespondToSelector:@selector(container)]) {
        
        __block id <NSObject, SZLApplicationContainerDelegate> containedApplication = [[containedAppClass alloc] init];
        
        self.delegate = containedApplication;
        containedApplication.container = self;
        __block UIViewController *vc = [containedApplication rootViewController];
        
        if (vc) {
            [vc willMoveToParentViewController:self.window.rootViewController];
            [self addChildViewController:vc];
            [_containerBoard willMoveToParentViewController:nil];
            [vc.view setFrame:[self.view bounds]];
            [self.view addSubview:vc.view];
            
            [UIView transitionWithView:self.view
                              duration:APPLICATION_OPEN_ANIMATION_SPEED
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                // we were adding the subview here but it was getting chunky for some reason
                                // removed for the timebeing. -Anluan O'Brien / Oct 22 2012
                                
                                
                            }
                            completion:^(BOOL finished) {
                                [_containerBoard.view removeFromSuperview];
                                [_containerBoard removeFromParentViewController];
                                [containedApplication containedApplication:self didFinishLaunchingWithOptions:launchOptions];
                                [containedApplication containedApplicationDidBecomeActive:self];
                                
								
#ifdef FORCEBACKBUTTON
                                if ([vc isKindOfClass:[UINavigationController class]]) {
                                    UINavigationController *nc = (UINavigationController*)vc;
                                    NSMutableArray *items = [nc.topViewController.navigationItem.leftBarButtonItems mutableCopy];
                                    if (!items) {
                                        items = [NSMutableArray arrayWithCapacity:1];
                                    }
                                    [items addObject:[self exitContainerBarButtonItem]];
                                    nc.topViewController.navigationItem.leftBarButtonItems = items;
                                }
#endif
                            }];
        }
        self.containedApplicationRootViewController = vc;
    } else {
        NSLog(@"The contained app class (%@) must conform to the SZLApplicationContainerDelegate protocol.",containedAppClass);
    }
}

-(void)launchBrowserWithURL:(NSURL *)url closeCompletionHandler:(void (^)(void))completion
{
	[self exitToContainerWithCompletion:^(void) {
		NSDictionary *launchOptions = [NSDictionary dictionaryWithObjectsAndKeys:url, kBrowserLaunchOptionsLaunchURLKey, completion, kBrowserLaunchOptionsCompletionHandlerKey, nil];
		[self requestToSwitchToContainedApplication:NSClassFromString(@"SecureBrowser") options:launchOptions];
	}];
}

-(UIBarButtonItem*)exitContainerBarButtonItem
{
    return [self exitContainerBarButtonItemWithTarget:self andAction:@selector(exitToContainer)];
}

-(UIBarButtonItem*)exitContainerBarButtonItemWithTarget:(id)target andAction:(SEL)action
{
    if (!_exitButton) {
        _exitButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedString(@"Exit", @"Exit button to return to Container springboard") style:UIBarButtonItemStyleDone target:self action:@selector(exitToContainer)];
    }
    
    if ((target != self) && (action != @selector(exitToContainer))) {
        NSMethodSignature *exitSignature = [[target class] instanceMethodSignatureForSelector:action];
        _exitInvocation = [NSInvocation invocationWithMethodSignature:exitSignature];
        [_exitInvocation setTarget:target];
        [_exitInvocation setSelector:action];
    } else {
        _exitInvocation = nil;
    }
    
    return _exitButton;
}

-(void)exitToContainer
{
    [self exitToContainerWithCompletion:nil];
    _exitInvocation = nil;
}

-(void)exitToContainerWithCompletion:(void (^)(void))completionBlock
{
    if (_exitInvocation) {
        [_exitInvocation invoke];
    }
    
//    [SZLSecureFileManager cleanup];
    __block SZLConcreteApplicationContainer *bself = self;
    
    // Commented out these two line 2/26/13 - Ed Millard
    //
    // These cause the container to be reloaded every time you return to the container
    // from email, calendar etc.  Its not clear why.  We want to keep the container
    // in place and more importantly we want the ASAccount object in the container
    // to remain in tact instead of constantly deleted and reloading it in
    // SZCViewController.  If these lines need to be here we need to explore why and
    // come up with a way to keep the ASAccount objects in tact.
    //
    // self.containerBoard = nil;
    // [self setupContainerBoard];
//    [self.containedApplicationRootViewController willMoveToParentViewController:nil];
//	[_containerBoard willMoveToParentViewController:self];

	[self addChildViewController:_containerBoard];
	[self.containedApplicationRootViewController willMoveToParentViewController:nil];

	// PROBLEM:
	// since we remove the _containerBoard from the view hierarchy, it doesn't know about any
	// rotations. So we have to resize when we add it back in
	CGRect newFrameRect = self.containedApplicationRootViewController.view.bounds;
	newFrameRect.origin = CGPointZero;
	_containerBoard.view.frame = newFrameRect;

    [UIView transitionWithView:self.view
                      duration:APPLICATION_OPEN_ANIMATION_SPEED
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
						[self.view addSubview:_containerBoard.view];
						[bself.containedApplicationRootViewController.view removeFromSuperview];
                    }
                    completion:^(BOOL finished) {
						[_containerBoard didMoveToParentViewController:self];
						[bself.containedApplicationRootViewController removeFromParentViewController];
						[bself.containedApplicationRootViewController didMoveToParentViewController:nil];


//                        [bself.containedApplicationRootViewController removeFromParentViewController];
//
//                        [self addChildViewController:_containerBoard];
//						[self.view addSubview:_containerBoard.view];
//
//						[bself.containedApplicationRootViewController didMoveToParentViewController:nil];

                        if (completionBlock) {
                            completionBlock();
                        }
                        
                        if (bself.delegate && [bself.delegate respondsToSelector:@selector(containedApplicationWillTerminate:)]) {
                            [bself.delegate containedApplicationWillTerminate:self];
                        }
                        
//                        // FIXME BIG HACK
//                        // BAD
//                        // Should check when PIM client settings close, in PIM client app
//                        Class pushClass = NSClassFromString(@"iKonic_Settings");
//                        if ([bself.delegate isMemberOfClass:pushClass]) {
//                            SZCPushManager *pushManager = [SZCPushManager PushManager];
//                            if (pushManager.tryToRegisterForPush && [pushManager findEmailAddress]) {
//                                [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//                            }
//                        }
//                        // BAD
//                        // END OF BIG HACK TO BE FIXED
                        
                        bself.containedApplicationRootViewController = nil;
                        bself.delegate.container = nil;
                        bself.delegate = nil;
                        
                    }];
}

- (void)willResignActive
{
    if (delegate && [delegate respondsToSelector:@selector(containedApplicationWillResignActive:)]) {
        [delegate containedApplicationWillResignActive:self];
    }
}

- (void)didEnterBackground
{
//    [SZLSecureFileManager cleanup];
    if (delegate && [delegate respondsToSelector:@selector(containedApplicationDidEnterBackground:)]) {
        [delegate containedApplicationDidEnterBackground:self];
    }
}

- (void)willEnterForeground
{
//    [SZLSecureFileManager cleanup];
    if (delegate && [delegate respondsToSelector:@selector(containedApplicationWillEnterForeground:)]) {
        [delegate containedApplicationWillEnterForeground:self];
    }
}

- (void)didBecomeActive
{
    if (delegate && [delegate respondsToSelector:@selector(containedApplicationDidBecomeActive:)]) {
        [delegate containedApplicationDidBecomeActive:self];
    }
}

-(void)handleContainerNotifications:(NSNotification*)notification
{
    /*
     if ([notification.name isEqualToString:SZCContainerScreenDidAppearNotification]) {
     if (delegate && [delegate respondsToSelector:@selector(containedApplicationWillTerminate:)]) {
     [delegate containedApplicationWillTerminate:self];
     }
     id <SZLApplicationContainerDelegate> containedApplication = self.delegate;
     containedApplication.container = nil;
     self.delegate = nil;
     containedApplication = nil;
     }
     */
}

@end
