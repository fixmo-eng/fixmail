#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "sqlite3.h"

/*
 @protocol SZLApplicationContainer
 @discussion Something
 */

@protocol SZLApplicationContainer;

/*
 @class SZLApplicationContainer
 @discussion See below
 */
@class SZLApplicationContainer;

/*
 @protocol SZLConfigurationDelegate
 @discussion See below
 */

@protocol SZLConfigurationDelegate;




/*!
 @protocol SZLApplicationContainerDelegate
 @discussion A protocol that defines the delegate for SZLApplicationContainer.
 The functions should be treated the same as they are with UIApplicationDelegate.
 The only difference being that SZLApplicationContainer is passwed which can talk to the container
 and will verify a contained app can call any messages that would normally be handled by UIApplication.
 */

@protocol SZLApplicationContainerDelegate


/*!
 @property container
 A property that the SafeZone Container will assign itself to and can be used as a reference by the contained application.
 This allows the class that implements the SZLApplicationContainerDelegate protocol
 to provide itself to the rest of contained application and gives the contained application access to the Container.
 */
@property (nonatomic, assign) id <SZLApplicationContainer> container;

/*!
 @function applicationIcon
 @abstract Provides the name of the contained application's icon.
 @result
 The NSString of the applicaiton icon name.
 */
+(NSString*)applicationIcon;

/*!
 @function applicationName
 @abstract Provides the name of the contained application.
 @result
 The NSString of the applicaiton name.
 */
+(NSString*)applicationName;

/*!
 @function rootViewController
 @abstract Provides access to the root view controller of the contained app.
 @result
 The root UIViewController of the contained app.
 */
-(UIViewController*)rootViewController;

/*!
 @function containedApplication:didFinishLaunchingWithOptions:
 @abstract Called whenever the contained app is launched from the Container's springboard.
 @param applicationContainer
 The delegating container application object.
 @param launchOptions
 A dictionary indicating the reason the application was launched (if any).
 @result
 NO if the application cannot handle the URL resource, otherwise return YES
 */
-(BOOL)containedApplication:(id <SZLApplicationContainer>)applicationContainer didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

/*!
 @function containedApplicationWillTerminate:
 @abstract Called when the contained app is exited and the Container becomes frontmost.
 @param applicationContainer
 The delegating container application object.
 */
-(void)containedApplicationWillTerminate:(id <SZLApplicationContainer>)applicationContainer;

@optional
/*  @discussion
 *  The following methods are used to support proper suspension of contained apps when a contained app is
 *  active in the container and the container itself is suspended by iOS.
 *
 *  In the future certain contained apps may be able to run in the "background" of the container, for certain functionality, at which point
 *  these messages would become required as opposed to optional.
 */

/*!
 @function containedApplicationDidBecomeActive:applicationContainer:
 @abstract Called when the SafeZone application goes from the inactive to active state.
 @param applicationContainer
 The delegating container application object.
 */
-(void)containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer;

/*!
 @function containedApplicationWillResignActive:applicationContainer:
 @abstract Called when the SafeZone application about to move from active to inactive.
 @param applicationContainer
 The delegating container application object.
 */
-(void)containedApplicationWillResignActive:(id <SZLApplicationContainer>)applicationContainer;

-(void)containedApplicationDidEnterBackground:(id <SZLApplicationContainer>)applicationContainer;

-(void)containedApplicationWillEnterForeground:(id <SZLApplicationContainer>)applicationContainer;


/*  @discussion
 *  The following methods are optional and used to support interaction between the container and a contained application
 * or between different contained applications. They are used by the container to decide whether to launch configuration
 * screens and can be used to see whether an application has been configured before launching it.
 */
/*!
 @property configurationDelegate
 A property that the SafeZone Container will assign itself to and can be used as a reference by the contained application.
 This allows the class implementing the SZLConfiguratioDelegate protocol to be given information about the notification
 and gives the contained application access to that class.
 */
@property (nonatomic, assign) id <SZLConfigurationDelegate> configurationDelegate;

/*!
 @function isConfigured
 @abstract Called by any contained application wanting access to another contained application.
 @result
 YES if application has been configured, NO otherwise.
 @discussion This must be implemented along with -(UIViewController *)configurationController and @property configurationDelegate.
 */
-(BOOL)isConfigured;

/*!
 @function requiresImmediateConfiguration
 @abstract Called upon launch of the Container, when the springboard is displayed.
 @result
 YES if application needs to be configured upon launch of the cCntainer, NO otherwise.
 @discussion This must be implemented along with -(UIViewController *)configurationController and @property configurationDelegate.
 */
-(BOOL)requiresImmediateConfiguration;

/*!
 @function configurationController
 @abstract Called whenever the container is launched, if requiresImmediateConfiguration returns YES. May also be called
 by any other contained application wanting access to that contained app, when -isConfigured returns NO.
 @result
 A configuration view controller for the contained application, to be presented by the Container.
 @discussion This must be implemented along with @property configurationDelegate and at least one of
 -(BOOL)requiresImmediateConfiguration and -(BOOL)isConfigured.
 */
-(UIViewController *)configurationController;

@end



/*!
 @protocol SZLConfigurationDelegate
 @discussion A protocol that defines any object acting as delegate for the SZLApplicationContainerDelegate's
 configuration methods. Any contained application can implement this when accessing other contained applications.
 */
@protocol SZLConfigurationDelegate

/*!
 @function configurationDidFail
 @abstract Called when configuration of the contained application fails and the configurationController should be dismissed.
 */
-(void)configurationDidFail:(id <SZLApplicationContainerDelegate>)application;

/*!
 @function configurationWasCancelled
 @abstract Called when configuration of the contained application is cancelled by the user and the configurationController
 should be dismissed.
 */
-(void)configurationWasCancelled:(id <SZLApplicationContainerDelegate>)application;

/*!
 @function configurationDidSucceed
 @abstract Called when the contained application is successfully configured and the configurationController should be dismissed.
 */
-(void)configurationDidSucceed:(id <SZLApplicationContainerDelegate>)application;
@end



/*!
 @protocol SZLApplicationContainer
 @discussion A protocol that defines the main application container.
 */
@protocol SZLApplicationContainer

/*!
 @property delegate
 @discussion A property representing the Container delegate.
 */
@property (nonatomic, retain) id <SZLApplicationContainerDelegate> delegate;

/*!
 @property window
 @discussion
 A property representing the main UI window.
 */
@property (nonatomic, retain) UIWindow *window;

/*
 *  Contained app developers are free to use the below methods to get an exit button from the container that will take care
 *  of exiting the app for them. If you choose not to use these buttons you must provide your own button and the message that handles
 *  the button must call -(void)exitToContainer on the container that is provided to the SZLApplicationContainerDelegate.
 *  If you choose to pass in your own target and action to the button method the passed in action should NOT call exitToContainer, exitToContainer will be called
 *  after the specified action has completed.
 */

/*!
 @function exitContainerBarButtonItem
 @abstract Provides an exit button for the contained app.
 @result
 The UIBarButtonItem for exiting back to the Container.
 */
-(UIBarButtonItem*)exitContainerBarButtonItem;

/*!
 @function exitContainerBarButtonItemWithTarget:target:andAction:
 @abstract Provides an exit button for the contained app and allows the contained app to provide the target and action.
 @param target
 @param action
 @result
 The UIBarButtonItem for exiting back to the Container.
 */
-(UIBarButtonItem*)exitContainerBarButtonItemWithTarget:(id)target andAction:(SEL)action;

/*!
 @function exitToContainer
 @abstract Called by the contained application when not using the provided button to exit back to the Container's springboard.
 */
-(void)exitToContainer;

@end