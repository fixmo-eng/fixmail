//
//  SZCContainerBoardAppIcon.h
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-26.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZCContainerBoardAppIcon : UIControl

@property (strong, nonatomic) UIImage *iconImage;
@property (strong, nonatomic) UIImageView *iconImageView;
@property (strong, nonatomic) UILabel *appNameLabel;

@end
