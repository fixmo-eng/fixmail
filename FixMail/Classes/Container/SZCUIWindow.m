//
//  SZCUIWindow.m
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-04-02.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCUIWindow.h"

@implementation SZCUIWindow

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setRootViewController:(UIViewController*)rootViewController
{
    [super setRootViewController:rootViewController];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
