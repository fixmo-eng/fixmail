//
//  SZCContainerBoardAppIcon.m
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-26.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCContainerBoardAppIcon.h"
#import <QuartzCore/QuartzCore.h>

@implementation SZCContainerBoardAppIcon

@synthesize iconImage = _iconImage;
@synthesize iconImageView;
@synthesize appNameLabel;

- (id)initWithFrame:(CGRect)frame
{
    
    CGRect iconFrame;
    CGRect iconImageFrame;
    CGRect appLabelFrame;
    
    if (IS_IPAD()) {
        iconFrame = (CGRect){frame.origin,(CGSize){100,100}};
        iconImageFrame = CGRectMake(14, 8, 72, 72);
        appLabelFrame = CGRectMake(4, 82, 92, 16);
    } else {
        iconFrame = (CGRect){frame.origin,(CGSize){80,80}};
        iconImageFrame = CGRectMake(11, 8, 57, 57);
        appLabelFrame = CGRectMake(4, 67, 76, 13);
    }
    
    //FIXME: set so that label size shrinks and DOESN'T get cut off 
    
    //CGRect iconFrame = (CGRect){frame.origin,(CGSize){100,100}};
    self = [super initWithFrame:iconFrame];
    if (self) {
        iconImageView = [[UIImageView alloc] initWithFrame:iconImageFrame];
//		iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
		iconImageView.contentMode = UIViewContentModeScaleAspectFit;
		iconImageView.clipsToBounds = true;
	
        iconImageView.frame = iconImageFrame;
//        iconImageView.layer.shadowOpacity = .3f;
//        iconImageView.layer.shadowOffset = CGSizeMake(0,0);
//        iconImageView.layer.shadowRadius = 4.0f;
		iconImageView.layer.cornerRadius = 10.0;
		iconImageView.layer.borderColor = [[UIColor colorWithRed:(70.0/255.0) green:(72.0/255.0) blue:(84.0/255.0) alpha:1.0] CGColor];
		iconImageView.layer.borderWidth = 4.0;

        appNameLabel = [[UILabel alloc] initWithFrame:appLabelFrame];
        appNameLabel.backgroundColor = [UIColor clearColor];
        appNameLabel.textAlignment = NSTextAlignmentCenter;
        appNameLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:13];
//        appNameLabel.textColor = [UIColor blackColor];
		
        appNameLabel.textColor = [UIColor whiteColor];
        appNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        appNameLabel.shadowOffset = CGSizeMake(2,2);

        [self addSubview:iconImageView];
        [self addSubview:appNameLabel];
    }
    return self;
}

-(void)setIconImage:(UIImage *)iconImage
{
    if (iconImage) {
        _iconImage = iconImage;
    } else {
        _iconImage = [UIImage imageNamed:@"FixMailRsrc.bundle/ContainerBoardDefaultIcon"];
    }

	iconImageView.image = _iconImage;
//    [iconImageView setImage:_iconImage forState:UIControlStateNormal];
    
    /*
    UIGraphicsBeginImageContextWithOptions(_iconImage.size, NO, _iconImage.scale);
    [[UIBezierPath bezierPathWithRoundedRect:iconImageView.bounds 
                                cornerRadius:(_iconImage.size.width/_iconImage.scale)/6.4] addClip];
    [_iconImage drawInRect:iconImageView.bounds];
    [iconImageView setImage:UIGraphicsGetImageFromCurrentImageContext() forState:UIControlStateNormal];
    UIGraphicsEndImageContext();
    */
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	self.iconImageView.layer.borderColor = [[UIColor orangeColor] CGColor];
	self.iconImageView.alpha = 0.5;
	self.appNameLabel.textColor = [UIColor orangeColor];
	return TRUE;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
	self.iconImageView.layer.borderColor = [[UIColor colorWithRed:(70.0/255.0) green:(72.0/255.0) blue:(84.0/255.0) alpha:1.0] CGColor];
	self.iconImageView.alpha = 1.0;
	
	self.appNameLabel.textColor = [UIColor whiteColor];
}

- (void)cancelTrackingWithEvent:(UIEvent *)event
{
	self.iconImageView.layer.borderColor = [[UIColor colorWithRed:(70.0/255.0) green:(72.0/255.0) blue:(84.0/255.0) alpha:1.0] CGColor];
	self.iconImageView.alpha = 1.0;
	
	self.appNameLabel.textColor = [UIColor whiteColor];
}

@end
