//
//  SZCViewController.h
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const SZCContainerScreenWillAppearNotification;
static NSString *const SZCContainerScreenDidAppearNotification;

@interface SZCViewController : UIViewController 


@property (weak, nonatomic) IBOutlet UIView *containerBoard;
@property (weak, nonatomic) IBOutlet UIImageView *szBanner;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
-(void)launchApp:(UIGestureRecognizer*)gestureRecognizer;

-(void)launchAppByName:(NSString *)appToLaunch;

@end
