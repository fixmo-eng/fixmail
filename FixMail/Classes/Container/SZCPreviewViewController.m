//
//  SZCPreviewViewController.m
//  SafeZone
//
//  Created by Leena Mansour on 12-10-02.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCPreviewViewController.h"
#import "SZLSecureFileManager.h"

static NSString *const kPdfMime = @"application/pdf";
static NSString *const kJpegMime = @"image/jpeg";
static NSString *const kPngMime = @"image/png";
static NSString *const kGifMime = @"image/gif";
static NSString *const kTiffMime = @"image/tiff";
static NSString *const kDocMime = @"application/msword";
static NSString *const kDocxMime = @"application/vnd.openxmlformats-officedocument.wordprocessingml.document";
static NSString *const kXlsMime = @"application/msexcel";
static NSString *const kXlsxMime = @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
static NSString *const kPptMime = @"application/mspowerpoint";
static NSString *const kPptxMime = @"application/vnd.openxmlformats-officedocument.presentationml.presentation";
static NSString *const kRtfMime = @"text/richtext";
static NSString *const kTextMime = @"text/plain";

@interface SZCPreviewViewController ()
{
    UIWebView* webView;
}

@end

@implementation SZCPreviewViewController
@synthesize path;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.delegate = self;
    [webView setAutoresizingMask:(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth)];
    webView.scalesPageToFit = YES;
    [webView setOpaque:NO];
    [webView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:webView];
    
    [self previewFile];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setPath:(NSString *)filePath
{
    path = filePath;
    [self previewFile];
}

- (BOOL)previewFile
{
    if (path) {
        NSData* fileData = [[SZLSecureFileManager defaultManager] contentsAtPath:path];
        if (fileData) {
            NSString* mimeType = [self getMIMEType:path];
            
            if (mimeType) {
                [webView loadData:fileData MIMEType:mimeType textEncodingName:@"utf-8" baseURL:[self getBaseURL:mimeType]];
                return YES;
                //[webView loadData:fileData MIMEType:mimeType textEncodingName:@"utf-8" basexURL:[NSURL URLWithString:[NSString stringWithFormat:@"file:/%@//", path]]];
            }
        }
    }
    return NO;
}

- (NSURL *)getBaseURL:(NSString *)mimeType
{
    if (mimeType == kDocMime || mimeType == kDocxMime || mimeType == kXlsMime || mimeType == kXlsxMime || mimeType == kPptMime || mimeType == kPptxMime)
    {
        return [NSURL URLWithString:@"/"];
    } else {
       return nil;
    }
}

- (NSString *)getMIMEType:(NSString *)filePath
{
    NSString* fileExtenstion = [[filePath pathExtension] lowercaseString];
    
    if ([fileExtenstion isEqualToString:@"pdf"]) {
        return kPdfMime;
    } else if ([fileExtenstion isEqualToString:@"jpeg"] || [fileExtenstion isEqualToString:@"jpg"]) {
        return kJpegMime;
    } else if ([fileExtenstion isEqualToString:@"png"]) {
        return kPngMime;
    } else if ([fileExtenstion isEqualToString:@"gif"]) {
        return kGifMime;
    } else if ([fileExtenstion isEqualToString:@"tiff"] || [fileExtenstion isEqualToString:@"tif"]) {
        return kTiffMime;
    } else if ([fileExtenstion isEqualToString:@"doc"]) {
        return kDocMime;
    } else if ([fileExtenstion isEqualToString:@"docx"]) {
        return kDocxMime;
    } else if ([fileExtenstion isEqualToString:@"xls"]) {
        return kXlsMime;
    } else if ([fileExtenstion isEqualToString:@"xlsx"]) {
        return kXlsxMime;
    } else if ([fileExtenstion isEqualToString:@"ppt"]) {
        return kPptMime;
    } else if ([fileExtenstion isEqualToString:@"pptx"]) {
        return kPptxMime;
    } else if ([fileExtenstion isEqualToString:@"rtf"]) {
        return kRtfMime;
    } else if ([fileExtenstion isEqualToString:@"txt"] || [fileExtenstion isEqualToString:@"text"] | [fileExtenstion isEqualToString:@"log"]) {
        return kTextMime;
    }
    
    return nil;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeOther) {
        return YES;
    } else {
        return NO;
    }
}

@end
