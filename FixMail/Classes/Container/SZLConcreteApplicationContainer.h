//
//  SZLConcreteApplicationContainer.h
//  SZContainer
//
//  Created by Anluan O'Brien on 12-03-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZLApplicationContainer.h"
#import "SZCViewController.h"

#define SZCContainerScreenWillAppearNotification @"SZCContainerScreenWillAppearNotification"
#define SZCContainerScreenDidAppearNotification @"SZCContainerScreenDidAppearNotification"


@interface SZLConcreteApplicationContainer : UIViewController <SZLApplicationContainer>
{
}
@property (nonatomic, retain) id <NSObject,SZLApplicationContainerDelegate> delegate;
@property (nonatomic, retain) UIWindow *window;

+(SZLConcreteApplicationContainer *)sharedApplicationContainer;
-(void)requestToSwitchToContainedApplication:(Class)containedAppClass;
-(void)requestToSwitchToContainedApplication:(Class)containedAppClass options:(NSDictionary *)launchOptions;

-(void)launchBrowserWithURL:(NSURL *)url closeCompletionHandler:(void (^)(void))completion;

-(void)exitToContainer;
-(void)exitToContainerWithCompletion:(void (^)(void))completionBlock;

- (void)willResignActive;
- (void)didEnterBackground;
- (void)willEnterForeground;
- (void)didBecomeActive;

@end