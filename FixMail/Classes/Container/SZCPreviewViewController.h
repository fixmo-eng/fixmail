//
//  SZCPreviewViewController.h
//  SafeZone
//
//  Created by Leena Mansour on 12-10-02.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZCPreviewViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) NSString* path;

@end
