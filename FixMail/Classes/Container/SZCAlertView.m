//
//  SZCAlertView.m
//  SafeZone
//
//  Created by Anluan O'Brien on 12-04-27.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCAlertView.h"
#import <QuartzCore/QuartzCore.h>

#define SZCALERTMINHEIGHT 50.0f

@implementation SZCAlertView

@synthesize spinner;
@synthesize message;
@synthesize title;

- (id)initWithFrame:(CGRect)frame
{
    if (frame.size.height < SZCALERTMINHEIGHT) {
        frame.size.height = SZCALERTMINHEIGHT;
    }
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        [super setBackgroundColor:[UIColor clearColor]];
        self.layer.shadowOffset = CGSizeMake(0,0);
        self.layer.shadowRadius = 15.0f;
        self.layer.shadowOpacity = .6f;
        
        self.title = [[UILabel alloc] initWithFrame:CGRectZero];
        self.title.textAlignment = NSTextAlignmentCenter;
        self.title.textColor = [UIColor whiteColor];
        self.title.backgroundColor = [UIColor clearColor];
        self.title.font = [UIFont boldSystemFontOfSize:14];
        self.title.lineBreakMode = NSLineBreakByWordWrapping;
        self.title.numberOfLines = 2;
        
        self.message = [[UILabel alloc] initWithFrame:CGRectZero];
        self.message.textAlignment = NSTextAlignmentCenter;
        self.message.textColor = [UIColor whiteColor];
        self.message.backgroundColor = [UIColor clearColor];
        self.message.font = [UIFont systemFontOfSize:14];
        self.message.lineBreakMode = NSLineBreakByWordWrapping;
        self.message.numberOfLines = 0;
        self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [self addSubview:self.title];
        [self addSubview:self.message];
        [self addSubview:self.spinner];
    }
    return self;
}

-(void)layoutSubviews
{
    self.title.frame = CGRectMake (10,5,self.bounds.size.width - 20, 36);
    CGPoint realCenter = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    CGFloat spinnerSize = roundf(self.frame.size.width * .15f);
    CGRect spinnerFrame = CGRectMake(realCenter.x - spinnerSize/2,10 + self.title.frame.size.height,spinnerSize,spinnerSize);
    self.spinner.frame = spinnerFrame;
    self.message.frame = CGRectMake (10,self.bounds.size.height * .45f,self.bounds.size.width - 20, self.bounds.size.height * .5f);
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    _bgColor = backgroundColor;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:10];
    [[_bgColor colorWithAlphaComponent:.4f] setFill];
    [path fill];
    [[UIColor whiteColor] setStroke];
    [path setLineWidth:2.0f];
    [path stroke];
}


@end
