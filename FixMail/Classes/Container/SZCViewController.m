//
//  SZCViewController.m
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCAppDelegate.h"
#import "SZCViewController.h"
#import "AppSettings.h"
//#import "FTAContainerApplication.h"
#import "SZLConcreteApplicationContainer.h"
//#import "SZLSecurity.h"
#import "SZCContainerBoardAppIcon.h"
//#import "SZCSafeGuardServerConnector.h"
//#import "SZLCertificate.h"
//#import "SZLUtility.h"
#import "ASAccount.h"
#import "FixMailAppDelegate.h"
#import "FXSettingsDelegate.h"
#import "FXContactsDelegate.h"

static NSString *availableAppString = @"FXSettingsDelegate FixMailAppDelegate FXCalendarDelegate FXContactsDelegate";

//@interface SZLSecureDatabase (Container)
//+(void)createEncryptedDatabases;
//+(void)createDatabaseAtPath:(NSString*)databasePath withSchemaNamed:(NSString*)schemaName;
//+(void)createDatabaseAtPath:(NSString*)databasePath withSchemaAtPath:(NSString*)schemaPath;
//+(void)closeAllConnections;
//@end

@interface SZCViewController ()
-(void)setupContainerBoardWithOrientation:(UIInterfaceOrientation)orientation;
@end

@implementation SZCViewController
@synthesize containerBoard, versionLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];

	if (IS_IPAD()) {
		self.szBanner.layer.cornerRadius = 20.0;
	} else {
		self.szBanner.layer.cornerRadius = 10.0;
	}
    
    self.versionLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.textColor = [UIColor whiteColor];
    self.versionLabel.backgroundColor = [UIColor clearColor];
    self.versionLabel.textAlignment = NSTextAlignmentCenter;
    self.versionLabel.font = [UIFont systemFontOfSize:10];
    self.versionLabel.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapVersion:)];
    tr.numberOfTapsRequired = 1;
    [self.versionLabel addGestureRecognizer:tr];
    
    [self.view addSubview:self.versionLabel];
    [self.view bringSubviewToFront:self.versionLabel];

	return;
}

- (void)viewDidUnload
{
    [self setContainerBoard:nil];
    [self setSzBanner:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (IS_IPAD()) {
        [UIView animateWithDuration:duration animations:^{            
            if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
                self.szBanner.center = CGPointMake(1024/2, self.szBanner.center.y);

            } else {
                self.szBanner.center = CGPointMake(768/2, self.szBanner.center.y);
            }
        }];
        [self setupContainerBoardWithOrientation:toInterfaceOrientation];
    }
}

- (void)defaultsChangedShouldResync:(BOOL)aShouldResync
{
	NSArray* anAccounts = [BaseAccount accounts];
	for(BaseAccount* aBaseAccount in anAccounts) {
		if([aBaseAccount isKindOfClass:[ASAccount class]]) {
			ASAccount* anAccount = (ASAccount*)aBaseAccount;
			[anAccount setActiveSyncPreferences:aShouldResync];
		}
	}
}

-(void)viewWillAppear:(BOOL)animated
{
//#ifdef DEBUG
//    self.view.backgroundColor = [UIColor greenColor];
//#endif
    
//    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    
//    [SZLSecureDatabase createEncryptedDatabases];
    [[NSNotificationCenter defaultCenter] postNotificationName:SZCContainerScreenWillAppearNotification object:self];
    [self willRotateToInterfaceOrientation:self.interfaceOrientation duration:.3];
    
//    [self willRotateToInterfaceOrientation:[[UIDevice currentDevice] orientation] duration:.3];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setupContainerBoardWithOrientation:self.interfaceOrientation];
//    [[SZCSafeGuardServerConnector sharedInstance] listenForCommand];

    if (([BaseAccount accountsConfigured] < 1) && (![[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultsConfigurePIMLaterKey])) {
		[self launchAppByName:@"FXSettingsDelegate"];
	} else {
		[self defaultsChangedShouldResync:TRUE];
        if([BaseAccount accounts].count == 0) {
            [ASAccount loadExistingEmailAccounts];
        }
	}

	return;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SZCContainerScreenDidAppearNotification object:self];
}

-(void)statusBarChanged:(NSNotification*)notification
{
    NSLog(@"SB: %@",[notification userInfo]);
}

-(void)setupContainerBoardWithOrientation:(UIInterfaceOrientation)orientation
{
    NSArray *availableApps = [availableAppString componentsSeparatedByString:@" "];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGFloat width = 0;
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        width = screenRect.size.width;
    }
    else
    {
        width = screenRect.size.height;
    }

    CGFloat totalIconLength = ICONS_PER_ROW() * 100;
    CGFloat totalIconSpacing = (ICONS_PER_ROW() -1) * ICON_SPACING();
    CGFloat buffer = roundf((width - totalIconLength - totalIconSpacing)/2);
    
    [[containerBoard subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if ([[containerBoard subviews] count] == 0) {
        for (NSString *app in availableApps) {
            Class appClass = [NSClassFromString(app) class];
            if ([appClass conformsToProtocol:@protocol(SZLApplicationContainerDelegate)]) {
                SZCContainerBoardAppIcon *appIcon = [[SZCContainerBoardAppIcon alloc] initWithFrame:CGRectMake((([availableApps indexOfObject:app] % ICONS_PER_ROW()) * 100)+ICON_SPACING()+buffer, (roundf([availableApps indexOfObject:app]/ICONS_PER_ROW()) * 100)+ICON_SPACING(), 0, 0)];
                @try {
                    appIcon.appNameLabel.text = [appClass performSelector:@selector(applicationName)];
                    appIcon.appNameLabel.textColor = [UIColor whiteColor];
                    appIcon.iconImage = [UIImage imageNamed:[appClass performSelector:@selector(applicationIcon)]];
                }
                @catch (NSException *exception) {
                    appIcon.appNameLabel.text = app;
                    appIcon.iconImage = nil;
                }
                @finally {
                    appIcon.tag = [availableApps indexOfObject:app];
                    [appIcon addTarget:self action:@selector(launchApp:) forControlEvents:UIControlEventTouchUpInside];
                    [containerBoard addSubview:appIcon];
                }
            }
        }
    }    
}

-(void)launchApp:(id)sender //(UIGestureRecognizer*)gestureRecognizer
{
    SZLConcreteApplicationContainer *appContainer = [SZLConcreteApplicationContainer sharedApplicationContainer];
    NSArray *availableApps = [availableAppString componentsSeparatedByString:@" "];
    
    [appContainer requestToSwitchToContainedApplication:NSClassFromString([availableApps objectAtIndex:[sender tag]])];
}

-(void)launchAppByName:(NSString *)appToLaunch
{
    SZLConcreteApplicationContainer *appContainer = [SZLConcreteApplicationContainer sharedApplicationContainer];
    
    [appContainer requestToSwitchToContainedApplication:NSClassFromString(appToLaunch)];
}

-(void)tapVersion:(UIGestureRecognizer*)gr
{
    NSString *buildDescription =  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"BuildDescription"];
    if ([self.versionLabel.text isEqualToString:buildDescription]) {
        self.versionLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    }
    else {
        self.versionLabel.text = buildDescription;
    }
}


@end
