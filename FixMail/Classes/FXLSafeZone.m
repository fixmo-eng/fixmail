//
//  FXLSafeZone.m
//  FixMail
//
//  Created by Sean Langley on 2013-04-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXLSafeZone.h"
//#import "SZLSecureFileManager.h"
//#import "SZLSecureDatabase.h"
//#import "SZLSecureSettings.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

@implementation FXLSafeZone




+ (BOOL)isInSafeZone
{
    return NSClassFromString(@"SZLSecureDatabase") != nil;
}

+ (NSBundle*) getResourceBundle
{
    NSBundle *bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"FixMailRsrc" ofType:@"bundle"]];
    return bundle;
}

+ (NSFileManager*) defaultSafeZoneFileManager
{
    if ([self isInSafeZone])
    {
        Class _SZLSecureFileManagerClass = NSClassFromString(@"SZLSecureFileManager");
        NSFileManager* fm = [_SZLSecureFileManagerClass defaultManager];
        return fm;
    }
    else
    {
        return [NSFileManager defaultManager];
    }
}

+ (NSFileManager*) secureFileManager
{
    if ([self isInSafeZone])
    {
        Class _SZLSecureFileManagerClass = NSClassFromString(@"SZLSecureFileManager");
        NSFileManager* fm = [[_SZLSecureFileManagerClass alloc] init ];
        return fm;
    }
    else
    {
        return [[NSFileManager alloc]init];
    }
}

+ (FMDatabase*) secureDatabaseWithPath:(NSString*)aPath
{
    if ([self isInSafeZone])
    {
        Class _SZLSecureDatabaseClass = NSClassFromString(@"SZLSecureDatabase");
        return [_SZLSecureDatabaseClass databaseWithPath:aPath];
    }
    else
    {
        return [FMDatabase databaseWithPath:aPath];
    }
}


+ (id)secureDatabaseQueueWithPath:(NSString*)aPath
{
    if ([self isInSafeZone])
    {
        Class _SecureFMDatabaseQueue = NSClassFromString(@"SZLSecureDatabaseQueue");
        return [_SecureFMDatabaseQueue databaseQueueWithPath:aPath];
    }
    else
    {
        return [FMDatabaseQueue databaseQueueWithPath:aPath];
    }
}

+ (void) secureResetStandardUserDefaults
{
    if ([self isInSafeZone])
    {
        Class _ClassSecureSharedSettings = NSClassFromString(@"SZLSecureSettings");
        return [_ClassSecureSharedSettings resetStandardUserDefaults] ;
    }
    else
    {
        return [NSUserDefaults resetStandardUserDefaults];
    }
    
}
+ (id) secureStandardUserDefaults
{
    if ([self isInSafeZone])
    {
        Class _ClassSecureSharedSettings = NSClassFromString(@"SZLSecureSettings");
        return [_ClassSecureSharedSettings performSelector:@selector(sharedSettings)];
    }
    else
    {
        return [NSUserDefaults standardUserDefaults];
    }

}

+ (NSString *) documentsDirectory
{
	NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	return documentsDirectory;
}

+ (NSString *) settingsDirectory
{
	return [FXLSafeZone documentsDirectory];
}

+ (NSString *) databaseDirectory
{
	return [FXLSafeZone documentsDirectory];
}

+ (NSString *) attachmentsDirectory
{
	NSString *documentsDirectory = [FXLSafeZone documentsDirectory];
	return [documentsDirectory stringByAppendingPathComponent:@"attachments"];
}

+ (Class) fileHandleClass
{
    if ([self isInSafeZone])
    {
        return NSClassFromString(@"SZLSecureFileHandle");
    }
    else
    {
        return [NSFileHandle class];
    }

}

@end
