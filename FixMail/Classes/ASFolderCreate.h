/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "WBXMLRequest.h"
#import "ASFolder.h"
#import "ASFolderCreateDelegate.h"

typedef enum {
    kFolderCreateSuccess                = 1,
    kFolderCreateFolderExists           = 2,
    kFolderCreateFolderIsSpecial        = 3,
    kFolderCreateParentNotFound         = 5,
    kFolderCreateServerError            = 6,
    kFolderCreateSyncKeyInvalid         = 9,
    kFolderCreateMalformedRequest       = 10,
    kFolderCreateUnknownError           = 11,
    kFolderCreateUnusualBackendIssue    = 12
} EFolderCreateStatus;

@interface ASFolderCreate : WBXMLRequest {
}

@property (nonatomic)           EFolderType                         folderType;
@property (nonatomic,strong)    NSString*                           name;
@property (nonatomic,strong)    NSString*                           parentID;
@property (nonatomic,strong)    NSObject<ASFolderCreateDelegate>*   delegate;
@property (nonatomic,strong)    ASFolder*                           folder;     // Results
@property (nonatomic)           EFolderCreateStatus                 statusCodeAS;

// Factory
+ (ASFolderCreate*)queueFolderCreate:(ASAccount*)anAccount
                          folderType:(EFolderType)aFolderType
                                name:(NSString*)aName
                            parentID:(NSString*)aParentID
                       displayErrors:(BOOL)aDisplayErrors;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end