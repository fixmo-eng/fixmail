//
//  OGiDateInformation.h
//  Gosh2
//
//  Created by Harold Smith III on 10/13/11.
//  Copyright 2011 Fixmo Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

struct OGiDateInformation {
	int day;
	int month;
	int year;
	
	int weekday;
	
	int minute;
	int hour;
	int second;
	
};
typedef struct OGiDateInformation OGiDateInformation;

@interface NSDate (TKCategory)

+ (NSDate *) yesterday;
+ (NSDate *) month;

- (NSDate *) monthDate;
- (NSDate *) lastOfMonthDate;



- (BOOL) isSameDay:(NSDate*)anotherDate;
- (int) monthsBetweenDate:(NSDate *)toDate;
- (NSInteger) daysBetweenDate:(NSDate*)d;
- (BOOL) isToday;
- (BOOL) isWeekDay;



- (NSDate *)dateByAddingHours:(NSUInteger)hours minutes:(int)minutes ;
- (NSDate *) dateByAddingDays:(NSUInteger)days;
+ (NSDate *) dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime;

- (NSString*) dayString;
- (NSString *) monthString;
- (NSString *) yearString;


- (OGiDateInformation) dateInformation;
- (OGiDateInformation) dateInformationWithTimeZone:(NSTimeZone*)tz;
+ (NSDate*) dateFromDateInformation:(OGiDateInformation)info;
+ (NSDate*) dateFromDateInformation:(OGiDateInformation)info timeZone:(NSTimeZone*)tz;
+ (NSString*) dateInformationDescriptionWithInformation:(OGiDateInformation)info;



// Gets the ticks of the date at midnight for the date
- (NSDate*)getRoundedMinuteDateTime;
- (NSDate*)getRoundedMinuteDateTimeGMT;
- (NSDate*)getMidnightDateTime;
- (NSDate*)getMidnightDateTimeGMT;
- (NSDate*)getGMTTime;
- (NSDate*)getFirstOfMonth;
- (NSDate*)subtractMonth;
- (NSDate*)addMonth;
- (BOOL)isLastWeekOfMonth;
- (int)getYear;
- (int)getMonthOfYear;
- (int)getDayOfTheMonth;
- (int)getDayOfTheWeek;
- (int)numberOfDaysInMonth;
- (int)numberOfWeeksInMonth;



@end
