/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAutoTestSettings.h"
#import "ASAccount.h"
#import "ASAutoTest.h"
#import "ASOutOfOffice.h"
#import "ASSettings.h"
#import "Calendar.h"

@implementation ASAutoTestSettings


////////////////////////////////////////////////////////////////////////////////////////////
// Test Harness
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Test Harness

- (void)start
{
    //[self doSetOOFGlobal];
    //[self doGetOOF];
    [self doSetOOFTimeBased];
    [self doGetOOF];
    //[self doSetOOFDisable];
    //[self doGetOOF];
}

- (void)success
{
    [self.autoTest autoTestSuccess:self];
}

- (void)fail
{
    [self.autoTest autoTestFailed:self];
}

- (void)doGetOOF
{
    ASSettings* aSettings = [ASSettings queueGetOutOfOffice:(ASAccount*)self.account
                                              displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        ASSettings* aSettings = aNotification.object;
        FXDebugLog(kFXLogActiveSync, @"doGetOOF success");
        if(aSettings.oofInternal) {
            //FXDebugLog(kFXLogActiveSync, @"%@", aSettings.oofInternal);
        }
        if(aSettings.oofExternalKnown) {
            //FXDebugLog(kFXLogActiveSync, @"%@", aSettings.oofExternalKnown);
        }
        if(aSettings.oofExternalUnknown) {
            //FXDebugLog(kFXLogActiveSync, @"%@", aSettings.oofExternalUnknown);
        }
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        //ASSettings* aSettings = aNotification.object;
        [aSettings displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doSetOOFTimeBased
{
    NSDate* aStartDate  = [[Calendar calendar] addToDate:[NSDate date] hours:0 minutes:5];
    NSDate* anEndDate   = [[Calendar calendar] addToDate:aStartDate hours:0 minutes:10];
    
    ASOutOfOffice* anOOF = [[ASOutOfOffice alloc] init];
    anOOF.enabled   = TRUE;
    anOOF.bodyType  = kOOFBodyText;
    anOOF.message   = @"Out of office";
    
    ASSettings* aSettings = [ASSettings queueSetOutOfOfficeForTime:(ASAccount*)self.account
                                                             start:aStartDate
                                                               end:anEndDate
                                                          internal:anOOF
                                                     externalKnown:anOOF
                                                   externalUnknown:anOOF
                                                     displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        //ASSettings* aSettings = aNotification.object;
        FXDebugLog(kFXLogActiveSync, @"doSetOOFTimeBased success");
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSettings* aSettings = aNotification.object;
        [aSettings displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doSetOOFGlobal
{
    ASOutOfOffice* anOOF = [[ASOutOfOffice alloc] init];
    anOOF.enabled   = TRUE;
    anOOF.bodyType  = kOOFBodyText;
    anOOF.message   = @"Out of office";
    
    ASSettings* aSettings = [ASSettings queueSetOutOfOffice:(ASAccount*)self.account
                                                   internal:anOOF
                                              externalKnown:anOOF
                                            externalUnknown:anOOF
                                              displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        //ASSettings* aSettings = aNotification.object;
        FXDebugLog(kFXLogActiveSync, @"doSetOOFGlobal success");
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSettings* aSettings = aNotification.object;
        [aSettings displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doSetOOFDisable
{
    ASSettings* aSettings = [ASSettings queueDisableOutOfOffice:(ASAccount*)self.account displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        //ASSettings* aSettings = aNotification.object;
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSettings queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSettings* aSettings = aNotification.object;
        [aSettings displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

@end