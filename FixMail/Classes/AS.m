/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/*
 *  FIXME K9 derived work, need appropriate copyrights here
 *  Tags and strings are derived from K9 Android email client under an Apache license
 */

// Active Sync Code Page and Tag resolver strings
//
// FIXME, need to count tags and limit check in resolver to prevent instability
//
#import "AS.h"

const int kMaxASCodePages = 0x19;

static const char* pageAirSync[] = {
    // 0x00 AirSync
    "Sync", "Responses", "Add", "Change", "Delete", "Fetch", "SyncKey", "ClientId",
    "ServerId", "Status", "Collection", "Class", "Version", "CollectionId", "GetChanges",
    "MoreAvailable", "WindowSize", "Commands", "Options", "FilterType", "Truncation",
    "RTFTruncation", "Conflict", "Collections", "ApplicationData", "DeletesAsMoves",
    "NotifyGUID", "Supported", "SoftDelete", "MIMESupport", "MIMETruncation", "Wait",
    "Limit", "Partial", "ConversationMode", "MaxItems", "HeartbeatInterval",
    NULL
};

static const char* pageContacts[] = {
    // 0x01 Contacts
    "Anniversary", "AssistantName", "AssistantTelephoneNumber", "Birthday", "ContactsBody",
    "ContactsBodySize", "ContactsBodyTruncated", "Business2TelephoneNumber", "BusinessAddressCity",
    "BusinessAddressCountry", "BusinessAddressPostalCode", "BusinessAddressState",
    "BusinessAddressStreet", "BusinessFaxNumber", "BusinessTelephoneNumber",
    "CarTelephoneNumber", "ContactsCategories", "ContactsCategory", "Children", "Child",
    "CompanyName", "Department", "Email1Address", "Email2Address", "Email3Address",
    "FileAs", "FirstName", "Home2TelephoneNumber", "HomeAddressCity", "HomeAddressCountry",
    "HomeAddressPostalCode", "HomeAddressState", "HomeAddressStreet", "HomeFaxNumber",
    "HomeTelephoneNumber", "JobTitle", "LastName", "MiddleName", "MobileTelephoneNumber",
    "OfficeLocation", "OtherAddressCity", "OtherAddressCountry",
    "OtherAddressPostalCode", "OtherAddressState", "OtherAddressStreet", "PagerNumber",
    "RadioTelephoneNumber", "Spouse", "Suffix", "Title", "Webpage", "YomiCompanyName",
    "YomiFirstName", "YomiLastName", "CompressedRTF", "Picture", "Alias", "WeightedRank",
    NULL
};
static const char* pageEmail[] = {
    // 0x02 Email
    "Attachment", "Attachments", "AttName", "AttSize", "Add0Id", "AttMethod", "AttRemoved",
    "Body", "BodySize", "BodyTruncated", "DateReceived", "DisplayName", "DisplayTo",
    "Importance", "MessageClass", "Subject", "Read", "To", "CC", "From", "ReplyTo",
    "AllDayEvent", "Categories", "Category", "DTStamp", "EndTime", "InstanceType",
    "IntDBusyStatus", "Location", "MeetingRequest", "Organizer", "RecurrenceId", "Reminder",
    "ResponseRequested", "Recurrences", "Recurrence", "Recurrence_Type", "Recurrence_Until",
    "Recurrence_Occurrences", "Recurrence_Interval", "Recurrence_DayOfWeek",
    "Recurrence_DayOfMonth", "Recurrence_WeekOfMonth", "Recurrence_MonthOfYear",
    "StartTime", "Sensitivity", "TimeZone", "GlobalObjId", "ThreadTopic", "MIMEData",
    "MIMETruncated", "MIMESize", "InternetCPID", "Flag", "FlagStatus", "EmailContentClass",
    "FlagType", "CompleteTime", "DisallowNewTimeProposal",
    NULL
};
static const char* pageAirNotify[] = {
    // 0x03 AirNotify (Obsolete)
    NULL
};
static const char* pageCalendar[] = {
    // 0x04 Calendar
    "TimeZone", "AllDayEvent", "Attendees", "Attendee", "Attendee_Email",
    "Attendee_Name", "Body", "BodyTruncated", "BusyStatus", "Categories",
    "Category", "Compressed_RTF", "DTStamp", "EndTime", "Exception",
    "Exceptions", "Exception_IsDeleted", "Exception_StartTime", "Location",
    "MeetingStatus", "Organizer_Email", "Organizer_Name", "Recurrence",
    "Recurrence_Type", "Recurrence_Until", "Recurrence_Occurrences",
    "Recurrence_Interval", "Recurrence_DayOfWeek", "Recurrence_DayOfMonth",
    "Recurrence_WeekOfMonth", "Recurrence_MonthOfYear", "Reminder",
    "Sensitivity", "Subject", "StartTime", "UID", "Attendee_Status",
    "Attendee_Type", "", "", "", "", "", "", "", "", "DisallowNewTimeProposal", 
    "ResponseRequested", "AppointmentReplyTime", "ResponseType", "CalendarType",
    "IsLeapMonth", "FirstDayOfWeek", "OnlineMeetingConfLink", "OnlineMeetingExternalLink",
    NULL
};
static const char* pageMove[] = {
    // 0x05 Move
    "MoveItems", "Move", "SrcMsgId", "SrcFldId", "DstFldId", "MoveResponse", "MoveStatus",
    "DstMsgId",
    NULL
};
static const char* pageGetItemEstimate[] = {
    // 0x06 GetItemEstimate
    "GetItemEstimate", "Version", "Collections", "Collection", "Class", "CollectionId",
    "DateTime", "Estimate", "Response", "Status",
    NULL
};
static const char* pageFolderHierarchy[] = {
    // 0x07 FolderHierarchy
    "Folders", "Folder", "FolderDisplayName", "FolderServerId", "FolderParentId", "Type",
    "FolderResponse", "FolderStatus", "FolderContentClass", "Changes", "FolderAdd",
    "FolderDelete", "FolderUpdate", "FolderSyncKey", "FolderCreate",
    "FolderDelete", "FolderUpdate", "FolderSync", "Count", "FolderVersion",
    NULL
};
static const char* pageMeetingResponse[] = {
    // 0x08 MeetingResponse
    "CalendarId", "CollectionId", "MeetingResponse", "RequestId", "Request",
    "Result", "Status", "UserResponse", "Version", "InstanceId",
    NULL
};
static const char* pageTasks[] = {
    // 0x09 Tasks
    "Body", "BodySize", "BodyTruncated", "Categories", "Category", "Complete",
    "DateCompleted", "DueDate", "UTCDueDate", "Importance", "Recurrence", "RecurrenceType",
    "RecurrenceStart", "RecurrenceUntil", "RecurrenceOccurrences", "RecurrenceInterval",
    "RecurrenceDOM", "RecurrenceDOW", "RecurrenceWOM", "RecurrenceMOY",
    "RecurrenceRegenerate", "RecurrenceDeadOccur", "ReminderSet", "ReminderTime",
    "Sensitivity", "StartDate", "UTCStartDate", "Subject", "CompressedRTF", "OrdinalDate",
    "SubordinalDate", "CalendarType", "IsLeapMonth", "FirstDayOfWeek",
    NULL
};
static const char* pageResolveRecipients[] = {
    // 0x0A ResolveRecipients
    "ResolveRecipients", "Response", "Status", "Type", "Recipient", "DisplayName", "EmailAddress", 
    "Certificates", "Certificate", "MiniCertificate", "Options", "To", "CertificateRetrieval",
    "RecipientCount", "MaxCertificates", "MaxAmbiguousRecipients", "CertificateCount", "Availability",
    "StartTime", "EndTime", "MergedFreeBusy", "Picture", "MaxSize", "Data", "MaxPictures",
    NULL
};
static const char* pageValidateCert[] = {
    // 0x0B ValidateCert
    "ValidateCert", "Certificates", "Certificate", "CertificateChain", "CheckCRL", "Status",
    NULL
};
static const char* pageContacts2[] = {
    // 0x0C Contacts2
    "CustomerId", "GovernmentId", "IMAddress", "IMAddress2", "IMAddress3", "ManagerName",
    "CompanyMainPhone", "AccountName", "NickName", "MMS",
    NULL
};
static const char* pagePing[] = {
    // 0x0D Ping
    "Ping", "AutdState", "PingStatus", "HeartbeatInterval", "PingFolders", "PingFolder",
    "PingId", "PingClass", "MaxFolders",
    NULL
};
static const char* pageProvision[] = {
    // 0x0E Provision
    "Provision", "Policies", "Policy", "PolicyType", "PolicyKey", "Data", "ProvisionStatus",
    "RemoteWipe", "EASProvisionDoc", "DevicePasswordEnabled",
    "AlphanumericDevicePasswordRequired",
    "DeviceEncryptionEnabled", "PasswordRecoveryEnabled", "-unused-", "AttachmentsEnabled",
    "MinDevicePasswordLength",
    "MaxInactivityTimeDeviceLock", "MaxDevicePasswordFailedAttempts", "MaxAttachmentSize",
    "AllowSimpleDevicePassword", "DevicePasswordExpiration", "DevicePasswordHistory",
    "AllowStorageCard", "AllowCamera", "RequireDeviceEncryption",
    "AllowUnsignedApplications", "AllowUnsignedInstallationPackages",
    "MinDevicePasswordComplexCharacters", "AllowWiFi", "AllowTextMessaging",
    "AllowPOPIMAPEmail", "AllowBluetooth", "AllowIrDA", "RequireManualSyncWhenRoaming",
    "AllowDesktopSync",
    "MaxCalendarAgeFilter", "AllowHTMLEmail", "MaxEmailAgeFilter",
    "MaxEmailBodyTruncationSize", "MaxEmailHTMLBodyTruncationSize",
    "RequireSignedSMIMEMessages", "RequireEncryptedSMIMEMessages",
    "RequireSignedSMIMEAlgorithm", "RequireEncryptionSMIMEAlgorithm",
    "AllowSMIMEEncryptionAlgorithmNegotiation", "AllowSMIMESoftCerts", "AllowBrowser",
    "AllowConsumerEmail", "AllowRemoteDesktop", "AllowInternetSharing",
    "UnapprovedInROMApplicationList", "ApplicationName", "ApprovedApplicationList", "Hash",
    NULL
};
static const char* pageSearch[] = {
    // 0x0F Search
    "Search", "Stores", "Store", "Name", "Query",
    "Options", "Range", "Status", "Response", "Result",
    "Properties", "Total", "EqualTo", "Value", "And",
    "Or", "FreeText", "SubstringOp", "DeepTraversal", "LongId",
    "RebuildResults", "LessThan", "GreateerThan", "Schema", "Supported",
    // New
    "UserName", "Password", "ConversationId", "Picture", "MaxSize", "MaxPictures",
    NULL
};
static const char* pageGal[] = {
    // 0x10 Gal
    "GalDisplayName", "GalPhone", "GalOffice", "GalTitle", "GalCompany", "GalAlias",
    "GalFirstName", "GalLastName", "GalHomePhone", "GalMobilePhone", "GalEmailAddress",
    // New
    "GalPicture", "GalStatus", "GalData",
    NULL
};
static const char* pageAirSyncBase[] = {
    // 0x11 AirSyncBase
    "BodyPreference", "BodyPreferenceType", "BodyPreferenceTruncationSize", "AllOrNone",
    "--unused--", "BaseBody", "BaseData", "BaseEstimatedDataSize", "BaseTruncated",
    "BaseAttachments", "BaseAttachment", "BaseDisplayName", "FileReference", "BaseMethod",
    "BaseContentId", "BaseContentLocation", "BaseIsInline", "BaseNativeBodyType",
    "BaseContentType",
    NULL
};
static const char* pageSettings[] = {
    // 0x12 Settings
    "Settings", "Status", "Get", "Set", "Oof", "OofState", "StartTime", "EndTime", "OofMessage", "AppliesToInternal",
    "AppliesToExternalKnown", "AppliesToExternalUnknown", "Enabled", "ReplyMessage", "BodyType", "DevicePassword",
    "Password", "DeviceInformation", "Model", "IMEI", "FriendlyName", "OS", "OSLanguage", "PhoneNumber", "UserInformation",
    "EmailAddresses", "SmtpAddress", "UserAgent", "EnableOutboundSMS", "MobileOperator", "PrimarySmtpAddress", "Accounts",
    "Account", "AccountId", "AccountName", "UserDisplayName", "SendDisabled", "RightsManagentInformation",
    NULL
};
static const char* pageDocumentLibrary[] = {
    // 0x13 DocumentLibrary
    "LinkId", "DisplayName", "IsFolder", "CreationDate", "LastModifiedDate", "IsHidden", "ContentLength", "ContentType",
    NULL
};
static const char* pageItemOperations[] = {
    // 0x14 ItemOperations
    "ItemOperations", "Fetch", "Store", "Options", "Range", "Total", "Properties", "Data", "Status", "Response", "Versions",
    "Schema", "Part", "EmptyFolderContents", "DeleteSubFolders", "UserName", "Password", "Move", "DstFldId", "ConversationId",
    "MoveAlways",
    NULL
};
static const char* pageCompose[] = {
    // 0x15 pageCompose
    "SendMail","SmartForward","SmartReply","SaveInSentItems","ReplaceMime","Undefined","Source","FolderId","ItemId","LongId",
    "InstanceId","Mime","ClientId","Status","AccountId",
    NULL
};
static const char* pageEmail2[] = {
    // 0x16 pageEmail2
    "UmCallerID", "UmUserNotes", "UmAttDuration", "UmAttOrder", "ConversationId", "ConversationIndex", "LastVerbExecuted", "LastVerbExecutionTime",
    "ReceivedAsBcc", "Sender", "CalendarType", "IsLeapMonth", "AccountId", "FirstDayOfWeek", "MeetingMessageType",
    NULL
};
static const char* pageNotes[] = {
    // 0x17 pageNotes
    "Subject", "MessageClass", "LastModifiedDate", "Categories", "Category",
    NULL
};
static const char* pageRights[] = {
    // 0x18 pageRights
    "RightsManagementSupport", "RightsManagementTemplates", "RightsManagementTemplate", "RightsManagementLicense", "EditAllowed",
    "ReplyAllowed", "ReplayAllAllowed", "ForwardAllowed", "ModifyRecipientsAllowed", "ExtractAllowed", "PrintAllowed", "ExportAllowed",
    "ProgrammaticAccessAllowed", "Owner", "ContentExpiryDate", "TemplateID", "TemplateName", "TemplateDescription", "ContentOwner",
    "RemoveRightsManagementDistribution",
    NULL
};


//const int kMaxASCodePages = 0x19;
static const char** pageTable[kMaxASCodePages] = {
    pageAirSync,            // 0x00
    pageContacts,           // 0x01
    pageEmail,              // 0x02
    pageAirNotify,          // 0x03
    pageCalendar,           // 0x04
    pageMove,               // 0x05 
    pageGetItemEstimate,    // 0x06
    pageFolderHierarchy,    // 0x07
    pageMeetingResponse,    // 0x08
    pageTasks,              // 0x09
    pageResolveRecipients,  // 0x0A
    pageValidateCert,       // 0x0B
    pageContacts2,          // 0x0C
    pagePing,               // 0x0D
    pageProvision,          // 0x0E
    pageSearch,             // 0x0F
    pageGal,                // 0x10
    pageAirSyncBase,        // 0x11
    pageSettings,           // 0x12
    pageDocumentLibrary,    // 0x13
    pageItemOperations,     // 0x14
    pageCompose,            // 0x15
    pageEmail2,             // 0x16
    pageNotes,              // 0x17
    pageRights              // 0x18
};

@implementation AS

+ (const char***)pageTable
{
    return pageTable;
}

+ (const char**)page:(int)aPageNumer
{
    if(aPageNumer >= 0 && aPageNumer < kMaxASCodePages) {
        return pageTable[aPageNumer];
    }else{
        FXDebugLog(kFXLogActiveSync, @"AS Page invalid: %d", aPageNumer);
        return pageTable[0];
    }
}

@end





