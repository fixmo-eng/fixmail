/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAutoTestDelegate.h"


@class ASAccount;
@class ASAutoTestFolder;
@class ASAutoTestSearch;
@class ASAutoTestSettings;

@interface ASAutoTest : NSObject <ASAutoTestDelegate> {
    
}

@property (nonatomic,strong) ASAccount*             account;
@property (nonatomic,strong) ASAutoTestFolder*      autoTestFolder;
@property (nonatomic,strong) ASAutoTestSearch*      autoTestSearch;
@property (nonatomic,strong) ASAutoTestSettings*    autoTestSettings;
@property (nonatomic,) BOOL                         inProgress;



- (id)initWithAccount:(ASAccount*)anAccount;

- (void)start;

@end
