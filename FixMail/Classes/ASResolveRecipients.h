/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "WBXMLRequest.h"
#import "ASResolveRecipientsDelegate.h"

typedef enum {
    kCertTypeNone  = 1,
    kCertTypeFull  = 2,
    kCertTypeMini  = 3
} ECertType;

typedef enum {
    kResolveStatusSuccess					= 1,
	kResolveStatusRecipientAmbiguous1		= 2,	// seem to be pretty much the same
	kResolveStatusRecipientAmbiguous2		= 3,	//	"		"		"		"
	kResolveStatusRecipientUnresolved		= 4,
    kResolveStatusProtocolError				= 5,
    kResolveStatusServerError				= 6,
	kResolveStatusNoSMIMECertificate		= 7,
	kResolveStatusCertificateLimitReached	= 8,
	kResolveStatusMoreThan100Recipients		= 160,
	kResolveStatusMoreThan20Recipients		= 161,
	kResolveStatusFreeBusyStateTimeout		= 162,
	kResolveStatusFreeBusyStateUnresolved	= 163,
    kResolveStatusNoPicture					= 173,
    kResolveStatusPictureTooLarge			= 174,
    kResolveStatusPictureLimitReached		= 175,
} EResolveStatus;

@interface ASResolveRecipients : WBXMLRequest {
}

@property (nonatomic,strong) NSArray*								recipients;
@property (nonatomic,strong) NSObject<ASResolveRecipientsDelegate>*	delegate;
@property (nonatomic) EResolveStatus								statusCodeAS;


// Factory
+ (ASResolveRecipients *)sendResolveRecipientsCert:(ASAccount*)anAccount
										recipients:(NSArray*)aRecipients
										  certType:(ECertType)aCertType
										  delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate;

+ (ASResolveRecipients *)sendResolveRecipientsAvailability:(ASAccount*)anAccount
												recipients:(NSArray*)aRecipients
												 startTime:(NSDate*)aStartTime
												   endTime:(NSDate*)anEndTime
												  delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate;

+ (ASResolveRecipients *)sendResolveRecipientsPicture:(ASAccount*)anAccount
										   recipients:(NSArray*)aRecipients
											 delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate;

@end