/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"

@class ASAccount;
@class ASFolder;
@class ASProvisionPolicy;

// Types
//
typedef enum {
    kProvisionRequestPolicy,
    kProvisionPolicyAcknowledge
} EProvisionActionType;

@interface ASProvision : WBXMLRequest {
    NSString*               policyKey;
    ASProvisionPolicy*      provisionPolicy;
    EProvisionActionType    provisionActionType;
    
    // FIXME Replace with ProvisionPolicy and remove these
    //
    // Provision Profile
    // FIXME move this in to ASProvisionPolicy class
    //
    // Password
    unsigned int            maxInactivityTimeDeviceLock;
    unsigned int            maxDevicePasswordFailedAttempts;
    unsigned int            minDevicePasswordLength;
    unsigned int            devicePasswordHistory;
    unsigned int            devicePasswordExpiration;
    
    BOOL                    allowSimpleDevicePassword;
    BOOL                    devicePasswordEnabled;
    BOOL                    passwordRecoveryEnabled;
    BOOL                    alphanumericDevicePasswordRequired;

    // Encryption
    BOOL                    deviceEncryptionEnabled;

    // Attachments
    unsigned int            maxAttachmentSize;
    BOOL                    attachmentsEnabled;
}

// Properties
@property EProvisionActionType          provisionActionType;
@property (strong) NSString*            policyKey;
@property (strong) ASProvisionPolicy*   provisionPolicy;


// Factory
+ (void)sendRequest:(ASAccount*)anAccount 
    provisionAction:(EProvisionActionType)aProvisionType 
          policyKey:(NSString*)aPolicyKey
    provisionPolicy:(ASProvisionPolicy*)aProvisionPolicy;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount 
  provisionActionType:(EProvisionActionType)aProvisionActionType
      provisionPolicy:(ASProvisionPolicy*)aProvisionPolicy;
@end

