/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"

@class ASContact;

@interface EmailAddress : BaseObject {
	BOOL			_certExists;
	BOOL			_addressIsValid;
}

@property (nonatomic,strong) NSString*      name;
@property (nonatomic,strong) NSString*      address;


- (id)initWithName:(NSString*)aName address:(NSString*)anAddress;
- (id)initWithString:(NSString*)aNameAndAddress;

- (NSDictionary*)asDictionary;
- (NSString *) fullEmailAddress;
- (BOOL) isValidEmailAddress;
- (BOOL) hasCertificate;

+ (NSString *) extractEmailAddress:(NSString *)inEmailAddress;
+ (BOOL) isProperRFCEmailAddress:(NSString *)inEmailAddress;

- (ASContact *)asExistingContact;
- (UIViewController *)asContactViewController;

@end
