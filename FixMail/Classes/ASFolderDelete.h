/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "WBXMLRequest.h"
#import "ASFolderDeleteDelegate.h"

@class ASFolder;

typedef enum {
    kFolderDeleteSuccess                = 1,
    kFolderDeleteFolderIsSpecial        = 3,
    kFolderDeleteDoesNotExist           = 4,
    kFolderDeleteServerError            = 6,
    kFolderDeleteSyncKeyInvalid         = 9,
    kFolderDeleteMalformedRequest       = 10,
    kFolderDeleteUnknownError           = 11
} EFolderDeleteStatus;

@interface ASFolderDelete : WBXMLRequest {
}

@property (nonatomic,strong)    ASFolder*                           folder;
@property (nonatomic,strong)    NSObject<ASFolderDeleteDelegate>*   delegate;
@property (nonatomic)           EFolderDeleteStatus                 statusCodeAS;


// Factory
+ (ASFolderDelete*)queueFolderDelete:(ASFolder*)aFolder
                       displayErrors:(BOOL)aDisplayErrors;
@end