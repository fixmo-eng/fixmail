#if 0
// Failed experitment, add and change don't seem to work on email which prevents changing drafts on the server
//
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncEmail.h"
#import "ASAccount.h"
#import "ASEmailFolder.h"
#import "ASSync.h"
#import "ASSyncObject.h"
#import "EmailAddress.h"
#import "WBXMLDataGenerator.h"

@implementation ASSyncEmail

@synthesize email;
@synthesize delegate;

// Constants
static NSString* kCommand               = @"Sync";
static const ASTag kCommandTag          = SYNC_SYNC;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithEmail:(ASEmail*)anEmail delegate:(NSObject<ASSyncObjectDelegate>*)aDelegate isAdd:(BOOL)anIsAdd
{
    if (self = [super init]) {
        self.email      = anEmail;
        self.delegate   = aDelegate;
        isAdd           = anIsAdd;
    }
    return self;
}

- (void)send
{
    ASSync* aSync = nil;
    
    if(isAdd) {
        aSync = [self sendAddEmail:self.email delegate:self.delegate];
    }else{
        aSync = [self sendChangeEmail:self.email delegate:self.delegate];
    }
    
    if(aSync) {
        [super sendAndWait:aSync];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Email sync failed: %@", self);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
// WBXML Generators
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Generators

- (ASSync*)sendAddEmail:(ASEmail*)anEmail delegate:(NSObject<ASSyncObjectDelegate>*)aDelegate
{
    ASSync* aSync = nil;
    @try {
        ASEmailFolder* aFolder  = (ASEmailFolder*)anEmail.folder;
        ASAccount* anAccount    = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.object                = (BaseObject*)anEmail;
        aSync.syncObjectDelegate    = aDelegate;
        
        NSData* aWbxml = [self wbxmlAddEmail:anEmail request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendAddEvent" exception:e];
    }
    return aSync;
}

- (NSString*)addressListAsString:(NSArray*)inList
{
    NSString *addressString = @"";
	for (EmailAddress *theAddress in inList) {
		if (addressString.length == 0) {
			addressString = [addressString stringByAppendingString:theAddress.fullEmailAddress];
		} else {
			addressString = [addressString stringByAppendingFormat:@", %@", theAddress.fullEmailAddress];
		}
	}
    return addressString;
}

- (void)wbxmlEmail:(ASEmail*)anEmail wbxml:(WBXMLDataGenerator&)wbxml
{
    //NSDateFormatter* aDateFormatter = [ASEmail dateFormatter];
    
    wbxml.start(SYNC_APPLICATION_DATA); {
        NSArray* aTos = anEmail.toAsArrayOfEmailAddresses;;
        if(aTos.count > 0) {
            for(EmailAddress* anEmailAddress in aTos) {
                FXDebugLog(kFXLogActiveSync, @"wbxmlEmail to: %@", anEmailAddress);
            }
            NSString* aString = [self addressListAsString:aTos];
            wbxml.keyValue(EMAIL_TO, aString);
        }
        if(anEmail.ccAsArrayOfEmailAddresses.count > 0) {
            wbxml.keyValue(EMAIL_CC, anEmail.ccFlat);
        }
        //if(anEmail.bccAsArrayOfEmailAddresses.count > 0) {
        //    wbxml.keyValue(EMAIL_BCC, anEmail.bccFlat);
        //}
        if(anEmail.subject.length > 0) {
            wbxml.keyValue(EMAIL_SUBJECT, anEmail.subject);
        }
        if(anEmail.body.length > 0) {
        }

    }wbxml.end();
}

- (NSData*)wbxmlAddEmail:(ASEmail*)anEmail request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASEmailFolder* aFolder = (ASEmailFolder*)anEmail.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.keyValue(SYNC_GET_CHANGES, @"1");
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_ADD); {
                        ASAccount* anAccount = (ASAccount*)aFolder.account;
                        wbxml.keyValue(SYNC_CLIENT_ID, [anAccount getClientID]);
                        [self wbxmlEmail:anEmail wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

- (NSData*)wbxmlChangeEmail:(ASEmail*)anEmail request:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
    ASEmailFolder* aFolder = (ASEmailFolder*)anEmail.folder;
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.start(SYNC_COMMANDS); {
                    wbxml.start(SYNC_CHANGE); {
                        wbxml.keyValue(SYNC_SERVER_ID, [anEmail uid]);
                        [self wbxmlEmail:anEmail wbxml:wbxml];
                    }wbxml.end();
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

- (ASSync*)sendChangeEmail:(ASEmail*)anEmail delegate:(NSObject<ASSyncObjectDelegate>*)anDelegate
{
    ASSync* aSync = nil;
    @try {
        ASEmailFolder* aFolder = (ASEmailFolder*)anEmail.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders           = [NSArray arrayWithObject:aFolder];
        aSync.isChangeRequest       = TRUE;
        aSync.object                = (BaseObject*)anEmail;
        aSync.syncObjectDelegate    = anDelegate;
        NSData* aWbxml = [self wbxmlChangeEmail:anEmail request:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendChangeEmail" exception:e];
    }
    return aSync;
}

@end
#endif