// Obsolete since switch to ASItemOperations for body fetch
#if 0
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncBody.h"
#import "ASFolder.h"
#import "ASSync.h"

@implementation ASSyncBody

@synthesize folder;
@synthesize email;
@synthesize bodyType;
@synthesize delegate;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(ASFolder*)aFolder email:(ASEmail*)anEmail bodyType:(EBodyType)aBodyType delegate:(NSObject<ASSyncMessageDelegate>*)aDelegate
{
    if (self = [super init]) {
        self.folder     = aFolder;
        self.email      = anEmail;
        self.bodyType   = aBodyType;
        self.delegate   = aDelegate;
    }
    return self;
}


- (void)send
{
    ASSync* aSync = [ASSync sendFetch:self.folder email:self.email bodyType:self.bodyType delegate:self.delegate];
    [super sendAndWait:aSync];
}

@end
#endif