/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAutoTestFolder.h"
#import "ASAccount.h"
#import "ASAutoTest.h"
#import "ASFolder.h"
#import "ASFolderCreate.h"
#import "ASFolderDelete.h"
#import "ASFolderSync.h"
#import "ASFolderUpdate.h"
#import "ASMoveItems.h"
#import "ASPing.h"
#import "ASSync.h"

@implementation ASAutoTestFolder


// Static
static NSString* kFolderTestCreate1 = @"FolderTestCreate1";
static NSString* kFolderTestCreate2 = @"FolderTestCreate2";
static NSString* kFolderTestUpdate1 = @"FolderTestUpdate1";

////////////////////////////////////////////////////////////////////////////////////////////
// Test Harness
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Test Harness

- (void)start
{
    // Cleanup test folders if they exist from previous failed run
    //
    [self cleanup];

    // Start test 
    [self doFolderCreate1];
}

- (void)cleanup
{
    // Cleanup test folders if they exist from previous failed run
    //
    ASFolder* aFolder = (ASFolder*)[self.account folderForName:kFolderTestCreate1];
    if(aFolder && !aFolder.isDeleted) {
        [ASFolderDelete queueFolderDelete:aFolder displayErrors:YES];
    }
    aFolder = (ASFolder*)[self.account folderForName:kFolderTestUpdate1];
    if(aFolder && !aFolder.isDeleted) {
        [ASFolderDelete queueFolderDelete:aFolder displayErrors:YES];
    }
}

- (void)success
{
    [ASFolderSync queueRequest:self.account displayErrors:YES];

    [self.autoTest autoTestSuccess:self];
}

- (void)fail
{
    [self.autoTest autoTestFailed:self];
}

- (BOOL)folderExists:(NSString*)aName folderType:(EFolderType)aFolderType
{
    BOOL success = NO;
    
    NSArray* aFolders = [self.account foldersForFolderType:aFolderType];
    for(ASFolder* aFolder in aFolders) {
        if([aFolder.uid isEqualToString:aFolder.uid]
           && [aFolder.displayName isEqualToString:aName]
           && !aFolder.isDeleted) {
            success = YES;
            break;
        }
    }
    return success;
}

- (void)doFolderCreate1
{
    ASFolderCreate* aFolderCreate = [ASFolderCreate queueFolderCreate:(ASAccount*)self.account
                                                           folderType:kFolderTypeUser
                                                                 name:kFolderTestCreate1
                                                             parentID:@"0"
                                                        displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aFolderCreate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderCreate* aFolderCreate = aNotification.object;
        self.folder1 = aFolderCreate.folder;
        @try {
            BOOL success = [self folderExists:kFolderTestCreate1 folderType:kFolderTypeUser];
            if(success) {
                FXDebugLog(kFXLogActiveSync, @"folder created: %@", self.folder1.displayName);
                // Next test, rename folder1
                //
                [self performSelector:@selector(doFolderCreate2:) withObject:self.folder1];
            }else{
                [self handleError:[NSString stringWithFormat:@"folder create failed: %@", self.folder1.displayName]];
            }
        }@catch (NSException* e) {
            [self logException:@"folderCreated" exception:e];
        }
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aFolderCreate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderCreate* aFolderCreate = aNotification.object;
        [aFolderCreate displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doFolderCreate2:(ASFolder*)aFolder
{
    ASFolderCreate* aFolderCreate = [ASFolderCreate queueFolderCreate:(ASAccount*)self.account
                                                           folderType:kFolderTypeUser
                                                                 name:kFolderTestCreate2
                                                             parentID:aFolder.uid
                                                        displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aFolderCreate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderCreate* aFolderCreate = aNotification.object;
        self.folder2 = aFolderCreate.folder;
        @try {
            BOOL success = [self folderExists:kFolderTestCreate2 folderType:kFolderTypeUser];
            if(success) {
                FXDebugLog(kFXLogActiveSync, @"folder created: %@", self.folder2.displayName);
                
                // Next test, rename folder1
                //
                [self performSelector:@selector(doFolderUpdate:) withObject:self.folder1];
            }else{
                [self handleError:[NSString stringWithFormat:@"folder create failed: %@", self.folder2.displayName]];
            }
        }@catch (NSException* e) {
            [self logException:@"folderCreated" exception:e];
        }
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aFolderCreate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderCreate* aFolderCreate = aNotification.object;
        [aFolderCreate displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doFolderUpdate:(ASFolder*)aFolder
{
    ASFolderUpdate* aFolderUpdate = [ASFolderUpdate queueFolderUpdate:aFolder newName:kFolderTestUpdate1 displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aFolderUpdate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderUpdate* aFolderUpdate = aNotification.object;
        ASFolder* aFolder = aFolderUpdate.folder;
        @try {
            BOOL success = [self folderExists:kFolderTestUpdate1 folderType:kFolderTypeUser];
            if(success) {
                FXDebugLog(kFXLogActiveSync, @"folder updated: %@", aFolder.displayName);
                
                // Next test, delete this folder
                //
                [self performSelector:@selector(doFolderDelete:) withObject:self.folder1];
                [self performSelector:@selector(doFolderDelete:) withObject:self.folder2];
            }else{
                [self handleError:[NSString stringWithFormat:@"folder update failed: %@", aFolder.displayName]];
            }
        }@catch (NSException* e) {
            [self logException:@"folderUpdated" exception:e];
        }
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aFolderUpdate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderUpdate* aFolderUpdate = aNotification.object;
        [aFolderUpdate displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}


- (void)doFolderMove:(ASFolder*)aFolder
{
#if 0
    ASFolderUpdate* aFolderUpdate = [ASMoveItems sendMove:self.account emails:<#(NSArray *)#> toFolder:<#(ASFolder *)#> displayErrors:<#(BOOL)#>:aFolder newName:kFolderTestUpdate1 displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aFolderUpdate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderUpdate* aFolderUpdate = aNotification.object;
        ASFolder* aFolder = aFolderUpdate.folder;
        @try {
            BOOL success = [self folderExists:kFolderTestUpdate1 folderType:kFolderTypeUser];
            if(success) {
                FXDebugLog(kFXLogActiveSync, @"folder updated: %@", aFolder.displayName);
                
                // Next test, delete this folder
                //
                [self performSelector:@selector(doFolderDelete:) withObject:aFolder];
            }else{
                [self handleError:[NSString stringWithFormat:@"folder update failed: %@", aFolder.displayName]];
            }
        }@catch (NSException* e) {
            [self logException:@"folderUpdated" exception:e];
        }
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aFolderUpdate queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderUpdate* aFolderUpdate = aNotification.object;
        [aFolderUpdate displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
#endif
}

- (void)doFolderDelete:(ASFolder*)aFolder
{
    ASFolderDelete* aFolderDelete = [ASFolderDelete queueFolderDelete:aFolder displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aFolderDelete queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderDelete* aFolderDelete = aNotification.object;
        ASFolder* aFolder = aFolderDelete.folder;
        
        @try {
            BOOL success = ![self folderExists:kFolderTestUpdate1 folderType:kFolderTypeUser];            
            if(success) {
                FXDebugLog(kFXLogActiveSync, @"folder deleted: %@", aFolder.displayName);
                
                // Last test send success to ASAutoTest
                [self success];
            }else{
                [self handleError:[NSString stringWithFormat:@"folder delete failed: %@", aFolder.displayName]];
                [self fail];
            }
        }@catch (NSException* e) {
            [self logException:@"folderDeleted" exception:e];
        }

		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aFolderDelete queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        ASFolderDelete* aFolderDelete = aNotification.object;
        [aFolderDelete displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

@end