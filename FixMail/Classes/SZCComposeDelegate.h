//
//  SZCComposeDelegate.h
//  FixMail
//
//  Created by Magali Boizot-Roche on 13-03-20.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SZCComposeDelegate <NSObject>
-(IBAction) toggleCompose:(id)sender;
@end
