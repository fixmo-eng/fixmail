//
//  EmailConstants.h
//  FixMail
//
//  Created by Colin Biggin on 2013-02-04.
//
//

#ifndef FixMail_EmailConstants_h
#define FixMail_EmailConstants_h

// Email Dictionary Keys
#if 0
static const NSString* kUid             = @"uid";
static const NSString* kDbNum           = @"dbNum";
static const NSString* kPk              = @"pk";
static const NSString* kFolderNum       = @"folderNum";
static const NSString* kSenderAddress   = @"senderAddress";
static const NSString* kSenderName      = @"senderName";
static const NSString* kPeople          = @"people";
static const NSString* kDateTime        = @"datetime";
static const NSString* kBody            = @"body";	
static const NSString* kSubject         = @"subject";	
static const NSString* kSyncingNew      = @"syncingNew";
static const NSString* kHasAttachment   = @"hasAttachment";
static const NSString* kHasMeeting      = @"hasMeeting";
static const NSString* kFlags           = @"flags";

// Search Results Keys
static const NSString* kRows            = @"rows";
static const NSString* kData            = @"data";
static const NSString* kEmail           = @"email";


#warning HACK
// Hacks
static const NSUInteger kIsRead			= 0x00000002;
static const NSUInteger kIsMarked		= 0x00000004;
#endif

#endif
