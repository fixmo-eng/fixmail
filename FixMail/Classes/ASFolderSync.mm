/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASFolderSync.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASFolderSync

// Properties
@synthesize delegate;
@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"FolderSync";
static const ASTag kCommandTag  = FOLDER_FOLDER_SYNC;

static const NSUInteger kMaxRetries = 16;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASFolderSync*)queueRequest:(ASAccount*)anAccount
                displayErrors:(BOOL)aDisplayErrors
{
    ASFolderSync* aFolderSync = [[ASFolderSync alloc] initWithAccount:anAccount];
    aFolderSync.displayErrors = aDisplayErrors;
    [aFolderSync queue];
    
    return aFolderSync;
}

- (void)send
{
    NSData* aWbxml = [self wbxmlFolderSync:self.account.syncKey];
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlFolderSync:(NSString*)aSyncKey
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.keyValue(FOLDER_SYNC_KEY, aSyncKey);
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
        folders = [[NSMutableArray alloc] initWithCapacity:12];
    }
    return self;
}

- (void)dealloc
{
    self.delegate = NULL;

}
  
////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
	case kFolderSyncStatusOK:
			break;
	case kFolderSyncStatusServerError:
            if(self.retries < kMaxRetries) {
                [self retry];
            }else{
                if([self.account.syncKey isEqualToString:kInitialSyncKey]) {
#ifdef DEBUG
					[HttpRequest handleASError:@"FolderSync server error"];
#else
					[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"FOLDER_SYNC_ERROR", @"ErrorAlert", @"Error alert message for Folder Sync error"), self.statusCodeAS]];
#endif
                    
                }else{
                    self.account.syncKey = kInitialSyncKey;
                    self.retries = 0;
                    [self retry];
                }
            }
			break;
	case kFolderSyncStatusInvalidKey:
		FXDebugLog(kFXLogActiveSync, @"Folder Sync invalid key, resyncing");
		if(delegate) {
			[delegate needsFolderResync];
		}
		break;
#ifdef DEBUG
    case kFolderSyncStatusUnknown:
        [HttpRequest handleASError:@"FolderSync status unknown"];
        break;
    case kFolderSyncStatusExists:           // Obsolete?
        [HttpRequest handleASError:@"FolderSync folder exists"];
        break;
    case kFolderSyncStatusSpecialFolder:    // Obsolete?
        [HttpRequest handleASError:@"Folder Sync special folder"];
        break;
    case kFolderSyncStatusDoesNotExist:     // Obsolete?
        [HttpRequest handleASError:@"Folder Sync does not exit"];
        break;
    case kFolderSyncStatusParentNotFound:   // Obsolete?
        [HttpRequest handleASError:@"Folder Sync parent not found"];
        break;
    case kFolderSyncStatusIncorrectFormat:
        [HttpRequest handleASError:@"Folder Sync incorrect formatr"];
        break;
    case kFolderSyncStatusUnknownError:
        [HttpRequest handleASError:@"Folder Sync unknown error"];
        break;
    case kFolderSyncStatusCodeUnknown:
        [HttpRequest handleASError:@"Folder Sync unknown code"];
        break;
    default:
        [self handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS];
        break;
#else
	default:
		[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"FOLDER_SYNC_ERROR", @"ErrorAlert", @"Error alert message for Folder Sync error"), self.statusCodeAS]];
		break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)_deleteParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:FOLDER_CHANGES]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case FOLDER_SERVER_ID:
                if(delegate) {
                    [delegate folderDeleted:[aParser getString]];
                }
                break;
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)_updateParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:FOLDER_CHANGES]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case FOLDER_SERVER_ID:
            {
                ASFolder* aFolder = (ASFolder*)[self.account folderForServerId:[aParser getString]];
                if(aFolder) {
                    [aFolder parser:aParser];
                    if(delegate) {
                        [delegate folderUpdated:aFolder];
                    }            
                }
                break;
            }
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)changesParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:FOLDER_CHANGES]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case FOLDER_ADD:
            {
                // Create a generic folder to run parser until we figure out what kind of folder
                // this is
                //
                ASFolder* aFolder = [[ASFolder alloc] initWithAccount:self.account];
                [aFolder parser:aParser];
                
                // Create a dictionary from the generic folder
                //
                NSMutableDictionary* aDictionary = [[NSMutableDictionary alloc] init];
                [aFolder folderAsDictionary:aDictionary];

                // Create a folder of the appropriate type and populate it from the generic folder's dictionary
                //
                ASFolder* aTypedFolder = (ASFolder*)[self.account createFolderForFolderType:aFolder.folderType];
                [aTypedFolder loadFromStore:aDictionary];
                
                [folders addObject:aTypedFolder]; 
                if(delegate) {
                    [delegate folderAdded:aTypedFolder];
                }
                break;
            }
            case FOLDER_DELETE:
                [self _deleteParser:aParser]; break;
            case FOLDER_UPDATE:
                [self _updateParser:aParser]; break;
            case FOLDER_COUNT:
                [aParser getInt]; break;
            default:
                [aParser skipTag]; break;
        }
    }
}
             
- (void)fastParser:(FastWBXMLParser*)aParser
{        
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case FOLDER_STATUS:
                    self.statusCodeAS = (EFolderSyncStatus)[aParser getInt];
                    break;
                case FOLDER_SYNC_KEY:
                    if(delegate) {
                        [delegate setFolderSyncKey:[aParser getString]];
                    }
                    break;
                case FOLDER_CHANGES:
                    [self changesParser:aParser]; break;
                default:
                    [aParser skipTag]; break;
            }
        } 
    }@catch (NSException* e) {
        [HttpRequest logException:@"FolderSync response" exception:e];
    }
    
    if( self.statusCodeAS == kFolderSyncStatusOK) {
        if(delegate) {
            [delegate folderSyncSuccess:folders];
        }
        [self success];
    }else if( self.statusCodeAS == kASPolicyRefresh) {
    }else{
        if(delegate) {
            [delegate folderSyncFailed:self.statusCode folderSyncStatus:self.statusCodeAS];
        }
        [self fail];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
    
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        self.contentType = [aHeaders objectForKey:@"Content-Type"];
        if([self.contentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", kCommand);
            [delegate folderSyncFailed:self.statusCode folderSyncStatus:kFolderSyncStatusServerError];
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
        [delegate folderSyncFailed:self.statusCode folderSyncStatus:kFolderSyncStatusUnknown];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end
