 //
//  EmailProcessor.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 6/29/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "EmailProcessor.h"
#import "AddEmailDBAccessor.h"
#import "ASEmail.h"
#import "ASEmailChange.h"
#import "ASEmailFolder.h"
#import "ASEmailThread.h"
#import "ASAccount.h"
#import "BaseAccount.h"
#import "BaseFolder.h"
#import "CommitObjects.h"
#import "ErrorAlert.h"
#import "FXDatabase.h"
#import "MeetingRequest.h"
#import "SearchRunner.h"
#import "SearchEmailDBAccessor.h"
#import "StringUtil.h"
#import "SyncManager.h"

#include "sqlite3.h"

// Defines
#define SECONDS_PER_DAY         86400.0 //24*3600
#define FOLDER_COUNT_LIMIT      1000 // maximum number of folders allowed
#define EMAIL_DB_COUNT_LIMIT    500
#define ADDS_PER_TRANSACTION    20
#define BODY_LENGTH_LIMIT       30000

// Static

static NSInteger kEmailDatabaseVersion              = 3;
static NSDateFormatter* sDateFormatter              = NULL;

static sqlite3_stmt *emailStmt                      = nil;
static sqlite3_stmt *emailCountStmt                 = nil;
static sqlite3_stmt *searchEmailInsertStmt          = nil;
static sqlite3_stmt *emailUpdateBodyAndFlagsStmt    = nil;
static sqlite3_stmt *emaiUpdateFlagsStmt            = nil;
static sqlite3_stmt *emaiUpdateMeetingRequestStmt   = nil;
static sqlite3_stmt *emailFindUidStmt               = nil;

static sqlite3_stmt *folderUpdateReadStmt           = nil;
static sqlite3_stmt *folderUpdateWriteStmt          = nil;
static sqlite3_stmt *folderDeleteStmt               = nil;

static BOOL transactionOpen                         = NO;

#ifdef DEBUG
static const BOOL    kDebugAccount  = FALSE;
static const BOOL    kDebugEvent    = FALSE;
static const BOOL    kDebugDatabase = FALSE;
static const BOOL    kDebugSearch   = FALSE;
static const BOOL    kDebugContacts = FALSE;
static const BOOL    kDebugUid      = FALSE;
#else
static const BOOL    kDebugAccount  = FALSE;
static const BOOL    kDebugEvent    = FALSE;
static const BOOL    kDebugDatabase = FALSE;
static const BOOL    kDebugSearch   = FALSE;
static const BOOL    kDebugContacts = FALSE;
static const BOOL    kDebugUid      = FALSE;
#endif

@implementation EmailProcessor

// Properties
@synthesize operationQueue;
@synthesize updateSubscriber;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static EmailProcessor *singleton = nil;

+ (id)getSingleton 
{ 
    //NOTE: don't get an instance of SyncManager until account settings are set!
    //
	@synchronized(self) {
		if (singleton == nil) {
			singleton = [[self alloc] init];
		}
	}
	return singleton;
}

+ (NSDateFormatter*)dateFormatter
{
    if(!sDateFormatter) {
        // This is Zulu (GMT) with millisecond resolution
        //
		// DateFormatter for AS Email processing
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sDateFormatter = [[NSDateFormatter alloc] init];
        [sDateFormatter setDateFormat:@"yyyyMMdd'T'HHmmssSSS'Z'"];
        [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sDateFormatter;
}

+ (BOOL)debugAccount
{
    return kDebugAccount;
}

+ (BOOL)debugEvent
{
    return kDebugEvent;
}

+ (BOOL)debugUid
{
    return kDebugUid;
}

+ (BOOL)debugDatabase
{
    return kDebugDatabase;
}

+ (BOOL)debugSearch
{
    return kDebugSearch;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init 
{
	self = [super init];
	
	if(self) {
        // Operation Queue
        //
		NSOperationQueue *ops = [[NSOperationQueue alloc] init];
		[ops setMaxConcurrentOperationCount:1]; // note that this makes it a simple, single queue
		self.operationQueue     = ops;
        
        // Formatter for date stored in the database
        //
        [EmailProcessor dateFormatter];
		
		transactionOpen         = NO;
	}
	
	return self;
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EMAIL_DATABASE", @"ErrorAlert", @"Error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EMAIL_DATABASE", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// DBaseEngine Virtual overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Virtual overrides

- (void)open:(BaseAccount*)anAccount
{
    sqlite3* aDatabase = nil;
    
    AddEmailDBAccessor* aDBAccessor = [AddEmailDBAccessor sharedManager];
    if([aDBAccessor isOpen]) {
    }else{
        aDatabase = [aDBAccessor database];
        if(aDatabase) {
            int aDatabaseVersion = [aDBAccessor userVersion];
            if(aDatabaseVersion < kEmailDatabaseVersion) {
                if(aDatabaseVersion > 0) {
                    [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"EMAIL_DATABASE_OUT_OF_DATE._WAS_V.%d,_NOW_V.%d", @"ErrorAlert", @"Error alert view message when email database out of date"),
                                       aDatabaseVersion, kEmailDatabaseVersion]];
                    NSMutableArray* aFolders = [anAccount foldersForFolderType:kFolderTypeInbox];
                    ASAccount* anASAccount = (ASAccount*)anAccount;
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeUser];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeDrafts];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeDeleted];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeOutbox];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeTasks];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeUserMailbox];
                    [anASAccount addFoldersToArray:aFolders ofFolderType:kFolderTypeNotes];

                    for(ASFolder* aFolder in aFolders) {
                        [aFolder setNeedsInitialSync];
                    }
                    [self deleteAccount];
                }
                [aDBAccessor setUserVersion:kEmailDatabaseVersion];
            }

            NSInvocationOperation* nextOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(tableCheck) object:nil];
            [self.operationQueue addOperation:nextOp];
                        
            // Reset shutdown flags, this is necessary if you delete an account and immediately provision another one
            //
            [[SearchRunner getSingleton] setCancelled:NO];
        }else{
            [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EMAIL_DATABASE_OPEN_FAILED:", @"ErrorAlert", @"Error alert view message when email database open fails"), [aDBAccessor databaseFilepath]]];
            return;
        }
    }
}

- (void)tableCheck
{
    [ASEmail tableCheck];
}

- (unsigned long long)spaceUsed
{
    unsigned long long aSpaceUsed = 0;
    
    NSString* aDBasePath = [[AddEmailDBAccessor sharedManager] databaseFilepath];
    aSpaceUsed += [DBaseEngine sizeForPath:aDBasePath];
    
    return aSpaceUsed;
}

- (void)shutDown
{
    [[SearchRunner getSingleton] cancel];
    
    // Cancel all search and counting operations.  Must let all commit and update ops complete
    //
    NSArray* anOps = self.operationQueue.operations;
    for(NSInvocationOperation* anOp in anOps) {
        NSInvocation* anInvocation = anOp.invocation;
        if(anInvocation.selector       == @selector(commitObjects:)) {
        }else if(anInvocation.selector == @selector(_updateBodyAndFlags:)) {
        }else if(anInvocation.selector == @selector(_updateFlags:)) {
        }else if(anInvocation.selector == @selector(_updateFlagsForEmails:)) {
        }else if(anInvocation.selector == @selector(_updateMeetingRequest:)) {
        }else{
            [anOp cancel];
        }
    }
    [self.operationQueue waitUntilAllOperationsAreFinished];
    
    // Clear SearchRunner queries
    //
    [SearchRunner clearPreparedStmts];
    [[SearchEmailDBAccessor sharedManager] close];
    
    // Clear email processor queries
    //
    [EmailProcessor clearPreparedStmts];
    [[AddEmailDBAccessor sharedManager] close];
}

- (void)deleteAccount
{
    [self shutDown];
    NSString* aFilePath = [[AddEmailDBAccessor sharedManager] databaseFilepath];
    [[NSFileManager defaultManager] removeItemAtPath:aFilePath error:nil];
}

- (NSString*)displayName
{
    return @"Email";
}

- (void)queueCommitObjects:(CommitObjects *)aCommitObjects
{
    NSInvocationOperation* nextOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(commitObjects:) object:aCommitObjects];
    [self.operationQueue addOperation:nextOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (void)clearPreparedStmts 
{
    // Email
	if(emailStmt) {
		sqlite3_finalize(emailStmt);
		emailStmt = nil;
	}
	if(emailCountStmt) {
		sqlite3_finalize(emailCountStmt);
		emailCountStmt = nil;
	}
    
    // Search
	if(searchEmailInsertStmt) {
		sqlite3_finalize(searchEmailInsertStmt);
		searchEmailInsertStmt = nil;
	}
    if(emailUpdateBodyAndFlagsStmt) {
		sqlite3_finalize(emailUpdateBodyAndFlagsStmt);
		emailUpdateBodyAndFlagsStmt = nil;
	}
    if(emaiUpdateFlagsStmt) {
		sqlite3_finalize(emaiUpdateFlagsStmt);
		emaiUpdateFlagsStmt = nil;
	}
    if(emaiUpdateMeetingRequestStmt) {
		sqlite3_finalize(emaiUpdateMeetingRequestStmt);
		emaiUpdateMeetingRequestStmt = nil;
	}
    if(emailFindUidStmt) {
		sqlite3_finalize(emailFindUidStmt);
		emailFindUidStmt = nil;
	}
	
    // Folder
	if(folderUpdateReadStmt) {
		sqlite3_finalize(folderUpdateReadStmt);
		folderUpdateReadStmt = nil;
	}
	if(folderUpdateWriteStmt) {
		sqlite3_finalize(folderUpdateWriteStmt);
		folderUpdateWriteStmt = nil;
	}
    if(folderDeleteStmt) {
		sqlite3_finalize(folderDeleteStmt);
		folderDeleteStmt = nil;
	}
}

+ (int)folderCountLimit 
{
	return FOLDER_COUNT_LIMIT;
}

// the folder num that's in the db combines the account num with the folder num (this was easier than changing the schema)
+ (int)combinedFolderNumFor:(int)folderNumInAccount withAccount:(int)accountNum 
{
	return accountNum * FOLDER_COUNT_LIMIT + folderNumInAccount;
}

+ (int)folderNumForCombinedFolderNum:(int)folderNum 
{
	return folderNum % FOLDER_COUNT_LIMIT;
}

+ (int)accountNumForCombinedFolderNum:(int)folderNum 
{
	return folderNum / FOLDER_COUNT_LIMIT;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Transactions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Transactions

- (void)beginTransactions 
{
    if(!transactionOpen) {
        if(kDebugDatabase) {
            FXDebugLog(kFXLogActiveSync, @"beginTransactions open=%d", transactionOpen);
        }
        if(![AddEmailDBAccessor beginTransaction]) {
            [self handleError:FXLLocalizedStringFromTable(@"BEGIN_TRANSACTION_WAS_NOT_SUCCESSFUL", @"ErrorAlert", @"Error alert message when begin transaction failed")];
        }
        
        transactionOpen     = YES;
        opsInTransaction   = 0;
    }else{
        FXDebugLog(kFXLogActiveSync, @"beginTransactions already open");
    }
}

- (void)endTransactions 
{
	if(transactionOpen) {
        if(!opsInTransaction) {
            FXDebugLog(kFXLogActiveSync, @"endTransactions there were no adds"); 
        }
        if(kDebugDatabase) {
            FXDebugLog(kFXLogActiveSync, @"endTransactions");
        }
		if(![AddEmailDBAccessor endTransaction]) {
            [self handleError:FXLLocalizedStringFromTable(@"END_TRANSACTION_WAS_NOT_SUCCESSFUL", @"ErrorAlert", @"Error alert message when end transaction failed")];
		}

        transactionOpen = NO;	   
	}else{
        FXDebugLog(kFXLogActiveSync, @"endTransactions already closed");
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Commit changes to database
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Commit changes to database

- (void)commitObjects:(CommitObjects*)aCommitObjects
{
    BaseFolder* aFolder = aCommitObjects.folder;
    
    // Objects Added
    //
    NSArray* anObjectsAdded = aCommitObjects.objectsAdded;
    if(anObjectsAdded.count > 0) {
        [self commitEmails:anObjectsAdded folder:aFolder];
    }
    
    // Objects Changed
    //
    NSArray* anObjectsChanged = aCommitObjects.objectsChanged;
    if(anObjectsChanged.count > 0) {
        [aFolder commitChanges:anObjectsChanged];
    }
    
    // Objects Deleted
    //
    NSArray* anObjectsDeleted = aCommitObjects.objectsDeleted;
    if(anObjectsDeleted.count > 0) {
        [self deleteEmails:anObjectsDeleted folder:aFolder];
    }
    
    if(aCommitObjects.target && aCommitObjects.action) {
        [aCommitObjects.target performSelectorOnMainThread:aCommitObjects.action withObject:aCommitObjects waitUntilDone:FALSE];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Add To Folder
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Add To Folder

- (void)_updateFolders:(int)pk uid:(NSString*)aUidString
           folderNum0:(int)aFolderNum0 folderNum1:(int)aFolderNum1 folderNum2:(int)aFolderNum2 folderNum3:(int)aFolderNum3
{
    // Email is still in at least one folder
    //
    if(folderUpdateWriteStmt == nil) {
        NSString *updateEmail = @"UPDATE email SET folder_num = ?, folder_num_1 = ?, folder_num_2 = ?, folder_num_3 = ? WHERE uid = ?;";
        int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [updateEmail UTF8String], -1, &folderUpdateWriteStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"folderUpdateWriteStmt",sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
            return;
        }
    }
    
    // write out folder0-4
    //
    sqlite3_bind_int(folderUpdateWriteStmt,  1, aFolderNum0);
    sqlite3_bind_int(folderUpdateWriteStmt,  2, aFolderNum1);
    sqlite3_bind_int(folderUpdateWriteStmt,  3, aFolderNum2);
    sqlite3_bind_int(folderUpdateWriteStmt,  4, aFolderNum3);
    sqlite3_bind_text(folderUpdateWriteStmt, 5, [aUidString UTF8String], -1, SQLITE_TRANSIENT);
    
    int result = sqlite3_step(folderUpdateWriteStmt);
    if(result != SQLITE_DONE) {
        if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            [self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"_updateFolders emailStmt", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }else{
            [self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"_updateFolders emailStmt", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }
    }else if(kDebugDatabase) {
        FXDebugLog(kFXLogActiveSync, @"UPDATE email SET folder_num: pk=%d uid %@ folders %d %d %d %d\n%@",
                 pk, aUidString,
                 aFolderNum0, aFolderNum1,aFolderNum2, aFolderNum3, self);
    }
	
    sqlite3_reset(folderUpdateWriteStmt);
}

- (int)_addToFolder:(int)newFolderNum uid:(NSString*)aUidString
{
	// Get  folder numbers for folders this email was in before this request
    //
	if(folderUpdateReadStmt == nil) {
        NSString* readEmail = @"SELECT folder_num, folder_num_1, folder_num_2, folder_num_3 FROM email WHERE uid = ? LIMIT 1";
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [readEmail UTF8String], -1, &folderUpdateReadStmt, nil);	
		if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"_addToFolder", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return 0;
		}
	}
	
	sqlite3_bind_text(folderUpdateReadStmt, 1, [aUidString UTF8String], -1, SQLITE_TRANSIENT);		

	int pk = 0;
	int folder_num_0 = 0;
	int folder_num_1 = 0;
	int folder_num_2 = 0;
	int folder_num_3 = 0;
	
	if (sqlite3_step(folderUpdateReadStmt) == SQLITE_ROW) {
		folder_num_0 = sqlite3_column_type(folderUpdateReadStmt, 0) == SQLITE_NULL ? -1 : sqlite3_column_int(folderUpdateReadStmt, 0);
		folder_num_1 = sqlite3_column_type(folderUpdateReadStmt, 1) == SQLITE_NULL ? -1 : sqlite3_column_int(folderUpdateReadStmt, 1);
		folder_num_2 = sqlite3_column_type(folderUpdateReadStmt, 2) == SQLITE_NULL ? -1 : sqlite3_column_int(folderUpdateReadStmt, 2);
		folder_num_3 = sqlite3_column_type(folderUpdateReadStmt, 3) == SQLITE_NULL ? -1 : sqlite3_column_int(folderUpdateReadStmt, 3);
	} else {
		sqlite3_reset(folderUpdateReadStmt);
		return pk;
	}
	
	sqlite3_reset(folderUpdateReadStmt);
	
	// is this folder already set?
	if((folder_num_0 == newFolderNum) || (folder_num_1 == newFolderNum) || (folder_num_2 == newFolderNum) || (folder_num_3 == newFolderNum)) {
		return pk;
	}
	
	// find out the setting that folder0-4 need to be
	if(folder_num_0 == -1) {
		folder_num_0 = newFolderNum;
	} else if (folder_num_1 == -1) {
		folder_num_1 = newFolderNum;
	} else if (folder_num_2 == -1) {
		folder_num_2 = newFolderNum;
	} else if (folder_num_3 == -1) {
		folder_num_3 = newFolderNum;
	} else {
        FXDebugLog(kFXLogFIXME, @"FIXME email appears in > 4 folders -> ignore");
		return pk;
	}
	
    [self _updateFolders:pk uid:aUidString folderNum0:folder_num_0 folderNum1:folder_num_1 folderNum2:folder_num_2 folderNum3:folder_num_3];

    return pk;
}

- (void)addEmailToFolder:(ASEmail*)anEmail folder:(BaseFolder*)aFolder
{
    [self _addToFolder:aFolder.folderNum uid:anEmail.uid];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Delete From Folder
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Delete From Folder

- (int)_deleteFromFolder:(int)aFolderNum uid:(NSString*)aUidString
{
	// Get  folder numbers for folders this email was in before this request
    //
	if(folderDeleteStmt == nil) {
        NSString* readEmail = @"SELECT folder_num, folder_num_1, folder_num_2, folder_num_3 FROM email WHERE uid = ? LIMIT 1";
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [readEmail UTF8String], -1, &folderDeleteStmt, nil);
		if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"_deleteFromFolder", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return 0;
		}
	}
	
	sqlite3_bind_text(folderDeleteStmt, 1, [aUidString UTF8String], -1, SQLITE_TRANSIENT);
    
	int pk = 0;
	int folder_num_0 = 0;
	int folder_num_1 = 0;
	int folder_num_2 = 0;
	int folder_num_3 = 0;
	
	if (sqlite3_step(folderDeleteStmt) == SQLITE_ROW) {
        folder_num_0 = sqlite3_column_type(folderDeleteStmt, 0) == SQLITE_NULL ? -1 : sqlite3_column_int(folderDeleteStmt, 0);
		folder_num_1 = sqlite3_column_type(folderDeleteStmt, 1) == SQLITE_NULL ? -1 : sqlite3_column_int(folderDeleteStmt, 1);
		folder_num_2 = sqlite3_column_type(folderDeleteStmt, 2) == SQLITE_NULL ? -1 : sqlite3_column_int(folderDeleteStmt, 2);
		folder_num_3 = sqlite3_column_type(folderDeleteStmt, 3) == SQLITE_NULL ? -1 : sqlite3_column_int(folderDeleteStmt, 3);
	} else {
		sqlite3_reset(folderDeleteStmt);
		return pk;
	}
	
	sqlite3_reset(folderDeleteStmt);
	
	// The new folder number shouldn't be in this entry
    //
	if((folder_num_0 == aFolderNum) || (folder_num_1 == aFolderNum) || (folder_num_2 == aFolderNum) || (folder_num_3 == aFolderNum)) {
    }else{
        FXDebugLog(kFXLogActiveSync, @"_deleteFromFolder failed email already in folder: %d %@", aFolderNum, aUidString); 
		return pk;
	}
	
	// find out the setting that folder0-4 need to be
	if(folder_num_0 == aFolderNum) {
		folder_num_0 = folder_num_1;
        folder_num_1 = folder_num_2;
        folder_num_2 = folder_num_3;
        folder_num_3 = -1;
	} else if (folder_num_1 == aFolderNum) {
        folder_num_1 = folder_num_2;
        folder_num_2 = folder_num_3;
        folder_num_3 = -1;
	} else if (folder_num_2 == aFolderNum) {
        folder_num_2 = folder_num_3;
        folder_num_3 = -1;
	} else if (folder_num_3 == aFolderNum) {
		folder_num_3 = -1;
	} else {
		return pk;
	}

    if(folder_num_0 == -1) {
        // email is deleted from all folders
        //
        [ASEmail deleteWithUid:aUidString];
    }else{
        [self _updateFolders:pk uid:aUidString folderNum0:folder_num_0 folderNum1:folder_num_1 folderNum2:folder_num_2 folderNum3:folder_num_3];
    }
    opsInTransaction++;

    return pk;
}

- (void)deleteEmails:(NSArray*)aUidStrings folder:(BaseFolder*)aFolder
{
    if(aUidStrings.count > 0) {
        [self beginTransactions];
        for(NSString* aUidString in aUidStrings) {
            [self _deleteFromFolder:aFolder.folderNum uid:aUidString];
        }
        [self endTransactions];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Add EMail
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Add EMail

- (BOOL)findUid:(NSString*)aUid
{
    if(emailFindUidStmt == nil) {
        NSString* searchForUidEntry = @"SELECT folder_num, folder_num_1, folder_num_2, folder_num_3 FROM email WHERE uid = ? LIMIT 1;";
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [searchForUidEntry UTF8String], -1, &emailFindUidStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), @"findUid",  sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return FALSE;
		}
	}
    
    sqlite3_bind_text(emailFindUidStmt, 1, [aUid UTF8String], -1, SQLITE_TRANSIENT);
    
    BOOL aFoundUid;
    if (sqlite3_step(emailFindUidStmt) == SQLITE_ROW) {
        aFoundUid = TRUE;
        //int folderNum   = sqlite3_column_int(emailFindUidStmt, 0);
        //int folderNum_1 = sqlite3_column_int(emailFindUidStmt, 1);
        //int folderNum_2 = sqlite3_column_int(emailFindUidStmt, 2);
        //int folderNum_3 = sqlite3_column_int(emailFindUidStmt, 3);
    }else{
        aFoundUid = FALSE;
    }
    
    sqlite3_reset(emailFindUidStmt);

    return aFoundUid;
}

- (void)commitEmails:(NSArray *)anEmails folder:(BaseFolder*)aFolder
{
    sqlite3* aDatabase = nil;
    
    AddEmailDBAccessor* aDBAccessor = [AddEmailDBAccessor sharedManager];
    if([aDBAccessor isOpen]) {
        aDatabase = [aDBAccessor database];
    }else{
        aDatabase = [aDBAccessor database];
        if(aDatabase) {
            int aDatabaseVersion = [aDBAccessor userVersion];
            if(aDatabaseVersion < kEmailDatabaseVersion) {
                [aDBAccessor setUserVersion:kEmailDatabaseVersion];
                if(aDatabaseVersion > 0) {
                    [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"EMAIL_DATABASE_OUT_OF_DATE._WAS_V.%d,_NOW_V.%d", @"ErrorAlert", @"Error alert view message when email database out of date"),
                            aDatabaseVersion, kEmailDatabaseVersion]];
                }
            }
            [ASEmail tableCheck];
        }else{
            [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"DATABASE_OPEN_FAILED:", @"ErrorAlert", @"Error alert view message when email database open failed"), [aDBAccessor databaseFilepath]]];
            return;
        }
    }
    
    NSMutableArray* anAddArray = [[NSMutableArray alloc] initWithCapacity:anEmails.count];
    if([aFolder.account emailCanBeInMultipleFolders]) {
        // GMail typically has an email in multiple folders which requires additional complexity
        //
        // See if email already exists and if so, assume this is the same email in a different folder
        //
        BOOL didAddToFolder = FALSE;
        for(ASEmail* anEmail in anEmails) {
            if([self findUid:anEmail.uid]) {
                [self addEmailToFolder:anEmail folder:aFolder];
                didAddToFolder = TRUE;
            }else{
                [anAddArray addObject:anEmail];
            }
        }
        if(didAddToFolder) {
            [self endTransactions];
        }
    }else{
        // Exchange typically doesn't allow an email to be in multiple folders so its a simpler case
        //
        [anAddArray addObjectsFromArray:anEmails];
    }
    
    // Insert apparently new emails in to email and contact databases
    //
    if(anAddArray.count > 0) {
        [self beginTransactions];
        for(ASEmail* anEmail in anAddArray) {
            @try {   
                //FXDebugLog(kFXLogActiveSync, @"addEmail:\n%@", anEmail);
                BaseFolder* aFolder = anEmail.folder;
                int aFolderNum = [aFolder folderNum];

                // Date Time
                //
                NSDate* date = [anEmail datetime];
                NSString* dateTimeString = [sDateFormatter stringFromDate:date];
                        
                // Sender
                //
                //NSString *senderName    = [anEmail from];
                //NSString *senderAddress = [anEmail replyTo];
                
                // Recipients
                //
                NSString* toJson        = anEmail.toJSON;
                NSString* ccJson        = anEmail.ccJSON;
                NSString* bccJson       = anEmail.bccJSON;

                // Attachments
                //
                NSString* anAttachmentJSON = @"";
                if(anEmail.attachments.count > 0) {
                    anAttachmentJSON = [anEmail attachmentsAsJSON];
                }
                
                // Meeting Request
                NSString* aMeetingJSON = @"";
                if(anEmail.meetingRequest) {
                    aMeetingJSON = [anEmail.meetingRequest asJSON];
                }
                
                // Draft Reference Mail or Threading (if its implemented some day)
                NSString* anEmailThreadJSON = @"";
                if(anEmail.emailThread) {
                    anEmailThreadJSON = [anEmail.emailThread asJSON];
                }
                
                // Insert into email database
                //
                if(emailStmt == nil) {
                    NSString *updateEmail =
                    @"INSERT OR REPLACE INTO email("
                                "uid, "
                                "datetime, "
                                "sender_name, "
                                "sender_address, "
                                "tos, "
                                "ccs, "
                                "bccs, "
                                "attachments, "
                                "meeting, "
                                "estimatedSize, "
                                "folder_num, "
                                "subject, "
                                "flags, "
                                "messageClass, "
                                "preview, "
                                "thread, "
                                "body) "
                        "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                    int dbrc = sqlite3_prepare_v2(aDatabase, [updateEmail UTF8String], -1, &emailStmt, nil);
                    if (dbrc != SQLITE_OK) {
                        [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), FXLLocalizedStringFromTable(@"INSERT_INTO_email_FAIL:", @"ErrorAlert", @"Error alert view message when insert into email failed"), sqlite3_errmsg(aDatabase)]];
                        continue;
                    }
                }
                sqlite3_bind_text(emailStmt, 1,     [anEmail.uid UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 2,     [dateTimeString UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 3,     [anEmail.from UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 4,     [anEmail.replyTo UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 5,     [toJson UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 6,     [ccJson UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 7,     [bccJson UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 8,     [anAttachmentJSON UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 9,     [aMeetingJSON UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_int(emailStmt,  10,    anEmail.estimatedSize);
                sqlite3_bind_int(emailStmt,  11,    aFolderNum);

                if(anEmail.subject) {
                    sqlite3_bind_text(emailStmt, 12,    [anEmail.subject UTF8String], -1, SQLITE_TRANSIENT);
                }else{
                    sqlite3_bind_text(emailStmt, 12,    "", 0, NULL);
                }
                
                sqlite3_bind_int(emailStmt,  13,    [anEmail flagsToStore]);
                sqlite3_bind_int(emailStmt,  14,    anEmail.messageClass);

                sqlite3_bind_text(emailStmt, 15,    [anEmail.preview UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(emailStmt, 16,    [anEmailThreadJSON UTF8String], -1, SQLITE_TRANSIENT);

                if(anEmail.body.length > 0) {
                    NSData* aData = anEmail.body;
                    sqlite3_bind_text(emailStmt, 17, [aData bytes], [aData length], SQLITE_TRANSIENT);
                }else{
                    sqlite3_bind_text(emailStmt, 17,    "", 0, NULL);
                }
                
                int rc = sqlite3_step(emailStmt);
                if (rc != SQLITE_DONE)	{
                    if (rc == SQLITE_BUSY || rc == SQLITE_LOCKED) {
                        [self handleError:[NSString stringWithFormat:@"%@ '%s':\n%@", FXLLocalizedStringFromTable(@"INSERT_INTO_email_FAIL:", @"ErrorAlert", nil), sqlite3_errmsg(aDatabase), anEmail]];
                    }else{
                        [self handleError:[NSString stringWithFormat:@"%@ '%s':\n%@", FXLLocalizedStringFromTable(@"INSERT_INTO_email_FAIL:", @"ErrorAlert", nil), sqlite3_errmsg(aDatabase), anEmail]];
                    }
                }else if(kDebugDatabase) {
                    FXDebugLog(kFXLogActiveSync, @"INSERT INTO email        %@ fn=%d %@  %@ \"%@\"", [[anEmail folder] displayName], aFolderNum, [anEmail uid], [anEmail from], [anEmail subject]);
                }
                opsInTransaction++;

                sqlite3_reset(emailStmt);
                
                // Add email address to contact database
                //
				[FXDatabase addContactWithEmail:anEmail.replyTo name:anEmail.from contact:contactEmailRecipients];

            }@catch (NSException* e) {
                [self _logException:@"commitEmail" exception:e];
            }
        }
        [self endTransactions];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Updaters body and or flags
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Updaters body and or flags

- (void)_updateBodyAndFlags:(ASEmail*)anEmail
{
	[self beginTransactions];

    if(emailUpdateBodyAndFlagsStmt == nil) {
		NSString* updateStmt = @"UPDATE email SET body=?, flags=? WHERE uid=?;";
		
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [updateStmt UTF8String], -1, &emailUpdateBodyAndFlagsStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert view message when specific method failed with specified error message"), @"updateBodyAndFlags",  sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return;
		}
	}
	
    NSData* aData = anEmail.body;
    
	sqlite3_bind_text(emailUpdateBodyAndFlagsStmt, 1, [aData bytes], [aData length], SQLITE_TRANSIENT);
    sqlite3_bind_int(emailUpdateBodyAndFlagsStmt,  2, [anEmail flagsToStore]);
	sqlite3_bind_text(emailUpdateBodyAndFlagsStmt, 3, [anEmail.uid UTF8String], [anEmail.uid length], SQLITE_TRANSIENT);
	
	int result = sqlite3_step(emailUpdateBodyAndFlagsStmt);
    if(result != SQLITE_DONE) {
        if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            [self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"updateBodyAndFlags", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }else{
            [self handleError:[NSString stringWithFormat: FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"updateBodyAndFlags", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }
	}else if(kDebugDatabase) {
        FXDebugLog(kFXLogActiveSync, @"UPDATE search_email %d bytes flags = 0x%x", [aData length], [anEmail flagsToStore]);
    }
    opsInTransaction++;
	
	sqlite3_reset(emailUpdateBodyAndFlagsStmt);
    [self endTransactions];
}

- (void)_doUpdateFlags:(ASEmail*)anEmail
{
    if(emaiUpdateFlagsStmt == nil) {
		NSString* updateStmt = @"UPDATE email SET flags=? WHERE uid=?;";
		
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [updateStmt UTF8String], -1, &emaiUpdateFlagsStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), @"updateFlags", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return;
		}
	}
    
    sqlite3_bind_int(emaiUpdateFlagsStmt,  1, anEmail.flagsToStore);
	sqlite3_bind_text(emaiUpdateFlagsStmt, 2, [anEmail.uid UTF8String], [anEmail.uid length], SQLITE_TRANSIENT);
	
	int result = sqlite3_step(emaiUpdateFlagsStmt);
    if(result != SQLITE_DONE) {
        if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", nil), @"updateFlags", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }else{
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", nil), @"updateFlags", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }
	}else if(kDebugDatabase) {
        FXDebugLog(kFXLogActiveSync, @"UPDATE search_email flags = 0x%x", [anEmail flagsToStore]);
    }
    opsInTransaction++;
	
	sqlite3_reset(emaiUpdateFlagsStmt);
}

- (void)_updateFlags:(ASEmail*)anEmail
{
    [self beginTransactions];
    [self _doUpdateFlags:anEmail];
    [self endTransactions];
}

- (void)_updateFlagsForEmails:(NSArray*)anEmails
{
    [self beginTransactions];
    for(ASEmail* anEmail in anEmails) {
        [self _doUpdateFlags:anEmail];
    }
    [self endTransactions];
}

- (void)queueUpdateBodyAndFlags:(ASEmail*)anEmail
{
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_updateBodyAndFlags:) object:anEmail];
    [self.operationQueue addOperation:anOp];
}

- (void)queueUpdateFlags:(ASEmail*)anEmail
{
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_updateFlags:) object:anEmail];
    [self.operationQueue addOperation:anOp];
}

- (void)queueUpdateFlagsForEmails:(NSArray*)anEmails
{
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_updateFlagsForEmails:) object:anEmails];
    [self.operationQueue addOperation:anOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Updater meeting request
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Updater meeting request

- (void)_updateMeetingRequest:(ASEmail*)anEmail
{
	[self beginTransactions];

    if(emaiUpdateMeetingRequestStmt == nil) {
		NSString* updateStmt = @"UPDATE email SET meeting=? WHERE uid=?;";
		
		int dbrc = sqlite3_prepare_v2([[AddEmailDBAccessor sharedManager] database], [updateStmt UTF8String], -1, &emaiUpdateMeetingRequestStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), @"updateMeetingRequest",  sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
			return;
		}
	}
    
    NSString* aString = [anEmail.meetingRequest asJSON];
    sqlite3_bind_text(emaiUpdateMeetingRequestStmt, 1, [aString UTF8String], -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(emaiUpdateFlagsStmt, 2, [anEmail.uid UTF8String], [anEmail.uid length], SQLITE_TRANSIENT);
	
	int result = sqlite3_step(emaiUpdateMeetingRequestStmt);
    if(result != SQLITE_DONE) {
        if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            [self handleError:[NSString stringWithFormat:@"Failed step in updateMeetingRequest: '%s'", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }else{
            [self handleError:[NSString stringWithFormat:@"Failed step in updateMeetingRequest: '%s'", sqlite3_errmsg([[AddEmailDBAccessor sharedManager] database])]];
        }
	}else if(kDebugDatabase) {
        FXDebugLog(kFXLogActiveSync, @"UPDATE email meetingRequest = %@", anEmail.meetingRequest);
    }
    opsInTransaction++;
	
	sqlite3_reset(emaiUpdateMeetingRequestStmt);
    [self endTransactions];
}

- (void)queueUpdateMeetingRequest:(NSArray*)anEmail
{
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_updateMeetingRequest:) object:anEmail];
    [self.operationQueue addOperation:anOp];
}

@end
