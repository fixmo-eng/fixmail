/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseAttachment.h"

@class FastWBXMLParser;

// Types
typedef enum {
    kMethodNormalAttachment             = 1,
    kMethodEmbeddedMessage              = 5,
    kMethodOLE                          = 6,
} EAttachmentMethod;

@interface ASAttachment : BaseAttachment {
    NSString*           contentId;
    EAttachmentMethod   method;
}

// Properties
@property (nonatomic,readwrite,strong) NSString*    contentId;
@property (nonatomic,readwrite) EAttachmentMethod   method;

// Factory
+ (NSMutableArray*)attachmentsFromJSON:(NSString*)aJSONString;
+ (ASAttachment*)attachmentFromDictionary:(NSDictionary*)aDictionary;

// Construct
- (id)init;

// Interface
- (void)fetch;

// Parser
- (void)parser:(FastWBXMLParser*)aParser;

@end
