/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "SearchRunner.h"
#import "sqlite3.h"

@class BaseFolder;

@interface EmailSearch : BaseObject {
    sqlite3_stmt* sqlStatement;
}

@property (nonatomic,strong) id<SearchManagerDelegate>  delegate;
@property (nonatomic,strong) BaseFolder*                folder;
@property (nonatomic,strong) NSDate*                    datetime;
@property (nonatomic,strong) NSString*                  uid;
@property (nonatomic) NSUInteger                        pageSize;
@property (nonatomic) BOOL                              cancelled;


- (id)initWithFolder:(BaseFolder*)aFolder delegate:(id<SearchManagerDelegate>)aDelegate;

- (sqlite3_stmt*)sqlStatement;
- (void)setSqlStatement:(sqlite3_stmt*)aSqlStatement;

- (void)didCancel;

@end
