//
//  SZCUtil.h
//  FixMail
//
//  FIXMO CONFIDENTIAL
//
//
//
//  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
//  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
//  and may be covered by U.S. and Foreign Patents, patents in process, and are
//  protected by trade secret or copyright law.
//  Dissemination of this information or reproduction of this material is strictly
//  forbidden unless prior written permission is obtained from Fixmo Inc.
//
//  Created by Leena Mansour on 2013-03-13.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

@interface SZCUtil : NSObject

+ (FMDatabase*)database;
+ (FMDatabaseQueue*)databaseQueue;

@end
