/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASFolder.h"
#import "ASItemOperationsDelegate.h"
#import "ASSync.h"
#import "SearchRunner.h"

@interface ASCalendarFolder : ASFolder <SearchManagerDelegate, ASItemOperationsDelegate>

// Properties
@property (nonatomic, strong) NSMutableArray*       alarmEvents;

// Factory
+ (UISplitViewController*)splitViewController;

// Interface
- (Event*)eventForUid:(NSString*)aUid;
- (Event*)eventForEventUid:(NSString*)anEventUID;

// ASFolder Overrides
- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType;
- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;
- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;

// BaseAccount Overrides
- (void)commitChanges:(NSArray*)anObjects;

// iCalendar(.ics) attachment loader
- (ASItemOperations*)loadICalendarAttachment:(ASEmail*)anEmail;
- (NSArray*)loadEventsFromICSData:(NSData*)aData;

@end
