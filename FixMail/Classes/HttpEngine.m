/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpEngine.h"
#import "HttpConnection.h"
#import "HttpRequest.h"

@implementation HttpEngine

// Constants
static NSString* kPost      = @"POST";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static HttpEngine*  sHttpEngine = NULL;

+ (id)engine
{
    if(!sHttpEngine) {
        sHttpEngine = [[HttpEngine alloc] initWithDelegate:NULL];
    }
    return sHttpEngine;
}

////////////////////////////////////////////////////////////////////////////////////////////
//Constructors
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Constructors

- (id)initWithDelegate:(NSObject*)aDelegate
{
    if (self = [super init]) {
		sHttpEngine			= self;
        
        delegate			= aDelegate;
        connections         = [[NSMutableDictionary alloc] initWithCapacity:0];
		
		threadCondition			 = [[NSCondition alloc] init];
		threadTerminate			 = FALSE;
		threadTerminated		 = FALSE;
		threadTerminateCondition = [[NSCondition alloc] init];
		
		thread = [[NSThread alloc] initWithTarget:self selector:@selector(thread:) object:nil];
		[thread start];
    }
	
	// Seed random number generator
    // Only used for OAuth I think
    //
	//srand(time(NULL));
	//rand();
    
    return self;
}

/*
- (void)dealloc
{    
    [[connections allValues] makeObjectsPerformSelector:@selector(cancel)];
    connections = NULL;
    
    sHttpEngine = NULL;

}
*/
- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: HttpEngine %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Thread
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Thread
- (void)thread:(id)arg
{
    @autoreleasepool {
	
		runLoop = [NSRunLoop currentRunLoop];
		
		while(1) {
			[threadCondition lock];
			[threadCondition wait];
			[threadCondition unlock];
			
			@try {
				[runLoop run];
			}@catch(NSException *e) {
				[self _logException:@"thread" exception:e];
			}
		}
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Build Query String
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Build Query String

+ (NSString*)encodeStringSpaceToPlus:(NSString*)string
{
    NSString* result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, 
																		   (CFStringRef)string, 
																		   (CFStringRef)@" ",
																		   (CFStringRef)@";%/?:@&=$+{}<>,",
																		   kCFStringEncodingUTF8));
	NSString* interim = [result stringByReplacingOccurrencesOfString:@" " withString:@"+"];
	return interim;
}

- (NSString*)_buildQueryString:(NSString*)aBase parameters:(NSDictionary*)aParams prefixed:(BOOL)isPrefixed
{
    NSMutableString* aString = [NSMutableString stringWithString:aBase];

    if (aParams) {
        NSArray *aNames = [aParams allKeys];
        NSUInteger aCount = [aNames count];
        for(int i = 0; i < aCount; i++) {
            if (i == 0 && isPrefixed) {
                [aString appendString:@"?"];
            } else if (i > 0) {
                [aString appendString:@"&"];
            }
            NSString* aName = [aNames objectAtIndex:i];
            [aString appendString:[NSString stringWithFormat:@"%@=%@", 
							   aName, [HttpEngine encodeStringSpaceToPlus:[aParams objectForKey:aName]]]];
        }
    }
    
    return aString;
}

////////////////////////////////////////////////////////////////////////////////////////////
// HTTP Request
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Send HTTP Request

- (NSMutableURLRequest*)createURLRequest:(NSString*)aProtocol
                                  method:(NSString*)aMethod 
                                     url:(NSString*)aUrl 
                                 headers:(NSDictionary*)aHeaders
                                  params:(NSDictionary*)aParams 
                                    body:(NSData*)aBody
                         timeoutInterval:(NSTimeInterval)aTimeoutInterval
{
	NSString* fullPath = aUrl;
    if (aParams) {
        fullPath = [self _buildQueryString:fullPath parameters:aParams prefixed:YES];
    }
    
	// Connection protocol
	//
    NSString* urlString = [NSString stringWithFormat:@"%@://%@", aProtocol, fullPath];
    
	// Build URL from string
	//
    NSURL *finalURL = [NSURL URLWithString:urlString];
    if (!finalURL) {
		FXDebugLog(kFXLogActiveSync, @"httpRequest URL failed: %@", urlString);
        return nil;
    }
	
	// URL Request
	//
    NSMutableURLRequest* urlRequest;
	urlRequest = [NSMutableURLRequest requestWithURL:finalURL 
                                         cachePolicy:NSURLRequestReloadIgnoringCacheData 
                                     timeoutInterval:aTimeoutInterval];
    if(aMethod) {
        [urlRequest setHTTPMethod:aMethod];
    }
    
    // Cookies
    //
    [urlRequest setHTTPShouldHandleCookies:NO];
	
	// Add headers
	//	
	for (id key in aHeaders) {
		NSString* header = [aHeaders objectForKey:key];
		[urlRequest setValue:header forHTTPHeaderField:key];
	}
	
    // Set body if this is a POST
	//
    BOOL isPOST = (aMethod && [aMethod isEqualToString:kPost]);
    if (isPOST && aBody) {
        [urlRequest setHTTPBody:aBody];
    }
    
    // Create connection
	//
	if(isPOST) {
		if(aBody) {
			//FXDebugLog(kFXLogActiveSync, @"httpRequest POST %@\nbody: %@", urlString, aBody);
            FXDebugLog(kFXLogActiveSync, @"httpRequest POST %@ body %d bytes", urlString, aBody.length);
		}else{
			FXDebugLog(kFXLogActiveSync, @"httpRequest POST %@", urlString);
		}
	}else{
		FXDebugLog(kFXLogActiveSync, @"httpRequest %@ %@", aMethod, urlString);
	}
    
    return urlRequest;
}
    
- (NSString*)sendRequest:(NSURLRequest*)aUrlRequest httpRequest:(HttpRequest*)anHttpRequest
{
    HttpConnection* aConnection;
    aConnection = [[HttpConnection alloc] initWithRequest:aUrlRequest 
                                              httpRequest:anHttpRequest 
                                                  runLoop:runLoop];
    if (aConnection) {
        [anHttpRequest setHttpConnection:aConnection];
		if([anHttpRequest thread]) {
			[threadCondition signal];
		}

		[connections setObject:aConnection forKey:[aConnection identifier]];
    }else{
		return nil;
    }
    
    return [aConnection identifier];
}

- (void)removeConnection:(NSString*)connectionIdentifier
{
    [connections removeObjectForKey:connectionIdentifier];
}

@end
