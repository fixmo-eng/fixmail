/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASFolderUpdate.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASFolderUpdateDelegate.h"
#import "WBXMLDataGenerator.h"

@implementation ASFolderUpdate

// Properties
@synthesize folder;
@synthesize name;
@synthesize delegate;
@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"FolderUpdate";
static const ASTag kCommandTag  = FOLDER_FOLDER_UPDATE;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASFolderUpdate*)queueFolderUpdate:(ASFolder*)aFolder
                             newName:(NSString*)aNewName
                       displayErrors:(BOOL)aDisplayErrors
{
    ASAccount* anAccount = (ASAccount*)aFolder.account;
    ASFolderUpdate* aFolderUpdate = [[ASFolderUpdate alloc] initWithAccount:anAccount];
    aFolderUpdate.folder          = aFolder;
    aFolderUpdate.name            = aNewName;
    [aFolderUpdate queue];
    
    return aFolderUpdate;
}

- (void)send
{
    NSData* aWbxml = [self wbxmlFolderUpdate:self.folder newName:self.name];
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlFolderUpdate:(ASFolder*)aFolder newName:(NSString*)aNewName
{
	WBXMLDataGenerator wbxml;
    
    ASAccount* anAccount = (ASAccount*)aFolder.account;
	wbxml.start(kCommandTag); {
        wbxml.keyValue(FOLDER_SYNC_KEY, anAccount.syncKey);
        wbxml.keyValue(FOLDER_SERVER_ID, aFolder.uid);
        wbxml.keyValue(FOLDER_PARENT_ID, aFolder.parentId);
        wbxml.keyValue(FOLDER_DISPLAY_NAME, aNewName);
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kFolderUpdateSuccess:
            break;
#ifdef DEBUG
        case kFolderUpdateFolderExists:
            [HttpRequest handleASError:@"Folder update folder exists"]; break;
        case kFolderUpdateIsRecipientFolder:
            [HttpRequest handleASError:@"Folder update is recipient folder"]; break;
        case kFolderUpdateDoesNotExist:
            [HttpRequest handleASError:@"Folder update does not exist"]; break;
        case kFolderUpdateParentNotFound:
            [HttpRequest handleASError:@"Folder update parent not found"]; break;
        case kFolderUpdateServerError:
            [HttpRequest handleASError:@"Folder update server error"]; break;
        case kFolderUpdateSyncKeyInvalid:
            [HttpRequest handleASError:@"Folder update sync key invalid"]; break;
        case kFolderUpdateMalformedRequest:
            [HttpRequest handleASError:@"Folder update malformed request"]; break;
        case kFolderUpdateUnknownError:
            [HttpRequest handleASError:@"Folder update unknown error"]; break;
        default:
            [super handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"FOLDER_UPDATE_ERROR", @"ErrorAlert", @"Error alert message for Folder Update error"), self.statusCodeAS]];
            break;
			
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case FOLDER_STATUS:
                    self.statusCodeAS = (EFolderUpdateStatus)[aParser getIntTraceable];
                    if(self.statusCodeAS == kFolderUpdateSuccess) {
                        self.folder.displayName = self.name;
                        [self.folder commit];
                    }
                    break;
                case FOLDER_SYNC_KEY:
                    self.account.syncKey = [aParser getStringTraceable];
                    [self.account commitSyncKey];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        if(self.statusCodeAS == kFolderUpdateSuccess) {
            if(self.delegate) {
                [self.delegate folderUpdated:self.folder];
            }
            [self success];
        }else{
            if(self.delegate) {
                [self.delegate folderUpdateFailed:self];
            }
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Folder update response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end