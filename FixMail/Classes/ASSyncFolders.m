/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncFolders.h"
#import "ASAccount.h"
#import "ASSync.h"

@implementation ASSyncFolders

@synthesize account;
@synthesize syncFolders;
@synthesize pingFolders;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
          syncFolders:(NSArray*)aSyncFolders
          pingFolders:(NSArray*)aPingFolders
        bodyFetchType:(EBodyFetchType)aBodyFetchType
{
    if (self = [super init]) {
        self.account        = anAccount;
        self.syncFolders    = aSyncFolders;
        self.pingFolders    = aPingFolders;
        self.bodyFetchType  = aBodyFetchType;
    }
    return self;
}


- (void)send
{
    ASSync* aSync = [ASSync sendSync:self.account
                         syncFolders:self.syncFolders
                         pingFolders:self.pingFolders
                       bodyFetchType:self.bodyFetchType
                            delegate:self.account];
    [super sendAndWait:aSync];
}

@end
