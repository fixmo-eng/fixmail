/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASFolderDelete.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASFolderDeleteDelegate.h"
#import "ASPing.h"
#import "WBXMLDataGenerator.h"

@implementation ASFolderDelete

// Properties
@synthesize delegate;
@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"FolderDelete";
static const ASTag kCommandTag  = FOLDER_FOLDER_DELETE;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASFolderDelete*)queueFolderDelete:(ASFolder*)aFolder
                       displayErrors:(BOOL)aDisplayErrors
{
    ASAccount* anAccount = (ASAccount*)aFolder.account;
    ASFolderDelete* aFolderDelete = [[ASFolderDelete alloc] initWithAccount:anAccount];
    aFolderDelete.folder        = aFolder;
    aFolderDelete.displayErrors = aDisplayErrors;
    [aFolderDelete queue];
    
    return aFolderDelete;
}

- (void)send
{
    NSData* aWbxml = [self wbxmlFolderDelete:self.folder];
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlFolderDelete:(ASFolder*)aFolder
{
	WBXMLDataGenerator wbxml;
    
    ASAccount* anAccount = (ASAccount*)aFolder.account;
	wbxml.start(kCommandTag); {
        wbxml.keyValue(FOLDER_SYNC_KEY, anAccount.syncKey);
        wbxml.keyValue(FOLDER_SERVER_ID, aFolder.uid);
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kFolderDeleteSuccess:
            break;
#ifdef DEBUG
        case kFolderDeleteFolderIsSpecial:
            [HttpRequest handleASError:@"Folder delete is recipient folder"]; break;
        case kFolderDeleteDoesNotExist:
            [HttpRequest handleASError:@"Folder delete does not exist"]; break;
        case kFolderDeleteServerError:
            [HttpRequest handleASError:@"Folder delete server error"]; break;
        case kFolderDeleteSyncKeyInvalid:
            [HttpRequest handleASError:@"Folder delete sync key invalid"]; break;
        case kFolderDeleteMalformedRequest:
            [HttpRequest handleASError:@"Folder delete malformed request"]; break;
        case kFolderDeleteUnknownError:
            [HttpRequest handleASError:@"Folder delete unknown error"]; break;
        default:
            [super handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"FOLDER_DELETE_ERROR", @"ErrorAlert", @"Error alert message for Folder Delete error"), self.statusCodeAS]];
            break;
			
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case FOLDER_STATUS:
                    self.statusCodeAS = (EFolderDeleteStatus)[aParser getIntTraceable];
                    if(self.statusCodeAS == kFolderDeleteSuccess) {
                        [self.account folderDeleted:self.folder.uid];
                    }
                    break;
                case FOLDER_SYNC_KEY:
                    self.account.syncKey = [aParser getStringTraceable];
                    [self.account commitSyncKey];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        if(self.statusCodeAS == kFolderDeleteSuccess) {
            if(self.delegate) {
                [self.delegate folderDeleted:self.folder];
            }
        }else{
            if(self.delegate) {
                [self.delegate folderDeleteFailed:self];
            }
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Folder delete response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end