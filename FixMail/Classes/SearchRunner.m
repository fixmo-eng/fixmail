//
//  SearchRunner.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 3/29/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "SearchRunner.h"
#import "ASAccount.h"
#import "ASEmail.h"
#import "BaseFolder.h"
#import "Calendar.h"
#import "DateUtil.h"
#import "EmailProcessor.h"
#import "EmailSearch.h"
#import "ErrorAlert.h"
#import "LoadEmailDBAccessor.h"
#import "FXDatabase.h"
#import "SearchEmailDBAccessor.h"

@implementation SearchRunner

// Static
static SearchRunner *searchSingleton    = nil;

// Properties
@synthesize cancelled;
@synthesize autocompleteLock;

// Constants
static const NSString* kDelegate        = @"delegate";
static const NSString* kQuery           = @"query";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (id)getSingleton 
{
	@synchronized(self) {
		if (searchSingleton == nil) {
			searchSingleton = [[SearchRunner alloc] init]; 
		}
	}
	return searchSingleton;
}

+ (void)clearPreparedStmts 
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init 
{
	if(self = [super init]) {
		self.autocompleteLock = [[NSObject alloc] init];
	}
	
	return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EMAIL_SEARCH", @"ErrorAlert", @"SearchRunner error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EMAIL_SEARCH", @"ErrorAlert", nil) message:anErrorMessage];

}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (void)cancel 
{
	self.cancelled = YES;
}

- (BOOL)isCancelled:(EmailSearch*)anEmailSearch
{
    return anEmailSearch.cancelled || self.cancelled;
}

static NSString* getStringForField(sqlite3_stmt* aStatement, int aFieldNum)
{
    NSString* aString = NULL;
    const char* sqlVal = (const char *)sqlite3_column_text(aStatement, aFieldNum);
    if(sqlVal != nil) {	
        aString = [NSString stringWithUTF8String:sqlVal]; 
    }else{
        aString = @"";
    }
    return aString;
}

static NSDate* getDateForField(sqlite3_stmt* aStatement, int aFieldNum)
{
    NSDate* aDate = [NSDate date];
    const char* sqlVal = (const char *)sqlite3_column_text(aStatement, aFieldNum);
    if(sqlVal != nil) {
        NSString* aDateString = [NSString stringWithUTF8String:sqlVal];
        aDate = [[EmailProcessor dateFormatter] dateFromString:aDateString];
    }
    return aDate;
}

static NSNumber* getIntFieldAsNumber(sqlite3_stmt* aStatement, int aFieldNum)
{
    int anInt = sqlite3_column_int(aStatement, aFieldNum);
    return [NSNumber numberWithInt:anInt];
}

static NSNumber* getUnsignedIntFieldAsNumber(sqlite3_stmt* aStatement, int aFieldNum)
{
    NSUInteger anUnsignedInt = (NSUInteger)sqlite3_column_int(aStatement, aFieldNum);
    return [NSNumber numberWithUnsignedInt:anUnsignedInt];
}

static NSNumber* getJSONFieldAsBoolNumber(sqlite3_stmt* aStatement, int aFieldNum)
{
    int aHasJSON = sqlite3_column_int(aStatement, aFieldNum) - 2; // will be non-0 if there are attachments, the -2 are to counter the string "[]"
    return [NSNumber numberWithBool:aHasJSON > 0];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Folder search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Folder search

- (void)sqlRowError:(int)sqlResult
{
    switch(sqlResult) {
        case SQLITE_DONE:
            break;
        case SQLITE_LOCKED:
        case SQLITE_BUSY:
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_SQLITE_LOCKED_OR_BUSY", @"ErrorAlert", @"Error alert for when sqlite is locked or busy"), @"performFolderSearch"]];
            break;
        case SQLITE_MISUSE:
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_SQLITE_MISUSE", @"ErrorAlert", @"Error alert for sqlite misuse"), @"performFolderSearch"]];
            break;
        case SQLITE_ERROR:
        default:
            [self handleError:[NSString stringWithFormat:@"performFolderSearch sqlite error: %s",
                               sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            break;
    }
}

- (int)executeFolderSearch:(EmailSearch*)anEmailSearch limit:(NSUInteger)aLimit
{
    int aCount = 0;

	NSMutableArray* anArray = [[NSMutableArray alloc] initWithCapacity:anEmailSearch.pageSize];
    NSString* aLastUid      = anEmailSearch.uid;
    ASFolder* aFolder       = (ASFolder*)anEmailSearch.folder;
    ASEmail* anEmail        = nil;
    sqlite3_stmt* folderSearchStmt = anEmailSearch.sqlStatement;

    if(aLastUid.length > 0) {
        // This is a page other than the first.  We are looking for the last UID of the previous page, it should be the first
        // row in this query.  If so we will discard it and then load everything following.
        // If its been deleted things get more complicated.
        //
        while(![self isCancelled:anEmailSearch]) {
            int sqlResult = sqlResult = sqlite3_step(folderSearchStmt);
            if(sqlResult == SQLITE_ROW) {
                @try {
                    NSString* aUid = getStringForField(folderSearchStmt,  0);
                    if(aLastUid.length > 0 && [aLastUid isEqualToString:aUid]) {
                        //FXDebugLog(kFXLogASEmail, @"pitch: %@", aUid);
                        aLimit--;
                        break;
                    }else
                    if(anEmailSearch.datetime) {
                        const char* aDateString = (const char *)sqlite3_column_text(folderSearchStmt, 1);
                        if(aDateString) {
                            NSDate* aDateTime = [[EmailProcessor dateFormatter] dateFromString:[NSString stringWithUTF8String:aDateString]];
                            if([aDateTime compare:anEmailSearch.datetime] == NSOrderedAscending) {
                                FXDebugLog(kFXLogASEmail, @"cursor UID missing: %@", anEmailSearch);
                                aCount++;
                                anEmail = [[ASEmail alloc] initWithFolder:aFolder];
                                [anEmail loadFromSQL:folderSearchStmt loadBody:TRUE];
                                [anArray addObject:anEmail];
                                FXDebugLog(kFXLogASEmail, @"loadEmail %@ %@", anEmail.uid, anEmail.datetime);                                
                                break;
                            }
                        }
                    }
                }@catch (NSException* e) {
                    [self _logException:@"performFolderSearch pre" exception:e];
                }
                aLimit--;
            }else{
                [self sqlRowError:sqlResult];
                break;
            }
        }
    }
    
	while(![self isCancelled:anEmailSearch]) {
        // Load the emails for this page
        //
        int sqlResult = sqlResult = sqlite3_step(folderSearchStmt);
        if(sqlResult == SQLITE_ROW) {
            @try {
                aCount++;
                anEmail = [[ASEmail alloc] initWithFolder:aFolder];
                [anEmail loadFromSQL:folderSearchStmt loadBody:TRUE];
                [anArray addObject:anEmail];
  #ifdef DEBUG
                if([EmailProcessor debugSearch]) {
                    FXDebugLog(kFXLogASEmail, @"\tloadEmail %@ %@", anEmail.uid, anEmail.datetime);
                }
  #endif
            }@catch (NSException* e) {
                [self _logException:@"performFolderSearch" exception:e];
            }
        }else{
            [self sqlRowError:sqlResult];
            break;
        }
	}

    // Deliver the search results
    //
	if(![self isCancelled:anEmailSearch]) {
        // Save the uid and date for the last email in this page
        //
        if(anEmail) {
            anEmailSearch.uid       = anEmail.uid;
            anEmailSearch.datetime  = anEmail.datetime;
        }else{
            FXDebugLog(kFXLogASEmail, @"executeFolderSearch no results");
        }
        
        if(anArray.count > 0 && ![self isCancelled:anEmailSearch]) {
            [anEmailSearch.delegate deliverSearchResults:anArray];
        }
        
        // If we got fewer emails than expected for the page size requested we've reached the end
        // Tell the client if there are more results or this is the end
        //
        if(![self isCancelled:anEmailSearch]) {
            BOOL moreResults = TRUE;
            if(aCount < aLimit) {
                moreResults = FALSE;
            }
            NSNumber* additionalResults = [NSNumber numberWithBool:moreResults];
            [anEmailSearch.delegate deliverAdditionalResults:additionalResults];
        }
	}

    return aCount;
}

- (void)performFolderSearch:(EmailSearch*)anEmailSearch
{
    int count = 0;
 
    @try {
        if([self isCancelled:anEmailSearch]) {
            [anEmailSearch didCancel];
            [[LoadEmailDBAccessor sharedManager] close];
            return;
        }
        
        sqlite3_stmt* folderSearchStmt = anEmailSearch.sqlStatement;

        if(!folderSearchStmt) {
            NSString *queryString = [NSString stringWithFormat:
                    @"SELECT %@"
                        "FROM email  "
                         "WHERE datetime <= ? AND (folder_num = ? OR folder_num_1 = ? OR folder_num_2 = ? OR folder_num_3 = ?) "
                         "ORDER BY datetime DESC LIMIT ?;", [ASEmail sqlFields]];
            int dbrc = sqlite3_prepare_v2([[LoadEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &folderSearchStmt, nil);	
            if (dbrc == SQLITE_OK) {
                anEmailSearch.sqlStatement = folderSearchStmt;
            }else{
                [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performFolderSearch", sqlite3_errmsg([[LoadEmailDBAccessor sharedManager] database])]];
                return;
            }
        }else{
            sqlite3_reset(folderSearchStmt);
        }
        
        if([self isCancelled:anEmailSearch]) {
            [anEmailSearch didCancel];
            [[LoadEmailDBAccessor sharedManager] close];
            return;
        }
       
        NSDate* aDate = anEmailSearch.datetime;
        if(!aDate) {
            // For first query we want current time but we add a day to deal with clocks which may be out of sync.
            // We may get an email that appears to be slightly in the future to this computer.
            // The other option is we prepare two different SQL statements with the first having no time comparison
            // and subsequent queries do
            //
            aDate = [[Calendar calendar] addDays:1 toDate:[NSDate date]];
        }
        NSString* dateString = [[EmailProcessor dateFormatter] stringFromDate:aDate];
        sqlite3_bind_text(folderSearchStmt, 1, [dateString UTF8String], -1, SQLITE_TRANSIENT);

        BaseFolder* aFolder = anEmailSearch.folder;
        int folderNum = aFolder.folderNum;
        sqlite3_bind_int(folderSearchStmt, 2, folderNum);
        sqlite3_bind_int(folderSearchStmt, 3, folderNum);
        sqlite3_bind_int(folderSearchStmt, 4, folderNum);
        sqlite3_bind_int(folderSearchStmt, 5, folderNum);
        
        NSUInteger aLimit = anEmailSearch.pageSize;
        if(aLimit > 1 && anEmailSearch.uid.length > 0) {
            aLimit++;
        }
        sqlite3_bind_int(folderSearchStmt, 6, aLimit);

      #ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"\tperformFolderSearch @", aFolder.displayName);
        }
      #endif

        count = [self executeFolderSearch:anEmailSearch limit:aLimit];
        
        [[LoadEmailDBAccessor sharedManager] close];
        
        if([self isCancelled:anEmailSearch]) {
            [anEmailSearch didCancel];
        }
        
      #ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"performFolderSearch count: %d", count);
        }
      #endif
    }@catch (NSException* e) {
        [self _logException:@"performMeetingSearch" exception:e];
    }
}

- (void)folderSearch:(EmailSearch*)anEmailSearch
{
	NSInvocationOperation* searchOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performFolderSearch:) object:anEmailSearch];
	[[[EmailProcessor getSingleton] operationQueue] addOperation:searchOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UID Search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UID Search

- (ASEmail*)emailForUid:(NSString*)aUid folder:(BaseFolder*)aFolder
{
    ASEmail* anEmail = nil;
    
    @try {
        sqlite3_stmt *aUidSearchStmt = nil;
        NSString *queryString = [NSString stringWithFormat:
                                 @"SELECT %@ "
                                    "FROM email "
                                    "WHERE uid = ? LIMIT 1;", [ASEmail sqlFields]];
        
        int dbrc = sqlite3_prepare_v2([[SearchEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &aUidSearchStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performMeetingSearch", sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            FXDebugLog(kFXLogActiveSync, @"%@", queryString);
            return 0;
        }

        sqlite3_bind_text(aUidSearchStmt, 1, [aUid UTF8String], -1, SQLITE_TRANSIENT);
        
        int sqlResult = sqlResult = sqlite3_step(aUidSearchStmt);
        if(sqlResult == SQLITE_ROW) {
            @try {
                anEmail = [aFolder.account createEmailForFolder:aFolder];
                [anEmail loadFromSQL:aUidSearchStmt loadBody:TRUE];
                FXDebugLog(kFXLogASEmail, @"Search result: %@ %@", anEmail.uid, anEmail.subject);
                                
            }@catch (NSException* e) {
                [self _logException:@"emailForUid" exception:e];
            }
        }else{
            [self sqlRowError:sqlResult];
        }

        sqlite3_finalize(aUidSearchStmt);
    }@catch (NSException* e) {
        [self _logException:@"emailForUid" exception:e];
    }
	return anEmail;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Email Search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Email Search

- (NSArray *)performEmailBodySearch:(EmailSearch*)anEmailSearch usingText:(NSString *)text
{
    @try {
        sqlite3_stmt *aBodySearchStmt = nil;
        NSString *queryString = [NSString stringWithFormat:
                                 @"SELECT * "
								 "FROM email "
								 "WHERE body LIKE '%%%@%%' "
								 "ORDER BY datetime DESC;", text];
        
        int dbrc = sqlite3_prepare_v2([[SearchEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &aBodySearchStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performEmailBodySearch", sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            FXDebugLog(kFXLogActiveSync, @"%@", queryString);
            return nil;
        }
        
        BaseFolder* aFolder = anEmailSearch.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        int aFolderNum = [aFolder folderNum];
        sqlite3_bind_int(aBodySearchStmt, 1, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 2, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 3, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 4, aFolderNum);
        sqlite3_bind_text(aBodySearchStmt, 5, "%%.ics%%",     -1, NULL);
#ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"\performEmailBodySearch folderNum=%d", aFolderNum);
        }
#endif
        
        NSMutableArray* anArray = [[NSMutableArray alloc] initWithCapacity:20];
        
        while(![self isCancelled:anEmailSearch]) {
            int sqlResult = sqlResult = sqlite3_step(aBodySearchStmt);
            if(sqlResult == SQLITE_ROW) {
                @try {
                    ASEmail* anEmail = [anAccount createEmailForFolder:aFolder];
                    [anEmail loadFromSQL:aBodySearchStmt loadBody:TRUE];
                    //FXDebugLog(kFXLogActiveSync, @"Search result: %@ %@", anEmail.subject, anEmail.attachmentJSON);
                    [anArray addObject:anEmail];
                }@catch (NSException* e) {
                    [self _logException:@"performEmailBodySearch" exception:e];
                }
            }else{
                [self sqlRowError:sqlResult];
                break;
            }
        }
        
        if(![self isCancelled:anEmailSearch]) {
            //[anEmailSearch.delegate deliverSearchResults:anArray];
            //NSNumber* additionalResults = [NSNumber numberWithBool:FALSE];
            //[anEmailSearch.delegate deliverAdditionalResults:additionalResults];
            sqlite3_finalize(aBodySearchStmt);
        }else{
            [anEmailSearch didCancel];
        }
		return anArray;
        
    }@catch (NSException* e) {
        [self _logException:@"performEmailBodySearch" exception:e];
		return nil;
    }
}

- (NSArray *)performEmailAddressSearch:(EmailSearch*)anEmailSearch usingText:(NSString *)text
{
	@try {
        sqlite3_stmt *aBodySearchStmt = nil;			
		NSString *queryString = [NSString stringWithFormat:
								 @"SELECT %@ "
								 "FROM email "
								 "WHERE (folder_num = ? OR folder_num_1 = ? OR folder_num_2 = ? OR folder_num_3 = ?) "
								 "AND (sender_name LIKE ? OR subject LIKE ? OR sender_address LIKE ? OR tos LIKE ? OR ccs LIKE ? OR bccs LIKE ? "
								 "OR (messageClass != ? AND messageClass != ? AND messageClass != ? AND body LIKE ?) ) "
								 "ORDER BY datetime DESC;", [ASEmail sqlFields]];
        
        int dbrc = sqlite3_prepare_v2([[SearchEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &aBodySearchStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performEmailBodySearch", sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            FXDebugLog(kFXLogActiveSync, @"%@", queryString);
            return nil;
        }
        
        BaseFolder* aFolder = anEmailSearch.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        int aFolderNum = [aFolder folderNum];
        int result = sqlite3_bind_int(aBodySearchStmt, 1, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 2, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 3, aFolderNum);
        sqlite3_bind_int(aBodySearchStmt, 4, aFolderNum);
		
		NSString *textPattern = [NSString stringWithFormat:@"%%%@%%", text];
        result = sqlite3_bind_text(aBodySearchStmt, 5, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
        result = sqlite3_bind_text(aBodySearchStmt, 6, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
        result = sqlite3_bind_text(aBodySearchStmt, 7, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
        result = sqlite3_bind_text(aBodySearchStmt, 8, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
        result = sqlite3_bind_text(aBodySearchStmt, 9, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
        result = sqlite3_bind_text(aBodySearchStmt, 10, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
		
        sqlite3_bind_int(aBodySearchStmt, 11, kMessageClassNoteSMIME);
		sqlite3_bind_int(aBodySearchStmt, 12, kMessageClassNoteReceiptSMIME);
		sqlite3_bind_int(aBodySearchStmt, 13, kMessageClassNoteSMIMEMultipartSigned);
        result = sqlite3_bind_text(aBodySearchStmt, 14, [textPattern UTF8String], -1, SQLITE_TRANSIENT);
		
#ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"\performEmailAddressSearch folderNum=%d", aFolderNum);
        }
#endif

        NSMutableArray* anArray = [[NSMutableArray alloc] initWithCapacity:20];
        
        while(![self isCancelled:anEmailSearch]) {
            int sqlResult = sqlResult = sqlite3_step(aBodySearchStmt);
            if(sqlResult == SQLITE_ROW) {
                @try {
                    ASEmail* anEmail = [anAccount createEmailForFolder:aFolder];
                    [anEmail loadFromSQL:aBodySearchStmt loadBody:TRUE];
                    //FXDebugLog(kFXLogActiveSync, @"Search result: %@ %@", anEmail.subject, anEmail.attachmentJSON);
                    [anArray addObject:anEmail];
                }@catch (NSException* e) {
                    [self _logException:@"performEmailAddressSearch" exception:e];
                }
            }else{
                [self sqlRowError:sqlResult];
                break;
            }
        }
        
        if(![self isCancelled:anEmailSearch]) {
            //[anEmailSearch.delegate deliverSearchResults:anArray];
            //NSNumber* additionalResults = [NSNumber numberWithBool:FALSE];
            //[anEmailSearch.delegate deliverAdditionalResults:additionalResults];
            sqlite3_finalize(aBodySearchStmt);
        }else{
            [anEmailSearch didCancel];
        }
		return anArray;
        
    }@catch (NSException* e) {
        [self _logException:@"performEmailBodySearch" exception:e];
		return nil;
    }
}




////////////////////////////////////////////////////////////////////////////////////////////
// Meeting Invitation Search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Meeting Invitation Search

- (void)performMeetingSearch:(EmailSearch*)anEmailSearch
{
    @try {
        sqlite3_stmt *aMeetingSearchStmt = nil;
        NSString *queryString = [NSString stringWithFormat:
                                 @"SELECT %@ "
                                    "FROM email "
                                    "WHERE (folder_num = ? OR folder_num_1 = ? OR folder_num_2 = ? OR folder_num_3 = ?) "
                                      "AND (LENGTH(meeting) > 2 OR attachments LIKE ?) "
                                    "ORDER BY datetime DESC;", [ASEmail sqlFields]];
        
        int dbrc = sqlite3_prepare_v2([[SearchEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &aMeetingSearchStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performMeetingSearch", sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            FXDebugLog(kFXLogActiveSync, @"%@", queryString);
            return;
        }
        
        BaseFolder* aFolder = anEmailSearch.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        int aFolderNum = [aFolder folderNum];
        sqlite3_bind_int(aMeetingSearchStmt, 1, aFolderNum);
        sqlite3_bind_int(aMeetingSearchStmt, 2, aFolderNum);
        sqlite3_bind_int(aMeetingSearchStmt, 3, aFolderNum);
        sqlite3_bind_int(aMeetingSearchStmt, 4, aFolderNum);
        sqlite3_bind_text(aMeetingSearchStmt, 5, "%%.ics%%",     -1, NULL);
      #ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"\tperformMeetingSearch folderNum=%d", aFolderNum);
        }
      #endif
        
        NSMutableArray* anArray = [[NSMutableArray alloc] initWithCapacity:20];
        
        while(![self isCancelled:anEmailSearch]) {
            int sqlResult = sqlResult = sqlite3_step(aMeetingSearchStmt);
            if(sqlResult == SQLITE_ROW) {
                @try {
                    ASEmail* anEmail = [anAccount createEmailForFolder:aFolder];
                    [anEmail loadFromSQL:aMeetingSearchStmt loadBody:TRUE];
                    //FXDebugLog(kFXLogActiveSync, @"Search result: %@ %@", anEmail.subject, anEmail.attachmentJSON);
                    [anArray addObject:anEmail];
                }@catch (NSException* e) {
                    [self _logException:@"performMeetingSearch" exception:e];
                }
            }else{
                [self sqlRowError:sqlResult];
                break;
            }
        }
        
        if(![self isCancelled:anEmailSearch]) {
            [anEmailSearch.delegate deliverSearchResults:anArray];
            NSNumber* additionalResults = [NSNumber numberWithBool:FALSE];
            [anEmailSearch.delegate deliverAdditionalResults:additionalResults];
            sqlite3_finalize(aMeetingSearchStmt);
        }else{
            [anEmailSearch didCancel];
        }
        
    }@catch (NSException* e) {
        [self _logException:@"performMeetingSearch" exception:e];
    }
}

- (void)meetingSearch:(EmailSearch*)anEmailSearch
{
	NSInvocationOperation* searchOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performMeetingSearch:) object:anEmailSearch];
	[[[EmailProcessor getSingleton] operationQueue]  addOperation:searchOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Meeting Invitation Search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Meeting Invitation Search

- (void)performOfflineChangesSearch:(EmailSearch*)anEmailSearch
{
    @try {
        sqlite3_stmt *anOfflineSearchStmt = nil;
        NSString *queryString = [NSString stringWithFormat:
                                 @"SELECT %@ "
                                    "FROM email "
                                 "WHERE (folder_num = ? AND (flags & ?) != 0);", [ASEmail sqlFields]];
        
        int dbrc = sqlite3_prepare_v2([[SearchEmailDBAccessor sharedManager] database], [queryString UTF8String], -1, &anOfflineSearchStmt, nil);
        if (dbrc != SQLITE_OK) {
            [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"performOfflineChangesSearch", sqlite3_errmsg([[SearchEmailDBAccessor sharedManager] database])]];
            FXDebugLog(kFXLogActiveSync, @"%@", queryString);
            return;
        }
        
        BaseFolder* aFolder = anEmailSearch.folder;
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        
        int aFolderNum = [aFolder folderNum];
        sqlite3_bind_int(anOfflineSearchStmt, 1, aFolderNum);
        sqlite3_bind_int(anOfflineSearchStmt, 2, [ASEmail hasOfflineChangeFlag]);

#ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"\tperformOfflineChangesSearch");
        }
#endif
        
        NSMutableArray* anArray = [[NSMutableArray alloc] initWithCapacity:20];
        
        while(![self isCancelled:anEmailSearch]) {
            int sqlResult = sqlResult = sqlite3_step(anOfflineSearchStmt);
            if(sqlResult == SQLITE_ROW) {
                @try {
                    ASEmail* anEmail = [anAccount createEmailForFolder:aFolder];
                    [anEmail loadFromSQL:anOfflineSearchStmt loadBody:TRUE];
                    //FXDebugLog(kFXLogActiveSync, @"Search result: %@ %@", anEmail.subject, anEmail.attachmentJSON);
                    [anArray addObject:anEmail];
                }@catch (NSException* e) {
                    [self _logException:@"performOfflineChangesSearch" exception:e];
                }
            }else{
                [self sqlRowError:sqlResult];
                break;
            }
        }
        
        if(![self isCancelled:anEmailSearch]) {
            [anEmailSearch.delegate deliverSearchResults:anArray];
            NSNumber* additionalResults = [NSNumber numberWithBool:FALSE];
            [anEmailSearch.delegate deliverAdditionalResults:additionalResults];
            sqlite3_finalize(anOfflineSearchStmt);
        }else{
            [anEmailSearch didCancel];
        }
        
    }@catch (NSException* e) {
        [self _logException:@"performOfflineChangesSearch" exception:e];
    }
}

- (void)offlineChangesSearch:(EmailSearch*)anEmailSearch
{
	NSInvocationOperation* searchOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performOfflineChangesSearch:) object:anEmailSearch];
	[[[EmailProcessor getSingleton] operationQueue]  addOperation:searchOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Autocompletion
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Autocompletion

- (void)performAutocomplete:(NSString *)query withDelegate:(id)delegate
{	
    @try {
  #ifdef DEBUG
        if([EmailProcessor debugSearch]) {
            FXDebugLog(kFXLogActiveSync, @"performAutocomplete query=%@", query);
        }
  #endif

		NSMutableArray *searchArray = [FXDatabase findContacts:query];
		
        if([delegate respondsToSelector:@selector(deliverAutocompleteResult:)])	{
            [delegate performSelector:@selector(deliverAutocompleteResult:) withObject:searchArray];
        }        
    }@catch (NSException* e) {
        [self _logException:@"performAutocomplete" exception:e];
    }
}

- (void)performAutocompleteAsync:(NSDictionary*)query 
{
	// the purpose of this method is to make performAutocomplete asynchronous
	@synchronized(self.autocompleteLock) {
		[self performAutocomplete:[query objectForKey:kQuery] withDelegate:[query objectForKey:kDelegate]];
	}
}

- (void)autocomplete:(NSString *)query withDelegate:(id)autocompleteDelegate 
{
	//Create the queryOp object
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:query forKey:kQuery];
	[params setObject:autocompleteDelegate forKey:kDelegate];
	
	//Invoke local search
	NSInvocationOperation* autocompleteOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performAutocompleteAsync:) object:params];
	[[[EmailProcessor getSingleton] operationQueue]  addOperation:autocompleteOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Contacts
////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark Contacts

-(NSDictionary*)findContact:(NSString*)inName
{
	NSArray *theContacts = [FXDatabase findContacts:inName];
	if (theContacts.count == 0) return nil;

	return theContacts[0];
}

-(NSMutableArray*)findContacts
{
	return [FXDatabase findContacts:nil];
}

@end
