/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"

@class ASAccount;
@class ASFolder;

typedef enum {
    kSearchSuccess                  = 1,
    kSearchRequstInvalid            = 2,
    kSearchServerError              = 3,
    kSearchBadLink                  = 4,
    kSearchAccessDenied             = 5,
    kSearchNotFound                 = 6,
    kSearchConnectionFailed         = 7,
    kSearchTooComplex               = 8,
    kSearchTimedOut                 = 10,
    kSearchFolderSyncRequired       = 11,
    kSearchEndOfRetrivableRange     = 12,
    kSearchAccessBlocked            = 13,
    kSearchCredentialsRequired      = 14
} ESearchStatus;

typedef enum {
    kSearchCmdMailbox               = 1
} ESearchCmd;

@interface ASSearch : WBXMLRequest {
}
@property (nonatomic) ESearchStatus     statusCodeAS;
@property (nonatomic) ESearchCmd        cmd;

@property (nonatomic,strong) ASFolder*  folder;
@property (nonatomic,strong) NSString*  query;
@property (nonatomic) NSRange           range;

@property (nonatomic) NSUInteger        total;

@property (nonatomic,strong) NSMutableArray*  results;

// Factory

// Email search
+ (ASSearch*)sendSearchMailbox:(ASFolder*)aFolder
                         query:(NSString*)aQuery
                         range:(NSRange)aRange
                 displayErrors:(BOOL)aDisplayErrors;

+ (ASSearch*)queueSearchMailbox:(ASFolder*)aFolder
                          query:(NSString*)aQuery
                          range:(NSRange)aRange
                  displayErrors:(BOOL)aDisplayErrors;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end
