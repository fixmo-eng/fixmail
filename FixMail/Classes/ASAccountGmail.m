/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASAccountGmail.h"
#import "ASFolder.h"

@implementation ASAccountGmail

// Constants
//static NSString* kServerIdInbox         = @"Mail:DEFAULT";
//static NSString* kServerIdDeleted       = @"Mail:Deleted Messages";
static NSString* kServerIdAllMail       = @"Mail:^all";
//static NSString* kServerIdSentItems     = @"Mail:^f";
//static NSString* kServerIdTrash         = @"Mail:^k";
//static NSString* kServerIdDrafts        = @"Mail:^r";
//static NSString* kServerIdSpam          = @"Mail:^s";
static NSString* kServerIdStarred       = @"Mail:^t";
//static NSString* kServerIdNotes         = @"Mail:Notes";
//static NSString* kServerIdGmailGroup    = @"Mail:^sync_gmail_group";
//static NSString* kServerIdCalendar      = @"Event:DEFAULT";
//static NSString* kServerIdContacts      = @"Contact:DEFAULT";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccountNum:(int)anAccountNum
{
    if (self = [super initWithAccountNum:anAccountNum]) {
        self.accountSubType = AccountSubTypeGmail;
    }
    
    return self;  
}

- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUserName
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(EEncryptionMethod)anEncryption
    authentication:(EAuthenticationMethod)anAuthentication
       folderNames:(NSArray *)aFolderNames
{
    if (self = [super initWithName:aName
                          userName:aUserName 
                          password:aPassword 
                          hostName:aHostName 
                              path:aPath
                              port:aPort 
                        encryption:anEncryption 
                    authentication:anAuthentication 
                       folderNames:aFolderNames]) {
        self.accountSubType = AccountSubTypeGmail;
    }
    
    return self;
}


////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (UIImage*)icon
{
    return [UIImage imageNamed:@"FixMailRsrc.bundle/settingsAccountGmailIcon.png"];
}

- (NSArray*)foldersToDisplay
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:folders.count];

    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeInbox];
    [self addFolderToArray:aFolders serverId:kServerIdStarred];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeDrafts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeLocalDrafts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeCalendar];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeContacts];
    
    [super folderHierarchyFromFolders:folders toFolders:aFolders];
    
    return aFolders;
}

- (NSArray*)foldersToSync
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:folders.count];
    
#if 1
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeInbox];
    [self addFolderToArray:aFolders serverId:kServerIdStarred];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeDrafts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeCalendar];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeContacts];
    for(ASFolder* aFolder in folders) {
        // All mail folder causes sync to break and go in to an infinite loop with no sync key changes but more available set
        // It also doubles the sync traffics and its mostly redundant so we are suppressing it for now
#warning FIXME need to special case the AllMail folder put it on all email entries
        if([aFolder.uid isEqualToString:kServerIdAllMail]) {
            continue;
        }
        
        //FXDebugLog(kFXLogActiveSync, @"foldersToSync %@ type=%d syncable=%d", aFolder.displayName, aFolder.folderType, [aFolder isSyncable]);
        if(aFolder.isSyncable && !aFolder.isDeleted) {
            if(![aFolders containsObject:aFolder]) {
                [aFolders addObject:aFolder];
            }
        }else{
            if([aFolders containsObject:aFolder]) {
                [aFolders removeObject:aFolder];
            }
            if(!aFolder.isDeleted) {
                FXDebugLog(kFXLogActiveSync, @"Folder not syncable: %@ %@", [aFolder displayName], [aFolder uid]);
            }
        }
    }
#else
    // This is for debugging only
    //
    //[self addFolderToArray:aFolders serverId:kServerIdInbox];
    //[self addFolderToArray:aFolders serverId:kServerIdStarred];
    //[self addFolderToArray:aFolders serverId:kServerIdSentItems];
    //[self addFolderToArray:aFolders serverId:kServerIdDrafts];
    [self addFolderToArray:aFolders serverId:kServerIdCalendar];
    //[self addFolderToArray:aFolders serverId:kServerIdContacts];
    
    // GMail specific folders
    //[self addFolderToArray:aFolders serverId:kServerIdGmailGroup];
    //[self addFolderToArray:aFolders serverId:kServerIdAllMail];
    //[self addFolderToArray:aFolders serverId:kServerIdSpam];
    //[self addFolderToArray:aFolders serverId:kServerIdTrash];
#endif
    return aFolders;
}

- (NSArray*)specialFoldersToSync
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:10];
    
    // Gmail calendar folder causes an HTTP 403 if you try to sync it and user hasn't
    // configured a Google calendar so it has to be tested seperately to see if it works
    //
    [aFolders addObjectsFromArray:[self foldersForFolderType:kFolderTypeCalendar]];
    return aFolders;
}

- (BOOL)isFilterTypeSupported:(EFilterType)aFilterType folderType:(EFolderType)aFolderType
{
    // Gmail seems to support all filter types on email and calendars
    return TRUE;
}

- (BOOL)emailCanBeInMultipleFolders
{
    return TRUE;
}

@end
