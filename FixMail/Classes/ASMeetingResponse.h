/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "WBXMLRequest.h"
#import "ASMeetingResponseDelegate.h"

@class ASEmail;

typedef enum
{
    kUserResponseAccept             = 1,
    kUserResponseTentativelyAccept  = 2,
    kUserResponseDecline            = 3
} EUserResponse;

typedef enum {
    kMeetingResponseSuccess             = 1,
    kMeetingResponseInvalidRequest      = 2,
    kMeetingResponseServerError3        = 3,
    kMeetingResponseServerError4        = 4
} EMeetingResponseStatus;

@interface ASMeetingResponse : WBXMLRequest {
    ASEmail*                                email;
    NSObject<ASMeetingResponseDelegate>*    delegate;

}

// Properties
@property (nonatomic,strong) ASEmail*                   email;
@property (strong) NSObject<ASMeetingResponseDelegate>* delegate;
@property (nonatomic) EMeetingResponseStatus            statusCodeAS;



// Factory
+ (void)sendMeetingResponse:(ASEmail*)anEmail
               userResponse:(EUserResponse)aUserResponse
               instanceDate:(NSDate*)anInstanceDate
                   delegate:(NSObject<ASMeetingResponseDelegate>*)aDelegate;

// Construct
- (id)initWithEmail:(ASEmail*)aFolder;

@end
