/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "FolderCell.h"
#import "ASFolder.h"

@implementation FolderCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) return nil;

	return nil;
}

- (void) setFolder:(BaseFolder *)inFolder
{
	if (inFolder == _folder) return;
	_folder = inFolder;
//	[_folder setIsCountWanted:TRUE];

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderSyncingNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderSyncedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderNewObjectsNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderChangeObjectNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderDeleteUidNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderCountingNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification:) name:kFolderCountedNotification object:nil];


	[self redoControls];
}

- (void) redoControls
{
	if (_folder == nil) {
		self.folderImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIcon.png"];
		self.nameLabel.text = @"<Unknown>";
		self.unreadLabel.text = @"";
		[self.activityIndicator stopAnimating];
		self.activityIndicator.hidden = true;
		return;
	}

	switch(_folder.folderType) {
		case kFolderTypeCalendar:
			self.folderImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIconCalendar.png"];
			break;
		case kFolderTypeContacts:
			self.folderImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIconContacts.png"];
			break;
		default:
			self.folderImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIcon.png"];
			break;
	}
	self.nameLabel.text = _folder.displayName;

	// check on the status of the folder
	if ((_folder.isSyncing) || (_folder.isResyncing)) {
		self.activityIndicator.hidden = false;
		[self.activityIndicator startAnimating];
		self.unreadLabel.text = @"";
		FXDebugLog(kFXLogASFolder, @"FolderCell (%@): Syncing", _folder.displayName);
	} else if (_folder.isCounting) {
		[self.activityIndicator stopAnimating];
		self.activityIndicator.hidden = true;
		self.unreadLabel.text = @"~";
		FXDebugLog(kFXLogASFolder, @"FolderCell (%@): Counting", _folder.displayName);
	} else if (_folder.isInitialSynced) {
		[self.activityIndicator stopAnimating];
		self.activityIndicator.hidden = true;
        if (_folder.unread) {
            self.unreadLabel.text = [NSString stringWithFormat:@"%d", _folder.unread];
        } else {
            self.unreadLabel.text = @"";
        }
		FXDebugLog(kFXLogASFolder, @"FolderCell (%@): Synced... unread %@", _folder.displayName, self.unreadLabel.text);
    } else if (!_folder.isSyncable) {
		[self.activityIndicator stopAnimating];
		self.activityIndicator.hidden = true;
		self.unreadLabel.text = [NSString stringWithFormat:@"%d", _folder.unread];
		FXDebugLog(kFXLogASFolder, @"FolderCell (%@): Local... count %@", _folder.displayName, self.unreadLabel.text);
	} else {
		[self.activityIndicator stopAnimating];
		self.activityIndicator.hidden = true;
		self.unreadLabel.text = @"-";
		FXDebugLog(kFXLogASFolder, @"FolderCell (%@): Unknown... flags %u", _folder.displayName, _folder.flagsToStore);
	}

	return;
}

- (void) updateNotification:(NSNotification *)inNotification
{
	if (inNotification.object != self.folder) return;

	[self redoControls];
}

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end