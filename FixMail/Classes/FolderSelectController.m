/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "FolderSelectController.h"
#import "AppSettings.h"
#import "ASSync.h"
#import "BaseFolder.h"
#import "EmailProcessor.h"
#import "ErrorAlert.h"
#import "FolderCell.h"
#import "ProgressView.h"
#import "SyncManager.h"
#import "composeMailVC.h"
#import "MailboxViewController.h"
#import "UIViewController+MGSplitViewController.h"

@interface FolderSelectController ()
{
    BOOL alreadyBeenDisplayed;
}
@end

@implementation FolderSelectController

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

-(void)viewDidLoad
{
	[super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:@"FolderCell" bundle:[FXLSafeZone getResourceBundle]] forCellReuseIdentifier:@"FolderCell"];

	if (!IS_IPAD()) {
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(toggleCancel:)];;
	}

	return;
}

-(void)viewWillAppear:(BOOL)animated
{
	[self loadAllFolders];
    
    [self.tableView reloadData];
}


- (void)viewDidAppear:(BOOL)animated 
{
    [super viewDidAppear:animated];
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)didReceiveMemoryWarning
{
	FXDebugLog(kFXLogActiveSync, @"AccountViewController received memory warning!");
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling
 
- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"ACCOUNT_VIEW", @"ErrorAlert", @"Error alert view title for AccountViewController") where:where exception:e];
}


////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (bool) addFolder:(BaseFolder *)inFolder
{
	if (inFolder == nil) return false;
	if (inFolder.isDeleted) return false;
	if (!inFolder.isVisible) return false;

	NSMutableArray *theArray = nil;
	switch (inFolder.folderType) {
		case kFolderTypeInbox:
		case kFolderTypeDrafts:
		case kFolderTypeDeleted:
		case kFolderTypeSent:
		case kFolderTypeOutbox:
			theArray = self.mainFolders;
			break;
		default:
			theArray = self.secondaryFolders;
			break;

		// don't add these
		case kFolderTypeContacts:
		case kFolderTypeCalendar:
			break;
	}

	if (theArray == nil) return false;
	if ([theArray containsObject:inFolder]) return false;

	[theArray addObject:inFolder];

	return true;
}

- (bool) deleteFolder:(BaseFolder *)inFolder
{
	if (inFolder == nil) return false;
	if (inFolder.isDeleted) return false;
	if (!inFolder.isVisible) return false;

	NSMutableArray *theArray = nil;
	if ([self.mainFolders containsObject:inFolder]) {
		theArray = self.mainFolders;
	} else if ([self.secondaryFolders containsObject:inFolder]) {
		theArray = self.secondaryFolders;
	}

	if (theArray == nil) return false;

	[theArray removeObject:inFolder];

	return true;
}


- (void) loadAllFolders
{
	if (self.mainFolders) {
        [self.mainFolders removeAllObjects];
    } else {
        self.mainFolders = [[NSMutableArray alloc] initWithCapacity:12];
    }
 	if (self.secondaryFolders) {
        [self.secondaryFolders removeAllObjects];
    } else {
        self.secondaryFolders = [[NSMutableArray alloc] initWithCapacity:12];
    }
   
    @try {
        NSArray* aFolders = [self.currentFolder.account foldersToDisplay];
        for(BaseFolder* aFolder in aFolders) {
			[self addFolder:aFolder];
        }
    } @catch (NSException* e) {
        [self _logException:@"loadAllFolders" exception:e];
    }

	return;
}

-(BaseFolder *) getFolderForIndexPath:(NSIndexPath *)inIndexPath
{
	NSArray *theFolders = nil;
	if (inIndexPath.section == 1) {
		theFolders = self.secondaryFolders;
	} else if (inIndexPath.section == 0) {
		if (self.mainFolders.count > 0) {
			theFolders = self.mainFolders;
		} else {
			theFolders = self.secondaryFolders;
		}
	}

	NSAssert(theFolders != nil, @"theFolders != nil");
	NSAssert(inIndexPath.row < theFolders.count, @"inIndexPath.row < theFolders");

	return theFolders[inIndexPath.row];
}

- (IBAction) toggleCancel:(id)sender
{
	NSAssert(self.delegate != nil, @"self.delegate != nil");

	[self.delegate folderViewController:self didSelectFolder:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////
// AccountDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark AccountDelegate

- (void)folderAdded:(BaseFolder*)aFolder
{
    if(![self addFolder:aFolder]) return;
	[self.tableView reloadData];
}

- (void)folderDeleted:(BaseFolder*)aFolder
{
    if(![self deleteFolder:aFolder]) return;
	[self.tableView reloadData];
}

- (void)folderUpdated:(BaseFolder*)aFolder
{
	[self.tableView reloadData];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
	int sectionCount = 0;
	if (self.mainFolders.count > 0) sectionCount++;
	if (self.secondaryFolders.count > 0) sectionCount++;

    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// take into account 0 mainFolders (admittedly unlikely)
	int theCount = 0;
	if (section == 1) {
		theCount = self.secondaryFolders.count;
	} else {
		if (self.mainFolders.count > 0) theCount = self.mainFolders.count;
		else theCount = self.secondaryFolders.count;
	}

    return theCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString* aCellIdentifier = @"FolderCell";

	FolderCell *aFolderCell = [tableView dequeueReusableCellWithIdentifier:aCellIdentifier];
	NSAssert(aFolderCell != nil, @"aFolderCell != nil");

	aFolderCell.folder = [self getFolderForIndexPath:indexPath];
	aFolderCell.selectionStyle = (aFolderCell.folder == self.currentFolder) ? UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleGray;
	aFolderCell.nameLabel.textColor = (aFolderCell.folder == self.currentFolder) ? [UIColor lightGrayColor] : [UIColor blackColor];
	aFolderCell.folderImageView.alpha = (aFolderCell.folder == self.currentFolder) ? 0.5 : 1.0;

    return aFolderCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	NSAssert(self.delegate != nil, @"self.delegate != nil");

	BaseFolder *aFolder = [self getFolderForIndexPath:indexPath];
	if (aFolder != self.currentFolder) {
		[self.delegate folderViewController:self didSelectFolder:aFolder];
	}

	return;
}



@end

