/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASContactFolder.h"
#import "ASContact.h"
#import "ASSync.h"
#import "CommitObjects.h"
#import "TraceEngine.h"
#import "FMSplitViewDelegate.h"
#import "FXDatabase.h"

#import "FXContactsHeader.h"
#import "ASContact+FMDatabase.h"
#import "listContactsVC.h"
#import "showContactVC.h"
#import "SearchRunner.h"
#import "MGSplitViewController.h"

@implementation ASContactFolder

// Static
//NSUInteger sNumContacts = 0;

////////////////////////////////////////////////////////////////////////////////
// BaseFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark BaseFolder Overrides

////////////////////////////////////////////////////////////////////////////////
// Change Handlers
////////////////////////////////////////////////////////////////////////////////
#pragma mark Change Handlers

- (void)changeObject:(BaseObject*)aBaseObject
          changeType:(EChange)aChangeType
        updateServer:(BOOL)updateServer
      updateDatabase:(BOOL)updateDatabase
{
    @try {
        ASContact* anEmail = (ASContact*)aBaseObject;
        
        // Notify clients of change
        //
        //FXDebugLog(kFXLogActiveSync, @"folder %@ changeObject delegates=%d", self.displayName, delegates.count);
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(changeObject:) withObject:anEmail waitUntilDone:FALSE];
        }
    }@catch (NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
}

- (void)deleteUid:(NSString*)aUid updateServer:(BOOL)updateServer
{
    @try {
        
        //if([[TraceEngine engine] traceType] == kTraceTypeMinimal) {
        //    sNumContacts--;
        //    FXDebugLog(kFXLogTrace, @"%d delete contact %@", sNumContacts, aUid);
        //}

        // Notify clients of deletion
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(deleteUid:) withObject:aUid waitUntilDone:FALSE];
        }
        if(updateServer) {
            [ASSync queueDeleteUid:aUid folder:self];
        }

		// get rid of the record from the DB and alert using notifications
		[FXDatabase deleteContactWithServerID:aUid];
		[[NSNotificationCenter defaultCenter] postNotificationName:kContactDeletedFromServerNotification
															object:self
														  userInfo:[NSDictionary dictionaryWithObject:aUid
																							   forKey:kUserInfoContactServerID]];

    }@catch (NSException* e) {
        [self logException:@"deleteUid" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// ASFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolder Overrides

- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType
{
    @try {
        ASContact* aContact;
        if(anObject) {
            aContact = (ASContact*)anObject;
        }else{
            aContact = [[ASContact alloc] initWithFolder:self];
			aContact.contactType = contactUser;
        }
        
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                {
                    [aContact setServerId:[aParser getStringTraceable]];
                    break;
                }
                case SYNC_APPLICATION_DATA:
                {
                    [aContact parser:aParser tag:tag];
                    //if([[TraceEngine engine] traceType] == kTraceTypeMinimal) {
                    //    FXDebugLog(kFXLogTrace, @"%d contact %@", sNumContacts, aContact.serverId);
                    //    sNumContacts++;
                    //}
					[FXDatabase commitChanges:@[aContact]];
					[[NSNotificationCenter defaultCenter] postNotificationName:kContactAddedFromServerNotification
																		object:self
																	  userInfo:[NSDictionary dictionaryWithObject:aContact.serverId
																										   forKey:kUserInfoContactServerID]];
                    break;
                }
                case SYNC_STATUS:
                    [ASFolder displaySyncStatus:(ESyncStatus)[aParser getIntTraceable]];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        
        [self addObject:aContact];
        
    }@catch (NSException* e) {
        [self logException:@"Contact Add" exception:e];
    }
}

- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [self deleteUid:[aParser getStringTraceable] updateServer:FALSE];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Contact Delete" exception:e];
    }
}

- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        ASContact* aContact = nil;
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                {	NSString *serverID = [aParser getStringTraceable];
					aContact = [FXDatabase getContactWithServerID:serverID];
					// now reset the damn thing
					[aContact reset];
					aContact.serverId = serverID;
                    break;
                }
                case SYNC_APPLICATION_DATA:
					if (aContact != nil) {
						[aContact parser:aParser tag:tag];
						[FXDatabase commitChanges:@[aContact]];
						[[NSNotificationCenter defaultCenter] postNotificationName:kContactUpdatedFromServerNotification
																			object:self
																		  userInfo:[NSDictionary dictionaryWithObject:aContact.serverId
																											   forKey:kUserInfoContactServerID]];
					}
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Contact Change" exception:e];
    }
}

#pragma mark - ASSync methods


- (DBaseEngine*)databaseEngine
{
    return [FXDatabase sharedManager];
}

//
// This is called at the end of an ASSync call and should be our main point
// when changing the underlying DB.
//
// Note: it looks like Ed is spinning these commits off to a separate operation queue
//
- (void)commitObjects
{
	if((self.objectsAdded.count == 0) && (self.objectsChanged.count == 0) && (self.objectsDeleted.count == 0)) return;

//	CommitObjects* aCommitObjects = [[CommitObjects alloc] initWithFolder:self];
	NSMutableArray *contactsArray;

	//
	// Contacts added
	//
	[contactsArray removeAllObjects];
	for (id theObject in self.objectsAdded) {
		if (![theObject isKindOfClass:[ASContact class]]) continue;
		[contactsArray addObject:theObject];
	}
	if (contactsArray.count > 0) {
		for (ASContact *theContact in contactsArray) {
			[theContact addToContactsDB:contactUser];
		}
	}

	//
	// Contacts changed
	//
	[contactsArray removeAllObjects];
	for (id theObject in self.objectsChanged) {
		if (![theObject isKindOfClass:[ASContact class]]) continue;
		[contactsArray addObject:theObject];
	}
	if (contactsArray.count > 0) {
        if (self.isInitialSynced) {
            for (ASContact *theContact in contactsArray) {
                // send out a notification for each changed object since we have no idea who is displaying what
                [[NSNotificationCenter defaultCenter] postNotificationName:kContactUpdatedFromServerNotification
                                                                    object:self
                                                                  userInfo:[NSDictionary dictionaryWithObject:theContact.serverId
                                                                                                       forKey:kUserInfoContactServerID]
                 ];
            }
		}
		[FXDatabase commitChanges:contactsArray];
	}
	self.objectsChanged = nil;

	//
	// Contacts deleted
	//
	[contactsArray removeAllObjects];
	for (id theObject in self.objectsDeleted) {
		if (![theObject isKindOfClass:[ASContact class]]) continue;
		[contactsArray addObject:theObject];
	}
	if (contactsArray.count > 0) {
		for (ASContact *theContact in contactsArray) {
			[FXDatabase deleteContact:theContact];
			[[NSNotificationCenter defaultCenter] postNotificationName:kContactDeletedFromServerNotification
																object:self
															  userInfo:[NSDictionary dictionaryWithObject:theContact.serverId
																								   forKey:kUserInfoContactServerID]
			 ];
		}
	}
	self.objectsDeleted = nil;

	return;
}

/*
- (void)commitChanges:(NSArray*)anObjects
{
#warning FIXME commitChanges
    for(ASContact* aContact in anObjects) {
       // [self changeObject:aContact changeType:kChangeEvent updateServer:FALSE updateDatabase:TRUE];
    }
}

- (void)deleteDatabaseForFolder
{
#warning FIXME deleteDatabaseForFolder
    FXDebugLog(kFXLogFIXME, @"FIXME deleteDatabaseForFolder: %@", self.displayName);
}
*/
@end
