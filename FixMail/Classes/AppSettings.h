//
//  AppSettings.h
//  NextMailIPhone
//
//  Created by Gabor Cselle on 2/3/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "BaseAccount.h"
#import "Calendar.h"

typedef enum {
	fixMailEdition = 1 
} fixmail_edition_enum;

typedef enum {
	calendarEvents1Day = 1,
	calendarEvents2Days,
	calendarEvents3Days,
	calendarEvents4Days,
	calendarEvents5Days,
	calendarEvents6Days,
	calendarEvents1Week,
	calendarEvents2Weeks,
	calendarEvents3Weeks,
	calendarEvents1Month,
	calendarEvents2Months,
	calendarEvents3Months,
	calendarEvents4Months,
	calendarEvents5Months,
	calendarEvents6Months,
	calendarEventsForever
} ECalendarEvents;

typedef enum {
	calendarStartOnSunday = 0,
	calendarStartOnMonday,
	calendarStartOnTuesday,
	calendarStartOnWednesday,
	calendarStartOnThursday,
	calendarStartOnFriday,
	calendarStartOnSaturday
} ECalendarStartDay;

typedef enum {
	sortOrderLastFirst = 0,
	sortOrderFirstLast = 1
} EContactSortOrder;

typedef enum {
	emailBodyText = 0,
	emailBodyHTML = 1,
	emailBodyTextHTML = 2
} EEmailBodyType;

@interface AppSettings : NSObject {
}

+(fixmail_edition_enum)fixMailEdition;

// Bundle Information
+(NSString*)appID;
+(NSString*)version;
+(NSString*)appName;

// Device Information
+(NSString*)deviceID;
+(NSString*)deviceType;
+(NSString*)systemVersion;
+(NSString*)model;

// Misc. settings

+(int)searchCount;
+(void)incrementSearchCount;

+(BOOL)reset;
+(void)setReset:(BOOL)value;

+(int)interval;

+(void)setLastpos:(NSString*)y;
+(NSString*)lastpos;

+(NSString*)dataInitVersion;
+(void)setDataInitVersion;

+(int)datastoreVersion;
+(void)setDatastoreVersion:(int)value;
	
+(int)globalDBVersion;
+(void)setGlobalDBVersion:(int)version;

// ActiveSync Settings in root.plist
+(NSString*)emailTimeFilter;
+(void)setEmailTimeFilter:(NSString*)value;

+(NSString*)calendarTimeFilter;
+(void)setCalendarTimeFilter:(NSString*)value;

+(BOOL)autoSync;
+(void)setAutoSync:(BOOL)value;

+(NSString*)bodyFormat;
+(void)setBodyFormat:(NSString*)value;

+(ECalendarViewType)calendarViewType;
+(void)setCalendarViewType:(ECalendarViewType)value;

+(NSString*)cacUserName;
+(void)setCacUserName:(NSString*)cacUserName;

//
// Settings
//
+ (void) resetUserSettings;

//
// Viewing
//
+ (int) settingsSection;
+ (void) setSettingsSection:(int)inValue;
+ (int) settingsRow;
+ (void) setSettingsRow:(int)inValue;

//
// Email
//
+ (int) emailRecentMessages;
+ (void) setEmailRecentMessages:(int)inValue;
+ (int) emailPreviewLines;
+ (void) setEmailPreviewLines:(int)inValue;
+ (BOOL) emailAskBeforeDeleting;
+ (void) setEmailAskBeforeDeleting:(BOOL)inValue;
+ (BOOL) emailSMIME;
+ (void) setEmailSMIME:(BOOL)inValue;
+ (void) setEmailReplyTo:(NSString*)inString;
+ (NSString *) emailReplyTo;
+ (void) setEmailSignature:(NSString*)inString;
+ (NSString *) emailSignature;
+ (EEmailBodyType) emailBodyType;
+ (void) setEmailBodyType:(EEmailBodyType)inValue;


//
// Calendar
//
+ (BOOL) calendarReminders;
+ (void) setCalendarReminders:(BOOL)inValue;

+ (ECalendarEvents) calendarEvents;
+ (void) setCalendarEvents:(ECalendarEvents)inValue;

+ (ECalendarStartDay) calendarStartDayOn;
+ (void) setCalendarStartDayOn:(ECalendarStartDay)inValue;

//
// Contacts
//
+ (EContactSortOrder) contactsSortOrder;
+ (void) setContactsSortOrder:(EContactSortOrder)inValue;

+ (EContactSortOrder) contactsDisplayOrder;
+ (void) setContactsDisplayOrder:(EContactSortOrder)inValue;

@end
