/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"
#import "ASEmail.h"
#import "ASSyncDelegate.h"
#import "ASSyncEventDelegate.h"
#import "ASSyncContactDelegate.h"
#import "ASSyncObjectDelegate.h"

@class Event;
@class ASCalendarFolder;

typedef enum {
    kFilterNone                             = 0,
    kFilter1DayAgo                          = 1,
    kFilter3DaysAgo                         = 2,
    kFilter1WeekAgo                         = 3,
    kFilter2WeeksAgo                        = 4,
    kFilter1MonthAgo                        = 5,
    kFilter3MonthsAgo                       = 6,
    kFilter6MonthsAgo                       = 7,
    kFilterIncompleteTasks                  = 8
} EFilterType;

@interface ASSync : WBXMLRequest {
    NSMutableDictionary*                message;    // Message dictionary being parsed
    ASFolder*                           folder;     // Current folder being parsed
}

// Properties
@property (nonatomic, strong) ASFolder*                     folder;
@property (nonatomic, readwrite) ESyncStatus                statusCodeAS;

@property (strong) NSObject<ASSyncDelegate>*                syncDelegate;           // Delegates
@property (strong) NSObject<ASSyncEventDelegate>*           eventDelegate;
@property (strong) NSObject<ASSyncContactDelegate>*         contactDelegate;
@property (strong) NSObject<ASSyncObjectDelegate>*          syncObjectDelegate;

@property (strong) NSMutableArray*                          moreAvailableFolders;   // Folders which need additional syncs
@property (strong) NSArray*                                 syncFolders;            // Folders being synced
@property (strong) NSArray*                                 pingFolders;            // Folders to ping when sync is complete

@property (nonatomic, readwrite) BOOL                       isChangeRequest;

@property (strong) BaseObject*                              object;                 // Object being fetched, added or changed
@property (nonatomic, readwrite) EBodyType                  bodyType;
@property (nonatomic, readwrite) EBodyFetchType             bodyFetchType;          // No body, preview(plain text truncated) or full body

// Factory
//+ (void)sendSync:(ASAccount*)anAccount folder:(ASFolder*)aFolder withPing:(BOOL)aWithPing delegate:(NSObject<ASSyncDelegate>*)aDelegate;

// Sync
+ (ASSync*)sendSync:(ASAccount*)anAccount
        syncFolders:(NSArray*)aSyncFolders 
        pingFolders:(NSArray*)aPingFolders
      bodyFetchType:(EBodyFetchType)aBodyFetchType
           delegate:(NSObject<ASSyncDelegate>*)aDelegate;
+ (void)queueDeleteUid:(NSString*)aUid folder:(ASFolder*)aFolder;
+ (ASSync*)sendDeleteUid:(NSString*)aUid folder:(ASFolder*)aFolder;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end
