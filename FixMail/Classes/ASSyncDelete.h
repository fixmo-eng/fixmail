/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncObject.h"

@class ASFolder;

@interface ASSyncDelete : ASSyncObject {
    ASFolder*   folder;
    NSString*   uid;
}

@property (nonatomic,strong) ASFolder*  folder;
@property (nonatomic,strong) NSString*  uid;

- (id)initWithFolder:(ASFolder*)aFolder uid:(NSString*)aUid;

- (void)send;

@end