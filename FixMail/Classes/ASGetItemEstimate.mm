#if 0 // Not Implemented
/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASGetItemEstimate.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASSync.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASGetItemEstimate

// Properties
@synthesize delegate;

// Constants
static NSString* kCommand       = @"GetItemEstimate";
static const ASTag kCommandTag  = GET_ITEM_ESTIMATE_GET_ITEM_ESTIMATE;

// Types
//
typedef enum {
    kGetItemEstimateSuccess             = 1,
    kGetItemEstimateCollectionInvalid   = 2,
    kGetItemEstimateSyncStateNotPrimed  = 3,
    kGetItemEstimateSyncKeyInvalid      = 4
} EGetItemEstimateStatus;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendGetEstimateForSync:(NSArray*)aFolders
                      delegate:(NSObject<ASGetItemEstimateDelegate>*)aDelegate
{
#warning FIXME  Implement delegate to return results to ASAccount
    if([aFolders count] > 0) {
        ASAccount* anAccount = (ASAccount*)[[aFolders objectAtIndex:0] account];
        ASGetItemEstimate* aGetItemEstimate = [[ASGetItemEstimate alloc] initWithAccount:anAccount];
        aGetItemEstimate.delegate        = aDelegate;
        
        NSData* aWbxml = [aGetItemEstimate wbxmlGetEstimate:aFolders];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aGetItemEstimate];
    }else{
        FXDebugLog(kFXLogActiveSync, @"sendGetEstimateForSync no folder");
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlGetEstimate:(NSArray*)aFolders
{
	WBXMLDataGenerator wbxml;
    
#warning FIXME  Works on Gmail at AS version 12.0, not on Fixmo Exchange server at 12.1

	wbxml.start(kCommandTag); {
        wbxml.start(GET_ITEM_ESTIMATE_COLLECTIONS); {
            for(ASFolder* aFolder in aFolders) {
                wbxml.start(GET_ITEM_ESTIMATE_COLLECTION); {
                    wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                    wbxml.keyValue(GET_ITEM_ESTIMATE_COLLECTION_ID, [aFolder uid]);
                    wbxml.start(SYNC_OPTIONS); {
                        wbxml.keyValueInt(SYNC_FILTER_TYPE, [account emailFilterType]);
                        wbxml.keyValue(SYNC_CLASS, @"Email");
                    }wbxml.end();
                }wbxml.end();
            }
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
        delegate                = nil;
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setStatus:(EGetItemEstimateStatus)aStatus
{
    switch(aStatus) {
        case kGetItemEstimateSuccess:
            break;
        case kGetItemEstimateCollectionInvalid:
            [HttpRequest handleASError:@"Get item estimate collection invalid"]; break;
        case kGetItemEstimateSyncStateNotPrimed:
            [HttpRequest handleASError:@"Get item estimate sync state not primed"]; break;
        case kGetItemEstimateSyncKeyInvalid :
            [HttpRequest handleASError:@"Get item estimate sync key invalid"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aStatus]; break;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case GET_ITEM_ESTIMATE_RESPONSE:
                    break;
                case GET_ITEM_ESTIMATE_STATUS:
                    [self setStatus:(EGetItemEstimateStatus)[aParser getInt]];
                    break;
                case GET_ITEM_ESTIMATE_COLLECTION:
                    break;
                case GET_ITEM_ESTIMATE_COLLECTION_ID:
                    FXDebugLog(kFXLogActiveSync, @"ITEM_ESTIMATE_COLLECTION_ID = %@", [aParser getString]);
                    break;
                case GET_ITEM_ESTIMATE_ESTIMATE:
                    FXDebugLog(kFXLogActiveSync, @"ITEM_ESTIMATE_ESTIMATE = %d", [aParser getInt]);
                    break;
                default:
                    [aParser skipTag]; break;
            }
        } 
    }@catch (NSException* e) {
        [HttpRequest logException:@"Item Estimate response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate


- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d  %@ headers:\n%@", kCommand, statusCode, aContentType, aHeaders);
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
	if(statusCode == 200) {        
        [self parseWBXMLData:data command:kCommandTag];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, statusCode);
    }
    [super connectionDidFinishLoading:connection];
}

@end
#endif