//
//  EmailDetailViewController.h
//  FixMail
//
//  Created by Sean Langley on 11/14/12.
//
//

#import <UIKit/UIKit.h>
#import "SZCComposeDelegate.h"

@interface PlaceholderSplitviewDetailViewController : UIViewController

@property (nonatomic, strong) UIViewController<SZCComposeDelegate> *composeDelegate;
@end
