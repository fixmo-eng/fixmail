/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseAccount.h"
#import "AccountDBAccessor.h"
#import "ASAccountDomino.h"
#import "ASAccountExchange.h"
#import "ASAccountGmail.h"
#import "ASEmail.h"
#import "AppSettings.h"
#import "BaseContact.h"
#import "BaseFolder.h"
#import "EmailProcessor.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "FXDatabase.h"
#import "GlobalDBFunctions.h"
#import "Reachability.h"
#import "SyncManager.h"

@implementation BaseAccount

// Properties
@synthesize folders;

// Persistent Store properties
@synthesize accountType;
@synthesize accountSubType;

@synthesize accountNum;
@synthesize name;
@synthesize userName;
@synthesize pcc;
@synthesize emailAddress;
@synthesize password;           // FIXME - Needs to be protected
@synthesize hostName;
@synthesize path;
@synthesize domain;
@synthesize port;
@synthesize encryption;
@synthesize authentication;
@synthesize deleted;
@synthesize isCreated;
@synthesize isReachable;
@synthesize reachability;
@synthesize ASVersion;

// Constants
static NSString* kPersistentStoreVersionKey     = @"__version";
static NSString* kFolderStates                  = @"folderStates";

static NSString* kAccountNum                    = @"accountNum";
static NSString* kPort                          = @"port";
static NSString* kEncryption                    = @"encryption";
static NSString* kAuthentication                = @"authentication";
static NSString* kDeleted                       = @"deleted";
static NSString* kUserName                      = @"userName";
static NSString* kPCC                           = @"pcc";
static NSString* kName                          = @"name";
static NSString* kHostName                      = @"hostName";
static NSString* kPath                          = @"path";
static NSString* kPassword                      = @"p";
static NSString* kDomain                        = @"domain";

// Static
static sqlite3_stmt*        accountInsertStmt   = nil;
static sqlite3_stmt*        accountFindStmt     = nil;
static sqlite3_stmt*        accountLoadStmt     = nil;
static sqlite3_stmt*        accountCountStmt    = nil;
static sqlite3_stmt*        updateSyncKeyStmt   = nil;
static sqlite3_stmt*        updatePolicyKeyStmt = nil;
static sqlite3_stmt*        updateEmailAddressStmt = nil;

static int                  kAccountDatabaseVersion = 1;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSMutableArray* sAccounts = NULL;

+ (BaseAccount*)currentLoggedInAccount
{
	if (sAccounts.count == 0) return nil;
	return sAccounts[0];
}

+ (NSUInteger)numAccounts
{
    return [sAccounts count];
}

+ (NSArray*)accounts
{
    return sAccounts;
}

+ (BaseAccount*)accountForAccountNumber:(int)anAccountNumber
{
    BaseAccount* anAccount = NULL;
    
    if(anAccountNumber >= 0 && anAccountNumber < [sAccounts count]) {
        anAccount = [sAccounts objectAtIndex:anAccountNumber];
    }
    return anAccount;
}

+ (BaseFolder*)folderForAccountNum:(int)anAccountNum folderNum:(int)aFolderNum
{
    BaseFolder* aFolder = NULL;
    
    if(anAccountNum >= 0 && anAccountNum < [sAccounts count]) {
        BaseAccount* anAccount = [sAccounts objectAtIndex:anAccountNum];
        aFolder = [anAccount folderForNum:aFolderNum];
    }else {
        FXDebugLog(kFXLogActiveSync, @"folderForAccountNum invalid account");
    }
    return aFolder;
}

+ (BaseFolder*)folderForCombinedFolderNum:(int)aCombinedFolderNum
{
    BaseFolder* aBaseFolder = NULL;

    int anAccountNum    = [EmailProcessor accountNumForCombinedFolderNum:aCombinedFolderNum];
    int aFolderNum      = [EmailProcessor folderNumForCombinedFolderNum:aCombinedFolderNum];
    aBaseFolder = [BaseAccount folderForAccountNum:anAccountNum folderNum:aFolderNum];
    if(!aBaseFolder) {
        FXDebugLog(kFXLogActiveSync, @"folderForAccountNum invalid: %d", aCombinedFolderNum);
    }
    
    return aBaseFolder;
}

+ (void)deleteAccount:(BaseAccount*)anAccount
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    [anAccount.reachability stopNotifier];
    anAccount.reachability = nil;
    [anAccount removeAllFolders];
    [anAccount deleteAllFolders];
    [sAccounts removeObject:anAccount];
}

+ (void)deleteAllAccounts
{
    [WBXMLRequest deleteQueue];

    NSUInteger aCount = sAccounts.count;
    for(NSInteger i = aCount-1 ; i >= 0 ; --i) {
        BaseAccount* anAccount = [sAccounts objectAtIndex:i];
        [BaseAccount deleteAccount:anAccount];
    }
    
    // reset - delete all data and settings
	[AppSettings setReset:NO];
	[[AccountDBAccessor sharedManager] deleteDatabase];
    
	[AppSettings setDataInitVersion];
	[AppSettings setGlobalDBVersion:0];
		
    [DBaseEngine deleteAccount];
    
	[GlobalDBFunctions closeAll];
	[GlobalDBFunctions deleteAll];
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccountNum:(int)anAccountNum
{
    if (self = [super init]) {
        if(!sAccounts) {
            sAccounts = [[NSMutableArray alloc] initWithCapacity:2];
        }
        [sAccounts addObject:self];
        
        folders             = [[NSMutableArray alloc] initWithCapacity:10];
        folderDictionary    = [[NSMutableDictionary alloc] initWithCapacity:10];
        
        accountDelegates    = [[NSMutableArray alloc] initWithCapacity:1];

        // Persistent Store
        accountNum          = anAccountNum;
    }
    
    return self;  
}

- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUserName
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(int)anEncryption
        authentication:(int)anAuthentication
       folderNames:(NSArray *)aFolderNames
{
    if (self = [super init]) {
        if(!sAccounts) {
            sAccounts = [[NSMutableArray alloc] initWithCapacity:2];
        }
        [sAccounts addObject:self];
        
        folders             = [[NSMutableArray alloc] initWithCapacity:10];
        folderDictionary    = [[NSMutableDictionary alloc] initWithCapacity:10];
        
        accountDelegates    = [[NSMutableArray alloc] initWithCapacity:1];
        
        // Persistent Store
        accountNum          = 0;
        self.name           = aName;
        self.userName       = aUserName;
        //self.emailAddress   = @"";
        self.path           = aPath;
        self.password       = aPassword;
        self.domain         = @"";
        port                = aPort;
        encryption          = anEncryption;
        authentication      = anAuthentication;
        
        [self setReachableHost:aHostName];
    }
    
    return self;
}

- (void)removeReachabilityNotifier
{
    if(self.reachability) {
        [self.reachability stopNotifier];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
        observingReachability = FALSE;
    }
}

- (void)doReachability:(Reachability*)aReachability
{
    NetworkStatus aHostStatus = aReachability.currentReachabilityStatus;
    switch(aHostStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
        {
            if(aHostStatus == ReachableViaWWAN) {
                FXDebugLog(kFXLogActiveSync, @"Server reachable via mobile: %@", self.hostName);
            }else{
                FXDebugLog(kFXLogActiveSync, @"Server reachable via WiFi: %@", self.hostName);
            }
            self.isReachable = YES;
            NSArray* aFolders = [self folders];
            for(BaseFolder* aFolder in aFolders) {
                if(aFolder.hasOfflineChanges) {
                    [aFolder syncOfflineChanges];
                }
            }
            break;
        }
        case NotReachable:
        default:
            FXDebugLog(kFXLogActiveSync, @"Server not reachable: %@", self.hostName);
            self.isReachable = NO;
            break;
    }
}

- (BOOL)setReachableHost:(NSString*)aHostName
{
    // Validate hostname
    // FIXME should I do other validity tests here?
    //
    if(aHostName.length > 3) {
        if(!observingReachability) {
            observingReachability = TRUE;
            [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification
                                                              object:nil queue:nil
                                                          usingBlock:^(NSNotification *inNotification) {
                                                              Reachability* aReachability = inNotification.object;
                                                              [self doReachability:aReachability];
                                                          }
             ];
        }
        
        if(![aHostName isEqualToString:self.hostName]) {
            self.hostName = aHostName;
            self.reachability = [Reachability reachabilityWithHostname:self.hostName];
            [self.reachability startNotifier];
        }
        [self doReachability:self.reachability];
    }else{
        // Invalid host name
        //
        FXDebugLog(kFXLogAll, @"Invalid server host name: %@", aHostName);
        [self removeReachabilityNotifier];
        self.isReachable = FALSE;
        self.hostName = aHostName;
    }
    return self.isReachable;
}

- (void)stopReachability
{
    // Virtual override
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"ACCOUNT", @"ErrorAlert", @"Error alert view title for BaseAccount") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"ACCOUNT", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Load/Store
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Store

- (void)createAccount:(NSSet*)aFoldersSelected folderPaths:(NSArray*)aFolderPaths firstSetup:(BOOL)aFirstSetup
{
    // Create tne new account
    //
    SyncManager *sm = [SyncManager getSingleton];
    
    [sm addAccountState];
    [self commitSettings];
    
    if(aFirstSetup) {
        [AppSettings setDataInitVersion];
    }
    
    // Create the user selected folder
    //
    int i = 0;
    for(NSString* folderPath in aFolderPaths) {
        if([aFoldersSelected containsObject:folderPath]) {
            if (i < [EmailProcessor folderCountLimit]-1) { // only add up do 1000 folders!
                [self setupFolderForName:folderPath];
                i++;
            }
        }
    }
    [self creationComplete];
}

- (void)create
{
    if(!self.isCreated) {
        self.isCreated = YES;
        
        // Create tne new account
        //
        SyncManager *sm = [SyncManager getSingleton];

        [sm addAccountState];
        [self commitSettings];

        if([sAccounts count] == 1) {
            [AppSettings setDataInitVersion];
        }

        // Create the user selected folder
        //
        NSUInteger aCount = [folders count];
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            BaseFolder* aFolder = [folders objectAtIndex:i];
            [self setupFolder:aFolder];
        }
        [self creationComplete];
    }else{
        FXDebugLog(kFXLogFIXME, @"account create called multiple times");
    }
}

- (void)loadFolders:(NSDictionary*)aFolderDictionary
{
    //FXDebugLog(kFXLogActiveSync, @"loadFolders: %@", aFolderDictionary);
    NSNumber* aVersion = [aFolderDictionary objectForKey:kPersistentStoreVersionKey];
    NSArray* aFolderStates = [aFolderDictionary objectForKey:kFolderStates];
    if(aVersion && aFolderStates) {
        NSUInteger aCount = [aFolderStates count];
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            NSDictionary* aFolderStore = [aFolderStates objectAtIndex:i];
            
            EFolderType aFolderType = [BaseFolder folderTypeFromStore:aFolderStore];
            
            BaseFolder* aFolder = [self createFolderForFolderType:aFolderType];
            if(aFolder) {
                [aFolder loadFromStore:aFolderStore];
                [self addFolder:aFolder];
                [aFolder setFolderNum:i];
                //FXDebugLog(kFXLogActiveSync, @"loadFolder: %@", aFolder);
                //[aFolder commit];
            }
        }
        
        // Open and version check databases, need folders loaded in event a resync is requred
        //
        [DBaseEngine open:self];

        for(BaseFolder* aFolder in self.folders) {
            [aFolder setup];
        }
        for(BaseFolder* aFolder in self.folders) {
            [aFolder foldersLoaded];
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"BaseAccount loadFolders failed");
    }
}

- (void)removeAllFolders
{
    [BaseFolder removeAllFolders];
    [folders removeAllObjects];
    [folderDictionary removeAllObjects];
}

- (void)loadSettings
{
    [self loadFromSQLWithAccountNum:accountNum];

    if(!deleted) {
        NSDictionary* aFolderStates = [SyncManager loadFolderState:accountNum];
        [self loadFolders:aFolderStates];
    }
}

- (void)commitSettings
{
    [self commitToDatabase];
}

- (void)asDictionary:(NSMutableDictionary*)aDictionary
{
    [aDictionary setObject:[NSNumber numberWithInt:accountNum]      forKey:kAccountNum];
    [aDictionary setObject:[NSNumber numberWithInt:port]            forKey:kPort];
    [aDictionary setObject:[NSNumber numberWithInt:encryption]      forKey:kEncryption];
    [aDictionary setObject:[NSNumber numberWithInt:authentication]  forKey:kAuthentication];
    [aDictionary setObject:[NSNumber numberWithInt:deleted]         forKey:kDeleted];

    [self addString:userName        toDictionary:aDictionary key:kUserName];
    [self addString:pcc             toDictionary:aDictionary key:kPCC];
    [self addString:name            toDictionary:aDictionary key:kName];
    [self addString:hostName        toDictionary:aDictionary key:kHostName];
    [self addString:path            toDictionary:aDictionary key:kPath];
    [self addString:password        toDictionary:aDictionary key:kPassword];
    [self addString:domain          toDictionary:aDictionary key:kDomain];   
}

- (void)fromDictionary:(NSDictionary*)aDictionary
{
    accountNum          = [self intFromDictionary:aDictionary key:kAccountNum];
    port                = [self intFromDictionary:aDictionary key:kPort];
    encryption          = [self intFromDictionary:aDictionary key:kEncryption];
    authentication      = [self intFromDictionary:aDictionary key:kAuthentication];
    
    self.userName       = [aDictionary objectForKey:kUserName];
    self.pcc            = [aDictionary objectForKey:kPCC];
    self.name           = [aDictionary objectForKey:kName];
    self.path           = [aDictionary objectForKey:kPath];
    self.password       = [aDictionary objectForKey:kPassword];
    self.domain         = [aDictionary objectForKey:kDomain];
    
    [self setReachableHost:[aDictionary objectForKey:kHostName]];
}

// Override
- (void)fromJSON:(NSString*)aString
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseAccount fromJSON");
}

// Override
- (NSDictionary*)asDictionary
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseAccount asDictionary");
    return nil;
}

////////////////////////////////////////////////////////////////////////////////
// Delegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark Delegate

- (void)addDelegate:(NSObject<AccountDelegate>*)anAccountDelegate
{
    if(![accountDelegates containsObject:anAccountDelegate]) {
        [accountDelegates addObject:anAccountDelegate];
    }
}

- (void)removeDelegate:(NSObject<AccountDelegate>*)anAccountDelegate
{
    if([accountDelegates containsObject:anAccountDelegate]) {
        [accountDelegates removeObject:anAccountDelegate];
    }   
}

////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (void)storeEmailAddress:(NSString *)anEmailAddress
{
    self.emailAddress = anEmailAddress;
    [self commitEmailAddress];
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (BaseFolder*)folderForNum:(int)aNum
{
    BaseFolder* aFolder = NULL;
    
    if(aNum >= 0 && aNum < [folders count]) {
        aFolder = [folders objectAtIndex:aNum];
    }else{
        FXDebugLog(kFXLogActiveSync, @"folderForNum invalid: %d", aNum);
    }
    
    return aFolder;
}

- (BaseFolder*)folderForServerId:(NSString*)aServerId
{
    return [folderDictionary objectForKey:aServerId];
}

- (BaseFolder*)folderForName:(NSString*)aName
{
    BaseFolder* aFolder = NULL;
    
    for(BaseFolder* aTempFolder in folders) {
        if([[aTempFolder displayName] isEqualToString:aName]) {
            aFolder = aTempFolder;
            break;
        }
    }
    return aFolder;
}

- (NSMutableArray*)foldersForFolderType:(EFolderType)aFolderType
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:1];
    
    for(BaseFolder* aFolder in folders) {
        if(aFolder.folderType == aFolderType && !aFolder.isDeleted) {
            [aFolders addObject:aFolder];
        }
    }
    return aFolders;
}

- (NSArray*)folderNames
{
    NSMutableArray* aFolderNames = [NSMutableArray arrayWithCapacity:[folders count]];
    
    for(BaseFolder* aFolder in folders) {
        [aFolderNames addObject:[aFolder displayName]];
    }
    return aFolderNames;
}


- (void)folderHierarchyFromFolders:(NSArray*)aFromFolders toFolders:(NSMutableArray*)aToFolders
{
    for(BaseFolder* aFolder in aFromFolders) {
        if(![aToFolders containsObject:aFolder]) {
            if([aFolder isVisible] && ![aFolder isDeleted]) {
                [aToFolders addObject:aFolder];
                if([aFolder hasChildren]) {
                    NSArray* aChildren = [aFolder children];
                    for(BaseFolder* aChild in aChildren) {
                        if(![aToFolders containsObject:aChild]) {
                            [aToFolders addObject:aChild];
                        }
                    }
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Factory Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory Overrides

- (BaseContact*)createContact
{
    return [[BaseContact alloc] init];
}

- (Event*)createEvent
{
    return [[Event alloc] init];
}

- (ASEmail*)createEmailForFolder:(BaseFolder*)aFolder
{
    return [[ASEmail alloc] initWithFolder:aFolder];
}

- (BaseFolder*)createFolderForFolderType:(EFolderType)aFolderType
{
    FXDebugLog(kFXLogFIXME, @"FIXME override createFolderForFolderType");
    return NULL;
}

////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (UIImage*)icon
{
    return [UIImage imageNamed:@"FixMailRsrc.bundle/settingsAccountImapIcon.png"];
}

- (void)setupFolderForName:(NSString*)aFolderPath
{
    NSAssert(0, @"setupFolderForName");
    FXDebugLog(kFXLogFIXME, @"FIXME setupFolder");
}

- (void)setupFolder:(BaseFolder*)aFolder
{
    NSAssert(0, @"setupFolder");
    FXDebugLog(kFXLogFIXME, @"FIXME setupFolder");
}

- (void)addFolder:(BaseFolder*)aFolder
{
    NSAssert(0, @"addFolder");
    FXDebugLog(kFXLogFIXME, @"FIXME addFolder");
}

- (void)deleteAllFolders
{
    [[SyncManager getSingleton] deleteAllFolders:accountNum];
    [BaseFolder removeAllFolders];
}

- (NSArray*)foldersToDisplay
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:10];
    
    for(BaseFolder* aFolder in folders) {
        if([aFolder isVisible] && ![aFolder isDeleted]) {
            [aFolders addObject:aFolder];
        }
    }
    
    return aFolders;
}

- (BOOL)emailCanBeInMultipleFolders
{
    return FALSE;
}

- (void)creationComplete
{
    FXDebugLog(kFXLogFIXME, @"FIXME creationComplete");
}

- (void)sendEmail:(ASEmail*)anEmail
{
    FXDebugLog(kFXLogFIXME, @"FIXME sendEmail");
}

////////////////////////////////////////////////////////////////////////////////
// DataBase
////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase

+(void)tableCheck 
{
    sqlite3* aDatabase = nil;
    
    AccountDBAccessor* aDBAccessor = [AccountDBAccessor sharedManager];
    if([aDBAccessor isOpen]) {
    }else{
        aDatabase = [aDBAccessor database];
        if(aDatabase) {
            int aDatabaseVersion = [aDBAccessor userVersion];
            if(aDatabaseVersion < kAccountDatabaseVersion) {
                if(aDatabaseVersion > 0) {
                    [ErrorAlert alert:FXLLocalizedStringFromTable(@"ACCOUNT", @"ErrorAlert", nil) message:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"EMAIL_DATABASE_OUT_OF_DATE._WAS_V.%d,_NOW_V.%d", @"ErrorAlert", @"Error alert view message when email database out of date"),
                                       aDatabaseVersion, kAccountDatabaseVersion]];
                    //[self deleteAccount];
                }
                [aDBAccessor setUserVersion:kAccountDatabaseVersion];
            }
        }
        
        NSString* aCreateString = @"CREATE TABLE IF NOT EXISTS account "
                                        "(accountNum INTEGER PRIMARY KEY, "
                                        "accountType INTEGER, "
                                        "accountSubType INTEGER, "
                                        "syncKey VARCHAR(16), "
                                        "policyKey VARCHAR(16), "
                                        "emailAddress VARCHAR(256), "
                                        "isDeleted INTEGER, "
                                        "json TEXT)";
        char* anErrorMsg;	 
        int aResult = sqlite3_exec([[AccountDBAccessor sharedManager] database], [aCreateString UTF8String], NULL, NULL, &anErrorMsg);
        if (aResult != SQLITE_OK) {
            FXDebugLog(kFXLogActiveSync, @"CREATE TABLE account failed: %d %s'", aResult, anErrorMsg);
        }
        //[BaseFolder tableCheck];
    }
}

+ (void)clearPreparedStatements
{
	if(accountInsertStmt) {
		sqlite3_finalize(accountInsertStmt);
		accountInsertStmt = nil;
	}
    if(accountFindStmt) {
		sqlite3_finalize(accountFindStmt);
		accountFindStmt = nil;
	}
    if(accountLoadStmt) {
		sqlite3_finalize(accountLoadStmt);
		accountLoadStmt = nil;
	}
    if(accountCountStmt) {
		sqlite3_finalize(accountCountStmt);
		accountCountStmt = nil;
	}
    if(updateSyncKeyStmt) {
		sqlite3_finalize(updateSyncKeyStmt);
		updateSyncKeyStmt = nil;
	}
    if(updatePolicyKeyStmt) {
		sqlite3_finalize(updatePolicyKeyStmt);
		updatePolicyKeyStmt = nil;
	}
    if(updateEmailAddressStmt) {
		sqlite3_finalize(updateEmailAddressStmt);
		updateEmailAddressStmt = nil;
	}
}

+ (NSString*)databaseError
{ 
    return [NSString stringWithUTF8String:sqlite3_errmsg([[AccountDBAccessor sharedManager] database])];
}

static NSString* getStringForField(sqlite3_stmt* emailLoadStmt, int aFieldNum)
{
    NSString* aString = NULL;
    const char * sqlVal = (const char *)sqlite3_column_text(emailLoadStmt, aFieldNum);
    if(sqlVal != nil) {
        aString = [NSString stringWithUTF8String:sqlVal];
    }else{
        aString = @"";
    }
    return aString;
}

+ (BaseAccount*)createAccountFromSQL:(int)anAccountNum
{
    BaseAccount* anAccount = nil;
    
    if(accountLoadStmt == nil) {
        NSString* accountFindString = @"SELECT "
            "accountType, "
            "accountSubType "
        "FROM account "
        "WHERE accountNum = ?;";
        int dbrc = sqlite3_prepare_v2([[AccountDBAccessor sharedManager] database], [accountFindString UTF8String], -1, &accountLoadStmt, nil);
        if (dbrc != SQLITE_OK) {
            FXDebugLog(kFXLogActiveSync, @"createAccountFromSQL prepare failed: %@", [BaseAccount databaseError]);
        }
    }
    
    sqlite3_bind_int(accountLoadStmt, 1, anAccountNum);
    if (sqlite3_step(accountLoadStmt) == SQLITE_ROW) {
        
        int anAccountType       = sqlite3_column_int(accountLoadStmt, 0);
        int aSubType            = sqlite3_column_int(accountLoadStmt, 1);
        switch(anAccountType) {
			case AccountTypeActiveSync:
			{
				switch(aSubType) {
					case AccountSubTypeGmail:
					{
						anAccount = [[ASAccountGmail alloc] initWithAccountNum:anAccountNum];
						break;
					}
					case AccountSubTypeExchange:
					{
						anAccount = [[ASAccountExchange alloc] initWithAccountNum:anAccountNum];
						break;
					}
                    case AccountSubTypeDomino:
					{
						anAccount = [[ASAccountDomino alloc] initWithAccountNum:anAccountNum];
						break;
					}
					default:
						FXDebugLog(kFXLogActiveSync, @"Invalid account subtype");
				}
				break;
			}
			default:
				FXDebugLog(kFXLogActiveSync, @"Unknown account type accountNum=%d accountType=%d", anAccountNum, anAccountType);
				break;
		}
    }else{
        // This is a normal occurrence
        //FXDebugLog(kFXLogActiveSync, @"uidEntryForUidString not found: %@", aUid);
    }
    
    sqlite3_reset(accountLoadStmt);
    
	return anAccount;
}

- (void)loadFromSQLWithAccountNum:(int)anAccountNum
{
    [BaseAccount tableCheck];
            
    if(accountFindStmt == nil) {
        NSString* accountFindString = @"SELECT "
                        "syncKey, "
                        "policyKey, "
                        "emailAddress, "
                        "isDeleted, "
                        "json "
                    "FROM account "
                    "WHERE accountNum = ?;";
        int dbrc = sqlite3_prepare_v2([[AccountDBAccessor sharedManager] database], [accountFindString UTF8String], -1, &accountFindStmt, nil);
        if (dbrc != SQLITE_OK) {
            FXDebugLog(kFXLogActiveSync, @"loadFromSQLWithAccountNum prepare failed: %@", [BaseAccount databaseError]);
        }
    }
    
    sqlite3_bind_int(accountFindStmt, 1, anAccountNum);
    if (sqlite3_step(accountFindStmt) == SQLITE_ROW) {
        self.syncKey        = getStringForField(accountFindStmt,  0);
        self.policyKey      = getStringForField(accountFindStmt,  1);
        self.emailAddress   = getStringForField(accountFindStmt,  2);
        self.deleted        = sqlite3_column_int(accountFindStmt, 3) != 0;
        
        NSString* aJSONString   = getStringForField(accountFindStmt,  4);
        if([aJSONString length] > 0) {
                //FXDebugLog(kFXLogActiveSync, @"loadSettings: %@", aJSONString);
            [self fromJSON:aJSONString];
        }else{
            FXDebugLog(kFXLogActiveSync, @"account JSON string invalid");
        }

        if([EmailProcessor debugAccount]) {
            FXDebugLog(kFXLogActiveSync, @"SELECT FROM account WHERE accountNum=%d json=%@", anAccountNum, aJSONString);
        }
    }else{
        // This is a normal occurrence
        //FXDebugLog(kFXLogActiveSync, @"uidEntryForUidString not found: %@", aUid);
    }
    
    sqlite3_reset(accountFindStmt);    	
}

+ (int)accountsConfigured
{
    NSUInteger aCount = 0;
    
    [BaseAccount tableCheck];
    if(!accountCountStmt) {
        NSString* accountFindString = @"SELECT json FROM account;";
        int dbrc = sqlite3_prepare_v2([[AccountDBAccessor sharedManager] database], [accountFindString UTF8String], -1, &accountCountStmt, nil);
        if (dbrc != SQLITE_OK) {
            FXDebugLog(kFXLogActiveSync, @"accountsConfigured prepare failed: %@", [BaseAccount databaseError]);
            return 0;
        }
    }
    
    while(sqlite3_step(accountCountStmt) == SQLITE_ROW) {
        const char* aText = (const char *)sqlite3_column_text(accountCountStmt, 0);
        if(aText != nil && strlen(aText) > 4) {
            aCount++;
        }
    }
    
    sqlite3_reset(accountCountStmt);
    	
	return aCount;
}

- (void)commitToDatabase 
{
    [BaseAccount tableCheck];
    [AccountDBAccessor beginTransaction];
 
    @try {
        if(accountInsertStmt == nil) {
            NSString* insertAccount = @"INSERT OR REPLACE INTO account("
                    "accountNum, "
                    "accountType, "
                    "accountSubType, "
                    "syncKey, "
                    "policyKey, "
                    "emailAddress, "
                    "json) "
                    "VALUES (?,?,?,?,?,?,?);";
            int dbrc = sqlite3_prepare_v2([[AccountDBAccessor sharedManager] database], [insertAccount UTF8String], -1, &accountInsertStmt, nil);	
            if (dbrc != SQLITE_OK) {
                FXDebugLog(kFXLogActiveSync, @"BaseAccount commit prepare failed: %d %@", self.accountNum, [BaseAccount databaseError]);
                return;
            }
        }
        
        sqlite3_bind_int(accountInsertStmt,  1, accountNum);
        sqlite3_bind_int(accountInsertStmt,  2, accountType);
        sqlite3_bind_int(accountInsertStmt,  3, accountSubType);
        sqlite3_bind_text(accountInsertStmt, 4, [self.syncKey UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(accountInsertStmt, 5, [self.policyKey UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(accountInsertStmt, 6, [self.emailAddress UTF8String], -1, SQLITE_TRANSIENT);
        
        NSString* aJSONString = [self asJSON];
        sqlite3_bind_text(accountInsertStmt, 7, [aJSONString UTF8String], -1, SQLITE_TRANSIENT);
        
        if (sqlite3_step(accountInsertStmt) != SQLITE_DONE)	{
            FXDebugLog(kFXLogActiveSync, @"INSERT OR REPLACE INTO account failed: %d %@", self.accountNum, [BaseAccount databaseError]);
        }else if([EmailProcessor debugAccount]) {
            FXDebugLog(kFXLogActiveSync, @"INSERT OR REPLACE INTO account emailAddress=%@ json=%@", self.emailAddress, aJSONString);
        }
        sqlite3_reset(accountInsertStmt);
    }@catch (NSException* e) {
        [self logException:@"commit" exception:e];
    }
    
    [AccountDBAccessor endTransaction];
}

- (void)commitSyncKey
{
    sqlite3* aDatabase = [[AccountDBAccessor sharedManager] database];
    if(updateSyncKeyStmt == nil) {
		NSString* updateStmt = @"UPDATE account SET syncKey=? WHERE accountNum=?;";
		int dbrc = sqlite3_prepare_v2(aDatabase, [updateStmt UTF8String], -1, &updateSyncKeyStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert view message when specific method failed with specified error message"), @"commitSyncKey", sqlite3_errmsg(aDatabase)]];
			return;
		}
	}
    
    NSString* aSyncKey = self.syncKey;
	sqlite3_bind_text(updateSyncKeyStmt, 1, [aSyncKey UTF8String], [aSyncKey length], SQLITE_TRANSIENT);
    sqlite3_bind_int(updateSyncKeyStmt,  2, self.accountNum);

	if (sqlite3_step(updateSyncKeyStmt) != SQLITE_DONE) {
		[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", @"Error alert view message when step failed in specific method"), @"commitSyncKey", sqlite3_errmsg(aDatabase)]];
	//}else if(kDebugDatabase) {
        //FXDebugLog(kFXLogActiveSync, @"UPDATE account syncKey = @", aSyncKey);
    }
	
	sqlite3_reset(updateSyncKeyStmt);
}

- (void)commitPolicyKey
{
    [BaseAccount tableCheck];

    sqlite3* aDatabase = [[AccountDBAccessor sharedManager] database];
    if(updatePolicyKeyStmt == nil) {
		NSString* updateStmt = @"UPDATE account SET policyKey=? WHERE accountNum=?;";
		int dbrc = sqlite3_prepare_v2(aDatabase, [updateStmt UTF8String], -1, &updatePolicyKeyStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), @"commitPolicyKey", sqlite3_errmsg(aDatabase)]];
			return;
		}
	}
    
    NSString* aPolicyKey = self.policyKey;
	sqlite3_bind_text(updatePolicyKeyStmt, 1, [aPolicyKey UTF8String], [aPolicyKey length], SQLITE_TRANSIENT);
    sqlite3_bind_int(updatePolicyKeyStmt,  2, self.accountNum);
    
	if (sqlite3_step(updatePolicyKeyStmt) != SQLITE_DONE) {
		[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", nil), @"commitPolicyKey", sqlite3_errmsg(aDatabase)]];
        //}else if(kDebugDatabase) {
        //FXDebugLog(kFXLogActiveSync, @"UPDATE account syncKey = @", aSyncKey);
    }
	
	sqlite3_reset(updatePolicyKeyStmt);
}

- (void)commitEmailAddress
{
    sqlite3* aDatabase = [[AccountDBAccessor sharedManager] database];
    if(updateEmailAddressStmt == nil) {
		NSString* updateStmt = @"UPDATE account SET emailAddress=? WHERE accountNum=?;";
		int dbrc = sqlite3_prepare_v2(aDatabase, [updateStmt UTF8String], -1, &updateEmailAddressStmt, nil);
		if (dbrc != SQLITE_OK) {
			[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", nil), @"commitEmailAddress", sqlite3_errmsg(aDatabase)]];
			return;
		}
	}
    
    NSString* anEmailAddress = self.emailAddress;
	sqlite3_bind_text(updateEmailAddressStmt, 1, [anEmailAddress UTF8String], [anEmailAddress length], SQLITE_TRANSIENT);
    sqlite3_bind_int(updateEmailAddressStmt,  2, self.accountNum);
    
	if (sqlite3_step(updateEmailAddressStmt) != SQLITE_DONE) {
		[self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FAILED_STEP_IN_%@((method)):_%s", @"ErrorAlert", nil), @"commitEmailAddress", sqlite3_errmsg(aDatabase)]];
        //}else if(kDebugDatabase) {
        //FXDebugLog(kFXLogActiveSync, @"UPDATE account syncKey = @", aSyncKey);
    }
	
	sqlite3_reset(updateEmailAddressStmt);
}


////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (void)description:(NSMutableString*)aString
{
    [self writeUnsigned:aString		tag:@"accountNum"       value:accountNum];

    [self writeString:aString		tag:@"name"             value:name];
    [self writeString:aString		tag:@"emailAddress"     value:emailAddress];
	[self writeString:aString		tag:@"password"         value:@"********"];
    [self writeString:aString		tag:@"hostName"         value:hostName];
    if([path length] > 0) {
        [self writeString:aString   tag:@"path"             value:path];
    }
    if([domain length] > 0) {
        [self writeString:aString   tag:@"domain"           value:domain];
    }

	[self writeUnsigned:aString		tag:@"port"             value:port];

	[self writeUnsigned:aString		tag:@"encryption"       value:encryption];
	[self writeUnsigned:aString		tag:@"authentication"   value:authentication];
    
    [self writeString:aString		tag:@"protocolVersion"      value:self.protocolVersion];
	[self writeString:aString		tag:@"syncKey"              value:self.syncKey];
	[self writeString:aString		tag:@"policyKey"            value:self.policyKey];
	[self writeInt:aString           tag:@"heartBeatIntervalSeconds"   value:self.heartBeatIntervalSeconds];
    
    NSUInteger aCount;
    aCount = [self.versionSupport count];
    if(aCount > 0) [aString appendString:@"\n"];
    for(int i = 0 ; i < aCount ; ++i) {
        [self writeString:aString    tag:@"AS version"           value:[self.versionSupport objectAtIndex:i]];
    }
    
    aCount = [self.commands count];
    if(aCount > 0) [aString appendString:@"\n"];
    for(int i = 0 ; i < aCount ; ++i) {
        [self writeString:aString    tag:@"AS command"           value:[self.commands objectAtIndex:i]];
    }
    
	[aString appendString:@"\n"];
}

@end
