/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAutoTestSearch.h"
#import "ASAccount.h"
#import "ASAutoTest.h"
#import "ASSearch.h"

@implementation ASAutoTestSearch


////////////////////////////////////////////////////////////////////////////////////////////
// Test Harness
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Test Harness

- (void)start
{
    NSArray* aFolders = [self.account foldersForFolderType:kFolderTypeInbox];
    ASFolder* aFolder = nil;
    if(aFolders.count > 0) {
        aFolder = [aFolders objectAtIndex:0];
    }
    if(!aFolder) {
        FXDebugLog(kFXLogActiveSync, @"doSearchemail failed, no inbox");
        [self fail];
    }
    
    // Start test
    NSRange aRange = NSMakeRange(0, 49);
    [self doSearchAll:aFolder query:@"Test" range:aRange];
    [self doSearchFrom:aFolder query:@"davebulk" range:aRange];
    [self doSearchSubject:aFolder query:@"Test" range:aRange];
}

- (void)success
{
    [self.autoTest autoTestSuccess:self];
}

- (void)fail
{
    [self.autoTest autoTestFailed:self];
}

- (void)doSearchAll:(ASFolder*)aFolder query:(NSString*)aString  range:(NSRange)aRange
{
    ASSearch* aSearch = [ASSearch sendSearchMailbox:aFolder query:aString range:aRange displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        ASSearch* aSearch = aNotification.object;
#ifdef DEBUG
        NSDateFormatter* aDateFormatter = [ASEmail dateFormatter];
#endif
        
        FXDebugLog(kFXLogActiveSync, @"doSearchAll %@", aString);
        for(ASEmail* anEmail in aSearch.results) {
            FXDebugLog(kFXLogActiveSync, @"\t%@ %@ %@", [aDateFormatter stringFromDate:anEmail.datetime], anEmail.from, anEmail.subject);
        }
        FXDebugLog(kFXLogActiveSync, @"");

		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSearch* aSearch = aNotification.object;
        [aSearch displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doSearchFrom:(ASFolder*)aFolder query:(NSString*)aString range:(NSRange)aRange
{
    ASSearch* aSearch = [ASSearch sendSearchMailbox:aFolder query:aString range:aRange displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        ASSearch* aSearch = aNotification.object;
#ifdef DEBUG
        NSDateFormatter* aDateFormatter = [ASEmail dateFormatter];
#endif
        FXDebugLog(kFXLogActiveSync, @"doSearchFrom %@", aString);
        for(ASEmail* anEmail in aSearch.results) {
            NSRange aRange = [anEmail.from rangeOfString:aString options:NSCaseInsensitiveSearch];
            if(aRange.length > 0) {
                FXDebugLog(kFXLogActiveSync, @"\t%@ %@ %@", [aDateFormatter stringFromDate:anEmail.datetime], anEmail.from, anEmail.subject);
            }
        }
        FXDebugLog(kFXLogActiveSync, @"");
        
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSearch* aSearch = aNotification.object;
        [aSearch displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

- (void)doSearchSubject:(ASFolder*)aFolder query:(NSString*)aString range:(NSRange)aRange
{
    ASSearch* aSearch = [ASSearch sendSearchMailbox:aFolder query:aString range:aRange displayErrors:NO];
    
    __block id anObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncSuccessNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Success
        //
        ASSearch* aSearch = aNotification.object;
#ifdef DEBUG
        NSDateFormatter* aDateFormatter = [ASEmail dateFormatter];
#endif
        FXDebugLog(kFXLogActiveSync, @"doSearchSubject %@", aString);
        for(ASEmail* anEmail in aSearch.results) {
            NSRange aRange = [anEmail.subject rangeOfString:aString options:NSCaseInsensitiveSearch];
            if(aRange.length > 0) {
                FXDebugLog(kFXLogActiveSync, @"\t%@ %@ %@", [aDateFormatter stringFromDate:anEmail.datetime], anEmail.from, anEmail.subject);
            }
        }
        FXDebugLog(kFXLogActiveSync, @"");
        
		[[NSNotificationCenter defaultCenter] removeObserver:anObserver];
	}];
    
    __block id aFailObserver = [[NSNotificationCenter defaultCenter] addObserverForName:kActiveSyncFailNotification object:aSearch queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *aNotification) {
        // Fail
        //
        ASSearch* aSearch = aNotification.object;
        [aSearch displayError];
        [self fail];
		[[NSNotificationCenter defaultCenter] removeObserver:aFailObserver];
	}];
}

@end