/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpConnection.h"

@class ASAccount;

typedef enum {
    kHttpOK                             = 200,
    kHttpCreated                        = 201,
    kHttpAccepted                       = 202,
    kHttpNonAuthorativeInformation      = 203,
    kHttpNoContent                      = 204,
    kHttpResetContent                   = 205,
    kHttpPartialContent                 = 206,
    
    kHttpBadRequest                     = 400,
    kHttpUnauthorized                   = 401,
    kHttpPaymentRequired                = 402,
    kHttpForbidden                      = 403,
    kHttpNotFound                       = 404,
    kHttpMethodNotAllowed               = 405,
    kHttpNotAcceptable                  = 406,
    kHttpProxyAuthenticationRequired    = 407,
    kHttpRequestTimeout                 = 408,
    kHttpConflict                       = 409,
    kHttpGone                           = 410,
    kHttpLengthRequired                 = 411,
    kHttpPreconditionFailed             = 412,
    kHttpRequestEntityTooLarge          = 413,
    kHttpRequestURITooLong              = 414,
    kHttpUnsupportedMediaType           = 415,
    kHttpRequestedRangeNotSatisfied     = 416,
    kHttpExpectationFailed              = 417,

    kHttpNeedsProvisioning              = 449,
    
    kHttpInternalServerError            = 500,
    kHttpNotImplemented                 = 501,
    kHttpBadGateway                     = 502,
    kHttpServiceUnavailable             = 503,
    kHttpGatewayTimeout                 = 504,
    kHttpVersionNotSupported            = 505

} EHttpStatusCode;

@interface HttpRequest : NSObject <NSURLConnectionDelegate, UIAlertViewDelegate> {
    NSMutableURLRequest*    URLrequest;
    BOOL                    observingReachability;
}

@property (weak) ASAccount*         account;
@property (weak) HttpConnection*    httpConnection;
@property EHttpStatusCode           statusCode;
@property (strong) NSString*        contentType;
@property BOOL                      thread;
@property (strong) NSDate*          retryTime;
@property int                       retrySeconds;
@property NSUInteger                retries;

// Construct/Destruct
- (id)initWithAccount:(ASAccount*)anAccount;
- (void)cancel;

// NSURLConnectionDelegate
- (void)connectionDidFinishLoading:(HttpConnection*)connection;

// Setters
- (void)setURLRequest:(NSMutableURLRequest*)aURLRequest;
- (NSMutableURLRequest*)URLrequest;

// Error Handling
+ (void)logException:(NSString*)where exception:(NSException*)e;
+ (NSString*)handleASError:(NSString*)anErrorMessage;

- (void)doRetry;  // May have virtual override in AS commands like ASItemOperations
- (void)retry;    /// Virtual overridden in WBXMLRequest

- (void)handleCertificateError:(NSString*)aMessage;
- (void)handleGenericError:(NSError*)anError;
- (void)handleHttpError;
- (void)handleReachability;
- (void)connectionRelease:(HttpConnection*)connection;

// Overrides
- (void)userAbortedWithMessage:(NSString*)aMesg;

@end
