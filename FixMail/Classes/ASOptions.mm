/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */


#import "ASOptions.h"
#import "ASAccount.h"
#import "HttpEngine.h"

@implementation ASOptions

@synthesize delegate;

// Static
static NSString* kCommand   = @"Options";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendRequest:(ASAccount*)anAccount delegate:(NSObject<ASOptionsDelegate>*)aDelegate
{
    ASOptions* anASOptions = [[ASOptions alloc] initWithAccount:anAccount];
    anASOptions.delegate = aDelegate;
    NSMutableURLRequest* aURLRequest = [anAccount createOptionsRequest:kCommand];
    [anAccount send:aURLRequest httpRequest:anASOptions];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// HttpRequest Overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark HttpRequest Overrides

- (void)userAbortedWithMessage:(NSString*)aMesg
{
    [self.account optionsAborted:aMesg];
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)aConnection didReceiveResponse:(NSURLResponse*)aResponse
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)aResponse;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
	if(self.statusCode == kHttpOK) {
        @try {
            NSDictionary* aHeaders = [resp allHeaderFields];
            //FXDebugLog(kFXLogActiveSync, @"ASOptions didReceiveResponse: %d headers: %@", statusCode, aHeaders);
            
            // Allow = "OPTIONS,POST";
            // "Cache-Control" = "private, max-age=0";
            // "Content-Length" = 0;
            // "Content-Type" = "application/octet-stream";
            // Date = "Fri, 20 Jul 2012 23:07:33 GMT";
            // Expires = "Fri, 20 Jul 2012 23:07:33 GMT";
            // "MS-ASProtocolCommands" = "Sync,SendMail,SmartForward,SmartReply,GetAttachment,FolderSync,MoveItems,GetItemEstimate,Search,Ping,ItemOperations,Provision,ResolveRecipients";
            // "MS-ASProtocolVersions" = "1.0,2.0,2.5,12.0";
            // "MS-Server-ActiveSync" = "8.1";
            // Public = "OPTIONS,POST";
            // Server = GSE;
            // "X-Content-Type-Options" = nosniff;
            // "X-Frame-Options" = SAMEORIGIN;
            // "X-XSS-Protection" = "1; mode=block";
            
            // Protocol versions
            //
            NSString* aVersionString = [aHeaders objectForKey:@"MS-ASProtocolVersions"];
            if(aVersionString) {
                FXDebugLog(kFXLogActiveSync, @"AS version: %@", aVersionString);
                NSArray* versions = [aVersionString componentsSeparatedByString:@","];
                [self.account setVersions:versions];
            }
            
            // Protocol commands
            NSString* aCommandString = [aHeaders objectForKey:@"MS-ASProtocolCommands"];
            if(aCommandString) {
                FXDebugLog(kFXLogActiveSync, @"AS commands: %@", aCommandString);

                NSArray* commands = [aCommandString componentsSeparatedByString:@","];
                [self.account setCommands:commands];
            }
            
            if(self.delegate) {
                [self.delegate authenticationSuccess:self];
            }
        }@catch (NSException* e) {
            [HttpRequest logException:@"Options response" exception:e];
        }
        
        [self.account optionsSet];
    }else{
        [super handleHttpError];
		[[NSNotificationCenter defaultCenter] postNotificationName:kHttpRequestFailureNotification object:nil];
    }
}

- (void)connection:(NSURLConnection*)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge*)aChallenge
{
    if(self.delegate) {
        [self.delegate authenticationChallenge:aChallenge options:self];
    }
}


- (void)connection:(HttpConnection*)connection didFailWithError:(NSError *)anError
{	
	[[NSNotificationCenter defaultCenter] postNotificationName:kHttpRequestFailureNotification object:nil];
	[super connection:connection didFailWithError:anError];
}

@end
