/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASCompose.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "HttpEngine.h"
#import "WBXMLDataGenerator.h"

@implementation ASCompose

// Properties
@synthesize delegate;
@synthesize clientID;

// Constants
static NSString* kCommand               = @"Compose";
static NSString* kCommandSendMail       = @"SendMail";
static NSString* kCommandSmartForward   = @"SmartForward";
static NSString* kCommandSmartReply     = @"SmartReply";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendMail:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate
{
    @try {
        ASCompose* aSendMail = [[ASCompose alloc] initWithAccount:anAccount tag:COMPOSE_SEND_MAIL];
        aSendMail.delegate = aDelegate;
        
        NSMutableURLRequest* aURLRequest;
        if([anAccount ASVersion] <= kASVersion2007SP1) {
            // AS 12.0 & 12.1 don't use WBXML for SendMail or have the Compose code page
            // Its just a SendMail command, a MIME message as the body and a MIME rfc content type
            aURLRequest = [anAccount createMailRequestWithCommand:kCommandSendMail mime:aMimeData];
        }else{
#warning FIXME SendMail WBXML not Tested, needs Exchange 2010
            NSData* aWbxml = [aSendMail wbxmlSendMail:anAccount mime:aMimeData];
            aURLRequest = [anAccount createURLRequestWithCommand:kCommandSendMail wbxml:aWbxml];
        }
        
        [anAccount send:aURLRequest httpRequest:aSendMail];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendMail" exception:e];
    }
}

+ (void)sendMail:(ASAccount*)anAccount string:(NSString*)aMimeString delegate:(NSObject<ASComposeDelegate>*)aDelegate
{
    NSData* aMimeData = [NSData dataWithBytes:[aMimeString UTF8String] length:aMimeString.length]; 
    [ASCompose sendMail:anAccount mime:aMimeData delegate:aDelegate];
}

+ (void)sendSmartForward:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate
{
    @try {
        ASCompose* aSmartForward = [[ASCompose alloc] initWithAccount:anAccount tag:COMPOSE_SMART_FORWARD];
        aSmartForward.delegate = aDelegate;

        NSMutableURLRequest* aURLRequest;
        if([anAccount ASVersion] <= kASVersion2007SP1) {
            // AS 12.0 & 12.1 don't use WBXML for SendMail or have the Compose code page
            // Its just a SendMail command, a MIME message as the body and a MIME rfc content type
            aURLRequest = [anAccount createMailRequestWithCommand:kCommandSmartForward mime:aMimeData];
        }else{
    #warning FIXME SmartForward Not Tested, needs Exchange 2010
            NSData* aWbxml = [aSmartForward wbxmlSmartForward:anAccount mime:aMimeData];
            aURLRequest = [anAccount createURLRequestWithCommand:kCommandSmartForward wbxml:aWbxml];
        }
        
        [anAccount send:aURLRequest httpRequest:aSmartForward];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendSmartForward" exception:e];
    }
}

+ (void)sendSmartReply:(ASAccount*)anAccount mime:(NSData*)aMimeData delegate:(NSObject<ASComposeDelegate>*)aDelegate
{
    @try {
        ASCompose* aSmartReply = [[ASCompose alloc] initWithAccount:anAccount tag:COMPOSE_SMART_REPLY];
        aSmartReply.delegate = aDelegate;

        NSMutableURLRequest* aURLRequest;
        if([anAccount ASVersion] <= kASVersion2007SP1) {
            // AS 12.0 & 12.1 don't use WBXML for SendMail or have the Compose code page
            // Its just a SendMail command, a MIME message as the body and a MIME rfc content type
           aURLRequest = [anAccount createMailRequestWithCommand:kCommandSmartReply mime:aMimeData];
        }else{
#warning FIXME SmartReply ot Tested, needs Exchange 2010
            NSData* aWbxml = [aSmartReply wbxmlSmartReply:anAccount mime:aMimeData];
            aURLRequest = [anAccount createURLRequestWithCommand:kCommandSmartReply wbxml:aWbxml];
        }
        
        [anAccount send:aURLRequest httpRequest:aSmartReply];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendSmartReply" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
// NOTE: These are not currently used, AS 12.0 just takes MIME data
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlSendMail:(ASAccount*)anAccount mime:(NSData*)aMimeData
{
	WBXMLDataGenerator wbxml;
	wbxml.start(COMPOSE_SEND_MAIL); {
        wbxml.keyValue(COMPOSE_CLIENT_ID, clientID);
        wbxml.tag(COMPOSE_SAVE_IN_SENT_ITEMS);
        wbxml.keyOpaque(COMPOSE_MIME, aMimeData);
    }wbxml.end();
    
    return wbxml.encodedData(self, COMPOSE_SEND_MAIL);
}

- (NSData*)wbxmlSmartForward:(ASAccount*)anAccount mime:(NSData*)aMimeData
{
	WBXMLDataGenerator wbxml;
    
    //NSString* anEmailAddress = [anAccount emailAddress];
	wbxml.start(COMPOSE_SMART_FORWARD); {
        wbxml.keyValue(COMPOSE_CLIENT_ID, clientID);
        //wbxml.tag(COMPOSE_SAVE_IN_SENT_ITEMS);
        wbxml.keyOpaque(COMPOSE_MIME, aMimeData);
    }wbxml.end();

    return wbxml.encodedData(self, COMPOSE_SMART_FORWARD);
}

- (NSData*)wbxmlSmartReply:(ASAccount*)anAccount mime:(NSData*)aMimeData
{
	WBXMLDataGenerator wbxml;
    
    //NSString* anEmailAddress = [anAccount emailAddress];
	wbxml.start(COMPOSE_SMART_REPLY); {
        wbxml.keyValue(COMPOSE_CLIENT_ID, clientID);
        //wbxml.tag(COMPOSE_SAVE_IN_SENT_ITEMS);
        wbxml.keyOpaque(COMPOSE_MIME, aMimeData);
    }wbxml.end();
    
    return wbxml.encodedData(self, COMPOSE_SMART_REPLY);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount tag:(ASTag)aTag
{
    if (self = [super initWithAccount:anAccount]) {
        [self setClientID:[anAccount getClientID]];
        commandTag = aTag;
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse* anHttpResponse = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[anHttpResponse statusCode];
	
    switch(self.statusCode) {
        case kHttpOK:
        {
            NSDictionary* aHeaders = [anHttpResponse allHeaderFields];
            //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
            NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
            if([aContentType isEqualToString:@"application/octet-stream"]) {
            }
            [delegate sendMailSuccess];
            break;
        }
        default:
        {
            [super handleHttpErrorForAccount:self.account connection:connection response:response];
            [delegate sendMailFailed:[NSHTTPURLResponse localizedStringForStatusCode:self.statusCode]];
            break;
        }
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK) {
        if(aLength > 0) {
            // There probably won't be any data here on a successful response
            //
            NSString* aString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            FXDebugLog(kFXLogActiveSync, @"%@ response OK withData:\n%@", kCommand, aString);
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
        if([data length] > 0) {
            NSString* aString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            FXDebugLog(kFXLogActiveSync, @"withData:\n%@", aString);
        }
    }
    [super connectionDidFinishLoading:connection];
}

@end