/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "DBaseEngine.h"
#import "GlobalDBFunctions.h"

@implementation DBaseEngine

// Static
static NSMutableArray* sEngines = nil;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
	self = [super init];	
	if(self) {
        if(!sEngines) {
            sEngines = [[NSMutableArray alloc] initWithCapacity:3];
        }
        [sEngines addObject:self];
	}
	
	return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

// Static interfaces
+ (void)open:(BaseAccount*)anAccount
{
#ifdef DEBUG
    unsigned long long aFreeSpace = [GlobalDBFunctions freeSpaceOnDisk];
    double aFreeMegabyes = (double)aFreeSpace / (1024.0*1024.0);
    FXDebugLog(kFXLogAll, @"Free    : %.3f MB", aFreeMegabyes);
#endif
    
    for(DBaseEngine* anEngine in sEngines) {
        [anEngine open:anAccount];
    }
    
#ifdef DEBUG
    for(DBaseEngine* anEngine in sEngines) {
        unsigned long long aSpaceUsed = [anEngine spaceUsed];
        NSString* aName = [anEngine displayName];
        if(aName.length < 8) {
            aName = [aName stringByAppendingString:@"   "];
        }
        FXDebugLog(kFXLogAll, @"%@: %llu bytes", aName, aSpaceUsed);
    }
#endif
}

+ (unsigned long long)spaceUsed
{
    unsigned long long aSpaceUsed = 0;
    
    for(DBaseEngine* anEngine in sEngines) {
        aSpaceUsed += [anEngine spaceUsed];
    }
    return aSpaceUsed;
}

+ (void)shutDown
{
    for(DBaseEngine* anEngine in sEngines) {
        [anEngine shutDown];
    }
}

+ (void)deleteAccount
{
    for(DBaseEngine* anEngine in sEngines) {
        [anEngine deleteAccount];
    }
}

+ (unsigned long long)sizeForPath:(NSString*)aPath
{
    unsigned long long aSize = 0;
    
    if(aPath.length > 0) {
		NSDictionary* fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:aPath error:nil];
		if (fileAttributes != nil) {
			NSNumber* fileSize;
			if ((fileSize = [fileAttributes objectForKey:NSFileSize])) {
                aSize = [fileSize unsignedLongValue];
            }
        }
    }
    
    return aSize;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Virtual overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Virtual overrides

- (void)open:(BaseAccount*)anAccount
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine open");
}

- (unsigned long long)spaceUsed
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine spaceUsed");
    return 0LL;
}

- (void)shutDown
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine shutDown");
}

- (void)deleteAccount
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine deleteAccount");
}

- (NSString*)displayName
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine displayName");
    return @"Unknown";
}

- (void)queueCommitObjects:(CommitObjects*)aCommitObjects
{
    FXDebugLog(kFXLogAll, @"FIXME DBaseEngine queueCommitObjects");
}

@end
