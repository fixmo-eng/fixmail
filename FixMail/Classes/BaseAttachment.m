/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseAttachment.h"

@implementation BaseAttachment

// Properties
@synthesize name;
@synthesize fileReference;
@synthesize contentType;
@synthesize estimatedDataSize;

// Flags Stored

static const NSUInteger kFlagsStoredMask    = 0x0000ffff;

static const NSUInteger	kIsFetched          = 0x00000001;
static const NSUInteger	kIsRead             = 0x00000002;
static const NSUInteger	kIsInline           = 0x00000004;

// Flags Transient
static const NSUInteger	kIsFetching         = 0x00010000;

// Constants
static NSString* kFileNameKey       = @"n";
static NSString* kFileReferenceKey  = @"f";
static NSString* kContentTypeKey    = @"t";
static NSString* kEstimatedSizeKey  = @"s";
static NSString* kFlagsKey          = @"fl";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
	if (self = [super init]) {
	}
	return self;
}


////////////////////////////////////////////////////////////////////////////////
// Load/Store
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Store

- (NSDictionary*)asDictionary
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:6];
    
    if([self.name length] > 0) {
        [aDictionary setObject:self.name forKey:kFileNameKey];
    }else{
        FXDebugLog(kFXLogActiveSync, @"BaseAttachment asDictionary name invalid: %@", self);
        [aDictionary setObject:@"" forKey:kFileNameKey];
    }
    if([self.contentType length] == 0) {
        self.contentType = [self.name pathExtension];
    }
    if([self.contentType length] > 0) {
        [aDictionary setObject:self.contentType forKey:kContentTypeKey];
    }
    
    if([self.fileReference length] > 0) {
        [aDictionary setObject:self.fileReference forKey:kFileReferenceKey];
    }else{
        FXDebugLog(kFXLogActiveSync, @"BaseAttachment asDictionary fileReference invalid: %@", self);
        [aDictionary setObject:@"" forKey:kFileReferenceKey];
    }
    
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.estimatedDataSize] forKey:kEstimatedSizeKey];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:(flags & kFlagsStoredMask)] forKey:kFlagsKey];

    return aDictionary;
}

- (void)fromDictionary:(NSDictionary*)aDictionary
{
    self.name           = [aDictionary objectForKey:kFileNameKey];
    self.contentType    = [aDictionary objectForKey:kContentTypeKey];
    self.fileReference  = [aDictionary objectForKey:kFileReferenceKey];
    NSNumber* aNumber   = [aDictionary objectForKey:kEstimatedSizeKey];
    if(aNumber) {
        self.estimatedDataSize = [aNumber unsignedIntValue];
    }
    
    aNumber             = [aDictionary objectForKey:kFlagsKey];
    if(aNumber) {
        flags = [aNumber unsignedIntValue];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Flags Stored
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Stored

- (BOOL)isFetched
{
	return (flags & kIsFetched) != 0;
}

- (void)setIsFetched:(BOOL)state
{
	if(state)	flags |= kIsFetched;
	else		flags &= ~kIsFetched;
}

- (BOOL)isRead
{
	return (flags & kIsRead) != 0;
}

- (void)setIsRead:(BOOL)state
{
	if(state)	flags |= kIsRead;
	else		flags &= ~kIsRead;
}

- (BOOL)isInline
{
	return (flags & kIsInline) != 0;
}

- (void)setIsInline:(BOOL)state
{
	if(state)	flags |= kIsInline;
	else		flags &= ~kIsInline;
}

////////////////////////////////////////////////////////////////////////////////
// Flags Transient
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Transient

- (BOOL)isFetching
{
	return (flags & kIsFetching) != 0;
}

- (void)setIsFetching:(BOOL)state
{
	if(state)	flags |= kIsFetching;
	else		flags &= ~kIsFetching;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
    [self writeString:string		tag:@"name"                 value:name];
    [self writeString:string		tag:@"fileReference"        value:fileReference];
    if([contentType length] > 0) {
        [self writeString:string		tag:@"contentType"          value:contentType];
    }
    [self writeInt:string               tag:@"estimatedDataSize"    value:estimatedDataSize];
    
	[string appendString:@"}\n"];
	
	return string;
}



@end
