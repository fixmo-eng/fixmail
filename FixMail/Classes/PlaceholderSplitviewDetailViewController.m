//
//  EmailDetailViewController.m
//  FixMail
//
//  Created by Sean Langley on 11/14/12.
//
//

#import "PlaceholderSplitviewDetailViewController.h"
#import "SZLConcreteApplicationContainer.h"

@interface PlaceholderSplitviewDetailViewController ()

@end

@implementation PlaceholderSplitviewDetailViewController

-(void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    NSArray* buttonArray = nil;
    if (splitViewBarButtonItem)
    {
        buttonArray = @[ [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem], splitViewBarButtonItem ];
    }
    else
    {
        buttonArray = @[ [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem] ];
    }
    
    self.navigationItem.leftBarButtonItems = buttonArray;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (self.composeDelegate) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(toggleCompose:)];
    }
    
    if (!self.navigationItem.leftBarButtonItem)
    {
        self.navigationItem.leftBarButtonItem =  [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

-(IBAction)toggleCompose:(id)sender
{
    if (self.composeDelegate) {
        [self.composeDelegate toggleCompose:sender];
    }
}

@end
