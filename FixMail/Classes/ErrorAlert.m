/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ErrorAlert.h"
#import "UIAlertView+Modal.h"

@implementation ErrorAlert

static NSString* kMessage       = @"message";
static NSString* kTitle         = @"title";

static ErrorAlert* sErrorAlert  = nil;
static BOOL sIsPosted           = FALSE;

- (void)showAlertView:(NSDictionary*)aDictionary
{
    if(!sIsPosted) {
        sIsPosted = TRUE;
        NSString* aTitle    = [aDictionary objectForKey:kTitle];
        NSString* aMessage  = [aDictionary objectForKey:kMessage];

        FXDebugLog(kFXLogAll, @"Error in %@: %@", aTitle, aMessage);
        UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:aTitle
                                                              message:aMessage
                                                             delegate:nil
                                                    cancelButtonTitle:FXLLocalizedStringFromTable(@"OK", @"ErrorAlert", @"OK button title for alert view")
                                                    otherButtonTitles:nil];
        [anAlertView showModal];
        sIsPosted = FALSE;
    }
}

+ (void)alert:(NSString*)aTitle message:(NSString*)anErrorMessage
{
    if(!sErrorAlert) {
        sErrorAlert = [[ErrorAlert alloc] init];
    }
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:2];
    [aDictionary setObject:anErrorMessage forKey:kMessage];
    [aDictionary setObject:aTitle forKey:kTitle];

    [sErrorAlert performSelectorOnMainThread:@selector(showAlertView:) withObject:aDictionary waitUntilDone:FALSE];
}

+ (void)exception:(NSString*)aTitle where:(NSString*)where exception:(NSException*)e
{
    NSString* aMessage = [NSString stringWithFormat:@"%@ %@ %@: %@", FXLLocalizedStringFromTable(@"EXCEPTION", @"ErrorAlert", @"Beginning of error alert message when exception occured"), where, [e name], [e reason]];
    [self alert:aTitle message:aMessage];
}


@end
