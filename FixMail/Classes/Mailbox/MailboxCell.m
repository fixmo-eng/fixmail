//
//  MailboxCell.m
//
//  Created by Gabor Cselle on 1/23/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "MailboxCell.h"
#import "ASEmail.h"
#import "DateUtil.h"
#import "AppSettings.h"
#import "UIView-ViewFrameGeometry.h"

static CGFloat kFieldPadding = 5.0;

@implementation MailboxCell

- (id) initWithStyle:(UITableViewCellStyle)inStyle reuseIdentifier:(NSString *)inReuseIdentifier
{
	self = [super initWithStyle:inStyle reuseIdentifier:inReuseIdentifier];
	if (!self) return nil;

	return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];

	int numberOfLines = self.numberOfLines;

	[self.dateLabel sizeToFit];
	self.dateLabel.right = 295.0;

	if (self.attachmentImage.hidden) {
		[self.fromLabel sizeToFit];
		self.fromLabel.width = (self.dateLabel.left - kFieldPadding) - self.fromLabel.left;
	} else {
		[self.fromLabel sizeToFit];
		self.attachmentImage.left = self.fromLabel.right + kFieldPadding;
		// figure out whether the attachment label will overlap the date label
		if (self.attachmentImage.right > (self.dateLabel.left - kFieldPadding)) {
			self.attachmentImage.right = self.dateLabel.left - kFieldPadding;
			self.fromLabel.width = (self.attachmentImage.left - kFieldPadding) - self.fromLabel.left;
		}
	}

	// make sure our status image is centered within the view
	CGPoint centerPoint = self.statusImage.center;
	centerPoint.y = self.height / 2.0;
	self.statusImage.center = centerPoint;

	[self.subjectLabel sizeToFit];
	CGRect subjectRect = self.subjectLabel.frame;
	subjectRect.size.width = 265.0;
	self.subjectLabel.frame = subjectRect;
	
	self.bodyLabel.numberOfLines = numberOfLines;
    CGSize bodySize = [self textSizeForLabel];
	CGRect bodyRect = self.bodyLabel.frame;
	bodyRect.origin.x = 35.0;
	bodyRect.origin.y = self.subjectLabel.bottom; // 45.0;
	bodyRect.size.width = 265.0;
	bodyRect.size.height = bodySize.height < numberOfLines * 18.0 ? bodySize.height : numberOfLines * 18.0;
	self.bodyLabel.frame = bodyRect;

	return;
}

-(CGSize) textSizeForLabel
{
    CGSize bigSize = CGSizeMake(265.0, 9999);
    CGSize newSize = [self.bodyLabel.text sizeWithFont:self.bodyLabel.font constrainedToSize:bigSize lineBreakMode:NSLineBreakByCharWrapping];
    return newSize;
}

- (void) setEmail:(ASEmail *)inEmail
{
	_email = inEmail;

    if(inEmail.folder.folderType == kFolderTypeLocalDrafts) {
        self.fromLabel.text = inEmail.toAsAddressString;
    } else if (inEmail.from.length > 0) {
        self.fromLabel.text = inEmail.from;
    } else if (inEmail.replyTo.length > 0) {
        self.fromLabel.text = inEmail.replyTo;
	} else {
        self.fromLabel.text = @"<???@???.??>";
	}
	self.subjectLabel.text = inEmail.subject;

	if (self.bodyLabel.numberOfLines > 0) {
		self.bodyLabel.text = inEmail.preview;
	}

	if (inEmail.hasMeeting) {
		self.attachmentImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIconCalendar"];
		self.attachmentImage.hidden = false;
    } else if (inEmail.hasAttachments) {
		self.attachmentImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/attachmentIcon.png"];
		self.attachmentImage.hidden = false;
	} else {
		self.attachmentImage.hidden = true;
	}
    
	if (inEmail.isUnread) {
		self.statusImage.image = [UIImage imageNamed:@"FixMailRsrc.bundle/unreadIcon.png"];
		self.statusImage.hidden = false;
	} else{
		self.statusImage.hidden = true;
	}

	if (inEmail.datetime != nil) {
		DateUtil *du = [DateUtil getSingleton];
		self.dateLabel.text = [du humanDate:inEmail.datetime];
	} else {
		self.dateLabel.text = @"(unknown)";
	}

	[self setNeedsLayout];

	return;
}

@end
