/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *  
 *  SZCAttachmentView.m
 *  Created by Colin Biggin on 2013-02-26.
*/
#import "SZCAttachmentView.h"
#import "ASAttachment.h"
#import "ASItemOperations.h"
#import "ASAccount.h"
#import "AttachmentManager.h"
#import "AttachmentUtils.h"

#import "UIView-ViewFrameGeometry.h"

static const CGFloat kSZCAttachmentViewDefaultWidth	=	300.0;
static const CGFloat kSZCAttachmentViewDefaultHeight =	56.0;

#define kSCZAddressViewFontSize			17.0

#pragma mark -

@interface SZCAttachmentView ()
@property (nonatomic, assign) BOOL isDownloaded;
@property (nonatomic, assign) BOOL isDownloading;
@property (nonatomic, assign) int percentageDownloaded;
@property (nonatomic, assign) BOOL errorOccurredDownloading;

@property (nonatomic, strong) UIImageView *attachmentIcon;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UILabel *attachmentLabel;
@property (nonatomic, strong) UILabel *sizeLabel;
@property (nonatomic, strong) UIProgressView *progressIndicator;
@property (nonatomic, strong) UIImageView *downloadIcon;
@property (nonatomic, strong) UIImageView *selectIcon;
@property (nonatomic, strong) UILabel *percentageLabel;

@end

@implementation SZCAttachmentView

- (void) initDefaults
{
	self.backgroundColor = [UIColor clearColor];
	self.userInteractionEnabled = YES;

	self.attachmentIcon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 8, 40.0, 40.0)];
	self.attachmentLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 8, 220.0, 21.0)];
	self.sizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 27, 220.0, 21.0)];
	self.progressIndicator = [[UIProgressView alloc] initWithFrame:CGRectMake(50, 33, 195.0, 9.0)];
	self.downloadIcon = [[UIImageView alloc] initWithFrame:CGRectMake(275, 18, 20.0, 20.0)];
	self.selectIcon = [[UIImageView alloc] initWithFrame:CGRectMake(275, 18, 20.0, 20.0)];
	self.percentageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 27, 30.0, 21.0)];

	self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	self.activityIndicator.frame = CGRectMake(275, 18, 20.0, 20.0);
	
	self.attachmentIcon.image = nil;
	self.attachmentIcon.contentMode = UIViewContentModeCenter;

	self.activityIndicator.hidesWhenStopped = YES;
	self.activityIndicator.hidden = YES;

	self.attachmentLabel.backgroundColor = [UIColor clearColor];
	self.attachmentLabel.font = [UIFont boldSystemFontOfSize:15.0];
	self.attachmentLabel.textColor = [UIColor blueColor];
	self.attachmentLabel.textAlignment = NSTextAlignmentLeft;
	self.attachmentLabel.minimumScaleFactor = 0.8;
	self.attachmentLabel.numberOfLines = 1;
	self.attachmentLabel.lineBreakMode = NSLineBreakByTruncatingTail;

	self.sizeLabel.backgroundColor = [UIColor clearColor];
	self.sizeLabel.font = [UIFont systemFontOfSize:13.0];
	self.sizeLabel.textColor = [UIColor grayColor];
	self.sizeLabel.textAlignment = NSTextAlignmentLeft;
	self.sizeLabel.minimumScaleFactor = 0.8;
	self.sizeLabel.numberOfLines = 1;
	self.sizeLabel.lineBreakMode = NSLineBreakByTruncatingTail;

	self.downloadIcon.image = [UIImage imageNamed:@"FixMailRsrc.bundle/attachmentIconDownload.png"];
	self.downloadIcon.contentMode = UIViewContentModeCenter;

	self.selectIcon.image = [UIImage imageNamed:@"FixMailRsrc.bundle/attachmentIconSelect.png"];
	self.selectIcon.contentMode = UIViewContentModeCenter;

	self.progressIndicator.progress = 0.0;
	self.progressIndicator.hidden = YES;

	self.percentageLabel.backgroundColor = [UIColor clearColor];
	self.percentageLabel.font = [UIFont systemFontOfSize:13.0];
	self.percentageLabel.textColor = [UIColor grayColor];
	self.percentageLabel.textAlignment = NSTextAlignmentLeft;
	self.percentageLabel.minimumScaleFactor = 0.8;
	self.percentageLabel.numberOfLines = 1;
	self.percentageLabel.lineBreakMode = NSLineBreakByTruncatingTail;

	[self addSubview:self.attachmentIcon];
	[self addSubview:self.activityIndicator];
	[self addSubview:self.attachmentLabel];
	[self addSubview:self.sizeLabel];
	[self addSubview:self.downloadIcon];
	[self addSubview:self.selectIcon];
	[self addSubview:self.progressIndicator];
	[self addSubview:self.percentageLabel];

	self.layer.cornerRadius = 8.0;
	self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
	self.layer.borderWidth = 2.0;

	self.isDownloaded = NO;
	self.isDownloading = NO;
	self.percentageDownloaded = 0;

	UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
	tapGestureRecognizer.delegate = self;
 	[self addGestureRecognizer:tapGestureRecognizer];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadStarted:) name:kAttachmentManagerStartedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadFinished:) name:kAttachmentManagerFinishedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadProgress:) name:kAttachmentManagerProgressNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadError:) name:kAttachmentManagerErrorNotification object:nil];
}

- (id) initWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail delegate:(id<SZCAttachmentViewDelegate>)inDelegate;
{
	CGRect theFrame = CGRectMake(0.0, 0.0, kSZCAttachmentViewDefaultWidth, kSZCAttachmentViewDefaultHeight);
    self = [super initWithFrame:theFrame];
    if (self) {
		[self initDefaults];

		self.delegate = inDelegate;
		self.email = inEmail;
		self.attachment = inAttachment;
		self.filePath = nil;
		self.attachmentAction = attachmentActionNone;

		[self redoDownloadStatuses];
    }

	return self;
}

- (id) initWithPath:(NSString *)inFilePath delegate:(id<SZCAttachmentViewDelegate>)inDelegate
{
	CGRect theFrame = CGRectMake(0.0, 0.0, kSZCAttachmentViewDefaultWidth, kSZCAttachmentViewDefaultHeight);
    self = [super initWithFrame:theFrame];
    if (self) {
		[self initDefaults];

		self.filePath = inFilePath;
		self.delegate = inDelegate;
		self.email = nil;
		self.attachment = nil;
		self.attachmentAction = attachmentActionNone;

		self.isDownloaded = YES;
		self.isDownloading = NO;

		[self redoControls];
    }

	return self;
}

- (id) initWithFrame:(CGRect)inFrame
{
    self = [super initWithFrame:inFrame];
    if (self) {
		[self initDefaults];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)inDecoder
{
	self = [super initWithCoder:inDecoder];
	if (self) {
		[self initDefaults];
	}

	return self;
}

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIResponder methods

- (BOOL) canBecomeFirstResponder
{
	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): canBecomeFirstResponder", self.attachment.name, self.attachment.fileReference);
	return YES;
}

- (BOOL) becomeFirstResponder
{
	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): becomeFirstResponder", self.attachment.name, self.attachment.fileReference);
	[super becomeFirstResponder];

	return YES;
}

- (BOOL) resignFirstResponder
{
	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): resignFirstResponder", self.attachment.name, self.attachment.fileReference);
    [super resignFirstResponder];

	return YES;
}

#pragma mark - Delegation

- (void) setDelegate:(id <SZCAttachmentViewDelegate>)inDelegate
{
	if (inDelegate == _delegate) return;

	if ([(NSObject *)inDelegate conformsToProtocol:@protocol(SZCAttachmentViewDelegate)]) {
		_delegate = inDelegate;
	}
}

- (void) downloadAttachment
{
	if (self.isDownloaded) {
		if ((_delegate != nil) && [_delegate respondsToSelector:@selector(attachmentDownloaded:path:)]) {
			NSString *thePath = [AttachmentManager attachmentPathWithAttachment:self.attachment email:self.email];
			[_delegate attachmentDownloaded:self path:thePath];
		}
	} else if (self.email.folder.account.isReachable) {
		if (self.isDownloading) {
			; // don't do anything
		} else {
			[AttachmentManager downloadAttachment:self.attachment email:self.email];
		}
	} else {
		// fake a downloadStart and downloadError
		NSDictionary *userInfo =  @{ kAttachmentManagerASAttachmentUserInfo : self.attachment };
		[self downloadStarted:[NSNotification notificationWithName:kAttachmentManagerStartedNotification object:self userInfo:userInfo]];
		[self downloadError:[NSNotification notificationWithName:kAttachmentManagerErrorNotification object:self userInfo:userInfo]];
	}

	return;
}

#pragma mark - UIGesture delegate methods

- (void) handleTap:(UITapGestureRecognizer *)inRecognizer
{
	self.backgroundColor = [UIColor grayColor];
	[self performSelector:@selector(setBackgroundColor:) withObject:[UIColor clearColor] afterDelay:0.1];

	if (self.isDownloading) return;

	if (_delegate == nil) {
		[self downloadAttachment];
	} else if ((_delegate != nil) && [_delegate respondsToSelector:@selector(attachmentClicked:path:)]) {
		// have to be careful here since we can be either:
		// 1: pointing to an ASAttachment (messageViewerVC)
		// 2: pointing to a filePath (composeMailVC)
		if (self.attachment != nil) { // 1:
			NSString *thePath = [AttachmentManager attachmentPathWithAttachment:self.attachment email:self.email];
			[_delegate attachmentClicked:self path:thePath];
		} else { // 2:
			NSAssert(self.filePath.length > 0, @"self.filePath.length > 0");
			[_delegate attachmentClicked:self path:self.filePath];
		}
	}

	return;
}

#pragma mark - Private methods

- (void) redoDownloadStatuses
{
	self.isDownloaded = [AttachmentManager isAttachmentDownloaded:_attachment email:_email];
	if (!self.isDownloaded) {
		self.isDownloading = [AttachmentManager isAttachmentDownloading:_attachment email:_email];
		if (self.isDownloading) {
			self.percentageDownloaded = [AttachmentManager percentDownloaded:_attachment email:_email];
		}
	}

	[self redoControls];
}

- (void) redoControlsFilePath
{
	NSAssert(_filePath != nil, @"_filePath != nil");

	// it exists, find it's size
	NSDictionary *fileAttributes = [[FXLSafeZone secureFileManager] attributesOfItemAtPath:_filePath error:nil];
	unsigned long fileSize = [[fileAttributes objectForKey:NSFileSize] unsignedLongValue];

	NSString *theFileName = [_filePath lastPathComponent];

	self.attachmentLabel.text = theFileName;

	// if a filePath, then we know it exists so none of the
	// downloading stuff is applicable
	self.sizeLabel.hidden = NO;
	self.progressIndicator.hidden = YES;
	self.percentageLabel.hidden = YES;
	self.activityIndicator.hidden = YES;

	NSString *sizeString = [AttachmentUtils fileSizeString:fileSize];
	self.sizeLabel.textColor = [UIColor grayColor];
	self.sizeLabel.text = [NSString stringWithFormat:@"File: %@", sizeString];
	self.downloadIcon.hidden = YES;
	self.selectIcon.hidden = NO;

	self.attachmentIcon.image = [AttachmentUtils documentIconForFile:theFileName];

	return;
}

- (void) redoControlsEmail
{
	NSAssert(_attachment != nil, @"_attachment != nil");
	NSAssert(_email != nil, @"_email != nil");

	self.attachmentLabel.text = _attachment.name;

	self.sizeLabel.hidden = self.isDownloading;
	self.progressIndicator.hidden = !self.isDownloading;
	self.percentageLabel.hidden = !self.isDownloading;
	self.activityIndicator.hidden = !self.isDownloading;

	self.sizeLabel.textColor = [UIColor grayColor];
	NSString *sizeString = [AttachmentUtils fileSizeString:self.attachment.estimatedDataSize];
	if (self.isDownloaded) {
		self.sizeLabel.text = [NSString stringWithFormat:@"Downloaded: %@", sizeString];
		self.downloadIcon.hidden = YES;
		self.selectIcon.hidden = NO;
	} else if (self.isDownloading) {
		self.downloadIcon.hidden = YES;
		self.selectIcon.hidden = YES;
		[self.activityIndicator startAnimating];
	} else {
		self.downloadIcon.hidden = NO;
		self.selectIcon.hidden = YES;
		self.sizeLabel.text = [NSString stringWithFormat:@"Est. Size: %@", sizeString];
	}

	self.attachmentIcon.image = [AttachmentUtils documentIconForFile:_attachment.name];

	return;
}

- (void) redoControls
{
	if (self.filePath.length > 0) [self redoControlsFilePath];
	else [self redoControlsEmail];
}

- (void) redoProgress
{
	self.progressIndicator.progress = (float)(self.percentageDownloaded/100.0);
	self.percentageLabel.text = [NSString stringWithFormat:@"%d%%", self.percentageDownloaded];
}

#define RADIANS(__X) ((float) ((__X * 3.141592) / 180.0))

- (void) animateArrows
{
	self.downloadIcon.alpha = 1.0;
	self.downloadIcon.transform = CGAffineTransformIdentity;
	self.downloadIcon.hidden = NO;
	self.selectIcon.alpha = 0.0;
	self.selectIcon.transform = CGAffineTransformRotate(self.downloadIcon.transform, RADIANS(90));
	self.selectIcon.hidden = NO;
 	[UIView animateWithDuration:0.2
		animations:^{
			self.downloadIcon.transform = CGAffineTransformRotate(self.downloadIcon.transform, RADIANS(-90));
			self.downloadIcon.alpha = 0.0;
			self.selectIcon.transform = CGAffineTransformIdentity;
			self.selectIcon.alpha = 1.0;
		}
		completion:^(BOOL inFinished) {
			[self redoControls];
		}
	];
}

- (void) internalDownloadStarted
{
	if (self.isDownloading) return;
	if (self.isDownloaded) return;

	self.sizeLabel.textColor = [UIColor grayColor];
	self.sizeLabel.hidden = YES;
	self.progressIndicator.progress = 0.0;
	self.progressIndicator.hidden = NO;
	self.percentageLabel.text = @"";
	self.percentageLabel.hidden = NO;
	self.downloadIcon.hidden = YES;
	self.selectIcon.hidden = YES;
	self.activityIndicator.hidden = NO;
	[self.activityIndicator startAnimating];

	self.errorOccurredDownloading = NO;
	self.isDownloading = YES;

	self.percentageDownloaded = 0;
	[self redoProgress];
}

- (void) internalDownloadFinished
{
	self.progressIndicator.hidden = YES;
	self.percentageLabel.hidden = YES;
	[self.activityIndicator stopAnimating];

	if (self.errorOccurredDownloading) {
		self.isDownloading = NO;
		self.isDownloaded = NO;

	} else {
		self.isDownloading = NO;
		self.isDownloaded = YES;
		[self animateArrows];

		// check if the delegate want to do something with the attachment
		if ((_delegate != nil) && [_delegate respondsToSelector:@selector(attachmentDownloaded:path:)]) {
			NSString *thePath = [AttachmentManager attachmentPathWithAttachment:self.attachment email:self.email];
			[_delegate attachmentDownloaded:self path:thePath];
		}
	}

	return;
}

#pragma mark - AttachmentManager Notifications

- (void) downloadStarted:(NSNotification *)inNotification
{
	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	if (theAttachment != self.attachment) return;

	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): downloadStarted", self.attachment.name, self.attachment.fileReference);

	[self internalDownloadStarted];
}

- (void) downloadFinished:(NSNotification *)inNotification
{
	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	if (theAttachment != self.attachment) return;

	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): downloadFinished", self.attachment.name, self.attachment.fileReference);

	[self internalDownloadFinished];
}

- (void) downloadProgress:(NSNotification *)inNotification
{
	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	if (theAttachment != self.attachment) return;

	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): downloadProgress", self.attachment.name, self.attachment.fileReference);

	NSNumber *thePercentage = [inNotification.userInfo objectForKey:kAttachmentManagerPercentageUserInfo];
	self.percentageDownloaded = thePercentage.intValue;
	[self redoProgress];
}

- (void) downloadError:(NSNotification *)inNotification
{
	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	if (theAttachment != self.attachment) return;

	FXDebugLog(kFXLogSZCAttachmentView, @"SZCAttachmentView (%@-%@): downloadError", self.attachment.name, self.attachment.fileReference);

	self.sizeLabel.text = [NSString stringWithFormat:@"Error Downloading: %@", [AttachmentUtils fileSizeString:self.attachment.estimatedDataSize]];
	self.sizeLabel.textColor = [UIColor redColor];
	self.sizeLabel.hidden = NO;

	self.errorOccurredDownloading = YES;
	[self internalDownloadFinished];

	return;
}

@end

