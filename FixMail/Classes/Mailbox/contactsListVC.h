//
//  contactsListVC.h
//  FixMail
//
//  Created by Colin Biggin on 2013-02-21.
//  Copyright (c) 2013 Colin Biggin. All rights reserved.
//

@interface contactsListVC : UITableViewController <UIActionSheetDelegate>

@property (nonatomic, strong) NSString *searchString;
@property (nonatomic, readonly) int rowCount;

@end
