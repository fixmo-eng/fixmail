/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *  
 *  mailViewerVC.h
 *  Created by Colin Biggin on 2013-02-26.
*/
#import "SZCAddressView.h"
#import "SZCAttachmentView.h"
#import "ASItemOperations.h"
#import "SZCComposeDelegate.h"
#import "SZCSeamlessHeaderWebView.h"
#import "SZLAttachmentBrowserController.h"

@class ASEmail;
@class BaseFolder;
@class MeetingViewController;

@protocol mailViewerDelegate <NSObject>
- (NSUInteger) indexOfEmail:(ASEmail *)inEmail;
- (NSUInteger) emailTotal:(ASEmail *)inEmail;
- (ASEmail *) getEmailAtIndex:(int)inIndex;
- (void) emailSelected:(ASEmail *)inEmail;
@end

@interface mailViewerVC : SZCSeamlessHeaderWebView <SZCAddressViewDelegate,
											SZCAttachmentViewDelegate,
											UIScrollViewDelegate,
                                            ASItemOperationsDelegate,
											UIActionSheetDelegate,
											UIAlertViewDelegate,
                                            SZCComposeDelegate,
											SZLAttachmentBrowserDelegate,
                                            UIWebViewDelegate>

@property (nonatomic, strong) ASEmail *email;
@property (nonatomic, strong) BaseFolder *folder;
@property (nonatomic, strong) id<mailViewerDelegate> delegate;

@property (nonatomic, weak) IBOutlet SZCAddressView *fromView;
@property (nonatomic, weak) IBOutlet SZCAddressView *toView;
@property (nonatomic, weak) IBOutlet SZCAddressView *ccView;
@property (nonatomic, weak) IBOutlet SZCAddressView *bccView;
@property (nonatomic, weak) IBOutlet UILabel *subjectView;
@property (nonatomic, weak) IBOutlet UILabel *dateView;
@property (nonatomic, strong) IBOutlet MeetingViewController *meetingVC;

@property (nonatomic, weak) IBOutlet UIView *fromSeparator;
@property (nonatomic, weak) IBOutlet UIView *toSeparator;
@property (nonatomic, weak) IBOutlet UIView *ccSeparator;
@property (nonatomic, weak) IBOutlet UIView *bccSeparator;
@property (nonatomic, weak) IBOutlet UIView *dateSeparator;
@property (nonatomic, weak) IBOutlet UIView *attachmentSeparator;
@property (nonatomic, weak) IBOutlet UIView *invitationSeparator;

@property (nonatomic, weak) IBOutlet UILabel *securityLabel;
@property (nonatomic, weak) IBOutlet UIImageView *signingImageView;
@property (nonatomic, weak) IBOutlet UILabel *signingLabel;
@property (nonatomic, weak) IBOutlet UILabel *signedByLabel;
@property (nonatomic, weak) IBOutlet UILabel *signeeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *encryptedImageView;
@property (nonatomic, weak) IBOutlet UILabel *encryptedLabel;
@property (nonatomic, weak) IBOutlet UIView *securitySeparator;

@property (nonatomic, strong) UIBarButtonItem *splitViewBarButtonItem;

+ (mailViewerVC *) createMailViewerVCWithDelegate:(id<mailViewerDelegate>)inDelegate
				  email:(ASEmail *)inEmail
				 folder:(BaseFolder *)inFolder;

- (void) newMessagesArrived;

@end
