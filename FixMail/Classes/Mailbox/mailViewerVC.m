/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  mailViewerVC.m
 *  Created by Colin Biggin on 2013-02-26.
*/
#import "mailViewerVC.h"
#import "composeMailVC.h"
#import "UIView-ViewFrameGeometry.h"
#import "PKMIMEMessage+FixmoExtensions.h"
#import "UIStoryboard+FixmoExtensions.h"
#import "SZLConcreteApplicationContainer.h"
#import "UIColor+FixmoExtensions.h"
#import "UIAlertView+Modal.h"
#import "UIActionSheet+Modal.h"
#import "UIWebView+FixmoExtensions.h"
#import "NSData+Base64.h"

#import "ASAccount.h"
#import "ASAttachment.h"
#import "ASContact.h"
#import "ASEmail.h"
#import "ASEmailFolder.h"
#import "ASItemOperations.h"
#import "DateUtil.h"
#import "EmailAddress.h"
#import "ErrorAlert.h"
#import "PKContentType.h"
#import "PKMIMEMessage.h"
#import "MeetingRequest.h"
#import "MeetingViewController.h"
#import "Reachability.h"
#import "StringUtil.h"
#import "SZCFilePreviewController.h"
#import "AppSettings.h"

#import "AttachmentManager.h"
#import "AttachmentUtils.h"
#import "FXMEmailSecurity.h"
#import "SZLComposeMailViewController.h"

// Constants
static const CGFloat kMinimumAddressViewHeight			= 34.0;
static const CGFloat kSeparatorPadding					= 5.0;
static const CGFloat kFieldPadding						= 3.0;
static const CGFloat kBadgePadding						= 0.0;
static const CGFloat kTopY								= 0.0;
static const CGFloat kLeftX								= 5.0;
static const CGFloat kRightX							= 315.0;
static const CGFloat kSegmentedControlerHeight			= 30.0;

static const CGFloat kButtonOffsetX                     = 20.0f;
static const CGFloat kButtonOffsetY                     = 5.0f;
static const CGFloat kMeetingViewHeight                 = 40.0f;

static const int kActionSheetReplyForwardTag			= 100;
static const int kActionSheetReplyAllForwardTag			= 101;
static const int kActionSheetIncludeAttachmentsTag		= 102;

static const int kStartingFontSize                      = 160;
static const int kMinimumFontSize                       = 60;
static const int kMaximumFontSize                       = 800;
static const int kFontSizeInterval                      = 30;


@interface mailViewerVC ()
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutlet UILabel *activityLabel;
@property (nonatomic, strong) UISegmentedControl *navControl;
@property (nonatomic, strong) UIBarButtonItem *navButtons;
@property (nonatomic, strong) UIBarButtonItem *flagButton;
@property (nonatomic, strong) UIBarButtonItem *organizeButton;
@property (nonatomic, strong) UIBarButtonItem *trashButton;
@property (nonatomic, strong) UIBarButtonItem *replyButton;
@property (nonatomic, strong) UIBarButtonItem *composeButton;

@property (nonatomic, strong) UIBarButtonItem *largerFontButton;
@property (nonatomic, strong) UIBarButtonItem *smallerFontButton;

@property NSArray *fromList;
@property NSArray *toList;
@property NSArray *ccList;
@property NSArray *bccList;
@property NSUInteger emailIndex;
@property NSUInteger emailTotal;
@property NSUInteger messageFontSize;
@property (nonatomic, strong) NSMutableArray *leftBarButtonItems;
@property (nonatomic, strong) SZLAttachmentBrowserController *fileBrowserController;
@property (nonatomic, strong) ASAttachment *attachmentToDownload;

@property (nonatomic, strong) NSData *p12AttachmentData;
@property (nonatomic, strong) NSString *p12AttachmentPassPhrase;
@property (nonatomic, strong) NSData *certAttachmentData;
@property (nonatomic, strong) NSString *certEmailAddress;
@property (nonatomic, strong) NSData *smimeData;
@property (nonatomic, strong) NSString *messageSignee;
@property (nonatomic, assign) BOOL isSMIMEMessage;
@property (nonatomic, assign) BOOL isSMIMEMessageUnpacked;
@property (nonatomic, assign) BOOL decryptingMessageBody;
@property (nonatomic, assign) BOOL isSignatureVerified;
@property (nonatomic, assign) BOOL isSuccessfullyDecrypted;

@end

@implementation mailViewerVC

+ (mailViewerVC *) createMailViewerVCWithDelegate:(id<mailViewerDelegate>)inDelegate
				  email:(ASEmail *)inEmail
				 folder:(BaseFolder *)inFolder
{
    mailViewerVC *mailViewerVC;
	if (IS_IPAD()) {
		mailViewerVC = [UIStoryboard storyboardWithName:kStoryboard_MailViewer_iPad identifier:@"mailViewerVC"];
        mailViewerVC.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
	} else {
		mailViewerVC = [UIStoryboard storyboardWithName:kStoryboard_MailViewer_iPhone identifier:@"mailViewerVC"];
	}

    if (mailViewerVC) {
		mailViewerVC.delegate = inDelegate;
		mailViewerVC.folder = inFolder;
        mailViewerVC.email = inEmail;
        mailViewerVC.messageFontSize = kStartingFontSize;
    }

    return mailViewerVC;
}

- (void) viewDidLoad
{
	FXDebugLog(kFXLogMailViewerVC | kFXLogVCLoadUnload, @"mailViewerVC: viewDidLoad");
    [super viewDidLoad];

    [self.messageView setDelegate:self];

	self.navigationController.navigationBarHidden = false;
	self.navigationController.toolbarHidden = IS_IPAD();

    [self setupControls];
	[self redoHeaders];
    
	return;
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogMailViewerVC | kFXLogVCAppearDisappear, @"mailViewerVC: viewWillAppear");
    
	[super viewWillAppear:inAnimated];
	[self setupNotifications];

	self.fileBrowserController = nil;
	self.attachmentToDownload = nil;
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogMailViewerVC | kFXLogVCAppearDisappear, @"mailViewerVC: viewDidAppear");
	[super viewDidAppear:inAnimated];

	// email might be set but not checked so check here
	if (self.email != nil) {
		[self redoBody];
	}
	return;
}

- (void) viewWillDisappear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogMailViewerVC | kFXLogVCAppearDisappear, @"mailViewerVC: viewWillDisappear");

	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[self stopActivity:false];

	[super viewWillDisappear:inAnimated];
}

- (void) dealloc
{
	FXDebugLog(kFXLogMailViewerVC | kFXLogVCLoadUnload, @"mailViewerVC: dealloc");

	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}

#pragma mark - Delegation

- (void) setDelegate:(id <mailViewerDelegate>)inDelegate
{
	if (inDelegate == _delegate) return;

	if ([(NSObject *)inDelegate conformsToProtocol:@protocol(mailViewerDelegate)]) {
		_delegate = inDelegate;
	}
}

- (void) newMessagesArrived
{
	// get the counts
	NSAssert(_delegate != nil, @"_delegate != nil");
	self.emailIndex = [_delegate indexOfEmail:self.email];
	self.emailTotal = [_delegate emailTotal:self.email];

	[self redoNavControls];
}

#pragma mark - Getters/Setters

-(NSMutableArray *)leftBarButtonItems
{
    if (!_leftBarButtonItems) {
        _leftBarButtonItems = [[NSMutableArray alloc] initWithCapacity:3];
        [_leftBarButtonItems addObject:[[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem]];
    }
    return _leftBarButtonItems;
}

- (void) setEmail:(ASEmail *)inEmail
{
	// get the counts
	NSAssert(_delegate != nil, @"_delegate != nil");
	self.emailIndex = [_delegate indexOfEmail:inEmail];
	self.emailTotal = [_delegate emailTotal:inEmail];

	if (_email == inEmail) return;
	_email = inEmail;

	// reset s/mime flags and determine if an s/mime message.
	self.isSuccessfullyDecrypted = false;
	self.isSignatureVerified = false;
	self.isSMIMEMessage = [self determineIfSMIME:inEmail];
	self.isSMIMEMessageUnpacked = NO;
	self.decryptingMessageBody = false;
	self.attachmentToDownload = nil;
	self.p12AttachmentData = nil;
	self.p12AttachmentPassPhrase = nil;
	self.certAttachmentData = nil;
	self.certEmailAddress = nil;

	self.toList		= inEmail.toAsArrayOfEmailAddresses;
	self.ccList		= inEmail.ccAsArrayOfEmailAddresses;
	self.bccList	= inEmail.bccAsArrayOfEmailAddresses;

	EmailAddress *fromAddress = [[EmailAddress alloc] initWithString:inEmail.from];
	if (fromAddress.address.length == 0) { // is invalid, so set name anyway
		fromAddress.name = inEmail.from;
		if (inEmail.replyTo.length > 0) {
			fromAddress.address = inEmail.replyTo;
		}
	} else {
		if (inEmail.replyTo.length > 0) {
			// replace the email address only
			EmailAddress *replyTo = [[EmailAddress alloc] initWithString:inEmail.replyTo];
			fromAddress.address = replyTo.address;
		}
	}

	self.fromList	= @[fromAddress];

	if (self.isViewLoaded) {
		self.messageView.hidden = true;
		[self redoHeaders];
		[self redoBody];
		self.messageView.hidden = false;
	}

	// leave this to the end
	[_delegate emailSelected:_email];

	return;
}


-(void) setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    if (self.splitViewBarButtonItem) {
        [self.leftBarButtonItems removeObject:self.splitViewBarButtonItem];
    }
    if (barButtonItem) {
        [self.leftBarButtonItems insertObject:barButtonItem atIndex:1];
    }
    self.navigationItem.leftBarButtonItems = self.leftBarButtonItems;
    _splitViewBarButtonItem = barButtonItem;
}

#pragma mark - IBActions

- (IBAction) toggleFlag:(id)inSender
{
	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
												 delegate:self
										cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_FLAG_OPTION", @"mailViewerVC", @"Cancel option for action sheet with flag options")
								   destructiveButtonTitle:nil
										otherButtonTitles:FXLLocalizedStringFromTable(@"MARK_UNREAD_FLAG_OPTION", @"mailViewerVC", @"Mark as unread option for action sheet with flag options"), nil
						  ];
	int indexClicked;
	if (IS_IPAD()) indexClicked = [theActionSheet showModalInView:self.view];
	else indexClicked = [theActionSheet showModalFromToolbar:self.navigationController.toolbar];

	if (indexClicked == 0) {
		[self.email setIsRead:NO];
	}

	return;
}

- (IBAction) toggleOrganize:(id)inSender
{
}

- (IBAction) toggleTrash:(id)inSender
{
	bool deleteMessage = true;
	if ([AppSettings emailAskBeforeDeleting]) {
		UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"DELETE_MESSAGE", @"mailViewerVC", @"Alert view title for message deletion confirmation")
															   message:nil
															  delegate:self
													 cancelButtonTitle:nil
													 otherButtonTitles:FXLLocalizedStringFromTable(@"YES_DELETE_MESSAGE", @"mailViewerVC", @"Yes button for message deletion confirmation"),
																		FXLLocalizedStringFromTable(@"NO_DELETE_MESSAGE", @"mailViewerVC", @"Alert view title for message deletion confirmation"), nil
									 ];
		int theButtonIndex = [theAlertView showModal];
		if (theButtonIndex == 1) deleteMessage = false;
	}

	if (deleteMessage) {
		[self deleteCurrentMessage];
		[self.navigationController popViewControllerAnimated:YES];
	}

	return;
}

- (IBAction) toggleReply:(id)inSender
{
	int recipientCount = self.toList.count + self.ccList.count + self.bccList.count;

	UIActionSheet *theActionSheet = nil;
	if (self.email.hasAttachments) {
		if (recipientCount > 1) {
			theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														 delegate:self
												cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_REPLY", @"mailViewerVC", @"Cancel option for action sheet with reply/forward options")
										   destructiveButtonTitle:nil
												otherButtonTitles:FXLLocalizedStringFromTable(@"REPLY", @"mailViewerVC", @"Reply option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"REPLY_ALL", @"mailViewerVC", @"Reply All option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD_ATTACHMENTS", @"mailViewerVC", @"Forward option with attachments for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD_NO_ATTACHMENTS", @"mailViewerVC", @"Forward option with no attachments for action sheet with reply/forward options"), nil
							  ];
			theActionSheet.tag = kActionSheetReplyAllForwardTag;
		} else {
			theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														 delegate:self
												cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_REPLY", @"mailViewerVC", @"Cancel option for action sheet with reply/forward options")
										   destructiveButtonTitle:nil
												otherButtonTitles:FXLLocalizedStringFromTable(@"REPLY", @"mailViewerVC", @"Reply option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD_ATTACHMENTS", @"mailViewerVC", @"Forward option with attachments for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD_NO_ATTACHMENTS", @"mailViewerVC", @"Forward option with no attachments for action sheet with reply/forward options"), nil
							  ];
			theActionSheet.tag = kActionSheetReplyForwardTag;
		}
	} else {
		if (recipientCount > 1) {
			theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														 delegate:self
												cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_REPLY", @"mailViewerVC", @"Cancel option for action sheet with reply/forward options")
										   destructiveButtonTitle:nil
												otherButtonTitles:FXLLocalizedStringFromTable(@"REPLY", @"mailViewerVC", @"Reply option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"REPLY_ALL", @"mailViewerVC", @"Reply All option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD", @"mailViewerVC", @"Forward option for action sheet with reply/forward options"), nil
							  ];
			theActionSheet.tag = kActionSheetReplyAllForwardTag;
		} else {
			theActionSheet = [[UIActionSheet alloc] initWithTitle:nil
														 delegate:self
												cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_REPLY", @"mailViewerVC", @"Cancel option for action sheet with reply/forward options")
										   destructiveButtonTitle:nil
												otherButtonTitles:FXLLocalizedStringFromTable(@"REPLY", @"mailViewerVC", @"Reply option for action sheet with reply/forward options"),
																FXLLocalizedStringFromTable(@"FORWARD", @"mailViewerVC", @"Forward option for action sheet with reply/forward options"), nil
							  ];
			theActionSheet.tag = kActionSheetReplyForwardTag;
		}
	}

	[theActionSheet showFromToolbar:self.navigationController.toolbar];
	return;
}

- (IBAction) toggleCompose:(id)sender
{
    UINavigationController *navController = [composeMailVC createNavigationMailComposerWithTo:nil cc:nil bcc:nil attachments:nil account:self.folder.account];
	[self presentViewController:navController animated:true completion:nil];
}

- (void) toggleMailNav:(id)inSender
{
	NSAssert(_delegate != nil, @"_delegate != nil");
	self.emailIndex = [_delegate indexOfEmail:self.email];
	self.emailTotal = [_delegate emailTotal:self.email];

	if (self.navControl.selectedSegmentIndex == 0) {
		self.email = [_delegate getEmailAtIndex:self.emailIndex-1];
	} else {
		self.email = [_delegate getEmailAtIndex:self.emailIndex+1];
	}

	return;
}

-(void)toggleContactVCBackButton:(id)sender
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void) fetchedMessage:(ASEmail*)inEmail
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC: fetchedMessage");

	if (inEmail == self.email) {
		[self redoBody];
	}

	return;
}

#pragma mark - SZCAddressViewDelegate methods

- (void) addressViewEmailAddressClicked:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress
{
	FXDebugLog(kFXLogMailViewerVC, @"addressViewEmailAddressClicked: %@ <%@>", inEmailAddress.name, inEmailAddress.address);
	
	UIViewController *contactVC = [inEmailAddress asContactViewController];
		
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactVC];
		navController.modalPresentationStyle = UIModalPresentationPageSheet;
		contactVC.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"BACK_TO_EMAIL", @"ASContact", @"Back button for returning to email when viewing contact from email viewer screen") style:UIBarButtonItemStyleDone target:self action:@selector(toggleContactVCBackButton:)];
		
		[self.navigationController presentViewController:navController animated:YES completion:nil];
}

#pragma mark - SZCAttachmentViewDelegate methods

- (void) attachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	NSParameterAssert(inAttachmentView != nil);

	// we have to check what kind of attachment it is and act accordingly
	//
	// Is it a .p12 attachment
	//	-> Yes, ask if it should be used as default certificate
	//	-> No, is it a viewable attachment?
	//		Yes, ask to download or view
	//		No, ask to download

	NSString *theFileName = [inAttachmentView.attachment.name lastPathComponent];
	NSString *theFileExtension = [inAttachmentView.attachment.name pathExtension];

	if ([theFileExtension caseInsensitiveCompare:@"p12"] == NSOrderedSame) {
		[self p12AttachmentClicked:inAttachmentView path:inAttachmentPath];
	} else if ([theFileExtension caseInsensitiveCompare:@"cer"] == NSOrderedSame) {
		[self certAttachmentClicked:inAttachmentView path:inAttachmentPath];
	} else if ([AttachmentUtils isAttachmentViewable:theFileName]) {
		[self viewableAttachmentClicked:inAttachmentView path:inAttachmentPath];
	} else {
		[self binaryAttachmentClicked:inAttachmentView path:inAttachmentPath];
	}

	return;
}

- (void) attachmentDownloaded:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	NSParameterAssert(inAttachmentView != nil);

	if (inAttachmentView.attachmentAction == attachmentActionMakeDefaultCertificate) {
		self.p12AttachmentData = [[FXLSafeZone defaultSafeZoneFileManager] contentsAtPath:inAttachmentPath];
		[self makeDefaultCertificate];
	} else if (inAttachmentView.attachmentAction == attachmentActionAddCertToDB) {
		self.certAttachmentData = [[FXLSafeZone defaultSafeZoneFileManager] contentsAtPath:inAttachmentPath];
		[self addCertToDB];
	} else if (inAttachmentView.attachmentAction == attachmentActionView) {
		NSAssert(inAttachmentView.attachment != nil, @"inAttachmentView.attachment != nil");
		SZCFilePreviewController *theController = [SZCFilePreviewController createPreviewer:inAttachmentPath
																					   name:inAttachmentView.attachment.name
																				 passPhrase:self.p12AttachmentPassPhrase
												   ];
		[self.navigationController pushViewController:theController animated:true];
	} else if (inAttachmentView.attachmentAction == attachmentActionSave) {
		NSAssert(self.fileBrowserController == nil, @"self.fileBrowserController == nil");
		NSAssert(self.attachmentToDownload == nil, @"self.attachmentToDownload == nil");

		self.attachmentToDownload = inAttachmentView.attachment;

		NSString *documentsDirectory = [FXLSafeZone documentsDirectory];
		self.fileBrowserController = [SZLAttachmentBrowserController launchFolderBrowserFromPath:documentsDirectory];
		self.fileBrowserController.delegate = self;
		[self.navigationController pushViewController:self.fileBrowserController animated:TRUE];
	}

	return;
}

#pragma mark SZCAttachmentViewDelegate private methods

- (void) p12AttachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"MAKE_CERTIFICATE_DEFAULT", @"mailViewerVC", @"Asking user what to do with the certificate")
																delegate:self
													   cancelButtonTitle:FXLLocalizedStringFromTable(@"NO_OPTION_CERTIFICATE_DEFAULT", @"mailViewerVC", @"Cancel Option of what to do with the certificate")
												  destructiveButtonTitle:nil
													   otherButtonTitles:FXLLocalizedStringFromTable(@"YES_OPTION_CERTIFICATE_DEFAULT", @"mailViewerVC", @"Yes Option of what to do with the certificate"), nil
									 ];

	int indexClicked;
	if (IS_IPAD()) indexClicked = [theActionSheet showModalInView:self.view];
	else indexClicked = [theActionSheet showModalFromToolbar:self.navigationController.toolbar];

	if ((indexClicked == theActionSheet.cancelButtonIndex) || (indexClicked == theActionSheet.destructiveButtonIndex)) {
		UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"WHATTODO_ATTACHMENT", @"mailViewerVC", @"Asking user what to do when clicking on the attachment")
																	delegate:self
														   cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_OPTION_WHATTODO_ATTACHMENT", @"mailViewerVC", @"Cancel Option when clicking on attachment")
													  destructiveButtonTitle:nil
														   otherButtonTitles:FXLLocalizedStringFromTable(@"VIEW_OPTION_WHATTODO_ATTACHMENT", @"mailViewerVC", @"Save Option when clicking on attachment"), nil
										 ];

		int indexClicked;
		if (IS_IPAD()) indexClicked = [theActionSheet showModalInView:self.view];
		else indexClicked = [theActionSheet showModalFromToolbar:self.navigationController.toolbar];

		if (indexClicked == theActionSheet.cancelButtonIndex) return;
		else if (indexClicked == theActionSheet.destructiveButtonIndex) return;

		if ([self enterPassPhrase]) {
			inAttachmentView.attachmentAction = attachmentActionView;
			[inAttachmentView downloadAttachment];
		}

	} else {
		if ([self enterPassPhrase]) {
			inAttachmentView.attachmentAction = attachmentActionMakeDefaultCertificate;
			[inAttachmentView downloadAttachment];
		}
	}

	return;
}

- (void) certAttachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:@"Add certificate to database?"
																delegate:self
													   cancelButtonTitle:@"No"
												  destructiveButtonTitle:nil
													   otherButtonTitles:@"Yes", nil
									 ];

	int indexClicked;
	if (IS_IPAD()) indexClicked = [theActionSheet showModalInView:self.view];
	else indexClicked = [theActionSheet showModalFromToolbar:self.navigationController.toolbar];

	if (indexClicked == theActionSheet.cancelButtonIndex) return;
	if (indexClicked == theActionSheet.destructiveButtonIndex) return;

	if ([self enterEmailAddress]) {
		inAttachmentView.attachmentAction = attachmentActionAddCertToDB;
		[inAttachmentView downloadAttachment];
	}

	return;
}

- (void) viewableAttachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	NSParameterAssert(inAttachmentView != nil);

	inAttachmentView.attachmentAction = attachmentActionView;
	[inAttachmentView downloadAttachment];

	return;
}

- (void) binaryAttachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	NSParameterAssert(inAttachmentView != nil);

	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"WHATTODO_ATTACHMENT", @"mailViewerVC", @"Asking user what to do when clicking on the attachment")
																delegate:self
													   cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_OPTION_WHATTODO_ATTACHMENT", @"mailViewerVC", @"Cancel Option when clicking on attachment")
												  destructiveButtonTitle:nil
													   otherButtonTitles:FXLLocalizedStringFromTable(@"SAVE_OPTION_WHATTODO_ATTACHMENT", @"mailViewerVC", @"Save Option when clicking on attachment"), nil
									 ];

	int indexClicked;
	if (IS_IPAD()) indexClicked = [theActionSheet showModalInView:self.view];
	else indexClicked = [theActionSheet showModalFromToolbar:self.navigationController.toolbar];

	if (indexClicked == theActionSheet.cancelButtonIndex) return;
	else if (indexClicked == theActionSheet.destructiveButtonIndex) return;

	if (indexClicked == 0) {
		inAttachmentView.attachmentAction = attachmentActionSave;
		[inAttachmentView downloadAttachment];
	}

	return;
}

- (BOOL) enterPassPhrase
{
	self.p12AttachmentPassPhrase = @"";

	// now get the pass-phrase for the certificate
	UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:nil
														   message:FXLLocalizedStringFromTable(@"ENTER_CERTIFICATE_PASSPHRASE", @"mailViewerVC", @"Asking user for the certificate pass-phrase")
														  delegate:self
												 cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_OPTION_CERTIFICATE_PASSPHRASE", @"mailViewerVC", @"Cancel Option of the certificate pass-phrase")
												 otherButtonTitles:FXLLocalizedStringFromTable(@"OK_OPTION_CERTIFICATE_PASSPHRASE", @"mailViewerVC", @"Ok Option of the certificate pass-phrase"), nil
								 ];
	theAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
	UITextField *theTextField = [theAlertView textFieldAtIndex:0];
	theTextField.keyboardType = UIKeyboardTypeDefault;
	int indexClicked = [theAlertView showModal];
	if (indexClicked == 0) return NO;

	// NOTE: we *DO* allow zero-length strings since no pass-phrase might have been entered... let the
	// certificate stuff do the verification.
	self.p12AttachmentPassPhrase = theTextField.text;

	return YES;
}

- (BOOL) enterEmailAddress
{
	self.certEmailAddress = @"";

	// now get the pass-phrase for the certificate
	UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:nil
														   message:@"Enter email address to store cert under"
														  delegate:self
												 cancelButtonTitle:@"Cancel"
												 otherButtonTitles:@"OK", nil
								 ];
	theAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
	UITextField *theTextField = [theAlertView textFieldAtIndex:0];
	theTextField.keyboardType = UIKeyboardTypeEmailAddress;
	int indexClicked = [theAlertView showModal];
	if (indexClicked == 0) return NO;

	self.certEmailAddress = theTextField.text;

	return (self.certEmailAddress.length > 0);
}

- (void) makeDefaultCertificate
{
	// we should only get here if we've agreed to use the .p12 attachment as our new certificate.
	ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
	NSAssert(currentAccount != nil, @"currentAccount != nil");

	if ([FXMEmailSecurity saveUserCertificateForEmail:currentAccount.emailAddress withP12Data:self.p12AttachmentData withPassphrase:self.p12AttachmentPassPhrase]) {
		// enable SMIME now
		[AppSettings setEmailSMIME:YES];
	} else {
		[ErrorAlert alert:FXLLocalizedStringFromTable(@"ERROR_ADDING_CERTIFICATE_TITLE", @"mailViewerVC", @"The title of the error alert when adding a cert fails")
				  message:FXLLocalizedStringFromTable(@"ERROR_ADDING_CERTIFICATE_MESSAGE", @"mailViewerVC", @"The message of the error alert when adding a cert fails")
		];
	}

	return;
}

- (void) addCertToDB
{
	NSParameterAssert(self.certAttachmentData != nil);
	NSParameterAssert(self.certEmailAddress != nil);

	// have to put the cert in base64 format first
	NSString *base64String = [self.certAttachmentData base64EncodedStringWithLinebreaks:YES];
	if ([FXMEmailSecurity saveCertificateForEmail:self.certEmailAddress withPublic:base64String]) {
		[ErrorAlert alert:@"Successful" message:[NSString stringWithFormat:@"Cert added for %@", self.certEmailAddress]];
	} else {
		[ErrorAlert alert:@"ERROR - Unsuccessful" message:[NSString stringWithFormat:@"Could not add cert for %@", self.certEmailAddress]];
	}

	return;
}

#pragma mark - SZLAttachmentBrowserDelegate methods

- (void) didCancel:(SZLAttachmentBrowserController *)inAttachmentBrowserController
{
	NSParameterAssert(inAttachmentBrowserController != nil);

	[self.navigationController popViewControllerAnimated:YES];
}

- (void) didSelectItem:(SZLAttachmentBrowserController *)inAttachmentBrowserController path:(NSString *)inFilePath
{
	NSParameterAssert(inAttachmentBrowserController != nil);
	NSParameterAssert(inFilePath.length > 0);
	NSAssert(self.attachmentToDownload != nil, @"self.attachmentToDownload != nil");

	// copy it over
	NSString *fromPath = [AttachmentManager attachmentPathWithAttachment:self.attachmentToDownload email:self.email];
	NSString *toPath = [inFilePath stringByAppendingPathComponent:self.attachmentToDownload.name];

	// have to check if the file name already exists and ask to replace existing or rename it with
	// a numbered extension, eg, text.doc -> text(1).doc -> text(2).doc
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:toPath]) {
		NSString *replacementPath = [AttachmentUtils createUniquePath:inFilePath name:self.attachmentToDownload.name];
		UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_TITLE", @"mailViewerVC", @"Duplicate filename found title")
															   message:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_QUERY", @"mailViewerVC", @"Asking user to replace or rename the duplicate filename")
															  delegate:self
													 cancelButtonTitle:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_CANCEL", @"mailViewerVC", @"Cancel Option of the duplicate filename")
													 otherButtonTitles:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_REPLACE", @"mailViewerVC", @"Replace Option of the duplicate filename"),
																	   FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_RENAME", @"mailViewerVC", @"Rename Option of the duplicate filename"), nil
									 ];
		int indexClicked = [theAlertView showModal];
		if (indexClicked != theAlertView.cancelButtonIndex) {
			if (indexClicked == 1) {
				[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:toPath error:nil];
			} else {
				[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:replacementPath error:nil];
			}
		}
	} else {
		[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:toPath error:nil];
	}

	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIScrollViewDelegate methods

-(void) scrollViewDidZoom:(UIScrollView *)scrollView
{
    [super scrollViewDidZoom:scrollView];
}

#pragma mark - UIActionSheetDelegate methods

- (void) actionSheet:(UIActionSheet *)inActionSheet clickedButtonAtIndex:(NSInteger)inButtonIndex
{	typedef enum { actionReply=10, actionReplyAll, actionForwardAttachments, actionForwardNoAttachments, actionCancel } tAction;

	tAction theAction = actionCancel;
	if (inButtonIndex == inActionSheet.cancelButtonIndex) return;

	if (inActionSheet.tag == kActionSheetReplyForwardTag) {
		if (inButtonIndex == 0) theAction = actionReply;
		else if (inButtonIndex == 1) theAction = actionForwardAttachments;
		else if (inButtonIndex == 2) theAction = actionForwardNoAttachments;
	} else if (inActionSheet.tag == kActionSheetReplyAllForwardTag) {
		if (inButtonIndex == 0) theAction = actionReply;
		else if (inButtonIndex == 1) theAction = actionReplyAll;
		else if (inButtonIndex == 2) theAction = actionForwardAttachments;
		else if (inButtonIndex == 3) theAction = actionForwardNoAttachments;
	}

	composeMailVC *theController = nil;
	switch (theAction) {
		case actionReply:
			theController = [composeMailVC createMailComposerWithTo:self.fromList cc:nil bcc:nil attachments:nil account:[self.folder account]];
			theController.mailType = composeReply;
			theController.subject = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"RE:", @"mailViewerVC", @"Subject prefix when replying to email"), self.email.subject];
			theController.referenceEmail = self.email;
			if (self.smimeData != nil) theController.referenceEmailSMimeData = self.smimeData;
			break;
		case actionReplyAll: {
			// couple things, add the 'From' to the 'To' list and then check each list and eliminate
			// our address from it
			NSMutableArray *tmpToList = [self sanitizedList:self.toList];
			[tmpToList addObjectsFromArray:self.fromList];
			NSMutableArray *tmpCcList = [self sanitizedList:self.ccList];
			NSMutableArray *tmpBccList = [self sanitizedList:self.bccList];
			theController = [composeMailVC createMailComposerWithTo:tmpToList cc:tmpCcList bcc:tmpBccList attachments:nil account:[self.folder account]];
			theController.mailType = composeReply;
			theController.subject = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"RE:", @"mailViewerVC", @"Subject prefix when replying to email"), self.email.subject];
			theController.referenceEmail = self.email;
			if (self.smimeData != nil) theController.referenceEmailSMimeData = self.smimeData;
		}	break;
		case actionForwardAttachments:
			theController = [composeMailVC createMailComposerWithTo:nil cc:nil bcc:nil attachments:self.email.attachments account:[self.folder account]];
			theController.mailType = composeForward;
			theController.subject = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"FWD:", @"mailViewerVC", @"Subject prefix when forwarding email"), self.email.subject];
			theController.referenceEmail = self.email;
			if (self.smimeData != nil) theController.referenceEmailSMimeData = self.smimeData;
			break;
		case actionForwardNoAttachments:
			theController = [composeMailVC createMailComposerWithTo:nil cc:nil bcc:nil attachments:nil account:[self.folder account]];
			theController.mailType = composeForward;
			theController.subject = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"FWD:", @"mailViewerVC", @"Subject prefix when forwarding email"), self.email.subject];
			theController.referenceEmail = self.email;
			if (self.smimeData != nil) theController.referenceEmailSMimeData = self.smimeData;
			break;
		case actionCancel:
		default:
			break;
	}

	if (theController != nil) {
		UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:theController];
		[self presentViewController:navController animated:true completion:nil];
	}

	return;
}

#pragma mark - UIWebViewDelegate methods

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
	if (self.email.isUnread) {
		[self.email changeReadStatus:TRUE];
	}

	int bodyHeight = [self.messageView bodyHeight];
	self.messageView.scrollView.contentSize = CGSizeMake(self.messageView.scrollView.width, bodyHeight);
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		if ([composeMailVC urlHasEmailScheme:request.URL]) {
			composeMailVC *composer = [composeMailVC createMailComposerWithTo:nil cc:nil bcc:nil attachments:nil account:[self.folder account]];
			[composer applyEmailSchemeURL:request.URL];
			[self presentViewController:composer animated:true completion:nil];
		} else {
			[[SZLConcreteApplicationContainer sharedApplicationContainer] launchBrowserWithURL:request.URL closeCompletionHandler:nil];
		}
		
		return NO;
	}
	return YES;
}

#pragma mark - Private methods

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadStarted:) name:kAttachmentManagerStartedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadFinished:) name:kAttachmentManagerFinishedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadProgress:) name:kAttachmentManagerProgressNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadError:) name:kAttachmentManagerErrorNotification object:nil];
}

- (void) setupControls
{
	self.toView.backgroundColor = [UIColor clearColor];
	self.ccView.backgroundColor = [UIColor clearColor];
	self.bccView.backgroundColor = [UIColor clearColor];
	self.fromView.backgroundColor = [UIColor clearColor];

	CGRect separatorRect = CGRectMake(0.0, 0.0, self.view.width, 1.0);
	self.toSeparator.frame = separatorRect;
	self.toSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
	self.ccSeparator.frame = separatorRect;
	self.ccSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
	self.bccSeparator.frame = separatorRect;
	self.bccSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
	self.fromSeparator.frame = separatorRect;
	self.fromSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
	self.dateSeparator.frame = separatorRect;
	self.dateSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
	self.attachmentSeparator.frame = separatorRect;
	self.attachmentSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
    self.invitationSeparator.frame = separatorRect;
	self.invitationSeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];
    self.securitySeparator.frame = separatorRect;
	self.securitySeparator.backgroundColor = [UIColor mailTableViewSeparatorColor];

	self.fromView.delegate = self;
	self.fromView.editable = false;
	self.fromView.title = FXLLocalizedStringFromTable(@"FROM_COLON", @"mailViewerVC", @"Sender (from) field title when viewing email");

	self.toView.delegate = self;
	self.toView.editable = false;
	self.toView.title = FXLLocalizedStringFromTable(@"TO_COLON", @"mailViewerVC", @"Recipient (to) field title when viewing email");

	self.ccView.delegate = self;
	self.ccView.editable = false;
	self.ccView.title = FXLLocalizedStringFromTable(@"CC_COLON", @"mailViewerVC", @"Cc field title when viewing email");

	self.bccView.delegate = self;
	self.bccView.editable = false;
	self.bccView.title = FXLLocalizedStringFromTable(@"BCC_COLON", @"mailViewerVC", @"Bcc field title when viewing email");

	self.subjectView.numberOfLines = 0;

	self.securityLabel.text = FXLLocalizedStringFromTable(@"SECURITY_COLON", @"mailViewerVC", @"Security field title when viewing email");

	self.fromSeparator.hidden = true;
	self.toSeparator.hidden = true;
	self.ccSeparator.hidden = true;
	self.bccSeparator.hidden = true;
	self.dateSeparator.hidden = true;
	self.attachmentSeparator.hidden = true;
	self.invitationSeparator.hidden = true;
	self.securitySeparator.hidden = true;

	//
	// Navigation bar - "Segmented" control to the right
	//
	self.navControl = [[UISegmentedControl alloc] initWithItems:
						[NSArray arrayWithObjects:
							[UIImage imageNamed:@"FixMailRsrc.bundle/arrowWhiteUp.png"],
							[UIImage imageNamed:@"FixMailRsrc.bundle/arrowWhiteDown.png"],
						 nil]];
	[self.navControl addTarget:self action:@selector(toggleMailNav:) forControlEvents:UIControlEventValueChanged];
	self.navControl.frame = CGRectMake(0, 0, 90, kSegmentedControlerHeight);
	self.navControl.segmentedControlStyle = UISegmentedControlStyleBar;
	self.navControl.momentary = YES;

	self.navButtons = [[UIBarButtonItem alloc] initWithCustomView:self.navControl];

	//
	// Toolbar
	//
	self.flagButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FixMailRsrc.bundle/flagIcon.png"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleFlag:)];
//	self.organizeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:@selector(toggleOrganize:)];
	self.trashButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(toggleTrash:)];
	self.replyButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemReply target:self action:@selector(toggleReply:)];
	self.composeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(toggleCompose:)];

	if (IS_IPAD()) {
		self.navControl.tintColor = [UIColor lightGrayColor];
        [self addLeftBarButtonItem:self.navButtons];
		self.navigationItem.rightBarButtonItems = @[self.flagButton, self.trashButton, self.replyButton, self.composeButton];

	} else {
		UIBarButtonItem *spacerItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

		self.navigationItem.rightBarButtonItem = self.navButtons;
		self.toolbarItems = @[self.flagButton, spacerItem, self.trashButton, spacerItem, self.replyButton, spacerItem, self.composeButton];
	}
    

	//
	// Activity Indicator
	// - initially just keep it out of the view hierarchy
	self.activityView.layer.cornerRadius = 10.0;
	self.activityIndicator.hidesWhenStopped = true;
	self.activityLabel.text = @"";
	[self.activityView removeFromSuperview];
    
    // Meeting Request
    //
    self.meetingVC = [MeetingViewController controller];
    self.meetingVC.view.hidden = TRUE;
    self.meetingVC.inMailView = TRUE;
    [self.headerView addSubview:self.meetingVC.view];

	return;
}

-(void)addLeftBarButtonItem:(UIBarButtonItem *)button
{
    if (button) [self.leftBarButtonItems addObject:button];
    self.navigationItem.leftBarButtonItems = self.leftBarButtonItems;
}

- (void) redoNavControls
{
	if ((self.emailTotal == NSNotFound) || (self.emailIndex == NSNotFound)) {
		self.navigationItem.title =  FXLLocalizedStringFromTable(@"NO_TOTAL_FOUND", @"mailViewerVC", @"Navigation Bar display when no total email number found");
	} else if (self.emailTotal > 0) {
		self.navigationItem.title = [NSString stringWithFormat:@"%d%@%d", self.emailIndex+1, FXLLocalizedStringFromTable(@"_OF_", @"mailViewerVC", @"Number separation when displaying that email is number X email of total Y emails (X of Y)"), self.emailTotal];
		[self.navControl setEnabled:(self.emailIndex > 0) forSegmentAtIndex:0];
		[self.navControl setEnabled:(self.emailIndex < (self.emailTotal-1)) forSegmentAtIndex:1];
	} else {
		[self.navControl setEnabled:false forSegmentAtIndex:0];
		[self.navControl setEnabled:false forSegmentAtIndex:1];
		self.navigationItem.title = nil;
	}
    
	return;
}

- (CGFloat) redoAddressView:(SZCAddressView *)inAddressView separator:(UIView *)inSeparator list:(NSArray *)inList top:(CGFloat)inTop
{
	CGFloat newY = inTop;

    @try {
        if (inList.count == 0) {
            inAddressView.hidden = YES;
            inSeparator.hidden = YES;
        } else {
            [inAddressView replaceEmailAddresses:inList];
            CGFloat theHeight = inAddressView.rowHeight;
            inAddressView.top = newY + kSeparatorPadding;
            inAddressView.left = kLeftX;
            inAddressView.width = self.view.width - inAddressView.left;

			[inAddressView sizeToFit];

            if (inAddressView.height != theHeight) {
                inAddressView.height = (theHeight < kMinimumAddressViewHeight) ? kMinimumAddressViewHeight : theHeight;
            }
            inSeparator.top = inAddressView.bottom + kSeparatorPadding;
            
            inAddressView.hidden = NO;
            inSeparator.hidden = NO;

            newY = inSeparator.bottom;
        }

        [inAddressView setNeedsDisplay];
    }@catch (NSException* e) {
        [self _logException:@"redoAddressView" exception:e];
    }
	return newY;
}

- (void) redoHeaders
{
    @try {
		self.headerView.hidden = YES;

        CGFloat theY = kTopY;
        theY = [self redoAddressView:self.fromView separator:self.fromSeparator list:self.fromList top:theY];
        theY = [self redoAddressView:self.toView separator:self.toSeparator list:self.toList top:theY];
        theY = [self redoAddressView:self.ccView separator:self.ccSeparator list:self.ccList top:theY];
        theY = [self redoAddressView:self.bccView separator:self.bccSeparator list:self.bccList top:theY];

        // Subject View
        self.subjectView.text = self.email.subject;
        self.subjectView.top = theY + kSeparatorPadding;
        self.subjectView.left = kLeftX;
        self.subjectView.width = self.view.width - self.subjectView.left;
        
        [self.subjectView sizeToFit];

        // Date View
        //
        self.dateView.top = self.subjectView.bottom + 5.0;
        self.dateView.left = kLeftX;
        if (self.email.datetime != nil) {
			DateUtil *dateFormatter = [DateUtil getSingleton];
            self.dateView.text = [dateFormatter stringFromDateWeekdayAndDateLongStyle:self.email.datetime];
        } else {
            self.dateView.text = FXLLocalizedStringFromTable(@"(UNKNOWN)", @"mailViewerVC", @"Display when email date and time is unknown");
        }
        self.dateSeparator.top = self.dateView.bottom + kSeparatorPadding;
        self.dateSeparator.hidden = NO;

        theY = self.dateSeparator.bottom + kSeparatorPadding;
        
        // Attachment view if there is one
        //
		NSMutableArray *deleteArray = [NSMutableArray new];
		for (UIView *theSubview in self.headerView.subviews) {
			if (![theSubview isKindOfClass:[SZCAttachmentView class]]) continue;
			[deleteArray addObject:theSubview];
		}
		if (deleteArray.count > 0) {
			[deleteArray makeObjectsPerformSelector:@selector(removeFromSuperview)];
		}

        if(self.email.hasAttachments) {
             for (ASAttachment *theAttachment in self.email.attachments) {
				// check for .p7s attachment type
				NSString *fileExtension = [theAttachment.name pathExtension];
				if ([fileExtension caseInsensitiveCompare:@"p7m"] == NSOrderedSame) {
					self.decryptingMessageBody = true;
//					[AttachmentManager downloadAttachment:theAttachment email:self.email];
				} else {
					SZCAttachmentView *theAttachmentView = [[SZCAttachmentView alloc] initWithAttachment:theAttachment email:self.email delegate:self];
					theAttachmentView.top = theY;
					theAttachmentView.left = kLeftX;
					[self.headerView addSubview:theAttachmentView];
					theY += (theAttachmentView.height + 3.0);
				}
			}
        } else {
            self.attachmentSeparator.hidden = YES;
        }

		if (self.isSMIMEMessage) {
			self.securityLabel.left = kLeftX;
			self.securityLabel.top = theY;
			self.encryptedLabel.top = self.encryptedImageView.top = theY;
			self.signingLabel.top = self.signingImageView.top = theY;
			self.signedByLabel.top = theY;

			self.securitySeparator.top = self.securityLabel.bottom + kSeparatorPadding;
			theY += self.securitySeparator.top + kSeparatorPadding;
		
            self.securityLabel.hidden = NO;
            self.encryptedLabel.hidden = NO;
            self.encryptedImageView.hidden = NO;
            self.signingLabel.hidden = NO;
            self.signingImageView.hidden = NO;
			self.signedByLabel.hidden = NO;
            self.securitySeparator.hidden = NO;
			[self redoSecurityHeaders];
		} else {
            self.securityLabel.hidden = YES;
            self.encryptedLabel.hidden = YES;
            self.encryptedImageView.hidden = YES;
            self.signingLabel.hidden = YES;
            self.signingImageView.hidden = YES;
			self.signedByLabel.hidden = YES;
			self.signeeLabel.hidden = YES;
            self.securitySeparator.hidden = YES;
		}

        // Meeting request view if there is one
        //
        if(self.email.meetingRequest) {
            // NOTE: You need to temporarily set the headerView height here or the first time
            // through the MeetingView may not respond to taps, at least on an iPhone running
            // IOS 6.0.2.  On iPad reloadHeaders is called in a somewhat different sequence
            // and this hack isn't needed here
            //
            self.headerView.height = theY - kTopY;
            
            [self.meetingVC loadEmail:self.email];
            UIView* aView = self.meetingVC.view;
            aView.top = theY;
            aView.hidden = NO;
            
            self.invitationSeparator.top = aView.bottom + kSeparatorPadding;
            self.invitationSeparator.hidden = NO;
            theY = self.invitationSeparator.bottom + kSeparatorPadding;
        }else{
            UIView* aView = self.meetingVC.view;
            aView.hidden = YES;
            self.invitationSeparator.hidden = YES;
        }
        
        self.headerView.width = self.messageView.width;
        self.headerView.height = theY - kTopY;
        [self resizedHeaderViewIncludingVertically:YES];

        // WebView to display in the email body, in most cases
        //
        [self.messageView loadHTMLString:@"<html><body></body></html>" baseURL:nil];

        [self redoNavControls];
    }@catch (NSException* e) {
        [self _logException:@"redoHeaders" exception:e];
    }

	@finally {
		self.headerView.hidden = NO;
	}
	return;
}

- (void) redoBody
{
	if (![self checkIfFetched]) {
		if (self.email.isFetching) {
			[self startActivity:FXLLocalizedStringFromTable(@"FETCHING", @"mailViewerVC", @"Displayed by Activity View when email is being fetched from server") animated:true delay:0.0];
		}
		// put the preview in here
		if (self.email.preview.length > 0) {
			NSData *theData = [NSData dataWithBytes:[self.email.preview UTF8String] length:[self.email.preview length]];
			[self.messageView loadData:theData MIMEType:@"text/plain" textEncodingName:@"UTF-8" baseURL:nil];
		}
		return;
	}

	[self startActivity:FXLLocalizedStringFromTable(@"LOADING", @"mailViewerVC", @"Displayed by Activity View when email is being loaded from database") animated:true delay:1.0];

//	if ((self.isSMIMEMessage) && (!self.decryptingMessageBody)) {
	if (self.isSMIMEMessage) {
		NSString* mimeString = [[NSString alloc] initWithData:self.email.body encoding:NSUTF8StringEncoding];
		[self redoSMIMEBody:mimeString];
	} else {
		CGFloat theTop = self.messageView.top;
		int messageHeight = [PKMIMEMessage bodyBlock:self.email top:theTop addToView:self.messageView];

		self.messageView.height = messageHeight;
		//self.messageView.left = kLeftX;
		self.messageView.width = self.view.width - self.messageView.left;
		self.messageView.scrollView.contentSize = CGSizeMake(self.messageView.width, self.messageView.bottom);
		
		[self stopActivity:true];
	}

	return;
}

- (void) redoSMIMEBody:(NSString *)inMimeString
{
	if (inMimeString.length == 0) return;

//	NSString *prefix = @"Content-Type: application/x-pkcs7-mime;smime-type=enveloped-data;name=\"smime.p7m\"\nContent-Transfer-Encoding: base64\nContent-Disposition: attachment;filename=\"smime.p7m\"\n\n";
//	inMimeString = [prefix stringByAppendingString:inMimeString];
//
	[FXMEmailSecurity decryptAndValidateMessage:inMimeString withPrivateCertificateData:nil andPassphrase:nil completion:^(NSString *newMimeContent, BOOL isSignatureValid, BOOL didDecryptionSucceed, NSString *signedCertificate) {
		self.isSignatureVerified = isSignatureValid;
		self.isSuccessfullyDecrypted = didDecryptionSucceed;

		// extract the signee from the certificate - if there
		// if not, default to the from address
		self.messageSignee = nil;
		if (signedCertificate != nil) {
			self.messageSignee = [FXMEmailSecurity emailAttributeForPEMString:signedCertificate];
		}
		if (self.messageSignee == nil) {
			self.messageSignee = self.email.from;
		}

		//replace the data right now
		FXDebugLog(kFXLogSZLSecurity, @"Signature Valid: %@\nDecryption Succeeded: %@\n",
				isSignatureValid ? @"YES" : @"NO", didDecryptionSucceed ? @"YES" : @"NO");
		FXDebugLog(kFXLogSZLSecurity, @"=== New MIME Content  below ===\n%@\n=== New MIME Content above===\n", newMimeContent);

		self.smimeData = [NSData dataWithBytes:newMimeContent.UTF8String length:newMimeContent.length];

//		self.messageView.height = [PKMIMEMessage mimeBodyBlock:self.smimeData top:self.messageView.top addToView:self.messageView];
		//self.messageView.left = kLeftX;
		self.messageView.height = [PKMIMEMessage mimeBodyBlock:self.smimeData top:0 addToView:self.messageView];
		self.messageView.width = self.view.width - self.messageView.left;
		self.messageView.scrollView.contentSize = CGSizeMake(self.messageView.width, 5000);

		self.isSMIMEMessageUnpacked = YES;
		[self stopActivity:true];
		[self redoSecurityHeaders];
	}];

	return;
}

- (void) redoSecurityHeaders
{
	if (!self.isSMIMEMessageUnpacked) {
		self.securityLabel.text = FXLLocalizedStringFromTable(@"SECURITY_LABEL_DETERMINING", @"mailViewerVC", @"Security Label before encryption/signing is determined");
		self.encryptedLabel.hidden = YES;
		self.encryptedImageView.hidden = YES;
		self.signingLabel.hidden = YES;
		self.signingImageView.hidden = YES;
		self.signedByLabel.hidden = YES;
		self.signeeLabel.hidden = YES;
		return;
	}

	self.securityLabel.text = FXLLocalizedStringFromTable(@"SECURITY_LABEL", @"mailViewerVC", @"Security Label");
	[self.securityLabel sizeToFit];

	self.encryptedLabel.hidden = NO;
	self.encryptedImageView.hidden = NO;
	self.signingLabel.hidden = NO;
	self.signingImageView.hidden = NO;
	if (self.isSuccessfullyDecrypted) {
		self.encryptedLabel.text = FXLLocalizedStringFromTable(@"ENCRYPTED_LABEL", @"mailViewerVC", @"Encrypted Label");
		self.encryptedImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/lockCertIcon.png"];
	} else {
		self.encryptedLabel.text = FXLLocalizedStringFromTable(@"UNENCRYPTED_LABEL", @"mailViewerVC", @"Unencrypted Label");
		self.encryptedImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/unlockCertIcon.png"];
	}
	[self.encryptedLabel sizeToFit];
	self.encryptedImageView.left = self.securityLabel.right + kFieldPadding;
	self.encryptedLabel.left = self.encryptedImageView.right + kBadgePadding;

	if (self.isSignatureVerified) {
		self.signingLabel.text = FXLLocalizedStringFromTable(@"SIGNED_LABEL", @"mailViewerVC", @"Signed Label");
		self.signingImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/signCertIcon.png"];
	} else {
		self.signingLabel.text = FXLLocalizedStringFromTable(@"UNSIGNED_LABEL", @"mailViewerVC", @"Unsigned Label");
		self.signingImageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/unsignCertIcon.png"];
	}
	[self.signingLabel sizeToFit];
	self.signingImageView.left = self.encryptedLabel.right + kFieldPadding;
	self.signingLabel.left = self.signingImageView.right + kBadgePadding;

	// header has to be re-adjusted down
	if (self.isSignatureVerified) {
		self.signedByLabel.text = FXLLocalizedStringFromTable(@"SIGNED_BY_LABEL", @"mailViewerVC", @"Unsigned Label");
		self.signeeLabel.text = self.messageSignee;
		[self.signedByLabel sizeToFit];
		[self.signeeLabel sizeToFit];

		self.signedByLabel.top = self.securityLabel.bottom + kSeparatorPadding;
		self.signeeLabel.top = self.securityLabel.bottom + kSeparatorPadding;
		self.signedByLabel.left = kLeftX;
		self.signeeLabel.left = self.signedByLabel.right + kFieldPadding;

		self.signedByLabel.hidden = NO;
		self.signeeLabel.hidden = NO;

		self.securitySeparator.top = self.signedByLabel.bottom + kSeparatorPadding;

        self.headerView.height = self.securitySeparator.bottom - kTopY;
        [self resizedHeaderViewIncludingVertically:YES];

	} else {
		self.signedByLabel.hidden = YES;
		self.signeeLabel.hidden = YES;
	}

	return;
}

- (NSString *) debugView:(UIView *)inView title:(NSString *)inTitle
{
	return [NSString stringWithFormat:@"%@: y:%.0f x:%.0f w:%.0f h:%.0f alpha:%.1f hidden:%@",
		inTitle, inView.top, inView.left, inView.width, inView.height, inView.alpha, inView.hidden?@"true":@"false"];
}

- (void) delayStartActivity:(NSString *)inMessage
{
	[self startActivity:inMessage animated:true delay:0.0];
}

- (void) startActivity:(NSString *)inMessage animated:(bool)inAnimated delay:(NSTimeInterval)inDelayAmount
{
	if (self.activityView.superview != nil) {
		[NSObject cancelPreviousPerformRequestsWithTarget:self];
		self.activityLabel.text = inMessage;
		return;
	}

	if (inDelayAmount > 0.01) {
		[NSObject cancelPreviousPerformRequestsWithTarget:self];
		[self performSelector:@selector(delayStartActivity:) withObject:inMessage afterDelay:inDelayAmount];
		return;
	}

	if (inAnimated) {
		self.activityView.hidden = false;
		self.activityView.alpha = 0.0;
		self.activityLabel.text = inMessage;
		[self.activityIndicator startAnimating];
		self.activityView.center = self.view.center;
		[self.view addSubview:self.activityView];
		[self.view bringSubviewToFront:self.activityView];

		[UIView animateWithDuration:0.3
			animations:^{
				self.activityView.alpha = 1.0;
			}
		];
	} else {
		self.activityLabel.text = inMessage;
		[self.activityIndicator startAnimating];
		self.activityView.center = self.view.center;
		self.activityView.hidden = false;
		self.activityView.alpha = 1.0;
		[self.view addSubview:self.activityView];
		[self.view bringSubviewToFront:self.activityView];
	}

	return;
}

- (void) stopActivity:(bool)inAnimated
{
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
	if (self.activityView.superview == nil) return;

	if (inAnimated) {
		[UIView animateWithDuration:0.3
			animations:^{
				self.activityView.alpha = 0.0;
			}
			completion:^(BOOL inFinished) {
				[self.activityView removeFromSuperview];
			}
		];
	} else {
		[self.activityView removeFromSuperview];
		self.activityView.alpha = 0.0;
	}

	return;
}

- (bool) checkIfFetched
{
	NSAssert(self.email != nil, @"self.email != nil");
	NSAssert(self.folder != nil, @"folder != nil");

	if (self.email.isFetched) return YES;
	if (self.email.isFetching) return NO;

    if(self.email.folder.account.isReachable) {
        [self startActivity:FXLLocalizedStringFromTable(@"FETCHING", @"mailViewerVC", @"Displayed by Activity View when email is being fetched from server") animated:true delay:0.0];

        ASAccount* anAccount = (ASAccount*)self.email.folder.account;
        if(anAccount.accountSubType == AccountSubTypeDomino) {
            // Domino 8 generates and HTTP conflict if you do item ops with a ping active, so we have to queue the item op so the ping will be removed
            // and restarted when the item op is done
            [ASItemOperations queueFetchEmailBody:self.email smime:self.isSMIMEMessage delegate:self];
        }else{
            [ASItemOperations sendFetchEmailBody:self.email smime:self.isSMIMEMessage delegate:self];
        }
    }else{
        [self startActivity:FXLLocalizedStringFromTable(@"OFFLINE", @"mailViewerVC", @"Displayed by Activity View when server is offline and email cant be fetched") animated:true delay:0.0];
        [self.activityIndicator stopAnimating];
        [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:nil queue:nil
            usingBlock:^(NSNotification *inNotification) {
                Reachability* aReachability = inNotification.object;
                switch(aReachability.currentReachabilityStatus) {
                    case ReachableViaWWAN:
                    case ReachableViaWiFi:
                    {
                        [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
                        [self.activityIndicator startAnimating];
                        [self checkIfFetched];
                        break;
                    }
                    case NotReachable:
                        break;
                }
            }];
    }

	return NO;
}

- (NSMutableArray *) sanitizedList:(NSArray *)inList
{
	if (inList.count == 0) return nil;

	BaseAccount *theAccount = [self.folder account];
	NSAssert(theAccount != nil, @"theAccount != nil");

	EmailAddress *meAddress = [[EmailAddress alloc] initWithName:theAccount.userName address:theAccount.emailAddress];

	NSMutableArray *newArray = [NSMutableArray arrayWithCapacity:inList.count];
	for (EmailAddress *theEmailAddress in inList) {
		if ([theEmailAddress.address caseInsensitiveCompare:meAddress.address] == NSOrderedSame) continue;
		[newArray addObject:theEmailAddress];
	}

	return newArray;
}

- (void) deleteCurrentMessage
{
	@try {
		[self.folder deleteUid:self.email.uid updateServer:TRUE];
		[self.folder commitObjects];
	}@catch (NSException* e) {
		[self _logException:@"delete email" exception:e];
	}
}

- (BOOL) determineIfSMIME:(ASEmail *)inEmail
{
	BOOL isSMIME = NO;
	switch (inEmail.messageClass) {
		case kMessageClassNoteSMIME:
		case kMessageClassNoteReceiptSMIME:
		case kMessageClassNoteSMIMEMultipartSigned:
			isSMIME = YES;
			break;
		default:
			break;
	}

	return isSMIME;
}

////////////////////////////////////////////////////////////////////////////////////////////
// ASItemOperationsDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASItemOperationsDelegate

- (void)deliverProgress:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (deliverProgress): %@", message);
}

- (void)deliverError:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (deliverError): %@", message);
}

- (void)deliverFile:(NSString*)aPath itemOp:(ASItemOperations*)anItemOp
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (deliverFile): %@", aPath);
}

- (void)deliverData:(NSData *)aData itemOp:(ASItemOperations *)anItemOp
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (deliverData): %ld bytes", aData.length);

    //NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
    //FXDebugLog(kFXLogActiveSync, @"deliverData: %@", aDataString);
    ASEmail* anEmail = anItemOp.email;
    [anEmail loadBody:aData];
    if(anEmail == self.email) {
        [self fetchedMessage:anEmail];
    }
}

#pragma mark Error Handling

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"MAIL_VIEW", @"ErrorAlert", @"mailViewerVC error alert view title") message:anErrorMessage];
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"MAIL_VIEW", @"ErrorAlert", nil) where:where exception:e];
}

/*
- (void) loadEmailThread
{
	FXDebugLog(kFXLogMailViewerVC, @"mailViewerFC: -> loadEmailThread");

	NSAssert(self.email != nil, @"self.email != nil");
	NSAssert(self.folder != nil, @"folder != nil");

	[self startActivity:@"Loading Email..." animated:true];
	[[self.folder account] fetchEmail:self.email folder:self.folder delegate:self];

	FXDebugLog(kFXLogMailViewerVC, @"mailViewerFC: <- loadEmailThread");
}

- (void) checkIfFetched
{
	NSAssert(self.email != nil, @"self.email != nil");
	NSAssert(self.folder != nil, @"folder != nil");

	if (self.email.isFetched) return;
	if (self.email.isFetching) return;

	// so we don't have the body, go and get it
	NSThread *driverThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadEmailThread) object:nil];
	[driverThread start];

	return;
}
*/

#pragma mark - AttachmentManager Notifications

- (void) downloadStarted:(NSNotification *)inNotification
{
	ASEmail *theEmail = [inNotification.userInfo objectForKey:kAttachmentManagerEmailUserInfo];
	if (theEmail != self.email) return;

	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	NSAssert(theAttachment != nil, @"theAttachment != nil");

	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (%@-%@): downloadStarted", theAttachment.name, theAttachment.fileReference);
}

- (void) downloadFinished:(NSNotification *)inNotification
{
	ASEmail *theEmail = [inNotification.userInfo objectForKey:kAttachmentManagerEmailUserInfo];
	if (theEmail != self.email) return;

	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	NSAssert(theAttachment != nil, @"theAttachment != nil");

	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (%@-%@): downloadFinished", theAttachment.name, theAttachment.fileReference);

	NSData *theData = [AttachmentManager dataWithAttachment:theAttachment email:theEmail];
	NSString *base64String = [theData base64EncodedStringWithLinebreaks:YES];
	[self redoSMIMEBody:base64String];
}

- (void) downloadProgress:(NSNotification *)inNotification
{
	ASEmail *theEmail = [inNotification.userInfo objectForKey:kAttachmentManagerEmailUserInfo];
	if (theEmail != self.email) return;

	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	NSAssert(theAttachment != nil, @"theAttachment != nil");

	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (%@-%@): downloadProgress", theAttachment.name, theAttachment.fileReference);

	NSNumber *thePercentage = [inNotification.userInfo objectForKey:kAttachmentManagerPercentageUserInfo];
}

- (void) downloadError:(NSNotification *)inNotification
{
	ASEmail *theEmail = [inNotification.userInfo objectForKey:kAttachmentManagerEmailUserInfo];
	if (theEmail != self.email) return;

	ASAttachment *theAttachment = [inNotification.userInfo objectForKey:kAttachmentManagerASAttachmentUserInfo];
	NSAssert(theAttachment != nil, @"theAttachment != nil");

	FXDebugLog(kFXLogMailViewerVC, @"mailViewerVC (%@-%@): downloadError", theAttachment.name, theAttachment.fileReference);
}

@end
