/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMCertificateManager.h
 *
 *  Created by Colin Biggin on 2013-05-16.
 */

#import "ASResolveRecipientsDelegate.h"

#define kCertificateRetrievalStartedNotification		@"kCertificateRetrievalStartedNotification"
#define kCertificateRetrievalErrorNotification			@"kCertificateRetrievalErrorNotification"
#define kCertificateRetrievalSuccessNotification		@"kCertificateRetrievalSuccessNotification"

#define kCertificateRetrievalCertificateUserInfo		@"kCertificateRetrievalCertificateUserInfo"
#define kCertificateRetrievalEmailUserInfo				@"kCertificateRetrievalEmailUserInfo"
#define kCertificateRetrievalErrorMessageUserInfo		@"kCertificateRetrievalErrorMessageUserInfo"

@interface FXMCertificateManager : NSObject <ASResolveRecipientsDelegate>

+ (void) downloadCertificate:(NSString *)inEmailAddress;
+ (bool) isCertificateDownloading:(NSString *)inEmailAddress;

@end
