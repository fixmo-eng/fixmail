//
//  FXLMIMEMessage.h
//  FixMail
//
//  Created by Colin Biggin on 2013-05-31.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//
#import "ASComposeDelegate.h"

#define kSendEmailStartedNotification		@"kSendEmailStartedNotification"
#define kSendEmailErrorNotification			@"kSendEmailErrorNotification"
#define kSendEmailSuccessNotification		@"kSendEmailSuccessNotification"

#define kSendEmailErrorMessageUserInfo		@"kSendEmailErrorMessageUserInfo"

typedef enum {
	errorNone = 0,
	errorMissingFromAddress,
	errorMissingToAddress,
	errorCouldNotEncrypt,
	errorCouldNotSign
} EFXLMIMEError;

@class ASEmail;
@class EmailAddress;

@interface FXLMIMEMessage : NSObject <ASComposeDelegate>

- (void) addToAddress:(EmailAddress *)inAddress;
- (void) addToAddresses:(NSArray *)inAddresses;
- (void) addCCAddress:(EmailAddress *)inAddress;
- (void) addCCAddresses:(NSArray *)inAddresses;
- (void) addBCCAddress:(EmailAddress *)inAddress;
- (void) addBCCAddresses:(NSArray *)inAddresses;
- (void) addFromAddress:(EmailAddress *)inAddress;

- (void) addSubject:(NSString *)inSubject;
- (void) addDate:(NSString *)inDateString;
- (void) addPlainTextPart:(NSString *)inText;
- (void) addHTMLTextPart:(NSString *)inText;

- (void) addAttachmentPath:(NSString *)inFilePath  name:(NSString *)inName;

- (NSString *) mimeString:(EFXLMIMEError *)outError;
- (void) queueMessage:(ASEmail *)inDraftEmail;

+ (FXLMIMEMessage *) newMessage;

@property (nonatomic, assign) bool signMessage;
@property (nonatomic, assign) bool encryptMessage;

@end
