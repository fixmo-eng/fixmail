//
//  SZCAddressView.m
//  MailViewer
//
//  Created by Anluan O'Brien on 2012-11-15.
//  Copyright (c) 2012 Anluan O'Brien. All rights reserved.
//

#import "SZCFlowLayoutView.h"

@implementation SZCFlowLayoutView

-(void)awakeFromNib
{
    self.hPadding = 6;
    self.vPadding = 6;
    self.hSpacing = 3;
    self.vSpacing = 3;    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.hPadding = 6;
        self.vPadding = 6;
        self.hSpacing = 3;
        self.vSpacing = 3;
    }
    return self;
}

-(void)layoutSubviews
{
    [self calculateSizeToContainViews:nil andLayout:YES];
}

-(CGSize)calculateSizeToContainViews:views andLayout:(BOOL)layout
{
    
    if (!views) {
        views = self.subviews;
    }
        
    CGFloat x = _hPadding;
    CGFloat y = _vPadding;
    CGFloat maxX = 0.0f;
    CGFloat rowHeight = 0.0f;
    CGFloat maxWidth = self.frame.size.width - _hPadding*2;
    for (UIView* subview in views) {
        if (!subview.hidden) {
            if (x > _hPadding && x + subview.frame.size.width > maxWidth) {
                x = _hPadding;
                y += rowHeight + _vSpacing;
                rowHeight = 0;
            }
            if (layout) {
                subview.frame = CGRectMake(x, y, subview.frame.size.width, subview.frame.size.height);
            }
            x += subview.frame.size.width + _hSpacing;
            if (x > maxX) {
                maxX = x;
            }
            if (subview.frame.size.height > rowHeight) {
                rowHeight = subview.frame.size.height;
            }
        }
    }
    
    return CGSizeMake(maxX+_hPadding, y+rowHeight+_vPadding);
}

-(void)sizeToFit
{
    CGRect currentFrame = self.frame;
    CGSize newSize = [self calculateSizeToContainViews:nil andLayout:YES];
    currentFrame.size.height = newSize.height;
    self.frame = currentFrame;
}

@end
