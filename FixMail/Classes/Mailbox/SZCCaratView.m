//
//  SZCCaratView.m
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//
#import "SZCCaratView.h"

static const NSTimeInterval kInitialBlinkDelay = 0.5;
static const NSTimeInterval kBlinkRate = 0.7;

@interface SZCCaratView ()
@property (strong) NSTimer *blinkTimer;
@end

@implementation SZCCaratView

- (id) initWithFrame:(CGRect)inFrame
{
    if ((self = [super initWithFrame:inFrame])) {
        self.backgroundColor = [UIColor colorWithRed:(70.0/255.0) green:(102.0/255.0) blue:(238.0/255.0) alpha:1.0];
    }    
    return self;
}

- (void) doBlink
{
    self.hidden = !self.hidden;
}

- (void) didMoveToSuperview
{
    self.hidden = NO;
    
    if (self.superview) {
        _blinkTimer = [NSTimer scheduledTimerWithTimeInterval:kBlinkRate target:self selector:@selector(doBlink) userInfo:nil repeats:YES];
        [self delayBlink];
    } else {
        [_blinkTimer invalidate];
    }
}

- (void) dealloc
{
    [_blinkTimer invalidate];
 }

- (void) delayBlink
{
    self.hidden = NO;
    
    [_blinkTimer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kInitialBlinkDelay]];
}

@end

