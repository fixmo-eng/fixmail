//
//  FXMContactTableViewController.m
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-06-19.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMContactTableViewController.h"
#import "ASContact.h"
#import "FXDatabase.h"
#import "FXMRecipientSearchCell.h"
#import "ASAccount.h"
#import "ASGALSearch.h"
#import "Reachability.h"


@interface FXMContactTableViewController ()
@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic) BOOL networkReachable;
@end

@implementation FXMContactTableViewController
@synthesize searchString = _searchString;
@synthesize contacts = _contacts;

+(FXMContactTableViewController *)controller
{
	return [[FXMContactTableViewController alloc] initWithStyle:UITableViewStylePlain];
}

#pragma mark - Setup

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
		[self.tableView registerClass:NSClassFromString(@"contactSearchCell") forCellReuseIdentifier:@"contactSearchCell"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self startReachability];
	[self setupNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[self stopReachability];
}

-(NSMutableArray *)contacts
{
	if (!_contacts) {
		_contacts = [NSMutableArray array];
	}
	return _contacts;
}


-(void)setSearchString:(NSString *)searchString
{
	//FIXME: be smart about this: if appending string, search in what we already have  - else, recreate array from database

	_searchString = searchString;

	if (self.networkReachable) {
		ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
		[ASGALSearch sendSearch:currentAccount query:searchString];
	}
	[self reloadTableView];
}

-(void)setupNotifications
{
	// GAL queries
	// -------------------------------------------------------------------------------
	[[NSNotificationCenter defaultCenter] addObserverForName:kGALSearchStartedNotification object:nil queue:nil
												  usingBlock:^(NSNotification *inNotification) {
													  //[self.activityIndicator startAnimating];
												  }
	 ];
	[[NSNotificationCenter defaultCenter] addObserverForName:kGALSearchFinishedResultsNotification object:nil queue:nil
												  usingBlock:^(NSNotification *inNotification) {
													  //[self.activityIndicator stopAnimating];
													  [self reloadTableView];
												  }
	 ];
	[[NSNotificationCenter defaultCenter] addObserverForName:kGALSearchFinishedNoResultsNotification object:nil queue:nil
												  usingBlock:^(NSNotification *inNotification) {
													 // [self.activityIndicator stopAnimating];
													  [self reloadTableView];
												  }
	 ];
	[[NSNotificationCenter defaultCenter] addObserverForName:kGALSearchFinishedErrorNotification object:nil queue:nil
												  usingBlock:^(NSNotification *inNotification) {
													  //[self.activityIndicator stopAnimating];
													  [self reloadTableView];
												  }
	 ];
}

#pragma mark - Private Methods

-(ASContact *)contactAtIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger index = [indexPath indexAtPosition:1];
	if (index >= [self.contacts count]) {
		return nil;
	}
	return [self.contacts objectAtIndex:[indexPath indexAtPosition:1]];
}

- (void) dismissKeyboard
{
	[self.view endEditing:YES];
}

-(void)reloadTableView
{
	[self.contacts removeAllObjects];
	[self.contacts addObjectsFromArray:[FXDatabase contactListForName:self.searchString]];
	[self.tableView reloadData];
}

#pragma mark - TableViewDataSource delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contacts count];
}

- (CGFloat) tableView:(UITableView *)inTableView heightForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"FXMRecipientSearchCell";
    FXMRecipientSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
	if (!cell) {
		cell = [[FXMRecipientSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	ASContact *contact = [self contactAtIndexPath:indexPath];
	cell.contact = contact;
    return cell;
}

#pragma mark - TableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ASContact *theContact = [self contactAtIndexPath:indexPath];
	NSAssert(theContact != nil, @"theContact != nil");
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kSZCContactListSelectedNotification object:theContact];
}

#pragma mark - Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
			self.networkReachable = YES;
            break;
        case NotReachable:
			self.networkReachable = NO;
            break;
    }
}

- (void)startReachability
{
    ASAccount *currentAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
    Reachability* aReachability = currentAccount.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

@end
