//
//  SZCAddressView.m
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//
#import "SZCAddressView.h"
#import "SZCAddressButton.h"
#import "SZCCaratView.h"
#import "UIView-ViewFrameGeometry.h"
#import "EmailAddress.h"
#import "FXMCertificateManager.h"

#define kSCZAddressViewFontSize			17.0
#define kSCZAddressViewTopMargin		0.0
#define kSCZAddressViewLeftMargin		0.0
#define kSCZAddressViewRightMargin		5.0
#define kSCZAddressViewBottomMargin		0.0

#define kSCZAddressViewLineHeight		31.0
#define kSCZAddressViewBubbleHeight		24.0
#define kSCZAddressViewBubbleSeparation	5.0

#define kMailCaratHeight				21.0
#define kMinimumButtonIndex				100

#define kNoButtonSelected				-1

#pragma mark -

@interface SZCAddressView ()
@property (nonatomic, strong) NSMutableArray *emailButtons;
@property (nonatomic, strong) SZCCaratView *textCarat;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, assign) int selectedButtonIndex;
@property (nonatomic, assign) bool editingButtons;
@property (nonatomic, assign) bool redoCertificateCheck;
@property (nonatomic, assign) float rowHeightCalculated;
@property (nonatomic, strong) NSTimer *typingTimer;

@property (nonatomic, assign) bool layoutButtons;
@property (nonatomic, assign) bool layoutTextEntry;

@end

@implementation SZCAddressView

@synthesize allAddressesHaveCertificates = _allAddressesHaveCertificates;
@synthesize typingTimer = _typingTimer;

- (void) initDefaults
{
	_title = @"";
	_editable = YES;
	_signable = NO;
	_encryptable = NO;
	_allAddressesHaveCertificates = NO;
	_redoCertificateCheck = NO;

	_emailButtons = [NSMutableArray new];
	_emailAddresses = [NSMutableArray new];
	_textEntered = [NSMutableString new];
	_selectedButtonIndex = kNoButtonSelected;
	_editingButtons = false;

	_textCarat = [[SZCCaratView alloc] initWithFrame:CGRectZero];

	_titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	_titleLabel.backgroundColor = [UIColor clearColor];
	_titleLabel.textColor = [UIColor darkGrayColor];
	_titleLabel.numberOfLines = 1;
	_titleLabel.font = [UIFont systemFontOfSize:kSCZAddressViewFontSize];
	_titleLabel.textAlignment = NSTextAlignmentLeft;

	_textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	_textLabel.backgroundColor = [UIColor clearColor];
	_textLabel.textColor = [UIColor blackColor];
	_textLabel.numberOfLines = 1;
	_textLabel.font = [UIFont systemFontOfSize:kSCZAddressViewFontSize];
	_textLabel.textAlignment = NSTextAlignmentLeft;

	return;
}

- (id) initWithFrame:(CGRect)inFrame
{
    self = [super initWithFrame:inFrame];
    if (self) {
		[self initDefaults];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)inDecoder
{
	self = [super initWithCoder:inDecoder];
	if (self) {
		[self initDefaults];
	}

	return self;
}

#pragma mark - UIResponder methods

- (BOOL) canBecomeFirstResponder
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): canBecomeFirstResponder", self.title);
	return _editable;
}

- (BOOL) becomeFirstResponder
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): becomeFirstResponder", self.title);
	[super becomeFirstResponder];

	if (!_editable) return NO;

	self.selectedButtonIndex = kNoButtonSelected;
	self.editingButtons = false;
	if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewBecameFirstResponder:)]){
		[_delegate addressViewBecameFirstResponder:self];
	}
	[self setNeedsLayout];

	return YES;
}

- (BOOL) resignFirstResponder
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): resignFirstResponder", self.title);

    [super resignFirstResponder];

	if (!_editable) return YES;

	// add any remaining text as an email address because
	// we're assuming they click to another field after typing in a valid address
	// - if not a valid email address, we ignore it
	if ([EmailAddress isProperRFCEmailAddress:self.textEntered]) {
		[self addEmailAddress:[self.textEntered copy]];
	}

	// FIXME: do we really need to do this?
	[self.textEntered setString:@""];
	
	[self.textCarat removeFromSuperview];
	[self setAllButtonsSelectTo:false];
	self.selectedButtonIndex = kNoButtonSelected;
	self.editingButtons = false;

	return YES;
}

#pragma mark - Delegation

- (void) setDelegate:(id <SZCAddressViewDelegate>)inDelegate
{
	if (inDelegate == _delegate) return;

	if ([(NSObject *)inDelegate conformsToProtocol:@protocol(SZCAddressViewDelegate)]) {
		_delegate = inDelegate;
	}
}

#pragma mark - Layout

- (void) layoutSubviews
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): layoutSubviews", self.title);

	[super layoutSubviews];

	[self doLayoutSubviews];

}

- (void) doLayoutSubviews
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): doLayoutSubviews", self.title);

	[super layoutSubviews];


	// Our layout will go as follows:
	//
	// To:	<email1> <email2>
	// <email3> <email4>
	// [TextField Entry]
	//

	CGFloat maxWidth = self.width;

	// get rid of any existing subviews
	[self deleteAllButtons];

	CGFloat currentX = kSCZAddressViewLeftMargin;
	CGFloat currentY = kSCZAddressViewTopMargin;
	if ((self.title.length > 0) && (self.titleLabel.superview == nil)) {
		self.titleLabel.text = self.title;
		[self.titleLabel sizeToFit];
		self.titleLabel.origin = CGPointMake(currentX, currentY+5.0);
		if (self.titleLabel.width > maxWidth) self.titleLabel.width = maxWidth-10;
		[self addSubview:self.titleLabel];
	}
	currentX = self.titleLabel.right + kSCZAddressViewBubbleSeparation;

	// now go through and create all the addresses
	int theButtonIndex = kMinimumButtonIndex;
	for (EmailAddress *theEmailAddress in self.emailAddresses) {

		SZCAddressButton *theButton = [[SZCAddressButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 0.0, kSCZAddressViewBubbleHeight)];
		theButton.titleLabel.font = [UIFont systemFontOfSize:15];
		if (theEmailAddress.name.length > 0) {
			[theButton setTitle:theEmailAddress.name forState:UIControlStateNormal];
		} else {
			[theButton setTitle:theEmailAddress.address forState:UIControlStateNormal];
		}
		[theButton addTarget:self action:@selector(toggleSelectButton:) forControlEvents:UIControlEventTouchUpInside];

		[self redoButtonCertCheck:theButton address:theEmailAddress];
		[theButton sizeToFit];

		theButton.tag = theButtonIndex; // **** IMPORTANT: tag corresponds to index in array MINUS kMinimumButtonIndex ****

		float maxWidth = self.width - kSCZAddressViewRightMargin - currentX;
	
		if (theButtonIndex == kMinimumButtonIndex) { // ie, the VERY first one
			if (theButton.width > maxWidth) theButton.width = maxWidth;
		} else if (theButton.width > maxWidth) {
			if (theButtonIndex != kMinimumButtonIndex) {
				currentX = kSCZAddressViewLeftMargin;
				currentY += kSCZAddressViewLineHeight;
				maxWidth = self.width - kSCZAddressViewRightMargin - currentX;
			}
			if (theButton.width > maxWidth) theButton.width = maxWidth;
		}

		// check if we need to bump to the next line
		if ((currentX + theButton.width) > (self.width - kSCZAddressViewRightMargin)) {
			currentX = kSCZAddressViewLeftMargin;
			currentY += kSCZAddressViewLineHeight;
		}
		
		theButton.origin = CGPointMake(currentX, currentY + (kSCZAddressViewLineHeight-kSCZAddressViewBubbleHeight)/2.0);
		[self addSubview:theButton];
		[_emailButtons addObject:theButton];
		
		currentX += theButton.width + kSCZAddressViewBubbleSeparation;

		if ((self.editingButtons) && (self.selectedButtonIndex == (theButtonIndex-kMinimumButtonIndex))) {
			theButton.selected = true;
		}

		theButtonIndex++;
	}

	if (self.isFirstResponder && !self.editingButtons) {

		// if no addresses, then put the carat EXACTLY where it is,
		// otherwise, goes on the next line
		
		if (theButtonIndex > kMinimumButtonIndex) {
			if (self.emailAddresses.count > 0) {
				currentX = ((SZCAddressButton *)[self.emailButtons lastObject]).right + kSCZAddressViewBubbleSeparation;
			}
		}

		self.textLabel.text = self.textEntered;
		[self.textLabel sizeToFit];
		self.textLabel.origin = CGPointMake(currentX, currentY+5.0);
		if (self.textLabel.width > maxWidth) self.textLabel.width = maxWidth-10;

		self.textCarat.frame = CGRectMake(self.textLabel.right + 1.0, currentY + 4.0, 3.0, kMailCaratHeight);
		if (self.textLabel.superview == nil) [self addSubview:self.textLabel];
		[self bringSubviewToFront:self.textLabel];
		if (self.textCarat.superview == nil) [self addSubview:self.textCarat];
		[self bringSubviewToFront:self.textCarat];
	} else {
		[self.textLabel removeFromSuperview];
		[self.textCarat removeFromSuperview];
	}
	
	self.rowHeightCalculated = currentY + kSCZAddressViewLineHeight;

	return;
}

#pragma mark - UITouch

- (void) touchesBegan:(NSSet *)inTouches withEvent:(UIEvent *)inEvent
{
	[super touchesBegan:inTouches withEvent:inEvent];
	[self becomeFirstResponder];
	[self setNeedsLayout];
}

#pragma mark - UIKeyInput methods

- (BOOL) hasText
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): hasText", self.title);

	if (self.textEntered.length > 0) return TRUE;
	if (self.emailAddresses.count > 0) return TRUE;
	return false;
}

- (void) insertText:(NSString *)inText
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): insertText <%@> <%@>", self.title, inText, self.textEntered);

	if ([self isEmailTerminationCharacter:inText]) {
		if (self.textEntered.length > 0) {
			[self addEmailAddress:[self.textEntered copy]];
		}
	} else {
		self.editingButtons = false;
		[self.textEntered insertString:inText atIndex:self.textEntered.length];
		
		//FIXME: we are re-creating the AddressButtons every time a character is entered!!
		[self setNeedsLayout];
	}
	if ((_delegate) && [_delegate respondsToSelector:@selector(addressViewTextChanged:)]){
		[_delegate addressViewTextChanged:self];
	}

	return;
}

- (void) deleteBackward
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): deleteBackward", self.title);

	if (self.textEntered.length > 0) {
		self.editingButtons = false;
		[self.textEntered deleteCharactersInRange:NSMakeRange(self.textEntered.length-1, 1)];
		if ((_delegate) && [_delegate respondsToSelector:@selector(addressViewTextChanged:)]){
			[_delegate addressViewTextChanged:self];
		}
	} else {
		// if already editing buttons, then button is highlighted
		// and we delete it
		if (self.editingButtons) {
			[self deleteSelectedButtons];
			self.selectedButtonIndex--;
			if (self.selectedButtonIndex >= 0) {
				[self selectButtonWithTag:(kMinimumButtonIndex + self.selectedButtonIndex)];
			} else {
				self.editingButtons = false;
			}
		} else {
			self.selectedButtonIndex = self.emailButtons.count - 1;
			[self selectButtonWithTag:(kMinimumButtonIndex + self.selectedButtonIndex)];
			self.editingButtons = true;
		}
	}
	[self setNeedsLayout];

	return;
}

#pragma mark - UITextInputTraits Protocol

- (UITextAutocorrectionType) autocorrectionType
{
	return UITextAutocorrectionTypeNo;
}

- (UITextSpellCheckingType) spellCheckingType
{
	return UITextSpellCheckingTypeNo;
}

- (UITextAutocapitalizationType) autocapitalizationType
{
	return UITextAutocapitalizationTypeNone;
}

- (UIKeyboardType) keyboardType
{
	return UIKeyboardTypeEmailAddress;
}

#pragma mark - Getters/Setters

- (void) setEmailAddresses:(NSMutableArray *)inArray
{
	[_emailAddresses removeAllObjects];
	[self internalAddEmailAddresses:inArray];
	[self setNeedsLayout];
}

- (void) addEmailAddress:(NSString *)inAddress
{
	if (inAddress.length == 0) return;

	EmailAddress *newAddress = [[EmailAddress alloc] initWithString:inAddress];
	[_emailAddresses addObject:newAddress];
	self.textEntered.string = @"";

	if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewEmailAddressAdded:email:)]){
		[_delegate addressViewEmailAddressAdded:self email:newAddress];
	}

	if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewLayoutChanged:)]){
		[_delegate addressViewLayoutChanged:self];
	}

	[self setNeedsLayout];
}

- (void) appendEmailAddresses:(NSArray *)inAddresses
{
	if (inAddresses.count == 0) return;

	self.textEntered.string = @"";
	[self internalAddEmailAddresses:inAddresses];
	[self doLayoutSubviews];
}

- (void) replaceEmailAddresses:(NSArray *)inArray
{
	[_emailAddresses removeAllObjects];
	[self internalAddEmailAddresses:inArray];
	[self doLayoutSubviews];
}

- (CGFloat) rowHeight
{
	CGFloat theHeight = 0.0;
	if ((self.emailAddresses.count == 0) && (self.title.length > 0)) { // special case
		theHeight = kSCZAddressViewTopMargin + kSCZAddressViewLineHeight + kSCZAddressViewBottomMargin;
	} else {
		theHeight = kSCZAddressViewTopMargin + self.rowHeightCalculated + kSCZAddressViewBottomMargin;
//		if (self.isFirstResponder && !self.editingButtons) theHeight += kSCZAddressViewLineHeight;
	}

	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): rowHeight: %.0f", self.title, theHeight);
	return theHeight;
}

- (void) setSignable:(BOOL)inValue
{
	if (_signable == inValue) return;
	_signable = inValue;
	_redoCertificateCheck = _signable;

	[self setNeedsLayout];
}

- (BOOL) allAddressesHaveCertificates
{
	if (!_signable) return NO;

	if (_redoCertificateCheck) {
		_allAddressesHaveCertificates = YES;
		for (EmailAddress *theAddress in self.emailAddresses) {
			if (theAddress.hasCertificate) continue;
			_allAddressesHaveCertificates = NO;
			break;
		}
	}

	return _allAddressesHaveCertificates;
}

-(void)resetTimer
{
	if (self.typingTimer) {
		[self.typingTimer invalidate];
	}
	self.typingTimer = [NSTimer scheduledTimerWithTimeInterval:kTypingPauseInterval target:self selector:@selector(typingDidPause:) userInfo:nil repeats:NO]; // user info can be nil if necessary
}

-(void)removeTimer
{
	if (self.typingTimer) {
		[self.typingTimer invalidate];
	}
}

- (void) checkCertificateState
{
	for (SZCAddressButton *theButton in self.emailButtons) {
		int theIndex = theButton.tag - kMinimumButtonIndex;
		EmailAddress *theEmailAddress = [self.emailAddresses objectAtIndex:theIndex];
		if ([FXMCertificateManager isCertificateDownloading:theEmailAddress.address]) {
			[theButton startAnimation];
		} else {
			[theButton stopAnimation];
			[self redoButtonCertCheck:theButton address:theEmailAddress];
		}
	}

	return;
}

#pragma mark - IBActions

- (IBAction) toggleSelectButton:(id)inSender
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): toggleSelectButton", self.title);

	if (![inSender isKindOfClass:[SZCAddressButton class]]) return;
	SZCAddressButton *theButton = (SZCAddressButton *)inSender;

	// always send message to delegate that it has been selected
	int theIndex = theButton.tag - kMinimumButtonIndex;
	EmailAddress *theEmailAddress = [self.emailAddresses objectAtIndex:theIndex];

	// only send this *IF* first selected
	if (theButton.selected) {
		if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewEmailAddressDoubleClicked:email:)]){
			[_delegate addressViewEmailAddressDoubleClicked:self email:theEmailAddress];
		}
	} else {
		if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewEmailAddressClicked:email:)]){
			[_delegate addressViewEmailAddressClicked:self email:theEmailAddress];
		}
	}

	if (!self.editable) {
		theButton.selected = false;
		return;
	}

	if (!self.isFirstResponder) [super becomeFirstResponder]; // **** call super, not self

	[self selectButtonWithTag:theButton.tag];
	self.selectedButtonIndex = [self.emailButtons indexOfObject:theButton];
	if (!self.editingButtons) {
		self.editingButtons = true;
		[self setNeedsLayout];
	}

	return;
}

#pragma mark - Private methods

- (void) deleteAllButtons
{
	if (self.emailButtons.count > 0) {
		[self.emailButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
		[self.emailButtons removeAllObjects];
	}

	return;
}

- (void) setAllButtonsSelectTo:(bool)inSelected
{
	for (SZCAddressButton *theButton in self.emailButtons) {
		theButton.selected = inSelected;
	}

	return;
}

- (void) selectButtonWithTag:(int)inTag
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): selectButtonWithTag (%d)", self.title, inTag);

	for (SZCAddressButton *theButton in self.emailButtons) {
		theButton.selected = (theButton.tag == inTag);
		FXDebugLog(kFXLogSZCAddressView, @"    theButton.tag (%d) %@", theButton.tag, theButton.selected ? @"selected" : @"unselected");
	}

	return;
}

- (void) deleteSelectedButtons
{
	FXDebugLog(kFXLogSZCAddressView, @"SZCAddressView (%@): deleteSelectedButtons", self.title);

	// have to do a couple of things:
	// 1) remove button from the superview
	// 2) remove the email address from self.emailAddresses where the index is the button's tag
	// 3) add it to the deleteArray to be deleted AFTER traversing the array
	NSMutableArray *deleteArray = [NSMutableArray new];
	for (SZCAddressButton *theButton in self.emailButtons) {
		if (!theButton.selected) continue;
		FXDebugLog(kFXLogSZCAddressView, @"    theButton.tag (%d)", theButton.tag);
		[theButton removeFromSuperview]; // 1
		[self.emailAddresses removeObjectAtIndex:(theButton.tag-kMinimumButtonIndex)]; // 2
		[deleteArray addObject:theButton]; // 3
	}
	if (deleteArray.count > 0) {
		[self.emailButtons removeObjectsInArray:deleteArray];
		if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewLayoutChanged:)]){
			[_delegate addressViewLayoutChanged:self];
		}
	}

	return;
}

- (bool) isEmailTerminationCharacter:(NSString *)inText
{
	if ([inText isEqualToString:@"\n"]) return true;
//	if ([inText isEqualToString:@" "]) return true; // don't allow this for searching
	if ([inText isEqualToString:@","]) return true;
	return false;
}

- (void) internalAddEmailAddresses:(NSArray *)inArray
{
	if (inArray.count == 0) return;

	// must ABSOLUTELY make sure that all our addresses are EmailAddresses
	// and if they are strings, then add them as EmailAddresses
	for (id theAddress in inArray) {
		if ([theAddress isKindOfClass:[EmailAddress class]]) {
			[self.emailAddresses addObject:theAddress];
			if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewEmailAddressAdded:email:)]){
				[_delegate addressViewEmailAddressAdded:self email:theAddress];
			}
		} else if ([theAddress isKindOfClass:[NSString class]]) {
			EmailAddress *newAddress = [[EmailAddress alloc] initWithString:theAddress];
			[self.emailAddresses addObject:newAddress];
			if ((_delegate != nil) && [_delegate respondsToSelector:@selector(addressViewEmailAddressAdded:email:)]){
				[_delegate addressViewEmailAddressAdded:self email:newAddress];
			}
		}
	}

	return;
}

-(void) typingDidPause:(NSTimer *)timer
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(addressViewTextEditingDidPause:)]) {
		[self.delegate addressViewTextEditingDidPause:self];
	}
}

- (void) redoButtonCertCheck:(SZCAddressButton *)inButton address:(EmailAddress *)inEmailAddress
{
	NSParameterAssert(inButton != nil);
	NSParameterAssert(inEmailAddress != nil);

	inButton.viewingMode = viewingViewing;
	if (self.signable & self.encryptable) {
		inButton.viewingMode = viewingSignedEncrypted;
	} else if (self.signable) {
		inButton.viewingMode = viewingSignedOnly;
	}
	
	// check if a valid email address as well
	if (inEmailAddress.isValidEmailAddress) {
		[inButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	} else {
		[inButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
	}
	if (self.signable) {
		inButton.isSigned = true;
		inButton.isEncrypted = inEmailAddress.hasCertificate;
	} else {
		inButton.isSigned = false;
		inButton.isEncrypted = false;
	}

	return;
}

@end

