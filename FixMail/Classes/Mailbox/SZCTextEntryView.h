//
//  SZCTextEntryView.h
//  FixMail
//
//  Created by Colin Biggin on 2013-02-19.
//
//

//#define kSZCTextEntryBecomeFirstResponderNotification		@"kSZCTextEntryBecomeFirstResponderNotification"
//#define kSZCTextEntryClickedNotification					@"kSZCTextEntryClickedNotification"

@class SZCTextEntryView;

@protocol SZCTextEntryViewDelegate <NSObject>

- (void) textEntryViewBecameFirstResponder:(SZCTextEntryView *)inView;
- (void) textEntryViewClicked:(SZCTextEntryView *)inView;
- (void) textEntryViewTextChanged:(SZCTextEntryView *)inView;

@end

@interface SZCTextEntryView : UIView <UIKeyInput>

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableString *textEntered;
@property (nonatomic, assign) bool editable;
@property (nonatomic, strong) id<SZCTextEntryViewDelegate> delegate;

- (CGFloat) rowHeight;

@end
