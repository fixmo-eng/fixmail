/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *  
 *  SZCAttachmentView.h
 *  Created by Colin Biggin on 2013-02-26.
*/

@class ASAttachment;
@class ASEmail;
@class SZCAttachmentView;

typedef enum
{
	attachmentActionNone = 0,
	attachmentActionView,
	attachmentActionSave,
	attachmentActionMove,
	attachmentActionMakeDefaultCertificate,
	attachmentActionAddCertToDB

} EAttachmentAction;


@protocol SZCAttachmentViewDelegate <NSObject>
@optional
- (void) attachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath;
- (void) attachmentDownloaded:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath;
@end

#pragma mark -

@interface SZCAttachmentView : UIView <UIGestureRecognizerDelegate>

@property (nonatomic, strong) id<SZCAttachmentViewDelegate> delegate;
@property (nonatomic, strong) ASAttachment *attachment;
@property (nonatomic, strong) ASEmail *email;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, assign) EAttachmentAction attachmentAction;

- (id) initWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail delegate:(id<SZCAttachmentViewDelegate>)inDelegate;
- (id) initWithPath:(NSString *)inFilePath delegate:(id<SZCAttachmentViewDelegate>)inDelegate;
- (void) downloadAttachment;

@end

