/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMEmailSecurity.m
 *
 *  Created by Colin Biggin on 2013-05-06.
 */
#import "FXMEmailSecurity.h"
#import "FXMCertificateManager.h"
#import "NSData+Base64.h"
#import "SZLSecureFileHandle.h"

NSString * const kUserCertificateIdentifier = @"userCertificate";

@implementation FXMEmailSecurity

//
// We will do a very basic cert saving:
// - create 'Certificates' sub-directory in Documents
// - save any certs as the email address with '.cert' extension and some char substitution
//
+ (NSString *) certPathForEmail:(NSString *)inEmail
{
	NSString *theFileName = [inEmail stringByReplacingOccurrencesOfString:@"@" withString:@"_"];
	theFileName = [theFileName stringByReplacingOccurrencesOfString:@"." withString:@"_"];
	theFileName = [theFileName stringByAppendingPathExtension:@"cert"];

	NSString *thePath = [FXLSafeZone documentsDirectory];
	thePath = [thePath stringByAppendingPathComponent:@"Certificates"];
	thePath = [thePath stringByAppendingPathComponent:theFileName];

	return thePath;
}

+ (void) createCertDirectory
{
	NSString *thePath = [FXLSafeZone documentsDirectory];
	thePath = [thePath stringByAppendingPathComponent:@"Certificates"];

	BOOL isDirectory;
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:thePath isDirectory:&isDirectory]) return;
	[[FXLSafeZone secureFileManager] createDirectoryAtPath:thePath withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (void)signMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase completion:(void(^)(NSString *newMimeContent))completion
{
	completion(mime);
}

+ (NSString*)encryptMessage:(NSString*)mime withRecipients:(NSDictionary*)recipients
{
	return @"";
}

+ (void) decryptAndValidateMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase completion:(void (^)(NSString *newMimeContent, BOOL isSignatureValid, BOOL didDecryptionSucceed, NSString *signedCertificate))completion
{
	completion(mime, TRUE, TRUE, @"SIGNED CERTIFICATE");
}

+ (BOOL) hasCertificateForEmail:(NSString *)emailAddress
{
	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) return YES;

	// check if container certificate
	if ([emailAddress caseInsensitiveCompare:kUserCertificateIdentifier] == NSOrderedSame) return NO;

	return NO;
}

+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withPublic:(NSString *)pemRepresentation
{
	const char *utfString = [pemRepresentation UTF8String];
	NSData *theData = [NSData dataWithBytes:utfString length:[pemRepresentation lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
	return [FXMEmailSecurity saveCertificateForEmail:emailAddress withP12Data:theData withPassphrase:nil];
}

+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withP12Data:(NSData*)data withPassphrase:(NSString*)passphrase
{
	if (emailAddress.length == 0) return NO;
	if (data.length == 0) return NO;

	[self createCertDirectory];

	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) {
		[[FXLSafeZone secureFileManager] removeItemAtPath:thePath error:nil];
	}

	return [[FXLSafeZone secureFileManager] createFileAtPath:thePath contents:data attributes:nil];
}

+ (void) deleteCertificateForEmail:(NSString *)emailAddress
{
	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) {
		[[FXLSafeZone secureFileManager] removeItemAtPath:thePath error:nil];
	}

	return;
}

+ (NSString *) pemStringForEmail:(NSString *)emailAddress
{
	if (emailAddress.length == 0) return nil;

	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
    if (![[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) return nil;

	NSData *fileData = [[FXLSafeZone secureFileManager] contentsAtPath:thePath];
	NSString *base64PEM = [fileData base64EncodedStringWithLinebreaks:YES];
	return [NSString stringWithFormat:@"-----BEGIN CERTIFICATE-----\n%@\n-----END CERTIFICATE-----",base64PEM];
}

+ (NSString *) valueForAttribute:(NSString*)attribute email:(NSString *)emailAddress
{
	if (emailAddress.length == 0) return nil;

	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
    if (![[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) return nil;

	return @"Not Implemented Yet";
}

+ (NSArray *) getAllAttributes:(NSString *)emailAddress
{
	if (emailAddress.length == 0) return nil;

	NSString *thePath = [FXMEmailSecurity certPathForEmail:emailAddress];
    if (![[FXLSafeZone secureFileManager] fileExistsAtPath:thePath]) return nil;

	return @[@"Not Implemented Yet"];
}

+ (NSArray *) getAllAttributesForP12Data:(NSData *)inData passPhrase:(NSString *)inPassPhrase
{
	// just return a bunch of nonsense
	return @[@"Attribute 1", @"Attribute 2", @"Attribute 3"];
}

+ (NSString *) emailAttributeForPEMString:(NSString *)inPEMString
{
	return @"whatever@whatever.com";
}

+ (BOOL) hasUserCertificate
{
    return [FXMEmailSecurity hasCertificateForEmail:kUserCertificateIdentifier];
}

+ (BOOL) saveUserCertificateForEmail:(NSString *)inEmail withP12Data:(NSData *)inData withPassphrase:(NSString*)inPassphrase
{
	if (inEmail.length > 0) {
		[FXMEmailSecurity saveCertificateForEmail:inEmail withP12Data:inData withPassphrase:inPassphrase];
	}
	return [FXMEmailSecurity saveCertificateForEmail:kUserCertificateIdentifier withP12Data:inData withPassphrase:inPassphrase];
}

+ (void) deleteUserCertificate
{
	[FXMEmailSecurity deleteCertificateForEmail:kUserCertificateIdentifier];
}

+ (NSString *) valueForUserAttribute:(NSString*)attribute
{
	return [FXMEmailSecurity valueForAttribute:attribute email:kUserCertificateIdentifier];
}

+ (NSArray *) getAllUserAttributes
{
	return [FXMEmailSecurity getAllAttributes:kUserCertificateIdentifier];
}

+ (void) dumpSettingsDB
{
	// don't do anything
	return;
}

@end

@implementation SZLSecureFileHandle

+ (id) fileHandleForReadingAtPath:(NSString *)path
{
	return [NSFileHandle fileHandleForReadingAtPath:path];
}

+ (id) fileHandleForReadingFromURL:(NSURL *)url error:(NSError *__autoreleasing *)error
{
	return [NSFileHandle fileHandleForReadingFromURL:url error:error];
}

+ (id) fileHandleForUpdatingAtPath:(NSString *)path
{
	return [NSFileHandle fileHandleForUpdatingAtPath:path];
}

+ (id) fileHandleForUpdatingURL:(NSURL *)url error:(NSError *__autoreleasing *)error
{
	return [NSFileHandle fileHandleForUpdatingURL:url error:error];
}

+ (id) fileHandleForWritingAtPath:(NSString *)inFilePath
{
	return [NSFileHandle fileHandleForWritingAtPath:inFilePath];
}

+ (id) fileHandleForWritingToURL:(NSURL *)url error:(NSError *__autoreleasing *)error
{
	return [NSFileHandle fileHandleForReadingFromURL:url error:error];
}

- (NSFileHandle *) handle
{
    return self;
}

+ (SZLSecureFileHandle *) handleForNSFileHandle:(NSFileHandle *)fileHandle
{
	return (SZLSecureFileHandle *)fileHandle;
}

@end
