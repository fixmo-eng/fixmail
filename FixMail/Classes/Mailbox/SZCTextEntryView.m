//
//  SZCTextEntryView.m
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//
#import "SZCTextEntryView.h"
#import "SZCCaratView.h"
#import "UIView-ViewFrameGeometry.h"

#define kSCZTextEntryViewTopPadding		0.0
#define kSCZTextEntryViewLeftPadding	0.0

#define kSCZTextEntryViewFontSize		17.0
#define kSCZTextEntryViewLineHeight		31.0
#define kSCZTextEntryViewCaratHeight	21.0
#define kSCZTextEntryViewCaratWidth		3.0

@interface SZCTextEntryView ()
@property (nonatomic, strong) SZCCaratView *textCarat;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *textLabel;
@end

@implementation SZCTextEntryView

- (void) initDefaults
{
	_title = @"";
	_textEntered = [NSMutableString new];
	_textCarat = [[SZCCaratView alloc] initWithFrame:CGRectZero];

	_titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	_titleLabel.backgroundColor = [UIColor clearColor];
	_titleLabel.textColor = [UIColor darkGrayColor];
	_titleLabel.numberOfLines = 1;
	_titleLabel.font = [UIFont systemFontOfSize:kSCZTextEntryViewFontSize];
	_titleLabel.textAlignment = NSTextAlignmentLeft;

	_textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	_textLabel.backgroundColor = [UIColor clearColor];
	_textLabel.textColor = [UIColor blackColor];
	_textLabel.numberOfLines = 1;
	_textLabel.font = [UIFont systemFontOfSize:kSCZTextEntryViewFontSize];
	_textLabel.textAlignment = NSTextAlignmentLeft;
	_textLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;

	// we'll default this to true so you have
	// to explicitly turn it off
	_editable = true;

	return;
}

- (id) initWithFrame:(CGRect)inFrame
{
    self = [super initWithFrame:inFrame];
    if (self) {
		[self initDefaults];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)inDecoder
{
	self = [super initWithCoder:inDecoder];
	if (self) {
		[self initDefaults];
	}

	return self;
}

#pragma mark NSResponder methods

- (BOOL) canBecomeFirstResponder
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): canBecomeFirstResponder", self.title);

	return YES;
}

- (BOOL) becomeFirstResponder
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): becomeFirstResponder", self.title);

	[super becomeFirstResponder];

	if (_delegate != nil) {
		[_delegate textEntryViewBecameFirstResponder:self];
	}

	return YES;
}

- (BOOL) resignFirstResponder
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): resignFirstResponder", self.title);
    [super resignFirstResponder];

	[self.textCarat removeFromSuperview];
	return YES;
}

#pragma mark -

- (void) layoutSubviews
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): layoutSubviews", self.title);

	[super layoutSubviews];

	// Our layout will go as follows:
	//
	// To:	<email1> <email2>
	//		<email3> <email4>
	//		[TextField Entry]
	//

	CGFloat maxWidth = 300.0;

	CGFloat currentX = kSCZTextEntryViewLeftPadding;
	CGFloat currentY = kSCZTextEntryViewTopPadding;
	if (self.title.length > 0) {
		self.titleLabel.text = self.title;
		[self.titleLabel sizeToFit];
		self.titleLabel.origin = CGPointMake(currentX, currentY+7.0);
		if (self.titleLabel.width > maxWidth) self.titleLabel.width = maxWidth-10;
		currentX = self.titleLabel.right + 5.0;
		if (self.titleLabel.superview == nil) {
			[self addSubview:self.titleLabel];
		}
	}

	self.textLabel.text = self.textEntered;
	[self.textLabel sizeToFit];
	self.textLabel.origin = CGPointMake(currentX, currentY+7.0);
	if (self.textLabel.right > (self.width - kSCZTextEntryViewCaratWidth - 1.0)) {
		self.textLabel.width = ((self.width-kSCZTextEntryViewCaratWidth-1.0) - self.textLabel.left);
	}
	if (self.textLabel.superview == nil) [self addSubview:self.textLabel];

	if ((self.isFirstResponder) && (self.editable)) {
		self.textCarat.frame = CGRectMake(self.textLabel.right + 1.0, currentY+6.0, kSCZTextEntryViewCaratWidth, kSCZTextEntryViewLineHeight);
		if (self.textCarat.superview == nil) [self addSubview:self.textCarat];
	} else {
		[self.textCarat removeFromSuperview];
	}

	return;
}

#pragma mark - Delegation

- (void) setDelegate:(id <SZCTextEntryViewDelegate>)inDelegate
{
	if (inDelegate == _delegate) return;

	if ([(NSObject *)inDelegate conformsToProtocol:@protocol(SZCTextEntryViewDelegate)]) {
		_delegate = inDelegate;
	}
}

#pragma mark - UITouch

- (void) touchesBegan:(NSSet *)inTouches withEvent:(UIEvent *)inEvent
{
	[super touchesBegan:inTouches withEvent:inEvent];

	if (self.editable) {
		[self becomeFirstResponder];
		[self setNeedsLayout];
	} else {
		if (_delegate != nil) {
			[_delegate textEntryViewClicked:self];
		}
	}

	return;
}

#pragma mark - UIKeyInput methods

- (BOOL) hasText
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): hasText", self.title);

	return (self.textEntered.length > 0);
}

- (void) insertText:(NSString *)inText
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): insertText <%@> <%@>", self.title, inText, self.textEntered);

	[self.textEntered insertString:inText atIndex:self.textEntered.length];
	[self setNeedsLayout];

	if (_delegate != nil) {
		[_delegate textEntryViewTextChanged:self];
	}

	return;
}

- (void) deleteBackward
{
	FXDebugLog(kFXLogSZCTextEntryView, @"SZCTextEntryView (%@): deleteBackward", self.title);

	if (self.textEntered.length == 0) return;

	[self.textEntered deleteCharactersInRange:NSMakeRange(self.textEntered.length-1, 1)];
	[self setNeedsLayout];

	if (_delegate != nil) {
		[_delegate textEntryViewTextChanged:self];
	}

	return;
}

#pragma mark - Getters/Setters

- (void) setTextEntered:(NSMutableString *)inString
{
	if (inString.length == 0) return;

	_textEntered = inString;
	[self setNeedsLayout];
}

- (CGFloat) rowHeight
{
	return kSCZTextEntryViewLineHeight;
}

@end
