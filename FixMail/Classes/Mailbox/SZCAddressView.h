//
//  SZCAddressView.h
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//

@class SZCAddressView;
@class EmailAddress;

@protocol SZCAddressViewDelegate <NSObject>
@optional
- (void) addressViewBecameFirstResponder:(SZCAddressView *)inAddressView;
- (void) addressViewLayoutChanged:(SZCAddressView *)inAddressView;
- (void) addressViewTextChanged:(SZCAddressView *)inAddressView;
- (void) addressViewEmailAddressAdded:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress;
- (void) addressViewEmailAddressClicked:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress;
- (void) addressViewEmailAddressDoubleClicked:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress;
- (void) addressViewTextEditingDidPause:(SZCAddressView *)inAddressView;
@end

#pragma mark -

@interface SZCAddressView : UIView <UIKeyInput>

@property (nonatomic, strong) NSMutableString *textEntered;
@property (nonatomic, strong) NSMutableArray *emailAddresses;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) BOOL signable;
@property (nonatomic, assign) BOOL encryptable;
@property (readonly) BOOL allAddressesHaveCertificates;
@property (nonatomic, strong) id<SZCAddressViewDelegate> delegate;

- (void) appendEmailAddresses:(NSArray *)inAddresses;
- (void) replaceEmailAddresses:(NSArray *)inAddresses;
- (CGFloat) rowHeight;
- (void) resetTimer;
- (void) removeTimer;
- (void) checkCertificateState;

@end

