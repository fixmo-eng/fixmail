//
//  SZCAddressView.h
//  MailViewer
//
//  Created by Anluan O'Brien on 2012-11-15.
//  Copyright (c) 2012 Anluan O'Brien. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SZCFlowLayoutView : UIView

@property (nonatomic,readwrite) CGFloat hPadding;
@property (nonatomic,readwrite) CGFloat vPadding;
@property (nonatomic,readwrite) CGFloat hSpacing;
@property (nonatomic,readwrite) CGFloat vSpacing;

-(CGSize)calculateSizeToContainViews:views andLayout:(BOOL)layout;

@end
