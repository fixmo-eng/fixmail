/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMEmailSecurity.m
 *
 *  Created by Colin Biggin on 2013-05-06.
 */

#import "FXMEmailSecurity.h"
#import "FXMCertificateManager.h"
#import "BaseAccount.h"
#import "ASResolveRecipients.h"
#import "ASValidateCert.h"
#import "EmailAddress.h"

#import "NSData+Base64.h"
#import "SZLSecurity.h"
#import "SZLSecureDatabase+SZL.h"
#import "SZLCAC.h"
#import "SZLSMIME.h"

extern NSString * const kUserCertificateIdentifier;
NSString * const kTempCertificateIdentifier = @"tempCertificate";

@implementation FXMEmailSecurity

+ (NSString*) signMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase
{
    SZLCertificate *cert = nil;
    if (data) {
        cert = [SZLCertificate newCertificateFromPKCS12Data:data andPassphrase:passphrase withIdentifier:@"temp"];
    } else {
        cert = [SZLCertificate certificateWithIdentifier:kUserCertificateIdentifier];
    }
    
    return [SZLSMIME signEmailMessage:mime WithSZLCertificate:cert];
}

// @param mime: the mime message
// @param recipients: a dictionary with the recipients' email address as the key and the X.509 certificate in PEM format as the value
+ (NSString*) encryptMessage:(NSString*)mime withRecipients:(NSDictionary*)recipients
{
    __block NSString *encryptedMessage = nil;
    
    if (!mime) {
        FXDebugLog(kFXLogSZLSecurity, @"No message was provided to encrypt.");
        return encryptedMessage;
    }
    NSMutableArray *recipientCertificates = [NSMutableArray arrayWithCapacity:[[recipients allKeys] count]];
    for (NSString *email in recipients) {
        SZLCertificate *cert = [SZLCertificate certificateWithIdentifier:email];
        if (!cert) {
            if ([recipients valueForKey:email] != [NSNull null]) {
                cert = [SZLCertificate certificateWithPEMString:[recipients valueForKey:email] andIdentifier:email];
                [cert saveCertificate];
            }
        }
        if (cert) {
            [recipientCertificates addObject:cert];
        }
    }
    encryptedMessage = [SZLSMIME encryptEmailMessage:mime WithRecipients:recipientCertificates];
    
    return encryptedMessage;
}

+ (void) decryptAndValidateMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase completion:(void (^)(NSString *newMimeContent, BOOL isSignatureValid, BOOL didDecryptionSucceed, NSString *signedCertificate))completion
{
    SZLCertificate *cert = nil;
    if (data) {
        cert = [SZLCertificate newCertificateFromPKCS12Data:data andPassphrase:passphrase withIdentifier:@"temp"];
    } else {
        cert = [SZLCertificate certificateWithIdentifier:kUserCertificateIdentifier];
    }
    [SZLSMIME decryptAndValidateMessage:mime withSZLCertificate:cert completion:completion];
}

+ (BOOL) hasCertificateForEmail:(NSString *)emailAddress
{
    SZLCertificate *possibleCertificate = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (possibleCertificate) {
        return YES;
    }

	// check if container certificate
	if ([emailAddress caseInsensitiveCompare:kUserCertificateIdentifier] == NSOrderedSame) return NO;

    return NO;
}

+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withPublic:(NSString *)pemRepresentation
{
    NSData *binaryPEM = [NSData dataFromBase64String:pemRepresentation];
    NSString *base64PEM = [binaryPEM base64EncodedStringWithLinebreaks:YES];
    NSString *newPEM = [NSString stringWithFormat:@"-----BEGIN CERTIFICATE-----\n%@\n-----END CERTIFICATE-----",base64PEM];
    SZLCertificate *cert;
    cert = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (!cert) {
        cert = [SZLCertificate certificateWithPEMString:newPEM andIdentifier:emailAddress];
    }
    
    if (!cert) {
		FXDebugLog(kFXLogSZLSecurity, @"There was a problem saving the certificate");
		return NO;
    }

	return YES;
}

+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withP12Data:(NSData*)data withPassphrase:(NSString*)passphrase
{
    SZLCertificate *cert = [SZLCertificate newCertificateFromPKCS12Data:data andPassphrase:passphrase withIdentifier:kUserCertificateIdentifier];
    if (cert) {
        [cert saveCertificate];
        cert = nil;
        cert = [SZLCertificate newCertificateFromPKCS12Data:data andPassphrase:passphrase withIdentifier:emailAddress];
        [cert saveCertificate];
    } else {
		FXDebugLog(kFXLogSZLSecurity, @"There was a problem saving the certificate");
		return NO;
    }

	return YES;
}

+ (void) deleteCertificateForEmail:(NSString *)emailAddress;
{
    SZLCertificate *possibleCertificate = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (possibleCertificate) {
		[possibleCertificate deleteCertificate];
	}

	return;
}

+ (NSString *) pemStringForEmail:(NSString *)emailAddress
{
	if (emailAddress.length == 0) return nil;

    SZLCertificate *possibleCertificate = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (!possibleCertificate) return nil;
	return [possibleCertificate PEMRepresentation];
}

+ (NSString *) valueForAttribute:(NSString*)attribute email:(NSString *)emailAddress
{
	if (attribute.length == 0) return nil;
	if (emailAddress.length == 0) return nil;

    SZLCertificate *possibleCertificate = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (!possibleCertificate) return nil;

	return [possibleCertificate valueForAttribute:attribute];
}

+ (NSArray *) getAllAttributes:(NSString *)emailAddress
{
	if (emailAddress.length == 0) return nil;

    SZLCertificate *possibleCertificate = [SZLCertificate certificateWithIdentifier:emailAddress];
    if (!possibleCertificate) return nil;

	return [possibleCertificate getAllAttributes];
}

+ (NSString *) emailAttributeForPEMString:(NSString *)inPEMString;
{
	NSString *theAddress = nil;
	SZLCertificate *theCert = [SZLCertificate certificateWithPEMString:inPEMString andIdentifier:@"tmpIdentifier"];
	if (theCert != nil) {
		theAddress = [theCert getCertificateEmailAddress];
	}

	return theAddress;
}

+ (NSArray *) getAllAttributesForP12Data:(NSData *)inData passPhrase:(NSString *)inPassPhrase
{
	if (inData.length == 0) return nil;

    SZLCertificate *cert = [SZLCertificate newCertificateFromPKCS12Data:inData andPassphrase:inPassPhrase withIdentifier:kTempCertificateIdentifier];
    if (!cert) return nil;

	NSArray *attributeArray = [cert getAllAttributes];
	[cert deleteCertificate];

	return attributeArray;
}

+ (BOOL) hasUserCertificate
{
    return [FXMEmailSecurity hasCertificateForEmail:kUserCertificateIdentifier];
}

+ (BOOL) saveUserCertificateForEmail:(NSString *)inEmail withP12Data:(NSData *)inData withPassphrase:(NSString*)inPassphrase
{
	if (inEmail.length > 0) {
		[FXMEmailSecurity saveCertificateForEmail:inEmail withP12Data:inData withPassphrase:inPassphrase];
	}
	return [FXMEmailSecurity saveCertificateForEmail:kUserCertificateIdentifier withP12Data:inData withPassphrase:inPassphrase];
}

+ (void) deleteUserCertificate
{
	[FXMEmailSecurity deleteCertificateForEmail:kUserCertificateIdentifier];
}

+ (NSString *) valueForUserAttribute:(NSString*)attribute
{
	return [FXMEmailSecurity valueForAttribute:attribute email:kUserCertificateIdentifier];
}

+ (NSArray *) getAllUserAttributes
{
	return [FXMEmailSecurity getAllAttributes:kUserCertificateIdentifier];
}

+ (void) dumpSettingsDB
{
	SZLSecureDatabase *secureDB = [SZLSecureDatabase openSZLContainerSettings];
	if (secureDB != nil) {
		[secureDB dump];
	}

	return;
}

@end
