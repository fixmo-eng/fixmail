//
//  SZCSeamlessHeaderWebView.m
//  
//
//  Created by Anluan O'Brien on 12/12/12.
//
//

#import "SZCSeamlessHeaderWebView.h"

@interface SZCSeamlessHeaderWebView ()
{
    CGRect initialHeaderViewFrame;
}
@property (strong, nonatomic) IBOutlet UIView *internalHeaderView;
@end

@implementation SZCSeamlessHeaderWebView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)resizedHeaderViewIncludingVertically:(BOOL)vertically
{
    if (!CGRectEqualToRect(initialHeaderViewFrame, CGRectMake(0, 0, 0, 0))) {
        self.headerView.frame = CGRectMake(initialHeaderViewFrame.origin.x, initialHeaderViewFrame.origin.y, self.headerView.frame.size.width, self.headerView.frame.size.height);
        initialHeaderViewFrame = CGRectMake(0, 0, 0, 0);
    }
    self.internalHeaderView.frame = self.headerView.frame;
    self.messageView.scrollView.contentSize = CGSizeMake(self.messageView.frame.size.width, self.messageView.scrollView.contentSize.height);
    self.messageView.scalesPageToFit = YES;
    [self.internalHeaderView bringSubviewToFront:self.headerView];
    if (vertically) {
        self.messageView.scrollView.contentOffset = CGPointMake(0,self.internalHeaderView.frame.size.height);
        self.messageView.scrollView.contentInset = UIEdgeInsetsMake(self.internalHeaderView.frame.size.height, 0, 0, 0);
    }
    self.internalHeaderView.center = CGPointMake(self.internalHeaderView.center.x,self.internalHeaderView.center.y - self.internalHeaderView.frame.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.messageView.userInteractionEnabled = YES;
    self.messageView.scalesPageToFit = YES;
    
    //[self.messageView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://anluan.com"]]];
    //    self.messageView.scrollView.contentOffset = CGPointMake(0,-1 * self.headerView.frame.size.height);
    //    self.messageView.scrollView.contentInset = UIEdgeInsetsMake(-1 * self.headerView.frame.size.height, 0, 0, 0);
    
    self.internalHeaderView = [[UIView alloc] initWithFrame:self.headerView.frame];
    self.internalHeaderView.backgroundColor = [UIColor whiteColor];
    self.messageView.scrollView.backgroundColor = [UIColor grayColor];
    
    [self.messageView.scrollView addSubview:self.internalHeaderView];
    [self.internalHeaderView addSubview:self.headerView];
    self.internalHeaderView.clipsToBounds = YES;
    [self.messageView.scrollView setDelegate:self];
    
    initialHeaderViewFrame = CGRectMake(0, 0, 0, 0);
    self.messageView.scrollView.backgroundColor = [UIColor lightGrayColor];
    self.internalHeaderView.backgroundColor = [UIColor whiteColor];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.messageView.frame = self.view.frame;
    self.headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.headerView.frame.size.height);
    // Do any additional setup after loading the view from its nib.
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect visibleRect;
    visibleRect.origin = scrollView.contentOffset;
    visibleRect.size = scrollView.bounds.size;
    
    float theScale = 1.0 / scrollView.zoomScale;
    visibleRect.origin.x *= theScale;
    visibleRect.origin.y *= theScale;
    visibleRect.size.width *= theScale;
    visibleRect.size.height *= theScale;
    
    if (CGRectEqualToRect(initialHeaderViewFrame, CGRectMake(0, 0, 0, 0))) {
        initialHeaderViewFrame = self.headerView.frame;
    }
    
    CGRect floatFrame = self.headerView.frame;
    //NSLog(@"%f and %f",(floatFrame.size.width - visibleRect.origin.x),visibleRect.origin.x);
    
    if (visibleRect.origin.x >= 0) {// && (self.internalHeaderView.frame.size.width - floatFrame.size.width) >= visibleRect.origin.x) {
        floatFrame.origin.x = visibleRect.origin.x;
        self.headerView.frame = floatFrame;
    }
    
    //        float theScale = 1.0 / scrollView.zoomScale;
    //    CGRect visibleRect = CGRectApplyAffineTransform(scrollView.bounds, CGAffineTransformMakeScale(1.0 / scrollView.zoomScale, 1.0 / scrollView.zoomScale));
    //
    //    NSLog(@"VR: (%f x) %@",theScale, NSStringFromCGRect(visibleRect));
    
    //    CGRect tmp = self.floatingView.frame;
    //    tmp.origin = visibleRect.origin;
    //    self.floatingView.frame = tmp;
    
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    //NSLog(@"CS: %@",NSStringFromCGSize(scrollView.contentSize));
    
    CGRect visibleRect;
    visibleRect.origin = scrollView.contentOffset;
    visibleRect.size = scrollView.bounds.size;
    
    float theScale = 1.0 / scrollView.zoomScale;
    visibleRect.origin.x *= theScale;
    visibleRect.origin.y *= theScale;
    visibleRect.size.width *= theScale;
    visibleRect.size.height *= theScale;
    
    if (CGRectEqualToRect(initialHeaderViewFrame, CGRectMake(0, 0, 0, 0))) {
        initialHeaderViewFrame = self.headerView.frame;
    }
    
    CGRect floatFrame = self.headerView.frame;
    //NSLog(@"%f and %f",(floatFrame.size.width - visibleRect.origin.x),visibleRect.origin.x);
    
    
    CGRect headerFrame = self.internalHeaderView.frame;
    if (scrollView.contentSize.width >= scrollView.frame.size.width) {
        
        //        CGPoint hCenter = self.headerView.center;
        //        hCenter.x = scrollView.contentSize.width/2.0f;
        //        self.headerView.center = hCenter;
        
        headerFrame.size.width = scrollView.contentSize.width;
        self.internalHeaderView.frame = headerFrame;
        self.headerView.frame = floatFrame;
        //        self.floatingView.center = (CGPoint) {CGRectGetMidX(self.headerView.bounds), CGRectGetMidY(self.headerView.bounds)};
    }
    
    if (visibleRect.origin.x >= 0 && (self.internalHeaderView.frame.size.width - floatFrame.size.width) >= visibleRect.origin.x) {
        floatFrame.origin.x = visibleRect.origin.x;
        self.headerView.frame = floatFrame;
        self.internalHeaderView.frame = headerFrame;
    }
    
    //    CGPoint headerCenterPoint = self.headerView.center;
    //    headerCenterPoint.x = scrollView.contentSize.width/2;
    //    self.headerView.center = headerCenterPoint;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setInternalHeaderView:nil];
    [self setMessageView:nil];
    [self setHeaderView:nil];
    [super viewDidUnload];
}

@end
