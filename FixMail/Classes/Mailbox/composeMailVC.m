//
//  composeMailVC.m
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//
#import "composeMailVC.h"
#import "contactsListVC.h"
#import "SZCAddressView.h"
#import "SZCAttachmentView.h"
#import "SZCFilePreviewController.h"
#import "SZCTextEntryView.h"
#import "UIView-ViewFrameGeometry.h"
#import "UIAlertView+Modal.h"
#import "StringUtil.h"
#import "PKMIMEMessage+FixmoExtensions.h"
#import "UIStoryboard+FixmoExtensions.h"
#import "UIImageView+FixmoExtensions.h"
#import "UIWebview+FixmoExtensions.h"

#import "ASAccount.h"
#import "ASCompose.h"
#import "ASContact.h"
#import "ASEmailThread.h"
#import "ASResolveRecipients.h"
#import "ASSyncEmail.h"
#import "ASAttachment.h"
#import "AttachmentManager.h"
#import "EmailAddress.h"
#import "ErrorAlert.h"
#import "NSDate+TKCategory.h"
#import "NSObject+SBJSON.h"
#import "Reachability.h"
#import "AppSettings.h"
#import "FXMEmailSecurity.h"
#import "FXMCertificateManager.h"
#import "FXLMIMEMessage.h"

#import "FXMContactTableViewController.h"

@interface NSString (Fixmo_Extensions)

- (NSString *) lastCharacter;
- (BOOL) endsWithNewLine;

@end

@implementation NSString (Fixmo_Extensions)

- (NSString *) lastCharacter
{
	NSUInteger theLength = self.length;
	if (theLength == 0) return nil;

	return [self substringWithRange:NSMakeRange(theLength-1, 1)];
}

- (BOOL) endsWithNewLine
{
	if (self.length == 0) return NO;

	unichar lastCharacter = [self characterAtIndex:(self.length-1)];
	return [[NSCharacterSet newlineCharacterSet] characterIsMember:lastCharacter];
}


@end

#pragma mark -

static const CGFloat kMinimumAddressViewHeight =	34.0;
static const CGFloat kMinimumBodyViewHeight =		200.0;
static const CGFloat kSeparatorPadding =			5.0;
static const CGFloat kTopY =						0.0;	// our navigation bar is not system generated but in IB
static const CGFloat kLeftX =						5.0;
static const CGFloat kFontSize =					14.0;
static const CGFloat kViewModeAnimationDelay =		0.1;
static const CGFloat kViewModeAnimationDuration =	0.3;

@interface composeMailVC ()
@property (nonatomic, strong) FXMContactTableViewController *contactsController;

@property (nonatomic, strong) SZLAttachmentBrowserController *fileBrowserController;
@property (nonatomic, strong) NSMutableArray *uploadList;

@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) IBOutlet UILabel *activityLabel;

@property (nonatomic, assign) bool shouldDigitallySign;
@property (nonatomic, assign) bool shouldEncrypt;

@property (nonatomic, assign) bool basicView;
@property (nonatomic, assign) bool webviewRendered;
@property (nonatomic, assign) bool attachmentViewOpened;
@property (nonatomic, assign) bool attachmentsHaveChanged;

@property (nonatomic, strong) UIBarButtonItem *sendButton;
@property (nonatomic, strong) UIBarButtonItem *signCertButton;
@property (nonatomic, strong) UIBarButtonItem *unsignCertButton;
@property (nonatomic, strong) UIBarButtonItem *lockCertButton;
@property (nonatomic, strong) UIBarButtonItem *unlockCertButton;

@property (nonatomic) BOOL networkReachable;
@end

@implementation composeMailVC

+ (composeMailVC *) createMailComposerWithTo:(NSArray *)inToList cc:(NSArray *)inCCList bcc:(NSArray *)inBCCList attachments:(NSArray *)inAttachments account:(BaseAccount*)inAccount;
{
	composeMailVC *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPad identifier:@"composeMailVC"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPhone identifier:@"composeMailVC"];
	}
	if (inToList.count > 0) {
		theController.toList = [NSArray arrayWithArray:inToList];
	}
	if (inCCList.count > 0) {
		theController.ccList = [NSArray arrayWithArray:inCCList];
	}
	if (inBCCList.count > 0) {
		theController.bccList = [NSArray arrayWithArray:inBCCList];
	}
	if (inAttachments.count > 0) {
		theController.attachmentList = [NSMutableArray arrayWithArray:inAttachments];
	}
	if (inAccount) {
		theController.account = inAccount;
	} else {
		if([BaseAccount accounts].count == 0) {
			[ASAccount loadExistingEmailAccounts];
		}
		theController.account = [BaseAccount currentLoggedInAccount];
	}
	
	theController.networkReachable = YES;

	return theController;
}

+ (UINavigationController *) createNavigationMailComposerWithTo:(NSArray *)inToList cc:(NSArray *)inCCList bcc:(NSArray *)inBCCList attachments:(NSArray *)inAttachments account:(BaseAccount*)inAccount
{
	composeMailVC *theController = [composeMailVC createMailComposerWithTo:inToList cc:inCCList bcc:inBCCList attachments:inAttachments account:inAccount];
	return [[UINavigationController alloc] initWithRootViewController:theController];
}

- (void) viewDidLoad
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCLoadUnload, @"composeMailVC: viewDidLoad");
    [super viewDidLoad];

    [self setViewOrientation:self.interfaceOrientation];
    
	self.navigationController.navigationBarHidden = false;

	self.from = [[EmailAddress alloc] initWithName:self.account.name address:self.account.emailAddress];

	[self setupControls];

	return;
}

- (void) viewDidUnload
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCLoadUnload, @"composeMailVC: viewDidUnload");

	self.toView = nil;
	self.ccView = nil;
	self.bccView = nil;
	self.fromView = nil;
	self.subjectView = nil;
	self.bodyView = nil;

	self.toSeparator = nil;
	self.ccSeparator = nil;
	self.bccSeparator = nil;
	self.fromSeparator = nil;
	self.subjectSeparator = nil;

	[self viewDidUnload];
}

- (void) viewWillAppear:(BOOL)animated
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCAppearDisappear, @"composeMailVC: viewWillAppear");

    [super viewWillAppear:animated];
    [self observeKeyboard];
	self.navigationController.navigationBarHidden = true;
    [self startReachablility];
}

- (void) viewDidAppear:(BOOL)animated
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCAppearDisappear, @"composeMailVC: viewDidAppear");

	[super viewDidAppear:animated];
	self.fileBrowserController = nil;
	[self setupNotifications];
}

- (void) viewWillDisappear:(BOOL)animated
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCAppearDisappear, @"composeMailVC: viewWillDisappear");

    [super viewWillDisappear:animated];
    [self stopReachability];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidDisappear:(BOOL)animated
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCAppearDisappear, @"composeMailVC: viewDidDisappear");

    [super viewDidDisappear:animated];
}

- (void) dealloc
{
	FXDebugLog(kFXLogComposeMailVC | kFXLogVCLoadUnload, @"composeMailVC: dealloc");

	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setViewOrientation:toInterfaceOrientation];
    [self redoControls];
    [self redoBodyWebview];
    [self redoBody];
}

#pragma mark - Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
			self.networkReachable = YES;
			[self redoSendButton];
            break;
        case NotReachable:
			self.networkReachable = NO;
			[self redoSendButton];
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

#pragma mark - IBActions

- (IBAction) toggleCancel:(id)inSender
{
    if (self.toView.emailAddresses.count == 0 && self.ccView.emailAddresses.count == 0 && self.bccView.emailAddresses.count == 0 && [self.subjectView.textEntered isEqualToString:@""] && [self.bodyView.text isEqualToString:@""]) {
		
		[self deleteDraft];
		[self dismissViewControllerAnimated:TRUE completion:nil];
		
	} else {
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
																 delegate:self
														cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_DELETE_DRAFT", @"composeMailVC", @"Alert view cancel button to keep composing draft") 
												   destructiveButtonTitle:FXLLocalizedStringFromTable(@"DELETE_DRAFT", @"composeMailVC", @"Alert view button to delete current draft when cancelling composing a message")
														otherButtonTitles:FXLLocalizedStringFromTable(@"SAVE_DRAFT", @"composeMailVC", @"Alertview button to save draft when cancelling composing a message"), nil];
		[actionSheet showInView:self.view];
		
	}
}

- (IBAction) toggleSend:(id)inSender
{
	// have to check a couple things before sending
	// 1: if there's a subject
	// 2: if there's attachments AND whether they've all been downloaded
	// 3: get rid of the keyboard
	// 5: send the message immediately
	// 6:

	// 1:
	if (self.subjectView.textEntered.length == 0) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"NO_SUBJECT_SPECIFIED", @"composeMailVC", @"Alert view title when sending email without a subject")
                                                     message:FXLLocalizedStringFromTable(@"ARE_YOU_SURE_YOU_WANT_TO_SEND_THIS_EMAIL_WITHOUT_A_SUBJECT", @"composeMailVC", @"Alert view message when sending email without a subject")
													delegate:self
										   cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_SEND_WITHOUT_SUBJECT", @"composeMailVC", @"Cancel Button of alert view when sending email without a subject")
										   otherButtonTitles:FXLLocalizedStringFromTable(@"SEND_WITHOUT_SUBJECT", @"composeMailVC", @"Send Button of alert view when sending email without a subject"), nil
						   ];
		if ([av showModal] == 0) return;
    }

	// 2:
	if (self.attachmentList.count > 0) {
		if ([self countAttachmentsNotDownloaded] > 0) {
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"ATTACHMENTS_NOT_DOWNLOADED", @"composeMailVC", @"There are still attachments that need to be downloaded")
                                                         message:FXLLocalizedStringFromTable(@"START_DOWNLOADING_ATTACHMENTS", @"composeMailVC", @"All attachments not downloaded will begin downloading")
                                                        delegate:self
                                               cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_DOWNLOADING", @"composeMailVC", @"Cancel indicating go back to message")
                                               otherButtonTitles:FXLLocalizedStringFromTable(@"OK_DOWNLOADING", @"composeMailVC", @"OK indicating commence downloading"), nil
                               ];
            if ([av showModal] == 1) {
                [self downloadRemainingAttachments];
            }
			return;
		}
	}

	// 3:
	[self dismissKeyboard];

	// 4:
	[self sendMessage];

	// 5: only stay up for a couple seconds, then dismiss
	[self stopActivity:true];
	[self dismissViewControllerAnimated:TRUE completion:nil];

	return;
}

- (IBAction) toggleAddAttachment:(id)inSender
{
	NSAssert(self.fileBrowserController == nil, @"self.fileBrowserController");

	[self dismissKeyboard];

	NSString *documentsDirectory = [FXLSafeZone documentsDirectory];
	self.fileBrowserController = [SZLAttachmentBrowserController launchFileBrowserFromPath:documentsDirectory];
	self.fileBrowserController.delegate = self;
	[self.navigationController pushViewController:self.fileBrowserController animated:TRUE];
}

- (IBAction) toggleSignMessage:(id)inSender
{
	self.shouldDigitallySign = !self.shouldDigitallySign;
	if (!self.shouldDigitallySign) self.shouldEncrypt = NO;
	[self redoSignEncryptButtons];

	return;
}

- (IBAction) toggleEncryptMessage:(id)inSender
{
	self.shouldEncrypt = !self.shouldEncrypt;
	[self redoSignEncryptButtons];

	if (self.shouldEncrypt) {
		NSMutableArray *allAddresses = [[NSMutableArray alloc] init];
		[allAddresses addObjectsFromArray:self.toView.emailAddresses];
		[allAddresses addObjectsFromArray:self.ccView.emailAddresses];
		[allAddresses addObjectsFromArray:self.bccView.emailAddresses];
		for (EmailAddress *theAddress in allAddresses) {
			if (theAddress.hasCertificate) continue;
			[self performSelector:@selector(delayedDownloadCertificate:) withObject:theAddress.address afterDelay:0.5];
	//		[FXMCertificateManager downloadCertificate:theAddress.address];
		}
	}
	[self.toView checkCertificateState];
	[self.ccView checkCertificateState];
	[self.bccView checkCertificateState];

}

-(void)toggleContactVCBackButton:(id)sender
{
	[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) toggleAttachmentDisclosure:(id)inSender
{
	[self dismissKeyboard];
 	_attachmentViewOpened = !_attachmentViewOpened;

	float separatorTop;
	CGRect theRect = [self bodyRect];
	if (_attachmentViewOpened) {
		separatorTop = self.attachmentsView.bottom + kSeparatorPadding;
		theRect.origin.y = separatorTop + kSeparatorPadding;
	} else {
		separatorTop = self.attachmentsView.top + (self.attachmentsLabel.bottom + kSeparatorPadding - 2.0);
		theRect.origin.y = separatorTop + kSeparatorPadding;
	}

	if (_attachmentViewOpened) {
		[self.attachmentsArrows rotateAndReplace:90 withImage:[UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyDown.png"]];
	} else {
		[self.attachmentsArrows rotateAndReplace:-90 withImage:[UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyRight.png"]];
	}
 	[UIView animateWithDuration:0.2
		animations:^{
			self.attachmentSeparator.top = separatorTop;
			self.bodyView.frame = theRect;
		}
		completion:^(BOOL inFinished) {
			[self redoBody];
			[self dumpAllViews];
		}
	];

	return;
}

#pragma mark - SZLAttachmentBrowserDelegate methods

- (void) didCancel:(SZLAttachmentBrowserController *)inAttachmentBrowserController
{
	NSParameterAssert(inAttachmentBrowserController != nil);

	[self.navigationController popToRootViewControllerAnimated:YES];
	self.fileBrowserController = nil;
}

- (void) didSelectItem:(SZLAttachmentBrowserController *)inAttachmentBrowserController path:(NSString *)inFilePath
{
	NSParameterAssert(inAttachmentBrowserController != nil);
	NSParameterAssert(inFilePath.length > 0);

	if (self.uploadList == nil) self.uploadList = [[NSMutableArray alloc] initWithCapacity:1];
	[self.uploadList addObject:inFilePath];
	[self.navigationController popToRootViewControllerAnimated:YES];
	self.fileBrowserController = nil;

	_attachmentViewOpened = true;
	_attachmentsHaveChanged = true;
	[self redoControls];
}

#pragma mark - UITextViewDelegate methods

- (void) textViewDidChange:(UITextView *)inTextView
{
//	[self.bodyWebview prependTextToBody:inTextView.text];
	[self redoBody];
}

#pragma mark - UIWebViewDelegate methods

- (BOOL)webView:(UIWebView *)inWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	if (self.webviewRendered) return NO;
	// Should check email 
	return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)inWebView
{
	self.bodyWebview.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)inWebView
{
	[self.bodyWebview encloseBodyInQuoteStyle];
	[self.bodyWebview makeEditable];
	self.bodyWebview.keyboardDisplayRequiresUserAction = NO;

	int bodyHeight = [self.bodyWebview bodyHeight];
	self.bodyWebview.height = bodyHeight;
	self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.bodyWebview.top + bodyHeight);

	self.bodyWebview.scrollView.scrollEnabled = YES;
	self.bodyWebview.hidden = NO;

	self.webviewRendered = true;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

#pragma mark - UIActionSheetDelegate methods
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == actionSheet.cancelButtonIndex) {
		// Return to email
	} else if (buttonIndex == actionSheet.destructiveButtonIndex) {
		[self deleteDraft];
		[self dismissViewControllerAnimated:TRUE completion:nil];
	} else {
		[self saveDraft];
		[self dismissViewControllerAnimated:TRUE completion:nil];
	}
}

#pragma mark - SZCAddressViewDelegate methods

- (void) delayedDownloadCertificate:(NSString *)inEmailAddress
{
	[FXMCertificateManager downloadCertificate:inEmailAddress];
}

- (void) addressViewEmailAddressAdded:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress
{
	if (!self.shouldDigitallySign) return;
	if (inEmailAddress.hasCertificate) return;

	[self performSelector:@selector(delayedDownloadCertificate:) withObject:inEmailAddress.address afterDelay:0.5];
	return;
}

- (void) reformatAddressFields
{
	[UIView animateWithDuration:kViewModeAnimationDuration
		animations:^{
			[self redoControls];
			[self redoSignEncryptButtons];
	}];
}

- (void) addressViewBecameFirstResponder:(SZCAddressView *)inAddressView
{
	if (inAddressView == self.ccView) {
		_basicView = false;
	} else if (inAddressView == self.bccView) {
		_basicView = false;
	} else {
		[self determineViewMode];
	}
	[self performSelector:@selector(reformatAddressFields) withObject:nil afterDelay:kViewModeAnimationDelay];
}

- (void) addressViewLayoutChanged:(SZCAddressView *)inAddressView
{
	[self determineViewMode];
	[self performSelector:@selector(reformatAddressFields) withObject:nil afterDelay:kViewModeAnimationDelay];
}

- (void) addressViewTextChanged:(SZCAddressView *)inAddressView
{
	[self redoSendButton];
	
	if (inAddressView.textEntered.length == 0) {
		[inAddressView removeTimer];
		[self dismissContactList];
		return;
	} else if (inAddressView.textEntered.length < 3) {
		
		// set off timer
		[inAddressView resetTimer];
		
	} else {
		[inAddressView removeTimer];
		[self showContactList:inAddressView];
	}
}


- (void) addressViewEmailAddressClicked:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress
{
	FXDebugLog(kFXLogComposeMailVC, @"addressViewEmailAddressClicked: %@ <%@>", inEmailAddress.name, inEmailAddress.address);
	
	[self dismissKeyboard];
	
	UIViewController *contactVC = [inEmailAddress asContactViewController];
	
	UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:contactVC];
	navController.modalPresentationStyle = UIModalPresentationPageSheet;
	contactVC.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"BACK_TO_EMAIL", @"ASContact", @"Back button for returning to email when viewing contact from email viewer screen") style:UIBarButtonItemStyleDone target:self action:@selector(toggleContactVCBackButton:)];
	
	[self.navigationController presentViewController:navController animated:YES completion:nil];
}

- (void) addressViewEmailAddressDoubleClicked:(SZCAddressView *)inAddressView email:(EmailAddress *)inEmailAddress
{
	FXDebugLog(kFXLogComposeMailVC, @"addressViewEmailAddressDoubleClicked: %@ <%@>", inEmailAddress.name, inEmailAddress.address);
}

- (void) addressViewTextEditingDidPause:(SZCAddressView *)inAddressView
{
	// should be called when timer is set off ends
	[self showContactList:inAddressView];
}

#pragma mark - SZCTextEntryViewDelegate methods

- (void) textEntryViewBecameFirstResponder:(SZCTextEntryView *)inView
{
	[self determineViewMode];
	[self performSelector:@selector(reformatAddressFields) withObject:nil afterDelay:kViewModeAnimationDelay];
}

- (void) textEntryViewClicked:(SZCTextEntryView *)inView
{
	if ((inView == self.fromView) && (_basicView)) {
		if (self.toView.isFirstResponder) [self.toView resignFirstResponder];
		else if (self.ccView.isFirstResponder) [self.ccView resignFirstResponder];
		else if (self.bccView.isFirstResponder) [self.bccView resignFirstResponder];
		[self.ccView becomeFirstResponder];
	}

/*	__block bool makeCCFieldResponder = false;
	[UIView animateWithDuration:kViewModeAnimationDuration
		animations:^{
			makeCCFieldResponder = false;
			if ((inView == self.fromView) && (_basicView)) {
				_basicView = false;
				makeCCFieldResponder = true;
				[self redoControls];
			} else {
				[self determineViewMode];
				[self redoControls];
			}
		}
		completion:^(BOOL inFinished) {
			if (makeCCFieldResponder) {
				if (self.toView.isFirstResponder) [self.toView resignFirstResponder];
				[self.ccView becomeFirstResponder];
			}
		}
	];
*/
	return;
}

- (void) textEntryViewTextChanged:(SZCTextEntryView *)inView
{
	if (inView == self.subjectView) {
		[self redoSendButton];
	}

	return;
}

#pragma mark - SZCAttachmentView methods

- (void) attachmentClicked:(SZCAttachmentView *)inAttachmentView path:(NSString *)inAttachmentPath
{
	NSParameterAssert(inAttachmentView != nil);

	UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:nil
														   message:FXLLocalizedStringFromTable(@"DELETE_VIEW_ATTACHMENT", @"composeMailVC", @"Asking user what to do when clicking on the attachment")
														  delegate:self
												 cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_OPTION_DELETE_VIEW_ATTACHMENT", @"composeMailVC", @"Cancel Option of alert view when clicking on attachment")
												 otherButtonTitles:FXLLocalizedStringFromTable(@"DELETE_OPTION_DELETE_VIEW_ATTACHMENT", @"composeMailVC", @"Delete Option of alert view when clicking on attachment"),
																   FXLLocalizedStringFromTable(@"VIEW_OPTION_DELETE_VIEW_ATTACHMENT", @"composeMailVC", @"View Option of alert view when clicking on attachment"), nil
								 ];

	int indexClicked = [theAlertView showModal];
	if (indexClicked == 1) {
		// 1) remove from the attachment list or upload list
		// 2) remove the view
		// 3) redo the views

		// 1)
		if (inAttachmentView.attachment != nil) {
			[self.attachmentList removeObject:inAttachmentView.attachment];
		} else {
			NSAssert(inAttachmentView.filePath != nil, @"inAttachmentView.filePath != nil");
			[self.uploadList removeObject:inAttachmentView.filePath];
		}
		_attachmentsHaveChanged = true;

		// 2)
		[inAttachmentView removeFromSuperview];

		// 3)
		[self redoControls];

	} else if (indexClicked == 2) {

		NSString *theFilePath = nil;
		NSString *theFileName = nil;
		if (inAttachmentView.attachment != nil) {
			theFilePath = [AttachmentManager attachmentPathWithAttachment:inAttachmentView.attachment email:inAttachmentView.email];
			theFileName = inAttachmentView.attachment.name;
		} else {
			NSAssert(inAttachmentView.filePath != nil, @"inAttachmentView.filePath != nil");
			theFilePath = inAttachmentView.filePath;
			theFileName = [inAttachmentView.filePath lastPathComponent];
		}
		SZCFilePreviewController *theController = [SZCFilePreviewController createPreviewer:theFilePath name:theFileName passPhrase:nil];
		[self.navigationController pushViewController:theController animated:true];

	}

	return;
}

#pragma mark - Private methods

- (void) setupNotifications
{
	[[NSNotificationCenter defaultCenter] addObserverForName:kSZCContactListSelectedNotification object:nil queue:nil
		usingBlock:^(NSNotification *inNotification) {
			// our object will be an ASContact so do the following:
			// 1) get the contact
			// 2) add the email address of the contact to the current list
			// 3) get rid of the list
			// 4) redo send button

			// 1)
			ASContact *theContact = (ASContact *)inNotification.object;

			// 2)
			if (self.toView.isFirstResponder) [self.toView appendEmailAddresses:@[theContact.emailAddress1]];
			else if (self.ccView.isFirstResponder) [self.ccView appendEmailAddresses:@[theContact.emailAddress1]];
			else if (self.bccView.isFirstResponder) [self.bccView appendEmailAddresses:@[theContact.emailAddress1]];

			// 3)
			[self dismissContactList];

			// 4)
			[self redoSendButton];

			[self determineViewMode];
			[self redoControls];
		}
	];

	[[NSNotificationCenter defaultCenter] addObserverForName:kCertificateRetrievalStartedNotification object:nil queue:nil
		usingBlock:^(NSNotification *inNotification) {
			[self.toView checkCertificateState];
			[self.ccView checkCertificateState];
			[self.bccView checkCertificateState];
			[self redoSignEncryptButtons];
		}
	];

	[[NSNotificationCenter defaultCenter] addObserverForName:kCertificateRetrievalSuccessNotification object:nil queue:nil
		usingBlock:^(NSNotification *inNotification) {
			[self.toView checkCertificateState];
			[self.ccView checkCertificateState];
			[self.bccView checkCertificateState];
			[self redoSignEncryptButtons];
		}
	];

	[[NSNotificationCenter defaultCenter] addObserverForName:kCertificateRetrievalErrorNotification object:nil queue:nil
		usingBlock:^(NSNotification *inNotification) {
			[self.toView checkCertificateState];
			[self.ccView checkCertificateState];
			[self.bccView checkCertificateState];
			[self redoSignEncryptButtons];
		}
	];
	return;
}

- (void) setViewOrientation:(UIInterfaceOrientation)orienation
{
    if (IS_IPAD()) {
        if (UIDeviceOrientationIsLandscape(orienation)) {
            self.view.width=1024.0f;
            self.view.height=768.0f;
        } else {
            self.view.width=768.0f;
            self.view.height=1024.0f;
        }
    }
}

- (void) setupControls
{
	//
	// Nav buttons
	//
	self.sendButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"SEND", @"composeMailVC", @"Title for button for sending composed email") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleSend:)];
	self.signCertButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FixMailRsrc.bundle/signWhiteIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(toggleSignMessage:)];
	self.unsignCertButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FixMailRsrc.bundle/unsignWhiteIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(toggleSignMessage:)];
	self.lockCertButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FixMailRsrc.bundle/lockWhiteIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(toggleEncryptMessage:)];
	self.unlockCertButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FixMailRsrc.bundle/unlockWhiteIcon.png"] style:UIBarButtonItemStyleBordered target:self action:@selector(toggleEncryptMessage:)];

	//
	// Activity Indicator
	// - initially just keep it out of the view hierarchy
	self.activityView.layer.cornerRadius = 10.0;
	self.activityIndicator.hidesWhenStopped = true;
	self.activityLabel.text = @"";
	[self.activityView removeFromSuperview];

	self.toView.backgroundColor = [UIColor clearColor];
	self.ccView.backgroundColor = [UIColor clearColor];
	self.bccView.backgroundColor = [UIColor clearColor];
	self.fromView.backgroundColor = [UIColor clearColor];
	self.subjectView.backgroundColor = [UIColor clearColor];

	CGRect separatorRect = CGRectMake(0.0, 0.0, self.scrollView.width, 1.0);
	self.toSeparator.frame = separatorRect;
	self.toSeparator.backgroundColor = [UIColor lightGrayColor];
    self.ccSeparator.frame = separatorRect;
	self.ccSeparator.backgroundColor = [UIColor lightGrayColor];
	self.bccSeparator.frame = separatorRect;
	self.bccSeparator.backgroundColor = [UIColor lightGrayColor];
	self.fromSeparator.frame = separatorRect;
	self.fromSeparator.backgroundColor = [UIColor lightGrayColor];
	self.subjectSeparator.frame = separatorRect;
	self.subjectSeparator.backgroundColor = [UIColor lightGrayColor];

	self.toView.left = kLeftX;
	self.toView.delegate = self;
	self.toView.title = FXLLocalizedStringFromTable(@"TO_COLON", @"composeMailVC", @"Title of recipient (to) field when composing email");
	[self.toView appendEmailAddresses:self.toList];

	self.ccView.left = kLeftX;
	self.ccView.delegate = self;
	self.ccView.title = FXLLocalizedStringFromTable(@"CC_COLON", @"composeMailVC", @"Title of cc field when composing email");
	[self.ccView appendEmailAddresses:self.ccList];

	self.bccView.left = kLeftX;
	self.bccView.delegate = self;
	self.bccView.title = FXLLocalizedStringFromTable(@"BCC_COLON", @"composeMailVC", @"Title of bcc field when composing email");
	[self.bccView appendEmailAddresses:self.bccList];

	self.fromView.left = kLeftX;
	self.fromView.delegate = self;
	self.fromView.title = FXLLocalizedStringFromTable(@"FROM_COLON", @"composeMailVC", @"Title of sender (from) field when composing email");
	if (self.from.name.length > 0) {
		[self.fromView.textEntered setString:self.from.name];
		self.fromView.editable = false;
	}

	self.subjectView.left = kLeftX;
	self.subjectView.delegate = self;
	self.subjectView.title = FXLLocalizedStringFromTable(@"SUBJECT_COLON", @"composeMailVC", @"Title of subject field when composing email");
	if (self.subject.length > 0) {
		[self.subjectView.textEntered setString:self.subject];
	}

	_basicView = true;
	if (self.ccList.count > 0) _basicView = false;
	else if (self.bccList.count > 0) _basicView = false;

	// in _basicView,make sure to set the alphas to 0 otherwise
	// there will be artificts going from _basicView -> !_basicView
	self.ccView.alpha = _basicView ? 0.0 : 1.0;
	self.bccView.alpha = _basicView ? 0.0 : 1.0;
	self.ccSeparator.alpha = _basicView ? 0.0 : 1.0;
	self.bccSeparator.alpha = _basicView ? 0.0 : 1.0;

	// configure the attachmentsView
	self.attachmentsView.backgroundColor = [UIColor whiteColor];
	self.attachmentsView.hidden = true;
	self.attachmentsView.left = 0.0;
	self.attachmentsView.width = self.scrollView.width;
	self.attachmentSeparator.frame = CGRectMake(0.0, 0.0, self.attachmentsView.width, 1.0);;
	self.attachmentSeparator.backgroundColor = [UIColor lightGrayColor];

	// configure the bodyview
	self.bodyView.backgroundColor = [UIColor whiteColor];
	self.bodyView.left = 0.0;
	self.bodyView.width = self.scrollView.width;

	self.bodyView.font = [UIFont systemFontOfSize:kFontSize];
    self.bodyView.text = [self getPlainText:@"\n\n"];
	if (self.mailType == composeReply) {
		self.bodyView.text = [self replyString];
	} else if (self.mailType == composeForward) {
		self.bodyView.text = [self replyString];
	} else if (self.body.length > 0) {
		self.bodyView.text = self.body;
	}

	self.bodyWebview.backgroundColor = [UIColor whiteColor];
	self.bodyWebview.hidden = true;
	self.bodyWebview.delegate = self;

	self.webviewRendered = false;
	self.attachmentViewOpened = false;
	self.attachmentsHaveChanged = true;

	[self redoControls];

	self.shouldDigitallySign = false;
	self.shouldEncrypt = false;
	[self redoSignEncryptButtons];

}

- (void) redoControls
{
	CGFloat toHeight = self.toView.rowHeight;
	self.toView.top = kTopY + kSeparatorPadding;
	if (self.toView.height != toHeight) {
		if (toHeight < kMinimumAddressViewHeight) toHeight = kMinimumAddressViewHeight;
		self.toView.height = toHeight;
	}
	self.toSeparator.top = self.toView.bottom + kSeparatorPadding;

	if (_basicView) {

		self.ccView.alpha = 0.0;
		self.bccView.alpha = 0.0;
		self.ccSeparator.alpha = 0.0;
		self.bccSeparator.alpha = 0.0;

		NSString *fromString = [NSString stringWithFormat:@"%@/%@, %@:", FXLLocalizedStringFromTable(@"CC", @"composeMailVC", @"Cc name in string"), FXLLocalizedStringFromTable(@"BCC", @"composeMailVC", "Bcc name in string"), FXLLocalizedStringFromTable(@"FROM", @"composeMailVC", @"From name in string")];
		if (self.from != nil) fromString = [fromString stringByAppendingString:[NSString stringWithFormat:@" %@", self.from.name]];
		CGFloat fromHeight = self.fromView.rowHeight;
		self.fromView.title = fromString;
		[self.fromView.textEntered setString:@""];
		[self.fromView setNeedsLayout];
		self.fromView.top = self.toSeparator.bottom + kSeparatorPadding;
		if (fromHeight  != self.fromView.height) {
			self.fromView.height = fromHeight;
		}
		self.fromSeparator.top = self.fromView.bottom + kSeparatorPadding;

	} else {

		self.ccView.alpha = 1.0;
		self.bccView.alpha = 1.0;
		self.ccSeparator.alpha = 1.0;
		self.bccSeparator.alpha = 1.0;

		CGFloat ccHeight = self.ccView.rowHeight;
		self.ccView.top = self.toView.bottom + 10.0;
		if (self.ccView.height != ccHeight) {
			if (ccHeight < kMinimumAddressViewHeight) ccHeight = kMinimumAddressViewHeight;
			self.ccView.height = ccHeight;
		}
		self.ccSeparator.top = self.ccView.bottom + kSeparatorPadding;

		CGFloat bccHeight = self.bccView.rowHeight;
		self.bccView.top = self.ccSeparator.bottom + kSeparatorPadding;
		if (self.bccView.height != bccHeight) {
			if (bccHeight < kMinimumAddressViewHeight) bccHeight = kMinimumAddressViewHeight;
			self.bccView.height = bccHeight;
		}
		self.bccSeparator.top = self.bccView.bottom + kSeparatorPadding;

		CGFloat fromHeight = self.fromView.rowHeight;
		self.fromView.title = FXLLocalizedStringFromTable(@"FROM_COLON", @"composeMailVC", @"Title of sender (from) field when composing email");
		if (self.from != nil) {
			[self.fromView.textEntered setString:self.from.name];
			self.fromView.editable = false;
		}
		[self.fromView setNeedsLayout];
		self.fromView.top = self.bccSeparator.bottom + kSeparatorPadding;
		if (fromHeight  != self.fromView.height) {
			self.fromView.height = fromHeight;
		}
		self.fromSeparator.top = self.fromView.bottom + kSeparatorPadding;
	}

	CGFloat subjectHeight = self.subjectView.rowHeight;
	self.subjectView.top = self.fromSeparator.bottom + kSeparatorPadding;
	if (subjectHeight  != self.subjectView.height) {
		self.subjectView.height = subjectHeight;
	}
	self.subjectSeparator.top = self.subjectView.bottom + kSeparatorPadding;

	// position our attachment button beside the subject
	self.attachmentButton.top = self.subjectView.top;
	self.attachmentButton.right = self.view.right - kSeparatorPadding;
	self.subjectView.width = (self.attachmentButton.left - 3.0) - self.subjectView.left;

	if (_basicView) {
		self.ccSeparator.top = self.fromSeparator.top;
		self.bccSeparator.top = self.subjectSeparator.top;
	}

	CGFloat theBottom = self.subjectSeparator.bottom + 1;

	self.attachmentsView.top = theBottom;
	[self redoAttachments];

	if ((self.attachmentList.count > 0) || (self.uploadList.count > 0)) {
		theBottom = self.attachmentSeparator.bottom + kSeparatorPadding;
	}

	self.bodyView.top = theBottom;

	self.bodyWebview.top = self.bodyView.bottom + kSeparatorPadding;
	[self redoBodyWebview];
	[self redoBody];

	[self redoSendButton];

	[self dumpAllViews];
}

- (CGRect) bodyRect
{
	CGFloat theHeight = [self heightForBody];
	CGRect theRect = self.bodyView.frame;

	CGFloat visualDiff = self.scrollView.bottom - self.bodyView.top;
	if ((visualDiff > theHeight) && (self.bodyWebview.hidden)) {
		theRect.size.height = visualDiff;
	} else {
		theRect.size.height = theHeight;
	}
	theRect.size.height += 16.0;

	return theRect;
}

- (void) redoBodyWebview
{
	if (_webviewRendered) return;

	if (self.referenceEmailSMimeData != nil) {
		self.bodyWebview.height = [PKMIMEMessage mimeBodyBlock:self.referenceEmailSMimeData top:self.bodyWebview.top addToView:self.bodyWebview];
		self.bodyWebview.width = self.view.width - self.bodyWebview.left;
		self.bodyWebview.scrollView.contentSize = CGSizeMake(self.bodyWebview.width, self.bodyWebview.bottom);
			
	} else if (self.referenceEmail != nil) {
		CGFloat theTop = self.bodyWebview.top;

		// this will probably only be temporary
		int messageHeight = [PKMIMEMessage bodyBlock:self.referenceEmail top:theTop addToView:self.bodyWebview];
		self.bodyWebview.height = messageHeight;

		self.bodyWebview.width = self.view.width - self.bodyWebview.left;
		self.bodyWebview.scrollView.contentSize = CGSizeMake(self.bodyWebview.width, messageHeight);

	} else {
		self.bodyWebview.hidden = true;
	}

	return;
}

- (void) redoBody
{
	self.bodyView.frame = [self bodyRect];
	if (self.bodyWebview.hidden) {
		self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.bodyView.top + self.bodyView.height);
	} else {
		self.bodyWebview.top = self.bodyView.bottom + kSeparatorPadding;
		self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.bodyView.top + self.bodyView.height + self.bodyWebview.height);
	}
	self.scrollView.scrollEnabled = true;

	[self dumpAllViews];
}

- (CGFloat) heightForBody
{
    float horizontalPadding = 24;
    float verticalPadding = 16;
    float widthOfTextView = self.bodyView.contentSize.width - horizontalPadding;
    float height = [self.bodyView.text sizeWithFont:self.bodyView.font constrainedToSize:CGSizeMake(widthOfTextView, 999999.0f) lineBreakMode:NSLineBreakByWordWrapping].height + verticalPadding;
 
    return height;
}

- (void) redoAttachments
{
	if (!_attachmentsHaveChanged) {
		if (_attachmentViewOpened) {
			self.attachmentsArrows.image = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyDown.png"];
			self.attachmentSeparator.top = self.attachmentsView.bottom;
		} else {
			self.attachmentsArrows.image = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyRight.png"];
			self.attachmentSeparator.top = self.attachmentsView.top + self.attachmentsLabel.bottom + kSeparatorPadding - 2.0;
		}
		return;
	}

	// have to delete any attachment subviews and recreate them
	NSMutableArray *deleteArray = [NSMutableArray new];
	for (UIView *theSubview in self.attachmentsView.subviews) {
		if (![theSubview isKindOfClass:[SZCAttachmentView class]]) continue;
		[deleteArray addObject:theSubview];
	}
	if (deleteArray.count > 0) {
		[deleteArray makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}

	// check if any attachments, and if none, then hide the view entirely
	if ((self.attachmentList.count == 0) && (self.uploadList.count == 0)) {
		self.attachmentsView.hidden = YES;
		self.attachmentSeparator.hidden = YES;
		self.attachmentsLabel.text = @"";
		return;
	}

	float theBottom = self.attachmentsLabel.bottom + kSeparatorPadding + kSeparatorPadding;
	for (ASAttachment *theAttachment in self.attachmentList) {
		SZCAttachmentView *theAttachmentView = [[SZCAttachmentView alloc] initWithAttachment:theAttachment email:self.referenceEmail delegate:self];
		theAttachmentView.top = theBottom;
		theAttachmentView.left = 10.0;
		[self.attachmentsView addSubview:theAttachmentView];
		theBottom += (theAttachmentView.height + kSeparatorPadding);
	}
	for (NSString *theFilePath in self.uploadList) {
		SZCAttachmentView *theAttachmentView = [[SZCAttachmentView alloc] initWithPath:theFilePath delegate:self];
		theAttachmentView.top = theBottom;
		theAttachmentView.left = 10.0;
		[self.attachmentsView addSubview:theAttachmentView];
		theBottom += (theAttachmentView.height + kSeparatorPadding);
	}

	// our label will show the count of attachments
	int attachmentCount = self.attachmentList.count + self.uploadList.count;
	self.attachmentsLabel.text = [NSString stringWithFormat:@"%d attachment%@", attachmentCount, (attachmentCount != 1) ? @"s" : @""];

	// now resize the entire view
	self.attachmentsView.height = theBottom + kSeparatorPadding;
	if (_attachmentViewOpened) {
		self.attachmentsArrows.image = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyDown.png"];
		self.attachmentSeparator.top = self.attachmentsView.bottom;
	} else {
		self.attachmentsArrows.image = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowGreyRight.png"];
		self.attachmentSeparator.top = self.attachmentsView.top + self.attachmentsLabel.bottom + kSeparatorPadding - 2.0;
	}

	self.attachmentsView.hidden = NO;
	self.attachmentSeparator.hidden = NO;

	_attachmentsHaveChanged = false;

	return;
}

/*
- (CGFloat) measureBodyText
{
	CGSize maxSize = CGSizeMake(self.bodyView.width, 2000);
	CGSize newSize = [self.bodyView.text sizeWithFont:self.bodyView.font constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];

	return newSize.height;
}

//	if (self.referenceEmail != nil) {
//        // Replying to an existing email, split body scroll view
//        // between the text view and a webView showing the mail being
//        // replied to
//        //
//        self.bodyView.height = [self heightForTextView:self.bodyView containingString:self.bodyView.text];
//        
//		self.bodyWebview.hidden = false;
//		self.bodyWebview.top = self.bodyView.bottom + 1;
//		if (!_webviewRendered) {
//			int bodyHeight = [PKMIMEMessage bodyBlock:self.referenceEmail top:0 addToView:self.bodyWebview];
//			self.bodyWebview.height = bodyHeight;
//			self.scrollView.contentSize = CGSizeMake(self.scrollView.width, self.scrollView.top + bodyHeight);
//			_webviewRendered = true;
//		}
//		self.scrollView.contentOffset = CGPointZero;
//	} else {
        // Composing new email, just show the bodyView text view
        //
        self.bodyView.height = self.scrollView.height;

// ****
		self.bodyWebview.backgroundColor = [UIColor clearColor];
		self.bodyWebview.hidden = true;
		[self.bodyWebview removeFromSuperview];
// ****



//	}

	[self redoSendButton];
//	[self redoSignEncryptButtons];

	[self dumpAllViews];

	return;
}
*/
-(void) sendMessage
{
	FXLMIMEMessage *theMessage = [FXLMIMEMessage newMessage];
	[theMessage addFromAddress:self.from];
	[theMessage addToAddresses:self.toView.emailAddresses];
	[theMessage addCCAddresses:self.ccView.emailAddresses];
	[theMessage addBCCAddresses:self.bccView.emailAddresses];
	[theMessage addSubject:self.subjectView.textEntered];

//	switch ([AppSettings emailBodyType]) {
//		default:
//		case emailBodyText:
//			[theMessage addPlainTextPart:[self getPlainText:self.bodyView.text]];
//			break;
//		case emailBodyHTML:
//			[theMessage addHTMLTextPart:[self getHTMLText:self.bodyView.text]];
//			break;
//		case emailBodyTextHTML:
//			[theMessage addPlainTextPart:[self getPlainText:self.bodyView.text]];
//			[theMessage addHTMLTextPart:[self getHTMLText:self.bodyView.text]];
//			break;
//	}
    switch ([AppSettings emailBodyType]) {
		default:
		case emailBodyText:
			[theMessage addPlainTextPart:self.bodyView.text];
			break;
		case emailBodyHTML:
			[theMessage addHTMLTextPart:[self getHTMLText:self.bodyView.text]];
			break;
		case emailBodyTextHTML:
			[theMessage addPlainTextPart:self.bodyView.text];
			[theMessage addHTMLTextPart:self.bodyView.text];
			break;
	}

	if (self.attachmentList.count > 0) {
		for (ASAttachment *theAttachment in self.attachmentList) {
			NSString *filePath = [AttachmentManager attachmentPathWithAttachment:theAttachment email:self.referenceEmail];
			[theMessage addAttachmentPath:filePath name:theAttachment.name];
		}
	}

	if (self.uploadList.count > 0) {
		for (NSString *thePath in self.uploadList) {
			[theMessage addAttachmentPath:thePath name:[thePath lastPathComponent]];
		}
	}

	if (self.shouldEncrypt) {
		// make sure all recipients are signed
		if (![self allAddressesDigitallySigned]) {
			[ErrorAlert alert:@"Cannot encrypt message" message:@"Some recipients do not have certificates. Encryption has been disabled"];
			return;
		}

		theMessage.encryptMessage = true;
	}
        
    if (self.shouldDigitallySign) {
		theMessage.signMessage = true;
	}

	[theMessage queueMessage:self.draftEmail];

	return;
}

- (void) redoSendButton
{
	// check on our buttons now
	BOOL enableSendButton = NO;
    
    if (self.networkReachable && ((self.toView.emailAddresses.count > 0) || (self.ccView.emailAddresses.count > 0))) {
        enableSendButton = YES;
	}
	self.sendButton.enabled = enableSendButton;
}

- (void) redoSignEncryptButtons
{
	BOOL hasCertificate = [FXMEmailSecurity hasUserCertificate];
	if (!hasCertificate) {
		self.navItem.rightBarButtonItem = self.sendButton;
		return;
	}

	NSMutableArray *barButtonItems = [NSMutableArray arrayWithCapacity:3];
	[barButtonItems addObject:self.sendButton];

	// icons should show the existing state..
	// ie, shouldDigitallySign -> sign
	if (self.shouldDigitallySign) {
		[barButtonItems addObject:self.signCertButton];
		if (self.shouldEncrypt) {
			self.lockCertButton.enabled = true;
			[barButtonItems addObject:self.lockCertButton];
		} else {
			self.lockCertButton.enabled = true;
			[barButtonItems addObject:self.unlockCertButton];
		}
	} else {
		[barButtonItems addObject:self.unsignCertButton];
		self.lockCertButton.enabled = false;
		[barButtonItems addObject:self.lockCertButton];
	}
//		if (self.shouldEncrypt) {
//			[barButtonItems addObject:self.lockCertButton];
//		} else {
//			[barButtonItems addObject:self.unlockCertButton];
//		}

	self.navItem.rightBarButtonItems = barButtonItems;

	self.toView.signable = self.shouldDigitallySign;
	self.ccView.signable = self.shouldDigitallySign;
	self.bccView.signable = self.shouldDigitallySign;
	self.toView.encryptable = self.shouldEncrypt;
	self.ccView.encryptable = self.shouldEncrypt;
	self.bccView.encryptable = self.shouldEncrypt;

	// now do a check here
//	BOOL allAddressesSigned = [self allAddressesDigitallySigned];

//	if ((!allAddressesSigned) && (self.shouldEncrypt)) {
//		// can't allow this - disable encryption but put up a warning
//		[ErrorAlert alert:@"Cannot encrypt message" message:@"Some recipients do not have certificates. Encryption has been disabled"];
//		self.shouldEncrypt = false;
//	}
//
	// icons should show the existing state..
	// ie, shouldEncrypt -> Lock
//	if (!allAddressesSigned) {
//		[barButtonItems addObject:self.unlockCertButton];
//		self.lockCertButton.enabled = false;
//		self.unlockCertButton.enabled = false;
//	} else {
//		self.lockCertButton.enabled = true;
//		self.unlockCertButton.enabled = true;
//		if (self.shouldEncrypt) {
//			[barButtonItems addObject:self.lockCertButton];
//		} else {
//			[barButtonItems addObject:self.unlockCertButton];
//		}
//	}
//	self.navItem.rightBarButtonItems = barButtonItems;

	return;
}

- (NSString *) getPlainText:(NSString *)inBody
{
	NSMutableString *theBody = [NSMutableString stringWithString:inBody];

	// check our webviewRendered flag since that is editable now
	if (self.webviewRendered) {
		NSString *webBody = [self.bodyWebview bodyTextInQuoteStyle];
		[theBody appendString:webBody];
	}

	NSString *theSignature = [AppSettings emailSignature];
    NSString *actualSig = [theSignature stringByReplacingOccurrencesOfString:@" " withString:@""];
	if (actualSig.length > 0) {
		// make sure the signature ends with a new-line
		if (![theSignature endsWithNewLine])
            theSignature = [theSignature stringByAppendingString:@"\n"];

		// and make sure the signature is placed on a new line after the body
//		if (![theBody endsWithNewLine]) {
			[theBody appendFormat:@"\n--\n\n%@\n", theSignature];
//		} else {
//			[theBody appendString:theSignature];
//		}
	}

	return theBody;
}

- (NSString *) getHTMLText:(NSString *)inBody
{
	static NSString *_htmlPrefix =
		@"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">" \
		@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"></head>" \
		@"<body>" \
		@"<br>";

	static NSString *_htmlSuffix = @"<br></body></html>";

	NSString *theBody = [StringUtil expandIntoHtml:inBody];

	// check our webviewRendered flag since that is editable now:
	// -> true: we have a "reply" or "forward" message that should be re-included
	// -> false: probably just a compose
	if (self.webviewRendered) {
		NSString *quotedBodyHTML = [self.bodyWebview bodyTextInQuoteStyle];
		if (quotedBodyHTML.length > 0) {
			theBody = [theBody stringByAppendingString:quotedBodyHTML];
		}
	}

	NSString *tmpBody = [StringUtil expandIntoHtml:theBody];
	theBody = [NSString stringWithFormat:@"%@\n%@\n%@", _htmlPrefix, tmpBody, _htmlSuffix];

	return theBody;
}
/*
- (NSString *) getHTMLText:(NSString *)inBody
{
	static NSString *_htmlPrefix =
		@"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">" \
		@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"></head>" \
		@"<body>" \
		@"<br>";

	static NSString *_htmlSuffix = @"<br></body></html>";

	static NSString *_blockQuotePrefix = @"<blockquote><p>";
	static NSString *_blockQuoteSuffix = @"</p></blockquote>";

	NSString *theBody = inBody;
	NSString *theSignature = [AppSettings emailSignature];
	if (theSignature.length > 0) {
		// make sure the signature ends with a new-line
		if (![theSignature endsWithNewLine]) theSignature = [theSignature stringByAppendingString:@"\n"];

		// and make sure the signature is placed on a new line after the body
		if (![theBody endsWithNewLine]) {
			theBody = [theBody stringByAppendingFormat:@"\n%@", theSignature];
		} else {
			theBody = [theBody stringByAppendingString:theSignature];
		}
	}

	NSString *tmpBody = [StringUtil expandIntoHtml:theBody];
	NSString *htmlBody = [NSString stringWithFormat:@"<p>%@</p>", tmpBody];
	if (self.referenceEmail != nil) {
		NSString *referenceString = [PKMIMEMessage bodyAsString:self.referenceEmail.body];
		if (referenceString.length > 0) {
			theBody = [theBody stringByAppendingFormat:@"%@%@%@", _blockQuotePrefix, referenceString, _blockQuoteSuffix];
		}
	}

	NSString *htmlString = [NSString stringWithFormat:@"%@\n%@\n%@", _htmlPrefix, htmlBody, _htmlSuffix];

	return htmlString;
}
*/
- (void) determineViewMode
{
	if (self.ccView.emailAddresses.count > 0) _basicView = false;
	else if (self.bccView.emailAddresses.count > 0) _basicView = false;
	else _basicView = true;
}

- (void) handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"COMPOSE_MAIL", @"errorAlert", @"Title of error alert view generated in composeMailVC") message:anErrorMessage];
}

- (void) dismissContactList
{
	if (!self.contactsController) return;
	if (self.contactsController.view.superview == nil) return;

	[self.contactsController.view removeFromSuperview];
}

- (void) showContactList:(SZCAddressView *)inAddressView
{
	if (!self.contactsController) {
		if (IS_IPAD()) {
			self.contactsController = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPad identifier:@"FXMContactTableViewController"];
		} else {
			self.contactsController = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPhone identifier:@"FXMContactTableViewController"];
		}
	}
	NSAssert(self.contactsController != nil, @"self.contactsController != nil");
	
	UIView *theSeparator = nil;
	if (inAddressView == self.toView) theSeparator = self.toSeparator;
	else if (inAddressView == self.ccView) theSeparator = self.ccSeparator;
	else if (inAddressView == self.bccView) theSeparator = self.bccSeparator;
	
	self.contactsController.searchString = inAddressView.textEntered;
	if ([self.contactsController.tableView numberOfRowsInSection:0] == 0) {
		[self dismissContactList];
		return;
	}
	
	// ok, we have contacts, so add it if not already there
	if (self.contactsController.view.superview == nil) {
		self.contactsController.view.left = theSeparator.left;
		self.contactsController.view.top = theSeparator.bottom;
		self.contactsController.view.width = theSeparator.width;
		[self.scrollView addSubview:self.contactsController.view];
	}
	
	return;
}

- (NSString *) replyString
{
	NSDate* date = self.referenceEmail.datetime;
	
	//NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *dateString = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
    //FXDebugLog(4, date );

//	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
//	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
//	NSString* dateString = [dateFormatter stringFromDate:date];
//
//	[dateFormatter setDateStyle:NSDateFormatterNoStyle];
//	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
//	NSString* timeString = [dateFormatter stringFromDate:date];
    
    
    NSString *timeString = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterShortStyle];
    
	NSMutableString *headerString = [NSMutableString new];
    NSString *signature = [AppSettings emailSignature];
    NSString *actualSig = [signature stringByReplacingOccurrencesOfString:@" " withString:@""];
	if (actualSig.length > 0)
        headerString = [NSMutableString stringWithFormat:@"\n\n--\n\n%@", signature];
    
	if(self.referenceEmail.from != nil && [self.referenceEmail.from length] > 0) {
		[headerString appendFormat:FXLLocalizedStringFromTable(@"\n\nON_%@((date))_AT_%@((time))_%@((name))_<%@>((address))_WROTE\n", @"composeMailVC", @"String prepended to email when replying to one"), dateString, timeString, self.referenceEmail.from, self.referenceEmail.replyTo];
	} else {
		[headerString appendFormat:FXLLocalizedStringFromTable(@"\n\nON_%@((date))_AT_%@((time))_%@((name))_WROTE\n", @"composeMailVC", @"String prepended to email when replying to email without a sender email address"), dateString, timeString, self.referenceEmail.replyTo];
	}
	
    return headerString;
}

- (int) countAttachmentsNotDownloaded
{
	if (self.attachmentList.count == 0) return 0;

	int theCount = 0;
	for (ASAttachment *theAttachment in self.attachmentList) {
		if (![AttachmentManager isAttachmentDownloaded:theAttachment email:self.referenceEmail]) theCount++;
	}

	return theCount;
}

- (void) downloadRemainingAttachments
{
	for (ASAttachment *theAttachment in self.attachmentList) {
		if ([AttachmentManager isAttachmentDownloaded:theAttachment email:self.referenceEmail]) continue;
		[AttachmentManager downloadAttachment:theAttachment email:self.referenceEmail];
	}
}

- (void) startActivity:(NSString *)inMessage animated:(bool)inAnimated 
{
	if (self.activityView.superview != nil) {
		self.activityLabel.text = inMessage;
		return;
	}

	// disallow user interaction as well
	self.view.userInteractionEnabled = false;
	self.cancelButton.enabled = false;
	self.sendButton.enabled = false;

	if (inAnimated) {
		self.activityView.hidden = false;
		self.activityView.alpha = 0.0;
		self.activityLabel.text = inMessage;
		[self.activityIndicator startAnimating];
		self.activityView.center = self.view.center;
		[self.view addSubview:self.activityView];
		[self.view bringSubviewToFront:self.activityView];

		[UIView animateWithDuration:kViewModeAnimationDuration
			animations:^{
				self.activityView.alpha = 1.0;
			}
		];
	} else {
		self.activityLabel.text = inMessage;
		[self.activityIndicator startAnimating];
		self.activityView.center = self.view.center;
		self.activityView.hidden = false;
		self.activityView.alpha = 1.0;
		[self.view addSubview:self.activityView];
		[self.view bringSubviewToFront:self.activityView];
	}

	return;
}

- (void) stopActivity:(bool)inAnimated
{
	if (self.activityView.superview == nil) return;

	if (inAnimated) {
		[UIView animateWithDuration:kViewModeAnimationDuration
			animations:^{
				self.activityView.alpha = 0.0;
			}
			completion:^(BOOL inFinished) {
				[self.activityView removeFromSuperview];
				self.view.userInteractionEnabled = true;
				self.cancelButton.enabled = true;
			}
		];
	} else {
		self.view.alpha = 1.0;
		self.view.userInteractionEnabled = true;
		self.cancelButton.enabled = true;
		[self.activityView removeFromSuperview];
	}

	return;
}

- (BOOL) allAddressesDigitallySigned
{
	int totalAddresses = self.toView.emailAddresses.count + self.ccView.emailAddresses.count + self.bccView.emailAddresses.count;
	if (totalAddresses == 0) return NO;

	if ((self.toView.emailAddresses.count > 0) && (!self.toView.allAddressesHaveCertificates)) return NO;
	else if ((self.ccView.emailAddresses.count > 0) && (!self.ccView.allAddressesHaveCertificates)) return NO;
	else if ((self.bccView.emailAddresses.count > 0) && (!self.bccView.allAddressesHaveCertificates)) return NO;

	return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Keyboard

- (void) adjustScrollView:(NSNotification*)aNotification isHide:(BOOL)isHide
{
    NSDictionary* userInfo = [aNotification userInfo];
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if (UIDeviceOrientationIsLandscape(self.interfaceOrientation)) {
        keyboardSize = CGSizeMake(keyboardSize.height, keyboardSize.width);
    }
            
	if (isHide) {
		self.scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
	} else {
		self.scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height + 16, 0.0);
	}

	return;
}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
	[self adjustScrollView:aNotification isHide:FALSE];
#ifdef DEBUG
	[self dumpAllViews];
#endif
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
	[self adjustScrollView:aNotification isHide:TRUE];
#ifdef DEBUG
	[self dumpAllViews];
#endif
	[self dismissContactList];
}

- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:[[self view] window]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:[[self view] window]];
}

- (void) dismissKeyboard
{
	if (self.bodyView.isFirstResponder) [self.bodyView resignFirstResponder];
	else if (self.toView.isFirstResponder) [self.toView resignFirstResponder];
	else if (self.ccView.isFirstResponder) [self.ccView resignFirstResponder];
	else if (self.bccView.isFirstResponder) [self.bccView resignFirstResponder];
	else if (self.fromView.isFirstResponder) [self.fromView resignFirstResponder];
	else if (self.subjectView.isFirstResponder) [self.subjectView resignFirstResponder];
	else if (self.bodyWebview.isFirstResponder) [self.bodyWebview resignFirstResponder];

}

////////////////////////////////////////////////////////////////////////////////////////////
// Draft
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Draft

- (NSString*)emailAddressesAsJSON:(NSArray*)anEmailAddresses
{
    NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:anEmailAddresses.count];
    for(EmailAddress* anEmailAddress in anEmailAddresses) {
        [anArray addObject:[anEmailAddress asDictionary]];
    }
    return [anArray JSONRepresentation];
}

- (void)saveDraft
{
    ASFolder* aLocalDraftsFolder = nil;
    
    ASAccount* anAccount = (ASAccount*)self.account;
    NSArray* aFolders = [anAccount foldersForFolderType:kFolderTypeLocalDrafts];
    if(aFolders.count == 0) {
        // Create local drafts folder, this is NOT an ActiveSync folder, local storage only
        //
        aLocalDraftsFolder = (ASFolder*)[anAccount createLocalDraftsFolder];
    }else if(aFolders.count == 1) {
        aLocalDraftsFolder = [aFolders objectAtIndex:0];
    }else{
        FXDebugLog(kFXLogComposeMailVC, @"WARNING - Multiple local drafts folder");
        aLocalDraftsFolder = [aFolders objectAtIndex:0];
    }

    if(aLocalDraftsFolder) {
        ASEmail* anEmail;
        if(self.draftEmail) {
            anEmail = self.draftEmail;
        }else{
            anEmail = [[ASEmail alloc] initWithFolder:aLocalDraftsFolder];
            anEmail.uid = [anAccount getClientID];
        }
        anEmail.datetime = [NSDate date];

        // Email addressesed to and from
        anEmail.from     = anAccount.emailAddress;
        anEmail.toJSON   = [self emailAddressesAsJSON:self.toView.emailAddresses];
        
        NSArray* aCCs    = self.ccView.emailAddresses;
        if(aCCs.count > 0) {
            anEmail.ccJSON = [self emailAddressesAsJSON:self.ccView.emailAddresses];
        }
        
        NSArray* aBCCs   = self.bccView.emailAddresses;
        if(aBCCs.count > 0) {
            anEmail.bccJSON = [self emailAddressesAsJSON:self.bccView.emailAddresses];
        }

        // Subject
        anEmail.subject  = self.subjectView.textEntered;

        // Body
        NSString* aBody     = self.bodyView.text;
        const char* aBytes  = [aBody UTF8String];
        anEmail.body        = [NSData dataWithBytes:aBytes length:strlen(aBytes)];
        anEmail.bodyType    = kBodyTypePlainText;
        [anEmail setIsFetched:TRUE];
        [anEmail setIsRead:TRUE];
        
        // Body Preview
        NSRange aRange      = NSMakeRange(0, MIN(256, aBody.length));
        anEmail.preview     = [aBody substringWithRange:aRange];

        // FIXME attachements
        // anEmail.attachments
        
        if(self.referenceEmail) {
            anEmail.emailThread = [[ASEmailThread alloc] init];
            anEmail.emailThread.uid = self.referenceEmail.uid;
        }
        
        // Save to database, not synced to ActiveSync
        //
        [aLocalDraftsFolder addObject:anEmail];
        [aLocalDraftsFolder commitObjects];
    }else{
        FXDebugLog(kFXLogComposeMailVC, @"saveDraft failed, invalid local drafts folder");
    }
}

- (void) deleteDraft
{
    if(self.draftEmail) {
        ASEmail* anEmail = self.draftEmail;
        if(anEmail.uid.length > 0) {
            ASFolder* aLocalDraftsFolder = (ASFolder*)anEmail.folder;
            [aLocalDraftsFolder deleteUid:anEmail.uid updateServer:FALSE];
            [aLocalDraftsFolder commitObjects];
        }else{
            FXDebugLog(kFXLogComposeMailVC, @"deleteDraft failed, invalid uid: %@");
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Greater SafeZone Methods
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Greater SafeZone Methods

+(BOOL)urlHasEmailScheme:(NSURL *)url
{
	NSString *scheme = url.scheme;
	if (scheme && [scheme isEqualToString:@"mailto"]) {
		return YES;
	}
	return NO;
}

-(void)applyEmailSchemeURL:(NSURL *)url
{
	NSString *scheme = url.scheme;
	if (scheme && [scheme isEqualToString:@"mailto"]) {
		
		NSMutableArray *toAddresses = [NSMutableArray array];
		// This will only parse a basic URL with one email - other options include mailto:foo@example.com?cc=bar@example.com&subject=Greetings%20from%20Cupertino!&body=Wish%20you%20were%20here!
		// mailto:frank@wwdcdemo.example.com
		
		
		NSArray *addresses = [[url.absoluteString stringByReplacingOccurrencesOfString:@"mailto:" withString:@""] componentsSeparatedByString:@"?"];
		
		for (NSString *address in addresses) {
			// parse it properly
			// for now: assuming one simple email
			[toAddresses addObject:address];
		}
		
		self.toList = toAddresses;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Debug

- (void) dumpAllViews
{
	FXDebugLog(kFXLogComposeMailVC, @"===============================");
	[self debugView:self.scrollView title:@"ScrollView"];
	FXDebugLog(kFXLogComposeMailVC, @"ContentSize: w:%.0f h:%.0f", self.scrollView.contentSize.width, self.scrollView.contentSize.height);

	[self debugView:self.fromView title:@"From"];
	[self debugView:self.toView title:@"To"];
	[self debugView:self.ccView title:@"Cc"];
	[self debugView:self.bccView title:@"Bcc"];
	[self debugView:self.subjectView title:@"Subject"];
	[self debugView:self.attachmentButton title:@"AttachmentButton"];
	[self debugView:self.attachmentsView title:@"Attachments"];
	[self debugView:self.bodyView title:@"Body"];
	[self debugView:self.bodyWebview title:@"Web Body"];
}

- (void) debugView:(UIView *)inView title:(NSString *)inTitle
{
	bool isResponder = false;
	if ([inView respondsToSelector:@selector(isFirstResponder)]) isResponder = ((UIResponder *)inView).isFirstResponder;

	FXDebugLog(kFXLogComposeMailVC, [NSString stringWithFormat:@"%@: y:%.0f x:%.0f w:%.0f h:%.0f alpha:%.1f hidden:%@ responder:%@",
		inTitle, inView.top, inView.left, inView.width, inView.height, inView.alpha, inView.hidden ? @"Yes" : @"No", isResponder ? @"Yes" : @"No"]);
}

@end
