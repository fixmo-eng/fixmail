//
//  SZCImagePreviewController.h
//  SafeZone
//
//  Created by Colin Biggin on 13-06-19.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "SZCFilePreviewController.h"

@interface SZCImagePreviewController : SZCFilePreviewController <UIScrollViewDelegate>

+ (SZCImagePreviewController *) createImagePreviewer;

@end
