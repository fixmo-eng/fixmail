//
//  AttachmentManager.m
//  FixMail
//
//  Created by Colin Biggin on 2013-04-05.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//
#import "AttachmentManager.h"
#import "ASAccount.h"
#import "ASAttachment.h"
#import "BaseAccount.h"
#import "ASEmail.h"

@interface attachmentDownload  : NSObject
@property (nonatomic, strong) ASAttachment *attachment;
@property (nonatomic, strong) ASItemOperations *itemOperation;
@property (nonatomic, strong) ASEmail *email;
@property (nonatomic, assign) int percentDownloaded;
@end

@implementation attachmentDownload
@end

#pragma mark -

static NSMutableDictionary *sCurrentDownloads = nil;

@implementation AttachmentManager

+ (AttachmentManager *) sharedAttachmentManager
{
	static AttachmentManager *_sharedInstance;
	if(!_sharedInstance) {
		static dispatch_once_t oncePredicate;
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
		});
	}

	return _sharedInstance;
}

+ (NSString *) attachmentsPathWithEmail:(ASEmail *)inEmail
{
	NSString *thePathName = [FXLSafeZone attachmentsDirectory];

	if (inEmail != nil) {
		NSString *emailDirectory = [inEmail.uid stringByReplacingOccurrencesOfString:@":" withString:@"_"];
		thePathName = [thePathName stringByAppendingPathComponent:emailDirectory];
	}

	BOOL isDirectory = FALSE;
	if (![[FXLSafeZone secureFileManager] fileExistsAtPath:thePathName isDirectory:&isDirectory]) {
		NSError *theError;
		[[FXLSafeZone secureFileManager] createDirectoryAtPath:thePathName withIntermediateDirectories:TRUE attributes:nil error:&theError];
	}

	return thePathName;
}

+ (BOOL) isAttachmentDownloaded:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	// to check if it exists, check if filesize is equal to estimated size
	// BUG: estimated size seems to be up to 64 bytes off

	NSString *attachmentPath = [AttachmentManager attachmentPathWithAttachment:inAttachment email:inEmail];
	if (![[FXLSafeZone secureFileManager] fileExistsAtPath:attachmentPath]) return NO;

	// it exists, find it's size
	NSDictionary *fileAttributes = [[FXLSafeZone secureFileManager] attributesOfItemAtPath:attachmentPath error:nil];
	unsigned long fileSize = [[fileAttributes objectForKey:NSFileSize] unsignedLongValue];
	if (fileSize == inAttachment.estimatedDataSize) return YES;

	// *** bug fix here ***
	float lowerBound = inAttachment.estimatedDataSize - 128;
	if (lowerBound < 1) lowerBound = 1;
	float upperBound = inAttachment.estimatedDataSize;

	if ((fileSize >= lowerBound) && (fileSize <= upperBound)) {
		return YES;
	}

	return NO;
}

+ (BOOL) isAttachmentDownloading:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	if (sCurrentDownloads.count == 0) return NO;

	if ([AttachmentManager getDownloadForAttachment:inAttachment] != nil) return YES;

	return NO;
}

+ (unsigned long) percentDownloaded:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	unsigned long thePercentage = 0;
	attachmentDownload *theDownload = [AttachmentManager getDownloadForAttachment:inAttachment];
	if (theDownload != nil) {
		thePercentage = theDownload.percentDownloaded;
	}

	return thePercentage;
}

+ (void) downloadAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	if ([AttachmentManager isAttachmentDownloaded:inAttachment email:inEmail]) return;
	if ([AttachmentManager isAttachmentDownloading:inAttachment email:inEmail]) return;

	// ok, start it up
	if (sCurrentDownloads == nil) {
		sCurrentDownloads = [[NSMutableDictionary alloc] initWithCapacity:5];
	}

	NSString *attachmentPath = [AttachmentManager attachmentPathWithAttachment:inAttachment email:inEmail];
    ASItemOperations* anItemOp = [ASItemOperations sendRequest:(ASAccount *)[BaseAccount currentLoggedInAccount]
                                                 fileReference:inAttachment.fileReference
                                                      filePath:attachmentPath
                                                  expectedSize:inAttachment.estimatedDataSize
                                                      delegate:[AttachmentManager sharedAttachmentManager]
                                  ];

	attachmentDownload *theDownload = [[attachmentDownload alloc] init];
	theDownload.attachment = inAttachment;
	theDownload.itemOperation = anItemOp;
	theDownload.email = inEmail;
	theDownload.percentDownloaded = 0;

	NSString *attachmentFileName = [AttachmentManager attachmentFileName:inAttachment];
	[sCurrentDownloads setObject:theDownload forKey:attachmentFileName];

	NSDictionary *userDictionary = @{
			kAttachmentManagerASAttachmentUserInfo : theDownload.attachment,
			kAttachmentManagerEmailUserInfo : theDownload.email,
			kAttachmentManagerASItemOperationUserInfo : theDownload.itemOperation
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kAttachmentManagerStartedNotification object:nil userInfo:userDictionary];
}

+ (void) cleanupAttachmentFolderForEmail:(ASEmail *)inEmail
{
	NSString *thePath = [AttachmentManager attachmentsPathWithEmail:inEmail];

	NSError *theError;
	[[FXLSafeZone secureFileManager] removeItemAtPath:thePath error:&theError];
}

+ (NSData *) dataWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	NSString *attachmentPath = [AttachmentManager attachmentPathWithAttachment:inAttachment email:inEmail];
	if (![[FXLSafeZone secureFileManager] fileExistsAtPath:attachmentPath]) return nil;

	return [[FXLSafeZone secureFileManager] contentsAtPath:attachmentPath];
}

+ (void) deleteAllAttachments
{
	NSString* attachmentPath = [FXLSafeZone attachmentsDirectory];
	
	[[FXLSafeZone secureFileManager] removeItemAtPath:attachmentPath error:nil];
}

#pragma mark - Internal routines

+ (NSString *) attachmentFileName:(ASAttachment *)inAttachment
{
	NSParameterAssert(inAttachment != nil);

	NSString *fnExtension = [inAttachment.name pathExtension];
	NSString *fnName = [[inAttachment.name lastPathComponent] stringByDeletingPathExtension];

	NSString *attachmentUID = [inAttachment.fileReference stringByReplacingOccurrencesOfString:@":" withString:@"_"];
	attachmentUID = [attachmentUID stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
	attachmentUID = [attachmentUID stringByReplacingOccurrencesOfString:@"%3a" withString:@"_"];
	fnName = [fnName stringByAppendingFormat:@"--%@--", attachmentUID];

	return [fnName stringByAppendingPathExtension:fnExtension];
}

+ (NSString *) attachmentPathWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail
{
	NSParameterAssert(inAttachment != nil);
	NSParameterAssert(inEmail != nil);

	NSString *theFileName = [AttachmentManager attachmentFileName:inAttachment];
	return [[AttachmentManager attachmentsPathWithEmail:inEmail] stringByAppendingPathComponent:theFileName];
}

+ (attachmentDownload *) getDownloadForAttachment:(ASAttachment *)inAttachment
{
	NSArray *theDownloads = [sCurrentDownloads allValues];
	for (attachmentDownload *theDownload in theDownloads) {
		if (theDownload.attachment == inAttachment) return theDownload;
	}

	return nil;
}

+ (attachmentDownload *) getDownloadForItemOperation:(ASItemOperations *)inItemOperation
{
	NSArray *theDownloads = [sCurrentDownloads allValues];
	for (attachmentDownload *theDownload in theDownloads) {
		if (theDownload.itemOperation == inItemOperation) return theDownload;
	}

	return nil;
}

+ (void) removeDownload:(attachmentDownload *)inDownload
{
	if (inDownload == nil) return;

	// the 'key' is the filename of the attachment
	NSString *attachmentFileName = [AttachmentManager attachmentFileName:inDownload.attachment];
	[sCurrentDownloads removeObjectForKey:attachmentFileName];
}

#pragma mark - ASItemOperationsDelegate

- (void) deliverProgress:(NSString*)inMessage itemOp:(ASItemOperations *)inItemOperation
{
	FXDebugLog(kFXLogAttachmentManager, @"AttachmentManager (deliverProgress): %@", inMessage);

	attachmentDownload *theDownload = [AttachmentManager getDownloadForItemOperation:inItemOperation];
	if (theDownload == nil) return;

	// message comes in as a percentage
	theDownload.percentDownloaded = inMessage.intValue;
	NSDictionary *userDictionary = @{
			kAttachmentManagerPercentageUserInfo : [NSNumber numberWithInt:theDownload.percentDownloaded],
			kAttachmentManagerASAttachmentUserInfo : theDownload.attachment,
			kAttachmentManagerEmailUserInfo : theDownload.email,
			kAttachmentManagerASItemOperationUserInfo : theDownload.itemOperation
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kAttachmentManagerProgressNotification object:nil userInfo:userDictionary];
}

- (void) deliverError:(NSString*)inMessage itemOp:(ASItemOperations *)inItemOperation
{
	FXDebugLog(kFXLogAttachmentManager, @"AttachmentManager (deliverError): %@", inMessage);

	attachmentDownload *theDownload = [AttachmentManager getDownloadForItemOperation:inItemOperation];
	if (theDownload == nil) return;

	NSDictionary *userDictionary = @{
			kAttachmentManagerASAttachmentUserInfo : theDownload.attachment,
			kAttachmentManagerEmailUserInfo : theDownload.email,
			kAttachmentManagerASItemOperationUserInfo : theDownload.itemOperation,
            kAttachmentManagerErrorMessage: inMessage
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kAttachmentManagerErrorNotification object:nil userInfo:userDictionary];

	[AttachmentManager removeDownload:theDownload];
}

- (void) deliverFile:(NSString*)aPath itemOp:(ASItemOperations*)inItemOperation
{
	FXDebugLog(kFXLogAttachmentManager, @"AttachmentManager (deliverFile): %@", aPath);

	attachmentDownload *theDownload = [AttachmentManager getDownloadForItemOperation:inItemOperation];
	if (theDownload == nil) return;

	NSDictionary *userDictionary = @{
			kAttachmentManagerASAttachmentUserInfo : theDownload.attachment,
			kAttachmentManagerEmailUserInfo : theDownload.email,
			kAttachmentManagerASItemOperationUserInfo : theDownload.itemOperation
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kAttachmentManagerFinishedNotification object:nil userInfo:userDictionary];

	[AttachmentManager removeDownload:theDownload];
}

- (void) deliverData:(NSData *)aData itemOp:(ASItemOperations *)inItemOperation
{
	FXDebugLog(kFXLogAttachmentManager, @"AttachmentManager (deliverData): %ld bytes", aData.length);

	attachmentDownload *theDownload = [AttachmentManager getDownloadForItemOperation:inItemOperation];
	if (theDownload == nil) return;

	return;
}

@end
