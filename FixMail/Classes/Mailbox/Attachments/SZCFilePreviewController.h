//
//  SZCFilePreviewController.h
//  SafeZone
//
//  Created by Colin Biggin on 13-06-18.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "SZLAttachmentBrowserController.h"

@interface SZCFilePreviewController : UIViewController <SZLAttachmentBrowserDelegate>

@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSString *saveFileName;
@property (nonatomic, assign) BOOL showSaveButton;

+ (SZCFilePreviewController *) createPreviewer:(NSString *)inFilePath name:(NSString *)inSaveFileName passPhrase:(NSString *)inPassPhrase;
- (void) previewFile;

@end
