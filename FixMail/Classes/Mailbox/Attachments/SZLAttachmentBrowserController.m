//
//  SZLFileBrowserController.m
//  SafeZone
//
//  Created by Leena Mansour on 12-06-17.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZLAttachmentBrowserController.h"
#import "AttachmentUtils.h"

#define PL_MAX_HEIGHT 480

NSString * const kSZLAttachmentCancelButton = @"Cancel";
NSString * const kSZLAttachmentOkButton = @"OK";

@interface SZLAttachmentBrowserController ()
{
    NSMutableArray *parentFolder;
    NSMutableArray *currentFolder;
    NSMutableArray *folders;
    NSString *userPath;
}

@property (assign, nonatomic) BOOL folderBrowserFlag;
@property (strong, nonatomic) NSString *latestPath;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UILabel *filePathLabel;

-(void)setLeftBarButtonItem;
-(NSMutableArray *) loadChildrenFromPath:(NSString *)path;
-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil path:(NSString *)path;
@end

@implementation SZLAttachmentBrowserController
@synthesize tableView = _tableView;
@synthesize delegate;
@synthesize latestPath;

+ (SZLAttachmentBrowserController *) launchFileBrowserFromPath:(NSString *)browsingPath
{
    SZLAttachmentBrowserController *fileBrowser = [[SZLAttachmentBrowserController alloc] initWithNibName:@"SZLAttachmentBrowserController" bundle:[FXLSafeZone getResourceBundle]  path:browsingPath];
	fileBrowser.folderBrowserFlag = false;
    return fileBrowser;
}

+ (SZLAttachmentBrowserController *) launchFolderBrowserFromPath:(NSString *)browsingPath;
{
    SZLAttachmentBrowserController *fileBrowser = [[SZLAttachmentBrowserController alloc] initWithNibName:@"SZLAttachmentBrowserController" bundle:[FXLSafeZone getResourceBundle]  path:browsingPath];
	fileBrowser.folderBrowserFlag = true;
    return fileBrowser;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil path:(NSString *)path
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        userPath = path;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentFolder = [[NSMutableArray alloc] initWithCapacity:5];
    parentFolder = nil;
    folders = [[NSMutableArray alloc] initWithCapacity:5];
    currentFolder = nil;

	self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 24.0)];

	self.filePathLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, self.tableView.frame.size.width-10.0, 20.0)];
	self.filePathLabel.backgroundColor = [UIColor darkGrayColor];
	self.filePathLabel.layer.cornerRadius = 4.0;
	self.filePathLabel.font = [UIFont boldSystemFontOfSize:10];
	self.filePathLabel.textAlignment = NSTextAlignmentLeft;
	self.filePathLabel.textColor = [UIColor whiteColor];
	self.filePathLabel.minimumScaleFactor = 0.5;
	self.filePathLabel.lineBreakMode = NSLineBreakByTruncatingHead;
	self.filePathLabel.autoresizingMask = UIViewAutoresizingNone;
	[self.headerView addSubview:self.filePathLabel];

	self.tableView.tableHeaderView = self.headerView;
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    [currentFolder removeAllObjects];
    [super viewWillAppear:animated];

	self.navigationController.navigationBarHidden = NO;

    self.latestPath = userPath;
    currentFolder = [self loadChildrenFromPath:userPath];

	if (self.folderBrowserFlag) {
		[self setRightBarButtonItemWithTitle:kSZLAttachmentOkButton];
	} else {
		[self setRightBarButtonItemWithTitle:kSZLAttachmentCancelButton];
	}
    //    [self setToolbarItems];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [currentFolder count];        
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0f;
}


//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 24.0f;
//}
//
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//        cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
//        cell.textLabel.backgroundColor = [UIColor clearColor];
//        cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    }
    
    cell.textLabel.text = nil;
    NSString *fileName = [currentFolder objectAtIndex:indexPath.row];

    cell.textLabel.text = fileName;
    
    NSString *itemPath = [self constructFullPath:fileName];
    
    if ([self.latestPath isEqualToString:itemPath]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    //if folder, add a >
    BOOL isDir;
    if ([[FXLSafeZone secureFileManager] fileExistsAtPath:itemPath isDirectory:&isDir] && isDir) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
		cell.imageView.image = [UIImage imageNamed:@"FixMailRsrc.bundle/folderIcon.png"];
		cell.detailTextLabel.text = @"";
    } else {
		cell.imageView.image = [AttachmentUtils documentIconForFile:fileName];
		// it exists, find it's size
		NSDictionary *fileAttributes = [[FXLSafeZone secureFileManager] attributesOfItemAtPath:itemPath error:nil];
		unsigned long fileSize = [[fileAttributes objectForKey:NSFileSize] unsignedLongValue];
		cell.detailTextLabel.text = [NSString stringWithFormat:@"Size: %@", [AttachmentUtils fileSizeString:fileSize]];
	}
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 18.0f)];
    return imageView;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *item = [currentFolder objectAtIndex:indexPath.row];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    BOOL isDir;
    NSString *itemPath = [self constructFullPath:item];
    if ([[FXLSafeZone secureFileManager] fileExistsAtPath:itemPath isDirectory:&isDir] && isDir)
    {
        parentFolder = currentFolder;
        [folders addObject:itemPath];
        currentFolder = nil;
        [tableView reloadData];
        currentFolder = [self loadChildrenFromPath:itemPath];
        [tableView reloadData];
    } else {
		if (!self.folderBrowserFlag) {
			if ([cell accessoryType] == UITableViewCellAccessoryCheckmark) {
				[cell setAccessoryType:UITableViewCellAccessoryNone];
				[self setRightBarButtonItemWithTitle:kSZLAttachmentCancelButton];
				self.latestPath = [folders lastObject];
			} else {
				self.latestPath = itemPath;
				[tableView reloadData];
				[self setRightBarButtonItemWithTitle:kSZLAttachmentOkButton];
			}
		}

        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSLog(@"Choosing Path: %@", self.latestPath);
        
    }
}

-(void)traverseUp:(id)sender
{
    NSString *title = nil;
    if (parentFolder) {
        currentFolder = nil;
        currentFolder = parentFolder;
        [folders removeLastObject];
        parentFolder = nil;
        if ([folders lastObject]) {
            title = [[folders lastObject] lastPathComponent];
			[self redoPathLabel:[folders lastObject]];
        } else {
            title = [userPath lastPathComponent];
			[self redoPathLabel:userPath];
        }
    } else {
        if ([folders lastObject] && ([folders count] > 1)) {
            [folders removeLastObject];
            currentFolder = [self loadChildrenFromPath:[folders lastObject]];
            title = [[folders lastObject] lastPathComponent];
			[self redoPathLabel:[folders lastObject]];
        } else {
            currentFolder = [self loadChildrenFromPath:userPath];
            parentFolder = nil;
            title = [userPath lastPathComponent];
			[self redoPathLabel:userPath];
            [folders removeLastObject];
        }
    }
    self.title = [title capitalizedString];

    [self setLeftBarButtonItem];
    [self.tableView reloadData];
    [self.tableView setNeedsDisplay];
}

-(void)setLeftBarButtonItem
{
    if ([folders count] && ([folders count] > 1)) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[[folders objectAtIndex:[folders count]-1] lastPathComponent] style:UIBarButtonItemStyleDone target:self action:@selector(traverseUp:)];
    } else if ([folders count]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[userPath lastPathComponent] style:UIBarButtonItemStylePlain target:self action:@selector(traverseUp:)];
    } else {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

-(void)setRightBarButtonItemWithTitle:(NSString *)title
{
    if ([title isEqualToString:kSZLAttachmentCancelButton]) {
        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithTitle:kSZLAttachmentCancelButton style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
        self.navigationItem.rightBarButtonItem = cancel;
    } else if ([title isEqualToString:kSZLAttachmentOkButton]) {
        UIBarButtonItem *select = [[UIBarButtonItem alloc] initWithTitle:kSZLAttachmentOkButton style:UIBarButtonItemStylePlain target:self action:@selector(itemSelected:)];
        self.navigationItem.rightBarButtonItem = select;
    }
}

//-(void)setToolbarItems
//{
//    UIBarButtonItem *addFolder = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFolder:)];
//    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *select = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStylePlain target:self action:@selector(itemSelected:)];
//    
//    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpace, select, nil]];
//    [[self navigationController] setToolbarHidden:NO animated:NO];
//}

-(void)cancel:(id)sender
{
    [[self delegate] didCancel:self];
}

- (void)itemSelected:(id)sender
{
    [[self delegate] didSelectItem:self path:self.latestPath];
}

#pragma mark Information gathering methods

- (void) redoPathLabel:(NSString *)inPath
{
	NSRange theRange = [inPath rangeOfString:@"Documents"];
	NSString *theText = @"";
	if (theRange.length > 0) {
		theText = [inPath substringFromIndex:theRange.location];
	} else {
		theText = inPath;
	}
	self.filePathLabel.text = [@"  " stringByAppendingString:theText];
	if (self.folderBrowserFlag) self.latestPath = inPath;
}

- (BOOL) isValidFileOrFolder:(NSString *)inPath
{
	NSString *fileName = [inPath lastPathComponent];
	if ([fileName caseInsensitiveCompare:@"settings"] == NSOrderedSame) return NO;
	if ([fileName caseInsensitiveCompare:@"attachments"] == NSOrderedSame) return NO;
	if ([fileName caseInsensitiveCompare:@".DS_Store"] == NSOrderedSame) return NO; // probably mac only

#if TARGET_IPHONE_SIMULATOR || FIXMAIL_CONTAINER
	// this folder should only exist in testing
	if ([fileName caseInsensitiveCompare:@"Certificates"] == NSOrderedSame) return NO;
#endif

	NSString *fileSuffix = [inPath pathExtension];
	if ([fileSuffix caseInsensitiveCompare:@"tdb"] == NSOrderedSame) return NO;
	if ([fileSuffix caseInsensitiveCompare:@"sqlite3"] == NSOrderedSame) return NO;
	if ([fileSuffix caseInsensitiveCompare:@"plist"] == NSOrderedSame) return NO;

	return YES;
}

- (NSMutableArray *) loadChildrenFromPath:(NSString *)path
{ 
    NSError *error = nil;
    
    NSArray *paths = [[FXLSafeZone secureFileManager] contentsOfDirectoryAtPath:path error:&error];
    NSMutableArray *newPaths = [NSMutableArray array];
    
    for (NSString* tempPath in paths) {
//		if (self.folderBrowserFlag) {
//			BOOL isDir;
//			[[FXLSafeZone secureFileManager] fileExistsAtPath:[path stringByAppendingPathComponent:tempPath] isDirectory:&isDir];
//			if (!isDir) continue;
//		}
		if ([self isValidFileOrFolder:tempPath]) {
			[newPaths addObject:tempPath];
		}
    }

	[self redoPathLabel:path];
    self.title = [[path lastPathComponent] capitalizedString];
    
    [self setLeftBarButtonItem];
    return newPaths;    
}

- (NSString *) constructFullPath:(NSString *)item
{
    NSString *path;    
    if (folders.count > 0) {
		path = (NSString *)folders[folders.count-1];
    } else {
        path = userPath;
    }
    path = [path stringByAppendingPathComponent:item];
    return path;
}

@end
