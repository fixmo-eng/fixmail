//
//  SZCFilePreviewController.m
//  SafeZone
//
//  Created by Colin Biggin on 13-06-18.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "SZCFilePreviewController.h"
#import "SZCCertPreviewController.h"
#import "SZCImagePreviewController.h"
#import "SZCTextPreviewController.h"
#import "SZCWebPreviewController.h"
#import "AttachmentUtils.h"
#import "UIAlertView+Modal.h"

@implementation SZCFilePreviewController

+ (SZCFilePreviewController *) createPreviewer:(NSString *)inFilePath name:(NSString *)inSaveFileName passPhrase:(NSString *)inPassPhrase
{
	NSParameterAssert(inFilePath != nil);
	NSParameterAssert(inSaveFileName != nil);

	SZCFilePreviewController *theController = nil;
	switch ([AttachmentUtils documentTypeForFile:inFilePath]) {
		case documentTypeImage:
			theController = [SZCImagePreviewController createImagePreviewer];
			break;
		case documentTypeCode:
			theController = [SZCTextPreviewController createCodePreviewer];
			break;
		case documentTypeText:
			theController = [SZCTextPreviewController createTextPreviewer];
			break;
		case documentTypeCert:
			if ([[inSaveFileName pathExtension] caseInsensitiveCompare:@"pem"] == NSOrderedSame) {
				theController = [SZCCertPreviewController createPEMPreviewer:inPassPhrase];
			} else {
				theController = [SZCCertPreviewController createP12Previewer:inPassPhrase];
			}
			break;
		default:
			theController = [SZCWebPreviewController createWebPreviewer];
			break;
	}

	theController.filePath = inFilePath;
	theController.saveFileName = inSaveFileName;

	return theController;
}

- (void) viewDidLoad
{
    [super viewDidLoad];

	if (self.showSaveButton) {
		UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
																				   target:self
																				   action:@selector(toggleSave:)
									  ];
		self.navigationItem.rightBarButtonItem = barButton;
	}

	return;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	self.navigationController.navigationBarHidden = false;

    [self previewFile];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD()) {
        return YES;
    } else {
        return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
    }
}

- (void) previewFile
{
}

- (IBAction) toggleSave:(id)inSender
{
	NSString *documentsDirectory = [FXLSafeZone documentsDirectory];
	SZLAttachmentBrowserController *theController = [SZLAttachmentBrowserController launchFolderBrowserFromPath:documentsDirectory];
	theController.delegate = self;
	[self.navigationController pushViewController:theController animated:YES];
}

#pragma mark - SZLAttachmentBrowserDelegate methods

- (void) didCancel:(SZLAttachmentBrowserController *)inAttachmentBrowserController
{
	NSParameterAssert(inAttachmentBrowserController != nil);

	[self.navigationController popViewControllerAnimated:YES];
}

- (void) didSelectItem:(SZLAttachmentBrowserController *)inAttachmentBrowserController path:(NSString *)inFilePath
{
	NSParameterAssert(inAttachmentBrowserController != nil);
	NSParameterAssert(inFilePath.length > 0);

	// copy it over
	NSString *fromPath = self.filePath;
	NSString *toPath = [inFilePath stringByAppendingPathComponent:self.saveFileName];

	// have to check if the file name already exists and ask to replace existing or rename it with
	// a numbered extension, eg, text.doc -> text(1).doc -> text(2).doc
	if ([[FXLSafeZone secureFileManager] fileExistsAtPath:toPath]) {
		NSString *replacementPath = [AttachmentUtils createUniquePath:inFilePath name:self.saveFileName];
		UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_TITLE", @"mailViewerVC", @"Duplicate filename found title")
															   message:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_QUERY", @"mailViewerVC", @"Asking user to replace or rename the duplicate filename")
															  delegate:self
													 cancelButtonTitle:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_CANCEL", @"mailViewerVC", @"Cancel Option of the duplicate filename")
													 otherButtonTitles:FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_REPLACE", @"mailViewerVC", @"Replace Option of the duplicate filename"),
																	   FXLLocalizedStringFromTable(@"DUPLICATE_FILENAME_RENAME", @"mailViewerVC", @"Rename Option of the duplicate filename"), nil
									 ];
		int indexClicked = [theAlertView showModal];
		if (indexClicked != theAlertView.cancelButtonIndex) {
			if (indexClicked == 1) {
				[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:toPath error:nil];
			} else {
				[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:replacementPath error:nil];
			}
		}
	} else {
		[[FXLSafeZone secureFileManager] copyItemAtPath:fromPath toPath:toPath error:nil];
	}

	[self.navigationController popViewControllerAnimated:YES];
}

@end
