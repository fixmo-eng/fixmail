//
//  SZCImagePreviewController.m
//  SafeZone
//
//  Created by Colin Biggin on 13-06-19.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "SZCImagePreviewController.h"

@interface SZCImagePreviewController ()
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@end

@implementation SZCImagePreviewController

+ (SZCImagePreviewController *) createImagePreviewer
{
	SZCImagePreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCImagePreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCImagePreviewController"];
	}
	theController.showSaveButton = YES;

	return theController;
}

- (void) previewFile
{
	UIImage *theImage = nil;
    if (self.filePath.length > 0) {
        NSData *fileData = [[FXLSafeZone secureFileManager] contentsAtPath:self.filePath];
		theImage = [UIImage imageWithData:fileData];
    }
	self.imageView.image = theImage;

    return;
}

#pragma mark UIScrollViewDelegate methods

- (UIView *) viewForZoomingInScrollView:(UIScrollView *)inScrollView
{
	return self.imageView;
}

@end
