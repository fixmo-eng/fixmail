//
//	SZCTextPreviewController.m
//  SafeZone
//
//  Created by Colin Biggin on 13-06-19.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "SZCTextPreviewController.h"

#define kMinimumTextFontSize		8
#define kMaximumTextFontSize		48
#define kCodeFontFamily				@"Courier"

@interface SZCTextPreviewController ()
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, strong) UISegmentedControl *fontControl;
@property (nonatomic, assign) int textFontSize;
@property (nonatomic, assign) bool isCode;
@end

@implementation SZCTextPreviewController

+ (SZCTextPreviewController *) createTextPreviewer
{
	SZCTextPreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCTextPreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCTextPreviewController"];
	}
	theController.showSaveButton = YES;
	theController.textFontSize = 16;
	theController.isCode = false;

	return theController;
}

+ (SZCTextPreviewController *) createCodePreviewer
{
	SZCTextPreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCTextPreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCTextPreviewController"];
	}
	theController.showSaveButton = YES;
	theController.textFontSize = 14;
	theController.isCode = true;

	return theController;
}

- (void) viewDidLoad
{
	[super viewDidLoad];

	self.fontControl = [[UISegmentedControl alloc] initWithItems:
						[NSArray arrayWithObjects:
							[UIImage imageNamed:@"FixMailRsrc.bundle/decreaseFont.png"],
							[UIImage imageNamed:@"FixMailRsrc.bundle/increaseFont.png"],
						 nil]];
	[self.fontControl addTarget:self action:@selector(toggleFontChange:) forControlEvents:UIControlEventValueChanged];
	self.fontControl.frame = CGRectMake(0, 0, 90, 30.0);
	self.fontControl.segmentedControlStyle = UISegmentedControlStyleBar;
	self.fontControl.momentary = YES;

	UIBarButtonItem *fontButton = [[UIBarButtonItem alloc] initWithCustomView:self.fontControl];


	NSMutableArray *buttonArray = [NSMutableArray arrayWithArray:self.navigationItem.rightBarButtonItems];
	[buttonArray addObject:fontButton];
	self.navigationItem.rightBarButtonItems = buttonArray;
}

- (void) previewFile
{
	NSString *theString = @"";
    if (self.filePath.length > 0) {
        NSData *fileData = [[FXLSafeZone secureFileManager] contentsAtPath:self.filePath];
		theString = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    }

	self.textView.text = theString;

	[self redoControls];

    return;
}

- (void) setTextFontSize:(int)inTextFontSize
{
	int newFontSize = inTextFontSize;
	if (newFontSize < kMinimumTextFontSize) newFontSize = kMinimumTextFontSize;
	if (newFontSize > kMaximumTextFontSize) newFontSize = kMaximumTextFontSize;

	if (newFontSize != _textFontSize) {
		_textFontSize = newFontSize;
		[self redoControls];
	}

	return;
}

#pragma mark - IBActions

- (IBAction) toggleFontChange:(id)inSender
{
	if (self.fontControl.selectedSegmentIndex == 0) {
		--self.textFontSize;
	} else {
		++self.textFontSize;
	}

	return;
}

#pragma mark - Private methods

- (void) redoControls
{
	if (self.isCode) {
		self.textView.font = [UIFont fontWithName:kCodeFontFamily size:_textFontSize];
	} else {
		self.textView.font = [UIFont systemFontOfSize:_textFontSize];
	}
	[self.fontControl setEnabled:(_textFontSize > kMinimumTextFontSize) forSegmentAtIndex:0];
	[self.fontControl setEnabled:(_textFontSize < kMaximumTextFontSize) forSegmentAtIndex:1];
}

@end

