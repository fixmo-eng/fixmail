//
//	SZCCertPreviewController.m
//  SafeZone
//
//  Created by Colin Biggin on 13-06-20.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "SZCCertPreviewController.h"
#import "FXMEmailSecurity.h"

@interface SZCCertPreviewController ()
@property (nonatomic, weak) IBOutlet UITextView *attributesTextView;
@property (nonatomic, weak) IBOutlet UITextView *rawTextView;
@property (nonatomic, strong) NSString *passPhrase;
@property (nonatomic, assign) bool isPEM;
@end

@implementation SZCCertPreviewController

+ (SZCCertPreviewController *) createP12Previewer:(NSString *)inPassPhrase
{
	SZCCertPreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCCertPreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCCertPreviewController"];
	}
	theController.isPEM = false;
	theController.passPhrase = inPassPhrase;

	return theController;
}

+ (SZCCertPreviewController *) createPEMPreviewer:(NSString *)inPassPhrase
{
	SZCCertPreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCCertPreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCCertPreviewController"];
	}
	theController.isPEM = true;
	theController.passPhrase = inPassPhrase;

	return theController;
}

- (void) viewDidLoad
{
	[super viewDidLoad];
}

- (void) previewFile
{
	NSString *rawString = @"<Not Applicable>";
	NSString *attributesString = @"";
    if (self.filePath.length > 0) {
        NSData *fileData = [[FXLSafeZone secureFileManager] contentsAtPath:self.filePath];

		NSArray *theAttributes = [FXMEmailSecurity getAllAttributesForP12Data:fileData passPhrase:self.passPhrase];
		if (theAttributes.count > 0) {
			attributesString = [theAttributes componentsJoinedByString:@"\n"];
		}

		if (self.isPEM) {
			rawString = [[NSString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
		}
    }

	self.attributesTextView.text = attributesString;
	self.rawTextView.text = rawString;

    return;
}

@end

