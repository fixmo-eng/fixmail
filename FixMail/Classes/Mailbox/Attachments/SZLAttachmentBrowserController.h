//
//  SZCFileBrowserController.h
//  SafeZone
//
//  Created by Leena Mansour on 12-06-17.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SZLAttachmentBrowserController;

@protocol SZLAttachmentBrowserDelegate <NSObject>
@required
- (void)didCancel:(SZLAttachmentBrowserController *)inAttachmentBrowserController;
- (void)didSelectItem:(SZLAttachmentBrowserController *)inAttachmentBrowserController path:(NSString *)inPath;
@end

@interface SZLAttachmentBrowserController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (assign) id delegate;

+ (SZLAttachmentBrowserController *) launchFileBrowserFromPath:(NSString *)browsingPath;
+ (SZLAttachmentBrowserController *) launchFolderBrowserFromPath:(NSString *)browsingPath;

@end
