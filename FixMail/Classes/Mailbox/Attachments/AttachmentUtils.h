//
//  AttachmentUtils.h
//  FixMail
//
//  Created by Colin Biggin on 2013-04-09.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	documentTypeUnknown = 0,

	documentTypeText = 10,
	documentTypePDF,
	documentTypeImage,
	documentTypeWeb,
	documentTypeCert,
	documentTypeCode,
	documentTypeBinary,

} EDocumentType;

@interface AttachmentUtils : NSObject

+ (BOOL) isAttachmentViewable:(NSString *)inFilePath;
+ (UIImage *) documentIconForFile:(NSString *)inFilePath;
+ (EDocumentType) documentTypeForFile:(NSString *)inFilePath;
+ (NSString *) fileSizeString:(long)inSize;
+ (NSString *) getMIMEType:(NSString *)inFilePath;
+ (NSString *) createUniquePath:(NSString *)inPath name:(NSString *)inFileName;

@end
