//
//  SZCCertPreviewController.h
//  SafeZone
//
//  Created by Colin Biggin on 13-06-20.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "SZCFilePreviewController.h"

@interface SZCCertPreviewController : SZCFilePreviewController

+ (SZCCertPreviewController *) createP12Previewer:(NSString *)inPassPhrase;
+ (SZCCertPreviewController *) createPEMPreviewer:(NSString *)inPassPhrase;

@end
