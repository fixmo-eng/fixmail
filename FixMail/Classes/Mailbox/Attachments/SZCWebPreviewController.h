//
//  SZCWebPreviewController.h
//  SafeZone
//
//  Created by Leena Mansour on 12-10-02.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCFilePreviewController.h"

@interface SZCWebPreviewController : SZCFilePreviewController <UIWebViewDelegate>

+ (SZCWebPreviewController *) createWebPreviewer;

@end
