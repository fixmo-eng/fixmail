//
//  SZCWebPreviewController.m
//  SafeZone
//
//  Created by Leena Mansour on 12-10-02.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCWebPreviewController.h"
#import "AttachmentUtils.h"

@interface SZCWebPreviewController ()
@property (nonatomic, weak) IBOutlet UIWebView* webView;
@end

@implementation SZCWebPreviewController

+ (SZCWebPreviewController *) createWebPreviewer
{
	SZCWebPreviewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPad identifier:@"SZCWebPreviewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Previewers_iPhone identifier:@"SZCWebPreviewController"];
	}
	theController.showSaveButton = YES;

	return theController;
}

- (void)previewFile
{
    if (self.filePath) {
        NSData* fileData = [[FXLSafeZone secureFileManager] contentsAtPath:self.filePath];
        if (fileData) {
            NSString* mimeType = [AttachmentUtils getMIMEType:self.filePath];
            
            if (mimeType) {
                [self.webView loadData:fileData MIMEType:mimeType textEncodingName:@"utf-8" baseURL:[NSURL fileURLWithPath:self.filePath]];
            }
        }
    }
    return;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeOther) {
        return YES;
    } else {
        return NO;
    }
}

@end
