//
//  AttachmentManager.h
//  FixMail
//
//  Created by Colin Biggin on 2013-04-05.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//
#import "ASItemOperations.h"

@class ASAttachment;
@class ASEmail;

#define kAttachmentManagerStartedNotification		@"kAttachmentManagerStartedNotification"
#define kAttachmentManagerProgressNotification		@"kAttachmentManagerProgressNotification"
#define kAttachmentManagerErrorNotification			@"kAttachmentManagerErrorNotification"
#define kAttachmentManagerFinishedNotification		@"kAttachmentManagerFinishedNotification"

#define kAttachmentManagerPercentageUserInfo		@"kAttachmentManagerPercentageUserInfo"
#define kAttachmentManagerASAttachmentUserInfo		@"kAttachmentManagerASAttachmentUserInfo"
#define kAttachmentManagerASItemOperationUserInfo	@"kAttachmentManagerASItemOperationUserInfo"
#define kAttachmentManagerEmailUserInfo				@"kAttachmentManagerEmailUserInfo"
#define kAttachmentManagerErrorMessage              @"kAttachmentManagerErrorMessage"

@interface AttachmentManager : NSObject <ASItemOperationsDelegate>

+ (NSString *) attachmentsPathWithEmail:(ASEmail *)inEmail;
+ (NSString *) attachmentPathWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;

+ (BOOL) isAttachmentDownloaded:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;
+ (BOOL) isAttachmentDownloading:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;
+ (unsigned long) percentDownloaded:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;

+ (void) downloadAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;
+ (void) cleanupAttachmentFolderForEmail:(ASEmail *)inEmail;

+ (NSData *) dataWithAttachment:(ASAttachment *)inAttachment email:(ASEmail *)inEmail;

+ (void) deleteAllAttachments;

@end