//
//  AttachmentUtils.m
//  FixMail
//
//  Created by Colin Biggin on 2013-04-09.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "AttachmentUtils.h"

NSString *const kPdfMime = @"application/pdf";
NSString *const kJpegMime = @"image/jpeg";
NSString *const kPngMime = @"image/png";
NSString *const kGifMime = @"image/gif";
NSString *const kTiffMime = @"image/tiff";
NSString *const kBmpMime = @"image/bmp";
NSString *const kDocMime = @"application/msword";
NSString *const kDocxMime = @"application/vnd.openxmlformats-officedocument.wordprocessingml.document";
NSString *const kXlsMime = @"application/msexcel";
NSString *const kXlsxMime = @"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
NSString *const kPptMime = @"application/mspowerpoint";
NSString *const kPptxMime = @"application/vnd.openxmlformats-officedocument.presentationml.presentation";
NSString *const kRtfMime = @"text/richtext";
NSString *const kTextMime = @"text/plain";
NSString *const kCalendarMime = @"text/calendar";
NSString *const kHtmlMime = @"text/html";
NSString *const kCssMime = @"text/css";
NSString *const kBinaryMime = @"application/octet-stream";

NSString *const kPEMMIME = @"application/x-pem-file";
NSString *const kP12MIME = @"application/x-pkcs12";
NSString *const kP7SMIME = @"application/x-pkcs7-signature";
NSString *const kCertMIME = @"application/x-x509-ca-cert";

NSString *const kCertIconFile = @"FixMailRsrc.bundle/attachmentIconCertificate.png";
NSString *const kImageIconFile = @"FixMailRsrc.bundle/attachmentIconImage.png";
NSString *const kTextIconFile = @"FixMailRsrc.bundle/attachmentIconText.png";
NSString *const kCodeIconFile = @"FixMailRsrc.bundle/attachmentIconCode.png";
NSString *const kWordIconFile = @"FixMailRsrc.bundle/attachmentIconMSWord.png";
NSString *const kExcelIconFile = @"FixMailRsrc.bundle/attachmentIconMSExcel.png";
NSString *const kPowerPointIconFile = @"FixMailRsrc.bundle/attachmentIconMSPowerPoint.png";
NSString *const kHtmlIconFile = @"FixMailRsrc.bundle/attachmentIconHTML.png";
NSString *const kCssIconFile = @"FixMailRsrc.bundle/attachmentIconCSS.png";
NSString *const kCalendarIconFile = @"FixMailRsrc.bundle/attachmentIconICS.png";
NSString *const kPdfIconFile = @"FixMailRsrc.bundle/attachmentIconPDF.png";
NSString *const kZipIconFile = @"FixMailRsrc.bundle/attachmentIconZIP.png";
NSString *const kNumbersIconFile = @"FixMailRsrc.bundle/attachmentIconNumbers.png";
NSString *const kPagesIconFile = @"FixMailRsrc.bundle/attachmentIconPages.png";
NSString *const kKeynoteIconFile = @"FixMailRsrc.bundle/attachmentIconKeynote.png";

NSString *const keyDoc = @"D";
NSString *const keyMIME = @"M";
NSString *const keyImage = @"I";

static NSDictionary *sFileDictionary = nil;

@implementation AttachmentUtils

+ (NSDictionary *) fileDictionary:(NSString *)inFilePath
{
	if (sFileDictionary == nil) {
		sFileDictionary = @{

			// certicates
			@"pem":		@{ keyDoc : @(documentTypeCert),	keyMIME : kPEMMIME,			keyImage : kCertIconFile },
			@"p12":		@{ keyDoc : @(documentTypeCert),	keyMIME : kP12MIME,			keyImage : kCertIconFile },
			@"cer":		@{ keyDoc : @(documentTypeCert),	keyMIME : kCertMIME,		keyImage : kCertIconFile },
			@"cert":	@{ keyDoc : @(documentTypeCert),	keyMIME : kCertMIME,		keyImage : kCertIconFile },
			@"p7s":		@{ keyDoc : @(documentTypeCert),	keyMIME : kP7SMIME,			keyImage : kCertIconFile },

			// image formats
			@"png":		@{ keyDoc : @(documentTypeImage),	keyMIME : kPngMime,			keyImage : kImageIconFile },
			@"jpg":		@{ keyDoc : @(documentTypeImage),	keyMIME : kJpegMime,		keyImage : kImageIconFile },
			@"jpeg":	@{ keyDoc : @(documentTypeImage),	keyMIME : kJpegMime,		keyImage : kImageIconFile },
			@"gif":		@{ keyDoc : @(documentTypeImage),	keyMIME : kGifMime,			keyImage : kImageIconFile },
			@"tiff":	@{ keyDoc : @(documentTypeImage),	keyMIME : kTiffMime,		keyImage : kImageIconFile },
			@"tif":		@{ keyDoc : @(documentTypeImage),	keyMIME : kTiffMime,		keyImage : kImageIconFile },
			@"bmp":		@{ keyDoc : @(documentTypeImage),	keyMIME : kBmpMime,			keyImage : kImageIconFile },

			// calendar
			@"ics":		@{ keyDoc : @(documentTypeText),	keyMIME : kCalendarMime,	keyImage : kCalendarIconFile },

			// adobe
			@"pdf":		@{ keyDoc : @(documentTypePDF),		keyMIME : kPdfMime,			keyImage : kPdfIconFile },

			// iWork suite
			@"numbers":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kBinaryMime,		keyImage : kNumbersIconFile },
			@"pages":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kBinaryMime,		keyImage : kPagesIconFile },
			@"keynote":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kBinaryMime,		keyImage : kKeynoteIconFile },

			// microsoft
			@"doc":		@{ keyDoc : @(documentTypeWeb),		keyMIME : kDocMime,			keyImage : kWordIconFile },
			@"docx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kDocxMime,		keyImage : kWordIconFile },
			@"xls":		@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsMime,			keyImage : kExcelIconFile },
			@"xlsx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsxMime,		keyImage : kExcelIconFile },
			@"ppt":		@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptMime,			keyImage : kPowerPointIconFile },
			@"pptx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },

			// uncommon microsoft
			@"dotx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kDocMime,			keyImage : kWordIconFile },
			@"dotm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kDocxMime,		keyImage : kWordIconFile },
			@"xlsm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsMime,			keyImage : kExcelIconFile },
			@"xltx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsxMime,		keyImage : kExcelIconFile },
			@"xltm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsMime,			keyImage : kExcelIconFile },
			@"xlsb":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsxMime,		keyImage : kExcelIconFile },
			@"xlam":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kXlsxMime,		keyImage : kExcelIconFile },
			@"pptm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },
			@"potx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },
			@"pptm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },
			@"ppam":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },
			@"ppsx":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },
			@"ppsm":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kPptxMime,		keyImage : kPowerPointIconFile },

			// web/html
			@"htm":		@{ keyDoc : @(documentTypeWeb),		keyMIME : kHtmlMime,		keyImage : kHtmlIconFile },
			@"html":	@{ keyDoc : @(documentTypeWeb),		keyMIME : kHtmlMime,		keyImage : kHtmlIconFile },
			@"css":		@{ keyDoc : @(documentTypeWeb),		keyMIME : kCssMime,			keyImage : kCssIconFile },

			// text
			@"txt":		@{ keyDoc : @(documentTypeText),	keyMIME : kTextMime,		keyImage : kTextIconFile },
			@"text":	@{ keyDoc : @(documentTypeText),	keyMIME : kTextMime,		keyImage : kTextIconFile },

			// code
			@"c":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"h":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"m":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"cpp":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"cp":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"c++":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"h++":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"mm":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"java":	@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"py":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"rb":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"ruby":	@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"sh":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"csh":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"bsh":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },
			@"xml":		@{ keyDoc : @(documentTypeCode),	keyMIME : kTextMime,		keyImage : kCodeIconFile },

			// others
			@"gzip":	@{ keyDoc : @(documentTypeBinary),	keyMIME : kBinaryMime,		keyImage : kZipIconFile },
			@"zip":		@{ keyDoc : @(documentTypeBinary),	keyMIME : kBinaryMime,		keyImage : kZipIconFile }

		};
	}

    NSString *fileExtenstion = [[inFilePath pathExtension] lowercaseString];
	return sFileDictionary[fileExtenstion];
}

+ (BOOL) isAttachmentViewable:(NSString *)inFilePath
{
	EDocumentType documentType = [AttachmentUtils documentTypeForFile:inFilePath];
	return (documentType != documentTypeUnknown) && (documentType != documentTypeBinary);
}

+ (UIImage *) documentIconForFile:(NSString *)inFilePath
{	NSString *theIconImage = nil;

	NSDictionary *theDictionary = [AttachmentUtils fileDictionary:inFilePath];
	if (theDictionary != nil) {
		theIconImage = theDictionary[keyImage];
	}

	if (theIconImage == nil) theIconImage = @"FixMailRsrc.bundle/attachmentIconGeneric.png";
	return [UIImage imageNamed:theIconImage];
}

+ (EDocumentType) documentTypeForFile:(NSString *)inFilePath
{
	NSDictionary *theDictionary = [AttachmentUtils fileDictionary:inFilePath];
	if (theDictionary == nil) return documentTypeUnknown;

    return (EDocumentType)[theDictionary[keyDoc] intValue];
}

+ (NSString *) getMIMEType:(NSString *)inFilePath
{
	NSDictionary *theDictionary = [AttachmentUtils fileDictionary:inFilePath];
	if (theDictionary == nil) return kBinaryMime;

    return theDictionary[keyMIME];
}

+ (NSString *) fileSizeString:(long)inSize
{
	NSString *theString = @"";
	if (inSize < 1024) {
		theString = [NSString stringWithFormat:@"%ld bytes", inSize];
	} else if (inSize < (1024*1024)) {
		theString = [NSString stringWithFormat:@"%.1f KB", (float)(inSize/1024.0)];
	} else {
		theString = [NSString stringWithFormat:@"%.1f MB", (float)(inSize/(1024.0*1024.0))];
	}

	return theString;
}

+ (NSString *) createUniquePath:(NSString *)inPath name:(NSString *)inFileName
{
	NSString *toPath = [inPath stringByAppendingPathComponent:inFileName];
	if (![[FXLSafeZone secureFileManager] fileExistsAtPath:toPath]) return toPath; // too easy

	// now go through and add an appendix to it
	NSString *fileBase = [inFileName stringByDeletingPathExtension];
	NSString *fileExtension = [inFileName pathExtension];

	for (int fileIndex=1; fileIndex<1000; fileIndex++) {
		NSString *fileName = [NSString stringWithFormat:@"%@_%d", fileBase, fileIndex];
		fileName = [fileName stringByAppendingPathExtension:fileExtension];
		toPath = [inPath stringByAppendingPathComponent:fileName];
		if (![[FXLSafeZone secureFileManager] fileExistsAtPath:toPath]) break;
	}

	return toPath;
}

@end
