//
//  SZCSeamlessHeaderWebView.h
//  
//
//  Created by Anluan O'Brien on 12/12/12.
//
//

#import <UIKit/UIKit.h>

@interface SZCSeamlessHeaderWebView : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *messageView;
@property (strong, nonatomic) IBOutlet UIView *headerView;

-(void)resizedHeaderViewIncludingVertically:(BOOL)vertically;
@end
