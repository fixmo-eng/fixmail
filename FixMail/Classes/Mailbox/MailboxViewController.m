//
//  MailboxViewController.m
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "MailboxViewController.h"
#import "BaseAccount.h"
#import "ASEmail.h"
#import "ASEmailFolder.h"
#import "ASEmailThread.h"
#import "DateUtil.h"
#import "EmailProcessor.h"
#import "EmailSearch.h"
#import "ErrorAlert.h"
#import "MailboxCell.h"
#import "FMSplitViewDelegate.h"
#import "PlaceholderSplitviewDetailViewController.h"
#import "UIStoryboard+FixmoExtensions.h"
#import "UIButton+FixmoExtensions.h"
#import "UIActionSheet+Modal.h"
#import "UIView-ViewFrameGeometry.h"
#import "AppSettings.h"
#import "ASMoveItems.h"

#import "composeMailVC.h"
#import "mailViewerVC.h"
#import "UIViewController+MGSplitViewController.h"
#import "FolderSelectController.h"
#import "FXMSearchTimerManager.h"

////////////////////////////////////////////////////////////////////////////////
// MailboxViewController
////////////////////////////////////////////////////////////////////////////////
#pragma mark MailboxViewController

@interface MailboxViewController ()
{
    BOOL loading;
}
@property (nonatomic, strong) mailViewerVC *mailViewerController;
@property (nonatomic, strong) FolderSelectController *moveItemsFolder;
@property (nonatomic, strong) FolderSelectController *gotoFolder;
@property (nonatomic, weak) IBOutlet UISearchBar *emailSearchBar;
@property (nonatomic, strong) NSString *emailSearchString;
@property (nonatomic) BOOL searching;
@property (nonatomic, strong) NSArray *emailSearchData;
@property (nonatomic, strong) FXMSearchTimerManager *searchTimerManager;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) UIBarButtonItem *deleteButton;
@property (nonatomic, strong) UIBarButtonItem *moveButton;
@property (nonatomic, strong) UIBarButtonItem *markButton;

@property (nonatomic, strong) UIBarButtonItem *updatedStatusButtonItem;
@property (nonatomic, strong) UILabel *updatedStatusLabel;
@property (nonatomic, strong) UIBarButtonItem *sortOrderButton;
@property (nonatomic, strong) UIBarButtonItem *composeButton;

@property (assign) EMailSortBy sortBy;
@property (assign) EMailSortOrder sortOrder;

@property (assign) int nResults;
@property (nonatomic,assign) int numberOfPreviewLines;


@end

#pragma mark -

@implementation MailboxViewController

static BOOL receivedAdditionalAllMail   = NO; // whether we received an additionalResults call
static BOOL moreResultsAllMail          = NO; // are there more results after this?

#define kAlertViewTagError				10
#define kActionSheetTagSort				11
#define kActionSheetMarkRead			12
#define kActionSheetMarkUnread			13
#define kActionSheetMarkReadUnread		14
#define kAlertViewDeleteItems			15

#define kGotoFolderAnimationDuration	0.3

static const int kPageSize = 50;

@synthesize searching;
@synthesize searchTimerManager = _searchTimerManager;

////////////////////////////////////////////////////////////////////////////////
// Factory 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (MailboxViewController*)controller
{
	MailboxViewController *theController = nil;
	if (IS_IPAD()) {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Mailbox_iPad identifier:@"MailboxViewController"];
	} else {
		theController = [UIStoryboard storyboardWithName:kStoryboard_Mailbox_iPhone identifier:@"MailboxViewController"];
	}

	return theController;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"MAILBOX_VIEW", @"ErrorAlert", @"MailboxViewController error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"MAILBOX_VIEW", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad 
{
	[super viewDidLoad];

	// appearance stuff
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	self.navigationController.navigationBarHidden = NO;

	self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    self.navigationController.toolbarHidden = NO;

	self.tableView.rowHeight = [self calculateRowHeight];

	// replace our title view with a drop-down control button
	[self redoTitleButton];


	self.sortBy = sortMailByDate;
	self.sortOrder = sortMailOrderDescending;
	[self nonEditingMode:false];

	[self redoControls];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	EmailProcessor* ep = [EmailProcessor getSingleton];
	ep.updateSubscriber = self;
    
    [self setUpNonEditingModeToolbarForInterfaceOrientation:self.interfaceOrientation animated:NO];
}

- (void)viewDidAppear:(BOOL)animated 
{
    [super viewDidAppear:animated];
    
	if(animated) {
        // animating to or from - reload unread, server state
        //
		[self reloadTable];
	}
	
	[self.navigationController setToolbarHidden:NO animated:animated];	
}

- (void)viewWillDisappear:(BOOL)animated 
{
	[super viewWillDisappear:animated];
	
	EmailProcessor* ep = [EmailProcessor getSingleton];
	ep.updateSubscriber = nil;

////  This causes the email loading to halt in portrait mode.
////  The view 'disappears' and 'appears' when selecting the navigation
////  stopping the loading of the emails
//    if([self isMovingFromParentViewController]) {
//        if(self.emailSearch) {
//            self.emailSearch.cancelled  = TRUE;
//            self.emailSearch            = nil;
//        }
//        self.folder         = nil;
//        self.emailData      = nil;
//    }
    
	if (IS_IPAD()) {
		[self replaceDetailWithPlaceholder];
	}
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (NSUInteger) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void) didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	FXDebugLog(kFXLogActiveSync, @"MailboxViewController reveived memory warning - dumping cache");
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self setUpNonEditingModeToolbarForInterfaceOrientation:toInterfaceOrientation animated:YES];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	if (IS_IPAD()) {
		[self redoTitleButton];
	}

	return;
}

- (void) setUpNonEditingModeToolbarForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation animated:(BOOL)animated
{
    if (!IS_IPAD() || UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        if (!self.composeButton) {
            self.composeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(toggleCompose:)];
        }
    } else {
        self.composeButton = nil;
    }
    UIBarButtonItem	*spacerItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	NSArray *toolbarItems = [NSArray arrayWithObjects:self.sortOrderButton, spacerItem, self.updatedStatusButtonItem, spacerItem, self.composeButton, nil];
	[self setToolbarItems:toolbarItems animated:animated];
}

////////////////////////////////////////////////////////////////////////////////
// Message Handling
////////////////////////////////////////////////////////////////////////////////
#pragma mark Message Handling

- (void)runFolderSearch
{
	// run a search with given offset
	receivedAdditionalAllMail   = NO;
	moreResultsAllMail          = NO;

    if(!self.emailSearch) {
        self.emailSearch = [[EmailSearch alloc] initWithFolder:self.folder delegate:self];
    }
    [[SearchRunner getSingleton] folderSearch:self.emailSearch];
}

-(void)insertRows:(NSArray*)anEmails
{
    @try {
        //NSMutableArray* anIndexPaths = [NSMutableArray arrayWithCapacity:anEmails.count];
        
        NSUInteger aRowNew = 0;
        NSUInteger aRowLast = self.emailData.count;

        for(ASEmail* anEmail in anEmails) {
            NSUInteger aRow;
            if([anEmail isNew]) {
                aRow = aRowNew;
                aRowNew++;
            }else{
                aRow = aRowLast;
            }
            //NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:aRow inSection:0];
            //[anIndexPaths addObject:anIndexPath];
            [self.emailData insertObject:anEmail atIndex:aRow];

            aRowLast++;
        }
        //[self.tableView insertRowsAtIndexPaths:anIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
		[self reloadTable];
		if (self.mailViewerController != nil) {
			[self.mailViewerController newMessagesArrived];
		}
	} @catch (NSException* e) {
        [self _logException:@"insertRows" exception:e];
	}
	@finally {
   		[self.folder setIsCountWanted:TRUE];
	}
}

- (void)loadEmails:(NSArray*)inEmails
{
	if (inEmails.count == 0) return;

//	[self performSelectorOnMainThread:@selector(insertRows:) withObject:inEmails waitUntilDone:NO];
	// do a check on all the incoming emails and see if they are already in the table
	NSMutableArray *newEmails = [[NSMutableArray alloc] initWithCapacity:inEmails.count];
	for (ASEmail *theEmail in inEmails) {
		if ([self.emailData containsObject:theEmail]) {
            FXDebugLog(kFXLogASEmail, @"loadEmails duplicate load: %@", theEmail);
            continue;
        }
		// add it
		[newEmails addObject:theEmail];
	}

	if (newEmails.count > 0) {
		[self performSelectorOnMainThread:@selector(insertRows:) withObject:newEmails waitUntilDone:NO];
	}

//	[self.emailData addObjectsFromArray:inEmails];
//	[self reloadTable];
}

- (void)doLoad 
{
	self.nResults               = 0;

	receivedAdditionalAllMail   = NO;
	moreResultsAllMail          = NO;
	
	self.emailData = [[NSMutableArray alloc] initWithCapacity:50];
	
    if(self.folder.isInitialSynced || !self.folder.isSyncable) {
        [self runFolderSearch];
    }
}

////////////////////////////////////////////////////////////////////////////////
// SearchManagerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark SearchManagerDelegate

- (void)deliverSearchResults:(NSArray *)anEmails
{
	NSOperationQueue* q = [[EmailProcessor getSingleton] operationQueue];
	NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadEmails:) object:anEmails];
	[q addOperation:op];
}

- (void)deliverAdditionalResults:(NSNumber*)inMoreResults
{
	receivedAdditionalAllMail = YES;
	moreResultsAllMail = [inMoreResults boolValue];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self reloadTable];
        loading = NO;
        
    });
}

- (BOOL)shouldDeliverObject:(NSString *)aUid
{
    return TRUE;
}

#pragma mark - mailViewerDelegate methods

- (NSUInteger) indexOfEmail:(ASEmail *)inEmail
{
	if (self.searching) {
		if (!self.emailSearchData) return NSNotFound;
		if (self.emailSearchData.count == 0) return NSNotFound;
		return [self.emailSearchData indexOfObject:inEmail];
	}
	if (self.emailData == nil) return NSNotFound;
	if (self.emailData.count == 0) return NSNotFound;
	return [self.emailData indexOfObject:inEmail];
}

- (NSUInteger) emailTotal:(ASEmail *)inEmail
{
	if (self.searching) {
		return self.emailSearchData.count;
	}
	return self.emailData.count;
}

- (ASEmail *) getEmailAtIndex:(int)inIndex
{
	if (self.searching) {
		if (!self.emailSearchData) return nil;
		if (self.emailData.count <= inIndex) return nil;
		
		return [self.emailSearchData objectAtIndex:inIndex];
	}
	if (self.emailData == nil) return nil;
	if (self.emailData.count <= inIndex) return nil;

	return [self.emailData objectAtIndex:inIndex];
}

- (void) emailSelected:(ASEmail *)inEmail
{
	if (inEmail == nil) return;

	NSIndexPath *theIndexPath = [self getIndexPathForUID:inEmail.uid];
	if (theIndexPath != nil) {
		[self.tableView selectRowAtIndexPath:theIndexPath animated:true scrollPosition:UITableViewScrollPositionNone];
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
// Compose
////////////////////////////////////////////////////////////////////////////////
#pragma mark - IBActions

- (IBAction) toggleSort:(id)inSender
{
	NSString *ascendingString = FXLLocalizedStringFromTable(@"ASCENDING_SORT_ORDER", @"MailboxViewController", @"Ascending Sort Order in AlertSheet");
	NSString *descendingString = FXLLocalizedStringFromTable(@"DESCENDING_SORT_ORDER", @"MailboxViewController", @"Descending Sort Order in AlertSheet");
	switch (self.sortOrder) {
		case sortMailOrderAscending:
			ascendingString = [@"✔ " stringByAppendingString:ascendingString];
			break;
		case sortMailOrderDescending:
		default:
			descendingString = [@"✔ " stringByAppendingString:descendingString];
			break;
	}

	NSString *attachmentsString = FXLLocalizedStringFromTable(@"ATTACHMENTS", @"MailboxViewController", @"Action sheet option to sort by attachments");
	NSString *dateString = FXLLocalizedStringFromTable(@"DATE", @"MailboxViewController", @"Action sheet option to sort by date");
	NSString *fromString = FXLLocalizedStringFromTable(@"FROM", @"MailboxViewController", @"Action sheet option to sort by sender (from) field");
	NSString *sizeString = FXLLocalizedStringFromTable(@"SIZE", @"MailboxViewController", @"Action sheet option to sort by  email size");
	NSString *subjectString = FXLLocalizedStringFromTable(@"SUBJECT", @"MailboxViewController", @"Action sheet option to sort by subject field");
	NSString *toString = FXLLocalizedStringFromTable(@"TO", @"MailboxViewController", @"Action sheet option to sort by recipient (to) field");
	NSString *unreadString = FXLLocalizedStringFromTable(@"UNREAD_SORT_OPTION", @"MailboxViewController", @"Action sheet option to separate read from unread emails");
	switch (self.sortBy) {
		case sortMailByAttachments: attachmentsString = [@"✔ " stringByAppendingString:attachmentsString]; break;
		case sortMailByFrom: fromString = [@"✔ " stringByAppendingString:fromString]; break;
		case sortMailBySize: sizeString = [@"✔ " stringByAppendingString:sizeString]; break;
		case sortMailBySubject: subjectString = [@"✔ " stringByAppendingString:subjectString]; break;
		case sortMailByTo: toString = [@"✔ " stringByAppendingString:toString]; break;
		case sortMailByUnread: unreadString = [@"✔ " stringByAppendingString:unreadString]; break;
		default:
		case sortMailByDate: dateString = [@"✔ " stringByAppendingString:dateString]; break;
	}

	UIActionSheet *theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"SORT_MAIL_BY", @"MailboxViewController", @"Title of action sheet giving different sorting options")
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                  destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_SORT_MAIL_BY", @"MailboxViewController", @"Cancel option of action sheet giving different sorting options")
                                                       otherButtonTitles:ascendingString,
									 descendingString,
									 attachmentsString,
									 dateString,
									 fromString,
									 sizeString,
									 subjectString,
									 toString,
									 unreadString, nil
									 ];

	int theIndex = [theActionSheet showModalFromBarButtonItem:self.sortOrderButton animated:true];
	switch (theIndex) {
			case 1: self.sortOrder = sortMailOrderAscending; break;
			case 2: self.sortOrder = sortMailOrderDescending; break;
			case 3: self.sortBy = sortMailByAttachments; break;
			case 4: self.sortBy = sortMailByDate; break;
			case 5: self.sortBy = sortMailByFrom; break;
			case 6: self.sortBy = sortMailBySize; break;
			case 7: self.sortBy = sortMailBySubject; break;
			case 8: self.sortBy = sortMailByTo; break;
			case 9: self.sortBy = sortMailByUnread; break;
			default: break;
	}

	[self reloadTable];
	[self redoSortControls];

	return;
}

- (IBAction) toggleCompose:(id)sender
{
    BaseAccount* anAccount = NULL;
    if(self.folder) {
        anAccount = [self.folder account];
    }else{
        anAccount = [BaseAccount accountForAccountNumber:0];
    }
    UINavigationController *navController = [composeMailVC createNavigationMailComposerWithTo:nil cc:nil bcc:nil attachments:nil account:anAccount];
	[self presentViewController:navController animated:true completion:nil];
}

- (IBAction) toggleDelete:(id)inSender
{
	if (self.tableView.indexPathsForSelectedRows.count == 0) return;

	// check if the "Deleted Items" folder (or any others)
	// they should be permanently deleted

	if (self.folder.folderType == kFolderTypeDeleted) {
        UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:NULL
                                                               message:FXLLocalizedStringFromTable(@"ARE_YOU_SURE_YOU_WANT_TO_PERMANENTLY_DELETE_THESE_MESSAGES?", @"MailboxViewController", @"Alert view message when deleting emails from the Deleted Items folder")
                                                              delegate:self
                                                     cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_MESSAGE_DELETION", @"MailboxViewController", @"Cancel button when deleting emails from the Deleted Items folder for the alertview asking if really wanted to delete emails permanently")
                                                     otherButtonTitles:FXLLocalizedStringFromTable(@"DELETE_PERMANENT_MESSAGE_DELETION", @"MailboxViewController", @"Delete/Proceed button when deleting items from the Deleted Items folder for the alertview asking if really wanted to delete emails permanently"), nil];
        theAlertView.tag = kAlertViewDeleteItems;
        [theAlertView show];
    } else if ([AppSettings emailAskBeforeDeleting]) {
        UIAlertView *theAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"DELETE_MESSAGE", @"mailViewerVC", @"Alert view title for message deletion confirmation")
															   message:nil
															  delegate:self
													 cancelButtonTitle:FXLLocalizedStringFromTable(@"NO_DELETE_MESSAGE", @"mailViewerVC", @"Alert view title for message deletion confirmation")
													 otherButtonTitles:FXLLocalizedStringFromTable(@"YES_DELETE_MESSAGE", @"mailViewerVC", @"Yes button for message deletion confirmation"), nil];
        theAlertView.tag = kAlertViewDeleteItems;
        [theAlertView show];
	} else {
		[self deleteSelectedRows];
	}

	return;
}


- (IBAction) toggleMove:(id)inSender
{
	NSAssert(self.moveItemsFolder == nil, @"self.moveItemsFolder == nil");

	if (IS_IPAD()) {
		self.moveItemsFolder = [[FolderSelectController alloc] initWithNibName:kNib_FolderView_iPad bundle:[FXLSafeZone getResourceBundle]];
		self.moveItemsFolder.delegate = self;
		self.moveItemsFolder.currentFolder = self.folder;
		[self.navigationController pushViewController:self.moveItemsFolder animated:YES];
	} else {
		self.moveItemsFolder = [[FolderSelectController alloc] initWithNibName:kNib_FolderView_iPhone bundle:[FXLSafeZone getResourceBundle]];
		self.moveItemsFolder.delegate = self;
		self.moveItemsFolder.currentFolder = self.folder;
		UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:self.moveItemsFolder];
		[self presentViewController:nc animated:YES completion:^{}];
	}
}

- (IBAction) toggleMark:(id)inSender
{
	// go through and figure out how many read/unread and bring up the appropriate dialog
	int numRead = 0;
	int numUnread = 0;
	for (NSIndexPath *theIndexPath in self.tableView.indexPathsForSelectedRows) {
		ASEmail *theEmail = self.emailData[theIndexPath.row];
		if (theEmail.isRead) numRead++;
		else numUnread++;
	}

	// now bring up appropriate dialog
	UIActionSheet *theActionSheet;
	if ((numRead > 0) && (numUnread > 0)) {
		theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"CHANGE_MESSAGES_TO", @"MailboxViewController", @"Action sheet title when attempting to MARK emails") delegate:self cancelButtonTitle:nil destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_MARK_EMAILS", @"MailboxViewController", @"Action sheet option to cancel when pressed MARK emails")
				otherButtonTitles:[NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"READ", @"MailboxViewController", @"Action sheet option to mark emails as read when pressed MARK emails"), numUnread], [NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"UNREAD", @"MailboxViewController", @"Action sheet option to mark emails as unread when pressed MARK emails"), numRead], nil
		];
		theActionSheet.tag = kActionSheetMarkReadUnread;
	} else if (numRead > 0) {
		theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"CHANGE_MESSAGES_TO", @"MailboxViewController", @"Action sheet title when attempting to MARK emails") delegate:self cancelButtonTitle:nil destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_MARK_EMAILS", @"MailboxViewController", @"Action sheet option to cancel when pressed MARK emails")
				otherButtonTitles:[NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"UNREAD", @"MailboxViewController", @"Action sheet option to mark emails as unread when pressed MARK emails"), numRead], nil
		];
		theActionSheet.tag = kActionSheetMarkUnread;
	} else if (numUnread > 0) {
		theActionSheet = [[UIActionSheet alloc] initWithTitle:FXLLocalizedStringFromTable(@"CHANGE_MESSAGES_TO", @"MailboxViewController", @"Action sheet title when attempting to MARK emails") delegate:self cancelButtonTitle:nil destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_MARK_EMAILS", @"MailboxViewController", @"Action sheet option to cancel when pressed MARK emails")
				otherButtonTitles:[NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"READ", @"MailboxViewController", @"Action sheet option to mark emails as read when pressed MARK emails"), numUnread], nil
		];
		theActionSheet.tag = kActionSheetMarkRead;
	}

	if (theActionSheet != nil) {
		[theActionSheet showFromBarButtonItem:self.markButton animated:true];
	}

	return;
}

- (IBAction) toggleEdit:(id)inSender
{
	[self editingMode];
	[self redoControls];

	return;
}

- (IBAction) toggleGotoFolder:(id)inSender
{
	if (self.gotoFolder != nil) {
		[UIView animateWithDuration:kGotoFolderAnimationDuration
			animations:^{
				self.gotoFolder.view.top = self.view.bottom;
			}
			completion:^(BOOL inFinished) {
				self.tableView.scrollEnabled = true;
				[self.navigationController setToolbarHidden:NO animated:YES];
				self.editButtonItem.enabled = YES;
				[self.gotoFolder.view removeFromSuperview];
				self.gotoFolder = nil;
				[self redoTitleButton];
			}
		];
	} else {

		self.gotoFolder = [[FolderSelectController alloc] initWithNibName:kNib_FolderView_iPhone bundle:[FXLSafeZone getResourceBundle]];
		self.gotoFolder.delegate = self;
		self.gotoFolder.currentFolder = self.folder;
		self.gotoFolder.view.top = self.view.bottom;
		self.gotoFolder.view.size = self.view.size;
		[self.view addSubview:self.gotoFolder.view];
		[self.view bringSubviewToFront:self.gotoFolder.view];
		[UIView animateWithDuration:kGotoFolderAnimationDuration
			animations:^{
				self.gotoFolder.view.top = self.view.top;
			}
			completion:^(BOOL inFinished) {
				self.tableView.scrollEnabled = false;
				[self.navigationController setToolbarHidden:YES animated:YES];
				self.editButtonItem.enabled = false;
				[self redoTitleButton];
			}
		];

	}

	return;
}

- (IBAction) toggleCancel:(id)inSender
{
	[self nonEditingMode:true];
	[self redoControls];

	return;
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark - UITableView DataSource & Delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	if (self.searching) {		
		return self.emailSearchData.count;
	}
	return [self.emailData count] + 1;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.searching) {
		return NO;
	}
	return (indexPath.row < self.emailData.count);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	
	UITableViewCell *theCell = nil;
	NSString *mailboxCellIdentifier = @"MailCell";
	
	if (self.searching) {
		MailboxCell *searchCell = [self.tableView dequeueReusableCellWithIdentifier:mailboxCellIdentifier];
		NSAssert(searchCell != nil, @"mbCell is nil");
		if (self.emailSearchData && indexPath.row < self.emailSearchData.count) {
			
			searchCell.numberOfLines = self.numberOfPreviewLines;
			searchCell.email = [self.emailSearchData objectAtIndex:indexPath.row];
		}
		theCell = searchCell;
		return theCell;
	}

	if (indexPath.row < self.emailData.count) {
		MailboxCell *mbCell = (MailboxCell*)[tableView dequeueReusableCellWithIdentifier:mailboxCellIdentifier];
		NSAssert(mbCell != nil, @"mbCell is nil");
        mbCell.numberOfLines = self.numberOfPreviewLines;
		mbCell.email = [self.emailData objectAtIndex:indexPath.row];
		
		mbCell.backgroundColor = [UIColor whiteColor];
		theCell = mbCell;
	
	} else {
		// Can be one of the following
		// 1) No mail (self.emailData.count == 0)
		// 2) Loading
		// 3) More mail
		// 4) No More mail
		// -------------------------------------------------------------------------------
		bool startAnimating = false;
		if (self.folder.isInitialSync) {
            theCell = [tableView dequeueReusableCellWithIdentifier:@"FolderSyncingCell"];
			startAnimating = true;
        }else if (!self.folder.isInitialSynced && self.folder.isSyncable) {
            theCell = [tableView dequeueReusableCellWithIdentifier:@"FolderNotSyncedCell"];
        }else if (!receivedAdditionalAllMail) {
			theCell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell"];
			startAnimating = true;
		} else if (moreResultsAllMail) {
			theCell = [tableView dequeueReusableCellWithIdentifier:@"MoreMailCell"];
		} else if([self.emailData count] == 0) {
			theCell = [tableView dequeueReusableCellWithIdentifier:@"NoMailCell"];
		} else {
			theCell = [tableView dequeueReusableCellWithIdentifier:@"NoMoreMailCell"];
		}

		if (startAnimating) {
			for (UIView *theSubview in theCell.contentView.subviews) {
				if (![theSubview isKindOfClass:[UIActivityIndicatorView class]]) continue;
				UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)theSubview;
				[activityView startAnimating];
			}
		}
	}
    
    if (indexPath.row >= (self.emailData.count / 3 * 2) && moreResultsAllMail)
    {
        if (!loading)
        {
            loading = YES;
            [self runFolderSearch];
        }
    }
	NSAssert(theCell != nil, @"theCell != nil");
	return theCell;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.tableView.editing) {
		[self redoControls];
		return;
	}

	return;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (self.tableView.editing) {
		if (indexPath.row == self.emailData.count) {
			// they've selected one of the "message" cells
			[tableView deselectRowAtIndexPath:indexPath animated:YES];
		} else {
			[self redoControls];
		}
		return;
	}
	
	ASEmail *theEmail;
	if (self.searching) {
		
		theEmail = self.emailSearchData[indexPath.row];
	} else {
		// speed optimization (leads to incorrectness): cancel SearchRunner when user selects a result
		//
		[self.emailSearch setCancelled:TRUE];
		theEmail = self.emailData[indexPath.row];
	}
	
    if(theEmail.folder.folderType == kFolderTypeLocalDrafts) {
        composeMailVC *theController = [composeMailVC createMailComposerWithTo:theEmail.toAsArrayOfEmailAddresses
                                                                            cc:theEmail.ccAsArrayOfEmailAddresses
                                                                           bcc:theEmail.bccAsArrayOfEmailAddresses
																   attachments:theEmail.attachments
                                                                       account:theEmail.folder.account];
        theController.draftEmail    = theEmail;        
        theController.subject       = theEmail.subject;
        if(theEmail.emailThread.uid.length > 0) {
            NSString* aUid = theEmail.emailThread.uid;
            ASEmailFolder* anEmailFolder = (ASEmailFolder*)self.folder;
            ASEmail* anReferencedEmail = [anEmailFolder emailForUID:aUid];
            if(anReferencedEmail) {
                theController.referenceEmail = anReferencedEmail;
            }else{
                FXDebugLog(kFXLogASEmail, @"Draft referenced email not found: %@", theEmail);
            }
        }
        NSString* aString           = [[NSString alloc] initWithData:theEmail.body encoding:NSUTF8StringEncoding];
        theController.body          = aString;
        self.emailSearch.cancelled  = TRUE;
        self.emailSearch            = nil;

        [self presentViewController:theController animated:true completion:nil];
    }else{
        if (!self.mailViewerController) {
            self.mailViewerController = [mailViewerVC createMailViewerVCWithDelegate:self email:theEmail folder:self.folder];
        } else {
            self.mailViewerController.folder = self.folder;
            self.mailViewerController.email = theEmail;
        }
        FMSplitViewDelegate *splitDelegate = (FMSplitViewDelegate*)((MGSplitViewController*)self.mgSplitViewController).delegate;
        if (splitDelegate) {
            splitDelegate.detailViewController = self.mailViewerController;
            [splitDelegate updateNavigationButton];
        } else {
            [self.navigationController pushViewController:self.mailViewerController animated:YES];
        }
    }

	return;
}

//-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.tableView.editing) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (indexPath.row < [self.emailData count]) {
            ASEmail *emailToDelete = [self.emailData objectAtIndex:indexPath.row];
            [self.emailData removeObjectsInArray:@[emailToDelete]];
            
            // **** NOTE **** deleteRowsAtIndexPaths resets indexPathsForSelectedRows
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView endUpdates];
            
            [self reloadTable];
            //        [self redoControls];
            
            // now go through and delete them from ActiveSync
            NSAssert(self.folder != nil, @"self.folder != nil");
            [self.folder deleteUid:emailToDelete.uid updateServer:TRUE];
            [self.folder commitObjects];
            [self.folder setIsCountWanted:TRUE];
            
            //        [self toggleCancel:self];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// UISearchBar delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UISearchBar delegate

-(void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	self.searching = YES;
	self.searchDisplayController.searchResultsDataSource = self;
	self.searchDisplayController.searchResultsDelegate = self;
}

-(void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	[self.searchTimerManager updateTimerForSearchText:searchText];
	if (searchText.length == 0) {
		self.emailSearchData = nil;
	}
	self.emailSearchString = searchText;
	if (searchText.length > 2) {
		[self performEmailSearchFromText:searchBar.text];
	}
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[self.searchTimerManager updateTimerForSearchText:@""];
	[self performEmailSearchFromText:searchBar.text];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	self.emailSearchString = @"";
	self.emailSearchData = nil;
	self.searching = NO;
	[self.tableView reloadData];
}

////////////////////////////////////////////////////////////////////////////////////////////
// FXMSearchTimerManager delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - FXMSearchTimerManager delegate
-(void)searchTimerFired:(NSTimer *)searchTimer
{
	[self performEmailSearchFromText:self.searchDisplayController.searchBar.text];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - UIAlertView delegate

- (void) alertViewCancel:(UIAlertView *)inAlertView
{
}

- (void) alertView:(UIAlertView *)inAlertView clickedButtonAtIndex:(NSInteger)inButtonIndex
{
	if (inAlertView.tag == kAlertViewDeleteItems) {
		if (inButtonIndex == 1) {
			[self deleteSelectedRows];
		}
	}
	return;
}
#pragma mark - UIActionSheet delegate

- (void) actionSheet:(UIActionSheet *)inActionSheet clickedButtonAtIndex:(NSInteger)inButtonIndex;
{
	bool doCancelOfEdit = true;
	if (inActionSheet.tag == kActionSheetTagSort) {
		doCancelOfEdit = false;
		switch (inButtonIndex) {
			case 1: self.sortBy = sortMailByAttachments; break;
			case 2: self.sortBy = sortMailByDate; break;
	//		case 3: self.sortBy = sortMailByFlags; break;
			case 3: self.sortBy = sortMailByFrom; break;
			case 4: self.sortBy = sortMailBySize; break;
			case 5: self.sortBy = sortMailBySubject; break;
			case 6: self.sortBy = sortMailByTo; break;
			case 7: self.sortBy = sortMailByUnread; break;
		}
		[self redoControls];
		[self reloadTable];
	} else if (inActionSheet.tag == kActionSheetMarkReadUnread) {
		if (inButtonIndex == 1) {
			[self markSelectedRowsRead];
		} else if (inButtonIndex == 2) {
			[self markSelectedRowsUnread];
		}
	} else if (inActionSheet.tag == kActionSheetMarkRead) {
		if (inButtonIndex == 1) {
			[self markSelectedRowsRead];
		}
	} else if (inActionSheet.tag == kActionSheetMarkUnread) {
		if (inButtonIndex == 1) {
			[self markSelectedRowsUnread];
		}
	}

	if (doCancelOfEdit) {
		[self toggleCancel:self];
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////
// Getters and Setters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters and Setters

- (void)setFolder:(BaseFolder*)aFolder
{
    if(_folder) {
        [_folder removeDelegate:self];
    }
    if(aFolder) {
        _folder = aFolder;
        [_folder addDelegate:self];
        [self doLoad];
        //FXDebugLog(kFXLogActiveSync, @"MailboxViewController doLoad: %@", aFolder);
    }
}

-(FXMSearchTimerManager *)searchTimerManager
{
	if (!_searchTimerManager) {
		_searchTimerManager = [[FXMSearchTimerManager alloc] initWithDelegate:self invalidateAfterTwoCharacters:YES];
	}
	return _searchTimerManager;
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder
{
    [self reloadTable];
}

- (void)folderSynced:(BaseFolder*)aFolder
{
    [self runFolderSearch];
}

- (void)newObjects:(NSArray*)anObjects
{
    @try {
        [self performSelector:@selector(loadEmails:) withObject:anObjects];
    }@catch (NSException* e) {
        [self _logException:@"newObjects" exception:e];
    }
	@finally {
		[self redoUpdatedLabel];
	}
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    @try {
		ASEmail *theEmail = (ASEmail *)aBaseObject;
 		NSIndexPath *theIndexPath = [self getIndexPathForUID:theEmail.uid];
		if (theIndexPath != nil) {
			[self.tableView beginUpdates];
			[self.tableView reloadRowsAtIndexPaths:@[theIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView endUpdates];
			[self.folder setIsCountWanted:TRUE];
		} else {
            FXDebugLog(kFXLogActiveSync, @"changeObject failed: %@", aBaseObject);
        }
    }@catch (NSException* e) {
        [self _logException:@"changeObject" exception:e];
    } 
	@finally {
		[self redoUpdatedLabel];
	}
}

/*
- (void)changeObject:(BaseObject*)aBaseObject
{
    @try {
        BOOL found = false;
        ASEmail* aChangedEmail = (ASEmail*)aBaseObject;
        NSString* aUid = aChangedEmail.uid;
        for(int i = 0; i < [self.emailData count]; i++) {
            ASEmail* anEmail = [self.emailData objectAtIndex:i];
            NSString* anEmailUid = anEmail.uid;
            if([aUid isEqualToString:anEmailUid]) {
#warning FIXME shouldn't this get the original object and not create a new one?
                [self.emailData replaceObjectAtIndex:i withObject:aChangedEmail];
                NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                found = TRUE;
                break;
            }
        }
        if(!found) {
            FXDebugLog(kFXLogActiveSync, @"changeObject failed: %@", aBaseObject);
        }
    }@catch (NSException* e) {
        [self _logException:@"changeObject" exception:e];
    } 
}
*/
- (void)deleteUid:(NSString*)inUID
{
	// This routine might be called in following circumstances
	// 1) toggleDelete hit, ASSync does it's stuff, and calls us back but
	//    probably no cells found since we've animated them out in toggleDelete
	// 2) User hits trash-can icon in mailViewerVC and cell *IS* found
	//    -> if iPhone, then this wouldn't even be visible
	//    -> if iPad, then it would
	// -------------------------------------------------------------------------------
	@try {
		NSIndexPath *theIndexPath = [self getIndexPathForUID:inUID];
		if (theIndexPath != nil) {
            NSInteger aRow = theIndexPath.row;
            [self.emailData removeObjectAtIndex:aRow];
			[self.tableView beginUpdates];
			[self.tableView deleteRowsAtIndexPaths:@[theIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			[self.tableView endUpdates];

			[self.folder setIsCountWanted:TRUE];
		}
    }@catch (NSException* e) {
        [self _logException:@"deleteUid" exception:e];
    } 
/*
    @try {
        BOOL found = false;
        for(int i = 0; i < [self.emailData count]; i++) {
            ASEmail* email = [self.emailData objectAtIndex:i];
            NSString* anEmailUid = email.uid;
            if([aUid isEqualToString:anEmailUid]) {
                [self.emailData removeObjectAtIndex:i];
                NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                found = TRUE;
                break;
            }
        }
        if(!found) {
            FXDebugLog(kFXLogActiveSync, @"deleteUid failed: %@", aUid);
        }
    }@catch (NSException* e) {
        [self _logException:@"deleteUid" exception:e];
    }
*/
}

#pragma mark - Private methods

- (void) redoControls
{
	if (self.tableView.editing) {
		int markedCount = self.tableView.indexPathsForSelectedRows.count;
		if (markedCount == 0) {
			self.deleteButton.title = FXLLocalizedStringFromTable(@"DELETE_MESSAGE_OPTION", @"MailboxViewController", @"Delete button when in Edit Mailbox mode");
			self.moveButton.title = FXLLocalizedStringFromTable(@"MOVE", @"MailboxViewController", @"Move button when in Edit Mailbox mode");
			self.markButton.title = FXLLocalizedStringFromTable(@"MARK", @"MailboxViewController", @"Mark button when in Edit Mailbox mode");
		} else {
			self.deleteButton.title = [NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"DELETE_MESSAGE_OPTION", @"MailboxViewController", @"Delete button when in Edit Mailbox mode"), markedCount];
			self.moveButton.title = [NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"MOVE", @"MailboxViewController", @"Move button when in Edit Mailbox mode"), markedCount];
			self.markButton.title = [NSString stringWithFormat:@"%@ (%d)", FXLLocalizedStringFromTable(@"MARK", @"MailboxViewController", @"Mark button when in Edit Mailbox mode"), markedCount];
		}
		self.deleteButton.enabled = (markedCount > 0);
		self.moveButton.enabled = (markedCount > 0);
		self.markButton.enabled = (markedCount > 0);

	} else {
		[self redoUpdatedLabel];
		[self redoSortControls];
	}

	return;
}

- (void) redoUpdatedLabel
{
	NSString *updateString = @"";
	NSDate *updateDate = ((ASFolder *)self.folder).syncDate;
	if (updateDate == nil) {
		updateString = FXLLocalizedStringFromTable(@"NEVER_UPDATED", @"MailboxViewController", @"Label indicating never been updated/synced");
	} else {
		DateUtil *ds = [DateUtil getSingleton];
		NSString *lastUpdatedString = FXLLocalizedStringFromTable(@"LAST_UPDATED_PREFIX", @"MailboxViewController", @"Prefix label for when the mailbox was last updated/synced");
		NSString *dateString = [ds stringFromDateWeekdayAndDateTimeShortStyle:updateDate];
		updateString = [NSString stringWithFormat:@"%@ %@", lastUpdatedString, dateString];
	}

	if (self.updatedStatusButtonItem == nil) {
		self.updatedStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
		self.updatedStatusLabel.backgroundColor = [UIColor clearColor];
		self.updatedStatusLabel.font = [UIFont systemFontOfSize:13.0];
		self.updatedStatusLabel.textColor = [UIColor whiteColor];
		self.updatedStatusLabel.textAlignment = NSTextAlignmentCenter;
		self.updatedStatusLabel.text = updateString;
		[self.updatedStatusLabel sizeToFit];
		self.updatedStatusButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.updatedStatusLabel];
	} else {
		self.updatedStatusLabel.text = updateString;
		[self.updatedStatusLabel sizeToFit];
	}

	return;
}

- (void) redoSortControls
{
	UIImage *theImage = nil;
	if (self.sortOrder == sortMailOrderAscending) theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowWhiteUp.png"];
	else theImage = [UIImage imageNamed:@"FixMailRsrc.bundle/arrowWhiteDown.png"];

	if (self.sortOrderButton == nil) {
		self.sortOrderButton = [[UIBarButtonItem alloc] initWithImage:theImage style:UIBarButtonItemStyleBordered target:self action:@selector(toggleSort:)];
	} else {
		self.sortOrderButton.image = theImage;
	}

	return;
}

- (void) editingMode
{
	// these should only be present in editing mode
	// -------------------------------------------------------------------------------
	self.sortOrderButton = nil;
	self.composeButton = nil;

	UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(toggleCancel:)];
	[self.navigationItem setRightBarButtonItem:rightButton animated:true];

    self.deleteButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"DELETE_MESSAGE_OPTION", @"MailboxViewController", @"Delete button when in Edit Mailbox mode") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleDelete:)];
 	self.deleteButton.tintColor = [UIColor colorWithRed:(177.0/255.0) green:(25.0/255.0) blue:(38.0/255.0) alpha:1.0];
	self.deleteButton.width = 95.0;

	self.moveButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"MOVE", @"MailboxViewController", @"Move button when in Edit Mailbox mode") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleMove:)];
	self.moveButton.width = 95.0;

	UIBarButtonItem	*spacerItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	
	self.markButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"MARK", @"MailboxViewController", @"Mark button when in Edit Mailbox mode") style:UIBarButtonItemStyleBordered target:self action:@selector(toggleMark:)];
	self.markButton.width = 95.0;

	NSArray *toolbarItems = [NSArray arrayWithObjects:spacerItem, self.deleteButton, spacerItem, self.markButton, spacerItem, self.moveButton, spacerItem, nil];
	[self setToolbarItems:toolbarItems animated:true];
	[self.navigationItem setHidesBackButton:true animated:true];

	// this will do the enabling of the indentation/checkmarks
	// -------------------------------------------------------------------------------
    [self.tableView setEditing:YES animated:YES];

	return;
}

- (void) nonEditingMode:(bool)inAnimated
{
	// these should only be present in editing mode
	// -------------------------------------------------------------------------------
	self.deleteButton = nil;
	self.moveButton = nil;
	self.markButton = nil;

    // the top-nav should have the 'edit' button which will allow multi-selection
	// -------------------------------------------------------------------------------
	UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(toggleEdit:)];
	[self.navigationItem setRightBarButtonItem:rightButton animated:inAnimated];
    
	[self redoSortControls];
	[self redoUpdatedLabel];
    [self setUpNonEditingModeToolbarForInterfaceOrientation:self.interfaceOrientation animated:inAnimated];

	// make sure we're not editing
	// -------------------------------------------------------------------------------
	[self.tableView setEditing:NO animated:inAnimated];
	[self.navigationItem setHidesBackButton:false animated:true];

	return;
}

- (void) reloadTable
{
    self.numberOfPreviewLines = [AppSettings emailPreviewLines];
    
	NSArray *sortedArray = [self.emailData sortedArrayUsingComparator: ^(id obj1, id obj2) {
	 
		ASEmail *email1 = obj1;
		ASEmail *email2 = obj2;
		id s1, s2;
		switch (self.sortBy) {
			case sortMailByAttachments:
				s1 = [NSNumber numberWithBool:email1.hasAttachments];
				s2 = [NSNumber numberWithBool:email2.hasAttachments];
				break;
			default:
			case sortMailByDate:
				s1 = email1.datetime;
				s2 = email2.datetime;
				break;
	//		case sortMailByFlags:
			case sortMailByFrom:
				s1 = email1.from;
				s2 = email2.from;
				break;
			case sortMailBySize:
				s1 = [NSNumber numberWithInt:email1.estimatedSize];
				s2 = [NSNumber numberWithInt:email2.estimatedSize];
				break;
			case sortMailBySubject:
				s1 = email1.subject;
				s2 = email2.subject;
				break;
			case sortMailByTo:
				s1 = email1.displayTo;
				s2 = email2.displayTo;
				break;
			case sortMailByUnread:
				s1 = [NSNumber numberWithBool:email1.isUnread];
				s2 = [NSNumber numberWithBool:email2.isUnread];
				break;
		}

		// default is ascending
		NSComparisonResult comparisonResult = NSOrderedSame;
		if (self.sortOrder == sortMailOrderAscending) {
			comparisonResult = [s1 compare:s2];
		} else {
			comparisonResult = [s2 compare:s1];
		}
		return comparisonResult;
	}];

	[self.emailData removeAllObjects];
	[self.emailData addObjectsFromArray:sortedArray];

	[self.tableView reloadData];
}

- (void) markSelectedRowsUnread
{
	bool isChanged = false;
	for (NSIndexPath *theIndexPath in self.tableView.indexPathsForSelectedRows) {
		ASEmail *theEmail = self.emailData[theIndexPath.row];
		if (theEmail.isUnread) continue;
		[theEmail changeReadStatus:false];
		isChanged = true;
	}

	if (isChanged) {
		[self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForSelectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
		[self redoControls];
		[self.folder setIsCountWanted:TRUE];
	}

	return;
}

- (void) markSelectedRowsRead
{
	bool isChanged = false;
	for (NSIndexPath *theIndexPath in self.tableView.indexPathsForSelectedRows) {
		ASEmail *theEmail = self.emailData[theIndexPath.row];
		if (theEmail.isRead) continue;
		[theEmail changeReadStatus:true];
		isChanged = true;
	}

	if (isChanged) {
		[self.tableView reloadRowsAtIndexPaths:self.tableView.indexPathsForSelectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
		[self redoControls];
		[self.folder setIsCountWanted:TRUE];
	}

	return;
}

- (void) deleteSelectedRows
{
	// get rid of objects from array
	NSMutableArray *deletedObjects = [NSMutableArray new];
	for (NSIndexPath *theIndexPath in self.tableView.indexPathsForSelectedRows) {
		[deletedObjects addObject:[self.emailData objectAtIndex:theIndexPath.row]];
	}
	[self.emailData removeObjectsInArray:deletedObjects];

	// **** NOTE **** deleteRowsAtIndexPaths resets indexPathsForSelectedRows
	[self.tableView beginUpdates];
	[self.tableView deleteRowsAtIndexPaths:self.tableView.indexPathsForSelectedRows withRowAnimation:UITableViewRowAnimationFade];
	[self.tableView endUpdates];

	[self reloadTable];
	[self redoControls];

	// now go through and delete them from ActiveSync
	NSAssert(self.folder != nil, @"self.folder != nil");
	for (ASEmail *theEmail in deletedObjects) {
		[self.folder deleteUid:theEmail.uid updateServer:TRUE];
	}
    [self.folder commitObjects];
    [self.folder setIsCountWanted:TRUE];

	[self toggleCancel:self];
	return;
}

- (NSIndexPath *) getIndexPathForUID:(NSString*)inUID
{
	NSIndexPath *theIndexPath = nil;
	for(int theIndex = 0; theIndex < self.emailData.count; theIndex++) {
		ASEmail *theEmail = self.emailData[theIndex];
		if (![theEmail.uid isEqualToString:inUID]) continue;
		// found it
		theIndexPath = [NSIndexPath indexPathForRow:theIndex inSection:0];
		break;
	}

	return theIndexPath;
}

- (CGFloat) calculateRowHeight
{
	CGFloat theHeight = 44.0;
	switch ([AppSettings emailPreviewLines]) {
		case 0: theHeight = 44.0; break;
		case 1: theHeight = 66.0; break;
		default:
		case 2: theHeight = 84.0; break;
		case 3: theHeight = 100.0; break;
		case 4: theHeight = 116.0; break;
		case 5: theHeight = 132.0; break;
	}
	return theHeight;
}

- (void) redoTitleButton
{
	UIButton *theButton = nil;
	if (self.gotoFolder != nil) theButton = [UIButton createArrowUpWithTitle:self.title];
	else theButton = [UIButton createArrowDownWithTitle:self.title];
	[theButton addTarget:self action:@selector(toggleGotoFolder:) forControlEvents:UIControlEventTouchUpInside];

	self.navigationItem.titleView = theButton;
}

- (void) replaceDetailWithPlaceholder
{
	FMSplitViewDelegate *splitDelegate = (FMSplitViewDelegate*)((MGSplitViewController*)self.mgSplitViewController).delegate;
	NSAssert(splitDelegate != nil, @"splitDelegate != nil");
	if (!splitDelegate.detailViewController) {
		splitDelegate.detailViewController = [[PlaceholderSplitviewDetailViewController alloc] initWithNibName:@"PlaceholderSplitviewDetailView" bundle:[FXLSafeZone getResourceBundle]];
		((PlaceholderSplitviewDetailViewController *)splitDelegate.detailViewController).composeDelegate = self;
	}
}

-(void)performEmailSearchFromText:(NSString *)text
{
	[self.activityIndicator startAnimating];
	[self.view bringSubviewToFront:self.activityIndicator];
	self.activityIndicator.hidden = NO;
	EmailSearch *emailSearch = [[EmailSearch alloc] initWithFolder:self.folder delegate:nil];
	self.emailSearchData = [[SearchRunner getSingleton] performEmailAddressSearch:emailSearch usingText:text];
	[self.activityIndicator stopAnimating];
	[self.searchDisplayController.searchResultsTableView reloadData];
}

#pragma mark - FolderViewDelegate Methods

- (void) moveItems:(FolderSelectController *)inController didSelectFolder:(BaseFolder *)inFolder
{
	if (inFolder == nil) { // a nil folder will be the user hitting the cancel button
		[self dismissViewControllerAnimated:YES completion:^{
			[self toggleCancel:self];
		}];
		return;
	}

    NSArray *emailsToMove = [self itemsForSelectedRows];
    ASMoveItems *aMoveItems =[ASMoveItems sendMove:(ASAccount*)inFolder.account emails:emailsToMove toFolder:(ASFolder*)inFolder displayErrors:NO];
    aMoveItems.delegate = self;

	if (IS_IPAD()) {
		[self.navigationController popViewControllerAnimated:YES];
		[self toggleCancel:self];
	} else {
		//__weak id weakSelf = self;
		[self dismissViewControllerAnimated:YES completion:^{
			[self toggleCancel:self];
		}];
	}
}

- (void) gotoFolder:(FolderSelectController *)inController didSelectFolder:(BaseFolder *)inFolder
{
	bool doCancel = false;
	if (inFolder != nil) { // a nil folder will be the user hitting the cancel button

		MailboxViewController *newController = [MailboxViewController controller];
		if (newController != nil) {
			newController.folder = inFolder;
			newController.title = inFolder.displayName;
			newController.mgSplitViewController = self.mgSplitViewController;
			newController.navigationItem.leftBarButtonItem = self.navigationItem.leftBarButtonItem;

			[UIView animateWithDuration:kGotoFolderAnimationDuration
				animations:^{
					self.gotoFolder.view.top = self.view.bottom;
				}
				completion:^(BOOL inFinished) {
					// put in the new controller AFTER - if you do it before, no animation of
					// the FolderSelect window
					[self.navigationController setViewControllers:@[newController] animated:NO];

					if (IS_IPAD()) {
						FMSplitViewDelegate *splitDelegate = (FMSplitViewDelegate*)((MGSplitViewController*)self.mgSplitViewController).delegate;
						NSAssert(splitDelegate != nil, @"splitDelegate != nil");
						splitDelegate.detailViewController = [[PlaceholderSplitviewDetailViewController alloc] initWithNibName:@"PlaceholderSplitviewDetailView" bundle:[FXLSafeZone getResourceBundle]];
						((PlaceholderSplitviewDetailViewController *)splitDelegate.detailViewController).composeDelegate = self;
						[splitDelegate updateNavigationButton];
					}

					self.tableView.scrollEnabled = true;
					[self.navigationController setToolbarHidden:NO animated:YES];
					self.editButtonItem.enabled = YES;
					[self.gotoFolder.view removeFromSuperview];
					self.gotoFolder = nil;
				}
			];
		} else {
			doCancel = true;
		}
	}

	if (doCancel) {
		// it's cancelled, so just animate it down
		[UIView animateWithDuration:kGotoFolderAnimationDuration
			animations:^{
				self.gotoFolder.view.top = self.view.bottom;
			}
			completion:^(BOOL inFinished) {
				self.tableView.scrollEnabled = true;
				[self.navigationController setToolbarHidden:NO animated:YES];
				self.editButtonItem.enabled = YES;
				[self.gotoFolder.view removeFromSuperview];
				self.gotoFolder = nil;
			}
		];
	}

	return;
}

-(void)folderViewController:(FolderSelectController*)inController didSelectFolder:(BaseFolder*)folder
{
	if (inController == self.moveItemsFolder) {
		[self moveItems:inController didSelectFolder:folder];
		self.moveItemsFolder = nil;
	} else if (inController == self.gotoFolder) {
		[self gotoFolder:inController didSelectFolder:folder];
	}

	return;
}

-(NSArray*)itemsForSelectedRows
{
    NSArray *indexPaths = [self.tableView indexPathsForSelectedRows];
    NSMutableArray *itemsForPaths = [NSMutableArray arrayWithCapacity:[indexPaths count]];
    for (NSIndexPath *ip in indexPaths) {
        [itemsForPaths addObject:[self.emailData objectAtIndex:ip.row]];
    }
    return [NSArray arrayWithArray:itemsForPaths];
}

////////////////////////////////////////////////////////////////////////////////
//  ASMoveItemsDelegate Methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark - ASMoveItemsDelegate Methods

- (void)moveItemsSuccess:(ASMoveItems*)aMoveItems
{
	// Always gives an HTTP_ERROR -- [HttpRequest postHttpError] -- the move doesn't seem to happen on the server!!
	for (ASEmail *email in aMoveItems.emails) {
		[self.emailData removeObjectAtIndex:[self.emailData indexOfObject:email]];
	}
	[self.tableView reloadData];
}

- (void)moveItemsFailed:(ASMoveItems*)aMoveItems
{
#warning FIXME moveItemsFailed
    [aMoveItems displayError];
    switch(aMoveItems.statusCodeAS) {
        case kMoveItemsSuccess:
            break;
        case kMoveItemsInvalidSourceID:
            break;
        case kMoveItemsInvalidDestinationID:
            break;
        case kMoveItemsSameSourceAndDestination:
            break;
        case kMoveItemsLockedOrMoreThanOneDestination:
             break;
        case kMoveItemsLocked:
            break;
    }
}

@end
