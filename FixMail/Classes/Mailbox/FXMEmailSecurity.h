/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMEmailSecurity.h
 *
 *  Created by Colin Biggin on 2013-05-06.
 */

#import <Foundation/Foundation.h>

@interface FXMEmailSecurity : NSObject

+ (NSString*) signMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase;
+ (NSString*) encryptMessage:(NSString*)mime withRecipients:(NSDictionary*)recipients;
+ (void) decryptAndValidateMessage:(NSString*)mime withPrivateCertificateData:(NSData*)data andPassphrase:(NSString*)passphrase completion:(void (^)(NSString *newMimeContent, BOOL isSignatureValid, BOOL didDecryptionSucceed, NSString *signedCertificate))completion;
+ (BOOL) hasCertificateForEmail:(NSString *)emailAddress;
+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withPublic:(NSString *)pemRepresentation;
+ (BOOL) saveCertificateForEmail:(NSString *)emailAddress withP12Data:(NSData*)data withPassphrase:(NSString*)passphrase;
+ (void) deleteCertificateForEmail:(NSString *)emailAddress;
+ (NSString *) pemStringForEmail:(NSString *)emailAddress;
+ (NSString *) valueForAttribute:(NSString*)attribute email:(NSString *)emailAddress;
+ (NSArray *) getAllAttributes:(NSString *)emailAddress;
+ (NSArray *) getAllAttributesForP12Data:(NSData *)inData passPhrase:(NSString *)inPassPhrase;
+ (NSString *) emailAttributeForPEMString:(NSString *)inPEMString;

+ (BOOL) hasUserCertificate;
+ (BOOL) saveUserCertificateForEmail:(NSString *)inEmail withP12Data:(NSData *)inData withPassphrase:(NSString*)inPassphrase;
+ (void) deleteUserCertificate;
+ (NSString *) valueForUserAttribute:(NSString*)attribute;
+ (NSArray *) getAllUserAttributes;

+ (void) dumpSettingsDB;

@end
