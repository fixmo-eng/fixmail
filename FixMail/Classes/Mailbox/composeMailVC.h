//
//  composeMailVC.h
//  FixMail
//
//  Created by Colin Biggin on 2013-02-15.
//
//
#import "SZCAddressView.h"
#import "SZCAttachmentView.h"
#import "SZCTextEntryView.h"
#import "SZLAttachmentBrowserController.h"
 
@class BaseAccount;
@class ASEmail;
@class PKMIMEData;
@class EmailAddress;

typedef enum
{
	composeNew = 10,
	composeReply,
	composeForward

} EComposeMailType;

@interface composeMailVC : UIViewController <SZCAddressViewDelegate, SZCAttachmentViewDelegate,  SZCTextEntryViewDelegate, SZLAttachmentBrowserDelegate, UIGestureRecognizerDelegate, UIWebViewDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) BaseAccount *account;
@property (nonatomic, assign) EComposeMailType mailType;
@property (nonatomic, strong) EmailAddress *from;
@property (nonatomic, strong) NSMutableArray *toList;
@property (nonatomic, strong) NSMutableArray *ccList;
@property (nonatomic, strong) NSMutableArray *bccList;
@property (nonatomic, strong) NSMutableArray *attachmentList;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) ASEmail *referenceEmail;
@property (nonatomic, strong) NSData *referenceEmailSMimeData;
@property (nonatomic, strong) ASEmail *draftEmail;

@property (nonatomic, weak) IBOutlet UINavigationItem *navItem;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *cancelButton;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet SZCAddressView *toView;
@property (nonatomic, weak) IBOutlet SZCAddressView *ccView;
@property (nonatomic, weak) IBOutlet SZCAddressView *bccView;
@property (nonatomic, weak) IBOutlet SZCTextEntryView *fromView;
@property (nonatomic, weak) IBOutlet SZCTextEntryView *subjectView;
@property (nonatomic, weak) IBOutlet UIButton *attachmentButton;

@property (nonatomic, weak) IBOutlet UIView *attachmentsView;
@property (nonatomic, weak) IBOutlet UIButton *attachmentsDisclosureButton;
@property (nonatomic, weak) IBOutlet UIImageView *attachmentsArrows;
@property (nonatomic, weak) IBOutlet UILabel *attachmentsLabel;

@property (nonatomic, weak) IBOutlet UITextView *bodyView;
@property (nonatomic, weak) IBOutlet UIWebView *bodyWebview;

@property (nonatomic, weak) IBOutlet UIView *toSeparator;
@property (nonatomic, weak) IBOutlet UIView *ccSeparator;
@property (nonatomic, weak) IBOutlet UIView *bccSeparator;
@property (nonatomic, weak) IBOutlet UIView *fromSeparator;
@property (nonatomic, weak) IBOutlet UIView *subjectSeparator;
@property (nonatomic, weak) IBOutlet UIView *attachmentSeparator;

- (IBAction) toggleCancel:(id)inSender;
- (IBAction) toggleSend:(id)inSender;
- (IBAction) toggleAddAttachment:(id)inSender;
- (IBAction) toggleSignMessage:(id)inSender;
- (IBAction) toggleEncryptMessage:(id)inSender;
- (IBAction) toggleAttachmentDisclosure:(id)inSender;

// Construct/Destruct
+ (composeMailVC *) createMailComposerWithTo:(NSArray *)inToList cc:(NSArray *)inCCList bcc:(NSArray *)inBCCList attachments:(NSArray *)inAttachments account:(BaseAccount*)inAccount;

+ (UINavigationController *) createNavigationMailComposerWithTo:(NSArray *)inToList cc:(NSArray *)inCCList bcc:(NSArray *)inBCCList attachments:(NSArray *)inAttachments account:(BaseAccount*)inAccount;

// Greater SafeZone methods

+(BOOL)urlHasEmailScheme:(NSURL *)url;
-(void)applyEmailSchemeURL:(NSURL *)url;

@end
