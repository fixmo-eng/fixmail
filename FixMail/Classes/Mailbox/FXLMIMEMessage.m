//
//  FXLMIMEMessage.m
//  FixMail
//
//  Created by Colin Biggin on 2013-05-31.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//
#import "FXLMIMEMessage.h"
#import "AttachmentUtils.h"
#import "DateUtil.h"
#import "EmailAddress.h"
#import "FXMEmailSecurity.h"
#import "NSData+Base64.h"
#import "ASAccount.h"
#import "ASCompose.h"
#import "ASEmail.h"
#import "ASFolder.h"
#import "BaseAccount.h"

#define kAttachmentPathKey		@"path"
#define kAttachmentFileNameKey	@"name"

@interface FXLMIMEMessage ()

@property (nonatomic, strong) NSMutableArray *toAddresses;
@property (nonatomic, strong) NSMutableArray *ccAddresses;
@property (nonatomic, strong) NSMutableArray *bccAddresses;
@property (nonatomic, strong) NSMutableArray *attachmentFiles;

@property (nonatomic, strong) EmailAddress *fromAddress;
@property (nonatomic, strong) ASEmail *draftEmail;
@property (nonatomic, strong) NSString *subject;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *plainTextPart;
@property (nonatomic, strong) NSString *htmlTextPart;

//@property (nonatomic, strong) NSString *partBoundaryString;

@end

#pragma mark -

@implementation FXLMIMEMessage

static NSOperationQueue *sOperationQueue = nil;

+ (NSOperationQueue *) operationQueue
{
    if(!sOperationQueue) {
        sOperationQueue = [[NSOperationQueue alloc] init];
        [sOperationQueue setMaxConcurrentOperationCount:1];
    }

    return sOperationQueue;
}


+ (FXLMIMEMessage *) newMessage
{
	srandom(time(nil));
	return [[FXLMIMEMessage alloc] init];
}

- (void) queueMessage:(ASEmail *)inDraftEmail
{
	NSBlockOperation *theOperation = [NSBlockOperation blockOperationWithBlock:^(void) {

		[[NSNotificationCenter defaultCenter] postNotificationName:kSendEmailStartedNotification object:self];

		self.draftEmail = inDraftEmail;

		EFXLMIMEError theError;
        NSNotification *errorNotification = nil;

        NSString *mimeString = [self mimeBody:&theError];
        if (theError != errorNone) {
                errorNotification = [NSNotification notificationWithName:kSendEmailErrorNotification
                                                                  object:self
                                                                userInfo:@{ kSendEmailErrorMessageUserInfo : [self errorMessage:theError] }];
        }

        NSString *mimeHeader = [self mimeHeader:&theError];
        if (theError != errorNone) {
                errorNotification = [NSNotification notificationWithName:kSendEmailErrorNotification
																object:self
															  userInfo:@{ kSendEmailErrorMessageUserInfo : [self errorMessage:theError] }];
        }
        
        
        if (!errorNotification) {
			ASAccount *theAccount = (ASAccount *)[BaseAccount currentLoggedInAccount];
			NSAssert(theAccount != nil, @"theAccount != nil");
            
            if (self.signMessage) {
				NSString *newMimeString = [FXMEmailSecurity signMessage:mimeString withPrivateCertificateData:nil andPassphrase:nil];
					if (newMimeString.length == 0) {
                        errorNotification = [NSNotification notificationWithName:kSendEmailErrorNotification
                                                                          object:self
                                                                        userInfo:@{ kSendEmailErrorMessageUserInfo : [self errorMessage:errorCouldNotSign] }];
					} else {
                        mimeString = newMimeString;
					}
			}
            
            if (self.encryptMessage) {
				// ok, go ahead
				NSDictionary *recipientDictionary = [self generateRecipientCertificates];
				NSString *newMimeString = [FXMEmailSecurity encryptMessage:mimeString withRecipients:recipientDictionary];
				if (newMimeString.length == 0) {
                    errorNotification = [NSNotification notificationWithName:kSendEmailErrorNotification
                                                                      object:self
                                                                    userInfo:@{ kSendEmailErrorMessageUserInfo : [self errorMessage:errorCouldNotEncrypt] }];
				} else {
                    mimeString = newMimeString;
				}
			}

            if (!errorNotification) {
                mimeString = [mimeHeader stringByAppendingString:mimeString];
				[ASCompose sendMail:theAccount string:mimeString delegate:self];
			}
		}

        if (errorNotification) {
            [[NSNotificationCenter defaultCenter] postNotification:errorNotification];
        }

	}];

    [[FXLMIMEMessage operationQueue] addOperation:theOperation];

	return;
}

- (id) init
{
	self = [super init];
	if (!self) return nil;

	self.toAddresses = [[NSMutableArray alloc] init];
	self.ccAddresses = [[NSMutableArray alloc] init];
	self.bccAddresses = [[NSMutableArray alloc] init];
	self.attachmentFiles = [[NSMutableArray alloc] init];
	self.signMessage = false;
	self.encryptMessage = false;

	return self;
}

- (void) addToAddress:(EmailAddress *)inAddress
{
	if (inAddress == nil) return;
	[self.toAddresses addObject:inAddress];
}

- (void) addToAddresses:(NSArray *)inAddresses
{
	if (inAddresses.count == 0) return;
	for (EmailAddress *theAddress in inAddresses) [self addToAddress:theAddress];
}

- (void) addCCAddress:(EmailAddress *)inAddress
{
	if (inAddress == nil) return;
	[self.ccAddresses addObject:inAddress];
}

- (void) addCCAddresses:(NSArray *)inAddresses
{
	if (inAddresses.count == 0) return;
	for (EmailAddress *theAddress in inAddresses) [self addCCAddress:theAddress];
}

- (void) addBCCAddress:(EmailAddress *)inAddress
{
	if (inAddress == nil) return;
	[self.bccAddresses addObject:inAddress];
}

- (void) addBCCAddresses:(NSArray *)inAddresses
{
	if (inAddresses.count == 0) return;
	for (EmailAddress *theAddress in inAddresses) [self addBCCAddress:theAddress];
}

- (void) addFromAddress:(EmailAddress *)inAddress
{
	if (inAddress == nil) return;
	self.fromAddress = inAddress;
}

- (void) addSubject:(NSString *)inSubject
{
	if (inSubject.length == 0) return;
	self.subject = [inSubject copy];
}

- (void) addDate:(NSString *)inDateString
{
	if (inDateString.length == 0) return;
	self.date = [inDateString copy];
}

- (void) addPlainTextPart:(NSString *)inText
{
	if (inText.length == 0) return;
	self.plainTextPart = [inText copy];
}

- (void) addHTMLTextPart:(NSString *)inText
{
	if (inText.length == 0) return;
	self.htmlTextPart = [inText copy];
}

- (void) addAttachmentPath:(NSString *)inFilePath name:(NSString *)inName
{
	if (inFilePath.length == 0) return;
	if (inName.length == 0) return;
	[self.attachmentFiles addObject: @{ kAttachmentPathKey : inFilePath, kAttachmentFileNameKey : inName} ];
}

#define kMaxLineLength		78

- (NSString *) getAddressString:(NSArray *)inList prefix:(NSString *)inPrefixString
{
	if (inPrefixString.length == 0) return nil;
	if (inList.count == 0) return nil;

	// have to comma separate them
	bool firstFlag = true;
	NSString *theString = [inPrefixString stringByAppendingString:@": "];

	int lineLength = theString.length;
	for (EmailAddress *theAddress in inList) {
		NSString *tmpString = theAddress.fullEmailAddress;
		if (firstFlag) {
			theString = [theString stringByAppendingString:tmpString];
			lineLength = theString.length;
			firstFlag = false;
		} else {
			if ((lineLength + tmpString.length) > (kMaxLineLength-1)) {
				theString = [theString stringByAppendingFormat:@",\r\n    %@", tmpString];
				lineLength = 4 + tmpString.length;
			} else {
				theString = [theString stringByAppendingFormat:@", %@", tmpString];
				lineLength += (2 + tmpString.length);
			}
		}
	}
	theString = [theString stringByAppendingString:@"\r\n"];

	return theString;
}

- (NSString*)mimeHeader:(EFXLMIMEError *)outError
{
    // some basic error stuff first
	if (self.fromAddress == 0) {
		*outError = errorMissingFromAddress;
		return nil;
	}
	if (self.toAddresses.count == 0) {
		*outError = errorMissingToAddress;
		return nil;
	}
    
	if (self.date.length == 0) self.date = [[DateUtil getSingleton] rfcStringWithDate:nil];
	if (self.subject == nil) self.subject = @"";
    
	NSMutableString *messageHeader = [[NSMutableString alloc] init];
    
	// for compliance to syntax checker
	[messageHeader appendFormat:@"From: %@\r\n", self.fromAddress.fullEmailAddress];
	[messageHeader appendFormat:@"Return-Path: <%@>\r\n", self.fromAddress.address];
    
	if (self.toAddresses.count > 0) [messageHeader appendString:[self getAddressString:self.toAddresses prefix:@"To"]];
	if (self.ccAddresses.count > 0) [messageHeader appendString:[self getAddressString:self.ccAddresses prefix:@"Cc"]];
	if (self.bccAddresses.count > 0) [messageHeader appendString:[self getAddressString:self.bccAddresses prefix:@"Bcc"]];
	[messageHeader appendFormat:@"Subject: %@\r\n", self.subject];
	[messageHeader appendFormat:@"Date: %@\r\n", self.date];
	[messageHeader appendString:@"MIME-Version: 1.0\r\n"];
    
	*outError = errorNone;
	return messageHeader;
}

- (NSString*)mimeBody:(EFXLMIMEError *)outError
{
    NSMutableString *messageBody = [[NSMutableString alloc] init];

    // we approach
	if (self.attachmentFiles.count > 0) {
		NSString *partBoundaryString = [self generateRandomBoundary];
		[messageBody appendFormat:@"Content-Type: multipart/mixed; boundary=\"%@\"\r\n\r\n", partBoundaryString];
        
		[messageBody appendFormat:@"--%@\r\n", partBoundaryString];
		if ((self.htmlTextPart.length > 0) && (self.plainTextPart.length > 0)) {
            
			[messageBody appendString:[self generateMultiPartBody]];
            
		} else if (self.plainTextPart.length > 0) {
			[messageBody appendString:@"Content-Type: text/plain\r\nContent-Transfer-Encoding: 7bit\r\n\r\n"];
			[messageBody appendFormat:@"%@\r\n", self.plainTextPart];
            
		} else if (self.htmlTextPart.length > 0) {
			[messageBody appendString:@"Content-Type: text/html\r\nContent-Transfer-Encoding: 8bit\r\n\r\n"];
			[messageBody appendFormat:@"%@\r\n", self.htmlTextPart];
		}
        
		for (NSDictionary *theAttachment in self.attachmentFiles) {
			[messageBody appendFormat:@"--%@\r\n", partBoundaryString];
			[messageBody appendString:[self generateAttachmentBody:theAttachment]];
		}
        
		[messageBody appendFormat:@"--%@--\r\n", partBoundaryString];
        
	} else if ((self.plainTextPart.length > 0) && (self.htmlTextPart.length > 0)) {
        
		[messageBody appendString:[self generateMultiPartBody]];
        
	} else if (self.plainTextPart.length > 0) {
		[messageBody appendString:@"Content-Type: text/plain\r\n\r\n"];
		[messageBody appendFormat:@"%@\r\n", self.plainTextPart];
        
	} else if (self.htmlTextPart.length > 0) {
		[messageBody appendString:@"Content-Type: text/html\r\n\r\n"];
		[messageBody appendFormat:@"%@\r\n", self.htmlTextPart];
	}
    
	*outError = errorNone;
	return messageBody;
}

//
// For testing purposes, we're using the syntax checker at:
// http://tools.ietf.org/tools/msglint/msglint
//
// Couple things it wants to have:
// Return-Path: <blah@blah.com>
//
- (NSString *) mimeString:(EFXLMIMEError *)outError
{
    *outError = errorNone;
    NSString *mimeMessage = [self mimeHeader:outError];
    if (*outError == errorNone) {
        mimeMessage = [mimeMessage stringByAppendingString:[self mimeBody:outError]];
    }
    
    return mimeMessage;
}

#pragma mark - Utility methods

- (NSString *) generateRandomBoundary
{
	return [NSString stringWithFormat:@"%ld%ld", random(), random()];
}

- (NSString *) generateAttachmentBody:(NSDictionary *)inDictionary
{
	NSParameterAssert(inDictionary != nil);

	NSString *thePath = [inDictionary valueForKey:kAttachmentPathKey];
	NSAssert(thePath.length > 0, @"thePath.length > 0");

	NSString *theFileName = [inDictionary valueForKey:kAttachmentFileNameKey];
	NSAssert(theFileName.length > 0, @"theFileName > 0");

	// make sure we can actually get our file first
	NSData *theData = [[FXLSafeZone secureFileManager] contentsAtPath:thePath];
	if (theData == nil) return nil;

	// ok, we have it, now compose the string
	NSMutableString *theString = [[NSMutableString alloc] init];
	[theString appendFormat:@"Content-Type: %@; name=%@\r\n", [AttachmentUtils getMIMEType:theFileName], theFileName];
	[theString appendString:@"Content-Transfer-Encoding: base64\r\n\r\n"];

	[theString appendString:[theData base64EncodedStringWithLinebreaks:YES]];
	[theString appendString:@"\r\n"];

	return theString;
}

- (NSString *) generateMultiPartBody
{
	NSAssert(self.plainTextPart.length > 0, @"self.plainTextPart.length > 0");
	NSAssert(self.htmlTextPart.length > 0, @"self.htmlTextPart > 0");

	NSMutableString *theString = [[NSMutableString alloc] init];
	NSString *boundaryString = [self generateRandomBoundary];

	[theString appendFormat:@"Content-Type: multipart/alternative; boundary=\"%@\"\r\n\r\n", boundaryString];

	[theString appendFormat:@"--%@\r\nContent-Type: text/plain\r\nContent-Transfer-Encoding: 7bit\r\n\r\n", boundaryString];
	[theString appendFormat:@"%@\r\n\r\n", self.plainTextPart];

	[theString appendFormat:@"--%@\r\nContent-Type: text/html\r\nContent-Transfer-Encoding: 8bit\r\n\r\n", boundaryString];
	[theString appendFormat:@"%@\r\n\r\n", self.htmlTextPart];

	[theString appendFormat:@"--%@--\r\n\r\n", boundaryString];

	return theString;
}

- (NSDictionary *) generateRecipientCertificates
{
	NSMutableDictionary *certificateArray = [NSMutableDictionary new];
	NSMutableArray *recipientArray = [NSMutableArray new];
	[recipientArray addObjectsFromArray:self.toAddresses];
	[recipientArray addObjectsFromArray:self.ccAddresses];
	[recipientArray addObjectsFromArray:self.bccAddresses];

	for (EmailAddress *theAddress in recipientArray) {
		NSString *thePEMCert = [FXMEmailSecurity pemStringForEmail:theAddress.address];
		if (thePEMCert.length > 0) {
			[certificateArray setObject:thePEMCert forKey:theAddress.address];
		}
	}

	return certificateArray;
}

- (NSString *) errorMessage:(EFXLMIMEError)inError
{
	NSString *theString = @"";
	switch (inError) {
		case errorNone: theString = @"EFXLMIMEError: None"; break;
		case errorMissingFromAddress: theString = @"EFXLMIMEError: Missing 'From' Address"; break;
		case errorMissingToAddress: theString = @"EFXLMIMEError: Missing 'To' Address"; break;
		case errorCouldNotEncrypt: theString = @"EFXLMIMEError: Could Not Encrypt"; break;
		case errorCouldNotSign: theString = @"EFXLMIMEError: Could Not Sign"; break;
		default:
			theString = [NSString stringWithFormat:@"EFXLMIMEError: Unknown (%d)", inError];
			break;
	}

	return theString;
}

#pragma mark - ASComposeDelegate methods

- (void) sendMailSuccess
{
	//
	// this is equivelant to the composeMailVC:deleteDraft method
	//
	if(self.draftEmail) {
        if(self.draftEmail.uid.length > 0) {
            ASFolder* aLocalDraftsFolder = (ASFolder*)self.draftEmail.folder;
            [aLocalDraftsFolder deleteUid:self.draftEmail.uid updateServer:FALSE];
            [aLocalDraftsFolder commitObjects];
        }else{
            FXDebugLog(kFXLogASEmail, @"deleteDraft failed, invalid uid: %@");
        }
	}
	[[NSNotificationCenter defaultCenter] postNotificationName:kSendEmailSuccessNotification object:self];
}

- (void) sendMailFailed:(NSString *)inMessage
{
	NSDictionary *userDictionary = @{
		kSendEmailErrorMessageUserInfo : inMessage
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kSendEmailErrorNotification object:self userInfo:userDictionary];
}

@end
