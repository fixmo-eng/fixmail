//
//  MailboxViewController.h
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "FolderDelegate.h"
#import "SearchRunner.h"
#import "mailViewerVC.h"
#import "MGSplitViewController.h"
#import "FolderSelectController.h"
#import "ASMoveItemsDelegate.h"
#import "FXMSearchTimerManager.h"

typedef enum {
	sortMailByAttachments = 10,
	sortMailByDate,
	sortMailByFlags,
	sortMailByFrom,
	sortMailBySize,
	sortMailBySubject,
	sortMailByTo,
	sortMailByUnread
} EMailSortBy;

typedef enum {
	sortMailOrderAscending = 10,
	sortMailOrderDescending
} EMailSortOrder;

@class BaseFolder;
@class EmailSearch;

@interface MailboxViewController : UIViewController <
		UITableViewDataSource, UITableViewDelegate,
		FolderDelegate, SearchManagerDelegate, UIActionSheetDelegate, mailViewerDelegate, SZCComposeDelegate,FolderSelectDelegate,ASMoveItemsDelegate, UISearchBarDelegate, FXMSearchTimerManagerDelegate
		>

// Properties
@property (nonatomic, strong) BaseFolder *folder;
@property (nonatomic, strong) EmailSearch *emailSearch;
@property (nonatomic, strong) NSMutableArray *emailData;

@property (nonatomic, strong) IBOutlet UITableView *tableView;

// Factory
+ (MailboxViewController*)controller;

// Interface
- (void)doLoad;

// New Interface
- (void)setFolder:(BaseFolder*)aFolder;

@end
