//
//  SZCAddressButton.h
//  MailViewer
//
//  Created by Anluan O'Brien on 2012-11-19.
//  Copyright (c) 2012 Anluan O'Brien. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	viewingViewing = 0,
	viewingSelecting,
	viewingSignedOnly,
	viewingEncryptedOnly,
	viewingSignedEncrypted
} ESZCAddressButtonViewingMode;

@interface SZCAddressButton : UIButton

@property (nonatomic, assign) ESZCAddressButtonViewingMode viewingMode;
@property (nonatomic, assign) BOOL isSigned;
@property (nonatomic, assign) BOOL isEncrypted;
@property (nonatomic, assign) BOOL isAnimating;

- (void) startAnimation;
- (void) stopAnimation;

@end
