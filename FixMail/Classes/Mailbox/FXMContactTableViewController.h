//
//  FXMContactTableViewController.h
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-06-19.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXMContactTableViewController : UITableViewController

@property (nonatomic, strong) NSString *searchString;
+(FXMContactTableViewController *)controller;

@end
