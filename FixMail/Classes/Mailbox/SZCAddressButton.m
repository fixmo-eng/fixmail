//
//  SZCAddressButton.m
//  MailViewer
//
//  Created by Anluan O'Brien on 2012-11-19.
//  Copyright (c) 2012 Anluan O'Brien. All rights reserved.
//

#import "SZCAddressButton.h"

@implementation SZCAddressButton

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		_isSigned = NO;
		_isEncrypted = NO;
		self.viewingMode = viewingSelecting;
	}
	return self;
}

- (void) setViewingMode:(ESZCAddressButtonViewingMode)inViewingMode
{
	if (_viewingMode == inViewingMode) return;
	_viewingMode = inViewingMode;

	switch (inViewingMode) {
		case viewingViewing: [self defaultViewing]; break;
		default:
		case viewingSelecting: [self defaultSelecting]; break;
		case viewingSignedOnly: [self defaultSigned]; break;
		case viewingEncryptedOnly: [self defaultEncrypted]; break;
		case viewingSignedEncrypted: [self defaultSignedEncrypted]; break;
	}

	return;
}

- (void) startAnimation
{
	if (_isAnimating) return;
	_isAnimating = true;

	[UIView animateWithDuration:1.0 delay:0.0 options:(UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse)
		animations:^{
			self.alpha = 0.5;
		}
		completion:^(BOOL inFinished) {
//			self.alpha = 1.0;
		}
	];
}

- (void) stopAnimation
{
	[self.layer removeAllAnimations];
	_isAnimating = false;
	self.alpha = 1.0;
}

- (void) setIsSigned:(BOOL)inValue
{
	if ((self.viewingMode != viewingSignedOnly) && (self.viewingMode != viewingSignedEncrypted)) return;
	if (_isSigned == inValue) return;

	_isSigned = inValue;
	[self stopAnimation];

	if (self.viewingMode == viewingSignedOnly) {
		[self defaultSigned];
	} else if (self.viewingMode == viewingSignedEncrypted) {
		[self defaultSignedEncrypted];
	}

	return;
}

- (void) setIsEncrypted:(BOOL)inValue
{
	if ((self.viewingMode != viewingEncryptedOnly) && (self.viewingMode != viewingSignedEncrypted)) return;
	if (_isEncrypted == inValue) return;
	_isEncrypted = inValue;

	if (self.viewingMode == viewingEncryptedOnly) {
		[self defaultEncrypted];
	} else if (self.viewingMode == viewingSignedEncrypted) {
		[self defaultSignedEncrypted];
	}

	return;
}

- (void) defaultViewing
{	UIImage *backgroundImage = nil;

	[self setBackgroundImage:nil forState:UIControlStateDisabled];
	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateNormal];

	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateSelected];
	[self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];

	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

	[self setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
	self.adjustsImageWhenHighlighted = false;
}

- (void) defaultSelecting
{	UIImage *backgroundImage = nil;

	[self setBackgroundImage:nil forState:UIControlStateDisabled];
	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_disclosure"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateNormal];

	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_disclosure_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateSelected];
	[self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];

	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

	[self setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 20)];
}

- (void) defaultSigned
{	UIImage *backgroundImage = nil;

	[self setBackgroundImage:nil forState:UIControlStateDisabled];
	if (_isSigned) backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_signed"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	else backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_unverified"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateNormal];

	if (_isSigned) backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_signed_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	else backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_unverified_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateSelected];
	[self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];

	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

	[self setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 20)];
}

- (void) defaultEncrypted
{	UIImage *backgroundImage = nil;

	[self setBackgroundImage:nil forState:UIControlStateDisabled];
	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_disclosure"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateNormal];

	backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_disclosure_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateSelected];
	[self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];

	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

	[self setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 20)];
}

- (void) defaultSignedEncrypted
{	UIImage *backgroundImage = nil;

	[self setBackgroundImage:nil forState:UIControlStateDisabled];
	if (_isEncrypted) backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_secure"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	else backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_insecure"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateNormal];

	if (_isEncrypted) backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_secure_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	else backgroundImage = [[UIImage imageNamed:@"FixMailRsrc.bundle/address_atom_insecure_selected"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 34)];
	[self setBackgroundImage:backgroundImage forState:UIControlStateSelected];
	[self setBackgroundImage:backgroundImage forState:UIControlStateHighlighted];

	[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];

	[self setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 20)];
}

@end