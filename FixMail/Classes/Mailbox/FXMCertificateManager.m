/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *  FXMCertificateManager.m
 *
 *  Created by Colin Biggin on 2013-05-16.
 */
#import "FXMCertificateManager.h"
#import "FXMEmailSecurity.h"
#import "BaseAccount.h"
#import "ASResolveRecipients.h"
#import "ASValidateCert.h"
#import "EmailAddress.h"

@interface certificateDownload  : NSObject
@property (nonatomic, strong) ASResolveRecipients *resolveOperation;
@property (nonatomic, strong) NSString *emailAddress;
@end

@implementation certificateDownload
@end

#pragma mark -

static NSMutableDictionary *sCurrentDownloads = nil;

@implementation FXMCertificateManager

+ (FXMCertificateManager *) sharedInstance
{
	static FXMCertificateManager *_sharedInstance;
	if(!_sharedInstance) {
		static dispatch_once_t oncePredicate;
		dispatch_once(&oncePredicate, ^{
			_sharedInstance = [[super allocWithZone:nil] init];
		});
	}

	return _sharedInstance;
}

+ (void) downloadCertificate:(NSString *)inEmailAddress
{
	NSParameterAssert(inEmailAddress != nil);

	FXDebugLog(kFXLogCertificateManager, @"downloadCertificate: %@", inEmailAddress);

	// ok, start it up
	if (sCurrentDownloads == nil) {
		sCurrentDownloads = [[NSMutableDictionary alloc] initWithCapacity:5];
	}

	// check if an existing query is being sent and if so, just return
	if ([FXMCertificateManager getDownloadForEmail:inEmailAddress] != nil) return;

	// ok, now do a GAL query
	if (inEmailAddress.length == 0) return;

	ASResolveRecipients *anItemOp = [ASResolveRecipients sendResolveRecipientsCert:(ASAccount *)[BaseAccount currentLoggedInAccount]
										recipients:@[inEmailAddress]
										  certType:kCertTypeFull
										  delegate:[FXMCertificateManager sharedInstance]];

	certificateDownload *theDownload = [[certificateDownload alloc] init];
	theDownload.resolveOperation = anItemOp;
	theDownload.emailAddress = inEmailAddress;

	[sCurrentDownloads setObject:theDownload forKey:inEmailAddress];
	[[NSNotificationCenter defaultCenter] postNotificationName:kCertificateRetrievalStartedNotification object:nil userInfo:nil];
}

+ (bool) isCertificateDownloading:(NSString *)inEmailAddress
{
	return ([FXMCertificateManager getDownloadForEmail:inEmailAddress] != nil);
}

#pragma mark - ASResolveRecipientsDelegate methods

- (void) deliverCertificate:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp
{
	FXDebugLog(kFXLogCertificateManager, @"deliverCertificate:");

	if (inData.length == 0) return;
	if (inEmailAddress == nil) return;

	// remove it from the queue
	[FXMCertificateManager removeDownloadForOp:inResolveOp];

	NSString *theEmailAddress = nil;
	if ([inEmailAddress isKindOfClass:[EmailAddress class]]) {
		theEmailAddress = ((EmailAddress *)inEmailAddress).address;
	} else if ([inEmailAddress isKindOfClass:[NSString class]]) {
		theEmailAddress = inEmailAddress;
	}
	if (theEmailAddress == nil) return;

	// store the damn thing
	NSString *theCertificate = [[NSString alloc] initWithData:inData encoding:NSUTF8StringEncoding];
	[FXMEmailSecurity saveCertificateForEmail:theEmailAddress withPublic:theCertificate];

	NSDictionary *userDictionary = @{
		kCertificateRetrievalEmailUserInfo : theEmailAddress,
		kCertificateRetrievalCertificateUserInfo : theCertificate
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kCertificateRetrievalSuccessNotification object:nil userInfo:userDictionary];
}

- (void) deliverMiniCertificate:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp
{
	FXDebugLog(kFXLogCertificateManager, @"deliverMiniCertificate:");

	// remove it from the queue
	[FXMCertificateManager removeDownloadForOp:inResolveOp];
}

- (void) deliverPicture:(NSData *)inData emailAddress:(id)inEmailAddress resolveOp:(ASResolveRecipients*)inResolveOp
{
	FXDebugLog(kFXLogCertificateManager, @"deliverPicture:");

	// remove it from the queue
	[FXMCertificateManager removeDownloadForOp:inResolveOp];
}

- (void) delayedDeliverFinishedNotification:(NSString *)inMessage
{
	NSDictionary *userDictionary = @{
		kCertificateRetrievalErrorMessageUserInfo : inMessage
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kCertificateRetrievalSuccessNotification object:nil userInfo:userDictionary];
}

- (void) deliverFinished:(NSString*)inMessage resolveOp:(ASResolveRecipients*)inResolveOp
{
	FXDebugLog(kFXLogCertificateManager, @"deliverFinished: %@", inMessage);

	// remove it from the queue
	[FXMCertificateManager removeDownloadForOp:inResolveOp];

//	NSDictionary *userDictionary = @{
//		kCertificateRetrievalErrorMessageUserInfo : inMessage
//	};
//	[[NSNotificationCenter defaultCenter] postNotificationName:kCertificateRetrievalSuccessNotification object:nil userInfo:userDictionary];

	[self performSelector:@selector(delayedDeliverFinishedNotification:) withObject:inMessage afterDelay:4.0];
}

- (void) deliverError:(NSString*)inMessage resolveOp:(ASResolveRecipients*)inResolveOp
{
	FXDebugLog(kFXLogCertificateManager, @"deliverError: %@", inMessage);

	// remove it from the queue
	[FXMCertificateManager removeDownloadForOp:inResolveOp];

	NSDictionary *userDictionary = @{
		kCertificateRetrievalErrorMessageUserInfo : inMessage
	};
	[[NSNotificationCenter defaultCenter] postNotificationName:kCertificateRetrievalErrorNotification object:nil userInfo:userDictionary];
}

#pragma mark - Internal routines

+ (certificateDownload *) getDownloadForEmail:(NSString *)inEmailAddress
{
	if (inEmailAddress.length == 0) return nil;
	return [sCurrentDownloads objectForKey:inEmailAddress];
}

+ (certificateDownload *) getDownloadForResolveOperation:(ASResolveRecipients *)inResolveOperation
{
	if (inResolveOperation == nil) return nil;

	NSArray *theDownloads = [sCurrentDownloads allValues];
	for (certificateDownload *theDownload in theDownloads) {
		if (theDownload.resolveOperation == inResolveOperation) return theDownload;
	}

	return nil;
}

+ (void) removeDownload:(certificateDownload *)inDownload
{
	if (inDownload == nil) return;

	// the email address is the 'key'
	[sCurrentDownloads removeObjectForKey:inDownload.emailAddress];
}

+ (void) removeDownloadForOp:(ASResolveRecipients *)inResolveOperation
{
	if (inResolveOperation == nil) return;

	// the email address is the 'key'
	certificateDownload *theDownload = [FXMCertificateManager getDownloadForResolveOperation:inResolveOperation];
	if (theDownload != nil) {
		[sCurrentDownloads removeObjectForKey:theDownload.emailAddress];
	}

	return;
}

@end
