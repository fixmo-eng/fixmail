//
//  contactsListVC.m
//  FixMail
//
//  Created by Colin Biggin on 2013-02-21.
//  Copyright (c) 2013 Colin Biggin. All rights reserved.
//
#import "contactsListVC.h"
#import "contactSearchCell.h"
#import "FXContactsHeader.h"
#import "FXDatabase.h"
#import "FXSectionator.h"
#import "ASContact.h"

@interface contactsListVC ()
@property (strong) FXSectionator *sectionator;
@end

#pragma mark -

@implementation contactsListVC

- (void) viewDidLoad
{
	FXDebugLog(kFXLogContacts | kFXLogVCLoadUnload, @"contactsListVC: viewDidLoad");
    [super viewDidLoad];

	[self initializeData];
	[self setupNotifications];
	[self setupControls];
}

- (void) viewWillAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"contactsListVC: viewWillAppear");
	[super viewWillAppear:inAnimated];

	[self reloadTableview];
}

- (void) viewDidAppear:(BOOL)inAnimated
{
	FXDebugLog(kFXLogContacts | kFXLogVCAppearDisappear, @"contactsListVC: viewDidAppear");
	[super viewDidAppear:inAnimated];
}

- (void) dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return YES;
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)inDuration
{
	// for some reason, we lose our exit button on the iPad when switching orientations from
	// portrait to landscape.
	if (IS_IPAD()) {
	}

	return;
}

#pragma mark - Getters/Setters

- (void) setSearchString:(NSString *)inString
{
	[self initializeData]; // this might actually be called before viewDidLoad

	_searchString = inString;
	self.sectionator.searchString = inString;
	[self.tableView reloadData];

	return;
}

- (int) rowCount
{
	return [self.sectionator rowCountForSection:0];
}

#pragma mark - UITableView Delegate & DataSource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)inTableView
{
	return 1;
}

- (NSInteger) tableView:(UITableView *)inTableView numberOfRowsInSection:(NSInteger)inSection
{
	return [self.sectionator rowCountForSection:0];
}

- (UITableViewCell *) tableView:(UITableView *)inTableView cellForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	UITableViewCell *theCell = nil;

	contactSearchCell *searchCell = [self.tableView dequeueReusableCellWithIdentifier:@"contactSearchCell"];
	NSAssert(searchCell != nil, @"searchCell != nil");

	searchCell.contact = [self.sectionator objectForIndex:inIndexPath];
	theCell = searchCell;

    return theCell;
}

- (BOOL) tableView:(UITableView *)inTableView canEditRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	return NO;
}

- (CGFloat) tableView:(UITableView *)inTableView heightForRowAtIndexPath:(NSIndexPath *)inIndexPath
{
	return 44.0;
}

- (NSArray *) sectionIndexTitlesForTableView:(UITableView *)inTableView
{
	return nil;
}

- (void) tableView:(UITableView *)inTableView didSelectRowAtIndexPath:(NSIndexPath *)inIndexPath
{
    ASContact *theContact = [self.sectionator objectForIndex:inIndexPath];
	NSAssert(theContact != nil, @"theContact != nil");

	[[NSNotificationCenter defaultCenter] postNotificationName:kSZCContactListSelectedNotification object:theContact];

	return;
}

#pragma mark - Private methods

- (void) setupControls
{
	UIView *theView = self.tableView.tableHeaderView;
	theView.layer.shadowColor = [[UIColor blackColor] CGColor];
	theView.layer.shadowOpacity = 1.0;
	theView.layer.shadowRadius = 4.0;
	theView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
	theView.layer.masksToBounds = NO;
	theView.layer.cornerRadius = 0.0;
	theView.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.tableView.tableHeaderView.bounds].CGPath;
	theView.layer.shouldRasterize = YES;

	return;
}

- (void) setupNotifications
{
}

- (void) dismissKeyboard
{
	[self.view endEditing:YES];
}

- (void) reloadTableview
{
	[self initializeData];
	[self.tableView reloadData];

	return;
}

- (void) initializeData
{
	if (self.sectionator != nil) return;

	self.sectionator = [FXSectionator new];
	[self.sectionator resetKeys];
	[self.sectionator addSectionObjects:[FXDatabase contactList:contactUser]];
	[self.sectionator addSectionObjects:[FXDatabase contactList:contactEmailRecipients]];

}

@end
