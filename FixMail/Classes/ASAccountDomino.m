/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASAccountDomino.h"

@implementation ASAccountDomino

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccountNum:(int)anAccountNum
{
    if (self = [super initWithAccountNum:anAccountNum]) {
        self.accountSubType = AccountSubTypeDomino;
    }
    
    return self;
}

- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUserName
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(EEncryptionMethod)anEncryption
    authentication:(EAuthenticationMethod)anAuthentication
       folderNames:(NSArray *)aFolderNames
{
    if (self = [super initWithName:aName
                          userName:aUserName
                          password:aPassword
                          hostName:aHostName
                              path:aPath
                              port:aPort
                        encryption:anEncryption
                    authentication:anAuthentication
                       folderNames:aFolderNames]) {
        self.accountSubType = AccountSubTypeDomino;
    }
    
    return self;
}


////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (UIImage*)icon
{
    return [UIImage imageNamed:@"FixMailRsrc.bundle/settingsAccountExchangeIcon.png"];
}

- (NSArray*)foldersToDisplay
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:folders.count];
    
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeInbox];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeDrafts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeLocalDrafts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeCalendar];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeContacts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeDeleted];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeOutbox];
    // Junk E-Mail is type UserMailbox, on AS 12.1 at least, so we don't special case it
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeJournal];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeNotes];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeTasks];
    
    [super folderHierarchyFromFolders:folders toFolders:aFolders];
    
    return aFolders;
}

- (NSArray*)foldersToSync
{
    NSMutableArray* aFolders = [NSMutableArray arrayWithCapacity:folders.count];
    
#if 1
    // Add folders which are high priority to user first
    //
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeInbox];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeCalendar];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeContacts];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
    
    // Add the test of the folders and remove an non syncable folders
    //
    for(ASFolder* aFolder in folders) {
        //FXDebugLog(kFXLogActiveSync, @"foldersToSync %@ type=%d syncable=%d", aFolder.displayName, aFolder.folderType, [aFolder isSyncable]);
        if(aFolder.isSyncable && !aFolder.isDeleted) {
            if(![aFolders containsObject:aFolder]) {
                [aFolders addObject:aFolder];
            }
        }else{
            if([aFolders containsObject:aFolder]) {
                [aFolders removeObject:aFolder];
            }
            if(!aFolder.isDeleted) {
                FXDebugLog(kFXLogActiveSync, @"Folder not syncable: %@ %@", [aFolder displayName], [aFolder uid]);
            }
        }
    }
#else
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeInbox];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeSent];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeDrafts];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeCalendar];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeContacts];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeDeleted];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeOutbox];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeJournal];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeNotes];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeTasks];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserMailbox];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserCalendar];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserContacts];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserTasks];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserJournal];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUserNotes];
    //[self addFoldersToArray:aFolders ofFolderType:kFolderTypeUnknown];
    [self addFoldersToArray:aFolders ofFolderType:kFolderTypeRecipientInfo];
#endif
    return aFolders;
}

- (BOOL)isFilterTypeSupported:(EFilterType)aFilterType folderType:(EFolderType)aFolderType
{
    BOOL isSupported = TRUE;
    
    switch(aFolderType) {
        default:
            // Email folder types
            switch(aFilterType) {
                case kFilter3MonthsAgo:
                case kFilter6MonthsAgo:
                case kFilterIncompleteTasks:
                    isSupported = FALSE;
                    break;
                default:
                    break;
            }
            break;
        case kFolderTypeCalendar:
        case kFolderTypeUserCalendar:
            // Calendar folder Types
            switch(aFilterType) {
                case kFilter1DayAgo:
                case kFilter3DaysAgo:
                case kFilter1WeekAgo:
                case kFilterIncompleteTasks:
                    isSupported = FALSE;
                    break;
                default:
                    break;
            }
            break;
        case kFolderTypeTasks:
            // Task folder Types
            switch(aFilterType) {
                case kFilter1DayAgo:
                case kFilter3DaysAgo:
                case kFilter1WeekAgo:
                case kFilter3MonthsAgo:
                case kFilter6MonthsAgo:
                    isSupported = FALSE;
                    break;
                default:
                    break;
            }
            break;
        case kFolderTypeContacts:
        case kFolderTypeUserContacts:
        case kFolderTypeRecipientInfo:
            // Contact folder types, don't think filters apply here
            isSupported = FALSE;
            break;
    }
    return isSupported;
}

@end