//
//  OGiDateInformation.m
//  Gosh2
//
//  Created by Harold Smith III on 10/13/11.
//  Copyright 2011 Fixmo Inc. All rights reserved.
//

#import "OGiDateInformation.h"
#import "DateUtil.h"

@implementation NSDate (OGiDateCategory)

+ (NSDate*) yesterday{
	OGiDateInformation inf = [[NSDate date] dateInformation];
	inf.day--;
	return [NSDate dateFromDateInformation:inf];
}
+ (NSDate*) month{
    return [[NSDate date] monthDate];
}

- (NSDate*) monthDate {
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self];
	[comp setDay:1];
	NSDate *date = [gregorian dateFromComponents:comp];

    return date;
}
- (NSDate*) lastOfMonthDate {
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:self];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
	NSDate *date = [gregorian dateFromComponents:comp];

    return date;
}

+ (NSDate*) lastofMonthDate{
    NSDate *day = [NSDate date];
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
	return [gregorian dateFromComponents:comp];
}

+ (NSDate*) lastOfCurrentMonth{
	NSDate *day = [NSDate date];
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	[comp setDay:0];
	[comp setMonth:comp.month+1];
	return [gregorian dateFromComponents:comp];
}



- (int) weekday{
	
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comps = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSWeekdayCalendarUnit) fromDate:self];
	int weekday = [comps weekday];
    
	return weekday;
}
- (NSDate*) timelessDate {
	NSDate *day = self;
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:day];
	return [gregorian dateFromComponents:comp];
}
- (NSDate*) monthlessDate {
	NSDate *day = self;
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:day];
	return [gregorian dateFromComponents:comp];
}



- (BOOL) isSameDay:(NSDate*)anotherDate{
	NSCalendar* calendar = [DateUtil.getSingleton currentCalendar];
	NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self];
	NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anotherDate];
	return ([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
} 

- (int) monthsBetweenDate:(NSDate *)toDate{
    NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
    
    NSDateComponents *components = [gregorian components:NSMonthCalendarUnit
                                                fromDate:[self monthlessDate]
                                                  toDate:[toDate monthlessDate]
                                                 options:0];
    NSInteger months = [components month];
    return abs(months);
}
- (NSInteger) daysBetweenDate:(NSDate*)d{
	
	NSTimeInterval time = [self timeIntervalSinceDate:d];
	return abs(time / 60 / 60/ 24);
	
}
- (BOOL) isToday{
	return [self isSameDay:[NSDate date]];
} 


- (NSDate *)dateByAddingHours:(NSUInteger)hours minutes:(int)minutes 
{
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.hour = hours;
    c.minute = minutes;
	return [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:c toDate:self options:0];
}


- (NSDate *) dateByAddingDays:(NSUInteger)days {
	NSDateComponents *c = [[NSDateComponents alloc] init];
	c.day = days;
	return [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:c toDate:self options:0];
}
+ (NSDate *) dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"dd/MM/yyyy"];
	NSString *datePortion = [dateFormatter stringFromDate:aDate];
	
	[dateFormatter setDateFormat:@"HH:mm"];
	NSString *timePortion = [dateFormatter stringFromDate:aTime];
	
	[dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
	NSString *dateTime = [NSString stringWithFormat:@"%@ %@",datePortion,timePortion];
	return [dateFormatter dateFromString:dateTime];
}



- (NSString*) monthString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/GMT"]];
	[dateFormatter setDateFormat:@"MMMM"];
	return [dateFormatter stringFromDate:self];
}
- (NSString*) yearString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/GMT"]];
	[dateFormatter setDateFormat:@"yyyy"];
	return [dateFormatter stringFromDate:self];
}

- (NSString*) dayString{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/GMT"]];
	[dateFormatter setDateFormat:@"EEE"];
	return [dateFormatter stringFromDate:self];
}




- (OGiDateInformation) dateInformationWithTimeZone:(NSTimeZone*)tz{
	
	
	OGiDateInformation info;
	
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	[gregorian setTimeZone:tz];
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
													NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
										  fromDate:self];
	info.day = [comp day];
	info.month = [comp month];
	info.year = [comp year];
	
	info.hour = [comp hour];
	info.minute = [comp minute];
	info.second = [comp second];
	
	info.weekday = [comp weekday];
	
	
	return info;
	
}
- (OGiDateInformation) dateInformation{
	
	OGiDateInformation info;
	
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSMonthCalendarUnit | NSMinuteCalendarUnit | NSYearCalendarUnit | 
													NSDayCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit) 
										  fromDate:self];
	info.day = [comp day];
	info.month = [comp month];
	info.year = [comp year];
	
	info.hour = [comp hour];
	info.minute = [comp minute];
	info.second = [comp second];
	
	info.weekday = [comp weekday];
	
    
	return info;
}
+ (NSDate*) dateFromDateInformation:(OGiDateInformation)info timeZone:(NSTimeZone*)tz{
	
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	[gregorian setTimeZone:tz];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[NSDate date]];
	
	[comp setDay:info.day];
	[comp setMonth:info.month];
	[comp setYear:info.year];
	[comp setHour:info.hour];
	[comp setMinute:info.minute];
	[comp setSecond:info.second];
	[comp setTimeZone:tz];
	
	return [gregorian dateFromComponents:comp];
}
+ (NSDate*) dateFromDateInformation:(OGiDateInformation)info{
	
	NSCalendar *gregorian = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit) fromDate:[NSDate date]];
	
	[comp setDay:info.day];
	[comp setMonth:info.month];
	[comp setYear:info.year];
	[comp setHour:info.hour];
	[comp setMinute:info.minute];
	[comp setSecond:info.second];
	//[comp setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	
	return [gregorian dateFromComponents:comp];
}

+ (NSString*) dateInformationDescriptionWithInformation:(OGiDateInformation)info{
	return [NSString stringWithFormat:@"%d %d %d %d:%d:%d",info.month,info.day,info.year,info.hour,info.minute,info.second];
}

- (NSDate*)getRoundedMinuteDateTimeGMT
{
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[comps day]];
    [dateComps setMonth:[comps month]];
    [dateComps setYear:[comps year]];
    [dateComps setHour:[comps hour]];
    [dateComps setMinute:[comps minute]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
    
}

- (NSDate*)getRoundedMinuteDateTime
{
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[comps day]];
    [dateComps setMonth:[comps month]];
    [dateComps setYear:[comps year]];
    [dateComps setHour:[comps hour]];
    [dateComps setMinute:[comps minute]];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
	
} // getGMTMidnightDateTime


- (NSDate*)getMidnightDateTime
{
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];

	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];	
    [dateComps setDay:[comps day]];	
    [dateComps setMonth:[comps month]];	
    [dateComps setYear:[comps year]];	
    [dateComps setHour:0];	
    [dateComps setMinute:0];	
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    	
	// Return the date now + the offset
	return itemDate;
	
} // getGMTMidnightDateTime

- (NSDate*)getGMTTime
{
    
	// Figure out the offset 
	double itemDateOffset = [[NSTimeZone defaultTimeZone] secondsFromGMTForDate:self];
	
	NSDate *newDate = [self dateByAddingTimeInterval:itemDateOffset];
	
	// Return the date now + the offset
	return newDate;
	
} // getGMTMidnightDateTime



- (NSDate*)getMidnightDateTimeGMT
{
    
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/GMT"]];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:[comps day]];
    [dateComps setMonth:[comps month]];
    [dateComps setYear:[comps year]];
    [dateComps setHour:0];
    [dateComps setMinute:0];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
	
} // getGMTMidnightDateTime




- (int)getDayOfTheWeek
{
	NSCalendar *calendar= [DateUtil.getSingleton currentCalendar];
	NSCalendarUnit unitFlags = NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:self];
	NSInteger weekday = [dateComponents weekday];
    
	return (weekday - 1);
}

- (int)getDayOfTheMonth
{
	NSCalendar *calendar= [DateUtil.getSingleton currentCalendar];
	NSCalendarUnit unitFlags = NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:self];
	NSInteger weekday = [dateComponents day];
	
	return weekday;
}

- (int)getMonthOfYear
{
	NSCalendar *calendar= [DateUtil.getSingleton currentCalendar];
	NSCalendarUnit unitFlags = NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:self];
	NSInteger weekday = [dateComponents month];
	
	return weekday;
}


- (int)getYear
{
	NSCalendar *calendar= [DateUtil.getSingleton currentCalendar];
	NSCalendarUnit unitFlags = NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
	NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:self];
	NSInteger weekday = [dateComponents year];
	
	return weekday;
}


// Gets the first day of the month
- (NSDate*)addMonth
{
    
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [DateUtil.getSingleton currentCalendar];
	NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    int month = [comps month];
    int year = [comps year];
    
    if (month == 12)
    {
        year++;
        month = 0;
    }
    month++;
    
    [dateComps setDay:[comps day]];
    [dateComps setMonth:month];
    [dateComps setYear:year];
    [dateComps setHour:0];
    [dateComps setMinute:0];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
	
} // getFirstOfMonth

// Gets the first day of the month
- (NSDate*)subtractMonth
{
    
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendara = [DateUtil.getSingleton currentCalendar];
    
    [calendara setTimeZone:[NSTimeZone timeZoneWithName:@"Etc/GMT"]];
	NSDateComponents *comps = [calendara components:unitFlags fromDate:self];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    int month = [comps month];
    int year = [comps year];
    
    if (month == 1)
    {
        year--;
        month = 13;
    }
    month--;
    
    [dateComps setDay:[comps day]];
    [dateComps setMonth:month];
    [dateComps setYear:year];
    [dateComps setHour:0];
    [dateComps setMinute:0];
    NSDate *itemDate = [calendara dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
	
} // getFirstOfMonth

// Gets the first day of the month
- (NSDate*)getFirstOfMonth
{
    
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit |NSHourCalendarUnit |NSMinuteCalendarUnit|NSSecondCalendarUnit;
	NSCalendar *calendar = [DateUtil.getSingleton currentCalendar];
    NSDateComponents *comps = [calendar components:unitFlags fromDate:self];
	
	// This calendar will grab everything for the current date, but
	// will zero out all the hour/min/second parameters so we get midnight
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setDay:1];
    [dateComps setMonth:[comps month]];
    [dateComps setYear:[comps year]];
    [dateComps setHour:0];
    [dateComps setMinute:0];
    NSDate *itemDate = [calendar dateFromComponents:dateComps];
    
	// Return the date now + the offset
	return itemDate;
	
} // getFirstOfMonth

- (int)numberOfWeeksInMonth
{
    NSCalendar* cal = [DateUtil.getSingleton currentCalendar];
    unsigned int unitFlags =  NSWeekOfMonthCalendarUnit;
    NSDateComponents* month = [[NSDateComponents alloc] init];
    month.month = 1;
    month.day = -1;

    NSDate *startDate = [self getFirstOfMonth];
    NSDate *endDate = [cal dateByAddingComponents:month toDate:startDate options:0];
        
    NSDateComponents *start_components = [cal components:unitFlags
                                                fromDate:endDate];
    
    NSInteger ret = start_components.weekOfMonth;
    return ret;
}

// Determines how many days are within a month 
- (int)numberOfDaysInMonth
{
	// Grab the month...
	int month = [self getMonthOfYear];
	
	// Grab the year...
	int year = [self getYear];
	
	// Compute how many days are in the month, check for
	// leap year
	switch (month)
	{
		case 2:
			return (year - 1972) % 4 == 0 ? 29 : 28;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
		default:
			return 30;
			
	} // switch
	
} // daysInMonth

// Determines if the date passed in is in the last week of the month
- (BOOL)isLastWeekOfMonth
{
	// How many days are in the month
	int days = [self numberOfDaysInMonth];
	
	// What is our current date of the mnth?
	int dayOfMonth = [self getDayOfTheMonth];
	
	// If we are within 7 days of the end of the month, this is
	// the last week of the month
	if (dayOfMonth + 7 > days)	
	{
		return YES;
		
	} // if
	
	return NO;
	
} // isLastWeekOfMonth

- (BOOL) isWeekDay
{
    BOOL isWeekday = NO;
    NSCalendar *calendar= [DateUtil.getSingleton currentCalendar];
    
    int day = [self weekday];
    
    if (day > [calendar firstWeekday] && day <= [calendar firstWeekday] + 5)
    {
        isWeekday = YES;
    }

    
    return isWeekday;
}


@end
