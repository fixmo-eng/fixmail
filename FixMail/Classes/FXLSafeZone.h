//
//  FXLSafeZone.h
//  FixMail
//
//  Created by Sean Langley on 2013-04-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FXLSafeZone : NSObject

+ (BOOL)isInSafeZone;

+ (NSBundle*) getResourceBundle;

+ (NSFileManager*) secureFileManager;
+ (id) defaultSafeZoneFileManager;
+ (id) secureDatabaseQueueWithPath:(NSString*)aPath;
+ (id) secureDatabaseWithPath:(NSString*)aPath;

+ (void) secureResetStandardUserDefaults;
+ (id) secureStandardUserDefaults;

+ (NSString *) documentsDirectory;
+ (NSString *) settingsDirectory;
+ (NSString *) databaseDirectory;
+ (NSString *) attachmentsDirectory;

+ (Class) fileHandleClass;

@end
