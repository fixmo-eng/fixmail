#if 0 // Not Implemented
/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */


#import "ASAutoDiscover.h"
#import "ASAccount.h"
#import "HttpEngine.h"

@implementation ASAutoDiscover

// Static
static NSString* kCommand   = @"AutoDiscover";

#warning FIXME AutoDiscover isn't working yet, may not be turned on on Fixmo Exchange server

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendRequest:(ASAccount*)anAccount
{
    NSString* aDomain = NULL;
    NSArray* aParts = [[anAccount emailAddress] componentsSeparatedByString:@"@"];
    if([aParts count] == 2) {
        aDomain = [aParts objectAtIndex:1];
    }
    
#warning FIXME hardwire to fixmo domain
    aDomain = @"fixmo.com";
    //aDomain = @"stk09-ex01.fixmo.corp";
    
    // NSString* aUrlString = [NSString stringWithFormat:@"%@/autodiscover/autodiscover.xml", aDomain];
    NSString* aUrlString = [NSString stringWithFormat:@"autodiscover.%@/autodiscover/autodiscover.xml", aDomain];

    if([aDomain length] > 0) {
        NSMutableString* anXMLString = [NSMutableString string];
        [anXMLString appendString:@"<Autodiscover xmlns=\"http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006\">\n"];
        [anXMLString appendString:@"<Request>\n"];
        [anXMLString appendString:@"\t<EMailAddress>\n"];
        [anXMLString appendString:[NSString stringWithFormat:@"\t\t%@\n", [anAccount emailAddress]]];
        [anXMLString appendString:@"\t</EMailAddress>\n"];
        [anXMLString appendString:@"\t<AcceptableResponseSchema>\n"];
        [anXMLString appendString:@"\t\thttp://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006\n"];
        [anXMLString appendString:@"\t</AcceptableResponseSchema>\n"];
        [anXMLString appendString:@"</Request>\n"];
        [anXMLString appendString:@"</Autodiscover>\n"];
        FXDebugLog(kFXLogActiveSync, @"xml: %@\n", anXMLString);
        
        ASAutoDiscover* anASAutoDiscover = [[ASAutoDiscover alloc] initWithAccount:anAccount];
        NSMutableURLRequest* aURLRequest = [anAccount createAutoDiscoverRequest:aUrlString xml:anXMLString];
        [anAccount send:aURLRequest httpRequest:anASAutoDiscover];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Invalid domain for auto discover: %@", anAccount);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// HttpRequest Overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark HttpRequest Overrides

- (void)userAbortedWithMessage:(NSString*)aMesg
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate


- (void)connection:(HttpConnection*)aConnection didReceiveResponse:(NSURLResponse*)aResponse
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)aResponse;
    statusCode = (EHttpStatusCode)[resp statusCode];
	
	switch(statusCode) {
        case kHttpOK:
            @try {
                NSDictionary* aHeaders = [resp allHeaderFields];
                FXDebugLog(kFXLogActiveSync, @"AutoDiscover didReceiveResponse: %d headers: %@", statusCode, aHeaders);
            }@catch (NSException* e) {
                [HttpRequest logException:@"AutoDiscover response" exception:e];
            }
            break;
        case kHttpUnauthorized:
            FXDebugLog(kFXLogActiveSync, @"Autodiscover not authorized");
            break;
        default:
            FXDebugLog(kFXLogActiveSync, @"Autodiscover failed with status: %d", statusCode);
            break;
    }
}
@end
#endif