/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"

typedef enum {
    kOutOfOfficeInternal        = 1,
    kOutOfOfficeExternalKnown   = 2,
    kOutOfOfficeExternalUnknown = 3
} EOutOfOfficeType;

@interface ASOutOfOffice : BaseObject {
    
}

@property (nonatomic) EOutOfOfficeType      type;
@property (nonatomic) BOOL                  enabled;
@property (nonatomic,strong) NSString*      message;
@property (nonatomic,strong) NSString*      bodyType;

@end
