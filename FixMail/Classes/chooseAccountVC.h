//
//  AccountConfigureViewController.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/15/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

@interface chooseAccountVC : UIViewController {
	BOOL firstSetup;
	BOOL newAccount;
	int accountNum;	
	}

// Properties
@property (assign) BOOL                 firstSetup;
@property (assign) BOOL                 newAccount;
@property (assign) int                  accountNum;

// Actions
-(IBAction)gmailActiveSyncClicked;
-(IBAction)exchangeClicked;
-(IBAction)fixmoClicked_stk08;
-(IBAction)fixmoClicked_stk09;

@end
