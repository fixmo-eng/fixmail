/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"

@interface BaseAttachment : BaseObject {
    NSString*           name;
    NSString*           fileReference;
    NSString*           contentType;
    
    NSUInteger          estimatedDataSize;
    NSUInteger          flags;
}

// Properties
@property (nonatomic,readwrite,strong) NSString*    name;
@property (nonatomic,readwrite,strong) NSString*    fileReference;
@property (nonatomic,readwrite,strong) NSString*    contentType;
@property (nonatomic,readwrite) NSUInteger          estimatedDataSize;

// Load/Store
- (NSDictionary*)asDictionary;
- (void)fromDictionary:(NSDictionary*)aDictionary;

// Flags Stored
- (BOOL)isFetched;
- (void)setIsFetched:(BOOL)state;

- (BOOL)isRead;
- (void)setIsRead:(BOOL)state;

- (BOOL)isInline;
- (void)setIsInline:(BOOL)state;

// Flags Transient
- (BOOL)isFetching;
- (void)setIsFetching:(BOOL)state;

@end
