/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASMeetingResponse.h"
#import "ASAccount.h"
#import "HttpEngine.h"
#import "MeetingRequest.h"
#import "WBXMLDataGenerator.h"

@implementation ASMeetingResponse

// Properties
@synthesize email;
@synthesize delegate;

// Constants
static NSString* kCommand       = @"MeetingResponse";
static const ASTag kCommandTag  = MEETING_RESPONSE_MEETING_RESPONSE;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSMutableArray* sPings = NULL;

+ (void)sendMeetingResponse:(ASEmail*)anEmail
               userResponse:(EUserResponse)aUserResponse
               instanceDate:(NSDate*)anInstanceDate
                   delegate:(NSObject<ASMeetingResponseDelegate>*)aDelegate;
{
    ASAccount* anAccount = (ASAccount*)[anEmail.folder account];
    
    ASMeetingResponse* aMeetingResponse = [[ASMeetingResponse alloc] initWithEmail:anEmail];
    aMeetingResponse.delegate = aDelegate;
    NSData* aWbxml = [aMeetingResponse wbxmlMeetingResponse:aUserResponse instanceDate:anInstanceDate];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];

    [anAccount send:aURLRequest httpRequest:aMeetingResponse];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlMeetingResponse:(EUserResponse)aUserResponse instanceDate:(NSDate*)anInstanceDate
{
    NSData* aData = NULL;
    
    if(self.account) {
        WBXMLDataGenerator wbxml;
        
        wbxml.start(kCommandTag); {
            wbxml.start(MEETING_RESPONSE_REQUEST); {
                wbxml.keyValue(MEETING_RESPONSE_USER_RESPONSE, [NSString stringWithFormat:@"%d", aUserResponse]);
                
                // CollectionId of the Inbox usually
                wbxml.keyValue(MEETING_RESPONSE_COLLECTION_ID, self.email.folder.uid);
                
                // ServerId of the email that sent the meeting request
                wbxml.keyValue(MEETING_RESPONSE_REQUEST_ID, self.email.uid);
                
                // Optional if applies to only one instance of a recurring meeting
                if(anInstanceDate) {
                    wbxml.keyValue(MEETING_RESPONSE_INSTANCE_ID, [[MeetingRequest dateFormatter] stringFromDate:anInstanceDate]);
                }
            }wbxml.end();
        }wbxml.end();
        
        aData = wbxml.encodedData(self, kCommandTag);
    }else{
        FXDebugLog(kFXLogActiveSync, @"Meeting response invalid");
    }
    
    return aData;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithEmail:(ASEmail*)anEmail
{
    if (self = [super initWithAccount:(ASAccount*)[anEmail.folder account]]) {
        self.email = anEmail;
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setStatus:(EMeetingResponseStatus)aStatus
{
    self.statusCodeAS = aStatus;
    
    switch(aStatus) {
        case kMeetingResponseSuccess:
            break;
#ifdef DEBUG
        case kMeetingResponseInvalidRequest:
            // Stop sending the item. This is not a transient condition.
            [HttpRequest handleASError:@"Meeting response invalid meeting request"]; break;
        case kMeetingResponseServerError3:
            // Server misconfiguration, temporary system issue, or bad item. This is frequently a transient condition.
            // Retry the MeetingResponse command. If continued attempts fail, synchronize the folder again, and then attempt the
            // MeetingResponse command again. If it still continues to fail, make no changes.
            [HttpRequest handleASError:@"Meeting response error on server mailbox"]; break;
        case kMeetingResponseServerError4:
            // Server misconfiguration, temporary system issue, or bad item. This is frequently a transient condition.
            // Retry the MeetingResponse command. If continued attempts fail, synchronize the folder again, and then attempt the
            // MeetingResponse command again. If it still continues to fail, make no changes.
            [HttpRequest handleASError:@"Meeting response transient server misconfiguration"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aStatus]; break;
#else
		default:
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"MEETING_RESPONSE_ERROR", @"ErrorAlert", @"Error alert message for Meeting Response error"), aStatus]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case MEETING_RESPONSE_REQUEST:
                    FXDebugLog(kFXLogActiveSync, @"MEETING_RESPONSE_REQUEST");
                    break;
                case MEETING_RESPONSE_RESULT:
                    break;
                case MEETING_RESPONSE_STATUS:
                {
                    EMeetingResponseStatus aStatus = (EMeetingResponseStatus)[aParser getInt];
                    [self setStatus:aStatus];
                    break;
                }
                case MEETING_RESPONSE_REQUEST_ID:
                {
                    NSString* aRequestId = [aParser getString];
                    FXDebugLog(kFXLogActiveSync, @"MEETING_RESPONSE_REQUEST_ID: %@", aRequestId);
                    break;
                }
                case MEETING_RESPONSE_CALENDAR_ID:
                {
                    NSString* aCalendarId = [aParser getString];
                    FXDebugLog(kFXLogActiveSync, @"MEETING_RESPONSE_CALENDAR_ID: %@", aCalendarId);
                    break;
                }
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Ping response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML: %@", kCommand, aContentType);
        }
        if(delegate) {
            [delegate meetingResponseSuccess];
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
        if(delegate) {
            [delegate meetingResponseFailed:[NSHTTPURLResponse localizedStringForStatusCode:self.statusCode]];
        }
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
    [sPings removeObject:self];
    [super connectionDidFinishLoading:connection];
}


@end
