//
//  ASConfigViewController.h
//
//  Created by Gabor Cselle on 3/22/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "BaseConfigViewController.h"
#import "ASConfigViewDelegate.h"
#import "ASOptionsDelegate.h"
#import "ASTypes.h"
#import "SZLApplicationContainer.h"

@class ASAccount;
@class Reachability;

@interface ASConfigViewController : BaseConfigViewController <UITextFieldDelegate, UIAlertViewDelegate, ASConfigViewDelegate, ASOptionsDelegate, UIGestureRecognizerDelegate>
{	    
    NSString* name;
    NSString* userName;
	NSString* password;
    NSString* PCC;
    
	NSString* server;
	NSString* domain;

	int port;

	EEncryptionMethod encryption;
    EAuthenticationMethod authentication;
        
    BOOL configureInProgress;
    BOOL isReachableHost;
    BOOL observingReachability;
}

@property (strong) Reachability*    reachability;
@property (strong) NSString*        hostName;
@property (strong) NSString*        path;

@property (nonatomic, assign) id <SZLConfigurationDelegate> configurationDelegate;

+(UINavigationController *)accountConfigurationControllerWithDelegate:(id <SZLConfigurationDelegate>)configurationDelegate;

-(IBAction)loginClick;
-(IBAction)backgroundClick;
-(IBAction)selectFoldersClicked;
-(void)configureLaterClicked:(id)sender;

-(IBAction)gmailActiveSyncClicked;
-(IBAction)gmailFixmoClicked;
-(IBAction)exchangeClicked;
-(IBAction)fixmoClickedStk08;
-(IBAction)fixmoClickedStk09;
-(IBAction)fixmoClickedStk01;

@end

