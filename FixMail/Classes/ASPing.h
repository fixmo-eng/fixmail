/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"

@class ASAccount;

typedef enum {
    kPingNoStatus                       = 0,
    kPingNoChanges                      = 1,
    kPingChangesOccurred                = 2,
    kPingOmittedParameters              = 3,
    kPingSyntaxError                    = 4,
    kPingIntervalOutOfRange             = 5,
    kPingTooManyFolders                 = 6,
    kPingFolderHierarchySyncRequired    = 7,
    kPingServerError                    = 8,
    kPingInvalidWBXML                   = 102
} EPingStatus;

@interface ASPing : WBXMLRequest {
    NSArray*                    folders;
    NSMutableArray*             changedFolders;

    BOOL                        emptyPing;
}
// Properties
@property (strong) NSArray*         folders;
@property (strong) NSMutableArray*  changedFolders;
@property (nonatomic) EPingStatus   statusCodeAS;


@property (nonatomic) BOOL          emptyPing;

// Factory
+ (void)sendPing:(ASAccount*)anAccount folders:(NSArray*)aFolders;
+ (void)sendEmptyPing:(ASAccount*)anAccount folders:(NSArray*)aFolders;

+ (BOOL)accountHasPing:(ASAccount*)anAccount;
+ (void)removePingsForAccount:(ASAccount*)anAccount;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount folders:(NSArray*)aFolders;

@end
