/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncReadStatus.h"
#import "ASAccount.h"
#import "ASEmail.h"
#import "ASFolder.h"
#import "ASSync.h"
#import "WBXMLDataGenerator.h"

@implementation ASSyncReadStatus

@synthesize folder;
@synthesize emails;


// Constants
static NSString* kCommand               = @"Sync";
static const ASTag kCommandTag          = SYNC_SYNC;

///////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(ASFolder*)aFolder email:(ASEmail*)anEmail
{
    if (self = [super init]) {
        self.folder     = aFolder;
        self.emails     = [NSArray arrayWithObject:anEmail];
    }
    return self;
}

- (id)initWithFolder:(ASFolder*)aFolder emails:(NSArray*)anEmails
{
    if (self = [super init]) {
        self.folder     = aFolder;
        self.emails     = anEmails;
    }
    return self;
}

- (void)send
{
    ASSync* aSync;
    if(self.emails.count > 0) {
        aSync = [self sendChangeReadStatus:self.folder emails:self.emails];
    }else{
        FXDebugLog(kFXLogASEmail, @"sync read status invalid");
    }
    if(aSync) {
        [super sendAndWait:aSync];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
// Email Read Status
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Email Read Status

- (ASSync*)sendChangeReadStatus:(ASFolder*)aFolder emails:(NSArray*)anEmails
{
    ASSync* aSync = nil;
    @try {
        ASAccount* anAccount = (ASAccount*)[aFolder account];
        aSync = [[ASSync alloc] initWithAccount:anAccount];
        aSync.syncFolders       = [NSArray arrayWithObject:aFolder];
        //aSync.isChangeRequest   = YES;
        aSync.syncDelegate      = self;
        NSData* aWbxml = [self wbxmlChangeReadStatus:aFolder emails:anEmails sync:aSync];
        
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
        [anAccount send:aURLRequest httpRequest:aSync];
    }@catch (NSException* e) {
        [HttpRequest logException:@"sendChangeReadStatus" exception:e];
    }
    return aSync;
}

- (NSData*)wbxmlChangeReadStatus:(ASFolder*)aFolder emails:(NSArray*)anEmails sync:(ASSync*)aSync
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(SYNC_COLLECTIONS); {
            wbxml.start(SYNC_COLLECTION); {
                wbxml.keyValue(SYNC_SYNC_KEY, [aFolder syncKey]);
                wbxml.keyValue(SYNC_COLLECTION_ID, [aFolder uid]);
                wbxml.start(SYNC_COMMANDS); {
                    for(ASEmail* anEmail in anEmails) {
                        wbxml.start(SYNC_CHANGE); {
                            wbxml.keyValue(SYNC_SERVER_ID, [anEmail uid]);
                            wbxml.start(SYNC_APPLICATION_DATA); {
                                wbxml.keyValueInt(EMAIL_READ, (int)[anEmail isRead]);
                            }wbxml.end();
                        }wbxml.end();
                    }
                }wbxml.end();
            }wbxml.end();
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(aSync, kCommandTag);
}

///////////////////////////////////////////////////////////////////////////////////////////
// ASSyncDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASSyncDelegate

- (void)syncSuccess:(NSArray*)aSyncFolders isPinging:(BOOL)isPinging
{
    NSMutableDictionary* aUserInfo = [NSMutableDictionary dictionary];
    [aUserInfo setObject:self.emails forKey:kReadStatusEmails];
    [[NSNotificationCenter defaultCenter] postNotificationName:kReadStatusSyncedNotification
                                                        object:self
                                                      userInfo:aUserInfo];
}

- (void)syncFailed:(NSArray*)aSyncFolders
        httpStatus:(EHttpStatusCode)aStatusCode
        syncStatus:(ESyncStatus)aSyncStatus
{
    NSMutableDictionary* aUserInfo = [NSMutableDictionary dictionary];
    [aUserInfo setObject:self.emails forKey:kReadStatusEmails];
    [aUserInfo setObject:[NSNumber numberWithInt:aSyncStatus] forKey:kReadStatusStatusCode];

    [[NSNotificationCenter defaultCenter] postNotificationName:kReadStatusSyncedNotification
                                                        object:self
                                                      userInfo:aUserInfo];
    
}

@end