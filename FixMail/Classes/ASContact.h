/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseContact.h"
#import "AS.h"

@class ASFolder;
@class FastWBXMLParser;

typedef enum {
	kFirstNameMatch = 1,
	kMiddleNameMatch = 2,
	kLastNameMatch = 3,
	kEmailAddress1Match = 4,
	kEmailAddress2Match = 5,
	kEmailAddress3Match = 6
} ERecipientSearchMatch;

@interface ASContact : BaseContact
{
 	// for searching
	NSString				*_matchedCriteria;
	NSString				*_matchedString;
}

// ActiveSync specific
@property (nonatomic, strong) ASFolder *folder;
@property (nonatomic, strong) NSString *serverId;

@property (nonatomic, assign) int contactID;
@property (nonatomic, assign) int contactType;

@property (nonatomic, assign) bool isFavorite;
@property (nonatomic, assign) bool isMe;
@property (nonatomic, assign) bool isNew;

// used by ui - specifically editContactVC & listContactsVC
@property (nonatomic, readonly) bool isInLocalDB;
@property (nonatomic, assign) bool pushToLocalDB;
@property (nonatomic, readonly) bool isOnServer;
@property (nonatomic, readonly) bool isCompany;

// Minimal criteria
@property (nonatomic, strong) NSData *picture;
@property (nonatomic, strong) NSString *pictureString;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *middleName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *alias;

// Optional company stuff
@property (nonatomic, strong) NSString *company;
@property (nonatomic, strong) NSString *department;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *companyLocation;
@property (nonatomic, strong) NSString *webPage;

// Email
@property (nonatomic, strong) NSString *emailAddress1;
@property (nonatomic, strong) NSString *emailAddress2;
@property (nonatomic, strong) NSString *emailAddress3;

// Phones
@property (nonatomic, strong) NSString *mobilePhone;
@property (nonatomic, strong) NSString *pager;
@property (nonatomic, strong) NSString *otherPhone1;
@property (nonatomic, strong) NSString *otherPhone2;

// Addresses
@property (weak, nonatomic, readonly) NSString *companyAddress;
@property (nonatomic, strong) NSString *companyStreet;
@property (nonatomic, strong) NSString *companyCity;
@property (nonatomic, strong) NSString *companyState;
@property (nonatomic, strong) NSString *companyCountry;
@property (nonatomic, strong) NSString *companyPostalCode;
@property (nonatomic, strong) NSString *companyPhone1;
@property (nonatomic, strong) NSString *companyPhone2;
@property (nonatomic, strong) NSString *companyFax;

@property (weak, nonatomic, readonly) NSString *homeAddress;
@property (nonatomic, strong) NSString *homeStreet;
@property (nonatomic, strong) NSString *homeCity;
@property (nonatomic, strong) NSString *homeState;
@property (nonatomic, strong) NSString *homeCountry;
@property (nonatomic, strong) NSString *homePostalCode;
@property (nonatomic, strong) NSString *homePhone1;
@property (nonatomic, strong) NSString *homePhone2;
@property (nonatomic, strong) NSString *homeFax;

@property (weak, nonatomic, readonly) NSString *otherAddress;
@property (nonatomic, strong) NSString *otherStreet;
@property (nonatomic, strong) NSString *otherCity;
@property (nonatomic, strong) NSString *otherState;
@property (nonatomic, strong) NSString *otherCountry;
@property (nonatomic, strong) NSString *otherPostalCode;

// Other
@property (nonatomic, strong) NSString *notes;
@property (nonatomic, strong) NSString *spouse;
@property (nonatomic, strong) NSString *birthday;

// not doing anything with this... yet
@property (nonatomic, assign) int weightedRank;

@property (weak, nonatomic, readonly) NSString *firstAndMiddleName;
@property (weak, nonatomic, readonly) NSString *displayName;
@property (weak, nonatomic, readonly) NSString *sortKey;

// used for ASSyncContact
@property (weak, nonatomic, readonly) NSString *uniqueKey;

// for the contact image
@property (nonatomic, strong) UIImage *image;
//- (void) setImageWithData:(NSData *)inData;

// for searching
- (bool) matchesSearchString:(NSString *)inSearchString;
-(NSString *)nameMatchesSearchString:(NSString*)searchString;
-(NSString *)emailMatchesSearchString:(NSString*)searchString;
-(ERecipientSearchMatch)matchEmailOrNameSearchString:(NSString *)searchString;
@property (weak, nonatomic, readonly) NSString *matchedCriteria;
@property (weak, nonatomic, readonly) NSString *matchedString;

// showContactVC
@property (weak, nonatomic, readonly) NSArray *tableArrayStrings;

// Construct/Destruct
//- (id)initWithDisplayName:(NSString*)aDisplayName emailAddress:(NSString*)anEmailAddress;
- (id)initWithFolder:(ASFolder*)aFolder;
- (void) reset;

// Parser
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;

// Contact information
-(BOOL)hasName;

@end
