/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import <Foundation/Foundation.h>

@class HttpRequest;

@interface HttpConnection : NSURLConnection {
    NSMutableData*		data;		
}

@property (strong) NSString*        identifier;
@property (strong) NSURL*           URL;
@property (strong) HttpRequest* httpRequest;
@property (readonly) NSMutableData* data;


// ConstructDestruct
- (id)initWithRequest:(NSURLRequest *)aUrlRequest 
          httpRequest:(HttpRequest*)anHttpRequest
			  runLoop:(NSRunLoop*)aRunLoop;

- (void)resetData;
- (void)appendData:(NSData *)aData;

@end
