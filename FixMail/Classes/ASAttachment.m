/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASAttachment.h"
#import "FastWBXMLParser.h"
#import "NSString+SBJSON.h"

@implementation ASAttachment

// Properties
@synthesize contentId;
@synthesize method;

// Attachment estimated size is consistently overestimated by 64 bytes so we have to correct it
//
static const NSUInteger kAttachmentEstimatedSizeAdjustment = 64;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (NSMutableArray*)attachmentsFromJSON:(NSString*)aJSONString;
{
    NSMutableArray* anAttachments = [NSMutableArray array];
    NSArray* anObjects = [aJSONString JSONValue];
    for(NSDictionary* aDictionary in anObjects) {
        [anAttachments addObject:[ASAttachment attachmentFromDictionary:aDictionary]];
    }
    return anAttachments;
}

+ (ASAttachment*)attachmentFromDictionary:(NSDictionary*)aDictionary
{
    ASAttachment* anAttachment = [[ASAttachment alloc] init];
    [anAttachment fromDictionary:aDictionary];
    
    return anAttachment;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
	if (self = [super init]) {
	}
	return self;
}


////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)fetch
{
    
}

////////////////////////////////////////////////////////////////////////////////
// Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser

- (void)parser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    
    while ((tag = [aParser nextTag:BASE_ATTACHMENT]) != AS_END) {
        switch(tag) {
            case BASE_DISPLAY_NAME:
            {
                self.name = [aParser getString];
                NSString* anExtension = [self.name pathExtension];
                if([anExtension length]) {
                    self.contentType = anExtension;
                }
                break;
            }
            case BASE_FILE_REFERENCE:
                self.fileReference = [aParser getString];
                break;
            case BASE_METHOD:
                self.method = (EAttachmentMethod)[aParser getInt];
                break;
            case BASE_ESTIMATED_DATA_SIZE:
                self.estimatedDataSize = [aParser getInt] - kAttachmentEstimatedSizeAdjustment;
                break;
            case BASE_IS_INLINE:
                [self setIsInline:[aParser getBool]];
                break;
            case BASE_CONTENT_ID:
                self.contentId = [aParser getString];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
    if([contentId length] > 0) {
        [self writeString:string		tag:@"contentId"        value:contentId];
    }
	[self writeInt:string               tag:@"method"           value:method];
    
    if([self isFetched])    [self writeBoolean:string           tag:@"isFetched"        value:TRUE];
    if([self isRead])       [self writeBoolean:string           tag:@"isRead"           value:TRUE];
    if([self isInline])     [self writeBoolean:string           tag:@"isInline"         value:TRUE];
    
    if([self isFetching])   [self writeBoolean:string           tag:@"isFetching"       value:TRUE];

	[string appendString:@"}\n"];
	
	return string;
}

@end
