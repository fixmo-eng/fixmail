/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseAccount.h"
#import "ASConfigViewDelegate.h"
#import "ASFolder.h"
#import "ASFolderSyncDelegate.h"
#import "ASSyncDelegate.h"
#import "ASTypes.h"
#import "TraceDelegate.h"

@class ASContact;
@class Event;
@class ASProvisionPolicy;
@class HttpRequest;
@protocol ASOptionsDelegate;

#define kContentTypeWBXML   @"application/vnd.ms-sync.wbxml"
#define kInitialSyncKey     @"0"

@interface ASAccount : BaseAccount <ASSyncDelegate, ASFolderSyncDelegate> {
    BOOL                        provisionFailed;    // Set true if provision hanshake failed

    NSTimer*                    progressMessageTimer;   
    // ActiveSync Preferences
    BOOL                        autoSync;
  
    EFilterType                 emailFilterType;
    EFilterType                 calendarFilterType;
    
    EBodyType                   bodyType;
}

// Properties
@property (nonatomic) BOOL      isInitialSynced;
@property (nonatomic) BOOL      hasGetAttachment;

// Static
+ (void)startPings;
+ (void)removePings;
- (void)removePing;
- (BOOL)hasPing;
+ (void)loadExistingEmailAccounts;

// Construct/Destruct
- (id)initWithAccountNum:(int)anAccountNum;
- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUsername
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(EEncryptionMethod)anEncryption
        authentication:(EAuthenticationMethod)anAuthentication
       folderNames:(NSArray *)aFolderNames;

// Load/Store
- (void)loadSettings;
- (void)commitSettings;

// ActiveSync preferences
- (BOOL)autoSync;
- (void)setAutoSync:(BOOL)anAutoSync;

- (NSString*)stringForTimeFilter:(EFilterType)aFilterType;
- (NSArray*)filterTypeStrings;

- (EFilterType)filterTypeForString:(NSString*)aString;
- (BOOL)isFilterTypeSupported:(EFilterType)aFilterType folderType:(EFolderType)aFolderType;

- (EFilterType)emailFilterType;
- (EFilterType)calendarFilterType;

- (void)setEmailTimeFilter:(NSString*)aTimeFilterString shouldResync:(BOOL)shouldResync;
- (void)setCalendarTimeFilter:(NSString*)aTimeFilterString shouldResync:(BOOL)shouldResync;

- (EBodyType)bodyType;
- (void)setBodyType:(NSString*)aBodyTypeString;

- (void)setActiveSyncPreferences:(BOOL)aShouldResync;

// ASOptions
- (void)options:(NSObject<ASOptionsDelegate>*)aDelegate;
- (void)optionsSet;
- (void)optionsAborted:(NSString*)aMesg;

// ASFolderSync
- (void)folderSync;
- (BOOL)initialSync;
- (void)initialSyncFolder:(ASFolder*)aFolder;
- (BOOL)isInitialSynced;
- (void)resetForInitialSync;

// ASAccount subclass virtuals
- (NSArray*)foldersToSync;          // Required override
- (NSArray*)specialFoldersToSync;   // Optional override

// ASAccount subclass helpers
- (BaseFolder*)addFolderToArray:(NSMutableArray*)aFolders name:(NSString*)aName;
- (BaseFolder*)addFolderToArray:(NSMutableArray*)aFolders serverId:(NSString*)aServerId;
- (void)addFoldersToArray:(NSMutableArray*)aFolders ofFolderType:(EFolderType)aFolderType;

// ASProvision
- (void)needsProvisioning:(WBXMLRequest*)aFailedRequest;
- (void)provision;
- (void)setProvisionReceived:(NSString *)aPolicyKey;
- (void)setProvisionAcknowledged:(NSString *)aPolicyKey;
- (void)setProvisionFailed;

// ASPing
- (void)startPing;
- (void)ping:(NSArray*)aFolders;

// URL Request
- (NSMutableURLRequest*)createOptionsRequest:(NSString*)anASCommand;
- (NSMutableURLRequest*)createAttachmentRequest:(NSString*)anASCommand;
- (NSMutableURLRequest*)createAutoDiscoverRequest:(NSString*)aUrlString xml:(NSString*)anXML;
- (NSMutableURLRequest*)createMailRequestWithCommand:(NSString*)anASCommand mime:(NSData*)aMIMEData;
- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand wbxml:(NSData*)aWbxml;
- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand multipart:(BOOL)aWantMultipart wbxml:(NSData*)aWbxml;
- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand attachment:(NSString*)anAttachment;
- (void)send:(NSMutableURLRequest*)aURLRequest httpRequest:(HttpRequest*)anHttpRequest;

// Factory Overrides
- (ASContact*)createContact;
- (Event*)createEvent;
- (ASEmail*)createEmailForFolder:(BaseFolder*)aFolder;
- (BaseFolder*)createFolderForFolderType:(EFolderType)aFolderType;

- (ASFolder*)createLocalDraftsFolder;

//  BaseAccount Overrides
- (void)addFolder:(BaseFolder*)aFolder;
- (void)removeAllFolders;
- (void)sendEmail:(ASEmail*)anEmail;

// Getters
- (NSString*)getClientID;

// Setters
- (void)setVersions:(NSArray*)versions;

@end
