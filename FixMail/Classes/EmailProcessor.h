//
//  EmailProcessor.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 6/29/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "DBaseEngine.h"

@class ASEmail;
@class ASEmailFolder;
@class BaseFolder;
@class CommitObjects;

@interface EmailProcessor : DBaseEngine {
	int                 opsInTransaction;
}

// Properties
@property (nonatomic, strong) NSOperationQueue *operationQueue;
@property (nonatomic, strong) id updateSubscriber;

// Factory
+(id)getSingleton;
+ (NSDateFormatter*)dateFormatter;

+ (BOOL)debugAccount;
+ (BOOL)debugDatabase;
+ (BOOL)debugEvent;
+ (BOOL)debugSearch;
+ (BOOL)debugUid;

// Static
+(void)clearPreparedStmts;
+(int)folderCountLimit;

+(int)combinedFolderNumFor:(int)folderNumInAccount withAccount:(int)accountNum;
+(int)folderNumForCombinedFolderNum:(int)folderNum;
+(int)accountNumForCombinedFolderNum:(int)folderNum;	

// Transactions
-(void)beginTransactions;
-(void)endTransactions;

// Add/Chamge/Delete
-(void)queueUpdateBodyAndFlags:(ASEmail*)anEmail;
-(void)queueUpdateFlags:(ASEmail*)anEmail;
-(void)queueUpdateFlagsForEmails:(NSArray*)anEmails;
-(void)queueUpdateMeetingRequest:(ASEmail*)anEmail;

-(void)commitObjects:(CommitObjects*)aCommitObjects;

@end
