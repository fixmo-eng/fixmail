/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
//
//  Email.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 1/16/09.
//  Copyright 2010 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASEmail.h"
#import "AddEmailDBAccessor.h"
#import "AppSettings.h"
#import "ASAccount.h"
#import "ASAttachment.h"
#import "ASCompose.h"
#import "ASEmailFolder.h"
#import "ASFlag.h"
#import "ASMeetingResponse.h"
#import "ASEmailThread.h"
#import "Attendee.h"
#import "CalendarEmail.h"
#import "DateUtil.h"
#import "EmailAddress.h"
#import "EmailProcessor.h"
#import "ErrorAlert.h"
#import "LoadEmailDBAccessor.h"
#import "MeetingRequest.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation ASEmail

// Properties
@synthesize folder;
@synthesize folderNum;
@synthesize uid;
@synthesize datetime;
@synthesize from;
@synthesize replyTo;
@synthesize toJSON;
@synthesize ccJSON;
@synthesize bccJSON;
@synthesize subject;
@synthesize preview;
@synthesize body;
@synthesize attachments;
@synthesize meetingRequest;
@synthesize estimatedSize;
@synthesize flags;
@synthesize messageClass;
@synthesize bodyType;
@synthesize displayTo;

// Flags Stored
static const NSUInteger kFlagsStoredMask  = 0x0000ffff;

static const NSUInteger kIsFetched        = 0x00000001;
static const NSUInteger kIsRead           = 0x00000002;
static const NSUInteger kIsMIME           = 0x00000004;
static const NSUInteger kIsHTML           = 0x00000008;
static const NSUInteger kIsRTF            = 0x00000010;
static const NSUInteger kIsImportant      = 0x00000020;
static const NSUInteger kIsNotImportant   = 0x00000040;
static const NSUInteger kIsFlagged        = 0x00000080;
static const NSUInteger kHasOfflineChange = 0x00000100;

// Flags Transient
static const NSUInteger	kIsFetching     = 0x00010000;
static const NSUInteger	kIsDeleted      = 0x00020000;
static const NSUInteger	kIsChanged      = 0x00040000;
static const NSUInteger kIsNew          = 0x00080000;
static const NSUInteger kIsMarked       = 0x00100000;

// Static
static NSCharacterSet*      sContactCleaner = NULL;
static NSDateFormatter*     sDateFormatter = NULL;

// Constants
static NSString* kNameKey        = @"n";
static NSString* kEmailKey       = @"e";

////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (NSDateFormatter*)dateFormatter
{
    if(!sDateFormatter)  {
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sDateFormatter = [[NSDateFormatter alloc] init];
        [sDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sDateFormatter;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(BaseFolder*)aFolder
{
	if (self = [super init]) {
        folder = aFolder;
        folderNum = [aFolder combinedFolderNum];
        
        if(!sDateFormatter)  {
            [ASEmail dateFormatter];
        }
    }
	return self;
}

- (void)dealloc
{
    folder = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EMAIL", @"ErrorAlert", @"ASEmail error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EMAIL", @"ErrorAlert", nil) message:anErrorMessage];
}

+ (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EMAIL", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////
// Database
////////////////////////////////////////////////////////////////////////////////
#pragma mark Database

+ (void)addExec:(NSString*)aSQLString
{
    char* errorMsg;
    int aResult = sqlite3_exec([[AddEmailDBAccessor sharedManager] database], [aSQLString UTF8String] , NULL, NULL, &errorMsg);
	if(aResult != SQLITE_OK) {
        FXDebugLog(kFXLogActiveSync, @"add exec Failed: %@", aSQLString);
        [self handleError:[NSString stringWithFormat:@"%@: %s", FXLLocalizedStringFromTable(@"ADD_EXEC_FAILED", @"ErrorAlert", @"Error message when add exec failed"), errorMsg]];
	}
}

+ (void)loadExec:(NSString*)aSQLString
{
    char* errorMsg;
    int aResult = sqlite3_exec([[LoadEmailDBAccessor sharedManager] database], [aSQLString UTF8String] , NULL, NULL, &errorMsg);
	if(aResult != SQLITE_OK) {
        FXDebugLog(kFXLogActiveSync, @"load exec Failed: %@", aSQLString);
        [self handleError:[NSString stringWithFormat:@"%@: %s", FXLLocalizedStringFromTable(@"LOAD_EXEC_FAILED", @"ErrorAlert", @"Error message when load exec failed"), errorMsg]];
	}
}

+ (void)tableCheck
{
#ifdef DEBUG
    if([EmailProcessor debugDatabase]) {
        FXDebugLog(kFXLogActiveSync, @"tableCheck");
    }
#endif
    [self addExec:
     @"CREATE TABLE IF NOT EXISTS email ("
     "uid VARCHAR(20) PRIMARY KEY, "
     "datetime REAL, "
     "sender_name VARCHAR(50), "
     "sender_address VARCHAR(50), "
     "tos TEXT, "
     "ccs TEXT, "
     "bccs TEXT, "
     "attachments TEXT, "
     "meeting TEXT, "
     "estimatedSize INTEGER, "
     "folder_num INTEGER, "
     "folder_num_1 INTEGER, "
     "folder_num_2 INTEGER, "
     "folder_num_3 INTEGER, "
     "subject VARCHAR(256), "
     "flags INTEGER, "
     "messageClass INTEGER, "
     "preview TEXT, "
     "thread TEXT, "
     "body TEXT);"
     ];
    
    [self addExec:@"CREATE INDEX IF NOT EXISTS email_datetime on email (datetime desc);"];
	[self addExec:@"CREATE INDEX IF NOT EXISTS email_folder_num_0 on email (folder_num);"];
	[self addExec:@"CREATE INDEX IF NOT EXISTS email_folder_num_1 on email (folder_num_1);"];
	[self addExec:@"CREATE INDEX IF NOT EXISTS email_folder_num_2 on email (folder_num_2);"];
	[self addExec:@"CREATE INDEX IF NOT EXISTS email_folder_num_3 on email (folder_num_3);"];
}

+ (NSString*)sqlFields
{
    return [[ASEmail sqlFieldsNoBody] stringByAppendingString:@", body "];
}

+ (NSString*)sqlFieldsNoBody
{
    return @"uid, "               // uid must be first
    "datetime, "          // dateTime must be second
    "sender_name, "
    "sender_address, "
    "tos, "
    "ccs, "
    "bccs, "
    "attachments, "
    "meeting, "
    "estimatedSize, "
    "folder_num, "
    "subject, "
    "flags, "
    "messageClass, "
    "preview, "
    "thread";
}

+ (NSString*)uidFromSQL:(void*)aSQLStatement
{
    sqlite3_stmt * aStatement = (sqlite3_stmt *)aSQLStatement;
    return getStringForField(aStatement, 0);   // Must match index in sqlFields
}

+ (void)deleteWithUid:(NSString*)aUid
{
#ifdef DEBUG
    if([EmailProcessor debugDatabase]) {
        FXDebugLog(kFXLogActiveSync, @"deleteWithUid=%@", aUid);
    }
#endif
    NSString* aString = [NSString stringWithFormat:@"DELETE FROM email WHERE uid='%@';", aUid];
    [ASEmail addExec:aString];
}

static NSString* getStringForField(sqlite3_stmt* emailLoadStmt, int aFieldNum)
{
    NSString* aString = NULL;
    const char * sqlVal = (const char *)sqlite3_column_text(emailLoadStmt, aFieldNum);
    if(sqlVal != nil) {
        aString = [NSString stringWithUTF8String:sqlVal];
    }else{
        aString = @"";
    }
    return aString;
}

- (void)loadFromSQL:(void*)aSQLStatement loadBody:(BOOL)aLoadBody
{
    sqlite3_stmt * aStatement = (sqlite3_stmt *)aSQLStatement;
    self.uid                = getStringForField(aStatement,  0);
    
    const char* aDateString = (const char *)sqlite3_column_text(aStatement, 1);
    if(aDateString) {
        self.datetime = [[EmailProcessor dateFormatter] dateFromString:[NSString stringWithUTF8String:aDateString]];
    }else{
        self.datetime = [NSDate date]; // default == now!
    }
    
    self.from               = getStringForField(aStatement,  2);
    self.replyTo            = getStringForField(aStatement,  3);
    self.toJSON             = getStringForField(aStatement,  4);
    self.ccJSON             = getStringForField(aStatement,  5);
    self.bccJSON            = getStringForField(aStatement,  6);
    
    // Attachment
    NSString* anAttachJSON  = getStringForField(aStatement,  7);
    if(anAttachJSON.length > 2) {
        self.attachments = [ASAttachment attachmentsFromJSON:anAttachJSON];
    }
    
    // Meeting
    NSString* aMeetingJSON  = getStringForField(aStatement,  8);
    if(aMeetingJSON.length > 2) {
        MeetingRequest* aMeetingRequest = [[MeetingRequest alloc] initWithJSON:aMeetingJSON];
        self.meetingRequest = aMeetingRequest;
    }
    self.estimatedSize      = sqlite3_column_int(aStatement, 9);
    self.folderNum          = sqlite3_column_int(aStatement, 10);
    self.subject            = getStringForField(aStatement,  11);
    
    folder = [BaseAccount folderForCombinedFolderNum:self.folderNum];
    if(!folder) {
        FXDebugLog(kFXLogActiveSync, @"loadFromSQL invalid folder");
    }
    
    self.flags              = sqlite3_column_int(aStatement, 12);
    self.messageClass       = sqlite3_column_int(aStatement, 13);
    
    self.preview            = [[getStringForField(aStatement,  14) componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
    
    // Thread
    NSString* anEmailThreadJSON  = getStringForField(aStatement,  15);
    if(anEmailThreadJSON.length > 2) {
        ASEmailThread* anEmailThread = [[ASEmailThread alloc] initWithJSON:anEmailThreadJSON];
        self.emailThread = anEmailThread;
    }
    
    if(aLoadBody) {
        const char* aBodyString = (const char *)sqlite3_column_text(aStatement, 16);
        if(aBodyString != nil) {
#warning FIXME shouldn't load the body when processing change requests or doing housekeeping, unnecessary bloat
#ifdef DEBUG
            if([EmailProcessor debugDatabase]) {
                FXDebugLog(kFXLogActiveSync, @"loadFromSQL body length: %lu flags: 0x%x", strlen(aBodyString), self.flags);
            }
#endif
            self.body = [NSData dataWithBytes:aBodyString length:strlen(aBodyString)];
        }else{
            self.body = nil;
        }
    }else{
        self.body = nil;
    }
    
    
#ifdef DEBUG
    if([EmailProcessor debugDatabase]) {
        FXDebugLog(kFXLogActiveSync, @"loadFromSQL uid=%@ fn=%d body length=%d", self.uid, self.folderNum, [self.body length]);
    }
#endif
    //FXDebugLog(kFXLogActiveSync, @"ASEmail loadData: %@", self);
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (NSString*)_flatStringFromJSON:(NSString*)aJSONString
{
    NSMutableString* aFlatString = [NSMutableString stringWithCapacity:50];
    
    if([aJSONString length] > 0) {
        NSArray* anArray = [aJSONString JSONValue];
        for(NSDictionary* aDictionary in anArray) {
            NSString* aName = [aDictionary objectForKey:kNameKey];
            if([aName length] > 0) {
                [aFlatString appendFormat:@"%@ ", aName];
            }
            NSString* anAddress = [aDictionary objectForKey:kEmailKey];
            if([anAddress length] > 0) {
                [aFlatString appendFormat:@"%@ ", anAddress];
            }
        }
    }
    return [aFlatString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString*)toFlat
{
    return [self _flatStringFromJSON:self.toJSON];
}

- (NSString*)ccFlat
{
    return [self _flatStringFromJSON:self.ccJSON];
}

- (NSString*)bccFlat
{
    return [self _flatStringFromJSON:self.bccJSON];
}

- (NSString*)_addressStringFromJSON:(NSString*)aJSONString
{
    NSMutableString* anAddressString = [NSMutableString stringWithCapacity:50];
    
    if([aJSONString length] > 0) {
        BOOL isFirst = TRUE;
        NSArray* anArray = [aJSONString JSONValue];
        for(NSDictionary* aDictionary in anArray) {
            NSString* aName = [aDictionary objectForKey:kNameKey];
            NSString* anAddress = [aDictionary objectForKey:kEmailKey];
            if(isFirst) {
                isFirst = FALSE;
            }else{
                if([aName length] > 0 || [anAddress length] > 0) {
                    [anAddressString appendString:@","];
                }
            }
            if([aName length] > 0) {
                [anAddressString appendFormat:@"%@ ", aName];
            }
            if([anAddress length] > 0) {
                [anAddressString appendFormat:@"<%@>", anAddress];
            }
        }
    }
    return [anAddressString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString*)toAsAddressString
{
    return [self _addressStringFromJSON:self.toJSON];
}

- (NSString*)ccAsAddressString
{
    return [self _addressStringFromJSON:self.ccJSON];
}

- (NSString*)bccAsAddressString
{
    return [self _addressStringFromJSON:self.bccJSON];
}


- (NSArray*)_arrayOfAddressStringsFromJSON:(NSString*)aJSONString
{
    NSMutableArray* anAddressArray = [NSMutableArray array];
    
    if([aJSONString length] > 0) {
        NSArray* anArray = [aJSONString JSONValue];
        for(NSDictionary* aDictionary in anArray) {
            NSMutableString* anAddressString = [NSMutableString stringWithCapacity:50];
            NSString* aName = [aDictionary objectForKey:kNameKey];
            NSString* anAddress = [aDictionary objectForKey:kEmailKey];
            if([aName length] > 0) {
                [anAddressString appendFormat:@"%@ ", aName];
            }
            if([anAddress length] > 0) {
                [anAddressString appendFormat:@"<%@>", anAddress];
            }
            if([anAddress length] > 0) {
                [anAddressArray addObject:anAddressString];
            }
        }
    }
    return anAddressArray;
}

- (NSArray*)_arrayOfEmailAddressesFromJSON:(NSString*)aJSONString
{
    NSMutableArray* anAddressArray = [NSMutableArray array];
    
    if([aJSONString length] > 0) {
        NSArray* anArray = [aJSONString JSONValue];
        for(NSDictionary* aDictionary in anArray) {
            NSString* aName = [aDictionary objectForKey:kNameKey];
            NSString* anAddress = [aDictionary objectForKey:kEmailKey];
			EmailAddress *anEmail = [[EmailAddress alloc] initWithName:aName address:anAddress];
			[anAddressArray addObject:anEmail];
        }
    }
    return anAddressArray;
}

- (NSArray*)toAsArrayOfEmailAddresses
{
    return [self _arrayOfEmailAddressesFromJSON:self.toJSON];
}

- (NSArray*)ccAsArrayOfEmailAddresses
{
    return [self _arrayOfEmailAddressesFromJSON:self.ccJSON];
}

- (NSArray*)bccAsArrayOfEmailAddresses
{
    return [self _arrayOfEmailAddressesFromJSON:self.bccJSON];
}

- (NSArray*)toAsArrayOfAddressStrings
{
    return [self _arrayOfAddressStringsFromJSON:self.toJSON];
}

- (NSArray*)ccAsArrayOfAddressStrings
{
    return [self _arrayOfAddressStringsFromJSON:self.ccJSON];
}

- (NSArray*)bccAsArrayOfAddressStrings
{
    return [self _arrayOfAddressStringsFromJSON:self.bccJSON];
}

- (EmailAddress*)emailAddressFromString:(NSString*)aNameAndAddress
{
    if(!sContactCleaner)  {
        sContactCleaner = [NSCharacterSet characterSetWithCharactersInString:@" \">"];
    }
    
    NSString* aName = NULL;
    NSString* anAddresss = NULL;
    NSArray* aFields = [aNameAndAddress componentsSeparatedByString:@"<"];
    NSUInteger aCount = [aFields count];
    for(int i = 0 ; i < aCount ; ++i) {
        NSString* aString = [aFields objectAtIndex:i];
        switch(i) {
            case 0:
                aName = [aString stringByTrimmingCharactersInSet:sContactCleaner];
                break;
            case 1:
                anAddresss = [aString stringByTrimmingCharactersInSet:sContactCleaner];
                break;
        }
    }
    EmailAddress* anEmailAddress = [[EmailAddress alloc] initWithName:aName address:anAddresss];
    return anEmailAddress;
}

- (NSArray*)emailAddressesFromString:(NSString*)aNameAndAddress
{
    //FXDebugLog(kFXLogActiveSync, @"emailAddressesFromString: %@", aNameAndAddress);
    NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:2];
    NSArray* aFields = [aNameAndAddress componentsSeparatedByString:@","];
    NSUInteger aCount = [aFields count];
    for(int i = 0 ; i < aCount ; ++i) {
        EmailAddress* anAddress = [self emailAddressFromString:[aFields objectAtIndex:i]];
        //FXDebugLog(kFXLogActiveSync, @"emailAddressesFromString %d:\n%@", i, anAddress);
        [anArray addObject:[anAddress asDictionary]];
    }
    return anArray;
}

- (NSDictionary*)_dictionaryFromString:(NSString*)aNameAndAddress
{
    if(!sContactCleaner)  {
        sContactCleaner = [NSCharacterSet characterSetWithCharactersInString:@" \">"];
    }
    
    NSString* aName = NULL;
    NSString* anAddresss = NULL;
    NSArray* aFields = [aNameAndAddress componentsSeparatedByString:@"<"];
    NSUInteger aCount = [aFields count];
    for(int i = 0 ; i < aCount ; ++i) {
        NSString* aString = [aFields objectAtIndex:i];
        switch(i) {
            case 0:
                aName = [aString stringByTrimmingCharactersInSet:sContactCleaner];
                break;
            case 1:
                anAddresss = [aString stringByTrimmingCharactersInSet:sContactCleaner];
                break;
        }
    }
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:2];
    if([aName length] > 0) {
        [aDictionary setObject:aName forKey:kNameKey];
    }
    
    if([anAddresss length] > 0) {
        [aDictionary setObject:anAddresss forKey:kEmailKey];
    }
    return aDictionary;
}

- (NSString*)JSONFromAddressesString:(NSString*)anAddressString
{
    //FXDebugLog(kFXLogActiveSync, @"JSONFromAddressesString string: %@", anAddressString);
    NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:2];
    NSArray* aFields = [anAddressString componentsSeparatedByString:@","];
    NSUInteger aCount = [aFields count];
    for(int i = 0 ; i < aCount ; ++i) {
        NSDictionary* aDictionary = [self _dictionaryFromString:[aFields objectAtIndex:i]];
        //FXDebugLog(kFXLogActiveSync, @"JSONFromAddressesString %d:\n%@", i, aDictionary);
        [anArray addObject:aDictionary];
    }
    NSString* aJSON = [anArray JSONRepresentation];
    //FXDebugLog(kFXLogActiveSync, @"JSONFromAddressesString JSON: %@", aJSON);
    return aJSON;
}

- (BOOL)hasMeeting
{
    return self.meetingRequest != nil;
}

- (BOOL)hasAttachments
{
	return self.attachments.count > 0;
}

- (BOOL)hasAttachmentWithSuffix:(NSString*)aSuffix
{
    BOOL aHasAttachment = FALSE;
    
    for(ASAttachment* anAttachment in self.attachments) {
        if([anAttachment.name hasSuffix:aSuffix]) {
            aHasAttachment = TRUE;
            break;
        }
    }
    return aHasAttachment;
}

- (NSArray*)attachmentWithSuffix:(NSString*)aSuffix
{
    NSMutableArray* anAttachments = [NSMutableArray arrayWithCapacity:self.attachments.count];
    
    for(ASAttachment* anAttachment in self.attachments) {
        if([anAttachment.name hasSuffix:aSuffix]) {
            [anAttachments addObject:anAttachment];
        }
    }
    return anAttachments;
}

- (NSString*)attachmentsAsJSON
{
    NSString* aJSONString = nil;
    // Convert attachment object array to JSON string
    //
    if(self.attachments.count > 0) {
        NSMutableArray* aJSONArray = [NSMutableArray arrayWithCapacity:self.attachments.count];
        for(ASAttachment* anAttachment in self.attachments) {
            NSDictionary* aDictionary = [anAttachment asDictionary];
            [aJSONArray addObject:aDictionary];
        }
        aJSONString = [aJSONArray JSONRepresentation];
    }
    return aJSONString;
}

////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setToFromAddressString:(NSString*)anAddressString
{
    self.toJSON = [self JSONFromAddressesString:anAddressString];
}

- (void)setCCFromAddressString:(NSString*)anAddressString
{
    self.ccJSON = [self JSONFromAddressesString:anAddressString];
}

- (void)setBCCFromAddressString:(NSString*)anAddressString
{
    self.bccJSON = [self JSONFromAddressesString:anAddressString];
}

- (void)setImportanceHigh
{
    [self setIsImportant:TRUE];
    [self setIsNotImportant:FALSE];
}

- (void)setImportanceNormal
{
    [self setIsImportant:FALSE];
    [self setIsNotImportant:FALSE];
}

- (void)setImportanceLow
{
    [self setIsImportant:FALSE];
    [self setIsNotImportant:TRUE];
}

- (void)loadBody:(NSData*)aData
{
    self.body = aData;
    self.isFetched = TRUE;
    self.isFetching = FALSE;
    ASEmailFolder* aFolder = (ASEmailFolder*)self.folder;
    ASAccount* anAccount = (ASAccount*)aFolder.account;
    switch([anAccount bodyType]) {
        case kBodyTypeMIME:
        {
            self.isMIME = TRUE;
            self.isHTML = FALSE;
            break;
        }
        case kBodyTypeHTML:
        {
            self.isMIME = FALSE;
            self.isHTML = TRUE;
            break;
        }
        case kBodyTypePlainText:
        case kBodyTypeRTF:      // FIXME Do I need a flag for RTF?
        case kBodyTypeNone:
        {
            self.isHTML = FALSE;
            self.isMIME = FALSE;
            break;
        }
    }
    
    [[EmailProcessor getSingleton] queueUpdateBodyAndFlags:self];
}

////////////////////////////////////////////////////////////////////////////////
// Read Status
////////////////////////////////////////////////////////////////////////////////
#pragma mark Read Status

- (void)changeReadStatus:(BOOL)isRead
{
    if(isRead != [self isRead]) {
        [self setIsRead:isRead];
        [self.folder changeObject:self changeType:kChangeReadStatus updateServer:TRUE updateDatabase:TRUE];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Unnecessary call to changeReadStatus: %@", self);
    }
}


////////////////////////////////////////////////////////////////////////////////
// Meeting Response
////////////////////////////////////////////////////////////////////////////////
#pragma mark Meeting Response

- (NSString*)_responseAsPlainTextForAccount:(BaseAccount*)anAccount
{
    NSString *aString;
    
    if([self.meetingRequest isAccepted]) {
        
        aString = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((name))_ACCEPTED_MEETING_ON_%@((date))\n\n", @"ASEmail", @"Message saying person (Name) accepted meeting on specific date (Date)"), anAccount.name, [self.meetingRequest.startDate descriptionWithLocale:[NSLocale currentLocale]]];
        
    }else if([self.meetingRequest isDeclined]) {
        
        aString = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((name))_DECLINED_MEETING_ON_%@((date))\n\n", @"ASEmail", @"Message saying person (Name) declined meeting on specific date (Date)"), anAccount.name, [self.meetingRequest.startDate descriptionWithLocale:[NSLocale currentLocale]]];
        
    }else if([self.meetingRequest isTentativeAccepted]) {
        
        aString = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((name))_TENTATIVELY_ACCEPTED_MEETING_ON_%@((date))\n\n", @"ASEmail", @"Message saying person (Name) accepted meeting on specific date (Date)"), anAccount.name, [self.meetingRequest.startDate descriptionWithLocale:[NSLocale currentLocale]]];
        
    }else{
        FXDebugLog(kFXLogCalendar, @"_responseAsPlainTextForAccount invalid");
        return nil;
    }
    
    return aString;
}

- (void)_sendICalResponse
{
    ASAccount* anAccount = (ASAccount*)self.folder.account;
    
    // Build the iCalendar response based on the meeting request in this email
    //
    NSString* anIcalString   = [self.meetingRequest asICalForAccount:anAccount
                                                             subject:self.subject];
    //FXDebugLog(kFXLogASEmail,  @"sendICalResponse:\n%@", anIcalString);
    
    // Build the text body for the email for the organizer to read if he opens this email
    //
    NSString* aTextString    = [self _responseAsPlainTextForAccount:anAccount];
    //FXDebugLog(kFXLogASEmail,  @"aTextString:\n%@", aTextString);

    // Build the MIME message with the text body and iCalendar file
    //
    Attendee* anOrganizer = [[Attendee alloc] initWithString:self.meetingRequest.organizer];
    if(anIcalString.length > 0 && aTextString.length > 0) {
        NSString* aResponse;
        MeetingRequest* aMeetingRequest = self.meetingRequest;
        if([aMeetingRequest isAccepted]) {
            aResponse = FXLLocalizedStringFromTable(@"ACCEPTED", @"ASEmail", @"Calendar email subject when accept invitation");
        }else if([aMeetingRequest isTentativeAccepted]) {
            aResponse = FXLLocalizedStringFromTable(@"TENTATIVE", @"ASEmail", @"Calendar email subject when tentatively accept invitation");
        }else if([aMeetingRequest isDeclined]) {
            aResponse = FXLLocalizedStringFromTable(@"DECLINED", @"ASEmail", @"Calendar email subject when declined invitation");
        }else{
            FXDebugLog(kFXLogCalendar, @"_meetingResponseAsMIME invalid");
            return;
        }
        NSString* aSubject = [NSString stringWithFormat:@"%@: %@", aResponse, self.subject];
        
        NSString* aMessageString = [CalendarEmail calendarAsMIMEToAttendee:anOrganizer
                                                                   subject:aSubject
                                                                      body:aTextString
                                                                iCalString:anIcalString];
        //FXDebugLog(kFXLogActiveSync, @"anInvitation: %@", aMessageString);
        
        [ASCompose sendMail:anAccount string:aMessageString delegate:nil];
    }else{
        FXDebugLog(kFXLogCalendar, @"_sendICalResponse failed");
    }
}

- (void)meetingUserResponse:(EUserResponse)aUserResponse
{
    switch(aUserResponse) {
        case kUserResponseAccept:
            [self.meetingRequest setAccepted];
            break;
        case kUserResponseTentativelyAccept:
            [self.meetingRequest setTentativeAccepted];
            break;
        case kUserResponseDecline:
            [self.meetingRequest setDeclined];
            ASCalendarFolder* aCalendarFolder = [CalendarEmail calendarFolderForAccount:(ASAccount*)self.folder.account];
            [CalendarEmail removeMeeting:self folder:aCalendarFolder];
            break;
    }
    //[folder changeObject:self changeType:kChangeMeetingUserResponse updateServer:FALSE updateDatabase:TRUE];

    // First tell ActiveSync what our response is using an Active Sync Meeting Response
    //
    // When this completes then we will send a MIME message to the organizer with a text/calendar part
    // that tells the organizer what our response is
    //
#warning FIXME meetingUserResponse instanceDate for exceptions
    [ASMeetingResponse sendMeetingResponse:self userResponse:aUserResponse instanceDate:nil delegate:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// ASMeetingResponseDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASMeetingResponseDelegate

- (void)meetingResponseSuccess
{
    [self _sendICalResponse];
}

- (void)meetingResponseFailed:(NSString *)aMessage
{
    FXDebugLog(kFXLogCalendar, @"meetingResponseFailed: %@", aMessage);
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parsers
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)_baseBodyParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:BASE_BODY]) != AS_END) {
        switch(tag) {
            case BASE_DATA:
            {
                NSString* aString = [aParser getString];
                if([aString length] > 0) {
                    self.preview = aString;   // NOTE: this is only for preview body, full body should go through ASItemOperations
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME _baseBodyParser no data");
                }
                break;
            }
            case BASE_TYPE:
            {
				self.bodyType = [aParser getIntTraceable];
                //FXDebugLog(kFXLogActiveSync, @"ASEmail body preference type: %d", self.bodyType);
                break;
            }
            case BASE_ESTIMATED_DATA_SIZE:
            {
                self.estimatedSize = [aParser getUnsignedIntTraceable];
                break;
            }
            case BASE_TRUNCATED:
            {
                BOOL isTruncated = [aParser getIntTraceable];
				[self setIsFetched:!isTruncated];
                //FXDebugLog(kFXLogActiveSync, @"ASEmail isTruncated: %D", isTruncated);
                break;
            }
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)_categoryParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
#warning FIXME need test case to implement, categories are often empty
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case EMAIL_CATEGORY:
                FXDebugLog(kFXLogActiveSync, @"EMAIL_CATEGORY: %@", [aParser getStringTraceable]);
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)flagParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
#warning FIXME implement Email flag
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case EMAIL_FLAG_TYPE:
                FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG_TYPE: %@", [aParser getStringTraceable]);
                break;
            case EMAIL_FLAG_STATUS:
            {
                EFlagStatus aStatus = [aParser getIntTraceable];
                switch(aStatus) {
                    case kFlagStatusCleared:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG_STATUS Cleared");
                        break;
                    case kFlagStatusComplete:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG_STATUS Complete");
                        break;
                    case kFlagStatusActive:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG_STATUS Active");
                        break;
                    default:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG_STATUS Unknown");
                        break;
                }
                break;
            }
            case TASK_START_DATE:
                FXDebugLog(kFXLogActiveSync, @"TASK_START_DATE: %@", [aParser getStringTraceable]);
                break;
            case TASK_REMINDER_SET:
                FXDebugLog(kFXLogActiveSync, @"TASK_REMINDER_SET: %d", [aParser getBoolTraceable]);
                break;
            case TASK_REMINDER_TIME:
                FXDebugLog(kFXLogActiveSync, @"TASK_REMINDER_TIME: %@", [aParser getStringTraceable]);
                break;
            case TASK_DUE_DATE:
                FXDebugLog(kFXLogActiveSync, @"TASK_DUE_DATE: %@", [aParser getStringTraceable]);
                break;
            case TASK_UTC_DUE_DATE:
                FXDebugLog(kFXLogActiveSync, @"TASK_UTC_DUE_DATE: %@", [aParser getStringTraceable]);
                break;
            case TASK_UTC_START_DATE:
                FXDebugLog(kFXLogActiveSync, @"TASK_UTC_START_DATE: %@", [aParser getStringTraceable]);
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)setMessageClassFromString:(NSString*)aString
{
    // FIXME - Probable should build a dictionary here to avoid all these string tests
    //
    if([aString isEqualToString:@"IPM.Note"]) {
        self.messageClass = kMessageClassNote;
    }else if([aString hasPrefix:@"IPM.Note"]) {
        if([aString hasSuffix:@"Receipt.SMIME"]) {
            self.messageClass = kMessageClassNoteReceiptSMIME;
        }else if([aString hasSuffix:@"SMIME.MultipartSigned"]) {
            self.messageClass = kMessageClassNoteSMIMEMultipartSigned;
        }else if([aString hasSuffix:@"SMIME"]) {
            self.messageClass = kMessageClassNoteSMIME;
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Note: %@",aString);
            self.messageClass = kMessageClassNote;
        }
    }else if([aString hasPrefix:@"IPM.Schedule.Meeting"]) {
        if([aString hasSuffix:@"Request"]) {
            self.messageClass = kMessageClassScheduleMeetingRequest;
        }else if([aString hasSuffix:@"Canceled"]) {
            self.messageClass = kMessageClassScheduleMeetingCanceled;
        }else if([aString hasSuffix:@"Resp.Pos"]) {
            self.messageClass = kMessageClassScheduleMeetingResponsePos;
        }else if([aString hasSuffix:@"Resp.Neg"]) {
            self.messageClass = kMessageClassScheduleMeetingResponseNeg;
        }else if([aString hasSuffix:@"Resp.Tent"]) {
            self.messageClass = kMessageClassScheduleMeetingResponseTent;
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Schedule Meeting: %@",aString);
            self.messageClass = kMessageClassScheduleMeetingUnknown;
        }
    }else if([aString hasPrefix:@"IPM.Notification.Meeting"]) {
        self.messageClass = kMessageClassNotificationMeeting;
    }else if([aString hasPrefix:@"IPM.Post"]) {
        FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Post: %@",aString);
        self.messageClass = kMessageClassPost;
    }else if([aString hasPrefix:@"IPM.Octel.Voice"]) {
        FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Octel Voice: %@",aString);
        self.messageClass = kMessageClassOctelVoice;
    }else if([aString hasPrefix:@"IPM.Voicenotes"]) {
        FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Voice Notes: %@",aString);
        self.messageClass = kMessageClassVoiceNotes;
    }else if([aString hasPrefix:@"Sharing"]) {
        FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Voice Sharing: %@",aString);
        self.messageClass = kMessageClassVoiceSharing;
    }else if([aString hasPrefix:@"REPORT"] || [aString hasPrefix:@"*REPORT"]) {
        FXDebugLog(kFXLogFIXME, @"FIXME MessageClass REPORT: %@",aString);
        if([aString hasPrefix:@"IPM.Note.NDR"]) {
            self.messageClass = kMessageClassReportUndeliverable;
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME MessageClass Report: %@",aString);
            self.messageClass = kMessageClassReport;
        }
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME setMessageClassFromString: %@", aString);
        self.messageClass = kMessageClassUnknown;
    }
}

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    NSString* aMessageClass = nil;
    ASTag tag;
    while ((tag = [aParser nextTag:aTag]) != AS_END) {
        switch(tag) {
            case BASE_BODY:
                [self _baseBodyParser:aParser];
                break;
            case EMAIL_TO:
            {
                self.toJSON = [self JSONFromAddressesString:[aParser getString]];
                //FXDebugLog(kFXLogActiveSync, @"toJSON: %@", self.toJSON);
                break;
            }
            case EMAIL_FROM:
            {
                NSString* aFromString = [aParser getString];
                //FXDebugLog(kFXLogActiveSync, @"from: %@", aFromString);
                EmailAddress* anEmailAddress = [self emailAddressFromString:aFromString];
                self.from = anEmailAddress.name;
                if([anEmailAddress.address length] > 0) {
                    self.replyTo = anEmailAddress.address;
                }
                break;
            }
            case EMAIL_CC:
            {
                self.ccJSON = [self JSONFromAddressesString:[aParser getString]];
                //FXDebugLog(kFXLogActiveSync, @"ccJSON: %@", self.ccJSON);
                break;
            }
            case EMAIL_REPLY_TO:
            {
                NSString* aReplyToString = [aParser getString];
                //FXDebugLog(kFXLogActiveSync, @"replyTo: %@", aReplyToString);
                EmailAddress* anEmailAddress = [self emailAddressFromString:aReplyToString];
                self.replyTo = [anEmailAddress address];
                break;
            }
            case EMAIL_SUBJECT:
                self.subject = [aParser getString];
                break;
            case EMAIL_DATE_RECEIVED:
            {
                NSString* aDateString = [aParser getStringTraceable];
                NSDate* dateFromString = [sDateFormatter dateFromString:aDateString];
                //FXDebugLog(kFXLogActiveSync, @"EMAIL_DATE_RECEIVED %@ %@ %@", aDateString, [dateFromString description], [dateFromString descriptionWithLocale:[NSLocale currentLocale]]);
                self.datetime = dateFromString;
                break;
            }
            case EMAIL_DISPLAY_TO:
                self.displayTo = [aParser getString];
                break;
            case EMAIL_IMPORTANCE:
            {
                EImportance anImportance = (EImportance)[aParser getIntTraceable];
                switch(anImportance) {
                    case kImportanceNormal:
                        [self setImportanceNormal];
                        break;
                    case kImportanceHigh:
                        [self setImportanceHigh];
                        break;
                    case kImportanceLow:
                        [self setImportanceLow];
                        break;
                }
                break;
            }
            case EMAIL_READ:
                [self setIsRead:[aParser getBoolTraceable]];
                break;
            case EMAIL_MESSAGE_CLASS:
                aMessageClass = [aParser getStringTraceable];
                [self setMessageClassFromString:aMessageClass];
                break;
            case BASE_ATTACHMENTS:
                break;
            case BASE_ATTACHMENT:
            {
                if(!self.attachments) {
                    self.attachments = [NSMutableArray arrayWithCapacity:5];
                }
                ASAttachment* anAttachment = [[ASAttachment alloc] init];
                [anAttachment parser:aParser];
                //FXDebugLog(kFXLogActiveSync, @"Attachment: %@", anAttachment);
                [self.attachments addObject:anAttachment];
                break;
            }
            case CONTACTS_EMAIL1_ADDRESS:
            {
                NSString* anEmailAddress = [aParser getString];
                #pragma unused(anEmailAddress)
                //FXDebugLog(kFXLogActiveSync, @"CONTACTS_EMAIL1_ADDRESS: %@", anEmailAddress);
                break;
            }
            case EMAIL_THREAD_TOPIC:
            {
                NSString* aTopic = [aParser getString];
                #pragma unused(aTopic)
                //FXDebugLog(kFXLogActiveSync, @"EMAIL_THREAD_TOPIC: %@", aTopic);
                break;
            }
            case EMAIL_INTERNET_CPID:
            {
                NSString* aCPID = [aParser getString];
                #pragma unused(aCPID)
                //FXDebugLog(kFXLogActiveSync, @"EMAIL_INTERNET_CPID: %@", aCPID);
                break;
            }
            case EMAIL_FLAG:
                [self flagParser:aParser tag:tag];
                break;
            case EMAIL_CONTENT_CLASS:
            {
                NSString* aClass = [aParser getStringTraceable];
                if([aClass isEqualToString:@"urn:content-classes:message"]) {               // Email
                }else if([aClass isEqualToString:@"urn:content-classes:calendarmessage"]) { // Meeting 
                }else if([aClass isEqualToString:@"urn:content-classes:dsn"]) {             // Undeliverable
                }else{
                    FXDebugLog(kFXLogActiveSync, @"EMAIL_CONTENT_CLASS: %@", aClass);
                }
                break;
            }
            case BASE_NATIVE_BODY_TYPE:
            {
                EBodyType aBodyType = (EBodyType)[aParser getIntTraceable];
                #pragma unused(aBodyType)
                //FXDebugLog(kFXLogActiveSync, @"BASE_NATIVE_BODY_TYPE: %d", aBodyType);
                break;
            }
            case EMAIL_MEETING_REQUEST:
                self.meetingRequest = [[MeetingRequest alloc] init];
                [self.meetingRequest parser:aParser tag:tag messageClass:aMessageClass];
                break;
            case EMAIL_CATEGORIES:
                [self _categoryParser:aParser tag:tag];
                break;
            case EMAIL2_CONVERSATION_INDEX:
            {
                // FIXME - Not currently used, might cause malloc issues in FastWBXMLParser on this NSData return
                //
                // 5 byte time tag of original message and 5 byte response level(deltaT) for all responses so far
                //
                //NSData* aData = [aParser getData];
                //FXDebugLog(kFXLogActiveSync, @"EMAIL2_CONVERSATION_INDEX: %d", aData.length);
                //const unsigned char* aBytes = aData.bytes;
                //for(int i = 0 ; i < aData.length ; ++i) {
                //    fprintf(stdout, "%02x ", aBytes[i]);
                //}
                break;
            }
            case EMAIL2_UM_CONVERSATION_ID:
            {
                // FIXME - Not currently used, might cause malloc issues in FastWBXMLParser on this NSData return
                //
                // 16 byte conversation ID
                //
                //NSData* aData = [aParser getData];
                //FXDebugLog(kFXLogActiveSync, @"EMAIL2_UM_CONVERSATION_ID: %d", aData.length);
                //const unsigned char* aBytes = aData.bytes;
                //for(int i = 0 ; i < aData.length ; ++i) {
                //    fprintf(stdout, "%02x ", aBytes[i]);
                //}
                break;
            }
            case EMAIL2_RECEIVED_AS_BCC:
            {
                BOOL aReceivedAsBCC = [aParser getBoolTraceable];
                #pragma unused(aReceivedAsBCC)
                //FXDebugLog(kFXLogActiveSync, @"EMAIL2_RECEIVED_AS_BCC: %d", aReceivedAsBCC);
                break;
            }
            case EMAIL2_LAST_VERB_EXECUTED:
            {
                ELastVerbExecuted aLastVerb = (ELastVerbExecuted)[aParser getIntTraceable];
                switch(aLastVerb) {
                    case kLastVerbReplyToSender:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL2_LAST_VERB_EXECUTED: reply to sender"); break;
                    case kLastVerbReplyToAll:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL2_LAST_VERB_EXECUTED: reply to all"); break;
                    case kLastVerbForward:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL2_LAST_VERB_EXECUTED: forward"); break;
                    default:
                        FXDebugLog(kFXLogActiveSync, @"EMAIL2_LAST_VERB_EXECUTED: unknown %d", aLastVerb); break;
                }
                break;
            }
            case EMAIL2_LAST_VERB_EXECUTION_TIME:
            {
                NSString* aLastVerbExecutionTime = [aParser getString];
                #pragma unused(aLastVerbExecutionTime)
                //FXDebugLog(kFXLogActiveSync, @"EMAIL2_LAST_VERB_EXECUTION_TIME: %@", aLastVerbExecutionTime);
                break;
            }
            case EMAIL2_SENDER:
            {
                NSString* aSender = [aParser getString];
                #pragma unused(aSender)
                //FXDebugLog(kFXLogActiveSync, @"EMAIL2_SENDER: %@", aSender);
                break;
            }
            default:
                if(tag == AS_END_DOCUMENT) {
                    break;
                }else{
                    [aParser skipTag];
                }
                break;
        }
    }
    
    if([self isFetching]) {
        [self setIsFetching:FALSE];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Misc
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Misc

- (void)store
{
    EmailProcessor* emailProcessor = [EmailProcessor getSingleton];
    NSInvocationOperation *nextOp = [[NSInvocationOperation alloc] initWithTarget:emailProcessor selector:@selector(addEmail:) object:self];
    [emailProcessor.operationQueue addOperation:nextOp];
}

- (NSString *) decodeBody
{
	if (self.body.length == 0) return @"";
    
	NSString *decodedBody = [[NSString alloc] initWithData:self.body encoding:NSUTF8StringEncoding];
	return decodedBody;
}

#if 0
- (NSString *) bodyPreview
{
	if (!self.isFetched) {
		if (self.body.length == 0) return FXLLocalizedStringFromTable(@"<BODY_OF_MESSAGE_ON_SERVER>", @"ASEmail", @"Email body preview when email on server");
		// do a quick translation
		_bodyPreview = [self decodeBody];
		FXDebugLog(kFXLogASEmail, @"bodyPreview (Preview): %@", _bodyPreview);
	} else if (_bodyPreview == nil) {
		PKMIMEData *mimeData = [[PKMIMEData alloc] initWithData:self.body];
		if (self.bodyType == kBodyTypePlainText) {
			_bodyPreview = mimeData.dataString;
			FXDebugLog(kFXLogASEmail, @"bodyPreview (kBodyTypePlainText): %@", _bodyPreview);
		} else if (self.bodyType == kBodyTypeRTF) {
			_bodyPreview = mimeData.dataString;
			FXDebugLog(kFXLogASEmail, @"bodyPreview (kBodyTypeRTF): %@", _bodyPreview);
		} else if (self.bodyType == kBodyTypeHTML) {
			_bodyPreview = mimeData.dataString;
			FXDebugLog(kFXLogASEmail, @"bodyPreview (kBodyTypeHTML): %@", _bodyPreview);
		} else if (self.bodyType == kBodyTypeMIME) {
			NSDate *start = [NSDate date];
			PKMIMEMessage *mimeMessage = [PKMIMEMessage messageWithData:mimeData];
			_bodyPreview = [mimeMessage partWithID:@"text/plain"].messageString;
			if (_bodyPreview.length == 0) {
				_bodyPreview = mimeMessage.messageString;
			}
			NSTimeInterval diffTime = [[NSDate date] timeIntervalSinceDate:start];
			FXDebugLog(kFXLogASEmail, @"bodyPreview (kBodyTypeMIME): %@ (%.2f)", _bodyPreview, (float)diffTime);
		} else {
			FXDebugLog(kFXLogASEmail, @"bodyPreview (kBodyTypeNone)");
		}
	}
    
	return _bodyPreview;
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////
// MIME
////////////////////////////////////////////////////////////////////////////////////////////
#pragma markMIME

- (NSString*)asMIMEString
{
#if 1
    NSMutableString* aMIMEString = [NSMutableString stringWithCapacity:[self.body length] + 100];
    
    // To
    //
    NSString* aToString = [self toAsAddressString];
    if([aToString length] > 0) {
        [aMIMEString appendFormat:@"%@: %@\r\n", FXLLocalizedStringFromTable(@"TO", @"ASEmail", @"Recipient field title"), aToString];
    }else{
        FXDebugLog(kFXLogActiveSync, @"SendEmail invalid to");
    }
#if 0
    NSString* emailAddress = [account emailAddress];
    if([emailAddress length] > 0) {
        NSString* aFromString = [NSString stringWithFormat:@"%@ <%@>", [account name], emailAddress];
        [aMIMEString appendFormat:@"%@: %@\r\n",  FXLLocalizedStringFromTable(@"FROM", @"ASEmail", @"Sender field title")aFromString];
    }else{
        FXDebugLog(kFXLogActiveSync, @"SendEmail invalid from");
    }
#endif
    
    // Subject
    //
    if([self.subject length] > 0) {
        [aMIMEString appendFormat:@"%@: %@\r\n",  FXLLocalizedStringFromTable(@"SUBJECT", @"ASEmail", @"Subject field title"), self.subject];
    }else{
        FXDebugLog(kFXLogActiveSync, @"SendEmail no subject");
    }
    
    // CC
    //
    NSString* aCCString = [self ccAsAddressString];
    if([aCCString length] > 0) {
        [aMIMEString appendFormat:@"%@: %@\r\n",  FXLLocalizedStringFromTable(@"CC", @"ASEmail", @"Cc field title"), aCCString];
    }
    
    NSString* aBCCString = [self bccAsAddressString];
    if([aBCCString length] > 0) {
        [aMIMEString appendFormat:@"%@: %@\r\n",  FXLLocalizedStringFromTable(@"BCC", @"ASEmail", @"Bcc field title"), aBCCString];
    }
    
    [aMIMEString appendString:@"\r\n\r\n"];
    if([self.body length] > 0) {
        NSString* aDataString = [[NSString alloc] initWithData:self.body encoding:NSUTF8StringEncoding];
        [aMIMEString appendString:aDataString];
    }
    //FXDebugLog(kFXLogActiveSync, @"asMIMEString: %@", aMIMEString);
    return aMIMEString;
#else
    NSDictionary* aHeaders = [NSMutableDictionary dictionaryWithCapacity:20];
    
    // To
    //
    NSString* aToString = [self toAsAddressString];
    if([aToString length] > 0) {
        [aHeaders setValue:aToString forKey:@"To"];
    }else{
        FXDebugLog(kFXLogActiveSync, @"SendEmail invalid to");
    }
    
    // Subject
    //
    if([self.subject length] > 0) {
        [aHeaders setValue:subject forKey:@"Subject"];
    }else{
        FXDebugLog(kFXLogActiveSync, @"SendEmail no subject");
    }
    
    // From & ReplyTo
    //
    [aHeaders setValue:[account emailAddress] forKey:@"From"];
    [aHeaders setValue:[NSString stringWithFormat:@"<%@>", [account emailAddress]] forKey:@"return-path"];
    //[aHeaders setValue:[NSString stringWithFormat:@"%@",replyTo] forKey:@"From"];
    
    // CC
    //
    NSString* aCCString = [self ccAsAddressString];
    if([aCCString length] > 0) {
        [aHeaders setValue:aCCString forKey:@"Cc"];
    }
    
    NSString* aBCCString = [self bccAsAddressString];
    if([aBCCString length] > 0) {
        [aHeaders setValue:aBCCString forKey:@"Bcc"];
    }
    
    // MIME version
    [aHeaders setValue:@"1.0" forKey:@"MIME-Version"];
    
    // Content Type
    //
    //[aHeaders setValue:@"text/plain" forKey:@"Content-Type"];
    [aHeaders setValue:@"text/plain; charset=\"UTF-8\""             forKey:@"Content-Type"];
    //[aHeaders setValue:@"text/plain; charset=\"us-ascii\""        forKey:@"Content-Type"];
    //[aHeaders setValue:@"text/plain; charset=\"windows-1252\""    forKey:@"Content-Type"];
    
    // Content Transfer encoding
    //
    //[aHeaders setValue:@"7bit" forKey:@"Content-Transfer-Encoding"];
    //[aHeaders setValue:@"8bit"                forKey:@"Content-Transfer-Encoding"];
    //[aHeaders setValue:@"quoted-printable"    forKey:@"Content-Transfer-Encoding"];
    //[aHeaders setValue:@"base64"              forKey:@"Content-Transfer-Encoding"];
    //[aHeaders setValue:@"binary"              forKey:@"Content-Transfer-Encoding"];
    
    //[aHeaders setValue:@"en-US"               forKey:@"Content-Language"];
    //[aHeaders setValue:@"en-US"               forKey:@"Accept-Language"];
    
    // Date
    //
    //[aHeaders setValue:[[NSDate date] description] forKey:@"Date"];
    
    // Build MIME message using PK MIMEKit
    //
    [self debugMIMEData:body];
    PKMIMEData* aPKMIMEData = [PKMIMEData dataFromData:[NSData dataWithBytes:[body bytes] length:[body length]]
                                            andHeaders:aHeaders];
    
    NSMutableString* aMIMEString = [NSMutableString stringWithString:[aPKMIMEData headerStrings]];
    [aMIMEString appendString:@"\r\n"];
    NSData* aRawData = [aPKMIMEData rawData];
    [self debugMIMEData:aRawData];
    
    [aMIMEString appendString:[aPKMIMEData rawDataString]];
    [aMIMEString appendString:@"\r\n"];
    
    FXDebugLog(kFXLogActiveSync, @"sendMail:\n%@", aMIMEString);
    [self debugMIMEString:aMIMEString];
    
    // NSData* aData = [aPKMIMEData data];
#endif
    return aMIMEString;
}

////////////////////////////////////////////////////////////////////////////////////////////
// MIME Debug
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MIME Debug

#ifdef DEBUG


- (void)_debugPrint:(const unsigned char*)aBytes length:(NSUInteger)aLength
{
    printf("\n");
    for(int i = 0 ; i < aLength ; ++i) {
        unsigned char c = aBytes[i];
        if(c >= ' ' && c < 0x7f) {
            printf("%c", c);
        }else{
            printf("0x%02x", c);
        }
    }
    printf("\n");
}

- (void)debugMIMEString:(NSString*)aMIMEString
{
    // Debug email before sending
    const unsigned char* str = (const unsigned char*)[aMIMEString UTF8String];
    int aLength = [aMIMEString length];
    [self _debugPrint:str length:aLength];
}

//extern void msglint(const char*);
//
//- (void)debugMsgLint:aMIMEString
//{
//   msglint([aMIMEString UTF8String]);
//}

- (void)debugMIMEData:(NSData*)aMIMEData
{
    // Debug email before sending
    const unsigned char* str = (const unsigned char*)[aMIMEData bytes];
    int aLength = [aMIMEData length];
    [self _debugPrint:str length:aLength];
}

#endif


////////////////////////////////////////////////////////////////////////////////
// Flags Stored
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags

- (NSUInteger)flagsToStore
{
    return flags & kFlagsStoredMask;
}
- (BOOL)isFetched
{
	return (flags & kIsFetched) != 0;
}

- (void)setIsFetched:(BOOL)state
{
	if(state)	flags |= kIsFetched;
	else		flags &= ~kIsFetched;
}

- (BOOL)isRead
{
	return (flags & kIsRead) != 0;
}

- (BOOL)isUnread
{
	return (flags & kIsRead) == 0;
}

- (void)setIsRead:(BOOL)state
{
	if(state)	flags |= kIsRead;
	else		flags &= ~kIsRead;
}

+ (int)isReadFlag
{
    return kIsRead;
}

- (BOOL)isMIME
{
	return (flags & kIsMIME) != 0;
}

- (void)setIsMIME:(BOOL)state
{
	if(state)	flags |= kIsMIME;
	else		flags &= ~kIsMIME;
}

- (BOOL)isHTML
{
	return (flags & kIsHTML) != 0;
}

- (void)setIsHTML:(BOOL)state
{
	if(state)	flags |= kIsHTML;
	else		flags &= ~kIsHTML;
}

- (BOOL)isRTF
{
	return (flags & kIsRTF) != 0;
}

- (void)setIsRTF:(BOOL)state
{
	if(state)	flags |= kIsRTF;
	else		flags &= ~kIsRTF;
}

- (BOOL)isImportant
{
	return (flags & kIsImportant) != 0;
}

- (void)setIsImportant:(BOOL)state
{
    if(state)	flags |= kIsImportant;
	else		flags &= ~kIsImportant;
}

- (BOOL)isNotImportant
{
    return (flags & kIsNotImportant) != 0;
}
- (void)setIsNotImportant:(BOOL)state
{
    if(state)	flags |= kIsNotImportant;
	else		flags &= ~kIsNotImportant;
}

- (BOOL)isFlagged
{
    return (flags & kIsFlagged) != 0;
}

- (void)setIsFlagged:(BOOL)state
{
    if(state)	flags |= kIsFlagged;
	else		flags &= ~kIsFlagged;
}

- (BOOL)hasOfflineChange
{
    return (flags & kHasOfflineChange) != 0;
}

- (void)setHasOfflineChange:(BOOL)state
{
    if(state)	flags |= kHasOfflineChange;
	else		flags &= ~kHasOfflineChange;
}

+ (int)hasOfflineChangeFlag
{
    return kHasOfflineChange;
}

////////////////////////////////////////////////////////////////////////////////
// Flags Transient
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Transient

- (BOOL)isFetching
{
	return (flags & kIsFetching) != 0;
}

- (void)setIsFetching:(BOOL)state
{
	if(state)	flags |= kIsFetching;
	else		flags &= ~kIsFetching;
}

- (BOOL)isDeleted
{
	return (flags & kIsDeleted) != 0;
}

- (void)setIsDeleted:(BOOL)state
{
	if(state)	flags |= kIsDeleted;
	else		flags &= ~kIsDeleted;
}

- (BOOL)isChanged
{
	return (flags & kIsChanged) != 0;
}

- (void)setIsChanged:(BOOL)state
{
	if(state)	flags |= kIsChanged;
	else		flags &= ~kIsChanged;
}

- (BOOL)isNew
{
	return (flags & kIsNew) != 0;
}

- (void)setIsNew:(BOOL)state
{
	if(state)	flags |= kIsNew;
	else		flags &= ~kIsNew;
}

- (BOOL)isMarked
{
	return (flags & kIsMarked) != 0;
}

- (void)setIsMarked:(BOOL)state
{
	if(state)	flags |= kIsMarked;
	else		flags &= ~kIsMarked;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
	[self writeString:string		tag:@"from"         value:from];
	[self writeString:string		tag:@"replyTo"      value:replyTo];
    
    if([self.uid length] > 0) {
        [self writeString:string    tag:@"uid"          value:uid];
    }
    
    if(datetime) {
        [self writeString:string    tag:@"datetime"     value:[datetime descriptionWithLocale:[NSLocale currentLocale]]];
    }
    
    [self writeString:string		tag:@"toJSON"       value:toJSON];
    if([ccJSON length]) {
        [self writeString:string    tag:@"ccJSON"       value:ccJSON];
    }
    if([bccJSON length]) {
        [self writeString:string    tag:@"bccJSON"      value:bccJSON];
    }
    if([subject length]) {
        [self writeString:string    tag:@"subject"      value:subject];
    }
    
    if(messageClass != kMessageClassNone) {
        NSString* aString = nil;
        switch(messageClass) {
            case kMessageClassNone:
                aString = @"None"; break;
                
            // Note
            case kMessageClassNote:
                aString = @"Note"; break;
            case kMessageClassNoteSMIME:
                aString = @"NoteSMIME"; break;
            case kMessageClassNoteReceiptSMIME:
                aString = @"NoteReceiptSMIME"; break;
            case kMessageClassNoteSMIMEMultipartSigned:
                aString = @"NoteSMIMEMultipartSigned"; break;
                
            // Post
            case kMessageClassPost:
                aString = @"Post"; break;
                
            // Meeting
            case kMessageClassScheduleMeetingRequest:
                aString = @"MeetingRequest"; break;
            case kMessageClassScheduleMeetingCanceled:
                aString = @"MeetingCanceled"; break;
            case kMessageClassScheduleMeetingResponsePos:
                aString = @"MeetingResponsePos"; break;
            case kMessageClassScheduleMeetingResponseNeg:
                aString = @"MeetingResponseNeg"; break;
            case kMessageClassScheduleMeetingResponseTent:
                aString = @"MeetingResponseTent"; break;
            case kMessageClassScheduleMeetingUnknown:
                aString = @"MeetingUnknown"; break;
            case kMessageClassNotificationMeeting:
                aString = @"NotificationMeeting"; break;
                
            // Report
            case kMessageClassReport:
                aString = @"Report"; break;
            case kMessageClassReportUndeliverable:
                aString = @"ReportUndeliverable"; break;
                
            // InfoPathForm
            case kMessageClassInfoPathForm:
                aString = @"InfoPathForm"; break;
                
            // Voice
            case kMessageClassOctelVoice:
                aString = @"OctelVoice"; break;
            case kMessageClassVoiceNotes:
                aString = @"VoiceNotes"; break;
            case kMessageClassVoiceSharing:
                aString = @"VoiceSharing"; break;
                
            // Unknown
            case kMessageClassUnknown:
            default:
                aString = @"Unknown"; break;
        }
        [self writeString:string    tag:@"messageClass"     value:aString];
    }
    
    if(estimatedSize > 0) {
        [self writeUnsigned:string  tag:@"estimatedSize"    value:estimatedSize];
    }
    if([preview length]) {
        [self writeString:string    tag:@"preview"          value:preview];
    }
    if([displayTo length] > 0) {
        [self writeString:string    tag:@"displayTo"        value:displayTo];
    }
    
    // FIXME bodyType
    
    // Flags Stored
    if(self.isFetched)          [self writeBoolean:string   tag:@"isFetched"        value:YES];
    if(self.isRead)             [self writeBoolean:string   tag:@"isRead"           value:YES];

    if(self.isMIME)             [self writeBoolean:string   tag:@"isMIME"           value:YES];
    if(self.isHTML)             [self writeBoolean:string   tag:@"isHTML"           value:YES];
    if(self.isRTF)              [self writeBoolean:string   tag:@"isRTF"            value:YES];
    
    if(self.isImportant)        [self writeBoolean:string   tag:@"isImportant"      value:YES];
    if(self.isNotImportant)     [self writeBoolean:string   tag:@"isNotImportant"   value:YES];
    if(self.isFlagged)          [self writeBoolean:string   tag:@"isFlagged"        value:YES];
    if(self.hasOfflineChange)   [self writeBoolean:string   tag:@"hasOfflineChange" value:YES];

    // Flags Transient
    if(self.isFetching)         [self writeBoolean:string   tag:@"isFetching"       value:YES];
    if(self.isDeleted)          [self writeBoolean:string   tag:@"isDeleted"        value:YES];
    if(self.isChanged)          [self writeBoolean:string   tag:@"isChanged"        value:YES];
    if(self.isNew)              [self writeBoolean:string   tag:@"isNew"            value:YES];
    if(self.isMarked)           [self writeBoolean:string   tag:@"isMarked"         value:YES];

    if(self.meetingRequest) {
        [string appendString:[self.meetingRequest description]];
    }
    
    if([body length]) {
        [self writeUnsigned:string tag:@"body length" value:[body length]];
    }
    
    if(self.attachments.count > 0) {
        for(ASAttachment* anAttachment in self.attachments) {
            [string appendString:anAttachment.description];
        }
    }
    
	[string appendString:@"}\n"];
	
	return string;
}

@end