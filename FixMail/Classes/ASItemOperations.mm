/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASItemOperations.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASItemOperationsDelegate.h"
#import "FXLSafeZone.h"
#import "HttpEngine.h"
#import "NSData+MIMETransferEncoding.h"
#import "WBXMLDataGenerator.h"

@implementation ASItemOperations

// Properties
@synthesize delegate;
@synthesize target;
@synthesize selector;
@synthesize statusCodeAS;
@synthesize isBodyFetch;
@synthesize folder;
@synthesize email;
@synthesize filePath;
@synthesize fileData;
@synthesize percentReported;
@synthesize expectedSize;
@synthesize bytesDownloaded;
@synthesize range;
@synthesize smimeMessage;

// Constants
static NSString* kCommand       = @"ItemOperations";
static NSString* kGetAttachment = @"GetAttachment";
static const ASTag kCommandTag  = ITEM_OPERATIONS_ITEM_OPERATIONS;

typedef enum {
    kMIMESupportForNone                     = 0,
    kMIMESupportForSMIME                    = 1,
    kMIMESupportForAll                      = 2,
} EMIMESupport;

//static NSString* kMaxTruncationSize     = @"1000000000";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASItemOperations*)sendRequest:(ASAccount*)anAccount
                   fileReference:(NSString*)aFileReference 
                        filePath:(NSString*)aFilePath
                    expectedSize:(NSUInteger)anExpectedSize
                        delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.fileReference   = aFileReference;
    anItemOperation.cmd             = kItemOperationsCmdAttachment;
    anItemOperation.filePath        = aFilePath;
    anItemOperation.delegate        = aDelegate;
    anItemOperation.expectedSize    = anExpectedSize;
    if(anAccount.accountSubType == AccountSubTypeGmail || anAccount.accountSubType == AccountSubTypeDomino) {
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kGetAttachment attachment:aFileReference];
        [anAccount send:aURLRequest httpRequest:anItemOperation];
    }else{
        [anItemOperation send];
    }
    return anItemOperation;
}

+ (ASItemOperations*)queueRequest:(ASAccount*)anAccount
                    fileReference:(NSString*)aFileReference
                         filePath:(NSString*)aFilePath
                     expectedSize:(NSUInteger)anExpectedSize
                         delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.fileReference   = aFileReference;
    anItemOperation.cmd             = kItemOperationsCmdAttachment;
    anItemOperation.filePath        = aFilePath;
    anItemOperation.delegate        = aDelegate;
    anItemOperation.expectedSize    = anExpectedSize;
    if(anAccount.accountSubType == AccountSubTypeGmail || anAccount.accountSubType == AccountSubTypeDomino) {
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kGetAttachment  attachment:aFileReference];
        [anAccount send:aURLRequest httpRequest:anItemOperation];
    }else{
        [anItemOperation queue];
    }

    return anItemOperation;
}


+ (ASItemOperations*)sendRequest:(ASAccount*)anAccount
                   fileReference:(NSString*)aFileReference
                        filePath:(NSString*)aFilePath
                           range:(NSRange)aRange
                        delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd             = kItemOperationsCmdAttachmentWithRange;
    anItemOperation.fileReference   = aFileReference;
    anItemOperation.filePath        = aFilePath;
    anItemOperation.delegate        = aDelegate;
    if(anAccount.accountSubType == AccountSubTypeGmail || anAccount.accountSubType == AccountSubTypeDomino) {
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kGetAttachment  attachment:aFileReference];
        [anAccount send:aURLRequest httpRequest:anItemOperation];
    }else{
        anItemOperation.range           = aRange;
        anItemOperation.expectedSize    = aRange.length;
        [anItemOperation send];
    }
    
    return anItemOperation;
}

+ (ASItemOperations*)queueRequest:(ASAccount*)anAccount
                    fileReference:(NSString*)aFileReference
                         filePath:(NSString*)aFilePath
                            range:(NSRange)aRange
                         delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd             = kItemOperationsCmdAttachmentWithRange;
    anItemOperation.fileReference   = aFileReference;
    anItemOperation.filePath        = aFilePath;
    anItemOperation.delegate        = aDelegate;
    if(anAccount.accountSubType == AccountSubTypeGmail || anAccount.accountSubType == AccountSubTypeDomino) {
        NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kGetAttachment attachment:aFileReference];
        [anAccount send:aURLRequest httpRequest:anItemOperation];
    }else{
        anItemOperation.range           = aRange;
        anItemOperation.expectedSize    = aRange.length;
        [anItemOperation queue];
    }
    return anItemOperation;
}

+ (ASItemOperations*)sendFetchEmailBody:(ASEmail*)anEmail
								  smime:(BOOL)anSMIMEMessage
                               delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    [anEmail setIsFetching:TRUE];
    
    ASAccount* anAccount = (ASAccount*)anEmail.folder.account;
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd             = kItemOperationsCmdFetchEmailBody;
    anItemOperation.email			= anEmail;
    anItemOperation.delegate		= aDelegate;
    anItemOperation.isBodyFetch		= TRUE;
	anItemOperation.smimeMessage	= anSMIMEMessage;
    [anItemOperation send];
    
    return anItemOperation;
}

+ (ASItemOperations*)queueFetchEmailBody:(ASEmail*)anEmail
                                   smime:(BOOL)anSMIMEMessage
                                delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    [anEmail setIsFetching:TRUE];
    
    ASAccount* anAccount = (ASAccount*)anEmail.folder.account;
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd             = kItemOperationsCmdFetchEmailBody;
    anItemOperation.email			= anEmail;
    anItemOperation.delegate		= aDelegate;
    anItemOperation.isBodyFetch		= TRUE;
	anItemOperation.smimeMessage	= anSMIMEMessage;
    [anItemOperation queue];
    
    return anItemOperation;
}

+ (ASItemOperations*)sendEmptyFolderContents:(ASFolder*)aFolder
                                    delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{    
    ASAccount* anAccount = (ASAccount*)aFolder.account;
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd         = kItemOperationsCmdEmptyFolder;
    anItemOperation.folder      = aFolder;
    anItemOperation.delegate    = aDelegate;    
    [anItemOperation send];
    
    return anItemOperation;
}

+ (ASItemOperations*)queueEmptyFolderContents:(ASFolder*)aFolder
                                     delegate:(NSObject<ASItemOperationsDelegate>*)aDelegate
{
    ASAccount* anAccount = (ASAccount*)aFolder.account;
    ASItemOperations* anItemOperation = [[ASItemOperations alloc] initWithAccount:anAccount];
    anItemOperation.cmd         = kItemOperationsCmdEmptyFolder;
    anItemOperation.folder      = aFolder;
    anItemOperation.delegate    = aDelegate;
    [anItemOperation queue];
    
    return anItemOperation;
}

- (void)send
{
    NSData* aWbxml = nil;
    
    switch(self.cmd) {
        case kItemOperationsCmdAttachment:
            aWbxml = [self wbxmlFetch:self.account fileReference:self.fileReference];
            break;
        case kItemOperationsCmdAttachmentWithRange:
            aWbxml = [self wbxmlFetch:self.account fileReference:self.fileReference];
            break;
        case kItemOperationsCmdFetchEmailBody:
            aWbxml = [self wbxmlFetchEmailBody:self.email];
            break;
        case kItemOperationsCmdEmptyFolder:
            aWbxml = [self wbxmlEmptyFolder:self.folder];
            break;
    }
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand multipart:TRUE wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlFetch:(ASAccount*)anAccount fileReference:(NSString*)aFileReference
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(ITEM_OPERATIONS_FETCH); {
            wbxml.keyValue(ITEM_OPERATIONS_STORE, @"Mailbox");
            wbxml.keyValue(BASE_FILE_REFERENCE, aFileReference);
            if(self.range.length > 0) {
                wbxml.keyValue(ITEM_OPERATIONS_RANGE,
                               [NSString stringWithFormat:@"%u-%d",
                                    self.range.location,
                                    self.range.location+self.range.length]);
            }
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (void)emailOptions:(WBXMLDataGenerator&)wbxml
{
    wbxml.start(ITEM_OPERATIONS_OPTIONS); {
        if([self.account ASVersion] >= kASVersion2007) {
            wbxml.start(BASE_BODY_PREFERENCE); {
				if (self.smimeMessage) {
					wbxml.keyValueInt(BASE_TYPE, kBodyTypeMIME);
					wbxml.keyValueInt(BASE_ALL_OR_NONE, 1);
				} else {
					wbxml.keyValueInt(BASE_TYPE, [self.account bodyType]);
					//wbxml.keyValue(BASE_TRUNCATION_SIZE, kMaxTruncationSize);
					if ([self.account bodyType] == kBodyTypeHTML) {
						wbxml.keyValueInt(BASE_ALL_OR_NONE, 1);
					}
				}
            }wbxml.end();
        }else{
            FXDebugLog(kFXLogActiveSync, @"ASItemOperations fetchBody requires ActiveSync 12.0 or greater");
        }
        
        if(([self.account bodyType] == kBodyTypeMIME) || (self.smimeMessage)) {
            wbxml.keyValueInt(SYNC_MIME_SUPPORT, kMIMESupportForSMIME);
		}

    }wbxml.end();
}

- (NSData*)wbxmlFetchEmailBody:(ASEmail*)anEmail
{
    WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(ITEM_OPERATIONS_FETCH); {
            wbxml.keyValue(ITEM_OPERATIONS_STORE, @"Mailbox");
            wbxml.keyValue(SYNC_COLLECTION_ID, anEmail.folder.uid);
            wbxml.keyValue(SYNC_SERVER_ID, anEmail.uid);
            [self emailOptions:wbxml];
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlEmptyFolder:(ASFolder*)aFolder
{
    WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(ITEM_OPERATIONS_EMPTY_FOLDER_CONTENTS); {
            wbxml.keyValue(SYNC_COLLECTION_ID, aFolder.uid);
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)reportActiveSyncError:(NSString*)anErrorMessage
{
    if(delegate) {
        [delegate deliverError:anErrorMessage itemOp:self];
    }else{
        [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"ITEM_OPERATIONS", @"ErrorAlert", @"Error alert view message for ASItemOperations"), anErrorMessage]];
    }
}

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kItemOperationsSuccess:
            break;
#ifdef DEBUG
        case kItemOperationsProtocolError:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"PROTOCOL_ERROR", @"ASItemOperations", @"Message for ActiveSync Error - Protocol error")]; break;
        case kItemOperationsServerError:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"SERVER_ERROR", @"ASItemOperations", @"Message for ActiveSync Error - Server error")]; break;
        case kItemOperationsDocumentURIBad:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"DOCUMENT_URI_BAD", @"ASItemOperations", @"Message for ActiveSync Error - Document URI bad")]; break;
        case kItemOperationsDocumentAccessDenied:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"ACCESS_DENIED", @"ASItemOperations", @"Message for ActiveSync Error - Access denied")]; break;
        case kItemOperationsDocumentNotFound:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"DOCUMENT_NOT_FOUND", @"ASItemOperations", @"Message for ActiveSync Error - Document not found")]; break;
        case kItemOperationsDocumentFailedServerConnect:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"DOCUMENT_FAILED_SERVER_CONNECT", @"ASItemOperations", @"Message for ActiveSync Error - Document failed server connect")]; break;
        case kItemOperationsByteRangeInvalid:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"BYTE_RANGE_INVALID", @"ASItemOperations", @"Message for ActiveSync Error - Byte range invalid")]; break;
        case kItemOperationsStoreUnknown:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"STORE_UNKNOWN", @"ASItemOperations", @"Message for ActiveSync Error - Store unknown")]; break;
        case kItemOperationsFileEmpty:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"FILE_EMPTY", @"ASItemOperations", @"Message for ActiveSync Error - File empty")]; break;
        case kItemOperationsRequestedSizeTooLarge:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"REQUESTED_SIZE_TOO_LARGE", @"ASItemOperations", @"Message for ActiveSync Error - Requested size too large")]; break;
        case kItemOperationsInputOutputFailure:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"I_0_FAILURE", @"ASItemOperations", @"Message for ActiveSync Error - I/0 Failure")]; break;
        case kItemOperationsItemFailedConversion:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"ITEM_FAILED_CONVERSION", @"ASItemOperations", @"Message for ActiveSync Error - Item failed conversion")]; break;
        case kItemOperationsAttachmentInvalid:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"ATTACHMENT_INVALID", @"ASItemOperations", @"Message for ActiveSync Error - Attachment invalid")]; break;
        case kItemOperationsAccessDenied:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"ACCESS_DENIED", @"ASItemOperations", @"Message for ActiveSync Error - Access denied")]; break;
        case kItemOperationsPartialSuccess:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"PARTIAL_SUCCESS", @"ASItemOperations", @"Message for ActiveSync Error - Partial success")]; break;
        case kItemOperationsCredentialsRequired:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"CREDENTIALS_REQUIRES", @"ASItemOperations", @"Message for ActiveSync Error - Credentials required")]; break;
        case kItemOperationsProtocolErrorOptionsMissing:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"PROTOCOL_ERROR_OPTIONS_MISSING", @"ASItemOperations", @"Message for ActiveSync Error - Protocol error options missing")]; break;
        case kItemOperationsFolderMustBeIPFNote:
            [self reportActiveSyncError:FXLLocalizedStringFromTable(@"PROTOCOL_FOLDER_MUST_BE_NOTES", @"ASItemOperations", @"Message for ActiveSync Error - Protocol folder must be Notes")]; break;
        default:
            NSString* anErrorMessage = [self handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS];
            if(delegate) {
                [delegate deliverError:anErrorMessage itemOp:self];
            }
            break;
#else
		default:
			 [self reportActiveSyncError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"ITEM_OPERATIONS_ERROR", @"ErrorAlert", @"Error alert message for Item Operations error"), self.statusCodeAS]];
			
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// File
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark File

- (void)_createFile:(NSString*)path data:(NSData*)aData
{
	@try {
		NSFileManager* defaultManager = [FXLSafeZone secureFileManager];
		BOOL isDirectory = FALSE;
		if([defaultManager fileExistsAtPath:path isDirectory:&isDirectory]) {
			if(!isDirectory) {
				NSError* error = NULL;
				[defaultManager removeItemAtPath:path error:&error];
				if(error) {
					FXDebugLog(kFXLogActiveSync, @"export file remove failed: %@", error);
				}
			}
		}
		
		if(!isDirectory) {
			NSDictionary* attributes = nil;
			[defaultManager createFileAtPath:path contents:aData attributes:attributes];
		}else{
			FXDebugLog(kFXLogActiveSync, @"Export file is a directory: %@", path);
		}
	}@catch(NSException* exception){
		FXDebugLog(kFXLogActiveSync, @"createFile exception %@: %@", [exception name], [exception reason]);
	}
	
	return;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)_baseBodyParser:(FastWBXMLParser*)aParser
{
    BOOL wasDelivered = FALSE;
    ASTag tag;
    while ((tag = [aParser nextTag:BASE_BODY]) != AS_END) {
        switch(tag) {
            case BASE_DATA:
            {
                if(!self.isMultipart) {
                    wasDelivered = TRUE;
                    [delegate deliverData:[aParser getData] itemOp:self];
                }else{
                    //FXDebugLog(kFXLogActiveSync, @"BASE_DATA: %@", [aParser getString]);
                }
                break;
            }
            case BASE_TYPE:
            {
                //FXDebugLog(kFXLogActiveSync, @"BASE_TYPE: %d", [aParser getInt]);
                break;
            }
            case BASE_ESTIMATED_DATA_SIZE:
            {
                self.expectedSize = [aParser getIntTraceable];
                break;
            }
            case BASE_TRUNCATED:
            {
                //FXDebugLog(kFXLogActiveSync, @"BASE_TRUNCATED: %d", [aParser getBool]);
                break;
            }
            case ITEM_OPERATIONS_PART:
            {
                //FXDebugLog(kFXLogActiveSync, @"ITEM_OPERATIONS_PART: %@", [aParser getString]);
                break;
            }
            default:
                [aParser skipTag];
                break;
        }
    }
    if(!self.isMultipart) {
        if(!wasDelivered) {
            [delegate deliverData:[NSData data] itemOp:self];
        }
    }
}

- (void)fetchParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType
{
   @try {
        ASEmail* anEmail = (ASEmail*)anObject;

        anEmail.bodyType = aBodyType;
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [anEmail setUid:[aParser getStringTraceable]];
                    break;
                case ITEM_OPERATIONS_STATUS:
                    self.statusCodeAS = (EItemOperationsStatus)[aParser getIntTraceable];
                    if(self.statusCodeAS != kItemOperationsSuccess) {
                        [self displayError];
                    }
                    break;
                case SYNC_COLLECTION_ID:
                    break;
                case SYNC_CLASS:
                    break;
                case BASE_ATTACHMENTS:
                    break;
                case BASE_ATTACHMENT:
                    break;
                case BASE_DISPLAY_NAME:
                    //FXDebugLog(kFXLogActiveSync, @"BASE_DISPLAY_NAME = %@", [aParser getString]);
                    break;
                case BASE_CONTENT_TYPE:
                    //FXDebugLog(kFXLogActiveSync, @"BASE_CONTENT_TYPE = %@", [aParser getString]);
                    break;
                case BASE_METHOD:
                    //FXDebugLog(kFXLogActiveSync, @"BASE_METHOD = %@", [aParser getStringTraceable]);
                    break;
                case BASE_IS_INLINE:
                    //FXDebugLog(kFXLogActiveSync, @"BASE_IS_INLINE = %d", [aParser getBoolTraceable]);
                    break;
                case BASE_BODY:
                    [self _baseBodyParser:aParser];
                    break;
                case BASE_BODY_PREFERENCE:
                    FXDebugLog(kFXLogActiveSync, @"BASE_NATIVE_BODY_TYPE = %d", [aParser getIntTraceable]);
                    break;
                case BASE_ESTIMATED_DATA_SIZE:
                    self.expectedSize = [aParser getIntTraceable];
                    break;
                case BASE_NATIVE_BODY_TYPE:
                    //FXDebugLog(kFXLogActiveSync, @"BASE_NATIVE_BODY_TYPE = %d", [aParser getIntTraceable]);
                    break;
                case ITEM_OPERATIONS_PROPERTIES:
                    break;
                case ITEM_OPERATIONS_PART:
                    FXDebugLog(kFXLogActiveSync, @"ITEM_OPERATIONS_PART = %d", [aParser getIntTraceable]);
                    break;
                case BASE_FILE_REFERENCE:
                    // This is sent by some server when there is an error
                    FXDebugLog(kFXLogActiveSync, @"BASE_FILE_REFERENCE = %@", [aParser getString]);
                    break;
                case EMAIL_MESSAGE_CLASS:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_MESSAGE_CLASS = %@", [aParser getStringTraceable]);
                    break;
                case EMAIL_CONTENT_CLASS:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_CONTENT_CLASS = %@", [aParser getStringTraceable]);
                    break;
                case EMAIL_TO:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_TO = %@", [aParser getString]);
                    break;
                case EMAIL_CC:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_CC = %@", [aParser getString]);
                    break;
                case EMAIL_FROM:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_FROM = %@", [aParser getString]);
                    break;
                case EMAIL_REPLY_TO:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_REPLY_TO = %@", [aParser getString]);
                    break;
                case EMAIL_SUBJECT:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_SUBJECT = %@", [aParser getString]);
                    break;
                case EMAIL_DATE_RECEIVED:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_DATE_RECEIVED = %@", [aParser getStringTraceable]);
                    break;
                case EMAIL_DISPLAY_TO:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_DISPLAY_TO = %@", [aParser getString]);
                    break;
                case EMAIL_IMPORTANCE:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_DISPLAY_TO = %d", [aParser getIntTraceable]);
                    break;
                case EMAIL_THREAD_TOPIC:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_THREAD_TOPIC = %@", [aParser getString]);
                    break;
                case EMAIL_READ:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_READ = %d", [aParser getIntTraceable]);
                    break;
                case EMAIL_INTERNET_CPID:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_INTERNET_CPID = %@", [aParser getString]);
                    break;
                case EMAIL_FLAG:
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL_FLAG = %d", [aParser getIntTraceable]);
                    break;
                case EMAIL2_UM_CONVERSATION_ID:
                    //NSData* aData = [aParser getData];
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL2_UM_CONVERSATION_ID: %d", aData.length);
                    break;
                case EMAIL2_CONVERSATION_INDEX:
                    //NSData* aData = [aParser getData];
                    //FXDebugLog(kFXLogActiveSync, @"EMAIL2_CONVERSATION_INDEX: %d", aData.length);
                    break;
                case EMAIL_MEETING_REQUEST:
                    break;
                case BASE_CONTENT_ID:
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
   }@catch (NSException* e) {
       [HttpRequest logException:@"ItemOperations Fetch" exception:e];
   }
}

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case ITEM_OPERATIONS_STATUS:
                    self.statusCodeAS = (EItemOperationsStatus)[aParser getInt];
                    if(self.statusCodeAS != kItemOperationsSuccess) {
                        [self displayError];
                    }
                    break;
                case ITEM_OPERATIONS_RESPONSE:
                    break;
                case ITEM_OPERATIONS_FETCH:
                    [self fetchParser:aParser tag:ITEM_OPERATIONS_FETCH object:self.email bodyType:[self.account bodyType]];
                    break;
                case BASE_FILE_REFERENCE:
                    FXDebugLog(kFXLogActiveSync, @"BASE_FILE_REFERENCE = %@", [aParser getString]);
                    break;
                case ITEM_OPERATIONS_PROPERTIES:
                    break;
                case BASE_CONTENT_TYPE:
                    FXDebugLog(kFXLogActiveSync, @"BASE_CONTENT_TYPE = %@", [aParser getString]);
                    break;
                case ITEM_OPERATIONS_PART:
                    FXDebugLog(kFXLogActiveSync, @"ITEM_OPERATIONS_PART = %d", [aParser getInt]);
                    break;
                case ITEM_OPERATIONS_DATA:
                {
                    NSData* aData = [aParser getData];
                    FXDebugLog(kFXLogActiveSync, @"ITEM_OPERATIONS_DATA = %d: %@", [aData length], filePath);
                    //
                    // FIXME - This data looks nothing like what is expected
                    // This is not used if multipart is turned on anyway
                    //
                    //const char* aBytes = (const char*)[aData bytes];
                    //for(int i = 0 ; i < 100 ; ++i) {
                    //    printf("%c", aBytes[i]);
                    //}
                    //printf("\n");
                    //
                    if(self.filePath.length > 0) {
                        [self _createFile:filePath data:aData];
                    }
                    
                    [delegate deliverFile:filePath itemOp:self];
                    break;
                }
                default:
                    [aParser skipTag]; break;
            }
        }
        
        if(self.statusCodeAS == kItemOperationsSuccess) {
            [self success];
        }else{
            [self fail];
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"ItemOperations parser" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// HttpRequest Override
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark HttpRequest Override

- (void)doRetry
{
    if(bytesDownloaded > 0) {
#warning FIXME Generate new request with a range to resume download where it left off
        FXDebugLog(kFXLogActiveSync, @"ASItemOperations resume at %d %d: %@", bytesDownloaded, self.retries, self.URLrequest);
        if(self.fileHandle) {
            [self.fileHandle truncateFileAtOffset:0L];
        }else{
            [self.fileData setLength:0];
        }
    }
    [super doRetry];
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d  %@ headers:\n%@", kCommand, statusCode, aContentType, aHeaders);
        if([aContentType isEqualToString:@"application/vnd.ms-sync.multipart"]) {
            self.isMultipart = TRUE;
            if([filePath length] > 0) {
                [self _createFile:filePath data:NULL];
				self.fileHandle = [[FXLSafeZone fileHandleClass] fileHandleForWritingAtPath:filePath];
            }else{
                fileData = [[NSMutableData alloc] initWithCapacity:self.expectedSize];
            }
        }else if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
            self.isMultipart = FALSE;
        }else if([aContentType isEqualToString:@"application/octet-stream"]) {
            self.isOctetStream = TRUE;
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response unknown content type: %@", kCommand, aContentType);
            return;
        }
    }else if(self.statusCode == kHttpNeedsProvisioning
          || self.statusCode == kHttpServiceUnavailable) {
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }else{
        if(delegate) {
            [delegate deliverError:[NSHTTPURLResponse localizedStringForStatusCode:self.statusCode] itemOp:self];
        }else{
            [super handleHttpErrorForAccount:self.account connection:connection response:response];
        }
    }
}

- (void)parseWBXMLData
{
    // Parse the WBXML data if there is any, there should be
    //
    NSUInteger aLength = [data length];
    if(aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag]; 
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed no data: %d", kCommand, self.statusCode);
    }
}

- (void)connection:(HttpConnection*)connection didReceiveData:(NSData *)aData
{
    if(self.isMultipart) {
        //
        @try {
            NSUInteger aLength = [aData length];

            if([data length] == 0) {
                // We don't have the WBXML data yet so get the multipart header
                // Format is described in [MS-ASCMD].pdf in Section 2.2.2.8.1
                //
                
                // Get the number of parts
                //
                NSUInteger anOffset = 0;

                int numMultiparts;
                [aData getBytes:&numMultiparts length:sizeof(int)];
                anOffset += sizeof(int);
                
                if(numMultiparts < 1 || numMultiparts > 2) {
                    FXDebugLog(kFXLogFIXME, @"FIXME ItemOperations multipart only handles 1 or 2 parts");
                }
                
                // Get the part ranges, they are 2 integer tuples which 
                // have the start of the part and its length, the same as an NSRange
                //
                partRanges = [NSMutableArray arrayWithCapacity:numMultiparts];
                NSUInteger aTupleSize = sizeof(int)*2;
                for(int i = 0 ; i < numMultiparts ; ++i) {
                    int tuple[2];
                    NSRange aTupleRange = NSMakeRange(anOffset, aTupleSize);
                    [aData getBytes:tuple range:aTupleRange];
                    //FXDebugLog(kFXLogActiveSync, @"%d numParts: %08x %08x", i, tuple[0], tuple[1]);
                    
                    NSRange aPartRange = NSMakeRange(tuple[0], tuple[1]);
                    [partRanges addObject:[NSValue valueWithRange:aPartRange]];

                    if(i == 0) {
                        wbxmlRange = aPartRange;
                        // Part 1 is the WBXML data and is usually short
                        if((wbxmlRange.location + wbxmlRange.length) <= aLength) {
                            [data appendData:[aData subdataWithRange:aPartRange]];
                        }else{
                            FXDebugLog(kFXLogFIXME, @"FIXME we didn't get all the WBXML data yet");
                        }
                    }else if(i == 1) {
                        // Part 2 is the first part and may be long
                        partRange = aPartRange;
                    }else{
                        // there could be more payloads and they aren't handled by this yet
                    }
                    
                    anOffset += aTupleSize;
                }
                
                // After the WBXML is pulled, write the remaining data to the download file
                //
                if(numMultiparts > 1) {
                    if(partRange.location < aLength) {
                        NSRange aPartRange = partRange;
                        aPartRange.length = aLength - aPartRange.location;
                        if(filePath.length) {
                            [_fileHandle writeData:[aData subdataWithRange:aPartRange]];
                        }else{
                            [fileData appendData:[aData subdataWithRange:aPartRange]];
                        }
                        bytesDownloaded += aPartRange.length;
                        FXDebugLog(kFXLogActiveSync, @"bytesDownloaded1 : %d", bytesDownloaded);
                    }else{
                        FXDebugLog(kFXLogFIXME, @"FIXME we dont have all the WBXML data yet");
                    }
                }
                return;
            }

            if(filePath.length > 0) {
                [_fileHandle writeData:aData];
            }else{
                [fileData appendData:aData];
            }
            bytesDownloaded += aLength;
            FXDebugLog(kFXLogActiveSync, @"bytesDownloaded2 : %d", bytesDownloaded);


            if(delegate) {
                // Report progress, percent complete to the client
                //
                if(expectedSize > 0) {
                    NSUInteger aPercent = (NSUInteger)((float)bytesDownloaded / (float)expectedSize * 100.0f);
                    if(aPercent != percentReported) {
                        percentReported = aPercent;
                        [delegate deliverProgress:[NSString stringWithFormat:@"%3d%%", percentReported] itemOp:self];
                    }
                }else{
                     [delegate deliverProgress:[NSString stringWithFormat:@"%d bytes", bytesDownloaded] itemOp:self];
                }
            }
        }@catch (NSException* e) {
            [HttpRequest logException:@"ItemOperations didReceiveData" exception:e];
        }
    }else if(self.isOctetStream) {
        if(filePath.length > 0) {
            [_fileHandle writeData:[aData decodeFrom8Bit]];
        }else{
            [fileData appendData:[aData decodeFrom8Bit]];
        }
    }else{
        [data appendData:aData];
    }
}

- (void)doDeliver
{
    // Send data/file to client
    //
    if(delegate) {
        // fileData always exists now
        if(self.filePath.length == 0) {
            if(target && selector) {
                SuppressPerformSelectorLeakWarning([target performSelector:selector withObject:self]);
                
            }else{
                [delegate deliverData:self.fileData itemOp:self];
            }
        }else{
            // almost always, unless application type is "application/vnd.ms-sync.wbxml",
            // then the fileHandle should be non-nil, but just in case, handle it
            if (self.fileHandle != nil) {
                [self.fileHandle closeFile];
            } else {
                [self _createFile:self.filePath data:self.fileData];
            }
            [delegate deliverFile:self.filePath itemOp:self];
        }
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
	if(self.statusCode == kHttpOK) {
        if(self.isMultipart) {
            [self parseWBXMLData];
            
            // Did we get the payload and is it the right size
			// *** BUG ***
            // Unfortunately, the "expectedSize" is exactly that, expected, not necessarily the actual size
			// we are going to make the assumption that "expectedSize" will be a maximum, so if the file
			// comes in below the expectedSize, it's ok. We will allow 128 bytes of flexibility
			if ((expectedSize > 0) && (bytesDownloaded > expectedSize)) {
				[delegate deliverError:[NSString stringWithFormat:@"Download exceeded expected size %d of %d bytes", bytesDownloaded, expectedSize] itemOp:self];
			} else if ((expectedSize - bytesDownloaded) > 128) {
                   [delegate deliverError:[NSString stringWithFormat:@"Partial download %d of %d bytes", bytesDownloaded, expectedSize] itemOp:self];
            } else {
                if(filePath.length > 0) {
                    FXDebugLog(kFXLogActiveSync, @"%@ success %d bytes %@", kCommand, bytesDownloaded, filePath);
                }else{
                    FXDebugLog(kFXLogActiveSync, @"%@ success %d bytes", kCommand, bytesDownloaded);
                }
            }
            [self doDeliver];
        }else if(self.isOctetStream) {
            [self doDeliver];
        }else{
            [self parseWBXMLData];
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
    [super connectionDidFinishLoading:connection];
}

@end