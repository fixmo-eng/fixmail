//
//  BaseDBAccessor.m
// ----------------------------------------------------------------------
// Part of the SQLite Persistent Objects for Cocoa and Cocoa Touch
//
// Original Version: (c) 2008 Jeff LaMarche (jeff_Lamarche@mac.com)
// ----------------------------------------------------------------------
// This code may be used without restriction in any software, commercial,
// free, or otherwise. There are no attribution requirements, and no
// requirement that you distribute your changes, although bugfixes and 
// enhancements are welcome.
// 
// If you do choose to re-distribute the source code, you must retain the
// copyright notice and this license information. I also request that you
// place comments in to identify your changes.
//
// For information on how to use these classes, take a look at the 
// included Readme.txt file
// ----------------------------------------------------------------------

#import "BaseDBAccessor.h"
#import "ErrorAlert.h"
#import "StringUtil.h"

#pragma mark Private Method Declarations
@interface BaseDBAccessor (private)
- (void)executeUpdateSQL:(NSString *) updateSQL;
@end

// Constants
static const int kMaxRetries = 20;
static const int kRetryInterval = 1000;

@implementation BaseDBAccessor

// Virtual Override
+ (id)sharedManager
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseDBAccessor sharedManager");
    return nil;
}

+ (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EMAIL_DATABASE", @"ErrorAlert", @"BaseDBAccessor Email Database error alert title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////
// Transaction Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Transaction Interface

+ (BOOL)beginTransaction
{
	const char *sql1 = "BEGIN TRANSACTION"; //Should be exclusive?
	sqlite3_stmt *begin_statement;
	if (sqlite3_prepare_v2([[self sharedManager] database], sql1, -1, &begin_statement, NULL) != SQLITE_OK)
	{
		sqlite3_finalize(begin_statement);
		return NO;
	}
    int result = sqlite3_step(begin_statement);
	if (result != SQLITE_DONE)
	{
        if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"BEGIN_TRANSACTION_FAILED:", @"ErrorAlert", @"Error alert message when begin transaction failed"), sqlite3_errmsg([[self sharedManager] database])]];
        }else{
            [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"BEGIN_TRANSACTION_FAILED:", @"ErrorAlert", @"Error alert message when begin transaction failed"), sqlite3_errmsg([[self sharedManager] database])]];
        }
		sqlite3_finalize(begin_statement);
		return NO;
	}
	sqlite3_finalize(begin_statement);
	return YES;
}


+ (BOOL)endTransaction
{
    int autoCommit = sqlite3_get_autocommit([[self sharedManager] database]);
    if(!autoCommit) {
        const char *sql2 = "COMMIT TRANSACTION";
        sqlite3_stmt *commit_statement;
        if (sqlite3_prepare_v2([[self sharedManager] database], sql2, -1, &commit_statement, NULL) != SQLITE_OK)
        {
            sqlite3_finalize(commit_statement);
            return NO;
        }
        int result = sqlite3_step(commit_statement);

        if (result != SQLITE_DONE)
        {
            if (result == SQLITE_BUSY || result == SQLITE_LOCKED) {
                [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"END_TRANSACTION_FAILED:", @"ErrorAlert", @"Error alert message when end transaction failed"),sqlite3_errmsg([[self sharedManager] database])]];
            }else{
                [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"END_TRANSACTION_FAILED:", @"ErrorAlert", @"Error alert message when end transaction failed"),sqlite3_errmsg([[self sharedManager] database])]];
            }
            sqlite3_finalize(commit_statement);
            return NO;
        }
        sqlite3_finalize(commit_statement);
    }else{
        FXDebugLog(kFXLogActiveSync, @"endTransaction while autocommit is on");
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////
// NSObject
////////////////////////////////////////////////////////////////////////////////
#pragma mark NSObject

- (id)copyWithZone:(NSZone *)zone
{
	return self;
}

////////////////////////////////////////////////////////////////////////////////
// Private
////////////////////////////////////////////////////////////////////////////////
#pragma mark Private

- (void)executeUpdateSQL:(NSString *) updateSQL
{
    char *errorMsg;

    int aRetries = 0;
    while(aRetries < kMaxRetries) {
        int result = sqlite3_exec([self database],[updateSQL UTF8String] , NULL, NULL, &errorMsg);
        if(result == SQLITE_OK) {
            break;
        }else if(result == SQLITE_BUSY || result == SQLITE_LOCKED) {
            FXDebugLog(kFXLogAll, @"executeUpdateSQL database busy or locked, retrying");
            sqlite3_free(errorMsg);
            usleep(kRetryInterval);
            aRetries++;
        }else{
            [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ '%@': %s", FXLLocalizedStringFromTable(@"EXECUTE_SQL_FAILED", @"ErrorAlert", @"Error alert message when execute SQL failed"), updateSQL, errorMsg]];
                    sqlite3_free(errorMsg);
            sqlite3_free(errorMsg);
            break;
        }
    }
    if(aRetries >= kMaxRetries) {
        [BaseDBAccessor handleError:[NSString stringWithFormat:@"%@ '%@': %s", FXLLocalizedStringFromTable(@"EXECUTE_SQL_FAILED", @"ErrorAlert", @"Error alert message when execute SQL failed"), updateSQL, errorMsg]];
        sqlite3_free(errorMsg);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Transaction Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Transaction Interface

- (BOOL)isOpen
{
    return database != nil;
}

- (sqlite3 *)database
{
	static BOOL first = YES;
	
	if (first || database == NULL)
	{
		first = NO;
		if (!sqlite3_open([[self databaseFilepath] UTF8String], &database) == SQLITE_OK) 
		{
			// Even though the open failed, call close to properly clean up resources.
            [BaseDBAccessor handleError:[NSString stringWithFormat: @"%@ %@: %s", FXLLocalizedStringFromTable(@"DATABASE_OPEN_FAILED", @"ErrorAlert", @"Error alert message when database open failed"), [self databaseFilepath], sqlite3_errmsg(database)]];
			sqlite3_close(database);
            database = nil;
		}
		else
		{
            int autoVacuum = [self autoVacuum];
            if(autoVacuum != 1) {
                // Modify cache size so we don't overload memory. 50 * 1.5kb
                [self executeUpdateSQL:@"PRAGMA CACHE_SIZE=500"];
                
                // Default to UTF-8 encoding
                [self executeUpdateSQL:@"PRAGMA encoding = \"UTF-8\""];
                
                // Turn on full auto-vacuuming to keep the size of the database down
                // This setting can be changed per database using the setAutoVacuum instance method
                [self executeUpdateSQL:@"PRAGMA auto_vacuum=1"];
            }
		}
	}
	return database;
}

- (void)close
{
	if(database != NULL) {
		sqlite3_close(database);
		database = NULL;
	}
}

- (int)userVersion
{
    int aVersion = 0;
    
    sqlite3_stmt* aStatement;
    if(sqlite3_prepare_v2([self database], "PRAGMA user_version;", -1, &aStatement, NULL) == SQLITE_OK) {
        if(sqlite3_step(aStatement) == SQLITE_ROW) {
            aVersion = sqlite3_column_int(aStatement, 0);
        }else{
            [BaseDBAccessor handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method failed with specified error message"), @"queryUserVersion", sqlite3_errmsg(database)]];
        }
    } else {
		[BaseDBAccessor handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"queryUserVersion", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(aStatement);
    
    return aVersion;
}

- (void)setUserVersion:(int)aVersion
{
    [self executeUpdateSQL:[NSString stringWithFormat:@"PRAGMA USER_VERSION=%d", aVersion]];
}

- (int)autoVacuum
{
    int anAutoVacuum = 0;
    
    sqlite3_stmt* aStatement;
    if(sqlite3_prepare_v2([self database], "PRAGMA auto_vacuum;", -1, &aStatement, NULL) == SQLITE_OK) {
        if(sqlite3_step(aStatement) == SQLITE_ROW) {
            anAutoVacuum = sqlite3_column_int(aStatement, 0);
        }else{
            [BaseDBAccessor handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method failed with specified error message"), @"queryAutoVacuum", sqlite3_errmsg(database)]];
        }
    } else {
		[BaseDBAccessor handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"%@((method))_PREPARE_FAILED:_%s((errorMessage))", @"ErrorAlert", @"Error alert message when method prepare failed with specified error message"), @"queryAutoVacuum", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(aStatement);
    
    return anAutoVacuum;
}

- (void)setAutoVacuum:(SQLITE3AutoVacuum)mode
{
	NSString *updateSQL = [NSString stringWithFormat:@"PRAGMA auto_vacuum=%d", mode];
	[self executeUpdateSQL:updateSQL];
}

- (void)setCacheSize:(NSUInteger)pages
{
	NSString *updateSQL = [NSString stringWithFormat:@"PRAGMA cache_size=%d", pages];
	[self executeUpdateSQL:updateSQL];
}

- (void)setLockingMode:(SQLITE3LockingMode)mode
{
	NSString *updateSQL = [NSString stringWithFormat:@"PRAGMA cache_size=%d", mode];
	[self executeUpdateSQL:updateSQL];
}

- (void)deleteDatabase
{
	NSString* path = [self databaseFilepath];
	NSFileManager* fm = [NSFileManager defaultManager];
	[fm removeItemAtPath:path error:nil];
	
	database = NULL;
}

- (void)vacuum
{
	[self executeUpdateSQL:@"VACUUM"];
}

// Virtual Override
- (NSString *)databaseFilepath
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseDBAccessor databaseFilepath");
    return nil;
}

@end