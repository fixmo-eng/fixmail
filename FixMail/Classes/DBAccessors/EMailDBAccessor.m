//
//  EmailDBAccessor.m
// ----------------------------------------------------------------------
// Part of the SQLite Persistent Objects for Cocoa and Cocoa Touch
//
// Original Version: (c) 2008 Jeff LaMarche (jeff_Lamarche@mac.com)
// ----------------------------------------------------------------------
// This code may be used without restriction in any software, commercial,
// free, or otherwise. There are no attribution requirements, and no
// requirement that you distribute your changes, although bugfixes and
// enhancements are welcome.
//
// If you do choose to re-distribute the source code, you must retain the
// copyright notice and this license information. I also request that you
// place comments in to identify your changes.
//
// For information on how to use these classes, take a look at the
// included Readme.txt file
// ----------------------------------------------------------------------

#import "EmailDBAccessor.h"

@implementation EmailDBAccessor

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setDatabaseFilepath:(NSString *)aDatabaseFilepath
{
    databaseFilepath = aDatabaseFilepath; 
}

- (NSString *)databaseFilepath
{
	if (databaseFilepath == nil) {
		//assert(FALSE); // You should init CacheManager first, then this won't happen.
		NSMutableString *ret = [NSMutableString string];
		NSString *appName = [[NSProcessInfo processInfo] processName];
		for (int i = 0; i < [appName length]; i++)
		{
			NSRange range = NSMakeRange(i, 1);
			NSString *oneChar = [appName substringWithRange:range];
			if (![oneChar isEqualToString:@" "])
				[ret appendString:[oneChar lowercaseString]];
		}
		
		NSArray* paths          = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString* saveDirectory = [paths objectAtIndex:0];
		NSString* saveFileName  = [NSString stringWithFormat:@"%@.sqlite3", ret];
		NSString* filepath      = [saveDirectory stringByAppendingPathComponent:saveFileName];
		
		databaseFilepath = filepath;
		
		if (![[NSFileManager defaultManager] fileExistsAtPath:saveDirectory])
			[[NSFileManager defaultManager] createDirectoryAtPath:saveDirectory withIntermediateDirectories:YES attributes:nil error:nil];
	}
	return databaseFilepath;
}

@end