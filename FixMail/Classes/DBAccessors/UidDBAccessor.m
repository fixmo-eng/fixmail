/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "UidDBAccessor.h"
#import "StringUtil.h"

@implementation UidDBAccessor

// Constants
static NSString* kDatabaseName = @"uid.tdb";

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static UidDBAccessor* sharedSQLiteManager = nil;

+ (id)sharedManager
{
	@synchronized(self) {
		if (sharedSQLiteManager == nil) {
			sharedSQLiteManager = [[self alloc] init];;
		}
	}
	return sharedSQLiteManager;
}

+ (id)allocWithZone:(NSZone *)zone
{
	@synchronized(self) {
		if (sharedSQLiteManager == nil) {
			sharedSQLiteManager = [super allocWithZone:zone];
		}
	}
	
	return sharedSQLiteManager;
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (NSString *)databaseFilepath
{
	return [StringUtil filePathInDocumentsDirectoryForFileName:kDatabaseName];
}

@end
