//
//  GlobalDBFunctions.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 6/29/09.
//  Copyright 2009 NextMail Corporation. All rights reserved.
//

#import "GlobalDBFunctions.h"
#import "AccountDBAccessor.h"
#import "AppSettings.h"
#import "AttachmentManager.h"
#import "PastQuery.h"
#import "StringUtil.h"
#import "SyncManager.h"

@implementation GlobalDBFunctions

#pragma	mark Add DB management
NSInteger intSortReverse(id num1, id num2, void *context){
    int v1 = [num1 intValue];
    int v2 = [num2 intValue];
    if (v1 < v2)
        return NSOrderedDescending;
    else if (v1 > v2)
        return NSOrderedAscending;
    else
        return NSOrderedSame;
}

+(void)closeAll {
    [BaseAccount clearPreparedStatements];
    [[AccountDBAccessor sharedManager] close];
}

+(void)deleteAll {
	[AttachmentManager deleteAllAttachments];

	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES); 
	NSString *documentsDirectory = [paths objectAtIndex: 0]; 
	
	NSString* fileName;
	NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:documentsDirectory];
	while (fileName = [dirEnum nextObject]) {
		NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:fileName];
		[[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
	}
}

+(unsigned long long)freeSpaceOnDisk {
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex: 0];

	NSDictionary *fsAttributes = [[NSFileManager defaultManager] attributesOfFileSystemForPath:documentsDirectory error:nil];
	if(fsAttributes != nil) {
		return [[fsAttributes objectForKey:NSFileSystemFreeSize] unsignedLongLongValue];
	}
	
	return 0;
}

+(BOOL)enoughFreeSpaceForSync {
	// returns NO if not enough free space is available on disk to start a new sync
	float freeSpace = [GlobalDBFunctions freeSpaceOnDisk] / 1024.0f / 1024.0f;
	
	return (freeSpace > 4.0f); // at least 4 MB must be available
}

+(unsigned long long)totalAttachmentsFileSize {
	
	NSString *attDirectory = [FXLSafeZone attachmentsDirectory];
	
	NSString* file;
	NSDirectoryEnumerator *dirEnum = [[NSFileManager defaultManager] enumeratorAtPath:attDirectory];

	unsigned long long total = 0;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	while (file = [dirEnum nextObject]) {
		NSString *filePath = [attDirectory stringByAppendingPathComponent:file];
		NSDictionary *fileAttributes = [fileManager attributesOfItemAtPath:filePath error:nil];
		
		if (fileAttributes != nil) {
			NSNumber *fileSize;
			if ((fileSize = [fileAttributes objectForKey:NSFileSize])) {
				total += [fileSize unsignedLongValue];
			}
		}
	}
	
	return total;
}

+(unsigned long long)sizeForPath:(NSString*)aPath
{
    unsigned long long aSize = 0;
    
    if(aPath != nil) {
        NSFileManager* fileManager = [NSFileManager defaultManager];
		NSDictionary* fileAttributes = [fileManager attributesOfItemAtPath:aPath error:nil];
		if (fileAttributes != nil) {
			NSNumber* fileSize;
			if ((fileSize = [fileAttributes objectForKey:NSFileSize])) {
                aSize = [fileSize unsignedLongValue];
            }
        }
    }
    
    return aSize;
}

+(unsigned long long)totalFileSize {
	
	unsigned long long total = 0;

    // AccountDB 
	NSString *dbPath = [[AccountDBAccessor sharedManager] databaseFilepath];
    total += [GlobalDBFunctions sizeForPath:dbPath];
        
	return total;
}

+ (void)tableCheck {
	// NOTE: need to change dbGlobalTableVersion every time we change the schema
	if([AppSettings globalDBVersion] < 1) {
        [BaseAccount tableCheck];
		[PastQuery tableCheck];

		[AppSettings setGlobalDBVersion:1];
	}	
}
@end
