/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASOutOfOffice.h"

@implementation ASOutOfOffice


////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString string];
	[string appendString:@"{\n"];
    
    switch(self.type) {
        case kOutOfOfficeInternal:
            [self writeString:string		tag:@"type"      value:@"internal"];
            break;
        case kOutOfOfficeExternalKnown:
            [self writeString:string		tag:@"type"      value:@"externalKnown"];
            break;
        case kOutOfOfficeExternalUnknown:
            [self writeString:string		tag:@"type"      value:@"externalUnknown"];
            break;
    }
    [self writeBoolean:string       tag:@"enabled"      value:self.enabled];
    if(self.enabled) {
        [self writeString:string		tag:@"message"      value:self.message];
        [self writeString:string		tag:@"bodyType"     value:self.bodyType];
    }

	[string appendString:@"}\n"];
	
	return string;
}

@end
