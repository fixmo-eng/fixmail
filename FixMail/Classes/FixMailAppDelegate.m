//  NextMailAppDelegate.m
//  NextMail iPhone Application
//
//  Created by Gabor Cselle on 1/16/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "FixMailAppDelegate.h"
#import "MailboxViewController.h"
#import "ActivityIndicator.h"
#import "AppSettings.h"
#import "ASAccount.h"
#import "DBaseEngine.h"
#import "EmailProcessor.h"
#import "ErrorAlert.h"
#import "GlobalDBFunctions.h"
#import "MemUtil.h"
#import "PlaceholderSplitviewDetailViewController.h"
#import "SZLConcreteApplicationContainer.h"
#import "TraceEngine.h"
#import "TraceViewController.h"
#import "MGSplitViewController.h"
#import "UIViewController+MGSplitViewController.h"
#import "ASConfigViewController.h"

@interface  FixMailAppDelegate ()
{
	UIViewController* _rootViewController;
}
@end


@implementation FixMailAppDelegate

// Globals
BOOL gIsFixTrace = FALSE;

// Properties
@synthesize window;
@synthesize pushSetupScreen;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"APP", @"ErrorAlert", @"Error Alert Title for FixMailAppDelegate error") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"APP", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Misc
////////////////////////////////////////////////////////////////////////////////
#pragma mark Misc

////////////////////////////////////////////////////////////////////////////////
// UIApplicationDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIApplicationDelegate

- (void)defaultsChangedShouldResync:(BOOL)aShouldResync
{
	NSArray* anAccounts = [BaseAccount accounts];
	for(BaseAccount* aBaseAccount in anAccounts) {
		if([aBaseAccount isKindOfClass:[ASAccount class]]) {
			ASAccount* anAccount = (ASAccount*)aBaseAccount;
			[anAccount setActiveSyncPreferences:aShouldResync];
		}
	}
}



////////////////////////////////////////////////////////////////////////////////
// SZLApplicationContainerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark - SZLApplicationContainerDelegate

//
// Have to accomplish the following:
// 1) if firstSync, display chooseVC
//		1a) if successful, then display desktop
//		1b) if unsuccessful, stay on chooseVC
// 2) not firstSync, showDesktop
//
-(UIViewController*)rootViewController
{
	if (_rootViewController) {
		return _rootViewController;
	}
	
	@try {
		[NSThread setThreadPriority:1.0];
		
		// handle reset and clearing attachments
		// (the user can reset all data in iPhone > Settings)
		//if([AppSettings reset]) {
		//	[BaseAccount deleteAllAccounts];
		//}
		
		UINavigationController* navController = nil;
		[self defaultsChangedShouldResync:TRUE];
		
		// Load existing email accounts and folders
		//
		if([BaseAccount accounts].count == 0) {
			[ASAccount loadExistingEmailAccounts];
		}
		
        MailboxViewController *lastMailbox = nil;
		//if (gIsFixTrace) {
		//	NSArray* anAccounts = [BaseAccount accounts];
		//	if(anAccounts.count > 0) {
		//		TraceViewController* aTraceViewController = [[TraceViewController alloc] initWithNibName:@"TraceView" bundle:[FXLSafeZone getResourceBundle]];
		//		[aTraceViewController setAccount:(ASAccount*)[anAccounts objectAtIndex:0]];
		//
		//		navController = [[UINavigationController alloc] initWithRootViewController:aTraceViewController];
		//		navController.navigationBarHidden = NO;
		//	}
		//} else {
			lastMailbox = [self getLastMailbox];
			navController = [[UINavigationController alloc] initWithRootViewController:lastMailbox];
		//}

		if (IS_IPAD()) {
			PlaceholderSplitviewDetailViewController* detail = [[PlaceholderSplitviewDetailViewController alloc] initWithNibName:@"PlaceholderSplitviewDetailView" bundle:[FXLSafeZone getResourceBundle]];
            detail.composeDelegate = lastMailbox;
			self.splitViewController = [[MGSplitViewController alloc] initWithNibName:nil bundle:nil];
            [self.splitViewController setup];
            lastMailbox.mgSplitViewController = self.splitViewController;
			self.svDelegate = [[FMSplitViewDelegate alloc]init];
			self.svDelegate.splitViewController = self.splitViewController;
			[self.svDelegate setMasterViewController:navController];
			[self.svDelegate setDetailViewController:detail];
			self.splitViewController.delegate = self.svDelegate;
            
			detail.navigationController.navigationBar.barStyle = UIBarStyleBlack;
            
			_rootViewController = self.splitViewController;
		} else {
			_rootViewController = navController;
            navController.topViewController.navigationItem.leftBarButtonItem = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];
		}
		
	}@catch (NSException* e) {
		[self _logException:@"didFinishLaunchingWithOptions" exception:e];
	} 

	return _rootViewController;
}

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary*)options
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	self.window.rootViewController = [self rootViewController];

	[window makeKeyAndVisible];
	
	return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - applicationWillTerminate", [MemUtil freeMemoryAsMB]);

	@try {
        [DBaseEngine shutDown];
        [GlobalDBFunctions closeAll];

		// write unwritten changes to user defaults to disk
		[NSUserDefaults resetStandardUserDefaults];
    
		if(gIsFixTrace) {
			[[TraceEngine engine] terminate];
		}
	}@catch (NSException* e) {
		[self _logException:@"applicationWillTerminate" exception:e];
	} 
	FXDebugLog(kFXLogActiveSync, @"applicationWillTerminate complete Free Memory: %d MB", [MemUtil freeMemoryAsMB]);
}

- (void)applicationWillResignActive:(UIApplication *)application 
{
	FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - applicationWillResignActive", [MemUtil freeMemoryAsMB]);

    if (self.splitViewController)
    {
        FMSplitViewDelegate* svd = (FMSplitViewDelegate*)self.splitViewController.delegate;
        [svd.navigationPopoverController dismissPopoverAnimated:NO];
    }
	
    self.wasBackgrounded = YES;
    [ASAccount removePings];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - applicationDidBecomeActive", [MemUtil freeMemoryAsMB]);

	// Check settings in event users was in the Settings app changing them
	BOOL shouldResync = [BaseAccount numAccounts] > 0;
	[self defaultsChangedShouldResync:shouldResync];
	
	if ([BaseAccount accountsConfigured] < 1) {
        // If no accounts go to the account configuration view
		UINavigationController *configurationController = [ASConfigViewController accountConfigurationControllerWithDelegate:self];
		[self.rootViewController presentViewController:configurationController animated:YES completion:nil];
	}else{
        // Restart the ping if we were backgrounded
        //
        if(self.wasBackgrounded) {
            self.wasBackgrounded = NO;
            // Sync folders and start ping
            BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
            ASAccount* anASAccount = (ASAccount*)currentAccount;
            [anASAccount folderSync];
        }
    }
}

+(NSString*)applicationIcon
{
	return @"FixMailRsrc.bundle/SafeGuard_Email_Icon";
}

+(NSString*)applicationName
{
	return FXLLocalizedString(@"FixMail", @"FixMail app name displayed to the user");
}

-(BOOL)containedApplication:(id <SZLApplicationContainer>)applicationContainer didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[self applicationDidBecomeActive:nil];
	return YES;
}

-(void)containedApplicationWillTerminate:(id <SZLApplicationContainer>)applicationContainer
{
	[self applicationWillResignActive:nil];
}

////////////////////////////////////////////////////////////////////////////////
// SZLApplicationContainerDelegate - optional
////////////////////////////////////////////////////////////////////////////////
#pragma mark - SZLApplicationContainerDelegate - optional

-(void)containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer
{
    //[self applicationDidBecomeActive:nil];
}

-(void)containedApplicationWillResignActive:(id <SZLApplicationContainer>)applicationContainer
{
    //[self applicationWillResignActive:nil];
}

-(void)containedApplicationDidEnterBackground:(id <SZLApplicationContainer>)applicationContainer
{
    [self applicationWillResignActive:nil];
}

-(void)containedApplicationWillEnterForeground:(id <SZLApplicationContainer>)applicationContainer
{
    [self applicationDidBecomeActive:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Private methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Private methods

- (MailboxViewController *) getLastMailbox
{
    NSString* aLastPos = [AppSettings lastpos];
    FXDebugLog(kFXLogActiveSync, @"lastPos: %@", aLastPos);

    BaseFolder* aFolder = nil;
	BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
	if (currentAccount != nil) {
		if(aLastPos.length != 0) {
			aFolder = [currentAccount folderForServerId:aLastPos];
		}
	}

	// if we can't find anything, default to the inbox
	if (aFolder == nil) {
		NSArray *folders = [currentAccount foldersForFolderType:kFolderTypeInbox];
		if (folders.count > 0) {
			aFolder = (ASFolder *)folders[0];
		} else {
			aFolder = [currentAccount folderForNum:0];
		}
	}

	// if we STILL don't have this, give up
	MailboxViewController* aMailboxViewController = nil;
	if (aFolder == nil) {
        FXDebugLog(kFXLogActiveSync, @"setCurrentFolder invalid");
	} else {
		aMailboxViewController = [MailboxViewController controller];
		if (aMailboxViewController) {
			aMailboxViewController.title      = aFolder.displayName;
			aMailboxViewController.mgSplitViewController = self.splitViewController;
			aMailboxViewController.folder     = aFolder;
		}
    }

	return aMailboxViewController;
}


#pragma mark - SZLConfigurationDelegate Methods

-(void)configurationDidFail:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationWasCancelled:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationDidSucceed:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

@end






