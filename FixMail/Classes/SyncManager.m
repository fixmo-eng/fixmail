//
//  SyncManager.m
//  Remail iPhone
//
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "SyncManager.h"
#import "ASAccount.h"
#import "ErrorAlert.h"
#import "StringUtil.h"

// defines
#define SYNC_STATE_FILE_NAME_TEMPLATE   @"sync_state_%i.plist"
#define FOLDER_STATES_KEY               @"folderStates"

@implementation SyncManager

// Properties
@synthesize progressDelegate;
@synthesize progressNumbersDelegate;
@synthesize clientMessageDelegate;
@synthesize clientMessageWasError;
@synthesize theNewEmailDelegate;

@synthesize progressString;

@synthesize syncStates;
@synthesize syncInProgress;

@synthesize okContentTypes;
@synthesize extensionContentType;

@synthesize lastErrorAccountNum;
@synthesize lastErrorFolderNum;
@synthesize lastErrorStartSeq;
@synthesize skipMessageAccountNum;
@synthesize skipMessageFolderNum;
@synthesize skipMessageStartSeq;
@synthesize lastAbort;


////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static SyncManager *singleton = nil;

+ (id)getSingleton 
{ 
    // NOTE: don't get an instance of SyncManager until account settings are set!
    //
	@synchronized(self) {
		if (singleton == nil) {
			singleton = [[self alloc] init];
		}
	}
	return singleton;
}

+ (NSDictionary*)loadFolderState:(int)accountNum
{
    NSMutableDictionary* aFolderStates;

    NSString *filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, accountNum]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSData *fileData = [[NSData alloc] initWithContentsOfFile:filePath];
        aFolderStates = [NSPropertyListSerialization propertyListFromData:fileData mutabilityOption:NSPropertyListMutableContainersAndLeaves format:nil errorDescription:nil];
                
    } else { 
        aFolderStates = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"2", @"__version", [NSMutableArray array], FOLDER_STATES_KEY, nil];
    } 

    return aFolderStates;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init 
{
	if (self = [super init]) {
		self.skipMessageAccountNum = -1;
		self.skipMessageFolderNum = -1;
		self.skipMessageStartSeq = -1;
		self.lastErrorAccountNum = -1;
		self.lastErrorFolderNum = -1;
		self.lastErrorStartSeq = -1;
		self.lastAbort = [NSDate distantPast];
		self.clientMessageWasError = NO;
		
		//Get the persistent state
		self.syncStates = [NSMutableArray arrayWithCapacity:2];
        int numAccounts = [BaseAccount accountsConfigured];
		for(int i = 0; i < numAccounts; i++) {
            [self.syncStates addObject:[SyncManager loadFolderState:i]];
		}
	}
	
	return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [SyncManager handleASError:[NSString stringWithFormat:@"%@ %@ %@: %@", FXLLocalizedStringFromTable(@"EXCEPTION:", @"ErrorAlert", @"Error alert view message when exception occurred"), where, [e name], [e reason]]];
}

+ (void)handleASError:(NSString*)anErrorMessage
{
	
	FXDebugLog(kFXLogActiveSync, FXLLocalizedStringFromTable(@"SYNCMANAGER", @"ErrorAlert", @"SyncManager error alert view title"));
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"SYNCMANAGER", @"ErrorAlert", @"SyncManager error alert view title")
                                                          message:anErrorMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_ERROR", @"ErrorAlert", @"Error alert view cancel button")
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////
// Folder State
////////////////////////////////////////////////////////////////////////////////
#pragma	mark Folder State

- (int)folderCount:(int)accountNum 
{
    int aFolderCount = 0;
    
    if(accountNum < [self.syncStates count]) {
        NSArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
        aFolderCount = [folderStates count];
    }else{
        [SyncManager handleASError:@"Account does not exist or is not open"];
    }
    
    return aFolderCount;
}

- (NSMutableDictionary*)retrieveState:(int)folderNum accountNum:(int)accountNum 
{
    NSMutableDictionary* aFolderState = nil;
    
    if(accountNum < [self.syncStates count]) {
        @try {
            NSArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
            if (folderNum < [folderStates count]) {
                aFolderState = [folderStates objectAtIndex:folderNum];
            }else{
                FXDebugLog(kFXLogActiveSync, @"retrieveState folder is new or invalid: %d %d", accountNum, folderNum);
            }
        }@catch (NSException* e) {
            [self logException:@"retrieveState" exception:e];
        }
    }else{
        [SyncManager handleASError:@"Account does not exist or is not open to retrieve folder"];
    }

	return aFolderState;
}

- (void)addAccountState 
{
    @try {
        int numAccounts = [BaseAccount accountsConfigured];
        
        NSMutableDictionary* props = [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"2", @"__version", [NSMutableArray array], FOLDER_STATES_KEY, nil];
        [self.syncStates addObject:props];
        
        NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, numAccounts+1]];
        if(![[self.syncStates objectAtIndex:numAccounts] writeToFile:filePath atomically:YES]) {
            FXDebugLog(kFXLogActiveSync, @"Unsuccessful in persisting state to file %@", filePath);
        }
    }@catch (NSException* e) {
        [self logException:@"addAccountState" exception:e];
    }
}

- (void)addFolderState:(NSMutableDictionary *)data accountNum:(int)accountNum 
{
    @try {
        if(accountNum < [self.syncStates count]) {
            NSMutableArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
            
            [folderStates addObject:data];
            
            NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, accountNum]];
            if(![[self.syncStates objectAtIndex:accountNum] writeToFile:filePath atomically:YES]) {
                FXDebugLog(kFXLogActiveSync, @"Unsuccessful in persisting state to file %@", filePath);
            }
        }else{
            [SyncManager handleASError:@"Account does not exist or is not open to add folder"];
        }
    }@catch (NSException* e) {
        [self logException:@"addFolderState" exception:e];
    }
}

- (BOOL)isFolderDeleted:(int)folderNum accountNum:(int)accountNum
{
    BOOL aFolderIsDeleted = YES;
    
    if(accountNum < [self.syncStates count]) {
        @try {
            NSArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
            if (folderNum < [folderStates count]) {
                NSNumber* y =  [[folderStates objectAtIndex:folderNum] objectForKey:@"deleted"];
                aFolderIsDeleted = (y == nil) || [y boolValue];
            }
        }@catch (NSException* e) {
            [self logException:@"isFolderDeleted" exception:e];
        }
    }else{
        [SyncManager handleASError:@"Account does not exist or is not open"];
    }

    return aFolderIsDeleted;
}

- (void)markFolderDeleted:(int)folderNum accountNum:(int)accountNum 
{
    if(accountNum < [self.syncStates count]) {
        @try {
            NSMutableArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
            
            NSMutableDictionary* y = [folderStates objectAtIndex:folderNum];
            [y setValue:[NSNumber numberWithBool:YES] forKey:@"deleted"];
            
            NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, accountNum]];
            if(![[self.syncStates objectAtIndex:accountNum] writeToFile:filePath atomically:YES]) {
                FXDebugLog(kFXLogActiveSync, @"Unsuccessful in persisting state to file %@", filePath);
            }
        }@catch (NSException* e) {
            [self logException:@"markFolderDeleted" exception:e];
        }
    }else{
        [SyncManager handleASError:@"Account does not exist or is not open for folder delete"];
    }
}

- (void)deleteAllFolders:(int)accountNum
{
    @try {
        [self.syncStates removeAllObjects];
        NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, accountNum]];
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }@catch (NSException* e) {
        [self logException:@"deleteAllFolders" exception:e];
    }
}

- (void)persistState:(NSMutableDictionary *)data forFolderNum:(int)folderNum accountNum:(int)accountNum 
{
    @try {
        if(accountNum < self.syncStates.count) {
            NSMutableArray* folderStates = [[self.syncStates objectAtIndex:accountNum] objectForKey:FOLDER_STATES_KEY];
            NSUInteger aCount = folderStates.count;
            if(folderNum < folderStates.count) {
                [folderStates replaceObjectAtIndex:folderNum withObject:data];
                NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:[NSString stringWithFormat:SYNC_STATE_FILE_NAME_TEMPLATE, accountNum]];
                if(![[self.syncStates objectAtIndex:accountNum] writeToFile:filePath atomically:YES]) {
                    FXDebugLog(kFXLogActiveSync, @"Unsuccessful in persisting state to file %@", filePath);
                }
            }else{
                [SyncManager handleASError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"FOLDER_NOT_IN_CURRENT_STATE,_FOLDERNUM:_%d,_FOLDER_COUNT:_%d", @"ErrorAlert", @"Error when folder not in current state specifying folderNum and folder count"), folderNum, aCount]];
            }
        }else{
            [SyncManager handleASError:@"Account does not exist or is not open to save folder"];
        }
    }@catch (NSException* e) {
        [self logException:@"persistState" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
//  Progress reporting
////////////////////////////////////////////////////////////////////////////////
#pragma mark Progress reporting

- (void)reportProgressString:(NSString*)progress 
{
    self.progressString = progress;
	if(self.progressDelegate != nil && [self.progressDelegate respondsToSelector:@selector(didChangeProgressStringTo:)]) {
		[self.progressDelegate performSelectorOnMainThread:@selector(didChangeProgressStringTo:) withObject:progress waitUntilDone:NO];
	}
}

- (void)reportProgress:(float)progress withMessage:(NSString*)message 
{
	NSDictionary* progressDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:progress], @"progress", message, @"message", nil];
	
	if(self.progressDelegate != nil && [self.progressDelegate respondsToSelector:@selector(didChangeProgressTo:)]) {
		[self.progressDelegate performSelectorOnMainThread:@selector(didChangeProgressTo:) withObject:progressDict waitUntilDone:NO];
	}
}

- (void)reportProgressNumbers:(int)total synced:(int)synced folderNum:(int)folderNum accountNum:(int)accountNum 
{
	NSDictionary* progressDict = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:total], @"total", [NSNumber numberWithInt:synced], 
								  @"synced", [NSNumber numberWithInt:folderNum], @"folderNum", [NSNumber numberWithInt:accountNum], @"accountNum", nil];
	
	if(self.progressNumbersDelegate != nil && [self.progressNumbersDelegate respondsToSelector:@selector(didChangeProgressNumbersTo:)]) {
		[self.progressNumbersDelegate performSelectorOnMainThread:@selector(didChangeProgressNumbersTo:) withObject:progressDict waitUntilDone:NO];
	}
}

- (void)registerForProgressNumbersWithDelegate:(id) delegate 
{
	//assert([delegate respondsToSelector:@selector(didChangeProgressNumbersTo:)]); // can be reset to zero
	self.progressNumbersDelegate = delegate;
}

- (void)registerForProgressWithDelegate:(NSObject<ProgressDelegate>*)aDelegate
{
	self.progressDelegate = aDelegate;
    if(self.progressString.length > 0) {
        [self reportProgressString:self.progressString];
    }
}

- (void)removeProgressDelegate:(NSObject<ProgressDelegate>*)aDelegate
{
    if(self.progressDelegate == aDelegate) {
        self.progressDelegate = nil;
    }
}

@end
