/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASCalendarFolder.h"
#import "ASAttachment.h"
#import "ASItemOperations.h"
#import "AppSettings.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "CGICalendar.h"
#import "CommitObjects.h"
#import "MeetingRequest.h"
#import "NSDate+TKCategory.h"
#import "NSString+SBJSON.h"
#import "EmailProcessor.h"
#import "Event.h"
#import "EventEngine.h"
#import "EventRecurrence.h"
#import "TraceEngine.h"
#import "FMSplitViewDelegate.h"

@implementation ASCalendarFolder

// Properties
@synthesize alarmEvents;

// Static
//NSUInteger sNumEvents = 0;

static UISplitViewController* sSplitViewController;


+ (UISplitViewController*)splitViewController
{
    return sSplitViewController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount;
{
    if (self = [super initWithAccount:(BaseAccount*)anAccount]) {
    }
    
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (Event*)eventForUid:(NSString*)aUid
{
    Event* anEvent = [dictionary objectForKey:aUid];
    if(anEvent) {
    }else{
        anEvent = [[EventEngine engine] eventForUid:aUid folder:self];
    }
    return anEvent;
}

- (Event*)eventForEventUid:(NSString*)anEventUid
{
    Event* anEvent = nil;
    @try {
        // Events not current stored in dictionary so this doesn't do anything
        for(NSString* aKey in dictionary) {
            Event* aTmpEvent =  [dictionary objectForKey:aKey];
            if([aTmpEvent.eventUid isEqualToString:anEventUid]) {
                anEvent = aTmpEvent;
                break;
            }
        }
        if(anEvent) {
            FXDebugLog(kFXLogCalendar, @"eventForEventUid: %@", anEvent);
        }else{
            anEvent = [[EventEngine engine] eventForEventUid:anEventUid folder:self];
        }
    }@catch (NSException* e) {
        [self logException:@"eventForEventUid" exception:e];
    }
    return anEvent;
}


////////////////////////////////////////////////////////////////////////////////
// Change Handlers
////////////////////////////////////////////////////////////////////////////////
#pragma mark Change Handlers

- (void)_queueDatabaseSelector:(SEL)aSelector object:(NSObject*)anObject
{
    EventEngine* anEventEngine = [EventEngine engine];
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:anEventEngine selector:aSelector object:anObject];
    [anEventEngine.operationQueue addOperation:anOp];
}

- (void)changeObject:(BaseObject*)aBaseObject
          changeType:(EChange)aChangeType
        updateServer:(BOOL)updateServer
      updateDatabase:(BOOL)updateDatabase
{
    @try {
        Event* anEvent = (Event*)aBaseObject;
        
        // Do housekeeping on changed events, update sequence for iCalendar invites, update recurrence, etc.
        //
        [anEvent changed];
        
        // Notify clients of change
        //
        //FXDebugLog(kFXLogActiveSync, @"folder %@ changeObject delegates=%d", self.displayName, delegates.count);
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(changeObject:) withObject:anEvent waitUntilDone:FALSE];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kFolderChangeObjectNotification
                                                            object:self
                                                          userInfo:[NSDictionary dictionaryWithObject:anEvent
                                                                                               forKey:kFolderObject]];
        
        switch(aChangeType) {
            default:
                if(updateDatabase) {
                    [self _queueDatabaseSelector:@selector(storeEvent:) object:anEvent];
                }
                if(updateServer) {
                    // Event changes are currently synced from EventEditController since they are complicated
                }
                break;
        }
    }@catch (NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
}

- (void)deleteUid:(NSString*)aUid updateServer:(BOOL)updateServer
{
    @try {
        //if([[TraceEngine engine] traceType] == kTraceTypeMinimal) {
        //    sNumEvents--;
        //    FXDebugLog(kFXLogTrace, @"%d delete event %@", sNumEvents, aUid);
        //}
        
        // Notify clients of deletion
        for(NSObject<FolderDelegate>* delegate in delegates) {
            [delegate performSelectorOnMainThread:@selector(deleteUid:) withObject:aUid waitUntilDone:FALSE];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kFolderDeleteUidNotification
                                                            object:self
                                                          userInfo:[NSDictionary dictionaryWithObject:aUid
                                                                                               forKey:kFolderObjectUid]];
        
        if(!objectsDeleted) {
            objectsDeleted = [[NSMutableArray alloc] init];
        }
        [objectsDeleted addObject:aUid];
        
        if(updateServer) {
           [ASSync queueDeleteUid:aUid folder:self];
        }
    }@catch (NSException* e) {
        [self logException:@"deleteUid" exception:e];
    }
}

- (void)deleteDatabaseForFolder
{
    [[EventEngine engine] deleteDatabaseForFolderUid:self.uid];
}

////////////////////////////////////////////////////////////////////////////////
// ASFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolder Overrides

- (void)resyncFolder
{
    [[EventEngine engine] deleteDatabaseForFolderUid:self.uid];
    
    [super resyncFolder];
}

- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject  bodyType:(EBodyType)aBodyType
{
    @try {
        Event* anEvent;
        if(anObject) {
            anEvent = (Event*)anObject;
        }else{
            anEvent = [[Event alloc] initWithFolder:self]; 
        }
        
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [anEvent setUid:[aParser getStringTraceable]];
                    break;
                case SYNC_APPLICATION_DATA:
                    [anEvent parser:aParser tag:tag];
                    break;
                case SYNC_STATUS:
                    [ASFolder displaySyncStatus:(ESyncStatus)[aParser getIntTraceable]];
                    break;
                case SYNC_CLIENT_ID:
                    FXDebugLog(kFXLogActiveSync, @"SYNC_CLIENT_ID: %@", [aParser getString]);
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        //if([[TraceEngine engine] traceType] == kTraceTypeMinimal) {
        //    FXDebugLog(kFXLogTrace, @"%d event %@\n\t%@ %@", sNumEvents, anEvent.uid, anEvent.startDate, anEvent.endDate);
        //    sNumEvents++;
        //}
        if(anEvent.uid.length > 0) {
            [anEvent changed];
            [self addObject:anEvent];
        }else{
            FXDebugLog(kFXLogActiveSync, @"ActiveSync rejected event add: %@", anEvent);
        }

    }@catch (NSException* e) {
        [self logException:@"Calendar Add" exception:e];
    }
}

- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                    [self deleteUid:[aParser getStringTraceable] updateServer:FALSE];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Event Delete" exception:e];
    }
}

- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    @try {
        if(!self.objectsChanged) {
            self.objectsChanged = [[NSMutableArray alloc] initWithCapacity:20];
        }
        
        Event* anEvent = nil;
        ASTag tag;
        while ((tag = [aParser nextTag:aTag]) != AS_END) {
            switch(tag) {
                case SYNC_SERVER_ID:
                {
                    NSString* aUid = [aParser getStringTraceable];
                    anEvent = [self eventForUid:aUid];
                    if(anEvent) {
                        [self.objectsChanged addObject:anEvent]; 
                    }else{
                        FXDebugLog(kFXLogFIXME, @"FIXME changeParser event not found: %@", aUid);
                    }
                    break;
                }
                case SYNC_APPLICATION_DATA:
                    if(anEvent) {
                        [anEvent parser:aParser tag:tag]; 
                    }else{
                        FXDebugLog(kFXLogActiveSync, @"ASCalendarFolder changeParser failed");
                    }
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"Event Change" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Load/Save
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Save

- (void)loadFromStore:(NSDictionary *)aDictionary
{
    @try {
        [super loadFromStore:aDictionary];

    }@catch (NSException* e) {
        [self logException:@"loadFromStore" exception:e];
    }
}

- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary
{
    [super folderAsDictionary:aDictionary];
}

////////////////////////////////////////////////////////////////////////////////
// BaseFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark BaseFolder Overrides


- (DBaseEngine*)databaseEngine
{
    return [EventEngine engine];
}

- (void)commitComplete:(CommitObjects*)aCommitObjects
{
    [super folderCommitCompleted];
}

- (void)commitObjects
{
    if(self.objectsAdded.count > 0 || self.objectsChanged.count > 0 || self.objectsDeleted.count > 0) {
        @try {
            CommitObjects* aCommitObjects = [[CommitObjects alloc] initWithFolder:self];
            
            // Objects added
            //
            if([self.objectsAdded count] > 0) {
                NSArray* anObjects = aCommitObjects.objectsAdded = self.objectsAdded;
                self.objectsAdded = [[NSMutableArray alloc] initWithCapacity:30]; // FIXME capacity should match sync window size?
                
                // Store new objects on the folder
                //
                for(Event* anEvent in anObjects) {
                    if(anEvent.uid.length > 0) {
                        [dictionary setObject:anEvent forKey:anEvent.uid];
                    }else{
                        FXDebugLog(kFXLogActiveSync, @"commitObjects event invalid: %@", anEvent);
                    }
                }
                
                // And send them to delegate, the calendar view typically
                //
                if(self.isInitialSynced) {
                    for(NSObject<FolderDelegate>* delegate in delegates) {
                        [delegate newObjects:anObjects];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:kFolderNewObjectsNotification
                                                                        object:self
                                                                      userInfo:[NSDictionary dictionaryWithObject:anObjects
                                                                                                           forKey:kFolderObjects]];
                }else if(self.isInitialSync) {
                    [super commitWithCallback:aCommitObjects action:@selector(commitComplete:)];
                }
            }
            
            // Objects changed
            //
            if(self.objectsChanged.count > 0) {
               aCommitObjects.objectsChanged = self.objectsChanged;
                self.objectsChanged = nil;
            }
            
            // Objects deleted
            if(self.objectsDeleted.count > 0) {
                aCommitObjects.objectsDeleted = self.objectsDeleted;
                self.objectsDeleted = nil;
            }
            
            // Queue to database engine
            //
            [[self databaseEngine] queueCommitObjects:aCommitObjects];
        }@catch (NSException* e) {
            [self logException:@"commitObjects" exception:e];
        }
    }
}

- (void)commitChanges:(NSArray*)anObjects
{
    for(Event* anEvent in anObjects) {
        [self changeObject:anEvent changeType:kChangeEvent updateServer:FALSE updateDatabase:TRUE];
    }
}

- (void)setup
{
    [EventEngine tableCheck];
}

- (void)_loadReminders
{
    EventEngine* anEventEngine = [EventEngine engine];
    
    NSDate* aStartDate = [NSDate date];
    NSDate* anEndDate  = [aStartDate dateByAddingDays:2];
    NSMutableArray* anEvents         = [anEventEngine eventsForTimeRange:aStartDate endDate:anEndDate folder:self];
    NSMutableArray* aRecurringEvents = [anEventEngine eventsRecurring:self];
    if(!self.alarmEvents) {
        self.alarmEvents = [NSMutableArray arrayWithCapacity:8];
    }else{
        [self.alarmEvents removeAllObjects];
    }
    [anEventEngine addEventsForDate:aStartDate toArray:self.alarmEvents events:anEvents recurringEvents:aRecurringEvents];
    if(self.alarmEvents.count > 0) {
        for(int i = self.alarmEvents.count-1 ; i >= 0 ; --i) {
            Event* anEvent = [self.alarmEvents objectAtIndex:i];
            if(anEvent.reminderMinutes >= 0) {
                //FXDebugLog(kFXLogActiveSync, @"reminder event: %@", anEvent);
            }
        }
    }else{
        // No events today, need to set timer to load events tomorrow
    }
}

- (void)foldersLoaded
{
    // Load events for today which need to generate reminders/alerts
    //
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(_loadReminders) object:nil];
    [[EventEngine engine].operationQueue addOperation:anOp];
}

//////////////////////////////////////////////////////////////////////////////
// SearchManagerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark SearchManagerDelegate

- (BOOL)shouldDeliverObject:(NSString*)aUid
{
    BOOL aShouldDeliver;
    Event* anEvent = [dictionary objectForKey:aUid];
    if(anEvent) {
        //[self.invitationEmails addObject:anEvent];
        aShouldDeliver = FALSE;
    }else{
        aShouldDeliver = TRUE;
    }
    
    return aShouldDeliver;
}

- (void)deliverSearchResults:(NSArray *)aResults
{
    for(Event* anEvent in aResults) {
        FXDebugLog(kFXLogActiveSync, @"invite search result: %@", anEvent);
        [dictionary setObject:anEvent forKey:anEvent.uid];
        //[self.invitationEmails addObject:anEvent];
    }
}

- (void) deliverAdditionalResults:(NSNumber *)availableResults
{
}

//////////////////////////////////////////////////////////////////////////////
// iCalendar(.ics) attachment loader
////////////////////////////////////////////////////////////////////////////////
#pragma mark iCalendar(.ics) attachment loader

- (ASItemOperations*)loadICalendarAttachment:(ASEmail*)anEmail
{
    ASItemOperations* anItemOp = nil;
    
    NSArray* anAttachments = anEmail.attachments;
    for(ASAttachment* anAttachment in anAttachments) {
        if([anAttachment.contentType isEqualToString:@"ics"]) {
            anItemOp = [ASItemOperations sendRequest:(ASAccount*)self.account
                                       fileReference:anAttachment.fileReference
                                            filePath:nil
                                        expectedSize:anAttachment.estimatedDataSize
                                            delegate:self];
            anItemOp.email = anEmail;
            break;
        }
    }
    
    return anItemOp;
}

//////////////////////////////////////////////////////////////////////////////
// iCalendar(.ics) parsing
////////////////////////////////////////////////////////////////////////////////
#pragma mark iCalendar(.ics) parsing

- (NSDate*)dateForICSString:(NSString*)aString
{
    NSDate* aDate = nil;
    NSRange aRange = [aString rangeOfString:@"T"];
    if(aRange.length == 1) {
        NSRange aRange = [aString rangeOfString:@"Z"];
        if(aRange.length == 1) {
            aDate = [[Event iCalFormatterZulu] dateFromString:aString];
        }else{
            aDate = [[Event iCalFormatter] dateFromString:aString];
        }
    }else{
        aDate = [[Event iCalFormatterDate] dateFromString:aString];
    }
    if(!aDate) {
        FXDebugLog(kFXLogActiveSync, @"dateForICSString failed: %@", aString);
    }
    return aDate;
}

- (Event*)parseComponent:(CGICalendarComponent*)aComponent
                 toEvent:(Event*)anEvent
           dateFormatter:(NSDateFormatter*)aDateFormatter
{
    for (CGICalendarProperty *aProp in [aComponent properties]) {
        NSString* aPropName = aProp.name;
        //FXDebugLog(kFXLogActiveSync, @"Prop: %@ %@", aPropName, aProp.value);
        
        if([aPropName isEqualToString:@"DTSTART"]) {
            anEvent.startDate = [self dateForICSString:aProp.value];
        }else if([aPropName isEqualToString:@"DTEND"]) {
            anEvent.endDate = [self dateForICSString:aProp.value];
        }else if([aPropName isEqualToString:@"DTSTAMP"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS DTSTAMP: %@", [[EventEngine dateFormatter] dateFromString:aProp.value]);
        }else if([aPropName isEqualToString:@"CREATED"]) {
            anEvent.created = [self dateForICSString:aProp.value];
        }else if([aPropName isEqualToString:@"LAST-MODIFIED"]) {
            anEvent.lastModified = [self dateForICSString:aProp.value];
        }else if([aPropName isEqualToString:@"SUMMARY"]) {
            anEvent.title = aProp.value;
        }else if([aPropName isEqualToString:@"UID"]) {
            anEvent.eventUid = aProp.value;
        }else if([aPropName isEqualToString:@"LOCATION"]) {
            anEvent.location = aProp.value;
        }else if([aPropName isEqualToString:@"REQUEST-STATUS"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS REQUEST-STATUS: %@", aProp.value);
        }else if([aPropName isEqualToString:@"STATUS"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS STATUS: %@", aProp.value);
        }else if([aPropName isEqualToString:@"SEQUENCE"]) {
            anEvent.sequence = [aProp.value intValue];
        }else if([aPropName isEqualToString:@"ATTENDEE"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS ATTENDEE: %@", aProp.value);
        }else if([aPropName isEqualToString:@"ORGANIZER"]) {
            NSArray* anArray = [aProp.value componentsSeparatedByString:@":"];
            if(anArray.count == 2) {
                if([[anArray objectAtIndex:0] isEqualToString:@"mailto"]) {
                    anEvent.organizerEmail = [anArray objectAtIndex:1];
                }else{
                    FXDebugLog(kFXLogActiveSync, @"Invalid organizer mailto");
                }
            }else{
                FXDebugLog(kFXLogActiveSync, @"Invalid organizer");
            }
        }else if([aPropName isEqualToString:@"RRULE"]) {
            anEvent.recurrence = [self parseRecurrence:aProp];
        }else if([aPropName isEqualToString:@"DESCRIPTION"]) {
            anEvent.notes = aProp.value;
        }else if([aPropName isEqualToString:@"TRANSP"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS TRANSP: %@", aProp.value);
        }else if([aPropName isEqualToString:@"RECURRENCE-ID"]) {
            //FXDebugLog(kFXLogFIXME, @"FIXME ICS RECURRENCE-ID: %@", [[EventEngine dateFormatter] dateFromString:aProp.value]);
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME loadICSFromPath: %@ %@", aPropName, aProp.value);
        }
    }
    return anEvent;
}

- (EventRecurrence*)parseRecurrence:(CGICalendarProperty*)aProp
{
    EventRecurrence* aRecurrence = [[EventRecurrence alloc] init];
    
    NSArray* aParts = [aProp parts];
    for (CGICalendarParameter* aPart in aParts) {
        NSString* aName = [aPart name];
        if([aName isEqualToString:@"FREQ"]) {
            ERecurrenceType aRecurrenceType = kRecurrenceNone;
            NSString* aValue = aPart.value;
            if([aValue isEqualToString:@"YEARLY"]) {
                aRecurrenceType = kRecurrenceYearly;
            }else if([aValue isEqualToString:@"MONTHLY"]) {
                aRecurrenceType = kRecurrenceMonthly;
            }else if([aValue isEqualToString:@"WEEKLY"]) {
                aRecurrenceType = kRecurrenceWeekly;
            }else if([aValue isEqualToString:@"DAILY"]) {
                aRecurrenceType = kRecurrenceDaily;
            }else if([aValue isEqualToString:@"NONE"]) {
                aRecurrenceType = kRecurrenceNone;
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME RRULE FREQ: %@", aValue);
            }
            aRecurrence.recurrenceType = aRecurrenceType;
            
        }else if([aName isEqualToString:@"UNTIL"]) {
            NSDate* aDate = [[EventEngine dateFormatter] dateFromString:aPart.value];
            if(aDate) {
                aRecurrence.untilDate = aDate;
            }else{
                FXDebugLog(kFXLogActiveSync, @"UNTIL date conversion failed: %@", aPart.value);
            }
            
        }else if([aName isEqualToString:@"BYDAY"]) {
            switch(aRecurrence.recurrenceType) {
                case kRecurrenceMonthly:
                    aRecurrence.recurrenceType = kRecurrenceMonthlyOnNthDay; break;
                case kRecurrenceYearly:
                    aRecurrence.recurrenceType = kRecurrenceYearlyOnNthDay; break;
                default:
                    FXDebugLog(kFXLogFIXME, @"FIXME BYDAY: %@", aPart.value); break;
            }
            NSArray* aDays = [aPart values];
            for(__strong NSString* aDay in aDays) {
                if(aDay.length > 0) {
                    unichar aChar = [aDay characterAtIndex:0];
                    if(isnumber(aChar)) {
                        aRecurrence.weekOfMonth = (int)(aChar - '0');
                        aDay = [aDay substringFromIndex:1];
                    }else if(aDay.length > 1 && aChar == '-' && isnumber([aDay characterAtIndex:1])) {
                        aRecurrence.weekOfMonth = -(int)([aDay characterAtIndex:1] - '0');
                        aDay = [aDay substringFromIndex:2];
                    }

                    [aRecurrence dayOfWeekFromString:aDay];

                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME BYDAY invalid: %@", aPart.value);
                }
            }
        }else if([aName isEqualToString:@"BYMONTH"]) {
            NSArray* aMonths = [aPart values];
            if(aMonths.count > 1) {
                FXDebugLog(kFXLogFIXME, @"FIXME BYMONTH multiple months");
            }
            for(NSString* aMonth in aMonths) {
                aRecurrence.monthOfYear = [aMonth intValue];
                break;
            }
        }else if([aName isEqualToString:@"BYMONTHDAY"]) {
            NSArray* aDays = [aPart values];
            if(aDays.count == 1) {
                aRecurrence.dayOfMonth = [[aDays objectAtIndex:0] intValue];
            }else{
                NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:aDays.count];
                for(NSString* aDay in aDays) {
                    [anArray addObject:[NSNumber numberWithInt:[aDay intValue]]];
                }
                aRecurrence.daysOfMonth = anArray;
            }
        }else if([aName isEqualToString:@"BYWEEKNO"]) {
            FXDebugLog(kFXLogFIXME, @"FIXME BYWEEKNO: %@ %@", aName, aPart.value);
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME RRULE: %@ %@", aName, aPart.value);
        }
    }
    
    return aRecurrence;
}

- (NSArray*)parseCGICalendar:(CGICalendar*)aCGICalendar
{
    NSMutableArray* anEvents = [NSMutableArray array];
   
    for (CGICalendarObject *icalObj in aCGICalendar.objects) {
        // Properties, headers like VERSION, PRODID, CALSCALE and METHOD
        //
        for (CGICalendarProperty* aCalendarProperty in [icalObj properties]) {
            if([aCalendarProperty.name isEqualToString:@"METHOD"]) {
                FXDebugLog(kFXLogActiveSync, @"METHOD = %@",  aCalendarProperty.value);
            }else{
                //FXDebugLog(kFXLogFIXME, @"FIXME property: %@ %@", aCalendarProperty.name, aCalendarProperty.value);
            }
        }

        // Components, like VEVENT and VTIMEZONE
        //
        for (CGICalendarComponent* aCalendarComponent in [icalObj components]) {
            NSString* aComponentType = [aCalendarComponent type];
            if([aComponentType isEqualToString:@"VEVENT"]) {
                Event* anEvent = [[Event alloc] initWithFolder:self];
                [self parseComponent:aCalendarComponent toEvent:anEvent dateFormatter:[Event iCalFormatterZulu]];
                [anEvents addObject:anEvent];
            }else if([aComponentType isEqualToString:@"VTIMEZONE"]) {
                FXDebugLog(kFXLogFIXME, @"FIXME ics VTIMEZONE");
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME ics %@", aComponentType);
            }
        }
    }
    return anEvents;
}

- (NSArray*)loadEventsFromICSPath:(NSString*)aPath
{
    CGICalendar* aCGICalendar = [[CGICalendar alloc] init];
    NSError* anError = nil;
    BOOL success = [aCGICalendar parseWithPath:aPath error:&anError];
    if(!success || anError) {
        FXDebugLog(kFXLogActiveSync, @"loadEventsFromICSPath failed: %@ %@", anError, aPath);
        return [NSArray array];
    }
    
    NSArray* anEvents = [self parseCGICalendar:aCGICalendar];
    return anEvents;
}

- (NSArray*)loadEventsFromICSData:(NSData*)aData
{
    NSError* anError;
    CGICalendar* aCGICalendar = [[CGICalendar alloc] init];
    NSString* aString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
    //FXDebugLog(kFXLogActiveSync, @"deliverData ics: %@", aString);
    [aCGICalendar parseWithString:aString error:&anError];
    NSArray* anEvents = [self parseCGICalendar:aCGICalendar];
    return anEvents;
}

////////////////////////////////////////////////////////////////////////////////////////////
// ASItemOperationsDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASItemOperationsDelegate

- (void)deliverProgress:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
    
}

- (void)deliverError:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
    FXDebugLog(kFXLogActiveSync, @"deliverData ics error: %@", message);
}

- (void)deliverFile:(NSString*)aPath itemOp:(ASItemOperations*)anItemOp
{    
}

- (void)deliverData:(NSData *)aData itemOp:(ASItemOperations *)anItemOp
{
    NSArray* anEvents = [self loadEventsFromICSData:aData];
    
    ASEmail* anEmail = anItemOp.email;
    if(anEmail) {
        if(!anEmail.meetingRequest) {
            anEmail.meetingRequest = [[MeetingRequest alloc] init];
        }
        MeetingRequest* aMeetingRequest = anEmail.meetingRequest;
        for(Event* anEvent in anEvents) {
            //FXDebugLog(kFXLogActiveSync, @"deliverData ics: %@", anEvent);
            aMeetingRequest.startDate   = anEvent.startDate;
            aMeetingRequest.endDate     = anEvent.endDate;
            aMeetingRequest.organizer   = anEvent.organizerEmail;
            aMeetingRequest.recurrence  = anEvent.recurrence;
        }
        [[EmailProcessor getSingleton] queueUpdateMeetingRequest:anEmail];
    }else{
        FXDebugLog(kFXLogActiveSync, @"ASCalendarFolder ics parse no email");
    }
}

@end
