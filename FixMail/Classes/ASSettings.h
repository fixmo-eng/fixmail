/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

#import "WBXMLRequest.h"

@class ASAccount;
@class ASOutOfOffice;

// Types
//
#define kOOFBodyText    @"Text"
#define kOOFBodyHTML    @"HTML"

typedef enum {
    kSettingsSuccess                = 1,
    kSettingsProtocolError          = 2,
    kSettingsAccessDenied           = 3,
    kSettingsServerUnavailable      = 4,
    kSettingsInvalidArguments       = 5,
    kSettingsConflictingArguments   = 6,
    kSettingsDeniedByPolicy         = 7
} ESettingsStatus;

typedef enum {
    kSettingsOOFStateDisabled       = 0,
    kSettingsOOFStateGlobal         = 1,
    kSettingsOOFStateTimeBased      = 2
} ESettingsOOFState;

typedef enum {
    kSettingsCmdGetUserInfo         = 1,
    kSettingsCmdGetOOF              = 2,
    kSettingsCmdSetOOF              = 3,
    kSettingsCmdDisableOOF          = 4
} ESettingsCmd;



@interface ASSettings : WBXMLRequest {
}

@property (nonatomic) ESettingsStatus       statusCodeAS;
@property (nonatomic) ESettingsCmd          cmd;

@property (nonatomic) ESettingsOOFState     oofState;
@property (nonatomic,strong) NSDate*        startDate;
@property (nonatomic,strong) NSDate*        endDate;

@property (nonatomic,strong) ASOutOfOffice* oofInternal;
@property (nonatomic,strong) ASOutOfOffice* oofExternalKnown;
@property (nonatomic,strong) ASOutOfOffice* oofExternalUnknown;

// Get User Information
//
+ (ASSettings*)sendGetUserInfo:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;
+ (ASSettings*)queueGetUserInfo:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;

// Get Out Of Office state
//
+ (ASSettings*)sendGetOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;
+ (ASSettings*)queueGetOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;

// Set Out of Office, Global, no time range, takes effect immediately and stays until removed
//
+ (ASSettings*)sendSetOutOfOffice:(ASAccount*)anAccount
                         internal:(ASOutOfOffice*)anOofInternal
                    externalKnown:(ASOutOfOffice*)anExternalKnown
                  externalUnknown:(ASOutOfOffice*)anExternalUnknown
                    displayErrors:(BOOL)aDisplayErrors;
+ (ASSettings*)queueSetOutOfOffice:(ASAccount*)anAccount
                          internal:(ASOutOfOffice*)anOofInternal
                     externalKnown:(ASOutOfOffice*)anOofExternalKnown
                   externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                     displayErrors:(BOOL)aDisplayErrors;

// Set Out Of Office for time window 
//
+ (ASSettings*)sendSetOutOfOfficeForTime:(ASAccount*)anAccount
                                   start:(NSDate*)aStartDate
                                     end:(NSDate*)anEndDate
                                internal:(ASOutOfOffice*)anOofInternal
                           externalKnown:(ASOutOfOffice*)anExternalKnown
                         externalUnknown:(ASOutOfOffice*)anExternalUnknown
                           displayErrors:(BOOL)aDisplayErrors;
+ (ASSettings*)queueSetOutOfOfficeForTime:(ASAccount*)anAccount
                                    start:(NSDate*)aStartDate
                                      end:(NSDate*)anEndDate
                                 internal:(ASOutOfOffice*)anOofInternal
                            externalKnown:(ASOutOfOffice*)anOofExternalKnown
                          externalUnknown:(ASOutOfOffice*)anOofExternalUnknown
                            displayErrors:(BOOL)aDisplayErrors;

// Disable Out Of Office
+ (ASSettings*)sendDisableOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;
+ (ASSettings*)queueDisableOutOfOffice:(ASAccount*)anAccount displayErrors:(BOOL)aDisplayErrors;

// Construct
- (id)initWithAccount:(ASAccount*)anAccount;

@end
