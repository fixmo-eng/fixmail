#if 0
// Failed experitment, add and change don't seem to work on email which prevents changing drafts on the server
//
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncObject.h"

@class ASAccount;
@class ASSync;
@class ASEmail;
@protocol ASSyncObjectDelegate;

@interface ASSyncEmail : ASSyncObject {
    BOOL                            isAdd;
}

@property (nonatomic,strong) ASEmail*                       email;
@property (nonatomic,strong) NSObject<ASSyncObjectDelegate>* delegate;

- (id)initWithEmail:(ASEmail*)anEvent delegate:(NSObject<ASSyncObjectDelegate>*)aDelegate isAdd:(BOOL)anIsAdd;

- (void)send;

@end
#endif