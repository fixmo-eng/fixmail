/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "WBXMLRequest.h"
#import "ASFolderUpdateDelegate.h"

@class ASFolder;

typedef enum {
    kFolderUpdateSuccess                = 1,
    kFolderUpdateFolderExists           = 2,
    kFolderUpdateIsRecipientFolder      = 3,
    kFolderUpdateDoesNotExist           = 4,
    kFolderUpdateParentNotFound         = 5,
    kFolderUpdateServerError            = 6,
    kFolderUpdateSyncKeyInvalid         = 9,
    kFolderUpdateMalformedRequest       = 10,
    kFolderUpdateUnknownError           = 11
} EFolderUpdateStatus;

@interface ASFolderUpdate : WBXMLRequest {
}

@property (nonatomic,strong)    ASFolder*                           folder;
@property (nonatomic,strong)    NSString*                           name;
@property (nonatomic,strong)    NSObject<ASFolderUpdateDelegate>*   delegate;
@property (nonatomic)           EFolderUpdateStatus                 statusCodeAS;

// Factory
+ (ASFolderUpdate*)queueFolderUpdate:(ASFolder*)aFolder
                             newName:(NSString*)aNewName
                       displayErrors:(BOOL)aDisplayErrors;

@end