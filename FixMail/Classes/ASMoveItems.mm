/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASMoveItems.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASMoveItemsDelegate.h"
#import "WBXMLDataGenerator.h"

@implementation ASMoveItems

// Properties
@synthesize emails;
@synthesize toFolder;
@synthesize delegate;
@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"MoveItems";
static const ASTag kCommandTag  = MOVE_MOVE_ITEMS;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASMoveItems*)sendMove:(ASAccount*)anAccount
                  emails:(NSArray*)anEmails
                toFolder:(ASFolder*)aToFolder
           displayErrors:(BOOL)aDisplayErrors
{
    ASMoveItems* aMoveItems = [[ASMoveItems alloc] initWithAccount:anAccount];
    aMoveItems.emails           = anEmails;
    aMoveItems.displayErrors    = aDisplayErrors;
    aMoveItems.toFolder         = aToFolder;
    [aMoveItems send];
    
    return aMoveItems;
}

+ (ASMoveItems*)queueMove:(ASAccount*)anAccount
                   emails:(NSArray*)anEmails
                 toFolder:(ASFolder*)aToFolder
            displayErrors:(BOOL)aDisplayErrors
{
    ASMoveItems* aMoveItems = [[ASMoveItems alloc] initWithAccount:anAccount];
    aMoveItems.emails           = anEmails;
    aMoveItems.displayErrors    = aDisplayErrors;
    aMoveItems.toFolder         = aToFolder;
    [aMoveItems queue];
    
    return aMoveItems;
}

- (void)send
{
    NSData* aWbxml = [self wbxmlMove:self.account emails:self.emails toFolder:self.toFolder];
    NSMutableURLRequest* aURLRequest = [self.account createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [self.account send:aURLRequest httpRequest:self];
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlMove:(ASAccount*)anAccount
              emails:(NSArray*)anEmails
            toFolder:(ASFolder*)aToFolder
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        for(ASEmail* anEmail in anEmails) {
            wbxml.start(MOVE_MOVE); {
                wbxml.keyValue(MOVE_SRCMSGID, anEmail.uid);
                wbxml.keyValue(MOVE_SRCFLDID, anEmail.folder.uid);
                wbxml.keyValue(MOVE_DSTFLDID, aToFolder.uid);
            }
            wbxml.end();
        }
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if(self = [super initWithAccount:anAccount command:kCommand commandTag:kCommandTag]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)displayError
{
    switch(self.statusCodeAS) {
        case kMoveItemsSuccess:
            break;
#ifdef DEBUGX
        case kMoveItemsInvalidSourceID:
            [HttpRequest handleASError:@"Move items invalid source ID"]; break;
        case kMoveItemsInvalidDestinationID:
            [HttpRequest handleASError:@"Move items invalid destination"]; break;
        case kMoveItemsSameSourceAndDestination:
            [HttpRequest handleASError:@"Move items same source and destination"]; break;
        case kMoveItemsLockedOrMoreThanOneDestination:
            [HttpRequest handleASError:@"Move items locked or more than one destination"]; break;
        case kMoveItemsLocked:
            [HttpRequest handleASError:@"Move items locked"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)self.statusCodeAS]; break;
#else
		default:
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"MOVE_ITEMS_ERROR", @"ErrorAlert", @"Error alert message for Move Items error"), self.statusCodeAS]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{    
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case MOVE_RESPONSE:
                    break;
                case MOVE_STATUS:
                    self.statusCodeAS = (EMoveItemsStatus)[aParser getIntTraceable];
                    break;
                case MOVE_SRCMSGID:
                    FXDebugLog(kFXLogActiveSync, @"MOVE_SRCMSGID: %@", [aParser getString]);
                    break;
                case MOVE_DSTMSGID:
                    FXDebugLog(kFXLogActiveSync, @"MOVE_DSTMSGID: %@", [aParser getString]);
                    break;
                case MOVE_SRCFLDID:
                    FXDebugLog(kFXLogActiveSync, @"MOVE_SRCFLDID: %@", [aParser getString]);
                    break;
                case MOVE_DSTFLDID:
                    FXDebugLog(kFXLogActiveSync, @"MOVE_DSTFLDID: %@", [aParser getString]);
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
        if(self.delegate) {
            if(self.statusCodeAS == kMoveItemsSuccess) {
                [self.delegate moveItemsSuccess:self];
                [self success];
            }else{
                if(self.delegate) {
                    [self.delegate moveItemsFailed:self];
                }
                [self fail];
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"Move items response" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    [super connection:connection didReceiveResponse:response];
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    [super connectionDidFinishLoading:connection];
}

@end