/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"

@class ASEmail;

@interface ASEmailChange : BaseObject
{
    NSString*           uid;
    ASEmail*            email;
    NSUInteger          flags;
}

// Properties
@property (nonatomic, strong) NSString*     uid;
@property (nonatomic, strong) ASEmail*      email;


// Construct/Destruct
- (id)initWithUid:(NSString*)aUid;

// Flags Transient
- (BOOL)readChanged;
- (BOOL)readState;
- (void)setReadState:(BOOL)state;

- (BOOL)flagChanged;
- (BOOL)flagState;
- (void)setFlagState:(BOOL)state;

- (BOOL)categoryChanged;
- (void)setCategoryChanged:(BOOL)state;

@end
