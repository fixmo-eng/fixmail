//
//  SearchRunner.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 3/29/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//  Executes searches, notifies UI of results.
#import "sqlite3.h"
#import "StringUtil.h"
#import "ASEmail.h"

@class BaseFolder;
@class EmailSearch;

@interface SearchRunner : NSObject

// Properties
@property (nonatomic,strong) NSObject *autocompleteLock;
@property (assign) volatile BOOL cancelled; // flag for when we cancel a search op

// Factory
+(id)getSingleton;

// Static termination
+ (void)clearPreparedStmts;

-(void)folderSearch:(EmailSearch*)anEmailSearch;
-(ASEmail*)emailForUid:(NSString*)aUid folder:(BaseFolder*)aFolder;
-(NSArray *)performEmailBodySearch:(EmailSearch*)anEmailSearch usingText:(NSString *)text;
-(NSArray *)performEmailAddressSearch:(EmailSearch*)anEmailSearch usingText:(NSString *)text;
-(void)meetingSearch:(EmailSearch*)anEmailSearch;
-(void)offlineChangesSearch:(EmailSearch*)anEmailSearch;
-(void)autocomplete:(NSString *)query withDelegate:(id)autocompleteDelegate;

// Contact Search
-(NSDictionary*)findContact:(NSString*)name;
-(NSMutableArray*)findContacts;

// Cancel
-(void)cancel;

@end

@protocol SearchManagerDelegate
//Called with a local search result in *absolute* position pos
- (void) deliverSearchResults: (NSArray *)results;
//Called with either YES or NO depending on if there are more results after pos.
- (void) deliverAdditionalResults:(NSNumber *)availableResults;
- (BOOL) shouldDeliverObject:(NSString*)aUid;
@end