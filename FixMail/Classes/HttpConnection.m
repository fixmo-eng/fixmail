/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "HttpConnection.h"
#import "HttpRequest.h"

@implementation HttpConnection

@synthesize URL;
@synthesize identifier;
@synthesize httpRequest;
@synthesize data;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


- (NSString*)_stringWithNewUUID
{
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    NSString *newUUID = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj));
    CFRelease(uuidObj);
    return newUUID;
}

- (id)initWithRequest:(NSURLRequest *)aUrlRequest 
          httpRequest:(HttpRequest*)anHttpRequest
			  runLoop:(NSRunLoop*)aRunLoop
{
    if (self = [super initWithRequest:aUrlRequest delegate:anHttpRequest startImmediately:false]) {
        data                = [[NSMutableData alloc] initWithCapacity:0];
        self.identifier     = [self _stringWithNewUUID];
        self.httpRequest    = anHttpRequest;
		self.URL            = [aUrlRequest URL];
        
		if([anHttpRequest thread]) {
			[self scheduleInRunLoop:aRunLoop forMode:NSDefaultRunLoopMode];
		}else{
			[self scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
		}
		[self start];
    }
    
    return self;
}


- (void)resetData
{
    [data setLength:0];
}

- (void)appendData:(NSData *)aData
{
    [data appendData:aData];
}

@end
