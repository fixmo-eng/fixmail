/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 *
 *	ActiveSync Documentation: MS-ASCMD, section 2.2.2.13
 */
#import "ASResolveRecipients.h"
#import "ASAccount.h"
#import "ASEmail.h"
#import "EmailAddress.h"
#import "WBXMLDataGenerator.h"

@implementation ASResolveRecipients

@synthesize statusCodeAS;

// Constants
static NSString* kCommand       = @"ResolveRecipients";
static const ASTag kCommandTag  = RESOLVE_RECIPIENTS_RESOLVE_RECIPIENTS;

static NSUInteger kMaxAmbiguousRecipients   = 99;
static NSUInteger kMaxCertificates          = 99;
static NSUInteger kMaxPictures              = 10;
static NSUInteger kMaxPictureSize           = 1024;

// Types
//

typedef enum {
    kRecipientTypeGAL       = 1,
    kRecipientTypeContact   = 2
} ERecipientType;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (ASResolveRecipients *)sendResolveRecipientsCert:(ASAccount*)anAccount
										recipients:(NSArray*)aRecipients
										  certType:(ECertType)aCertType
										  delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate
{
    ASResolveRecipients* aResolveRecipients = [[ASResolveRecipients alloc] initWithAccount:anAccount];
	aResolveRecipients.recipients = aRecipients;
	aResolveRecipients.delegate = aDelegate;

    NSData* aWbxml = [aResolveRecipients wbxmlResolveRecipientsCert:anAccount
                                                         recipients:aRecipients
                                                           certType:aCertType];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:aResolveRecipients];

	return aResolveRecipients;
}

+ (ASResolveRecipients *)sendResolveRecipientsAvailability:(ASAccount*)anAccount
												recipients:(NSArray*)aRecipients
												 startTime:(NSDate*)aStartTime
												   endTime:(NSDate*)anEndTime
												  delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate
{
    ASResolveRecipients* aResolveRecipients = [[ASResolveRecipients alloc] initWithAccount:anAccount];
	aResolveRecipients.recipients = aRecipients;
	aResolveRecipients.delegate = aDelegate;

    NSData* aWbxml = [aResolveRecipients wbxmlResolveRecipientsAvailability:anAccount
                                                                 recipients:aRecipients
                                                                  startTime:(NSDate*)aStartTime
                                                                    endTime:(NSDate*)anEndTime];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:aResolveRecipients];

	return aResolveRecipients;
}

+ (ASResolveRecipients *)sendResolveRecipientsPicture:(ASAccount*)anAccount
										   recipients:(NSArray*)aRecipients
											 delegate:(NSObject<ASResolveRecipientsDelegate>*)aDelegate
{
    ASResolveRecipients* aResolveRecipients = [[ASResolveRecipients alloc] initWithAccount:anAccount];
	aResolveRecipients.recipients = aRecipients;
	aResolveRecipients.delegate = aDelegate;

    NSData* aWbxml = [aResolveRecipients wbxmlResolveRecipientsPicture:anAccount
                                                            recipients:aRecipients];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:aResolveRecipients];

	return aResolveRecipients;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlResolveRecipientsCert:(ASAccount*)anAccount
                           recipients:(NSArray*)aRecipients
                             certType:(ECertType)aCertType
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        for(id aRecipient in aRecipients) {
			if ([aRecipient isKindOfClass:[EmailAddress class]]) {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, ((EmailAddress *)aRecipient).address);
			} else {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, aRecipient);
			}
        }
        wbxml.start(RESOLVE_RECIPIENTS_OPTIONS); {
            wbxml.keyValueInt(RESOLVE_RECIPIENTS_CERTIFICATE_RETRIEVAL, aCertType);
            wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_CERTIFICATES, kMaxCertificates);
            wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_AMBIGUOUS_RECIPIENTS, kMaxAmbiguousRecipients);
        }wbxml.end();

    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlResolveRecipientsAvailability:(ASAccount*)anAccount
                                   recipients:(NSArray*)aRecipients
                                    startTime:(NSDate*)aStartTime
                                      endTime:(NSDate*)anEndTime
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        for(id aRecipient in aRecipients) {
			if ([aRecipient isKindOfClass:[EmailAddress class]]) {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, ((EmailAddress *)aRecipient).address);
			} else {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, aRecipient);
			}
        }
        wbxml.start(RESOLVE_RECIPIENTS_OPTIONS); {
            wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_AMBIGUOUS_RECIPIENTS, kMaxAmbiguousRecipients);
            wbxml.start(RESOLVE_RECIPIENTS_AVAILABILITY); {
                NSDateFormatter* aDateFormatter = [ASEmail dateFormatter];
                wbxml.keyValue(RESOLVE_RECIPIENTS_START_TIME, [aDateFormatter stringFromDate:aStartTime]);
                wbxml.keyValue(RESOLVE_RECIPIENTS_END_TIME,   [aDateFormatter stringFromDate:anEndTime]);
            }wbxml.end();
        }wbxml.end();

    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlResolveRecipientsPicture:(ASAccount*)anAccount
                              recipients:(NSArray*)aRecipients
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        for(id aRecipient in aRecipients) {
			if ([aRecipient isKindOfClass:[EmailAddress class]]) {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, ((EmailAddress *)aRecipient).address);
			} else {
				wbxml.keyValue(RESOLVE_RECIPIENTS_TO, aRecipient);
			}
        }
        wbxml.start(RESOLVE_RECIPIENTS_OPTIONS); {
            wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_AMBIGUOUS_RECIPIENTS, kMaxAmbiguousRecipients);
            wbxml.start(RESOLVE_RECIPIENTS_PICTURE); {
                wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_SIZE, kMaxPictureSize);
                wbxml.keyValueInt(RESOLVE_RECIPIENTS_MAX_PICTURES, kMaxPictures);
            }wbxml.end();
        }wbxml.end();
        
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setStatus:(EResolveStatus)aStatus
{
    self.statusCodeAS = aStatus;
    
    switch(aStatus) {
        case kResolveStatusSuccess:
            break;

		// have to handle these
        case kResolveStatusRecipientUnresolved:
			break;
        case kResolveStatusNoSMIMECertificate:
			break;
        case kResolveStatusRecipientAmbiguous1:
        case kResolveStatusRecipientAmbiguous2:
			break;
        case kResolveStatusProtocolError:
#ifdef DEBUG
            [HttpRequest handleASError:@"Resolve recipients protocol error"]; break;
#else
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"RESOLVE_ERROR", @"ErrorAlert", @"Error alert message for resolve error"), aStatus]];
#endif
        case kResolveStatusServerError:
#warning FIXME retry here
#ifdef DEBUG
            [HttpRequest handleASError:@"Resolve recipients server error, should retry"]; break;
#else
			[HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"RESOLVE_ERROR", @"ErrorAlert", @"Error alert message for resolve error"), aStatus]];
#endif
        // ActiveSync global errors that happen here
        case kResolveStatusCertificateLimitReached:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients more than 100 recipients"); break;
        case kResolveStatusMoreThan100Recipients:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients more than 100 recipients"); break;
        case kResolveStatusMoreThan20Recipients:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients more than 20 recipients"); break;
        case kResolveStatusFreeBusyStateTimeout:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients free busy state timeout"); break;
        case kResolveStatusFreeBusyStateUnresolved:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients free busy state unresolved"); break;
        case kResolveStatusNoPicture:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients no picture"); break;
        case kResolveStatusPictureTooLarge:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients picture too large"); break;
        case kResolveStatusPictureLimitReached:
            FXDebugLog(kFXLogActiveSync, @"Resolve recipients picture limit reached"); break;
            
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aStatus]; break;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)recipientParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:RESOLVE_RECIPIENTS_RECIPIENT]) != AS_END) {
        switch(tag) {
            case RESOLVE_RECIPIENTS_STATUS:
                [self setStatus:(EResolveStatus)[aParser getInt]];
                break;
            case RESOLVE_RECIPIENTS_TYPE:
            {
                ERecipientType aRecipientType = (ERecipientType)[aParser getInt];
                switch (aRecipientType) {
                    case kRecipientTypeGAL:
                        FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_TYPE: GAL");
                        break;
                    case kRecipientTypeContact:
                        FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_TYPE: Contact");
                        break;
                    default:
                        FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_TYPE: Unknown");
                        break;
                }
                break;
            }
            case RESOLVE_RECIPIENTS_DISPLAY_NAME:
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_DISPLAY_NAME: %@", [aParser getString]);
                break;
            case RESOLVE_RECIPIENTS_EMAIL_ADDRESS:
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_EMAIL_ADDRESS: %@", [aParser getString]);
                break;
                
            case RESOLVE_RECIPIENTS_PICTURE:
                [self pictureParser:aParser];
                break;
                
            case RESOLVE_RECIPIENTS_CERTIFICATES:
                [self certificateParser:aParser];
                break;
                
            case RESOLVE_RECIPIENTS_AVAILABILITY:
                [self availabilityParser:aParser];
                break;

            case RESOLVE_RECIPIENTS_RECIPIENT_COUNT:
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_RECIPIENT_COUNT: %d", [aParser getInt]);
                break;
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)certificateParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:RESOLVE_RECIPIENTS_CERTIFICATES]) != AS_END) {
        switch(tag) {
            case RESOLVE_RECIPIENTS_STATUS: {
				int aStatus = [aParser getInt];
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_STATUS: %d", aStatus);
                [self setStatus:(EResolveStatus)aStatus];
            }    break;
            case RESOLVE_RECIPIENTS_CERTIFICATE_COUNT:
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_CERTIFICATE_COUNT: %d", [aParser getInt]);
                break;
            case RESOLVE_RECIPIENTS_RECIPIENT_COUNT:
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_RECIPIENT_COUNT: %d", [aParser getInt]);
                break;
            case RESOLVE_RECIPIENTS_CERTIFICATE:
            {
                NSData* aData = [aParser getData];
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_CERTIFICATE: %d", aData.length);
				if(self.delegate) {
#warning FIXME Should handle multiple addresses
					[self.delegate deliverCertificate:aData emailAddress:self.recipients[0] resolveOp:self];
				}
                break;
            }
            case RESOLVE_RECIPIENTS_MINI_CERTIFICATE:
            {
                NSData* aData = [aParser getData];
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_MINI_CERTIFICATE: %d", aData.length);
				if(self.delegate) {
#warning FIXME Should handle multiple addresses
					[self.delegate deliverMiniCertificate:aData emailAddress:self.recipients[0] resolveOp:self];
				}
                break;
            }
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)pictureParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:RESOLVE_RECIPIENTS_PICTURE]) != AS_END) {
        switch(tag) {
            case RESOLVE_RECIPIENTS_STATUS:
                [self setStatus:(EResolveStatus)[aParser getInt]];
                break;
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)availabilityParser:(FastWBXMLParser*)aParser
{
    ASTag tag;
    while ((tag = [aParser nextTag:RESOLVE_RECIPIENTS_AVAILABILITY]) != AS_END) {
        switch(tag) {
            case RESOLVE_RECIPIENTS_STATUS:
                [self setStatus:(EResolveStatus)[aParser getInt]];
                break;
            case RESOLVE_RECIPIENTS_MERGED_FREE_BUSY:
            {
                NSData* aData = [aParser getData];
                FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_MERGED_FREE_BUSY: %d", aData.length);
                break;
            }
            default:
                [aParser skipTag]; break;
        }
    }
}

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case RESOLVE_RECIPIENTS_STATUS:
                    [self setStatus:(EResolveStatus)[aParser getInt]];
                    break;
                case RESOLVE_RECIPIENTS_RESPONSE:
//                    FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_RESPONSE: %d", [aParser getInt]);
                    break;
                case RESOLVE_RECIPIENTS_TO:
                    FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_TO: %@", [aParser getString]);
                    break;
                case RESOLVE_RECIPIENTS_RECIPIENT_COUNT:
                    FXDebugLog(kFXLogActiveSync, @"RESOLVE_RECIPIENTS_RECIPIENT_COUNT: %d", [aParser getInt]);
                    break;
                case RESOLVE_RECIPIENTS_RECIPIENT:
                    [self recipientParser:aParser];
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"ResolveRecipients parser" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
            
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", kCommand);
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag];
		if(self.delegate) {
			[self.delegate deliverFinished:[NSString stringWithFormat:@"%@ finished: %d", kCommand, self.statusCode] resolveOp:self];
		}
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
		if(self.delegate) {
			[self.delegate deliverError:[NSString stringWithFormat:@"%@ failed: %d", kCommand, self.statusCode] resolveOp:self];
		}
    }
    [super connectionDidFinishLoading:connection];
}

@end
