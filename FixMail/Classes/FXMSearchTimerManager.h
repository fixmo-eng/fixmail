//
//  FXMSearchTimerManager.h
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-07-04.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FXMSearchTimerManagerDelegate
-(void)searchTimerFired:(NSTimer *)searchTimer;
@end

@interface FXMSearchTimerManager : NSObject
@property (strong, nonatomic) id<FXMSearchTimerManagerDelegate> delegate;

-(FXMSearchTimerManager *)initWithDelegate:(id<FXMSearchTimerManagerDelegate>)delegate invalidateAfterTwoCharacters:(BOOL)invalidate;

-(void)updateTimerForSearchText:(NSString *)text;
@end
