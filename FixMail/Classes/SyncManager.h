//
//  SyncManager.h
//  Remail
//
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//
//  Singleton. SyncManager is the central place for coordinating syncs:
//  - starting syncs if not in progress
//  - registering for sync-related events
//  - persists sync state of sync processes
//
//  However, SyncManager itself has none of the syncing logic.
//  That's contained in GmailSync, ImapFolderWorker, etc.
#import "ProgressDelegate.h"

@class BaseAccount;

@interface SyncManager : NSObject {
	// delegates for reporting progress
	id                  progressDelegate;
	id                  progressNumbersDelegate;
	id                  clientMessageDelegate;
	id                  theNewEmailDelegate;

    NSString*           progressString;
    
	// sync-related stuff
	NSMutableArray*     syncStates;
	BOOL                syncInProgress;
	BOOL                clientMessageWasError;

	// Attachment checks
	NSSet*              okContentTypes;
	NSDictionary*       extensionContentType;
	
	// message to skip while syncing
	int                 lastErrorAccountNum;
	int                 lastErrorFolderNum;
	int                 lastErrorStartSeq;
	int                 skipMessageAccountNum;
	int                 skipMessageFolderNum;
	int                 skipMessageStartSeq;
    
	NSDate*             lastAbort;
}

@property (nonatomic,strong) id progressDelegate;
@property (nonatomic,strong) id progressNumbersDelegate;
@property (nonatomic,strong) id clientMessageDelegate;
@property (nonatomic,strong) id theNewEmailDelegate;

@property (strong) NSString* progressString;

@property (nonatomic,strong) NSMutableArray* syncStates;
@property (assign) BOOL syncInProgress;
@property (nonatomic) BOOL clientMessageWasError;

//for checking attachments
@property (nonatomic, strong) NSSet* okContentTypes;
@property (nonatomic, strong) NSDictionary* extensionContentType;

// skip message
@property (nonatomic) int lastErrorAccountNum;
@property (nonatomic) int lastErrorFolderNum;
@property (nonatomic) int lastErrorStartSeq;
@property (nonatomic) int skipMessageAccountNum;
@property (nonatomic) int skipMessageFolderNum;
@property (nonatomic) int skipMessageStartSeq;
@property (nonatomic, strong) NSDate* lastAbort;


+(id)getSingleton;
+ (NSDictionary*)loadFolderState:(int)accountNum;

-(id)init; 

// Folder states
-(int)folderCount:(int)accountNum;
-(void)addAccountState;
-(void)addFolderState:(NSMutableDictionary *)data accountNum:(int)accountNum;
-(BOOL)isFolderDeleted:(int)folderNum accountNum:(int)accountNum;
-(void)markFolderDeleted:(int)folderNum accountNum:(int)accountNum;
- (void)deleteAllFolders:(int)accountNum;
-(void)persistState:(NSMutableDictionary *)data forFolderNum:(int)folderNum accountNum:(int)accountNum;
-(NSMutableDictionary*)retrieveState:(int)folderNum accountNum:(int)accountNum;

// Sync process feedback endpoint
-(void)reportProgressString:(NSString*)progress;
-(void)reportProgress:(float)progress withMessage:(NSString*)message;
-(void)reportProgressNumbers:(int)total synced:(int)synced folderNum:(int)folderNum accountNum:(int)accountNum;

// registration for notifications
-(void)registerForProgressNumbersWithDelegate:(id) delegate;
-(void)registerForProgressWithDelegate:(NSObject<ProgressDelegate>*)delegate;
-(void)removeProgressDelegate:(NSObject<ProgressDelegate>*)aDelegate;

@end



