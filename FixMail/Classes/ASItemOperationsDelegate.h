/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

@class ASItemOperations;

@protocol ASItemOperationsDelegate <NSObject>

- (void)deliverProgress:(NSString*)message itemOp:(ASItemOperations*)anItemOp;
- (void)deliverError:(NSString*)message itemOp:(ASItemOperations*)anItemOp;
- (void)deliverFile:(NSString*)aPath itemOp:(ASItemOperations*)anItemOp; // This is called if storing to file when complete
- (void)deliverData:(NSData*)aData itemOp:(ASItemOperations*)anItemOp;  // This is called if storing to NSData when complete

@end
