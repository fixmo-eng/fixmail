/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "EmailSearch.h"
#import "BaseFolder.h"

@implementation EmailSearch

@synthesize delegate;
@synthesize folder;
@synthesize datetime;
@synthesize uid;
@synthesize pageSize;
@synthesize cancelled;

// Constants
static const NSInteger kPageSizeDefault = 50;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(BaseFolder*)aFolder delegate:(id<SearchManagerDelegate>)aDelegate
{
	if (self = [super init]) {
        self.folder     = aFolder;
        self.delegate   = aDelegate;
        pageSize        = kPageSizeDefault;
	}
	return self;
}

- (void)dealloc
{
    [self setSqlStatement:NULL];
}

- (sqlite3_stmt*)sqlStatement
{
    return sqlStatement;
}

- (void)setSqlStatement:(sqlite3_stmt*)aSqlStatement
{
    if(sqlStatement) {
		sqlite3_finalize(sqlStatement);
    }
    sqlStatement = aSqlStatement;
}

- (void)didCancel
{
    if(sqlStatement) {
		sqlite3_finalize(sqlStatement);
        sqlStatement = nil;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
	[self writeString:string		tag:@"folder"       value:self.folder.displayName];
    [self writePointer:string		tag:@"delegate"     pointer:(void*)self.delegate];

    if(self.datetime) {
        [self writeString:string    tag:@"dateTime"     value:[self.datetime descriptionWithLocale:[NSLocale currentLocale]]];
    }
    if(self.uid.length > 0) {
        [self writeString:string    tag:@"uid"          value:uid];
    }
    
    [self writeUnsigned:string      tag:@"pageSize"  value:self.pageSize];
    
	[string appendString:@"}\n"];
	
	return string;
}

@end
