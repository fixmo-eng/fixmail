/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASFolder.h"
#import "ASAccount.h"
#import "ASSyncFolders.h"
#import "ASSyncObject.h"
#import "CommitObjects.h"
#import "FastWBXMLParser.h"
#import "SyncManager.h"
#import "TraceEngine.h"

@implementation ASFolder

// Properties
@synthesize parentId;
@synthesize syncKey;
@synthesize syncStatus;

// Constants
static NSString* kParentId          = @"parentId";
static NSString* kSyncKey           = @"syncKey";
static NSString* kSyncDate          = @"syncDate";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount;
{
    if (self = [super initWithAccount:anAccount]) {
        syncKey         = @"0";         
    }
    
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ASFolder handleASError:[NSString stringWithFormat:@"%@ %@ %@: %@", FXLLocalizedStringFromTable(@"EXCEPTION:", @"ErrorAlert", @"Error alert view message when exception occurred"), where, [e name], [e reason]]];
}

+ (void)handleASError:(NSString*)anErrorMessage
{
	
	FXDebugLog(kFXLogActiveSync, FXLLocalizedStringFromTable(@"SYNC_RETRY", @"ErrorAlert", @"Error alert view message when syncing and retry occurred"));
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"FOLDER_ERROR", @"ErrorAlert", @"Error alert view title when folder error occured")
                                                          message:anErrorMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_ERROR", @"ErrorAlert", @"Error alert view cancel button")
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setSyncKey:(NSString*)aSyncKey syncDate:(NSDate*)aSyncDate
{
	NSAssert(aSyncKey != nil, @"aSyncKey != nil");
    self.syncKey    = aSyncKey;
    self.syncDate   = aSyncDate;
    
    NSMutableDictionary* aSyncState = [[SyncManager getSingleton] retrieveState:folderNum accountNum:accountNum];
    [aSyncState setObject:self.syncKey forKey:kSyncKey];
    if(self.syncDate) {
        [aSyncState setObject:self.syncDate forKey:kSyncDate];
    }else{
        [aSyncState removeObjectForKey:kSyncDate];
    }
}

+ (void)displaySyncStatus:(ESyncStatus)aSyncStatus
{
    switch(aSyncStatus) {
        case kSyncStatusOK:
            break;
        case kSyncStatusInvalidSyncKey:
            // MUST return to SyncKey element value (section 2.2.3.166.4) of 0 for the collection.
            // The client SHOULD either delete any items that were added since the last successful Sync
            // or the client MUST add those items back to the server after completing the full resynchronization.
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_INVALID_SYNC_KEY", @"ErrorAlert", @"Error alert view message when sync key was invalid")];
            break;
        case kSyncStatusProtocolError:
            // The request is invalid, fix it
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_PROTOCOL_ERROR", @"ErrorAlert", @"Error alert view message when syncing protocol error occurred")];
            break;
        case kSyncStatusServerError:
            // Retry sync, if it continues do a new sync with "0" key
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_SERVER_ERROR", @"ErrorAlert", @"Error alert view message when syncing server error occurred")];
            break;
        case kSyncStatusConversionError:
            // Stop sending the item, it will never work
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_CONVERSION_ERROR", @"ErrorAlert", @"Error alert view message when syncing conversion error occurred")];
            break;
        case kSyncStatusConflict:
            // User changed an item overwritten by server, abort operation and tell user
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_CONFLICT", @"ErrorAlert", @"Error alert view message when syncing conflict occurred")];
            break;
        case kSyncStatusObjectNotFound:
            // Do a FolderSync or Sync and prompt user if it continues
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_OBJECT_NOT_FOUND", @"ErrorAlert", @"Error alert view message when syncing and object not found")];
            break;
        case kSyncStatusCommandNotComplete:
            // Free up space in user's mailbox and retry
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_COMMAND_NOT_COMPLETE", @"ErrorAlert", @"Error alert view message when syncing and command not complete")];
            break;
        case kSyncStatusFolderHierarchyChanged:
            // Do a FolderSync and then retry
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_FOLDER_HIERARCHY_CHANGED", @"ErrorAlert", @"Error alert view message when syncing and folder hierarchy changed")];
            break;
        case kSyncStatusIncompleteCommand:
            // Resend a full Sync command request
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_INCOMPLETE_COMMAND", @"ErrorAlert", @"Error alert view message when syncing and command incomplete")];
            break;
        case kSyncStatusInvalidWaitOrHeartbeat:
            // ￼Update the Wait element value according to the Limit element and then resend the Sync command request.
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_INVALID_WAIT_OR_HEARTBEAT", @"ErrorAlert", @"Error alert view message when syncing and invalid wait or heartbeat")];
            break;
        case kSyncStatusInvalidCommand:
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_INVALID_COMMAND", @"ErrorAlert", @"Error alert view message when syncing and invalid command")];
            break;
        case kSyncStatusRetry:
            [ASFolder handleASError:FXLLocalizedStringFromTable(@"SYNC_RETRY", @"ErrorAlert", @"Error alert view message when syncing and retry occurred")];
            break;
        default:
        {
            WBXMLRequest* aQueuedRequest = [WBXMLRequest queuedRequest];
            if(aQueuedRequest) {
                [aQueuedRequest handleActiveSyncError:(EActiveSyncStatus)aSyncStatus];
            }else{
                [ASFolder handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"SYNC_STATUS_UNKNOWN_ERROR:", @"ErrorAlert", @"Error alert view message when syncing and unknown error occurred"), aSyncStatus]];
            }
            break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (NSString*)ASClass
{
    NSString* aClassString = @"";
    switch(folderType) {
        case kFolderTypeUser:
        case kFolderTypeInbox:
        case kFolderTypeDrafts:
        case kFolderTypeDeleted:
        case kFolderTypeSent:
        case kFolderTypeOutbox:  
        case kFolderTypeUserMailbox:
            aClassString = @"Email";        
            break;
        case kFolderTypeCalendar: 
            aClassString = @"Calendar";        
            break;
        case kFolderTypeContacts:
            aClassString = @"Contacts";        
            break;
        case kFolderTypeUserContacts:
            aClassString = @"Contacts";
            break;
        case kFolderTypeRecipientInfo:
            aClassString = @"Contacts";
            break;
        // Note: The following are untested
        case kFolderTypeNotes:
            aClassString = @"Email";    // This could be "Notes" but isn't
            break;
        case kFolderTypeJournal:
            aClassString = @"Email";
            break;
        case kFolderTypeTasks:
            aClassString = @"Email";
            break;
        default:  
            // FIXME aClassString = @"SMS";
            aClassString = @"Email";       
            break;
    }
    return aClassString;
}

////////////////////////////////////////////////////////////////////////////////
// Sync
////////////////////////////////////////////////////////////////////////////////
#pragma mark Sync

- (void)setNeedsInitialSync
{
    [self setIsInitialSynced:FALSE];
    [self setSyncKey:kInitialSyncKey syncDate:nil];
}

- (void)_folderSyncing
{
    for(NSObject<FolderDelegate>* delegate in delegates) {
        [delegate folderSyncing:self];
    }
}

- (void)folderSyncing
{
	[self setIsSyncing:TRUE];
    if(delegates.count > 0) {
        [self performSelectorOnMainThread:@selector(_folderSyncing) withObject:nil waitUntilDone:FALSE];
    }
	[[NSNotificationCenter defaultCenter] postNotificationName:kFolderSyncingNotification
														object:self
													  userInfo:nil
	 ];
}

- (void)_folderSynced
{
    for(NSObject<FolderDelegate>* delegate in delegates) {
        [delegate folderSynced:self];
    }
}

- (void)sendFolderSynced
{
    if(delegates.count > 0) {
        [self performSelectorOnMainThread:@selector(_folderSynced) withObject:nil waitUntilDone:FALSE];
    }
}

- (void)folderSynced
{
    [self setIsInitialSync:NO];
    [self setIsInitialSynced:YES];
    if(self.isResyncing) self.isResyncing = NO;
	[self setIsCountWanted:YES];
    [self commit];
    
    if(self.dbaseCommits <= 0) {
        [self sendFolderSynced];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kFolderSyncedNotification
                                                        object:self
                                                      userInfo:nil
     ];
}

- (void)folderCommitCompleted
{
    _dbaseCommits--;
    if(_dbaseCommits <= 0 && [self isInitialSynced]) {
        [self sendFolderSynced];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kFolderSyncedNotification
                                                        object:self
                                                      userInfo:nil
     ];
}

- (void)commitWithCallback:(CommitObjects*)aCommitObjects action:(SEL)anAction
{
    aCommitObjects.target = self;
    aCommitObjects.action = anAction;
    _dbaseCommits++;
}

////////////////////////////////////////////////////////////////////////////////
// ASFolder Virtual Methods
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolder Virtual Methods

- (void)_resyncFolder
{
    [self setSyncKey:kInitialSyncKey syncDate:nil];
    [self setIsInitialSynced:FALSE];
    [(ASAccount*)self.account initialSyncFolder:self];
}

- (void)resyncFolder
{
    [self setIsResyncing:TRUE];
    FXDebugLog(kFXLogActiveSync, @"Resync folder: %@", self.displayName);
    [self deleteDatabaseForFolder];
    [self performSelectorOnMainThread:@selector(_resyncFolder) withObject:nil waitUntilDone:FALSE];
}

- (void)addParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag object:(BaseObject*)anObject bodyType:(EBodyType)aBodyType
{
    NSAssert(0, @"addParser not overridden");
    FXDebugLog(kFXLogFIXME, @"FIXME ASFolder addParser");
}

- (void)deleteParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    NSAssert(0, @"deleteParser not overridden");
    FXDebugLog(kFXLogFIXME, @"FIXME ASFolder deleteParser");
}

- (void)changeParser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    NSAssert(0, @"changeParser not overridden");
    FXDebugLog(kFXLogFIXME, @"FIXME ASFolder changeParser");
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)parser:(FastWBXMLParser*)aParser
{
    int tag;
    while ((tag = [aParser nextTag:FOLDER_ADD]) != AS_END && tag != AS_END_DOCUMENT) {
        switch(tag) {
            case FOLDER_DISPLAY_NAME:
                self.displayName = [aParser getString]; 
                break;
            case FOLDER_TYPE:
                self.folderType = (EFolderType)[aParser getIntTraceable];
                break;
            case FOLDER_PARENT_ID:
                self.parentId = [aParser getStringTraceable];
                break;
            case FOLDER_SERVER_ID:
                self.uid = [aParser getStringTraceable];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
    //if([[TraceEngine engine] traceType] == kTraceTypeMinimal) {
    //    FXDebugLog(kFXLogTrace, @"folder %@ %@", self.uid, [BaseFolder stringForFolderType:self.folderType]);
    //}
    //FXDebugLog(kFXLogActiveSync, @"folder sync: %@", self);
}

////////////////////////////////////////////////////////////////////////////////
// Load/Save
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Save

- (void)loadFromStore:(NSDictionary *)aDictionary
{
    @try {
        [super loadFromStore:aDictionary];
        
        // ActiveSync variables
        //
        self.parentId = [aDictionary objectForKey:kParentId];
        if([parentId length] == 0) {
            FXDebugLog(kFXLogActiveSync, @"Folder parentId invalid");
        }
        
        NSString* aSyncKey  = [aDictionary objectForKey:kSyncKey];
        NSDate* aSyncDate   = [aDictionary objectForKey:kSyncDate];
        if([aSyncKey length] > 0) {
            self.syncKey    = aSyncKey;
            self.syncDate   = aSyncDate;
        }else{
            [self setSyncKey:kInitialSyncKey syncDate:nil];
        }
    }@catch (NSException* e) {
        [self logException:@"loadFromStore" exception:e];
    } 
}

- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary
{
    if(self.parentId) {
        [aDictionary setObject:self.parentId    forKey:kParentId];
    }
    if(self.syncKey) {
        [aDictionary setObject:self.syncKey     forKey:kSyncKey];
    }
    if(self.syncDate) {
        [aDictionary setObject:self.syncDate    forKey:kSyncDate];
    }
    
    [super folderAsDictionary:aDictionary];
}

////////////////////////////////////////////////////////////////////////////////
// BaseFolder Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark BaseFolder Overrides

// Required Override
- (void)commit
{
    [lock lock];
    @try {
        SyncManager* aSyncManager = [SyncManager getSingleton];
        NSMutableDictionary* aSyncState = [aSyncManager retrieveState:folderNum accountNum:accountNum];
        if(!aSyncState) {
            aSyncState = [NSMutableDictionary dictionaryWithCapacity:20];
        }
        [self folderAsDictionary:aSyncState];
        [aSyncManager persistState:aSyncState forFolderNum:folderNum accountNum:accountNum];
    }@catch (NSException* e) {
        [self logException:@"commit" exception:e];
    }
    [lock unlock];
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"{\n"];
        
    [self writeString:string		tag:kParentId       value:self.parentId];
    [self writeString:string		tag:kSyncKey        value:self.syncKey];
    if(self.syncDate) {
        NSString* aDateString = [NSDateFormatter localizedStringFromDate:self.syncDate dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
        [self writeString:string    tag:kSyncDate       value:aDateString];
    }

	[string appendString:@"}\n"];
	
	return string;
}

@end
