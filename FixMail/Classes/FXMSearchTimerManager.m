//
//  FXMSearchTimerManager.m
//  FixMail
//
//  Created by Magali Boizot-Roche on 2013-07-04.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMSearchTimerManager.h"

@interface FXMSearchTimerManager ()
@property (strong, nonatomic) NSTimer *searchTimer;
@property BOOL invalidateAfterTwoCharacters;
@end

@implementation FXMSearchTimerManager
@synthesize searchTimer;

-(FXMSearchTimerManager *)initWithDelegate:(id<FXMSearchTimerManagerDelegate>)delegate invalidateAfterTwoCharacters:(BOOL)invalidate
{
	self = [super init];
	if (self) {
		self.delegate = delegate;
		self.invalidateAfterTwoCharacters = invalidate;
	}
	return self;
}

-(void)updateTimerForSearchText:(NSString *)text
{
	if (self.searchTimer) [self.searchTimer invalidate];
	if (text.length > 0) {
		if (text.length < 3 || !self.invalidateAfterTwoCharacters) {
			self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:kTypingPauseInterval target:self.delegate selector:@selector(searchTimerFired:) userInfo:nil repeats:NO];
		}
	}
}

@end
