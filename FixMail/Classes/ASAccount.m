 /* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASAccount.h"
#import "AppSettings.h"
#import "ASAccountExchange.h"
#import "ASAccountGmail.h"
#import "ASAutoDiscover.h"
#import "ASCalendarFolder.h"
#import "ASContact.h"
#import "ASContactFolder.h"
#import "ASEmail.h"
#import "ASEmailFolder.h"
#import "ASFolder.h"
#import "ASFolderSync.h"
#import "ASGetItemEstimate.h"
#import "ASItemOperations.h"
#import "ASPing.h"
#import "ASProvision.h"
#import "ASProvisionPolicy.h"
#import "ASOptions.h"
#import "ASSettings.h"
#import "ASSync.h"
#import "ASSyncFolders.h"
#import "Event.h"
#import "EventEngine.h"
#import "EmailProcessor.h"
#import "FXDatabase.h"
#import "GlobalDBFunctions.h"
#import "HttpEngine.h"
#import "HttpRequest.h"
#import "MemUtil.h"
#import "NSData+Base64.h"
#import "NSString+SBJSON.h"
#import "Reachability.h"
#import "StringUtil.h"
#import "SyncManager.h"

@implementation ASAccount

// Properties
@synthesize isInitialSynced;

// Globals
extern BOOL gIsFixTrace;

// Constants
static const int kSecondsPerMinute              = 60;
static const int kHTTPTimeout                   = 20.0f;    // Seconds

// This is the time interval for ActiveSync pings.  If you receive no ActiveSync changes
// in this time period the ASPing will time and be restarted
//
// 15 Minutes is the max Domino servers seem to allow
// An Android client I looked at sets this to 17 and says longer settings can result in
// silently dropped connections
// So... you probably don't want to increase this much beyond 15 minutes. It has
// been set as high as 29 minutes in the past
// You don't want to set this too short or it will cause unnecessary network traffic
// and battery abuse
//
static const int kHeartBeatIntervalSeconds      = 15 * kSecondsPerMinute;

// Internationalize
//static NSString* kSyncCompleted                 = @"Sync Completed";

static NSString* k1Day                          = @"1 Day";
static NSString* k3Days                         = @"3 Days";
static NSString* k1Week                         = @"1 Week";
static NSString* k2Weeks                        = @"2 Weeks";
static NSString* k1Month                        = @"1 Month";
static NSString* k3Months                       = @"3 Months";
static NSString* k6Months                       = @"6 Months";
static NSString* kForever                       = @"Forever";
static NSString* kIncompleteTasks               = @"Incomplete Tasks";

static NSString* kMicrosoftServerActiveSync     = @"Microsoft-Server-ActiveSync";

static NSString* kGetAttachment                 = @"GetAttachment";

////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (void)startPings
{
    NSArray* anAccounts = [BaseAccount accounts];
    for(ASAccount* anAccount in anAccounts) {
        [anAccount startPing];
    }
}

+ (void)removePings
{
    // Shut down the ping
	//
	NSArray* anAccounts = [BaseAccount accounts];
	for(ASAccount* anAccount in anAccounts) {
		[ASPing removePingsForAccount:anAccount];
	}
}

- (void)removePing
{
    [ASPing removePingsForAccount:self];
}

- (BOOL)hasPing
{
    return [ASPing accountHasPing:self];
}

+ (void)loadExistingEmailAccounts
{
    FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - loadExistingEmailAccounts", [MemUtil freeMemoryAsMB]);
    
	int numAccounts = [BaseAccount accountsConfigured];
	BaseAccount* anAccount;
	for(int i = 0 ; i < numAccounts ; ++i) {
        anAccount = [BaseAccount createAccountFromSQL:i];
	}
    FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - loadExistingEmailAccounts", [MemUtil freeMemoryAsMB]);
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (void)initialSetup
{
    [self setAccountType:AccountTypeActiveSync];
    
    self.delegate           = NULL;
    self.traceDelegate      = NULL;
    
    self.deviceID           = [AppSettings deviceID];
    self.deviceModel        = [AppSettings deviceType];
    
    self.provisionPolicy    = [[ASProvisionPolicy alloc] init];

    self.policyKey          = @"";
    self.protocolVersion    = @"";
    self.syncKey            = @"0";
    ASVersion               = 0.0;
    self.versionSupport     = NULL;
    self.commands           = NULL;
    
    self.userAgent          = [NSString stringWithFormat:@"%@/%@", [AppSettings appName], [AppSettings version]];
    FXDebugLog(kFXLogActiveSync, @"User-Agent: %@", self.userAgent);
    
    self.heartBeatIntervalSeconds  = kHeartBeatIntervalSeconds;
    
    [self setActiveSyncPreferences:FALSE];
    
    // Start and check database engines
    //
    [EmailProcessor getSingleton];
    [EventEngine engine];
    [FXDatabase sharedManager];    
}

- (id)initWithAccountNum:(int)anAccountNum
{
    if (self = [super initWithAccountNum:anAccountNum]) {
        [self initialSetup];
        [self loadSettings];
    }
    
    return self;  
}

- (id)initWithName:(NSString*)aName
      userName:(NSString*)aUserName
          password:(NSString*)aPassword
          hostName:(NSString*)aHostName
              path:(NSString*)aPath
              port:(int)aPort
        encryption:(EEncryptionMethod)anEncryption
        authentication:(EAuthenticationMethod)anAuthentication
       folderNames:(NSArray *)aFolderNames
{
    if (self = [super initWithName:aName
                          userName:aUserName 
                          password:aPassword 
                          hostName:aHostName
                              path:aPath
                              port:aPort 
                        encryption:anEncryption 
                    authentication:anAuthentication 
                       folderNames:aFolderNames]) {
        [self initialSetup];
    }
    
    return self;
}

- (void)dealloc
{
    [self stopReachability];
    self.delegate       = nil;
    self.traceDelegate  = nil;
}

////////////////////////////////////////////////////////////////////////////////
// Load/Store
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Store

- (void)loadSettings
{
    @try {
        [super loadSettings];
        
        if(ASVersion >= kASVersion2007) {
            self.protocolVersion = [NSString stringWithFormat:@"%4.1f", ASVersion];
        }else{
            FXDebugLog(kFXLogActiveSync, @"ActiveSync not initialized or not supported");
            return;
        }
        
        [self setDeviceID:[AppSettings deviceID]];
        
        //[self autoDiscover];
        
        if([self.versionSupport count] == 0 || [self.commands count] == 0) {
            // If we have no AS version or command information do an AS OPTIONS request
            //
            [self options:nil]; // FIXME - should pass a delegate here and handle credential challenges
#define FEATURE_PROVISION
#ifdef FEATURE_PROVISION
        }else if([self.policyKey length] == 0) {
            // If we don't have a policy key do a provisioning handshake
            //
            [self provision];
#endif
        }else{
            // Everything is setup so go straight to a FolderSync which will start a Sync
            //
            if(!gIsFixTrace) {
                // This will do the first folderSync if network is enabled, followed by a ping if it completes
                // If device is offline it will wait for the reachability observer to indicate online, then do the folderSync and ping
                //
                [self startReachablility];
            }
        }
    }@catch (NSException* e) {
        [self logException:@"loadSettings" exception:e];
    }
}

- (void)commitSettings
{
    @try {
        [super commitSettings];
    }@catch (NSException* e) {
        [self logException:@"commitSettings" exception:e];
    }
}

- (void)testForGetAttachment:(NSArray*)aCommands
{
    self.hasGetAttachment = FALSE;
    for(NSString* aCommand in aCommands) {
        if([aCommand isEqualToString:kGetAttachment]) {
            self.hasGetAttachment = TRUE;
            break;
        }
    }
}

- (void)fromJSON:(NSString*)aString
{
    @try {
        NSDictionary* aDictionary = [aString JSONValue];
        
        ASVersion       = [self doubleFromDictionary:aDictionary key:@"ASVersion"];
        
        self.deviceID   = [aDictionary objectForKey:@"deviceID"];
        
        // Arrays
        self.versionSupport     = [aDictionary objectForKey:@"versionSupport"];
        self.commands           = [aDictionary objectForKey:@"commands"];
        [self testForGetAttachment:self.commands];

        [super fromDictionary:aDictionary];
    }@catch (NSException* e) {
        [self logException:@"fromJSON" exception:e];
    }
}

- (NSDictionary*)asDictionary
{
    NSMutableDictionary* aDictionary = [[NSMutableDictionary alloc] initWithCapacity:20];
    @try {
        [aDictionary setObject:[NSNumber numberWithDouble:ASVersion] forKey:@"ASVersion"];

        [super addString:self.deviceID   toDictionary:aDictionary key:@"deviceID"];

        // Arrays
        if(self.versionSupport) {
            [aDictionary setObject:self.versionSupport forKey:@"versionSupport"];
        }
        if(self.commands) {
            [aDictionary setObject:self.commands         forKey:@"commands"];
        }
        
        [super asDictionary:aDictionary];
    }@catch (NSException* e) {
        [self logException:@"asDictionary" exception:e];
    }
    return aDictionary;
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            if(![ASPing accountHasPing:self] && [WBXMLRequest operationQueueCount] == 0) {
                [self folderSync];
            }
            break;
        case NotReachable:
            [ASPing removePingsForAccount:self];
            break;
    }
}

- (void)startReachablility
{
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:self.reachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    if(self.delegate || [WBXMLRequest operationQueueCount] == 0) {
         [self updateReachability:self.reachability];
    }
}

// Virtual override
- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Preferences
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync

- (BOOL)autoSync
{
    return autoSync;
}

- (void)setAutoSync:(BOOL)anAutoSync
{
    if(anAutoSync != self.autoSync) {
        autoSync = anAutoSync;
#warning FIXME setAutoSync
        FXDebugLog(kFXLogFIXME, @"FIXME AutoSync setting changed to %d", self.autoSync);
    }
}

- (NSString*)stringForTimeFilter:(EFilterType)aFilterType
{
    NSString* aString;
    
    switch(aFilterType) {
        case kFilter1DayAgo:
            aString = k1Day; break;
        case kFilter3DaysAgo:
            aString = k3Days; break;
        case kFilter1WeekAgo:
            aString = k1Week; break;
        case kFilter2WeeksAgo:
            aString = k2Weeks; break;
        case kFilter1MonthAgo:
            aString = k1Month; break;
        case kFilter3MonthsAgo:
            aString = k3Months; break;
        case kFilter6MonthsAgo:
            aString = k6Months; break;
        case kFilterNone:
            aString = kForever; break;
        case kFilterIncompleteTasks:
            aString = kIncompleteTasks; break;
    }
    return aString;
}

- (NSArray*)filterTypeStrings
{
    NSArray* anArray = [NSArray arrayWithObjects:
                       kForever,
                       k1Day,
                       k3Days,
                       k1Week,
                       k2Weeks,
                       k1Month,
                       k3Months,
                       k6Months,
                       nil];
    return anArray;
}

- (EFilterType)emailFilterType
{
	return kFilter1MonthAgo;
//    return emailFilterType;
}

- (void)setEmailTimeFilter:(NSString*)aTimeFilterString shouldResync:(BOOL)shouldResync
{
//    EFilterType aFilterType = [self filterTypeForString:aTimeFilterString];
    EFilterType aFilterType = kFilter1MonthAgo;
    if(aFilterType != emailFilterType) {
        if(shouldResync) {
#warning FIXME Need to resync and or remove old email in some cases
            FXDebugLog(kFXLogActiveSync, @"Email Time filter setting changed from %@ to %@",
                     [self stringForTimeFilter:emailFilterType],
                     [self stringForTimeFilter:aFilterType]);
        }
        emailFilterType = aFilterType;
    }
}

- (EFilterType)calendarFilterType
{
    return calendarFilterType;
}

- (void)setCalendarTimeFilter:(NSString*)aTimeFilterString shouldResync:(BOOL)shouldResync
{
    EFilterType aFilterType = [self filterTypeForString:aTimeFilterString];
    if(aFilterType != calendarFilterType) {
        if(shouldResync) {
#warning FIXME Need to resync and or remove old email in some cases
            FXDebugLog(kFXLogActiveSync, @"Email Time filter setting changed from %@ to %@",
                     [self stringForTimeFilter:calendarFilterType],
                     [self stringForTimeFilter:aFilterType]);
        }
        calendarFilterType = aFilterType;
    }
}

- (EFilterType)filterTypeForString:(NSString*)aString
{
    EFilterType aFilterType;
    
    if([aString isEqualToString:k1Day]) {
        aFilterType = kFilter1DayAgo;
    }else if([aString isEqualToString:k3Days]) {
        aFilterType = kFilter3DaysAgo;
    }else if([aString isEqualToString:k1Week]) {
        aFilterType = kFilter1WeekAgo;
    }else if([aString isEqualToString:k2Weeks]) {
        aFilterType = kFilter2WeeksAgo;
    }else if([aString isEqualToString:k1Month]) {
        aFilterType = kFilter1MonthAgo;
    }else if([aString isEqualToString:k3Months]) {
        aFilterType = kFilter3MonthsAgo;
    }else if([aString isEqualToString:k6Months]) {
        aFilterType = kFilter6MonthsAgo;
    }else if([aString isEqualToString:kForever]) {
        aFilterType = kFilterNone;
    }else{
        aFilterType = kFilterNone;
    }
    return aFilterType;
}

// Override
- (BOOL)isFilterTypeSupported:(EFilterType)aFilterType folderType:(EFolderType)aFolderType
{
    return TRUE;
}

- (EBodyType)bodyType
{
    return bodyType;
}

- (void)setBodyType:(NSString*)aBodyTypeString
{
    EBodyType aBodyType;
    if([aBodyTypeString isEqualToString:@"MIME"]) {
        aBodyType = kBodyTypeMIME;
    }else if([aBodyTypeString isEqualToString:@"HTML"]) {
        aBodyType = kBodyTypeHTML;
    }else if([aBodyTypeString isEqualToString:@"RTF"]) {
        aBodyType = kBodyTypeRTF;
    }else if([aBodyTypeString isEqualToString:@"Plain Text"]) {
        aBodyType = kBodyTypePlainText;
    }else{
        aBodyType = kBodyTypeHTML;
//        aBodyType = kBodyTypeMIME;
    }
    if(bodyType != aBodyType) {
#warning FIXME Body format changed
        FXDebugLog(kFXLogFIXME, @"FIXME body format setting changed");
        bodyType = aBodyType;
    }
}

- (void)setActiveSyncPreferences:(BOOL)aShouldResync
{
    BOOL anAutoSync                     = [AppSettings autoSync];
    [self setAutoSync:anAutoSync];
    
    NSString* anEmailTimeFilterString   = [AppSettings emailTimeFilter];
    [self setEmailTimeFilter:anEmailTimeFilterString shouldResync:aShouldResync];
    
    NSString* aCalendarTimeFilterString = [AppSettings calendarTimeFilter];
    [self setCalendarTimeFilter:aCalendarTimeFilterString shouldResync:aShouldResync];
    
    NSString* aBodyFormat               = [AppSettings bodyFormat];
    [self setBodyType:aBodyFormat];
}

////////////////////////////////////////////////////////////////////////////////
// ASAutoDiscover 
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASAutoDiscover

#if 0 // Not Implemented
- (void)autoDiscover
{
#warning FIXME autoDiscover
    @try {
        [ASAutoDiscover sendRequest:self];
    }@catch (NSException* e) {
        [self logException:@"auto discover" exception:e];
    }
}
#endif

////////////////////////////////////////////////////////////////////////////////
// ASSettings
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASSettings

- (void)settings
{
    @try {
        [ASSettings sendGetUserInfo:self displayErrors:YES];
    }@catch (NSException* e) {
        [self logException:@"settings" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// ASOptions
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASOptions

- (void)options:(NSObject<ASOptionsDelegate>*)aDelegate
{
    @try {
        // This starts an ActiveSync Options command to get protocol version and commands
        //
        // When complete ASOptions will optionsSet to start a folderSync. When that is complete foldersSynced
        // will be called from ASFolderSync which will either configure the account if its new
        // or sync folders if its an existing account
        //
        // foldersSynced will then do an email sync at which point everything is up to date
        //
        if(self.delegate) {
            [self.delegate progressMessage:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"CONTACTING_%@((hostname))", @"ASAccount", @"Progress View Message while contacting host"), self.hostName]];
        }
        [ASOptions sendRequest:self delegate:aDelegate];
    }@catch (NSException* e) {
        [self logException:@"options" exception:e];
    }
}

- (void)optionsSet
{
    if(self.delegate) {
        if(ASVersion >= kASVersion2007) {
            [self.delegate progressMessage:@"ActiveSync supported"];
        }else if(ASVersion == 0.0) {
            [self.delegate configureAbortedWithMessage:[NSString stringWithFormat:@"%@ doesn't support ActiveSync", self.hostName]];
            return;
        }else if(ASVersion > 0.0f) {
            [self.delegate configureAbortedWithMessage:[NSString stringWithFormat:@"ActiveSync version unsupported: %4.1f", ASVersion]];
            return;
        }
    }
    
    [self testForGetAttachment:self.commands];

    //FXDebugLog(kFXLogActiveSync, @"optionsSet: %@", self);
    
    // Start provisioning handshake
    //
#ifdef FEATURE_PROVISION
    [self provision];
#else
    [self startReachablility];
#endif
}

-(void)optionsAborted:(NSString*)aMesg
{
    [self.delegate configureAbortedWithMessage:aMesg];
}

////////////////////////////////////////////////////////////////////////////////
// ASProvision
////////////////////////////////////////////////////////////////////////////////
#pragma mark AS Provision

- (void)needsProvisioning:(WBXMLRequest*)aFailedRequest
{
    self.policyKey = @"0";
    self.requestToResend = aFailedRequest;
    NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(provision) object:nil];
    [[NSOperationQueue mainQueue] addOperation:op];
}

- (void)provision
{
    @try {
        provisionFailed = false;
        
        if(self.delegate) {
            [self.delegate progressMessage:FXLLocalizedStringFromTable(@"PROVISIONING", @"ASAccount", @"Progress View Message while provisioning")];
        }
        [ASProvision sendRequest:self 
                 provisionAction:kProvisionRequestPolicy 
                       policyKey:@"" 
                 provisionPolicy:self.provisionPolicy];
    }@catch (NSException* e) {
        [self logException:@"provision" exception:e];
    }
}

- (void)setProvisionReceived:(NSString*)aPolicyKey
{
    self.policyKey = aPolicyKey;
    [self commitPolicyKey];
}

- (void)setProvisionAcknowledged:(NSString *)aPolicyKey
{
    if(![aPolicyKey isEqualToString:self.policyKey]) {
        [self setProvisionReceived:aPolicyKey];
    }
    if(self.delegate) {
        [self.delegate progressMessage:FXLLocalizedStringFromTable(@"PROVISIONED", @"ASAccount", @"Progress View Message when provisioned")];
    }

    if(self.requestToResend) {
        // Policy sync changed and blocked some ActiveSync request, we need to resend it now that the
        // provision policy has been reestablished
        HttpRequest* anHttpRequest = self.requestToResend;
        if([self.policyKey length] > 0) {
            NSMutableURLRequest* aURLRequest = [anHttpRequest URLrequest];
            NSMutableDictionary* aHeaders = [[aURLRequest allHTTPHeaderFields] mutableCopy];
            //FXDebugLog(kFXLogActiveSync, @"revised header: %@", [aURLRequest allHTTPHeaderFields]);
            [aHeaders setValue:self.policyKey forKey:@"X-MS-PolicyKey"];
            [aURLRequest setAllHTTPHeaderFields:aHeaders];
            //FXDebugLog(kFXLogActiveSync, @"revised header: %@", [aURLRequest allHTTPHeaderFields]);
            [self send:[anHttpRequest URLrequest] httpRequest:anHttpRequest];
        }else{
            FXDebugLog(kFXLogActiveSync, @"Reprovision failed: %@", self);
        }
        self.requestToResend = nil;
    }else{
        // First time policy provision, we want to do the first folder sync now
        [self startReachablility];
    }
}

- (void)setProvisionFailed
{
    FXDebugLog(kFXLogFIXME, @"FIXME Provisioning request failed");
    provisionFailed = true;
}

////////////////////////////////////////////////////////////////////////////////
// ASFolderSync
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolderSync

- (void)folderSync
{
    @try {
        if(!provisionFailed) {
            if([self.emailAddress length] == 0) {
                [self settings];
            }
            if(self.delegate) {
                [self.delegate progressMessage:FXLLocalizedStringFromTable(@"FOLDERS_SYNCING", @"ASAccount", @"Progress View Message while folders are syncing")];
            }else{
                [self setProgress:FXLLocalizedStringFromTable(@"FOLDERS_SYNCING", @"ASAccount", nil)];
            }
            [ASPing removePingsForAccount:self];
            //FXDebugLog(kFXLogActiveSync, @"folderSync: %@", self);
            ASFolderSync * aFolderSync = [ASFolderSync queueRequest:self displayErrors:NO];
            aFolderSync.delegate = self;
        }else{
            FXDebugLog(kFXLogActiveSync, @"Cant FolderSync provisioning failed");
        }
    }@catch (NSException* e) {
        [self logException:@"foldersSync" exception:e];
    }
}

- (BOOL)initialSync
{
    BOOL isInitialSyncing = NO;
    
    self.initialSyncFolders = [NSMutableArray arrayWithArray:[self foldersToSync]];
    
    // If resuming an interruped intial sync we need to remove any folders that are already synced
    //
    NSUInteger aCount = self.initialSyncFolders.count;
    for(int i = aCount-1 ; i >= 0 ; --i) {
        ASFolder* aFolder = [self.initialSyncFolders objectAtIndex:i];
        if([aFolder isInitialSynced]) {
            [self.initialSyncFolders removeObjectAtIndex:i];
        }
    }
    
    if(self.initialSyncFolders.count > 0) {
        self.isInitialSynced = NO;
        [self initialSyncNextFolder];
        isInitialSyncing = YES;
    }else{
        self.isInitialSynced = YES;
    }
    
    return isInitialSyncing;
}

- (void)initialSyncFolder:(ASFolder*)aFolder
{
    self.initialSyncFolders = [NSMutableArray arrayWithObject:aFolder];
    [self initialSyncNextFolder];
}

////////////////////////////////////////////////////////////////////////////////
// ASFolderSyncDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASFolderSyncDelegate

- (void)setFolderSyncKey:(NSString*)aSyncKey
{
    self.syncKey = aSyncKey;
    [self commitSyncKey];
}

- (void)needsFolderResync
{
    [self setFolderSyncKey:kInitialSyncKey];
    ASFolderSync * aFolderSync = [ASFolderSync queueRequest:self displayErrors:NO];
    aFolderSync.delegate = self;
}

- (void)folderSyncSuccess:(NSMutableArray*)aFolders
{
    @try {
        if(self.delegate) {
            // If there is a configure delegate we are assuming this is a first sync
            // FIXME -This will probably not be true if we go through the folder settings view
            //
            [self.delegate progressMessage:FXLLocalizedStringFromTable(@"FOLDERS_SYNCED", @"ASAccount", @"Progress View Message when folders are synced")];
            if(self.delegate) {
                [self.delegate configureSuccess:folders];
                self.delegate = NULL;
            }
        }else{
            [self setProgress:FXLLocalizedStringFromTable(@"FOLDERS_SYNCED", @"ASAccount", nil) eraseInSeconds:2];
            // Initial sync any folders that need it
            //
            if(![self initialSync]) {
                // No folders to initial sync, so start the ping
                //
                [self ping:[self foldersToSync]];
            }
        }

    }@catch (NSException* e) {
        [self logException:@"foldersSynced" exception:e];
    }
}

- (void)folderSyncFailed:(EHttpStatusCode)aStatusCode folderSyncStatus:(EFolderSyncStatus)aFolderSyncStatus
{
    if(self.delegate) {
        [self.delegate configureFailed:aStatusCode];
        self.delegate = NULL;
    }else{
        [self setProgress:FXLLocalizedStringFromTable(@"FOLDER_SYNC_FAILED", @"ASAccount", @"Progress View Message after folder sync failed")];
    }
}

- (void)folderAdded:(ASFolder*)aFolder
{
    //FXDebugLog(kFXLogActiveSync, @"ASAccount folderAdded: %@", aFolder);
    [self addFolder:aFolder];

    for(NSObject<AccountDelegate>* aDelegate in accountDelegates) {
        [aDelegate performSelectorOnMainThread:@selector(folderAdded:) withObject:aFolder waitUntilDone:FALSE];
    }
    
    if(!self.delegate) {
        // Don't do commit if there is a config delegate, the account isn't created yet
        //
        [aFolder commit];
    }
}

- (void)folderDeleted:(NSString*)aServerId
{
    ASFolder* aFolder = (ASFolder*)[self folderForServerId:aServerId];
    if(aFolder) {
        // FXDebugLog(kFXLogActiveSync, @"ASAccount folderDeleted: %@", aFolder);
        [aFolder setIsDeleted:TRUE];
        NSUInteger aFolderIndex = [folders indexOfObject:aFolder];
        if(aFolderIndex != NSNotFound) {
            [folders removeObjectAtIndex:aFolderIndex];
        }
        
        for(NSObject<AccountDelegate>* aDelegate in accountDelegates) {
            [aDelegate performSelectorOnMainThread:@selector(folderDeleted:) withObject:aFolder waitUntilDone:FALSE];
        }
        
        [aFolder commit];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME deleted folder not found: %@", aServerId);
    }
}

- (void)folderUpdated:(ASFolder*)aFolder
{
    FXDebugLog(kFXLogActiveSync, @"ASAccount folderUpdated: %@", aFolder);
    
    for(NSObject<AccountDelegate>* aDelegate in accountDelegates) {
        [aDelegate performSelectorOnMainThread:@selector(folderUpdated:) withObject:aFolder waitUntilDone:FALSE];
    }
    
    [aFolder commit];
}

////////////////////////////////////////////////////////////////////////////////
// ASAccount subclass virtuals
////////////////////////////////////////////////////////////////////////////////
#pragma mark  ASAccount subclass virtuals

// Required
- (NSArray*)foldersToSync
{
    NSAssert(0, @"FIXME ASAccount foldersToSync");
    return [NSArray array];
}

// Optional
- (NSArray*)specialFoldersToSync
{
    return [NSArray array];
}

////////////////////////////////////////////////////////////////////////////////
// ASAccount subclass helpers
////////////////////////////////////////////////////////////////////////////////
#pragma mark  ASAccount subclass helpers

- (BaseFolder*)addFolderToArray:(NSMutableArray*)aFolders name:(NSString*)aName
{    
    BaseFolder* aFolder = [self folderForName:aName];
    if(aFolder) {
        [aFolders addObject:aFolder];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ not found to sync", aName);
    }
    
    return aFolder;
}

- (BaseFolder*)addFolderToArray:(NSMutableArray*)aFolders serverId:(NSString*)aServerId
{
    BaseFolder* aFolder = [self folderForServerId:aServerId];
    if(aFolder) {
        [aFolders addObject:aFolder];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ not found to sync", aServerId);
    }
    
    return aFolder;
}

- (void)addFoldersToArray:(NSMutableArray*)aFolders ofFolderType:(EFolderType)aFolderType
{
    [aFolders addObjectsFromArray:[super foldersForFolderType:aFolderType]];
}

////////////////////////////////////////////////////////////////////////////////
// ASSync
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASSync

- (void)initialSyncNextFolder
{
    if(self.initialSyncFolders.count > 0) {
        // Get next folder from initial sync array
        //
        ASFolder* aFolder = [self.initialSyncFolders objectAtIndex:0];
        if(![aFolder isInitialSync] && ![aFolder isInitialSynced]) {
            // Update progress
            //
            NSString* aString;
            if(gIsFixTrace) {
                aString = [NSMutableString stringWithFormat:FXLLocalizedStringFromTable(@"SYNCING_%@((folderIdOrName))...", @"ASAccount", @"Progress View Message while syncing specific folder"), aFolder.uid];
            }else{
                aString = [NSMutableString stringWithFormat:FXLLocalizedStringFromTable(@"SYNCING_%@((folderIdOrName))...", @"ASAccount", @"Progress View Message while syncing specific folder"), aFolder.displayName];
            }
            [self setProgress:aString];
            
            // Start next folder syncing
            //
            [aFolder setSyncKey:kInitialSyncKey syncDate:nil];
            [[[ASSyncFolders alloc] initWithAccount:self
                                        syncFolders:[NSArray arrayWithObject:aFolder]
                                        pingFolders:nil
                                      bodyFetchType:kBodyFetchPreview] queue];
            
            // Tell folder and its clients this folder is syncing
            //
            [aFolder folderSyncing];
        }
    }else{
        self.isInitialSynced = YES;
        
        // No more folders to initial sync so start ping
        //
        if(self.traceDelegate) {
            [self.traceDelegate syncComplete];
        }
    }
}

- (void)resetForInitialSync
{
#if 0
    // This was used in the very old fixTrace standalone app, called from TraceViewController
    //
    // It pretty much doesn't work any more, at least not in SafeZone or fixMail so
    // disabling it so no one tries to use it
    //
    [DBaseEngine deleteAccount];
    [GlobalDBFunctions closeAll];
    [GlobalDBFunctions deleteAll];

    for(ASFolder* aFolder in folders) {
        [aFolder setIsInitialSynced:FALSE];
        [aFolder setSyncKey:kInitialSyncKey syncDate:nil];
        [aFolder commit];
    }
    
    NSArray* anAccounts = [BaseAccount accounts];
    for(BaseAccount* anAccount in anAccounts) {
        [anAccount commitSettings];
    }
#endif
}

////////////////////////////////////////////////////////////////////////////////
// Progress Reporting
////////////////////////////////////////////////////////////////////////////////
#pragma mark Progress Reporting

- (void)setProgress:(NSString*)aProgressString
{
    FXDebugLog(kFXLogActiveSync, @"ASAccount progress: %@", aProgressString);
    if(progressMessageTimer) {
        [progressMessageTimer invalidate];
        progressMessageTimer = nil;
    }
    [[SyncManager getSingleton] reportProgressString:aProgressString];
}

- (void)setProgress:(NSString*)aProgressString eraseInSeconds:(int)aSeconds
{
    progressMessageTimer = [NSTimer scheduledTimerWithTimeInterval:aSeconds
                                                            target:self
                                                          selector:@selector(clearProgress)
                                                          userInfo:nil
                                                           repeats:NO];
    [self setProgress:aProgressString];
}

- (void)clearProgress
{
    progressMessageTimer = nil;
    [[SyncManager getSingleton] reportProgressString:@""];
}

////////////////////////////////////////////////////////////////////////////////
// ASSyncDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASSyncDelegate

- (void)syncMoreAvailable:(NSArray*)aMoreAvailableFolders pingFolders:(NSArray *)aPingFolders
{
    [[[ASSyncFolders alloc] initWithAccount:self syncFolders:aMoreAvailableFolders
                                pingFolders:aPingFolders
                              bodyFetchType:kBodyFetchPreview] queue];
}

- (void)_setInitialSyncedFolders:(NSArray*)aSyncFolders
{
    if(aSyncFolders.count == 1) {
        ASFolder* aFolder = [aSyncFolders objectAtIndex:0];
        [aFolder folderSynced];
    }else{
        FXDebugLog(kFXLogActiveSync, @"multi folder initial sync shouldn't happen");
    }
    for(ASFolder* aFolder in aSyncFolders) {
        BOOL isRemoved = NO;
        if([self.initialSyncFolders containsObject:aFolder]) {
            isRemoved = YES;
            [self.initialSyncFolders removeObject:aFolder];
        }else{
            for(ASFolder* anInitialFolder in self.initialSyncFolders) {
                if([anInitialFolder.uid isEqualToString:aFolder.uid]) {
                    FXDebugLog(kFXLogActiveSync, @"_setInitialSyncedFolders inconsistency, two copies of folder: %@", [aFolder displayName]);
                    isRemoved = YES;
                    [self.initialSyncFolders removeObject:anInitialFolder];
                    break;
                }
            }
        }
        if(!isRemoved) {
            FXDebugLog(kFXLogActiveSync, @"remove from initialSyncFolders failed: %@", [aFolder displayName]);
            for(ASFolder* anInitialFolder in self.initialSyncFolders) {
                FXDebugLog(kFXLogActiveSync, @"\tinitialSyncFolders: %@", anInitialFolder.displayName, anInitialFolder.uid);
            }
        }
    }
    [self initialSyncNextFolder];
}

- (void)syncSuccess:(NSArray *)aSyncFolders isPinging:(BOOL)isPinging
{
    if(aSyncFolders.count == 1) {
        ASFolder* aFolder = [aSyncFolders objectAtIndex:0];
        
        // Handle initial sync if its in progress on this folder
        //
        if(![aFolder isInitialSynced]) {
            if(![aFolder isInitialSyncInterim]) {
                // ActiveSync 12.x seems to return a response to the initial sync which doesn't have the more
                // available flag set, though there is more available and the initial sync hasn't even start yet
                // so we need to just send another sync request to actually kick start the initial sync
                //
                [aFolder setIsInitialSyncInterim:TRUE];
                [[[ASSyncFolders alloc] initWithAccount:self
                                            syncFolders:aSyncFolders
                                            pingFolders:nil
                                          bodyFetchType:kBodyFetchPreview] queue];
                return;
            }else{
                [aFolder setIsInitialSyncInterim:FALSE];
            }
            
            // Do setup specific to folder type
            //
            [aFolder setup];
        }
        
        // Sync complete progress message
        //
        NSString* aString;
        if(gIsFixTrace) {
            aString = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"SYNC_COMPLETED", @"ASAccount", @"Progress view message when sync is completed"), aFolder.uid];
        }else{
            aString = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"SYNC_COMPLETED", @"ASAccount", nil), aFolder.displayName];
        }
        [self setProgress:aString];
    }else{
        // Multi folder sync completed, usually because a new or changed email is in multiple folders
        //
        [self setProgress:FXLLocalizedStringFromTable(@"SYNC_COMPLETED", @"ASAccount", nil)];
        if(self.traceDelegate) {
            [self.traceDelegate syncComplete];
        }
    }
    
    if(self.initialSyncFolders.count > 0) {
        // We are initial syncing and there are still more folders to do
        //
        [self _setInitialSyncedFolders:aSyncFolders];
    }else{
        // You usually get here when all the folders are initial synced and its time to start pinging
        //
        progressMessageTimer = [NSTimer scheduledTimerWithTimeInterval:2
                                                                target:self 
                                                              selector:@selector(clearProgress) 
                                                              userInfo:nil 
                                                               repeats:NO];
        self.isInitialSynced = TRUE;
        if(self.traceDelegate) {
            [self.traceDelegate syncComplete];
        }
    }
}

- (void)syncNotUpdating:(NSArray*)aSyncFolders
{
    if(self.initialSyncFolders.count > 0) {
        // If a folder has nothing in it, Gmail especially leaves the sync key at 1 and it
        // will never change, so stop trying to finish the initial sync
        //
        if(aSyncFolders.count == 1) {
            ASFolder* aFolder = [aSyncFolders objectAtIndex:0];
            NSString* aString;
            if(gIsFixTrace) {
                aString = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"EMPTY_%@((folderIdOrName))", @"ASAccount", @"Progress View Message while syncing specific folder"), aFolder.uid];
            }else{
                aString = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"EMPTY_%@((folderIdOrName))", @"ASAccount", nil), aFolder.displayName];
            }
            [self setProgress:aString];
        }
        [self _setInitialSyncedFolders:aSyncFolders];
    }
}

- (void)syncFailed:(NSArray *)aSyncFolders
        httpStatus:(EHttpStatusCode)aStatusCode
        syncStatus:(ESyncStatus)aSyncStatus
{
    FXDebugLog(kFXLogActiveSync, @"Sync failed: HTTP=%d AS=%d: %@", aStatusCode, aSyncStatus, aSyncFolders);
    
    for(ASFolder* aFolder in aSyncFolders) {
        switch(aStatusCode) {
            case kHttpOK:  // Error is ActiveSync, not HTTP
                switch(aSyncStatus) {
                    case kSyncStatusOK:
                        break;  // Should never happen
                    case kSyncStatusInvalidSyncKey:
                    case kSyncStatusProtocolError:
                    case kSyncStatusConversionError:
                    case kSyncStatusConflict:
                    case kSyncStatusIncompleteCommand:
                        // Programming errors not recoverable, just display it
                        [ASFolder displaySyncStatus:aSyncStatus];
                        break;
                    case kSyncStatusServerError:
                    case kSyncStatusRetry:
                        // Retry sync, if it continues do a new sync with "0" key
                        break;
                    case kSyncStatusObjectNotFound:
                        if(aSyncFolders.count == 1) {
                            ASFolder* aFolder = [aSyncFolders objectAtIndex:0];
                            [self folderDeleted:aFolder.uid];
                        }else{
                            [ASFolder displaySyncStatus:aSyncStatus];
                        }
                        break;
                    case kSyncStatusCommandNotComplete:
                        // Free up space in user's mailbox and retry
                        break;
                    case kSyncStatusFolderHierarchyChanged:
                        // Do a FolderSync and then retry
                        break;
                    case kSyncStatusInvalidWaitOrHeartbeat:
                        // ￼Update the Wait element value according to the Limit element and then resend the Sync command request.
                        break;
                    case kSyncStatusInvalidCommand:
                        // Notify the user and synchronize fewer folders within one request.
                        break;
                    default:
                        [ASFolder displaySyncStatus:aSyncStatus];
                        break;
                }
                break; 
            case kHttpForbidden:
                // User has no calendar on Gmail is a common cause for this
                [aFolder setIsSyncable:FALSE];
                [aFolder commit];
                break;
            case kHttpNeedsProvisioning:
                FXDebugLog(kFXLogFIXME, @"kHttpNeedsProvisioning");
                break;
            default:
#warning FIXME if folder sync failed should I mark it as not syncable?
                //[aFolder setIsSyncable:FALSE];
                //[aFolder commit];
                break;
        }
    }
    
    if(self.initialSyncFolders.count > 0) {
        [self.initialSyncFolders removeObjectsInArray:aSyncFolders];
        [self initialSyncNextFolder];
    }
}

////////////////////////////////////////////////////////////////////////////////
// ASPing
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASPing

- (void)startPing
{
    if(![ASPing accountHasPing:self]) {
        NSArray* aFolders = [self foldersToSync];
        for(ASFolder* aFolder in aFolders) {
            if(!aFolder.isInitialSynced) {
                // We are still initial syncing folders, dont start the ping
                //
                return;
            }
        }
        [self ping:aFolders];
    }
}

- (void)ping:(NSArray*)aFolders
{
    @try {
        if(![ASPing accountHasPing:self]) {
            if(aFolders.count > 0) {
                [ASPing sendPing:self folders:aFolders];
            }else{
                FXDebugLog(kFXLogActiveSync, @"No folders to ping");
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"ASAccount ping already running");
        }
    }@catch (NSException* e) {
        [self logException:@"ping" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// URL Request
////////////////////////////////////////////////////////////////////////////////
#pragma mark URL Request

- (NSString*)_ASRequestWithCommand:(NSString*)aCommand 
                     params:(NSMutableDictionary**)returnParams 
                    headers:(NSMutableDictionary**)returnHeaders
{
    // Build the core part of an ActiveSync request
    //
    NSString* aUrlString;
    if(self.path.length > 0) {
        aUrlString = [NSString stringWithFormat:@"%@:%d/%@/%@", self.hostName, self.port, self.path, kMicrosoftServerActiveSync];
    }else{
        aUrlString = [NSString stringWithFormat:@"%@:%d/%@", self.hostName, self.port, kMicrosoftServerActiveSync];
    }
    //NSString* aUrlString = [NSString stringWithFormat:@"%@:%d/servlet/traveler/Microsoft-Server-ActiveSync", self.hostName, self.port];
    
    NSString* aUserName;
    if ([self.pcc length] > 0) {
        aUserName = [self.userName stringByAppendingString:self.pcc];
    } else {
        aUserName = self.userName;
    }
    if ([self.domain length] > 0) {
        aUserName = [NSString stringWithFormat:@"%@\\%@", self.domain, aUserName];
    }
    
    // FIXME - Should probably Base64 encode this per MS-HTTP doc 2.2.1.1.1.1
    NSMutableDictionary* aParams = [NSMutableDictionary dictionaryWithCapacity:10];
    [aParams setObject:aCommand             forKey:@"Cmd"];
    [aParams setObject:aUserName            forKey:@"User"];
    [aParams setObject:self.deviceID        forKey:@"DeviceId"];
    [aParams setObject:self.deviceModel     forKey:@"DeviceType"];
    
    NSMutableDictionary* aHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
    switch(self.authentication) {
        case kAuthenticationBasic:
        {
            NSString* authStr	= [NSString stringWithFormat:@"%@:%@", aUserName, self.password];
            NSData*   authData	= [authStr dataUsingEncoding:NSASCIIStringEncoding];
            NSString* auth      = [NSString stringWithFormat:@"Basic %@", [authData base64EncodedString]];
            [aHeaders setObject:auth forKey:@"Authorization"];
            break;
        }
        case kAuthenticationToken:
            FXDebugLog(kFXLogFIXME, @"FIXME AS kAuthenticationToken");
            break;
        case kAuthenticationCertificate:
            FXDebugLog(kFXLogFIXME, @"FIXME AS kAuthenticationCertificate");
            break;
    }

    if([self.policyKey length] > 0) {
        [aHeaders setObject:self.policyKey    forKey:@"X-MS-PolicyKey"];
    }
    [aHeaders setObject:self.protocolVersion    forKey:@"MS-ASProtocolVersion"];
    [aHeaders setObject:@"keep-alive"           forKey:@"Connection"];
    [aHeaders setObject:self.userAgent          forKey:@"User-Agent"];
    
    *returnParams   = aParams;
    *returnHeaders  = aHeaders;
    return aUrlString;
}

- (NSMutableURLRequest*)createOptionsRequest:(NSString*)anASCommand
{
    // ASOptions uses this
    //
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];

    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https" 
                                                         method:@"OPTIONS" 
                                                            url:urlString
                                                        headers:headers 
                                                         params:params 
                                                           body:NULL
                                                timeoutInterval:kHTTPTimeout];
    return aURLRequest;
}

- (NSMutableURLRequest*)createAttachmentRequest:(NSString*)anASCommand
{
    // ASOptions uses this
    //
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];
    
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https"
                                                         method:@"POST"
                                                            url:urlString
                                                        headers:headers
                                                         params:params
                                                           body:NULL
                                                timeoutInterval:kHTTPTimeout];
    return aURLRequest;
}

- (NSMutableURLRequest*)createAutoDiscoverRequest:(NSString*)aUrlString xml:(NSString*)anXML
{
    // AutoDiscover uses this
    //
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https" 
                                                         method:@"POST" 
                                                            url:aUrlString
                                                        headers:NULL 
                                                         params:NULL 
                                                           body:[NSData dataWithBytes:[anXML UTF8String] length:[anXML length]]
                                                    timeoutInterval:kHTTPTimeout];
    return aURLRequest;
}

- (NSMutableURLRequest*)createMailRequestWithCommand:(NSString*)anASCommand mime:(NSData*)aMIMEData
{
    // ASCompose (i.e. SendMail, SmartForward, SmartReply) uses this
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];
    
    [headers setObject:@"message/rfc822"    forKey:@"Content-Type"];
	[params setObject:@"T" forKey:@"SaveInSent"];
    
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https" 
                                                         method:@"POST" 
                                                            url:urlString
                                                        headers:headers 
                                                         params:params 
                                                           body:aMIMEData
                                                    timeoutInterval:kHTTPTimeout];
    
    return aURLRequest;
}

- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand wbxml:(NSData*)aWbxml
{
    // Most standard AS commands use this request
    //
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];
    
    [headers setObject:kContentTypeWBXML    forKey:@"Content-Type"];
    
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https" 
                                                         method:@"POST" 
                                                            url:urlString
                                                        headers:headers 
                                                         params:params 
                                                           body:aWbxml
                                                timeoutInterval:kHTTPTimeout];
    
    return aURLRequest;
}

- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand multipart:(BOOL)aWantMultipart  wbxml:(NSData*)aWbxml
{
    // ItemOperations uses this request
    //
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];
    
    [headers setObject:kContentTypeWBXML    forKey:@"Content-Type"];

   // [headers setObject:@"application/vnd.ms-sync"    forKey:@"Content-Type"];
    if(aWantMultipart) {
        [headers setObject:@"T"    forKey:@"MS-ASAcceptMultiPart"];
    }else{
        [headers setObject:@"F"    forKey:@"MS-ASAcceptMultiPart"];
    }
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https" 
                                                         method:@"POST" 
                                                            url:urlString
                                                        headers:headers 
                                                         params:params 
                                                           body:aWbxml
                                                timeoutInterval:kHTTPTimeout];

    return aURLRequest;
}

- (NSMutableURLRequest*)createURLRequestWithCommand:(NSString*)anASCommand attachment:(NSString*)anAttachment
{
    // ItemOperations uses this request
    //
    NSMutableDictionary* params;
    NSMutableDictionary* headers;
    NSString* urlString = [self _ASRequestWithCommand:anASCommand params:&params headers:&headers];
    [params setObject:anAttachment forKey:@"AttachmentName"];
        
    HttpEngine* engine = [HttpEngine engine];
    NSMutableURLRequest* aURLRequest = [engine createURLRequest:@"https"
                                                         method:@"POST"
                                                            url:urlString
                                                        headers:headers
                                                         params:params
                                                           body:nil
                                                timeoutInterval:kHTTPTimeout];
    
    return aURLRequest;
}

- (void)send:(NSMutableURLRequest*)aURLRequest httpRequest:(HttpRequest*)anHttpRequest
{
    [anHttpRequest setURLRequest:aURLRequest];
    HttpEngine* anEngine = [HttpEngine engine];
    [anEngine sendRequest:aURLRequest httpRequest:anHttpRequest];
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (NSString*)getClientID
{
    // Return a unique ID for sending mail
    //
    // FIXME - We probably should just run a counter here this is overkill
    //
    NSString* aClientID = NULL;
    
    CFUUIDRef uuidObj = CFUUIDCreate(nil);
    aClientID = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj));
    CFRelease(uuidObj);
    
    aClientID =  [aClientID stringByReplacingOccurrencesOfString:@"-" withString:@""];

    return aClientID;
}

////////////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setVersions:(NSArray*)aVersions
{    
    [self setVersionSupport:aVersions];
    
    ASVersion = 0.0;
    NSUInteger aCount = [aVersions count];
    for(NSUInteger i = 0 ; i < aCount ; ++i) {
        NSString* aVersion = [aVersions objectAtIndex:i];
        double aVersionDouble = [aVersion doubleValue];
        if(aVersionDouble > ASVersion) {
            ASVersion = aVersionDouble;
        }
    }
    
    if(ASVersion > kASVersion2010SP1) {
        NSString* anASVersionWarning = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"ACTIVESYNC_VERSION_%f_UNTESTED", @"ErrorAlert", @"Error alert view message - ActiveSync version X untested"), ASVersion];
        [self handleError:anASVersionWarning];
    }else if(ASVersion < kASVersion2007) {
        [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"ACTIVESYNC_VERSION_%4.1f_WILL_NOT_WORK", @"ErrorAlert", @"Error alert view message - ActiveSync version 4.1X will not work"), ASVersion]];
    }
    
    FXDebugLog(kFXLogActiveSync, @"ActiveSync protocol version: %4.1f", ASVersion);


    [self setProtocolVersion:[NSString stringWithFormat:@"%4.1f", ASVersion]];
}

////////////////////////////////////////////////////////////////////////////////
// Factory Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory Overrides

- (BaseContact*)createContact
{
    return (BaseContact*)[[ASContact alloc] init];
}

- (Event*)createEvent
{
    return (Event*)[[Event alloc] init];
}

- (ASEmail*)createEmailForFolder:(BaseFolder*)aFolder
{
    return [[ASEmail alloc] initWithFolder:(ASFolder*)aFolder];
}

- (BaseFolder*)createFolderForFolderType:(EFolderType)aFolderType
{
    BaseFolder* aFolder = NULL;
    switch(aFolderType) {
        case kFolderTypeCalendar:
        case kFolderTypeUserCalendar:
            aFolder = [[ASCalendarFolder alloc] initWithAccount:self];
            break;
        case kFolderTypeContacts:
        case kFolderTypeUserContacts:
        case kFolderTypeRecipientInfo:
            aFolder = [[ASContactFolder alloc] initWithAccount:self];
            break;
        default:
            aFolder = [[ASEmailFolder alloc] initWithAccount:self];
            break;
    }
    aFolder.folderType = aFolderType;
    return aFolder;
}

- (ASFolder*)createLocalDraftsFolder
{
    ASFolder* aFolder = NULL;
    aFolder = [[ASEmailFolder alloc] initWithAccount:self];
    [aFolder setIsSyncable:FALSE];
    aFolder.uid         = [self getClientID];
    aFolder.displayName = FXLLocalizedStringFromTable(@"LOCAL_DRAFTS", @"ASAccount", @"Displayed name of Local Drafts folder");
    aFolder.parentId    = @"";
    [aFolder setSyncKey:@"" syncDate:nil];
    aFolder.folderType  = kFolderTypeLocalDrafts;
    aFolder.folderNum   = self.folders.count;
    [self addFolder:aFolder];
    NSMutableDictionary* aFolderState = [NSMutableDictionary dictionaryWithCapacity:20];
    [aFolder folderAsDictionary:aFolderState];
    [[SyncManager getSingleton] addFolderState:aFolderState accountNum:self.accountNum];
    
    return aFolder;
}

////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark  Overrides

- (void)setupFolderForName:(NSString*)aFolderName
{
    ASFolder* aFolder = (ASFolder*)[self folderForName:aFolderName];
    if(aFolder) {
        [self setupFolder:aFolder];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME setupFolder not found: %@", aFolderName);
    }
}

- (void)setupFolder:(BaseFolder*)aBaseFolder
{
    ASFolder* aFolder = (ASFolder*)aBaseFolder;
    NSMutableDictionary* aFolderState = [NSMutableDictionary dictionaryWithCapacity:20];
    [aFolder folderAsDictionary:aFolderState];
    [[SyncManager getSingleton] addFolderState:aFolderState accountNum:self.accountNum];
}

- (void)addFolder:(BaseFolder*)aBaseFolder
{
    //FXDebugLog(kFXLogActiveSync, @"addFolder: %@", aBaseFolder);
    ASFolder* aFolder = (ASFolder*)aBaseFolder;
    if([aFolder.parentId length] > 0 && ![aFolder.parentId isEqualToString:@"0"]) {
        BaseFolder* aParentFolder = [self folderForServerId:aFolder.parentId];
        if(aParentFolder) {
            [aParentFolder addChild:(BaseFolder*)aFolder];
        }else{
            FXDebugLog(kFXLogActiveSync, @"addFolder parent folder not found: %@", aBaseFolder);
        }
    }
    ASFolder* anExistingFolder = (ASFolder*)[self folderForServerId:[aFolder uid]];
    if(!anExistingFolder) {
        [folders addObject:aFolder]; 
        [folderDictionary setObject:aFolder forKey:[aFolder uid]];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Attempting to add folder twice: %@ %@", aFolder, anExistingFolder);
    }    
}

- (void)removeAllFolders
{
    [ASPing removePingsForAccount:self];
    [super removeAllFolders];
}

- (void)creationComplete
{
    [super removeAllFolders];

    NSDictionary* aFolderStates = [SyncManager loadFolderState:self.accountNum];
    [self loadFolders:aFolderStates];
    
    if([self initialSync]) {
        FXDebugLog(kFXLogActiveSync, @"ActiveSync create account complete. InitialSync started:\n%@", self);
    }else{
        FXDebugLog(kFXLogActiveSync, @"ActiveSync create account complete. InitialSync failed:\n%@", self);
    }
}

- (void)sendEmail:(ASEmail *)anEmail
{
    
}

@end
