/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"
#import "FolderDelegate.h"

@class BaseAccount;
@class DBaseEngine;

#define kFolderCountingNotification     @"kFolderCountingNotification"
#define kFolderCountedNotification		@"kFolderCountedNotification"

// Folder types
//
typedef enum {
    kFolderTypeUser                 = 1,
    kFolderTypeInbox                = 2,
    kFolderTypeDrafts               = 3,
    kFolderTypeDeleted              = 4,
    kFolderTypeSent                 = 5,
    kFolderTypeOutbox               = 6,
    kFolderTypeTasks                = 7,
    kFolderTypeCalendar             = 8,
    kFolderTypeContacts             = 9,
    kFolderTypeNotes                = 10,
    kFolderTypeJournal              = 11,
    kFolderTypeUserMailbox          = 12,
    kFolderTypeUserCalendar         = 13,
    kFolderTypeUserContacts         = 14,
    kFolderTypeUserTasks            = 15,
    kFolderTypeUserJournal          = 16,
    kFolderTypeUserNotes            = 17,
    kFolderTypeUnknown              = 18,
    kFolderTypeRecipientInfo        = 19,
    kFolderTypeLocalDrafts          = 50        // NOTE: This is not ActiveSync defined, local drafts folder is not synced
} EFolderType;

typedef enum {
    kChangeReadStatus               = 1,
    kChangeMeetingUserResponse      = 2,
    kChangeEvent                    = 3,
}EChange;

@interface BaseFolder : BaseObject {
    // Properties
    EFolderType                 folderType;
    BaseAccount*                __weak account;
    BaseFolder*                 __weak parent;
;

    // Persistent properties
    NSString*                   uid;
    NSString*                   displayName;

    int                         accountNum;
    int                         folderNum;
    int                         folderCount;
    NSUInteger                  flags;
    int                         unreadCount;
    // Object properties
    NSMutableArray*             objectsAdded;
    NSMutableArray*             objectsChanged;
    NSMutableArray*             objectsDeleted;
    
    // Local
    NSMutableArray*             children;
    NSMutableArray*             delegates;
    NSMutableDictionary*        dictionary;

    NSLock*                     lock;
    BOOL                        dirty;
}

// Properties
@property (nonatomic) EFolderType               folderType;
@property (weak, nonatomic, readonly) BaseAccount*    account;
@property (weak, nonatomic, readonly) BaseFolder*     parent;
@property (nonatomic, readonly) int                   unread;

// Persistent properties
@property (nonatomic, strong) NSString*         uid;
@property (nonatomic, strong) NSString*         displayName;

@property (nonatomic) int                       accountNum;
@property (nonatomic) int                       folderCount;
@property (nonatomic) int                       folderNum;

// Object properties
@property (strong) NSMutableArray*              objectsAdded;
@property (strong) NSMutableArray*              objectsChanged;
@property (strong) NSMutableArray*              objectsDeleted;


// ToolBar Factory
+ (void)setProgressItem:(UIBarButtonItem*)aProgressItem;
+ (UIBarButtonItem*)progressItem;
+ (NSString*)stringForFolderType:(EFolderType)aFolderType;

+ (void)removeAllFolders;

// Construct/Destruct
- (id)initWithAccount:(BaseAccount*)anAccount;

// Error Handling
- (void)logException:(NSString*)where exception:(NSException*)e;

// Utilities
- (int)combinedFolderNum;

// Load/Store
- (int)getInt:(NSDictionary*)aDictionary key:(NSString*)aKey;
- (BOOL)getBool:(NSDictionary*)aDictionary key:(NSString*)aKey;

+ (EFolderType)folderTypeFromStore:(NSDictionary*)aDictionary;
- (void)loadFromStore:(NSDictionary*)aDictionary;
- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary;

// Hierarchy
- (void)addChild:(BaseFolder*)aFolder;
- (void)removeChild:(BaseFolder*)aFolder;

- (BOOL)hasChildren;
- (NSArray*)children;

// Delegate
- (void)addDelegate:(NSObject<FolderDelegate>*)aFolderDelegate;
- (void)removeDelegate:(NSObject<FolderDelegate>*)aFolderDelegate;

// Objects
- (void)addObject:(BaseObject*)anObject;
- (void)addObjects:(NSArray*)anObjects;
- (void)changedObject:(BaseObject *)anObject;

// Overrides

- (DBaseEngine*)databaseEngine;
- (void)commit;
- (void)commitObjects;
- (void)commitChanges:(NSArray*)anObjects;
- (void)setup;
- (void)foldersLoaded;

- (void)changeObject:(BaseObject*)aBaseObject
          changeType:(EChange)aChangeType
        updateServer:(BOOL)updateServer
      updateDatabase:(BOOL)updateDatabase;
- (void)deleteUid:(NSString*)aUid updateServer:(BOOL)updateServer;
- (void)deleteDatabaseForFolder;
- (void)syncOfflineChanges;

#if 0
// Database
+ (void)tableCheck;
#endif

// Flags Stored
- (NSUInteger)flagsToStore;

- (BOOL)isDeleted;
- (void)setIsDeleted:(BOOL)state;

- (BOOL)isVisible;
- (void)setIsVisible:(BOOL)state;

- (BOOL)isSyncable;
- (void)setIsSyncable:(BOOL)state;

- (BOOL)isInitialSynced;
- (void)setIsInitialSynced:(BOOL)state;

- (BOOL)isSyncWanted;
- (void)setIsSyncWanted:(BOOL)state;

- (BOOL)hasOfflineChanges;
- (void)setHasOfflineChanges:(BOOL)state;

// Flags Transient
- (BOOL)isPinging;
- (void)setIsPinging:(BOOL)state;

- (BOOL)isMoreAvailable;
- (void)setIsMoreAvailable:(BOOL)state;

- (BOOL)isInitialSync;
- (void)setIsInitialSync:(BOOL)state;

- (BOOL)isSyncing;
- (void)setIsSyncing:(BOOL)state;

- (BOOL)isResyncing;
- (void)setIsResyncing:(BOOL)state;

- (BOOL)isInitialSyncInterim;
- (void)setIsInitialSyncInterim:(BOOL)state;

- (BOOL)isCounting;
- (void)setIsCounting:(BOOL)state;

- (BOOL)isCountWanted;
- (void)setIsCountWanted:(BOOL)state;

- (BOOL)isSyncingOfflineChanges;
- (void)setIsSyncingOfflineChanges:(BOOL)state;

// Debug
- (NSString*)description;

@end
