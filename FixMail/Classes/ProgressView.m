//
//  ProgressView.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 3/16/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "ProgressView.h"
#import "BaseFolder.h"
#import "SyncManager.h"

@implementation ProgressView

// Globals
extern BOOL gIsFixTrace;

// Properties
@synthesize progressLabel;
@synthesize updatedLabel;
@synthesize progressView;
@synthesize activity;

@synthesize updatedLabelTop;
@synthesize clientMessageLabelBottom;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static ProgressView*    sProgressView   = nil;
static UIBarButtonItem* sProgressItem   = nil;

+ (ProgressView*)progressView
{
    if(!sProgressView) {
        NSArray* nibContents = [[FXLSafeZone getResourceBundle] loadNibNamed:@"ProgressView" owner:self options:NULL];
        NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
        NSObject* nibItem = nil;
        while ((nibItem = [nibEnumerator nextObject]) != nil) {
            if([nibItem isKindOfClass: [ProgressView class]]) {
                sProgressView = (ProgressView*)nibItem;
                break;
            }
        }
        
        if(sProgressView) {
            [sProgressView.progressView setHidden:YES];
            [sProgressView.activity setHidden:YES];
            [sProgressView.progressLabel setHidden:YES];
            [sProgressView.updatedLabel setHidden:YES];
            [sProgressView.updatedLabelTop setHidden:YES];
            [sProgressView.clientMessageLabelBottom setHidden:YES];
            sProgressItem = [[UIBarButtonItem alloc] initWithCustomView:sProgressView];
            [BaseFolder setProgressItem:sProgressItem];
        }else{
            FXDebugLog(kFXLogActiveSync, @"ProgressView failed to load");
        }
    }
    return sProgressView;
}

+ (UIBarButtonItem*)barButtonItem
{
    if(!sProgressView) {
        [ProgressView progressView];
    }
    return sProgressItem;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        SyncManager* aSyncManager = [SyncManager getSingleton];
        [aSyncManager registerForProgressWithDelegate:self];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super initWithCoder:decoder]) {
        SyncManager* aSyncManager = [SyncManager getSingleton];
        [aSyncManager registerForProgressWithDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    SyncManager* aSyncManager = [SyncManager getSingleton];
    [aSyncManager removeProgressDelegate:self];
}

////////////////////////////////////////////////////////////////////////////////
// ProgressDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ProgressDelegate

- (void)didChangeProgressStringTo:(NSString*)progressString
{
    if(gIsFixTrace && progressString.length > 0) {
        FXDebugLog(kFXLogTrace, @"%@", progressString);
    }
	[sProgressView.progressLabel setHidden:YES];
	[sProgressView.activity setHidden:YES];
	[sProgressView.progressView setHidden:YES];
	
    [sProgressView.updatedLabelTop setHidden:YES];
    [sProgressView.clientMessageLabelBottom setHidden:YES];
    [sProgressView.updatedLabel setHidden:NO];
    sProgressView.updatedLabel.text = progressString;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kProgressViewStringDidChangeNotification object:nil];
}

- (void)didChangeProgressTo:(NSDictionary*)dict
{
	float progress = [[dict objectForKey:@"progress"] floatValue];
    
	sProgressView.progressView.progress = progress;
	sProgressView.progressLabel.text = [dict objectForKey:@"message"];
	
	[sProgressView.updatedLabelTop setHidden:YES];
	[sProgressView.clientMessageLabelBottom setHidden:YES];
	[sProgressView.updatedLabel setHidden:YES];
	
	[sProgressView.progressLabel setHidden:NO];
	[sProgressView.progressView setHidden:NO];
}

////////////////////////////////////////////////////////////////////////////////
// UIView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIView

- (void)drawRect:(CGRect)rect {
    // Drawing code
}

@end
