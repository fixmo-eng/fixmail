//
//  AppSettings.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 2/3/09.
//  Copyright 2010 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "AppSettings.h"
#import "StringUtil.h"
#import "FXLSafeZone.h"
#import "FXMEmailSecurity.h"

#pragma mark -

@implementation AppSettings

+(fixmail_edition_enum)fixMailEdition {
	return fixMailEdition;
}

+(NSString*)appID {
	// returns "com.remail.reMail", "com.remail.reMail2", "com.remail.reMail2G"
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+(NSString*)version {
    //FXDebugLog(kFXLogActiveSync, @"infoDictionary:\n%@", [[NSBundle mainBundle] infoDictionary]);
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

+(NSString*)appName {
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
}

+(NSString*)dataInitVersion {
	// version of the software at which the data store was initialized
	return  [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"app_data_init_version"];
}

+(void)setDataInitVersion {
	// version of the software at which the data store was initialized
	[[FXLSafeZone secureStandardUserDefaults] setObject:[AppSettings version] forKey:@"app_data_init_version"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(int)datastoreVersion {
	return [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"datastore_version"];
}

+(void)setDatastoreVersion:(int)value {
	[[FXLSafeZone secureStandardUserDefaults] setInteger:value forKey:[NSString stringWithFormat:@"datastore_version"]];
	[FXLSafeZone secureResetStandardUserDefaults];
}


+(NSString*)systemVersion {
	NSString* systemVersion = [UIDevice currentDevice].systemVersion;
	return systemVersion;
}

//The model is used for ActiveSync deviceType.  Device Type does not support spaces.
+(NSString*)model {
	NSString* model = [[UIDevice currentDevice].model stringByReplacingOccurrencesOfString:@" " withString:@""];
	return model;
}

+(NSString*) deviceType
{
    NSString* type = [NSString stringWithFormat:@"FixMail%@",[[UIDevice currentDevice].model stringByReplacingOccurrencesOfString:@" " withString:@""]];
    return type;
}

+ (NSString*)_getDeviceID
{
    // Return a unique ID for the device
    //
    NSString* aDeviceID = NULL;
    UIDevice* aDevice = [UIDevice currentDevice];
    if([aDevice respondsToSelector:@selector(identifierForVendor)]) {
        // This only works on IOS 6 or later
        //
        NSObject* aUUID = [aDevice performSelector:@selector(identifierForVendor)];
        if([aUUID respondsToSelector:@selector(UUIDString)]) {
            aDeviceID = [aUUID performSelector:@selector(UUIDString)];
        }
    }else{
        // This uuid is doing to change everytime the app is wiped which isn't ideal
        // but the best we can do on IOS 5 since UIDevice uuid was deprecated
        //
        CFUUIDRef uuidObj = CFUUIDCreate(nil);
        aDeviceID = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuidObj));
        CFRelease(uuidObj);
    }
    if(aDeviceID.length > 0) {
        aDeviceID =  [aDeviceID stringByReplacingOccurrencesOfString:@"-" withString:@""];
        FXDebugLog(kFXLogActiveSync, @"getDeviceID %@", aDeviceID);
    }else{
        FXDebugLog(kFXLogActiveSync, @"getDeviceID failed");
    }
    
	[[FXLSafeZone secureStandardUserDefaults] setObject:aDeviceID forKey:[NSString stringWithFormat:@"deviceID"]];
	[FXLSafeZone secureResetStandardUserDefaults];
    
    return aDeviceID;
}

+(NSString*)deviceID {
	NSString* aDeviceID = [[FXLSafeZone secureStandardUserDefaults] stringForKey:[NSString stringWithFormat:@"deviceID"]];	
	if([aDeviceID length] == 0) {
        aDeviceID = [AppSettings _getDeviceID];
    }
    return aDeviceID;
}

+(NSString*)pushDeviceToken {
	NSString* y = [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"push_device_token"];
	return y;
}

+(void)setPushDeviceToken:(NSString*)y {
	[[FXLSafeZone secureStandardUserDefaults] setObject:y forKey:@"push_device_token"];
}


+(void)setPushTime:(NSDate*)date {
	double value = (double)[date timeIntervalSince1970];
	[[FXLSafeZone secureStandardUserDefaults] setFloat:value forKey:@"push_time"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(NSDate*)pushTime {
	double interval = [[FXLSafeZone secureStandardUserDefaults] doubleForKey:@"push_time"]; 
	if(interval == 0.0) {
		return nil;
	}
	return [NSDate dateWithTimeIntervalSince1970:interval];	
}


+(NSString*)lastpos {
	NSString* y = [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"lastpos_preference"]; 
	
	if(y == nil) {
		return @"home";
	}
	
	return y;
}

+(void)setLastpos:(NSString*)y {
	[[FXLSafeZone secureStandardUserDefaults] setObject:y forKey:@"lastpos_preference"];
#ifdef DEBUG
    [[FXLSafeZone secureStandardUserDefaults] synchronize];
#endif
}

+(int)searchCount {
	int searchCount = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"search_count_preference"]; 
	return searchCount;
}

+(void)incrementSearchCount {
	int searchCount = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"search_count_preference"]; 
	[[FXLSafeZone secureStandardUserDefaults] setInteger:searchCount+1 forKey:@"search_count_preference"];
}

+(BOOL)pinged {
	return  [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"pinged"];
}

+(void)setPinged {
	[[FXLSafeZone secureStandardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"pinged"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(int)recommendationCount {
	int searchCount = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"recommendation_count_preference"]; 
	return searchCount;
}

+(void)incrementRecommendationCount {
	int searchCount = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"recommendation_count_preference"]; 
	[[FXLSafeZone secureStandardUserDefaults] setInteger:searchCount+1 forKey:@"recommendation_count_preference"];
}

+(NSString*)cacUserName
{
    return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"cac_username"];
}

+(void)setCacUserName:(NSString*)cacUserName
{
    [[FXLSafeZone secureStandardUserDefaults] setObject:cacUserName forKey:@"cac_username"];
}

////////////////////////////////////////////////////////
// Data schema version for DB

+(int)globalDBVersion {
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"global_db_version"]; 
	
	return pref;
}

+(void)setGlobalDBVersion:(int)version {
	[[FXLSafeZone secureStandardUserDefaults] setInteger:version forKey:@"global_db_version"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

// Old ReMail Settings

+(BOOL)reset {
	// returns YES if application should be reset at startup
	// this is triggered by setting the "reset" switch in App preferences to yes
	BOOL resetPreference = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"reset_preference"]; 
	
	return resetPreference;
}

+(void)setReset:(BOOL)value {
	[[FXLSafeZone secureStandardUserDefaults] setBool:value forKey:@"reset_preference"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(int)interval {
	int intervalPreference = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"interval_preference"]; 
	
	if(intervalPreference == 0) {
		return 300;
	}
	
	return intervalPreference;
}

// IMAP obsolete
+(BOOL)last12MosOnly {
	BOOL y = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"last_12mo_preference"]; 
	
	return y;
}

// IMAP obsolete
+(BOOL)logAllServerCalls {
	BOOL logPreference = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"log_all_server_calls"]; 
    
	return logPreference;
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync settings in root.plist
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync settings in root.plist

+(NSString*)emailTimeFilter {
	return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"emailTimeFilter"];		
}

+(void)setEmailTimeFilter:(NSString*)value {
	[[FXLSafeZone secureStandardUserDefaults] setObject:value forKey:@"emailTimeFilter"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(NSString*)calendarTimeFilter {
	return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"calendarTimeFilter"];
}

+(void)setCalendarTimeFilter:(NSString*)value {
	[[FXLSafeZone secureStandardUserDefaults] setObject:value forKey:@"calendarTimeFilter"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(BOOL)autoSync {
	return [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"autoSync"];		
}

+(void)setAutoSync:(BOOL)value {
	[[FXLSafeZone secureStandardUserDefaults] setBool:value forKey:@"autoSync"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+(NSString*)bodyFormat {
	return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"bodyFormat"];		
}

+(void)setBodyFormat:(NSString*)value {
	[[FXLSafeZone secureStandardUserDefaults] setObject:value forKey:@"bodyFormat"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

////////////////////////////////////////////////////////////////////////////////
// Settings
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Settings

+ (void) resetUserSettings
{
	// viewing
	[AppSettings setSettingsSection:0];
	[AppSettings setSettingsRow:0];

	// calendar
	[AppSettings setCalendarViewType:kCalendarMonth];
	[AppSettings setCalendarReminders:NO];
	[AppSettings setCalendarEvents:calendarEvents1Month];

	// email
	[AppSettings setEmailRecentMessages:100];
	[AppSettings setEmailPreviewLines:2];
	[AppSettings setEmailAskBeforeDeleting:YES];
	[AppSettings setEmailReplyTo:@""];
	[AppSettings setEmailSignature:@""];

	// contacts
	[AppSettings setContactsDisplayOrder:sortOrderFirstLast];
	[AppSettings setContactsSortOrder:sortOrderLastFirst];

	// S/MIME stuff
	// if we reset the SMIME setting, then get rid of any certificate as well.
	[AppSettings setEmailSMIME:NO];
	[FXMEmailSecurity deleteUserCertificate];

	// developer only
	[AppSettings setCalendarStartDayOn:calendarStartOnSunday];
	[AppSettings setEmailBodyType:emailBodyHTML];

	return;
}

+ (int) settingsSection
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"settingsSection"];
    return pref;
}

+ (void) setSettingsSection:(int)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:inValue forKey:[NSString stringWithFormat:@"settingsSection"]];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (int) settingsRow
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"settingsRow"];
    return pref;
}

+ (void) setSettingsRow:(int)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:inValue forKey:[NSString stringWithFormat:@"settingsRow"]];
	[FXLSafeZone secureResetStandardUserDefaults];
}

////////////////////////////////////////////////////////////////////////////////
// Calendar
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Calendar

+(ECalendarViewType)calendarViewType
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:[NSString stringWithFormat:@"calendar_view_type"]];
    return (ECalendarViewType)pref;
}

+(void)setCalendarViewType:(ECalendarViewType)type
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)type forKey:[NSString stringWithFormat:@"calendar_view_type"]];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (BOOL) calendarReminders
{
	BOOL pref = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"calendarReminders"];
    return pref;
}

+ (void) setCalendarReminders:(BOOL)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setBool:inValue forKey:@"calendarReminders"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (ECalendarEvents) calendarEvents
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"calendarEvents"];
    return (ECalendarEvents)pref;
}

+ (void) setCalendarEvents:(ECalendarEvents)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"calendarEvents"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (ECalendarStartDay) calendarStartDayOn
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"calendarStartDayOn"];
    return (ECalendarStartDay)pref;
}

+ (void) setCalendarStartDayOn:(ECalendarStartDay)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"calendarStartDayOn"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

////////////////////////////////////////////////////////////////////////////////
// Email
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Email

+ (int) emailRecentMessages
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"emailRecentMessages"];
    return pref;
}

+ (void) setEmailRecentMessages:(int)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"emailRecentMessages"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (int) emailPreviewLines
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"emailSetPreviewLines"];
    return pref;
}

+ (void) setEmailPreviewLines:(int)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"emailSetPreviewLines"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (BOOL) emailAskBeforeDeleting
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"emailAskBeforeDeleting"];
    return pref;
}

+ (void) setEmailAskBeforeDeleting:(BOOL)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setBool:inValue forKey:@"emailAskBeforeDeleting"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (BOOL) emailSMIME
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] boolForKey:@"emailSMIME"];
    return pref;
}

+ (void) setEmailSMIME:(BOOL)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setBool:inValue forKey:@"emailSMIME"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (void) setEmailReplyTo:(NSString*)inString
{
	[[FXLSafeZone secureStandardUserDefaults] setObject:inString forKey:@"emailReplyTo"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (NSString *) emailReplyTo
{
	return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"emailReplyTo"];		
}

+ (void) setEmailSignature:(NSString*)inString
{
	[[FXLSafeZone secureStandardUserDefaults] setObject:inString forKey:@"emailSignature"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (NSString *) emailSignature
{
	return [[FXLSafeZone secureStandardUserDefaults] stringForKey:@"emailSignature"];		
}

+ (EEmailBodyType) emailBodyType
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"emailBodyType"];
    return (EEmailBodyType)pref;
}

+ (void) setEmailBodyType:(EEmailBodyType)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"emailBodyType"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

////////////////////////////////////////////////////////////////////////////////
// Contacts
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Contacts

+ (EContactSortOrder) contactsSortOrder
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"contactsSortOrder"];
    return (EContactSortOrder)pref;
}

+ (void) setContactsSortOrder:(EContactSortOrder)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"contactsSortOrder"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

+ (EContactSortOrder) contactsDisplayOrder
{
	int pref = [[FXLSafeZone secureStandardUserDefaults] integerForKey:@"contactsDisplayOrder"];
    return (EContactSortOrder)pref;
}

+ (void) setContactsDisplayOrder:(EContactSortOrder)inValue
{
	[[FXLSafeZone secureStandardUserDefaults] setInteger:(int)inValue forKey:@"contactsDisplayOrder"];
	[FXLSafeZone secureResetStandardUserDefaults];
}

@end
