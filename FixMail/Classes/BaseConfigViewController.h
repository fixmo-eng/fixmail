/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "AppSettings.h"

@class BaseAccount;

@interface BaseConfigViewController : UITableViewController {
    BaseAccount*    account;
	int             accountNum;
	BOOL            newAccount;
	BOOL            firstSetup;
    
    email_account_sub_type_enum accountSubType;

    // UI
    //
	IBOutlet UIScrollView* scrollView;
	IBOutlet UIImageView* imageView;
    
	IBOutlet UITextField* nameField;
	IBOutlet UITextField* userNameField;
    IBOutlet UILabel* passwordLabel;
	IBOutlet UITextField* passwordField;
    IBOutlet UITextField* PCCField;
	IBOutlet UITextField* hostnameField;
    IBOutlet UIImageView* hostStatusImageView;
	IBOutlet UITextField* domainField;
	IBOutlet UITextField* portField;
    
	IBOutlet UIActivityIndicatorView* activityIndicator;
    
	IBOutlet UIButton* selectFoldersButton;
}

// Properties
@property (strong) BaseAccount*                 account;
@property (assign) int                          accountNum;
@property (assign) BOOL                         newAccount;
@property (assign) BOOL                         firstSetup;
@property (assign) email_account_sub_type_enum  accountSubType;


// UI Properties
@property (nonatomic, strong) IBOutlet UIScrollView* scrollView;
@property (nonatomic, strong) IBOutlet UIImageView* imageView;

@property (nonatomic, strong) IBOutlet UITextField* nameField;
@property (nonatomic, strong) IBOutlet UITextField* userNameField;
@property (nonatomic, strong) IBOutlet UIImageView* userNameStatusImageView;
@property (nonatomic, strong) IBOutlet UILabel* passwordLabel;
@property (nonatomic, strong) IBOutlet UITextField* passwordField;
@property (nonatomic, strong) IBOutlet UIImageView* passwordStatusImageView;
@property (nonatomic, strong) IBOutlet UITextField* PCCField;
@property (nonatomic, strong) IBOutlet UITextField* hostnameField;
@property (nonatomic, strong) IBOutlet UIImageView* hostStatusImageView;
@property (nonatomic, strong) IBOutlet UITextField* domainField;
@property (nonatomic, strong) IBOutlet UITextField* portField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *progressItem;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* activityIndicator;

@property (nonatomic, strong) IBOutlet UIButton* selectFoldersButton;

@end
