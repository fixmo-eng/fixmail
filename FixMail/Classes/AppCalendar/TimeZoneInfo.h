/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"

@interface TimeZoneInfo : BaseObject {
    BOOL            hasDaylightSavingsTime;
    
    NSTimeZone*     timeZone;
    NSDate*         date;
    NSString*       name;
    
    NSInteger       daylightOffsetMinutes;
    NSString*       daylightAbbreviation;
    NSDate*         daylightTranstionDate;
    NSInteger       standardOffsetMinutes;
    NSString*       standardAbbreviation;
    NSDate*         standardTranstionDate;
}

// Properties
@property (nonatomic) BOOL                  hasDaylightSavingsTime;

@property (nonatomic,strong) NSTimeZone*    timeZone;
@property (nonatomic,strong) NSDate*        date;
@property (nonatomic,strong) NSString*      name;

@property (nonatomic) NSInteger             daylightOffsetMinutes;
@property (nonatomic,strong) NSString*      daylightAbbreviation;
@property (nonatomic,strong) NSDate*        daylightTranstionDate;

@property (nonatomic) NSInteger             standardOffsetMinutes;
@property (nonatomic,strong) NSString*      standardAbbreviation;
@property (nonatomic,strong) NSDate*        standardTranstionDate;

// Factory
+ (TimeZoneInfo*)timeZoneInfo:(NSTimeZone*)aTimeZone forDate:(NSDate*)aDate;
+ (TimeZoneInfo*)timeZoneInfoWithName:(NSString*)aTimeZoneName forDate:(NSDate*)aDate;

// Construct/Destruct
- (id)initWithTimeZone:(NSTimeZone*)aTimeZone forDate:(NSDate*)aDate;
- (id)initWithTimeZoneName:(NSString*)aTimeZoneName forDate:(NSDate*)aDate;

// Interfaces
- (NSString*)daylightOffsetAsString;
- (NSString*)standardOffsetAsString;

@end