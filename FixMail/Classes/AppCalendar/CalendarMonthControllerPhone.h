/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarDelegate.h"
#import "FolderDelegate.h"
#import "TKCalendarDayTimelineViewDelegate.h"
#import "TKCalendarMonthTableViewController.h"

@class ASCalendarFolder;
@class CalendarDayController;
@class EventEditController;
@class EventViewController;

@interface CalendarMonthControllerPhone : TKCalendarMonthViewController
        <CalendarDelegate,
        FolderDelegate,
        TKCalendarMonthViewDelegate,
        TKCalendarDayTimelineViewDelegate,
        UIPopoverControllerDelegate> {
    
    BOOL                        isPushed;
}

// Properties
@property (nonatomic,readonly) ASCalendarFolder*        folder;
@property (nonatomic,strong) EventViewController*       eventViewController;
@property (nonatomic,strong) CalendarDayController*     dayController;
@property (nonatomic,strong) UIPopoverController*       popover;

// Factory
+ (CalendarMonthControllerPhone*)controller;

// Interface
- (void)setFolder:(ASCalendarFolder *)folder;
- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents;
- (void)pushViewController:(UIViewController*)anViewController;

@end
