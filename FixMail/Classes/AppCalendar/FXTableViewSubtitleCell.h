//
//  FXTableViewSubtitleCell.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-12.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXTableViewSubtitleCell : UITableViewCell

@end
