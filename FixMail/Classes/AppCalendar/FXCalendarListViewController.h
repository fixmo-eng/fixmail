//
//  FXCalendarListController.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarDelegate.h"
#import "FolderDelegate.h"
#import "FXCalendarUpdateDelegate.h"

@class ASCalendarFolder;

@interface FXCalendarListViewController : UITableViewController <FolderDelegate, UITableViewDataSource, UITableViewDelegate>
{
    
}

-(void)scrollToDate:(NSDate*)date;

@property (nonatomic,strong) ASCalendarFolder* folder;
@property (nonatomic,strong) NSDate* currentDate;

@property (nonatomic,weak) id<FXCalendarUpdateDelegate> updateDelegate;
@end
