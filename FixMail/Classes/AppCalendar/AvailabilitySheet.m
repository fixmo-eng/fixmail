/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "AvailabilitySheet.h"
#import "Event.h"

@implementation AvailabilitySheet

@synthesize event;
@synthesize tableView;
@synthesize indexPath;

// Action Sheet types
typedef enum
{
    kAvailabilityBusy           = 0,
    kAvailabilityFree           = 1,
    kAvailabilityTentative      = 2,
    kAvailabilityOutOfOffice    = 3
} EAvailabilityActions;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView
             event:(Event*)anEvent
         tableView:(UITableView*)aTableView
         indexPath:(NSIndexPath*)anIndexPath
{
    if (self = [super init]) {
        self.event      = anEvent;
        self.tableView  = aTableView;
        self.indexPath  = anIndexPath;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:FXLLocalizedStringFromTable(@"BUSY", @"Calendar", @"Displayed availability - busy")
                                           otherButtonTitles:FXLLocalizedStringFromTable(@"FREE", @"Calendar", @"Displayed availability - free"),
                                                            FXLLocalizedStringFromTable(@"TENTATIVE", @"Calendar", @"Displayed availability - tentative"),
                                                            FXLLocalizedStringFromTable(@"OUT_OF_OFFICE", @"Calendar", @"Displayed availability - out of office"), nil];
        [anActionSheet showInView:aView];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch((EAvailabilityActions)buttonIndex) {
        case kAvailabilityBusy:
            [event setBusyStatus:kBusyStatusBusy];
            break;
        case kAvailabilityFree:
            [event setBusyStatus:kBusyStatusFree];
            break;
        case kAvailabilityTentative:
            [event setBusyStatus:kBusyStatusTentative];
            break;
        case kAvailabilityOutOfOffice:
            [event setBusyStatus:kBusyStatusOutOfOffice];
            break;
        default:
            break;
    }
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
