//
//  FXCalendarDelegate.h
//  FixMail
//
//  Created by Colin Biggin on 2013-01-25.
//
//

#import "SZLApplicationContainer.h"

@interface FXCalendarDelegate : NSObject <SZLApplicationContainerDelegate, SZLConfigurationDelegate>

@property (nonatomic, assign) id <SZLApplicationContainer> container;

@end
