/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarEmail.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "ASEmail.h"
#import "Attendee.h"
#import "Event.h"
#import "MeetingRequest.h"
#import "PKMIMEMessage.h"

@implementation CalendarEmail

////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (NSString*)calendarAsMIMEToAttendee:(Attendee*)anAttendee
                              subject:(NSString*)aSubject
                                 body:(NSString*)aBody
                           iCalString:(NSString*)anIcalString
{
    NSString* aMIMEString = nil;
    
    @try {
        PKContentType* aContentType = [PKContentType contentTypeFromString:@"multipart/mixed"];
        
        PKMIMEMessage* aMesg = [[PKMIMEMessage alloc] initWithContentType:aContentType];
        NSMutableDictionary* aHeaders = aMesg.headers;
        
        // To Header
        //
        [aHeaders setObject:[anAttendee asToString] forKey:@"To"];
        
        // Subject Header
        //
        [aHeaders setObject:aSubject forKey:@"Subject"];
        
        // multipart/alternative
        //
        PKContentType* anAlternativeType = [PKContentType contentTypeFromString:@"multipart/alternative"];
        PKMIMEMessage* anAlternativeMesg = [[PKMIMEMessage alloc] initWithContentType:anAlternativeType];
        {
            // text/plain
            NSData* aTextData = [[NSData alloc] initWithBytes:[aBody UTF8String] length:[aBody length]];
            NSMutableDictionary* aTextHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
            [aTextHeaders setObject:@"text/plain"                       forKey:@"Content-Type"];
            PKMIMEData* aTextPlainData = [[PKMIMEData alloc] initWithData:aTextData];
            [aTextPlainData setHeadersFromDictionary:aTextHeaders];
            [anAlternativeMesg addPart:aTextPlainData];
            
            // text/html
            NSString* anHTMLBody = [NSString stringWithFormat:@"<html>\n<head>\n</head>\n<body>\n%@<br>\n</body>\n</html>\n", aBody];
            NSData* anHTMLData = [[NSData alloc] initWithBytes:[anHTMLBody UTF8String] length:[anHTMLBody length]];
            NSMutableDictionary* anHTMLHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
            [anHTMLHeaders setObject:@"text/html"                       forKey:@"Content-Type"];
            PKMIMEData* anHTMLMimeData = [[PKMIMEData alloc] initWithData:anHTMLData];
            [anHTMLMimeData setHeadersFromDictionary:anHTMLHeaders];
            [anAlternativeMesg addPart:anHTMLMimeData];
            
            // text/calendar
            //
            // NOTE: This is the important part, Exchange uses this to determine that this is a meeting request
            // so this part must be here and must be right.  Exchange doesn't seem to notice the .ics attachment
            //
            NSData* aCalendarData = [[NSData alloc] initWithBytes:[anIcalString UTF8String] length:[anIcalString length]];
            NSMutableDictionary* aCalendarHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
            [aCalendarHeaders setObject:@"text/calendar"                forKey:@"Content-Type"];
            PKMIMEData* aCalendarMimeData = [[PKMIMEData alloc] initWithData:aCalendarData];
            [aCalendarMimeData setHeadersFromDictionary:aCalendarHeaders];
            [anAlternativeMesg addPart:aCalendarMimeData];
            
            // Build the multipart/alternative part and added it to the MIME multipart/mixed main message
            //
            NSMutableDictionary* anAlternativeHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
            NSString* anAlternativeContentType = [NSString stringWithFormat:@"multipart/alternative; boundary=%@", [anAlternativeMesg boundary]];
            [anAlternativeHeaders setObject:anAlternativeContentType    forKey:@"Content-Type"];
            PKMIMEData* anAlternativeData = [[PKMIMEData alloc] initWithData:[anAlternativeMesg data]];
            [anAlternativeData setHeadersFromDictionary:anAlternativeHeaders];
            [aMesg addPart:anAlternativeData];
        }
        
        // ics attachment
        //
        NSMutableDictionary* anAttachHeaders = [NSMutableDictionary dictionaryWithCapacity:10];
        [anAttachHeaders setObject:@"application/ics; name=\"invite.ics\""  forKey:@"Content-Type"];
        [anAttachHeaders setObject:@"filename=\"invite.ics\""               forKey:@"Content-Disposition"];
        [anAttachHeaders setObject:@"base64"                                forKey:@"Content-Transfer-Encoding"];
        
        NSData* aMIMEData = [[NSData alloc] initWithBytes:[anIcalString UTF8String] length:[anIcalString length]];
        PKMIMEData* aVCalData = [[PKMIMEData alloc] initWithData:aMIMEData];
        [aVCalData setHeadersFromDictionary:anAttachHeaders];
        [aMesg addPart:aVCalData];
        
        // Build MIME string
        //
        aMIMEString = [aMesg messageStringWithHeaders];
        
    }@catch (NSException* e) {
        FXDebugLog(kFXLogASEmail, @"calendarAsMIMEToAttendee exception: %@ %@", [e name], [e reason]);
    }
    
    return aMIMEString;
}

+ (NSString*)iCalPostProcess:(NSString*)anICalString
{
    // This truncates lines in the iCalendar file to 72 characters and puts /r/n new lines
    // on the end
    //
    NSMutableString* aNewString = [NSMutableString stringWithCapacity:anICalString.length];
    @try {
        static const NSUInteger kMaxLineLength = 72;
        NSArray* aStrings = [anICalString componentsSeparatedByString:@"\n"];
        for(NSString* aString in aStrings) {
            NSInteger aLength = aString.length;
            if(aLength > kMaxLineLength) {
                NSUInteger aLocation = 0;
                NSRange aRange = NSMakeRange(aLocation, kMaxLineLength);
                [aNewString appendFormat:@"%@\r\n", [aString substringWithRange:aRange]];
                aLength   -= kMaxLineLength;
                aLocation += kMaxLineLength;
                while(aLength > 0) {
                    aRange.location = aLocation;
                    if(aLength < kMaxLineLength) {
                        aRange.length = aLength;
                    }
                    [aNewString appendFormat:@" %@\r\n", [aString substringWithRange:aRange]];
                    aLength   -= kMaxLineLength;
                    aLocation += kMaxLineLength;
                }
            }else{
                [aNewString appendFormat:@"%@\r\n", aString];
            }
        }
    }@catch (NSException* e) {
        FXDebugLog(kFXLogASEmail, @"iCalPostProcess exception: %@ %@", [e name], [e reason]);
    }
    
    return aNewString;
}

+ (void)removeMeeting:(ASEmail*)anEmail folder:(ASCalendarFolder*)aCalendarFolder
{
    @try {
        if(aCalendarFolder) {
            NSString* anEventUid = [anEmail.meetingRequest eventUid];
            Event* anEvent = [aCalendarFolder eventForEventUid:anEventUid];
            if(anEvent) {
                switch(anEmail.meetingRequest.instanceType) {
                    case kExceptionToRecurring:
#warning FIXME removeMeeting exception to recurring
                        FXDebugLog(kFXLogCalendar, @"FIXME removeMeeting exception to recurring: %@", anEmail.subject);
                        break;
                    case kSingleInstanceOfRecurring:
#warning FIXME removeMeeting instance of recurring
                        FXDebugLog(kFXLogCalendar, @"FIXME removeMeeting instance of recurring: %@", anEmail.subject);
                        break;
                    case kMasterRecurringAppointment:
                    case kSingleAppointment:
                        [aCalendarFolder deleteUid:anEvent.uid updateServer:TRUE];
                        [aCalendarFolder commitObjects];
                        break;
                }

            }else{
                FXDebugLog(kFXLogCalendar, @"removeMeeting event not found: %@", anEmail.subject);
            }
        }else{
            FXDebugLog(kFXLogCalendar, @"removeMeeting no calendar folder");
        }
        
        //ASEmailFolder* anEmailFolder = (ASEmailFolder*)anEmail.folder;
        //if(anEmailFolder && anEmailFolder.folderType == kFolderTypeInbox) {
        //    [anEmailFolder deleteUid:anEmail.uid updateServer:TRUE];
        //    [anEmailFolder commitObjects];
        //}
    }@catch (NSException* e) {
        FXDebugLog(kFXLogCalendar, @"removeMeeting exception: %@ %@", e.name, e.reason);
    }
}

+ (ASCalendarFolder*)calendarFolderForAccount:(ASAccount*)anAccount
{
    ASCalendarFolder* aCalendarFolder = nil;
    NSArray* aFolders = [anAccount foldersForFolderType:kFolderTypeCalendar];
    if(aFolders.count == 1) {
        aCalendarFolder = [aFolders objectAtIndex:0];
    }else if(aFolders.count > 1) {
        aCalendarFolder = [aFolders objectAtIndex:0];
        FXDebugLog(kFXLogCalendar, @"removeCanceled multiple calendars");
    }else{
        FXDebugLog(kFXLogCalendar, @"removeCanceled no calendar");
    }
    return aCalendarFolder;
}

@end
