//
//  FXMeetingViewController.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AvailabilityEditController.h"
#import "ReminderEditController.h"

@interface FXMeetingViewController : UITableViewController<AvailabilityEditControllerDelegate,ReminderEditControllerDelegate>
@property (nonatomic,strong) Event* event;

+ (FXMeetingViewController*)controllerWithEvent:(Event*)anEvent;

@end
