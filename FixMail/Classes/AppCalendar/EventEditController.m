/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "EventEditController.h"
#import "ASAccount.h"
#import "ASCompose.h"
#import "ASSync.h"
#import "ASSyncEvent.h"
#import "Attendee.h"
#import "AvailabilitySheet.h"
#import "AvailabilityEditController.h"
#import "BaseAccount.h"
#import "BaseFolder.h"
#import "Calendar.h"
#import "CalendarEmail.h"
#import "composeMailVC.h"
#import "DatePickerController.h"
#import "DatePickerControllerIPhone.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventException.h"
#import "EventRecurrence.h"
#import "Reachability.h"
#import "RecurrenceEditController.h"
#import "RecipientSheet.h"
#import "RecipientViewController.h"
#import "ReminderEditController.h"
#import "RecurrenceTypeSheet.h"
#import "UIAlertView+Modal.h"
#import "SVProgressHUD.h"

@implementation EventEditController

// Docs
//
// Working with meeting requests in Exchange ActiveSync
// http://msdn.microsoft.com/en-us/library/exchange/hh428684(v=exchg.140).aspx
//
// Working with meeting responses in Exchange ActiveSync
// http://msdn.microsoft.com/en-us/library/exchange/hh428685(v=exchg.140).aspx

// Properties
@synthesize actualStartTime;
@synthesize originalEvent;
@synthesize datePickerController;
@synthesize event;
@synthesize eventEditorType;
@synthesize meetingSegmentedControl;
@synthesize editButtonItem;
@synthesize deleteButtonItem;
@synthesize recipientPicker;        // Meeting
@synthesize recipientPopover;
@synthesize recipientView;
@synthesize acceptanceButtons;
@synthesize barButtonItems;         // Nav bar UI
@synthesize availabilitySheet;      // Sheets
@synthesize recipientSheet;
@synthesize popover;

// Section and Row Types
typedef enum
{
    kSectionTitleLocation       = 0,
    kSectionDate                = 1,
    kSectionAttributes          = 2,
    kSectionNotes               = 3,
    kSectionInvitees            = 4,
    kSectionCalendar            = 5,
    kSectionLast                = 6
} EEventSection;

typedef enum
{
    kEventTitle                 = 0,
    kEventLocation              = 1
} ETitleLocation;

typedef enum
{
    kAttributeRecurrence        = 0,
    kAttributeReminder          = 1,
    kAttributeAvailability      = 2
} EAttributes;

typedef enum
{
    kEventStartDate             = 0,
    kEventAllDay                = 1,
    kEventEndDate               = 2,
} EDate;

typedef enum
{
    kEventCancelChanges         = 0,
    kEventSave                  = 1
} EEventActions;

typedef enum
{
    kCancelChanges              = 0,
    kOnlyThisEvent              = 1,
    kFollowingEvents            = 2,
    kAllEvents                  = 3
} ERecurreringEventActions;

typedef enum
{
    kEventTimeZone              = 0,
    kEventCalendar              = 1
} EEventCalendar;

// Constants
static NSString* kSend                  = @"SEND";
static NSString* kCancel                = @"CANCEL";

static NSString* kEventEditCell         = @"EventEditCell";
static NSString* kMeetingCell           = @"MeetingCell";
static NSString* kSwitchCell            = @"SwitchCell";
static NSString* kTextFieldCell         = @"TextFieldCell";
static NSString* kTextViewCell          = @"TextViewCell";

static const CGFloat kNotesRowHeight    = 100.0f;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (EventEditController*)controllerForEvent:(Event*)anEvent eventEditorType:(EEventEditorType)anEventEditorType dateForInstance:(NSDate*)aDate
{
    EventEditController* sController = NULL;

    if(!sController) {
        sController = [[EventEditController alloc] initWithNibName:@"EventEditView" bundle:[FXLSafeZone getResourceBundle]];
    }
    sController.eventEditorType     = anEventEditorType;
    [sController loadEvent:anEvent];
    sController.actualStartTime     = aDate;
    return sController;
}

+ (EventEditController*)createEventForFolder:(BaseFolder*)aFolder date:(NSDate*)aDate
{
    Event* anEvent = [[Event alloc] initWithFolder:aFolder];
    BaseAccount* anAccount  = aFolder.account;
    anEvent.organizerName   = anAccount.name;
    anEvent.organizerEmail  = anAccount.emailAddress;
    anEvent.startDate       = aDate;
    anEvent.endDate         = [[Calendar calendar] addToDate:anEvent.startDate hours:1 minutes:0];
    anEvent.isEditable      = TRUE;
    anEvent.created         = [NSDate date];
    anEvent.lastModified    = anEvent.created;
    anEvent.title = FXLLocalizedString(@"NEW_EVENT", @"placeholder text for a new event");
    EventEditController* aController = [EventEditController controllerForEvent:anEvent eventEditorType:kEventEditorAddEvent dateForInstance:nil];
    return aController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)loadEvent:(Event*)anEvent
{
    self.originalEvent  = anEvent;
    self.event          = [anEvent mutableCopy];
    if(self.eventEditorType == kEventEditorAddEvent) {
        isEditing   = TRUE;
    }else{
        isEditing   = FALSE;
    }
    [self buildRightNavBar];
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)buildRightNavBar
{
	if (!self.isViewLoaded) return;

	NSMutableArray *buttonArray = [[NSMutableArray alloc] init];
    if (self.eventEditorType == kEventEditorChangeEvent) {
		self.deleteButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                                                              target:self
                                                                              action:@selector(deleteEvent)];
        [buttonArray addObject:self.deleteButtonItem];
	}
    if (!isEditing && self.event.isEditable) {
		self.editButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                            target:self
                                                                            action:@selector(editEvent)];
        [buttonArray addObject:self.editButtonItem];
    } else {
		UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                    target:self
                                                                                    action:@selector(done)];
        [buttonArray addObject:doneButton];
    }

    self.navigationItem.rightBarButtonItems = [NSArray arrayWithArray:buttonArray];
}

- (void)buildLeftNavBar
{
    UIBarButtonItem* aButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                 target:self
                                                                                 action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = aButtonItem; 
}

- (void)viewDidLoad
{
	return;
}

- (void)viewWillAppear:(BOOL)animated
{
    isIPad  = IS_IPAD();
    isPush  = FALSE;
        
    [self buildLeftNavBar];
    [self buildRightNavBar];

    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    
    [self startReachablility];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    
    if(!isPush) {
		self.originalEvent  = nil;
		self.event          = nil;
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self buildRightNavBar];
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            self.editButtonItem.enabled = TRUE;
            self.deleteButtonItem.enabled = TRUE;
            break;
        case NotReachable:
            self.editButtonItem.enabled = FALSE;
            self.deleteButtonItem.enabled = FALSE;
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.event.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}
    
////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EVENT_EDIT_EXCEPTION", @"ErrorAlert", @"EventEditController error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EVENT_EDIT", @"ErrorAlert", @"EventEditController error alert view title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Commit changes
////////////////////////////////////////////////////////////////////////////////
#pragma mark Commit changes

- (void)_syncEvent
{
    [self.event changed];
    if(self.eventEditorType == kEventEditorChangeEvent) {
        [self.event copyToObject:self.originalEvent zone:nil];
        [[[ASSyncEvent alloc] initWithEvent:self.originalEvent delegate:self isAdd:FALSE] queue];
        self.originalEvent = nil;
    }else if(self.eventEditorType == kEventEditorAddEvent) {
        [[[ASSyncEvent alloc] initWithEvent:self.event delegate:self isAdd:TRUE] queue];
        self.event = nil;
    }
}

- (void)splitEvent;
{
    if(self.originalEvent.recurrence) {
        // NOTE - If this event is in the past should I split at the specific date that was chosen
        // or should the change only apply to recurrences after the current date
        //
        Calendar* aCalendar = [Calendar calendar];
        if(!self.actualStartTime) {
            self.actualStartTime = [aCalendar timelessDate:[NSDate date]];
        }
        
        // Set untilDate on the old event and terminate it on this date
        //
        self.originalEvent.recurrence.untilDate = [aCalendar addDays:-1 toDate:self.actualStartTime];
        self.originalEvent.recurrence.occurences = 0;
        [[[ASSyncEvent alloc] initWithEvent:self.originalEvent delegate:self isAdd:FALSE] queue];

        // Create a new event starting at this date and start time
        //
        NSDateComponents* aComponents = [aCalendar datelessTimeComponents:self.event.startDate];
        [self changeStartDate:[aCalendar dateByAddingComponents:aComponents toDate:self.actualStartTime]];
        [[[ASSyncEvent alloc] initWithEvent:self.event delegate:self isAdd:TRUE] queue];
    }else{
        FXDebugLog(kFXLogActiveSync, @"splitEvent invalid: %@", self.originalEvent);
    }
}

- (void)_createException
{
    Event* orig = self.originalEvent;
    Event* edit = self.event;
    
    EventException* anException = [[EventException alloc] init];
    anException.exceptionStartTime  = orig.startDate;
    anException.startDate           = edit.startDate;
    anException.endDate             = edit.endDate;
    
    if(![orig.title isEqualToString:edit.title]) {
        anException.title = edit.title;
    }
    
    if(![orig.location isEqualToString:edit.location]) {
        anException.location = edit.location;
    }

    [edit.recurrence addException:anException];
    
    [self _syncEvent];
}

////////////////////////////////////////////////////////////////////////////////
// Actions from NavBar
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions from NavBar

- (void)dismiss
{
    [SVProgressHUD dismiss];
    if(IS_IPAD() && self.popover) {
        [self.popover dismissPopoverAnimated:TRUE];
        [self.popover.delegate popoverControllerDidDismissPopover:self.popover];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)done
{
    [SVProgressHUD showWithStatus:@"Saving Event"];
    
    if(currentTextField) {
        [currentTextField resignFirstResponder];
    }else if(currentTextView) {
        [currentTextView resignFirstResponder];
    }
    
    if(self.eventEditorType == kEventEditorAddEvent || [self eventChanged]) {
        if([self isExceptionAction]) {
            [self commitEventChanges];
        }else{
            [self _syncEvent];
        }
    }else{
        [self dismiss];
    }
}

- (void)cancel
{
    [self dismiss];
}

- (void)deleteEvent
{
    [SVProgressHUD showWithStatus:@"Deleting Event"];

    if(self.event.attendees.count > 0) {
        [self _prepareCancel];
    }
    if(self.event.uid.length > 0) {
        [self.event.folder deleteUid:self.event.uid updateServer:TRUE];
        [self.event.folder commitObjects];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Event has no uid so can't be deleted");
    }
    [self dismiss];
}

- (void)editEvent
{
    isEditing = TRUE;
    [self buildRightNavBar];
    [self.tableView reloadData];
}

- (void)pushViewController:(UIViewController*)aViewController
{
    isPush = TRUE;
    if(currentTextField) {
        [currentTextField resignFirstResponder];
    }
    aViewController.title = self.event.title;
    [self.navigationController pushViewController:aViewController animated:YES];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Event Changes
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Changes

- (BOOL)meetingChanged:(Event*)anOrig edited:(Event*)anEdit
{
    BOOL aChanged = FALSE;
    
    if(anOrig.attendees.count != anEdit.attendees.count) {
        aChanged = TRUE;
    }else{
        NSUInteger aCount = anOrig.attendees.count;
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            Attendee* anAttendee1 = [anOrig.attendees objectAtIndex:i];
            Attendee* anAttendee2 = [anEdit.attendees objectAtIndex:i];
            if(![anAttendee1.name isEqualToString:anAttendee2.name]) {
                aChanged = TRUE;
            }
            if(![anAttendee1.address isEqualToString:anAttendee2.address]) {
                aChanged = TRUE;
            }
            if(anAttendee1.attendeeStatus != anAttendee2.attendeeStatus) {
                aChanged = TRUE;
            }
            if(anAttendee1.attendeeType != anAttendee2.attendeeType) {
                aChanged = TRUE;
            }
        }
    }
    
    return aChanged;
}

- (BOOL)recurrenceChanged:(Event*)anOrig edited:(Event*)anEdit
{
    BOOL aChanged = FALSE;
    
    if(anOrig.recurrence && anEdit.recurrence) {
        EventRecurrence* aRecurrence1 = anOrig.recurrence;
        EventRecurrence* aRecurrence2 = anEdit.recurrence;
        
        if(aRecurrence1.recurrenceType != aRecurrence2.recurrenceType) {
            aChanged = TRUE;
        }
        if(aRecurrence1.interval != aRecurrence2.interval) {
            aChanged = TRUE;
        }
        if(aRecurrence1.firstDayOfWeek != aRecurrence2.firstDayOfWeek) {
            aChanged = TRUE;
        }
        if(aRecurrence1.dayOfWeek != aRecurrence2.dayOfWeek) {
            aChanged = TRUE;
        }
        if(aRecurrence1.weekOfMonth != aRecurrence2.weekOfMonth) {
            aChanged = TRUE;
        }
        if(aRecurrence1.monthOfYear != aRecurrence2.monthOfYear) {
            aChanged = TRUE;
        }
        if(aRecurrence1.dayOfMonth != aRecurrence2.dayOfMonth) {
            aChanged = TRUE;
        }
        if(aRecurrence1.occurences != aRecurrence2.occurences) {
            aChanged = TRUE;
        }
        if([aRecurrence1.untilDate compare:aRecurrence2.untilDate] != NSOrderedSame) {
            aChanged = TRUE;
        }
        if(![aRecurrence1.startTimelessDate isEqualToDate:aRecurrence2.startTimelessDate]) {
            aChanged = TRUE;
        }
    }else if(anOrig.recurrence  && !anEdit.recurrence) {
        aChanged = TRUE;
    }else if(!anOrig.recurrence  && anEdit.recurrence) {
        aChanged = TRUE;
    }
    return aChanged;
}

- (BOOL)eventChanged
{
    BOOL aChanged = FALSE;
    
    // Note: You could obviously return the first time a change is detected to
    // improve efficiency, but eventually we will probably need to take specific
    // actions when certain fields are changed, attendee's for meetings for example
    // which is why this is like it is
    //
    Event* orig = self.originalEvent;
    Event* edit = self.event;
    if([orig.startDate compare:edit.startDate] != NSOrderedSame) {
        aChanged = TRUE;
    }
    
    if([orig.endDate compare:edit.endDate] != NSOrderedSame) {
        aChanged = TRUE;
    }
    
    if(![orig.title isEqualToString:edit.title]) {
        aChanged = TRUE;
    }
    
    if(![orig.location isEqualToString:edit.location]) {
        aChanged = TRUE;
    }
    
    if(![orig.organizerName isEqualToString:edit.organizerName]) {
        aChanged = TRUE;
    }
    
    if(![orig.organizerEmail isEqualToString:edit.organizerEmail]) {
        aChanged = TRUE;
    }
    
    if(![orig.notes isEqualToString:edit.notes]) {
        aChanged = TRUE;
    }
    
    if(orig.reminderMinutes != edit.reminderMinutes) {
        aChanged = TRUE;
    }
    
    if(orig.flags != edit.flags) {
        aChanged = TRUE;
    }
    
    // Meeting
    //
    if([self meetingChanged:orig edited:edit]) {
        aChanged = TRUE;
    }
    
    // Recurrence
    //
    if([self recurrenceChanged:orig edited:edit]) {
        aChanged = TRUE;
    }
    
    return aChanged;
}

- (BOOL)isExceptionAction
{
    return (self.event.recurrence
            && self.event.recurrence.recurrenceType != kRecurrenceNone
            && self.eventEditorType != kEventEditorAddEvent);
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{    
    if([self isExceptionAction]) {
        switch((ERecurreringEventActions)buttonIndex) {
            case kCancelChanges:
                break;
            case kOnlyThisEvent:
                [self _createException];
                break;
            case kFollowingEvents:
                [self splitEvent];
                break;
            case kAllEvents:
                [self _syncEvent];
                break;
        }
    }else{
        switch((EEventActions)buttonIndex) {
            case kEventCancelChanges:
                break;
            case kEventSave:
                [self _syncEvent];
                break;
        }
    }
    [self dismiss];
}

- (void)commitEventChanges
{
    if(currentTextField) {
        [currentTextField resignFirstResponder];
    }

    // If we are editing an existing event and it hasn't changed we want to bail out
    // without bothering the user with actions
    //
    if(self.eventEditorType != kEventEditorAddEvent && ![self eventChanged]) {
        return;
    }

    UIActionSheet* anActionSheet;
    if([self isExceptionAction]) {
        // This is a recurring event so we need to give the user more options, to create an
        // exception or close out the old event recurrence and create a new one
        //
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_CHANGES", @"EventEditController", @"Alert view cancel button title when commiting event changes")
                                           otherButtonTitles:
                                                        FXLLocalizedStringFromTable(@"CHANGE_ONLY_THIS_EVENT", @"EventEditController", @"Alert view button title when commiting event changes to only change this event"),
                                                        FXLLocalizedStringFromTable(@"CHANGE_FOLLOWING_EVENTS", @"EventEditController", @"Alert view button title when commiting event changes to only change this event"),
                                                        FXLLocalizedStringFromTable(@"CHANGE_ALL_EVENTS", @"EventEditController", @"Alert view button title when commiting event changes to only change this event"),
                         nil];
    }else{
        // This is a non recurring event so its simpler, we cancel or just save it
        //
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:FXLLocalizedStringFromTable(@"CANCEL_CHANGES", @"EventEditController", @"Alert view cancel button title when commiting event changes")
                                           otherButtonTitles:
                         FXLLocalizedStringFromTable(@"SAVE", @"EventEditController", @"Alert view save button title when commiting event changes"),
                         nil];
    }
    if(isIPad) {
        [anActionSheet showInView:self.view];
    }else{
        [anActionSheet showFromToolbar:self.navigationController.toolbar];
    }
}

- (void)commitEventChangesFromToolbar
{
    [self commitEventChanges];
    [self dismiss];
}

- (void)_updateDates
{
    NSIndexPath* anIndexPathStart = [NSIndexPath indexPathForRow:kEventStartDate inSection:kSectionDate];
    NSIndexPath* anIndexPathEnd = [NSIndexPath indexPathForRow:kEventEndDate inSection:kSectionDate];
    NSArray* anArray =  [NSArray arrayWithObjects:anIndexPathStart, anIndexPathEnd, nil];
    [self.tableView reloadRowsAtIndexPaths:anArray withRowAnimation:UITableViewRowAnimationAutomatic];
    //[self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:field inSection:0] animated:TRUE scrollPosition:FALSE];
}

- (void)changeStartDate:(NSDate*)aDate
{
    if(self.event.recurrence) {
        // FIXME if date has changed do need to change recurrence options and settings
    }
    self.event.endDate = [self.event endDateForStartDate:aDate];
    self.event.startDate = aDate; // Must follow the endDate adjustment above
    [self _updateDates];
}

- (void)changeEndDate:(NSDate*)aDate
{
    if(self.event.recurrence) {
        // FIXME if date has changed do need to change recurrence options and settings
    }
    
    // If end date is before start date assume user wants to move start date and maintain
    // event duration
    //
    if([aDate compare:event.startDate] == NSOrderedAscending) {
        event.startDate = [event startDateForEndDate:aDate];
    }
    self.event.endDate = aDate; // Must follow the startDate adjustment above

    [self _updateDates];
}

- (void)titleEditingEnded:(UITextField*)aTextField
{
    NSString* aString = [aTextField text];
    event.title = aString;
    currentTextField = nil;
}

- (void)locationEditingEnded:(UITextField*)aTextField
{
    NSString* aString = [aTextField text];
    event.location = aString;
    currentTextField = nil;
}

- (void)recipientFieldChanged:(UITextField*)aTextField
{
    NSString* aString = [aTextField text];
    if(aString.length > 0) {
        if(!recipientPicker) {
            CGRect aRect = [[[aTextField superview] superview] frame];
            [self recipientPickerPopover:aRect];
        }
    }
    [self.recipientPicker search:aString];
}

- (void)recipientFieldEditingEnded:(UITextField*)aTextField
{
    [self recipientDismiss];
    currentTextField = nil;
}

////////////////////////////////////////////////////////////////////////////////
// TextField Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark TextField Utilities

- (void)editingDidBegin:(UITextField*)aTextField
{
    currentTextField = aTextField;
}

- (UITableViewCell*)_textFieldCellWithString:(NSString*)aText placeHolder:(NSString*)aPlaceHolder
{
    UITextField* aTextField;
    UITableViewCell* aCell = [self.tableView dequeueReusableCellWithIdentifier:kTextFieldCell];
    if( aCell == nil ) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kTextFieldCell];
        CGRect aRect = [aCell contentView].frame;
        CGFloat kTextFieldOffsetX;
        if(isIPad) {
            kTextFieldOffsetX = 4.0f;
        }else{
            kTextFieldOffsetX = 12.0f;
        }
        aRect.origin.x      += kTextFieldOffsetX;
        aRect.size.width    -= kTextFieldOffsetX;

        aTextField = [[UITextField alloc] initWithFrame:aRect];    
        aTextField.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        aTextField.autoresizesSubviews      = YES;
        aTextField.borderStyle              = UITextBorderStyleNone;
        aTextField.font                     = [UIFont boldSystemFontOfSize:17];
        aTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        aTextField.clearButtonMode          = UITextFieldViewModeAlways;
        [[aCell contentView] addSubview:aTextField];
        
        aCell.textLabel.text         = @"";
        aCell.detailTextLabel.text   = @"";
        aCell.accessoryType          = UITableViewCellAccessoryNone;
        
    }else{
        aTextField = [[[aCell contentView] subviews] objectAtIndex:0];
    }
    aTextField.text         = aText;
    aTextField.placeholder  = aPlaceHolder;

    [aTextField removeTarget:self action:nil forControlEvents:UIControlEventAllEvents];
    [aTextField addTarget:self action:@selector(editingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];

    return aCell;
}

- (UITableViewCell*)_textViewCellWithString:(NSString*)aText
{
    UITextView* aTextView;
    UITableViewCell* aCell = [self.tableView dequeueReusableCellWithIdentifier:kTextViewCell];
    if( aCell == nil ) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kTextViewCell];
        static const CGFloat kOffsetNoteView = 2.5f;
        CGRect aRect = [aCell contentView].frame;
        aRect.origin.x += kOffsetNoteView;
        aRect.origin.y += kOffsetNoteView;
        
        aRect.size.height = kNotesRowHeight - kOffsetNoteView*2.0f;
        aRect.size.width -= kOffsetNoteView*2.0f;
        UITextView* aTextView = [[UITextView alloc] initWithFrame:aRect];
        aTextView.delegate                 = self;
        aTextView.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        aTextView.autoresizesSubviews      = YES;
        aTextView.font                     = [UIFont boldSystemFontOfSize:17];
        [[aCell contentView] addSubview:aTextView];
        
        aCell.textLabel.text         = @"";
        aCell.detailTextLabel.text   = @"";
        aCell.accessoryType          = UITableViewCellAccessoryNone;
    }else{
        aTextView = [[[aCell contentView] subviews] objectAtIndex:0];
    }
    aTextView.text = aText;

    return aCell;
}

////////////////////////////////////////////////////////////////////////////////
// Recipient Picker
////////////////////////////////////////////////////////////////////////////////
#pragma mark Recipient Picker

- (void)recipientPickerPopover:(CGRect)aCellRect
{
    RecipientViewController* aRecipientPicker = [[RecipientViewController alloc] initWithNibName:@"RecipientView" bundle:[FXLSafeZone getResourceBundle]];
    aRecipientPicker.delegate = self;
    CGRect aRect = aRecipientPicker.view.frame;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        aRecipientPicker.contentSizeForViewInPopover = aRect.size;
        self.recipientPopover = [[UIPopoverController alloc] initWithContentViewController:aRecipientPicker];
        aRect.origin.y      = aCellRect.origin.y;
        aRect.origin.x      = 100;
        aRect.size.width    = 10;
        aRect.size.height   = 10;
        recipientPopoverRect = aRect;
        self.recipientPopover.delegate = aRecipientPicker;
    }else{
        aRect.origin.y      = aCellRect.origin.y - aRect.size.height;
        UIView* aView = [[UIView alloc] initWithFrame:aRect];
        aView.layer.borderColor = [UIColor darkGrayColor].CGColor;
        aView.layer.borderWidth = 3.0f;
        self.recipientView = aView;
        [self.recipientView addSubview:aRecipientPicker.view];
    }
    self.recipientPicker = aRecipientPicker;
}

////////////////////////////////////////////////////////////////////////////////
// ASComposeDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASComposeDelegate

- (void)sendMailSuccess
{
    FXDebugLog(kFXLogActiveSync, @"sendMailSuccess");
}

- (void)sendMailFailed:(NSString *)aMessage
{
    // FIXME - What to do here, let user resend or retry automatically
    //
    [self handleError:aMessage];
}

////////////////////////////////////////////////////////////////////////////////
// Attendee Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Attendee Utilities


- (void)addAttendee:(Attendee*)anAttendee
{
    NSUInteger aCount = self.event.attendees.count;
    [self.event addAttendee:anAttendee];
    
    NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:aCount+1 inSection:kSectionInvitees];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    anIndexPath = [NSIndexPath indexPathForRow:aCount inSection:kSectionInvitees];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)emailAttendee:(Attendee*)anAttendee
{
    NSString* aToString = [anAttendee asToString];
    if(aToString.length > 0) {
        NSArray* aToArray = [NSArray arrayWithObject:aToString];
        composeMailVC* aComposeViewController = [composeMailVC createMailComposerWithTo:aToArray cc:nil bcc:nil attachments:nil account:(BaseAccount*)self.event.folder.account];
        [self pushViewController:aComposeViewController];
    }else{
        FXDebugLog(kFXLogActiveSync, @"emailAttendee invalid: %@", anAttendee);
    }
}

- (void)deleteAttendee:(Attendee*)anAttendee
{
    NSUInteger anIndex = [self.event.attendees indexOfObject:anAttendee];
    if(anIndex != NSNotFound) {
        [self.event.attendees removeObject:anAttendee];
        NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:anIndex inSection:kSectionInvitees];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
#warning FIXME need to send disinvite
    }
}

- (Attendee*)attendeeForSelf
{
    for(Attendee* anAttendee in self.event.attendees) {
        if(anAttendee.attendeeSort == kAttendeeSortSelf) {
            return anAttendee;
        }
    }
    FXDebugLog(kFXLogActiveSync, @"attendeeForSelf invalid");
    return nil;
}

- (Attendee*)attendeeForOrganizer
{
    for(Attendee* anAttendee in self.event.attendees) {
        if(anAttendee.attendeeSort == kAttendeeSortOrganizer) {
            return anAttendee;
        }
    }
    FXDebugLog(kFXLogActiveSync, @"attendeeForOrganizer invalid");
    return nil;
}

- (void)response:(UISegmentedControl*)aSegmentedControl
{
    Attendee* anAttendee = [self attendeeForSelf];
    switch(aSegmentedControl.selectedSegmentIndex)
    {
        case kResponseAccept:
            anAttendee.attendeeStatus = kAttendeeResponseAccept;
            break;
        case kResponseTentative:
            anAttendee.attendeeStatus = kAttendeeResponseTentative;
            break;
        case kResponseDecline:
            anAttendee.attendeeStatus = kAttendeeResponseDecline;
            break;
    }
}

- (void)setResponse:(Attendee*)anAttendee
{
    if(anAttendee) {
        switch(anAttendee.attendeeStatus) {
            case kAttendeeResponseAccept:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseAccept;
                break;
            case kAttendeeResponseDecline:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseDecline;
                break;
            case kAttendeeResponseTentative:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseTentative;
                break;
            case kAttendeeResponseNotResponded:
            case kAttendeeResponseUnknown:
                self.meetingSegmentedControl.selectedSegmentIndex = -1;
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Meeting Invitations
////////////////////////////////////////////////////////////////////////////////
#pragma mark Meeting Invitations

- (void)sendICalToAttendee:(Attendee*)anAttendee iCalString:(NSString*)anIcalString
{
    NSString* aTextString = [self.event asString];
    //FXDebugLog(kFXLogActiveSync, @"sendICalToAttendee: %@\n%@\n%@", anAttendee, aTextString, anIcalString);

    NSString* aMessageString = [CalendarEmail calendarAsMIMEToAttendee:anAttendee
                                                               subject:self.event.title
                                                                  body:aTextString
                                                            iCalString:anIcalString];
    //FXDebugLog(kFXLogActiveSync, @"anInvitation: %@", aMessageString);
    
    ASAccount* anAccount = (ASAccount*)self.event.folder.account;
    [ASCompose sendMail:anAccount string:aMessageString delegate:self];
}

////////////////////////////////////////////////////////////////////////////////
// Invitation utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Invitation utilities

- (void)sendInvitations:(NSArray*)anAttendees
{
    NSString* anIcalString = [self.event asICalendar:kICalMethodRequest status:kICalStatusConfirmed];

    //FXDebugLog(kFXLogActiveSync, @"send ical: %@", aString);
    
    // Send invitatopm to each attendee
    //
    for(Attendee* anAttendee in anAttendees) {
        [self sendICalToAttendee:anAttendee iCalString:anIcalString];
    }
}

- (NSInteger)sendInvitationsAlert:(NSString*)aMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"SEND_INVITATIONS", @"EventEditController", @"AlertView title for prompting whether to send invitations")
                                                          message:aMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(kCancel, @"EventEditController", @"Cancel button title")
                                                otherButtonTitles:FXLLocalizedStringFromTable(kSend, @"EventEditController", @"Send button title"), nil];
    return [anAlertView showModal];
}

- (void)_prepareInvites
{
#warning FIXME actually need to figure out what changed in the invite list and only send invites where actual changes occured, (i.e. who got added, who got deleted, etc.)
    
    if(self.event.attendees.count > 0) {
        NSMutableString* aSendString = [NSMutableString stringWithString:@""];
        for(Attendee* anAttendee in self.event.attendees) {
            if(anAttendee.address.length > 0) {
                if(anAttendee.name.length > 0) {
                    [aSendString appendFormat:@"%@\n", anAttendee.name];
                }else{
                    [aSendString appendFormat:@"%@\n", anAttendee.address];
                }
            }else{
                FXDebugLog(kFXLogActiveSync, @"_prepareCancel attendee invalid: %@", anAttendee);
            }
        }
        if(aSendString.length > 0) {
            if([self sendInvitationsAlert:aSendString] == 1) {
                [self sendInvitations:self.event.attendees];
            }else{
                // Canceled sending invitations
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"_prepareCancel invalid");
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Cancel utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Cancel utilities

- (void)sendCancelToAttendee:(Attendee*)anAttendee iCalString:(NSString*)anIcalString
{
    NSString* aTextString = [self.event asString];
    FXDebugLog(kFXLogActiveSync, @"sendICalToAttendee: %@\n%@\n%@", anAttendee, aTextString, anIcalString);
    
    NSString* aSubject = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"CANCEL:_SEND_CANCEL_TO_ATTENDEES", @"EventEditController", @"Title of field for choosing date when event is all day"), self.event.title];
    
    NSString* aMessageString = [CalendarEmail calendarAsMIMEToAttendee:anAttendee
                                                               subject:aSubject
                                                                  body:aTextString
                                                            iCalString:anIcalString];
    FXDebugLog(kFXLogActiveSync, @"anInvitation: %@", aMessageString);
    
    ASAccount* anAccount = (ASAccount*)self.event.folder.account;
    [ASCompose sendMail:anAccount string:aMessageString delegate:self];
}

- (void)sendCancel:(NSArray*)anAttendees
{
    NSString* anIcalString = [self.event asICalendar:kICalMethodCancel status:kICalStatusConfirmed];
    
    // Send cancel to each attendee
    //
    for(Attendee* anAttendee in anAttendees) {
        [self sendCancelToAttendee:anAttendee iCalString:anIcalString];
    }
}

- (NSInteger)sendCancelAlert:(NSString*)aMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"SEND_CANCEL", @"EventEditController", @"Alertview title to send Cancel Alert")
                                                          message:aMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(kCancel, @"EventEditController", @"Cancel button title")
                                                otherButtonTitles:FXLLocalizedStringFromTable(kSend, @"EventEditController", @"Send button title"), nil];
    return [anAlertView showModal];
}

- (void)_prepareCancel
{
    if(self.event.attendees.count > 0) {
        NSMutableString* aSendString = [NSMutableString stringWithString:@""];
        for(Attendee* anAttendee in self.event.attendees) {
            if(anAttendee.address.length > 0) {
                if(anAttendee.name.length > 0) {
                    [aSendString appendFormat:@"%@\n", anAttendee.name];
                }else{
                    [aSendString appendFormat:@"%@\n", anAttendee.address];
                }
            }else{
                FXDebugLog(kFXLogActiveSync, @"_prepareCancel attendee invalid: %@", anAttendee);
            }
        }
        if(aSendString.length > 0) {
            if([self sendCancelAlert:aSendString] == 1) {
                [self sendCancel:self.event.attendees];
            }else{
                // Canceled sending cancel
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"_prepareCancel invalid");
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Cell utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Cell utilities

- (void)startDateCell:(UITableViewCell*)aCell
{
    NSDate* aDate;
    if(self.actualStartTime) {
        aDate = self.actualStartTime;
    }else{
        aDate = self.event.startDate;
    }
    NSString* aDateString;
    if(self.event.isAllDay) {
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"DATE", @"EventEditController", @"Title of field for choosing date when event is all day");
        aDateString = [[Event dateFormatterTimeless] stringFromDate:aDate];
    }else{
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"START", @"EventEditController", @"Title of field for choosing start date for event");
        aDateString = [[Event dateFormatter] stringFromDate:aDate];
    }
    aCell.detailTextLabel.text   = aDateString;
    aCell.accessoryType          = UITableViewCellAccessoryNone;
}

- (void)endDateCell:(UITableViewCell*)aCell
{
    NSDate* aDate;
    if(self.actualStartTime) {
        NSDate* aTimelessDate = [[Calendar calendar] timelessDate:self.actualStartTime];
        aDate = [self.event endTimeForDate:aTimelessDate];
    }else{
        aDate = self.event.endDate;
    }
    NSString* aDateString;
    if(self.event.isAllDay) {
        aCell.textLabel.text         = @"";
        aDateString = @"";
    }else{
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"END", @"EventEditController", @"Title of field for choosing end date for event");
        aDateString = [[Event dateFormatter] stringFromDate:aDate];
    }
    aCell.detailTextLabel.text   = aDateString;
    aCell.accessoryType          = UITableViewCellAccessoryNone;
}

- (UITableViewCell*)allDaySwitchCell:(UITableView*)aTableView
{
    UITableViewCell* aCell = [aTableView dequeueReusableCellWithIdentifier:kSwitchCell];
    if( aCell == nil ) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kSwitchCell];
        aCell.textLabel.text = FXLLocalizedStringFromTable(@"ALL_DAY", @"EventEditController", @"Title of switch for choosing whether event lasts all day or not");
        aCell.selectionStyle = UITableViewCellSelectionStyleNone;
        allDaySwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        aCell.accessoryView  = allDaySwitch;
        [allDaySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    }
    [allDaySwitch setOn:self.event.isAllDay animated:NO];
    allDaySwitch.enabled = isEditing;
    
    return aCell;
}

- (void)switchChanged:(id)sender
{
    UISwitch* aSwitch = sender;
    BOOL isAllDay = aSwitch.on;
    if(isAllDay != self.event.isAllDay) {
        self.event.isAllDay = isAllDay;
        [self.tableView reloadData];
    }
}

////////////////////////////////////////////////////////////////////////////////
// ASSyncEventDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark ASSyncEventDelegate

- (void)addEvent:(Event*)anEvent
{
    // This is usually the same event being sent back to us from ASSync, at least
    // for new events being added
    //
    self.event = anEvent;
    [self _prepareInvites];
    [self dismiss];
}

- (void)addEventFailed:(Event*)anEvent
            httpStatus:(EHttpStatusCode)aStatusCode
            syncStatus:(ESyncStatus)aSyncStatus
{
    FXDebugLog(kFXLogActiveSync, @"Add event failed: %d %d %@",aStatusCode, aSyncStatus, anEvent);
}

- (void)changedEvent:(Event *)anEvent
{
    //FXDebugLog(kFXLogActiveSync, @"changeComplete: %@", anEvent);
    [anEvent.folder changedObject:anEvent];
    [anEvent.folder commitObjects];
    [self _prepareInvites];
    [self dismiss];
}

////////////////////////////////////////////////////////////////////////////////
// RecipientDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark RecipientDelegate

- (void)recipientShow
{
    if(self.recipientPopover) {
        [self.recipientPopover presentPopoverFromRect:recipientPopoverRect
                                               inView:self.view
                             permittedArrowDirections:UIPopoverArrowDirectionDown
                                             animated:TRUE];
    }else if(self.recipientView) {
        [self.view addSubview:self.recipientView];
    }else{
        FXDebugLog(kFXLogActiveSync, @"recipientShow no recipientView");
    }
}

- (void)recipientDismiss
{
    if(self.recipientPopover) {
        [recipientPopover dismissPopoverAnimated:TRUE];
        self.recipientPopover   = nil;
    }else if(self.recipientView) {
        [self.recipientView removeFromSuperview];
    }else{
        FXDebugLog(kFXLogActiveSync, @"recipientDismiss no recipientView");
    }
    self.recipientPicker    = nil;
}

- (void)recipientNoMatch
{
    [self recipientDismiss];
}

- (void)recipientSelected:(EmailAddress*)aRecipient
{
    [self recipientDismiss];
    
    Attendee* anAttendee = [[Attendee alloc] initWithName:aRecipient.name address:aRecipient.address];
    [self addAttendee:anAttendee];
}

- (void)recipientViewDismissed
{
    self.recipientPopover   = nil;
    self.recipientPicker    = nil;
}

- (void)recipientDelete:(Attendee*)anAttendee
{
    [self deleteAttendee:anAttendee];
}

- (void)recipientEmail:(Attendee*)anAttendee
{
    [self emailAttendee:anAttendee];
}

- (void)recipientInvite:(Attendee*)anAttendee
{
    [self sendInvitations:[NSArray arrayWithObject:anAttendee]];
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* aString = nil;
    switch((EEventSection)section) {
        case kSectionTitleLocation:
            break;
        case kSectionDate:
            break;
        case kSectionAttributes:
            break;
        case kSectionInvitees:
            aString = FXLLocalizedStringFromTable(@"MEETING", @"EventEditController", @"Title of Invitees section asking maybe/decline/accept (Meeting)");
            break;
        case kSectionNotes:
            aString = FXLLocalizedStringFromTable(@"NOTES", @"EventEditController", @"Title of Notes section");
            break;
        case kSectionCalendar:
            break;
        case kSectionLast:
            break;
    }
    return aString;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kSectionLast;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRows = 0;
    
	switch((EEventSection)section) {
        case kSectionTitleLocation:
            numRows = 2;
            break;
        case kSectionDate:
            if(self.event.isAllDay) {
                numRows = 2;
            }else{
                numRows = 3;
            }
            break;
        case kSectionAttributes:
            numRows = 3;
            break;
        case kSectionInvitees:
        {
            numRows = event.attendees.count;
            if(isEditing) {
                numRows++;
            }
            break;
        }
        case kSectionNotes:
            numRows = 1;
            break;
        case kSectionCalendar:
            numRows = 1;    // Calendar row not implemented, just timezone
            break;
        case kSectionLast:
            break;
    }
    return numRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == kSectionNotes) {
        return kNotesRowHeight;
    }else{
        return self.tableView.rowHeight;
    }
}

- (UITableViewCell*)standardCell
{
    UITableViewCell* aCell = [self.tableView dequeueReusableCellWithIdentifier:kEventEditCell];
    if(aCell == nil) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kEventEditCell];
    }
    return aCell;
}

- (UITableViewCell*)meetingCell
{
    UITableViewCell* aCell = [self.tableView dequeueReusableCellWithIdentifier:kMeetingCell];
    if(aCell == nil) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kMeetingCell];
        
        NSArray* aTitles = [Event attendeeResponseStrings];
        self.meetingSegmentedControl = [[UISegmentedControl alloc] initWithItems:aTitles];
        [self.meetingSegmentedControl addTarget:self action:@selector(response:) forControlEvents:UIControlEventValueChanged];
        self.meetingSegmentedControl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        
        CGRect aRect = [aCell contentView].frame;
        self.meetingSegmentedControl.frame = aRect;

        [[aCell contentView] addSubview:self.meetingSegmentedControl];
        
        aCell.textLabel.text         = @"";
        aCell.detailTextLabel.text   = @"";
        aCell.accessoryType          = UITableViewCellAccessoryNone;
    }
    return aCell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* aCell;
    
	switch((EEventSection)indexPath.section) {
        case kSectionTitleLocation:
            switch((ETitleLocation)indexPath.row) {
                case kEventTitle:
                    if(isEditing) {
                        aCell = [self _textFieldCellWithString:self.event.title placeHolder:FXLLocalizedStringFromTable(@"TITLE", @"EventEditController", @"Placeholder for title field when editing event")];
                        UITextField* aTextField = [[aCell contentView].subviews objectAtIndex:0];
                        [aTextField addTarget:self action:@selector(titleEditingEnded:) forControlEvents:UIControlEventEditingDidEnd];
                    }else{
                        aCell = [self standardCell];
                        aCell.textLabel.text         = self.event.title;
                        aCell.detailTextLabel.text   = @"";
                        aCell.accessoryType          = UITableViewCellAccessoryNone;
                    }
                    break;
                case kEventLocation:
                    if(isEditing) {
                        aCell = [self _textFieldCellWithString:self.event.location placeHolder:FXLLocalizedStringFromTable(@"LOCATION", @"EventEditController", @"Placeholder for location field when editing event")];
                        UITextField* aTextField = [[aCell contentView].subviews objectAtIndex:0];
                        [aTextField addTarget:self action:@selector(locationEditingEnded:) forControlEvents:UIControlEventEditingDidEnd];
                    }else{
                        aCell = [self standardCell];
                        aCell.textLabel.text         = self.event.location;
                        aCell.detailTextLabel.text   = @"";
                        aCell.accessoryType          = UITableViewCellAccessoryNone;
                    }
                    break;
            }
            break;
        case kSectionDate:
            switch((EDate)indexPath.row) {
                case kEventStartDate:
                    aCell = [self standardCell];
                    [self startDateCell:aCell];
                    break;
                case kEventEndDate:
                    if(self.event.isAllDay) {
                        aCell = [self allDaySwitchCell:tableView];
                    }else{
                        aCell = [self standardCell];
                        [self endDateCell:aCell];
                    }
                    break;
                case kEventAllDay:
                    aCell = [self allDaySwitchCell:tableView];
                    break;
            }
            break;
        case kSectionAttributes:
            switch((EAttributes)indexPath.row) {
                case kAttributeRecurrence:
                    aCell = [self standardCell];
                    aCell.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
                    aCell.textLabel.text         = FXLLocalizedStringFromTable(@"RECURRENCE", @"EventEditController", @"Title of field specifying how often event recurs");
                    if(event.recurrence) {
                        aCell.detailTextLabel.text   =  [self.event.recurrence typeAsString];
                    }else{
                        aCell.detailTextLabel.text   = FXLLocalizedStringFromTable(@"NO", @"EventEditController", @"Detail of field specifying recurrence of event when event does not recur");
                    }
                    aCell.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
                    break;
                case kAttributeReminder:
                {
                    aCell = [self standardCell];
                    aCell.textLabel.text         = FXLLocalizedStringFromTable(@"REMINDER", @"EventEditController", @"Title of field specifying when to remind user about the event");
                    if(event.reminderMinutes > 0) {
                        aCell.detailTextLabel.text = [NSString stringWithFormat:FXLLocalizedStringFromTable(@"%d_MINUTES", @"EventEditController", @"Detail of field specifying how many minutes before event user should be reminded"), event.reminderMinutes];
                    }else if(event.reminderMinutes == 0) {
                        aCell.detailTextLabel.text = FXLLocalizedStringFromTable(@"AT_START", @"EventEditController", @"Detail of field specifying that user should be reminded at start of event");
                    }else{
                        aCell.detailTextLabel.text = FXLLocalizedStringFromTable(@"NONE_NO_REMINDER", @"EventEditController", @"Detail of reminder field specifying that no reminder is specified for the event");
                    }
                    aCell.accessoryType          = UITableViewCellAccessoryNone;
                    return aCell;    // This is always editable
                }
                case kAttributeAvailability:
                {
                    aCell = [self standardCell];
                    NSString* aString;
                    switch(event.busyStatus) {
                        case kBusyStatusBusy:
                            aString = FXLLocalizedStringFromTable(@"BUSY", @"Calendar", @"Displayed availability - busy"); break;
                        case kBusyStatusFree:
                            aString = FXLLocalizedStringFromTable(@"FREE", @"Calendar", @"Displayed availability - free"); break;
                        case kBusyStatusOutOfOffice:
                            aString = FXLLocalizedStringFromTable(@"OUT_OF_OFFICE", @"Calendar", @"Displayed availability - out of office"); break;
                        case kBusyStatusTentative:
                            aString = FXLLocalizedStringFromTable(@"TENTATIVE", @"Calendar", @"Displayed availability - tentative");; break;
                    }
                    aCell.accessoryType          = UITableViewCellAccessoryNone;
                    aCell.textLabel.text         = FXLLocalizedStringFromTable(@"AVAILABILITY", @"Calendar", @"Displayed availability - availability");
                    aCell.detailTextLabel.text   = aString;
                    return aCell;    // This is always editable
                }
            }
            break;
        case kSectionInvitees:
        {
            if(indexPath.row < event.attendees.count) {
                Attendee* anAttendee        = [event.attendees objectAtIndex:indexPath.row];
                if(anAttendee.attendeeSort == kAttendeeSortSelf) {
                    aCell = [self meetingCell];
                    [self setResponse:anAttendee];
                }else{
                    aCell = [self standardCell];
                    
                    aCell.accessoryType          = UITableViewCellAccessoryNone;
                    if(anAttendee.name.length > 0) {
                        aCell.textLabel.text         = anAttendee.name;
                    }else{
                        aCell.textLabel.text         = anAttendee.address;
                    }
                    
                    NSString* aStatus;
                    switch(anAttendee.attendeeSort) {
                        case kAttendeeSortSelf:
                            // Not reachable
                            break;
                        case kAttendeeSortOrganizer:
                            aStatus = FXLLocalizedStringFromTable(@"ORGANIZER", @"EventEditController", @"Attendee sort - organizer"); break;
                        case kAttendeeSortResource:
                            aStatus = FXLLocalizedStringFromTable(@"RESOURCE", @"EventEditController", @"Attendee sort - resource"); break;
                        case kAttendeeSortAccepted:
                            aStatus = FXLLocalizedStringFromTable(@"ACCEPTED", @"EventEditController", @"Attendee sort - accepted"); break;
                        case kAttendeeSortDeclined:
                            aStatus = FXLLocalizedStringFromTable(@"DECLINED", @"EventEditController", @"Attendee sort - declined"); break;
                        case kAttendeeSortNotResponded:
                            aStatus = FXLLocalizedStringFromTable(@"NO_RESPONSE", @"EventEditController", @"Attendee sort - no response"); break;
                        case kAttendeeSortTentative:
                            aStatus = FXLLocalizedStringFromTable(@"TENTATIVE", @"EventEditController", @"Attendee sort - tentative"); break;
                    }
                    aCell.detailTextLabel.text   = aStatus;
                    aCell.accessoryType          = UITableViewCellAccessoryNone;
                }
            }else{
                aCell = [self _textFieldCellWithString:@"" placeHolder:FXLLocalizedStringFromTable(@"ATTENDEE_PLACEHOLDER", @"EventEditController", @"Placeholder for empty attendee field")];
                UITextField* aTextField = [[aCell contentView].subviews objectAtIndex:0];
                [aTextField addTarget:self action:@selector(recipientFieldChanged:)         forControlEvents:UIControlEventEditingChanged];
                [aTextField addTarget:self action:@selector(recipientFieldEditingEnded:)    forControlEvents:UIControlEventEditingDidEnd];
            }
            break;
        }
        case kSectionCalendar:
            aCell = [self standardCell];
            switch ((EEventCalendar)indexPath.row) {
                case kEventCalendar:
                    aCell.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
                    aCell.textLabel.text         = FXLLocalizedStringFromTable(@"CALENDAR", @"EventEditController", @"Text label title for Calendar section");
                    aCell.detailTextLabel.text   = FXLLocalizedStringFromTable(@"CALENDAR_DETAIL", @"EventEditController", @"Text label title for Calendar section");
                    break;
                case kEventTimeZone:
                    aCell.textLabel.text         = FXLLocalizedStringFromTable(@"TIMEZONE", @"EventEditController", @"Text label title for Timezone section");
                    aCell.detailTextLabel.text   = [[NSTimeZone localTimeZone] abbreviation];
                    break;
            }
            aCell.accessoryType          = UITableViewCellAccessoryNone;
            break;
        case kSectionNotes:
        {
#warning FIXME need to handle body types which aren't plain text
            aCell = [self _textViewCellWithString:event.notes];
            UITextView* aTextView = [[aCell contentView].subviews objectAtIndex:0];
            aTextView.editable = isEditing;
            aTextView.text = self.event.notes;
            break;
        }
        case kSectionLast:
            break;
    }
    
    return aCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch((EEventSection)indexPath.section) {
        case kSectionTitleLocation:
            break;
        case kSectionDate:
            if(isEditing) {
                UIDatePicker* aDatePicker = nil;
                if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                    DatePickerController* aController;
                    switch((EDate)indexPath.row) {
                        case kEventStartDate:
                        {
                            CGRect location = [tableView rectForRowAtIndexPath:indexPath];
                            aController = [DatePickerController datePickerForViewController:self
                                                                                                     rect:location
                                                                                                     date:self.event.startDate
                                                                                                   action:@selector(changeStartDate:)];
                            break;
                        }
                        case kEventEndDate:
                        {
                            CGRect location = [tableView rectForRowAtIndexPath:indexPath];
                            aController = [DatePickerController datePickerForViewController:self
                                                                                                     rect:location
                                                                                                     date:self.event.endDate
                                                                                                   action:@selector(changeEndDate:)];
                            break;
                        }
                        case kEventAllDay:
                            break;
                    }
                    aDatePicker = aController.datePicker;
                    self.datePickerController = aController;
                }else{
                    DatePickerControllerIPhone* aController = [DatePickerControllerIPhone datePickerForViewController:self
                                                                                                  event:event
                                                                                                  field:(EDatePickerField)indexPath.row];
                    aController.isRecurrenceEdit = FALSE;
                    aController.delegate        = self;
                    aController.actualStartTime = self.actualStartTime;
                    aController.section         = kSectionDate;
                    aDatePicker                 = aController.datePicker;
                    self.datePickerController   = aController;
                    [self pushViewController:aController];
                }
                if(aDatePicker) {
                    if(self.event.isAllDay) {
                        aDatePicker.datePickerMode = UIDatePickerModeDate;
                    }else{
                        aDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
                    }
                }
            }
            break;
        case kSectionAttributes:
            switch((EAttributes)indexPath.row) {
                case kAttributeRecurrence:
                {
                    RecurrenceEditController* aController = [[RecurrenceEditController alloc] initWithNibName:@"RecurrenceEditView" bundle:[FXLSafeZone getResourceBundle]];
                    aController.event = self.event;
                    aController.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
                    [self pushViewController:aController];
                    break;
                }
                case kAttributeReminder:
                {
                    ReminderEditController* aController = [[ReminderEditController alloc] initWithNibName:@"ReminderView" bundle:[FXLSafeZone getResourceBundle]];
                    aController.event = self.event;
                    aController.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
                    [self pushViewController:aController];
                    break;
                }
                case kAttributeAvailability:
                    if(!isIPad) {
                        AvailabilityEditController* aController = [[AvailabilityEditController alloc] initWithNibName:@"AvailabilityView" bundle:[FXLSafeZone getResourceBundle]];
                        aController.event = self.event;
                        aController.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
                        [self pushViewController:aController];
                    }else{
                        self.availabilitySheet = [[AvailabilitySheet alloc] initWithView:self.view
                                                                               event:self.event
                                                                           tableView:tableView
                                                                           indexPath:indexPath];
                    }
                    break;
            }
            break;
        case kSectionInvitees:
            if(indexPath.row < event.attendees.count) {
                self.recipientSheet = [[RecipientSheet alloc] initWithView:self.view
                                                                  delegate:self
                                                                  attendee:[self.event.attendees objectAtIndex:indexPath.row]];
            }
            break;
        case kSectionCalendar:
            switch((EEventCalendar)indexPath.row) {
                case kEventCalendar:
                case kEventTimeZone:
                    break;
            }
            break;
        case kSectionNotes:
            break;
        case kSectionLast:
            break;
    }
}

////////////////////////////////////////////////////////////////////////////////
// UITextViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)aTextView
{
    currentTextView = aTextView;
}

- (void)textViewDidEndEditing:(UITextView *)aTextView
{
    NSString* aString = [aTextView text];
    event.notes = aString;
    currentTextView = nil;
}

////////////////////////////////////////////////////////////////////////////////
// DatePickerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark DatePickerDelegate

- (void)setDate:(NSDate*)aDate section:(int)aSection row:(int)aRow
{
    switch(aRow) {
        case kDatePickerStart:
            event.endDate = [event endDateForStartDate:aDate];
            event.startDate = aDate;
            break;
        case kDatePickerEnd:
            if([aDate compare:event.startDate] == NSOrderedAscending) {
                event.startDate = [event startDateForEndDate:aDate];
            }
            event.endDate   = aDate;
            break;
    }
}

- (NSDate*)dateForSection:(int)aSection row:(int)aRow
{
    NSDate* aDate = nil;
    switch(aRow) {
        case kDatePickerStart:
            if(self.actualStartTime) {
                aDate = self.actualStartTime;
            }else{
                aDate = self.event.startDate;
            }
            break;
        case kDatePickerEnd:
            if(self.actualStartTime) {
                NSDate* aTimelessDate = [[Calendar calendar] timelessDate:self.actualStartTime];
                aDate = [self.event endTimeForDate:aTimelessDate];
            }else{
                aDate = self.event.endDate;
            }
            break;
    }
    return aDate;
}

@end
