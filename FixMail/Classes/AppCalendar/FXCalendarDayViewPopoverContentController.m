//
//  FXCalendarDayViewPopoverContentController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-14.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarDayViewPopoverContentController.h"
#import "FXMeetingViewController.h"
#import "CalendarController.h"
#import "EventEditController.h"
#import "ASCalendarFolder.h"

@interface FXCalendarDayViewPopoverContentController ()

@end

@implementation FXCalendarDayViewPopoverContentController

+ (FXCalendarDayViewPopoverContentController*)controller
{
    FXCalendarDayViewPopoverContentController* sController = NULL;
    
    if(!sController) {
        sController = [[FXCalendarDayViewPopoverContentController alloc] initWithNibName:@"CalendarDayView" bundle:[FXLSafeZone getResourceBundle]];
        sController.numDays = 1;
    }
    return sController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView
{
    // Launch event editor/view for a specific event
    //
    [[CalendarController controller] setDate:eventView.startDate];
    Event* anEvent = eventView.event;
    FXMeetingViewController* aMeetingVC = [FXMeetingViewController controllerWithEvent:anEvent];
    [self.navigationController pushViewController:aMeetingVC animated:YES];
    
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline createEventForEventView:(TKCalendarDayEventView *)eventView
{
    // Launch event editor/view to create a new event
    //
    [[CalendarController controller] setDate:eventView.startDate];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:self.folder date:eventView.startDate];
    [self.navigationController pushViewController:anEventEditController animated:YES];
}


- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventDateWasSelected:(NSDate*)anEventDate
{
    [[CalendarController controller] setDate:anEventDate];
    
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:anEventDate];
    Event* anEvent      = anEventEditController.event;
    anEvent.startDate   = anEventDate;
    anEvent.endDate     = [[Calendar calendar] addToDate:anEventDate hours:1 minutes:0];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self.navigationController presentViewController:nc animated:YES completion:nil];
}


@end
