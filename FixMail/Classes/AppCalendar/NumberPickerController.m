/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "NumberPickerController.h"

@implementation NumberPickerController

// Properties
@synthesize target;
@synthesize action;
@synthesize min;
@synthesize max;
@synthesize popoverController;

+ (NumberPickerController*)numberPickerForViewController:(UIViewController*)aViewController
                                                    rect:(CGRect)aCellRect
                                                   value:(int)aValue
                                                     min:(int)aMin
                                                     max:(int)aMax
                                                  action:(SEL)anAction
{
    // This doesn't work on iPhone because of UIPopoverController
    //
    NumberPickerController* aNumberPicker = [[NumberPickerController alloc] initWithNibName:@"NumberPickerView" bundle:[FXLSafeZone getResourceBundle]];
    CGRect aRect = aNumberPicker.view.frame;
    aNumberPicker.contentSizeForViewInPopover = aRect.size;
    aNumberPicker.target = aViewController;
    aNumberPicker.action = anAction;
    aNumberPicker.min    = aMin;
    aNumberPicker.max    = aMax;
    [aNumberPicker setValue:aValue];
    
    if(IS_IPAD()) {
        UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:aNumberPicker];
        aRect.origin.y      = aCellRect.origin.y + aCellRect.size.height * 0.5f;
        aRect.origin.x      = 100;
        aRect.size.width    = 10;
        aRect.size.height   = 10;
        [aPopover presentPopoverFromRect:aRect inView:aViewController.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:TRUE];
        aNumberPicker.popoverController = aPopover;
    }else{
        [aViewController.navigationController pushViewController:aNumberPicker animated:YES];
    }
    return aNumberPicker;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    action = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setValue:(int)aValue
{
    [picker selectRow:aValue - self.min inComponent:0 animated:TRUE];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIPickerViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.max - self.min;
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIPickerViewDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 24.0f;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%d", row+1];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(target && action) {
        SuppressPerformSelectorLeakWarning([target performSelector:action withObject:[NSNumber numberWithInt:row + self.min]]);
    }else{
        FXDebugLog(kFXLogActiveSync, @"NumberPickerController action invalid");
    }
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
//{
//    return nil;
//}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return pickerView.frame.size.width;
}

@end
