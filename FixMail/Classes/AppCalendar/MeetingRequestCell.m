/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "MeetingRequestCell.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "ASEmail.h"
#import "ASEmailFolder.h"
#import "CalendarEmail.h"
#import "MeetingRequest.h"

@implementation MeetingRequestCell

@synthesize email;
@synthesize canceledLabel;
@synthesize segmentedControl;


////////////////////////////////////////////////////////////////////////////////////////////
// Construct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct

- (id)initWithStyle:(UITableViewCellStyle)aStyle reuseIdentifier:(NSString *)aReuseIdentifier
{
	self = [super initWithStyle:aStyle reuseIdentifier:aReuseIdentifier];
    if (self) {
    }
    
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)loadEmail:(ASEmail *)anEmail
{
    self.email = anEmail;
    if(anEmail) {
        titleLabel.text  = anEmail.subject;
        fromLabel.text   = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"FROM:", @"Meeting", @"Title for label specifying email sender"), anEmail.from];
        NSString* aWhen  = [[Event dateFormatter] stringFromDate:anEmail.meetingRequest.startDate];
        whenLabel.text   = [NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"WHEN:", @"Meeting", @"Title for label specifying meeting's start date"), aWhen];
    }else{
        titleLabel.text  = nil;
        fromLabel.text   = nil;
        whenLabel.text   = nil;
    }
}

- (IBAction)removeCanceled:(id)sender
{
    @try {
        ASAccount* anAccount = (ASAccount*)[self.email.folder account];
        ASCalendarFolder* aCalendarFolder = [CalendarEmail calendarFolderForAccount:anAccount];
        if(aCalendarFolder) {
            NSString* anEventUid = [self.email.meetingRequest eventUid];
            Event* anEvent = [aCalendarFolder eventForEventUid:anEventUid];
            if(anEvent) {
                [aCalendarFolder deleteUid:anEvent.uid updateServer:TRUE];
                [aCalendarFolder commitObjects];
            }else{
                FXDebugLog(kFXLogCalendar, @"removeCanceled failed event not found: %@", self.email);
            }
        }
        
        ASEmailFolder* anEmailFolder = (ASEmailFolder*)self.email.folder;
        if(anEmailFolder) {
            [anEmailFolder deleteUid:self.email.uid updateServer:TRUE];
            [anEmailFolder commitObjects];
        }
    }@catch (NSException* e) {
        FXDebugLog(kFXLogCalendar, @"removeCanceled exception: %@ %@", e.name, e.reason);
    }
}

@end