/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
*/
#import "TKCalendarMonthViewController.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "NSDate+TKCategory.h"
#import "TKCalendarMonthView.h"


@interface TKCalendarMonthViewController () {

}
@end

@implementation TKCalendarMonthViewController

// Proprties
@synthesize monthView;
@synthesize viewType;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

static CGSize sSize;

+ (void)setSize:(CGSize)aSize
{
    sSize = aSize;
}

+ (CGSize)size
{
    return sSize;
}

-(void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;
	if (monthView) {
		[monthView setFirstDayOfTheWeek:_firstDayOfTheWeek];
	}
	
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

-(void) viewDidLoad
{
	[super viewDidLoad];
    
    tileImage = [self loadImage:@"Month Calendar Date Tile.png"];
	if (!self.firstDayOfTheWeek) {
		self.firstDayOfTheWeek = 1;
	}
	
    NSDate* aDate = [[Calendar calendar] firstOfMonthForDate:[NSDate date]];
    self.monthView = [self createWithSize:sSize origin:CGPointMake(0.0f, 0.0f) date:aDate];
	
}

- (void) viewDidUnload
{
    [super viewDidUnload];

	self.monthView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (TKCalendarMonthView*)createWithSize:(CGSize)aSize origin:(CGPoint)aPoint date:(NSDate*)aDate
{
    EMonthViewType aMonthViewType;
    if (IS_IPAD()) {
        aMonthViewType = kMonthViewEvents;
    }else{
        aMonthViewType = kMonthViewDot;
    }
    TKCalendarMonthView* aMonthView = [[TKCalendarMonthView alloc] initWithSize:aSize
                                                                         origin:aPoint
                                                                           date:aDate
                                                                       viewType:self.viewType
                                                                  monthViewType:aMonthViewType
                                                                      tileImage:tileImage
															  firstDayOfTheWeek:self.firstDayOfTheWeek];
	aMonthView.delegate     = self;
	aMonthView.dataSource   = self;
	[self.view addSubview:aMonthView];
	[aMonthView rebuildWithReloadEvents:TRUE];
    
    return aMonthView;
}

- (UIImage*)loadImage:(NSString*)aFileName
{
    NSString* aResourcePath = [[FXLSafeZone getResourceBundle] resourcePath];
    return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", aResourcePath, aFileName]];
}


@end
