//
//  ODCalendarDayTimelineView.h
//  Created by Devin Ross on 7/28/09.
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
*/
#import "TKCalendarDayEventView.h"

@protocol TKCalendarDayTimelineViewDelegate;

@class Calendar;
@class OverlayView;
@class TKTimelineView;
@class TKTimelineAllDayView;

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineView 
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineView

@interface TKCalendarDayTimelineView : UIView <TapDetectingViewDelegate, UIScrollViewDelegate>
{
    id <TKCalendarDayTimelineViewDelegate> _delegate;

	UIScrollView*       _scrollView;
    CGFloat             scrollX;

	TKTimelineView*     _timelineView;
	OverlayView*        _overlayView;
    	
	NSMutableArray*     events;

    NSLock*             lock;
    
    NSTimer*            timer;

    NSDateFormatter*    dayOfWeekFormatter;
    NSArray*            daysOfWeekShort;
    
    NSDate*             currentDay;
    Calendar*           calendar;
    
    CGFloat             dayWidth;
    NSUInteger          numDays;
    
    NSMutableArray*     dateLabels;
    
	UIColor*            timelineColor;
	UIColor*            hourColor;
    BOOL                isPop;
    BOOL                hasTitleBar;
    
    // Selection
    TKCalendarDayEventView* tempEventView;
    
    // Title bar
    CGFloat             titleBarHeight;
	UIButton*           leftArrow;
    UIButton*           rightArrow;
	UIImageView*        topBackground;
    UIImageView*        shadow;

    // All day event view
    NSMutableArray*         allDayEvents;
    TKTimelineAllDayView*   allDayEventView;
    UIScrollView*           allDayScrollView;
}

// Property
@property (nonatomic, strong) id <TKCalendarDayTimelineViewDelegate> delegate;

@property (strong, nonatomic, readonly) UIScrollView*    scrollView;
@property (strong, nonatomic, readonly) TKTimelineView*  timelineView;
@property (strong, nonatomic, readonly) UIView*          overlayView;
@property (strong) NSMutableArray*  dateLabels;

@property (nonatomic, strong) NSMutableArray*           events;

@property (nonatomic) NSLock*                           lock;

@property (strong) NSTimer*                             timer;
@property (strong) NSDate*                              currentDay;
@property (nonatomic) NSUInteger                        numDays;
@property (nonatomic, strong) Calendar*                 calendar;

@property (nonatomic, strong) UIColor*                  timelineColor;
@property (nonatomic, strong) UIColor*                  hourColor;
@property (nonatomic) BOOL                              isPop;
@property (nonatomic) BOOL                              hasTitleBar;

@property (nonatomic, strong) NSMutableArray*           allDayEvents;
@property (nonatomic, strong) TKTimelineAllDayView*     allDayEventView;
@property (nonatomic, strong) UIScrollView*             allDayScrollView;

@property (nonatomic) int firstDayOfTheWeek;


// Initialisation
+ (BOOL)needsTitleBar;

- (id)initWithFrame:(CGRect)frame numDays:(int)aNumDays;
- (void)setupCustomInitialisation;
- (void)configureForOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                          frame:(CGRect)aFrame
           navigationController:(UINavigationController*)aNavigationController;

// Setters
- (void)setDate:(NSDate*)aDate;

// Reload Day
- (void)reloadDays:(BOOL)aSetPosition;

// Actions
- (void)next:(id)sender;
- (void)prev:(id)sender;

@end



