 //
//  ODCalendarDayTimelineView.h
//  Created by Devin Ross on 7/28/09.
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "TKTimelineView.h"
#import "Calendar.h"
#import "NSDate+TKCategory.h"
#import "TKGlobal.h"
#import "TKTimelineGlobal.h"
#import "UIImage+TKCategory.h"
#import "DateUtil.h"

@implementation TKTimelineView

// Properties
@synthesize times = _times;
@synthesize hourColor;
@synthesize numDays;
@synthesize dayWidth;
@synthesize isToday;
@synthesize is24HourClock = _is24HourClock;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when xibless (interface buildder)
- (id)initWithFrame:(CGRect)frame
{
    if(!(self=[super initWithFrame:frame])) return nil;
    [self setupCustomInitialisation];
    return self;
}

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when using xib (interface buildder)
- (id)initWithCoder:(NSCoder *)decoder
{
    if(!(self=[super initWithCoder:decoder])) return nil;
    [self setupCustomInitialisation];
	return self;
}

- (void)setupCustomInitialisation
{
	self.is24HourClock = [[DateUtil getSingleton] is24HourClock];
    isToday = FALSE;
}

////////////////////////////////////////////////////////////////////////////////
// Setup
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setup

// Setup array consisting of string
// representing time aka 12 (12 am/Users/emillard/Desktop/FixMail/fixmail/Classes/TKCalendarMonthViewController.m), 1 (1 am) ... 25 x

- (NSArray *)times
{
	DateUtil *dateUtil = [DateUtil getSingleton];
	BOOL isCurrently24HourClock = [dateUtil is24HourClock];
	if (!_times || (self.is24HourClock != isCurrently24HourClock)) {		
		NSMutableArray *timesArray = [[NSMutableArray alloc] initWithCapacity:25];
		
		NSDateComponents *timeComponents = [[NSDateComponents alloc] init];
		[timeComponents setMinute:0];
		
		if (isCurrently24HourClock) {
			
			for (int i = 0; i < 25; i++) {
				[timeComponents setHour:i];
				NSDate *time = [[NSCalendar currentCalendar] dateFromComponents:timeComponents];
				[(NSMutableArray *)timesArray addObject:[dateUtil stringFromDate24HourSystemHourAndMinuteOnly:time]];
			}
			
		} else {
			
			for (int i = 0; i < 25; i++) {
				if (i == 12) {
					[(NSMutableArray *)timesArray addObject:FXLLocalizedString(@"NOON", @"Word that should be displayed in a Timeline View for 12 o'clock noon")];
				} else {
					[timeComponents setHour:i];
					NSDate *time = [[NSCalendar currentCalendar] dateFromComponents:timeComponents];
					[(NSMutableArray *)timesArray addObject:[dateUtil stringFromDate12HourSystemHourAndAMOrPMSymbolOnly:time]];
				}
			}
		}
		
		_times = timesArray;
		self.is24HourClock = isCurrently24HourClock;
	}
	return _times;
}

////////////////////////////////////////////////////////////////////////////////
// Drawing
////////////////////////////////////////////////////////////////////////////////
#pragma mark Drawing

- (CGFloat)positionForDate:(NSDate*)aDate calendar:(Calendar*)aCalendar
{
    TKDateInformation aDateInfo = [aCalendar dateInformationFromDate:aDate];
    NSInteger hourStart = aDateInfo.hour;
    CGFloat hourStartPosition = roundf((hourStart * kTimelineVerticalDiff) + kTimelineVerticalOffset
                                       + ((kTimelineFontSize + kTimelineFontSpacer) / 2.0));
    
    // Get the minute start position, round minutes to 5
    //
    NSInteger minuteStart = aDateInfo.minute;
    minuteStart = round(minuteStart / 5.0) * 5;
    CGFloat minuteStartPosition = roundf((CGFloat)minuteStart / 60.0f * kTimelineVerticalDiff);
    return hourStartPosition + minuteStartPosition;
}

- (void)drawRect:(CGRect)rect
{
	// Draw timeline from 12 am to noon to 12 am next day
    //
	
	// Time appearance
	UIFont* timeFont        = [UIFont boldSystemFontOfSize:kTimelineFontSize];
	UIColor* timeColor      = hourColor ? hourColor : [UIColor blackColor];

    UIColor* aLightGrayColor = [UIColor colorWithRed:0.95f green:0.95f blue:0.95f alpha:1.0f];
    struct CGColor* aLightGrayCGColor = [aLightGrayColor CGColor];
    
    UIColor* aDarkGrayColor = [UIColor colorWithRed:0.85f green:0.85f blue:0.85f alpha:1.0f];
    struct CGColor* aDarkGrayCGColor = [aDarkGrayColor CGColor];
    
    struct CGColor* aDarkerGrayCGColor = [[UIColor lightGrayColor] CGColor];
        
    CGSize aBoundsSize = self.bounds.size;

    CGContextRef context = UIGraphicsGetCurrentContext();
    
	// Draw each time string
    //
    CGFloat aVerticalOffset = 0.0f;
    CGFloat aLineVerticalOffset = roundf(kTimelineVerticalDiff / 2.0f);
    CGFloat aFontCenterY = roundf((kTimelineFontSize + kTimelineFontSpacer) / 2.0f);
	for(NSInteger i = 0; i < self.times.count; i++) {
		// Position to the hour
        //
		NSString* time = [self.times objectAtIndex:i];
        aVerticalOffset = kTimelineVerticalOffset + i * kTimelineVerticalDiff + kTimelineVerticalMargin;
        if(i == 0) {
            // Draw gray rect to fill the out of bounds top part of the timeline
            //
            CGContextRef context = UIGraphicsGetCurrentContext();
            CGContextSetFillColorWithColor(context, aLightGrayCGColor);
            
            CGRect aFillRect = CGRectMake(kTimelineEventHorizontalOffset, 0.0f, aBoundsSize.width, aVerticalOffset + aFontCenterY);
            CGContextFillRect(context, aFillRect);
        }
        
        // Draw the hour numeric
        //
        [timeColor set];
		CGRect timeRect = CGRectMake(kTimelineHorizontalOffset, aVerticalOffset,
                                     kTimelineTimeWidth + kTimelinePeriodWidth,
                                     kTimelineFontSize + kTimelineFontSpacer);
        
		[time drawInRect:CGRectIntegral(timeRect)
                withFont:timeFont
           lineBreakMode:NSLineBreakByWordWrapping 
               alignment:NSTextAlignmentRight];
		
		// Draw horizontal line on the hour
        //
		CGContextSaveGState(context);
		CGContextSetStrokeColorWithColor(context, aDarkerGrayCGColor);
		CGContextSetLineWidth(context, 0.5);
		CGContextTranslateCTM(context, -0.5, -0.5); // Translate context for clear line
		
		CGContextBeginPath(context);
        aVerticalOffset += aFontCenterY;
		CGContextMoveToPoint(context, kTimelineEventHorizontalOffset, aVerticalOffset);
		CGContextAddLineToPoint(context, aBoundsSize.width, aVerticalOffset);
		CGContextStrokePath(context);
		
        // Draw horizontal line on the half hour
        //
		if (i != self.times.count-1) {
            CGContextSetStrokeColorWithColor(context, aDarkGrayCGColor);
			CGContextBeginPath(context);
			CGContextMoveToPoint(context, kTimelineEventHorizontalOffset, aVerticalOffset + aLineVerticalOffset);
			CGContextAddLineToPoint(context, aBoundsSize.width, aVerticalOffset + aLineVerticalOffset);
			//CGFloat dash1[] = {2.0f, 1.0f};
			//CGContextSetLineDash(context, 0.0f, dash1, 2);
			CGContextStrokePath(context);
		}
        CGContextRestoreGState(context);
	}
    
    // Vertical lines if week view
    //
    if(numDays > 1) {
        CGFloat x = kTimelineEventHorizontalOffset;
        CGFloat y = aBoundsSize.height;
        for(int i = 0 ; i < numDays ; ++i) {
            CGContextBeginPath(context);
            CGContextMoveToPoint(context,    x, 0.0f);
            CGContextAddLineToPoint(context, x, y);
            CGContextStrokePath(context);
            x += dayWidth;
        }
    }
    
    // Draw gray rect to fill the out of bounds bottom part of the timeline
    //
    CGRect aFillRect = CGRectMake(kTimelineEventHorizontalOffset, aVerticalOffset, aBoundsSize.width, kTimelineVerticalMargin);
    CGContextSetFillColorWithColor(context, aLightGrayCGColor);
    CGContextFillRect(context, aFillRect);
}

@end