//
//  ODCalendarDayTimelineView.m
//  Created by Devin Ross on 7/28/09.
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "TKCalendarDayTimelineView.h"
#import "AppSettings.h"
#import "Calendar.h"
#import "NSDate+TKCategory.h"
#import "TKCalendarDayOverlayView.h"
#import "TKGlobal.h"
#import "TKTimelineGlobal.h"
#import "TKCalendarDayTimelineViewDelegate.h"
#import "TKTimelineView.h"
#import "TKTimelineAllDayView.h"
#import "UIImage+TKCategory.h"
#import "DateUtil.h"

// Types
typedef struct
{
    CGFloat min;
    CGFloat max;
} ESpan;

// Constants
static const CGFloat kTopBarHeight          = 45.0;
static const CGFloat kTimelineHeight        = 24 * kTimelineVerticalOffset+23 * kTimelineVerticalDiff;

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineView 
////////////////////////////////////////////////////////////////////////////////

#pragma mark TKCalendarDayTimelineView
@interface TKCalendarDayTimelineView (private)

@property (readonly) UIImageView*   topBackground;
@property (readonly) UIButton*      leftArrow;
@property (readonly) UIButton*      rightArrow;
@property (readonly) UIImageView*   shadow;

@end


@implementation TKCalendarDayTimelineView

@synthesize delegate=_delegate;
@synthesize events;
@synthesize allDayEvents;
@synthesize lock;
@synthesize timer;
@synthesize calendar;
@synthesize currentDay;
@synthesize numDays;
@synthesize dateLabels;
@synthesize timelineColor;
@synthesize hourColor;
@synthesize isPop;
@synthesize hasTitleBar;
@synthesize allDayEventView;
@synthesize allDayScrollView;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

// Constants
static const CGFloat kViewWidth         = 320.0f;
static const CGFloat kArrowWidth        = 48.0f;
static const CGFloat kTitleBarHeight    = 38.0f;
static const CGFloat kMinEventHeight    = 24.0f;
static const CGFloat kHeaderFontSize    = 19.0f;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

+ (BOOL)needsTitleBar
{
    BOOL aNeedsTitleBar;
    
    UIDevice* aDevice = [UIDevice currentDevice];
    if(aDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad
       || aDevice.orientation == UIDeviceOrientationPortrait
       || aDevice.orientation == UIDeviceOrientationPortraitUpsideDown) {
        aNeedsTitleBar = TRUE;
    }else{
        aNeedsTitleBar = FALSE;
    }
    
    return aNeedsTitleBar;
}
// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when xibless (interface buildder)
- (id)initWithFrame:(CGRect)frame numDays:(int)aNumDays
{
    if(!(self=[super initWithFrame:frame])) return nil;
    
    self.numDays        = aNumDays;
    self.hasTitleBar    = [TKCalendarDayTimelineView needsTitleBar];

    return self;
}

- (void)dealloc 
{
    [lock lock];
    if(self.timer) {
        [self.timer invalidate];
    }
    [lock unlock];

}

- (void)configureForOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                          frame:(CGRect)aFrame
           navigationController:(UINavigationController*)aNavigationController
{
    // Always set frame size for scroll view and main view, these are highly variable between different
    // size devices and orientations
    //
    CGRect aFrameRect       = self.scrollView.frame;
    aFrameRect.size         = aFrame.size;
    self.scrollView.frame   = aFrameRect;
    
    aFrameRect              = self.frame;
    aFrameRect.size         = aFrame.size;
    self.frame              = aFrameRect;
    
    if(!IS_IPAD()) {
        // iPhone
        //
        CGRect aRect = self.frame;

        switch(toInterfaceOrientation) {
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
            {
                self.numDays        = 7;
                if(!aNavigationController.navigationBarHidden) {
                    CGSize aNavBarSize = aNavigationController.navigationBar.frame.size;
                    aRect.size.height += aNavBarSize.height;
                    aNavigationController.navigationBarHidden = TRUE;
                }
                self.scrollView.directionalLockEnabled = TRUE;
                break;
            }
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
            {
                self.numDays        = 1;
                if(aNavigationController.navigationBarHidden) {
                    aNavigationController.navigationBarHidden = FALSE;
                    CGSize aNavBarSize = aNavigationController.navigationBar.frame.size;
                    aRect.size.height -= aNavBarSize.height;
                }
                self.scrollView.directionalLockEnabled = FALSE;
                break;
            }
        }
        
        CGSize aSize = self.scrollView.contentSize;
        
        switch(toInterfaceOrientation) {
            case UIInterfaceOrientationLandscapeLeft:
            case UIInterfaceOrientationLandscapeRight:
            {
                aSize.width         = 1024;
                break;
            }
            case UIInterfaceOrientationPortrait:
            case UIInterfaceOrientationPortraitUpsideDown:
            {
                aSize.width         = 320;
                break;
            }
        }

        self.hasTitleBar = [TKCalendarDayTimelineView needsTitleBar];
        [self buildTitleBar];
        if(self.hasTitleBar) {
            aRect.origin.y    += titleBarHeight;
            aRect.size.height -= titleBarHeight;
        }
        self.scrollView.frame          = aRect;
        aRect.size = aSize;
        self.scrollView.contentSize    = aSize;
        
        aRect.origin.y      = -kTimelineVerticalMargin;
        aRect.size.height   += kTimelineVerticalMargin * 2.0f;
        self.timelineView.frame        = aRect;
    }else{
        // iPad
        //
        CGSize aSize = self.scrollView.contentSize;
        CGRect aRect = CGRectMake(0.0f, 0.0f, aSize.width, aSize.height);
        aRect.origin.y      = -kTimelineVerticalMargin;
        aRect.size.height   += kTimelineVerticalMargin * 2.0f;
        aRect.size.width    = self.scrollView.frame.size.width;
        self.timelineView.frame = aRect;
        
        aSize.width = self.scrollView.frame.size.width;
        self.scrollView.contentSize = aSize;
    }
}

- (void)buildTitleBar
{
    if(hasTitleBar) {
        for (id view in self.topBackground.subviews) {
            [view removeFromSuperview];
        }
        titleBarHeight = kTitleBarHeight;
        CGRect aRect = CGRectMake(0, 0, self.bounds.size.width, titleBarHeight);
        self.topBackground.frame = aRect;
        [self addSubview:self.topBackground];
        for(int i = 0 ; i < numDays ; ++i) {
            UILabel* aLabel = [self.dateLabels objectAtIndex:i];
            CGRect aRect = aLabel.frame;
            aRect.size.height = titleBarHeight;
            aLabel.frame = aRect;
            [self.topBackground addSubview:aLabel];
        }
        if(!IS_IPAD()) {
            [self addSubview:self.leftArrow];
            [self addSubview:self.rightArrow];
        }
    }else{
        titleBarHeight = 0;
        [self.topBackground removeFromSuperview];
        [self.leftArrow removeFromSuperview];
        [self.rightArrow removeFromSuperview];
    }
}

- (void)buildAllDayView
{
    CGRect anAllDayRect         = self.frame;
    anAllDayRect.origin.y       = titleBarHeight;
    anAllDayRect.size.height    = 0.0f;
    
    TKTimelineAllDayView* anAllDayEventView;
    if(!self.allDayScrollView) {
        UIScrollView* aScrollView = [[UIScrollView alloc] initWithFrame:anAllDayRect];
        CGSize aContentSize         = CGSizeMake(self.bounds.size.width, kMinEventHeight);
        aScrollView.contentSize     = aContentSize;
        aScrollView.scrollEnabled   = TRUE;
        aScrollView.backgroundColor = [UIColor whiteColor];

        anAllDayEventView = [[TKTimelineAllDayView alloc] initWithFrame:anAllDayRect];
        anAllDayEventView.frame             = CGRectMake(0.0f, 0.0f, aContentSize.width, aContentSize.height);
        anAllDayEventView.backgroundColor   = [UIColor whiteColor];

        [aScrollView addSubview:anAllDayEventView];
        
        self.allDayScrollView   = aScrollView;
        self.allDayEventView    = anAllDayEventView;
    }else{
        anAllDayEventView = self.allDayEventView;
        for (id view in self.allDayEventView.subviews) {
            [view removeFromSuperview];
        }
    }
    
    anAllDayEventView.offsetX     = kTimelineEventHorizontalOffset;
    anAllDayEventView.dayWidth    = dayWidth;
    anAllDayEventView.numDays     = numDays;
    
    if(hasTitleBar) {
        if(!IS_IPAD()) {
            [self addSubview:self.leftArrow];
            [self addSubview:self.rightArrow];
        }
    }else{
        for(int i = 0 ; i < numDays ; ++i) {
            UILabel* aLabel = [self.dateLabels objectAtIndex:i];
            CGRect aRect = aLabel.frame;
            aRect.size.height = kMinEventHeight;
            aLabel.frame = aRect;
            [anAllDayEventView addSubview:aLabel];
        }
        [self.leftArrow removeFromSuperview];
        [self.rightArrow removeFromSuperview];
    }
}

- (void)setupCustomInitialisation
{
    if(!lock) {
        lock = [[NSLock alloc] init];
    }
    
    // Calendar
    //
    self.calendar = [Calendar calendar];
    
    dayWidth = (self.scrollView.contentSize.width - kTimelineEventHorizontalOffset) / numDays;
	
    [self buildLabels];

    // Title setup
    //
    [self buildTitleBar];

    // All day event view
    //
    [self buildAllDayView];

	// Add main scroll view
	[self addSubview:self.scrollView];
    
	// Add timeline view inside scrollview
	[self.scrollView addSubview:self.timelineView];
    self.timelineView.dayWidth = dayWidth;
    self.timelineView.numDays = numDays;

    daysOfWeekShort = [[DateUtil getSingleton] arrayOfStringsShortWeekdaySymbols];
    
    [self reloadDays:FALSE];
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: TKCalendarDayTimelineView %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////
// Setters 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setDate:(NSDate*)aDate
{
    if(![aDate isEqualToDate:self.currentDay]) {
        [self setCurrentDay:aDate];
        [self reloadDays:TRUE/*setPosition*/];
    }
}

-(void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;

}

////////////////////////////////////////////////////////////////////////////////
// Execute Method When Notification Fires 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Execute Method When Notification Fires

//help executing a method when a notification fire
- (void)observeValueForKeyPath:(NSString *)keyPath 
                      ofObject:(id)object 
                        change:(NSDictionary *)change 
                       context:(void *)context
{
    SuppressPerformSelectorLeakWarning([self performSelector:(SEL)context withObject:change]);
} 

////////////////////////////////////////////////////////////////////////////////
// Setup 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setup

- (UIScrollView*)scrollView
{
	if (!_scrollView) {
        CGRect aRect = CGRectMake(0.0f, kTopBarHeight, self.bounds.size.width, self.bounds.size.height-kTopBarHeight);
		_scrollView = [[UIScrollView alloc] initWithFrame:aRect];
		_scrollView.contentSize     = CGSizeMake(self.bounds.size.width, kTimelineHeight);
		_scrollView.scrollEnabled   = TRUE;
		_scrollView.backgroundColor = [UIColor whiteColor];
        [_scrollView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
		_scrollView.alwaysBounceVertical = TRUE;
        [_scrollView setDelegate:self];
	}
	return _scrollView;
}

- (TKTimelineView*)timelineView
{
	if (!_timelineView) {
		_timelineView = [[TKTimelineView alloc]initWithFrame:CGRectMake(self.bounds.origin.x, 
																		self.bounds.origin.y,
																		self.bounds.size.width,
																		kTimelineHeight)];
		_timelineView.backgroundColor = [UIColor whiteColor];
		_timelineView.delegate = self;		
		
	}
	return _timelineView;
}

- (OverlayView*)overlayView
{
	if (!_overlayView) {
		_overlayView = [[OverlayView alloc]initWithFrame:CGRectMake(self.bounds.origin.x,
																		self.bounds.origin.y,
																		kTimelineEventHorizontalOffset,
																		kTimelineHeight)];
        _overlayView.opaque = FALSE;
        _overlayView.delegate = self;
        _overlayView.timelineView = self.timelineView;
	}
	return _overlayView;
}

- (void)setTimelineColor:(UIColor*) aColor 
{
	_timelineView.backgroundColor = aColor;
}

////////////////////////////////////////////////////////////////////////////////
// View Event
////////////////////////////////////////////////////////////////////////////////
#pragma mark View Event

- (void)didMoveToWindow
{
}

////////////////////////////////////////////////////////////////////////////////
// Reload Day
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reload Day

- (void)_minuteTimer:(NSTimer*)aTimer
{
    [_overlayView setNeedsDisplay];
}

static NSInteger sortByStartDate(id obj1, id obj2, void *context)
{
	BOOL reverse = *((BOOL *)context);
	NSDate* aDate1 = [(TKCalendarDayEventView*)obj1 startDate];
	NSDate* aDate2 = [(TKCalendarDayEventView*)obj2 startDate];
    if ((NSInteger *)reverse == NO) {
        return [aDate1 compare:aDate2];
    }
    return [aDate2 compare:aDate1];
}

- (CGFloat)dayOffsetX:(int)i
{
    return i * dayWidth + kTimelineEventHorizontalOffset;
}

- (CGFloat)loadAllDayEventView:(TKCalendarDayEventView*)anEventView dayOfWeek:(int)i allDayEventY:(CGFloat)anAllDayEventY
{
    [self.allDayEvents addObject:anEventView];
    
    CGFloat anEventHeight = kMinEventHeight;
    
    CGRect aRect;
    aRect.origin.x    = [self dayOffsetX:i] + kTimelineHorizontalLineDiff * 0.5;
    aRect.origin.y    = anAllDayEventY; anAllDayEventY += anEventHeight;
    aRect.size.width  = dayWidth - kTimelineHorizontalLineDiff;
    aRect.size.height = anEventHeight;
    anEventView.frame = aRect;
    anEventView.delegate = self;
    [anEventView setNeedsDisplay];
    [allDayEventView addSubview:anEventView];
    
    return anAllDayEventY;
}

- (void)setDateString:(NSDate*)aDate inLabel:(UILabel*)aLabel
{
	DateUtil *dateUtil = [DateUtil getSingleton];
	NSString *aDisplayDate;
	NSString* aDayOfWeekString;
	
    if(numDays > 1) {
        int aWeekDay = [self.calendar weekday:aDate] - 1;
        aDayOfWeekString = [daysOfWeekShort objectAtIndex:aWeekDay];
		
		aDisplayDate = [dateUtil stringFromDateMonthAndDayOnly:aDate];
		aDisplayDate = [NSString stringWithFormat:@"%@ %@", aDayOfWeekString, aDisplayDate];
		
    }else{
		aDisplayDate = [dateUtil stringFromDateWeekdayAndDateLongStyle:aDate];
	}
	aLabel.text = aDisplayDate;
}

- (CGFloat)loadDate:(NSDate*)aDate dayOfWeek:(int)aDayOfWeek isToday:(BOOL)anIsToday span:(ESpan*)aSpan
{
    // String for date in column header
    //
    UILabel* aLabel = [self.dateLabels objectAtIndex:aDayOfWeek];
    [self setDateString:aDate inLabel:aLabel];
        
    // Get sorted events for this day
    //
    NSArray* anUnsortedEventViews = [self.delegate calendarDayTimelineView:self eventsForDate:aDate];
    
    BOOL aReverseSort = FALSE;
    NSArray* anEventViews = [anUnsortedEventViews sortedArrayUsingFunction:sortByStartDate context:&aReverseSort];
    [self.events addObjectsFromArray:anEventViews];
    
    NSMutableArray* sameTimeEvents = [[NSMutableArray alloc] init];
    
    CGFloat dayOffset = [self dayOffsetX:aDayOfWeek];
    
    NSInteger repeatNumber  = 0;        // number of nested appointments
    
    CGFloat anAllDayEventY  = 0.0f;
    
    CGFloat aStartMarker    = -10.0f;     // starting point to check if they match
    CGFloat anEndMarker     = -10.0f;

    for(TKCalendarDayEventView* anEventView in anEventViews) {
        Event* anEvent = anEventView.event;
        
        // Making sure delgate is sending events that matches current day
        //
        if ([anEventView.startDate isSameDay:aDate]) {
            
            // All day events are placed in the header
            //
            if([anEvent isAllDay]) {
                anAllDayEventY = [self loadAllDayEventView:anEventView dayOfWeek:aDayOfWeek allDayEventY:anAllDayEventY];
                continue;
            }
            
            // Vertical position event
            //
            CGFloat aStartPosition = [_timelineView positionForDate:anEventView.startDate calendar:self.calendar];
            CGFloat anEndPosition;
            if ([anEventView.startDate isSameDay:anEventView.endDate]) {
                anEndPosition  = [_timelineView positionForDate:anEventView.endDate calendar:self.calendar];
            }else{
                anEndPosition = kTimelineHeight;
            }
            
            // Event height
            CGFloat eventHeight = anEndPosition - aStartPosition;
            if(eventHeight < kTimelineVerticalDiff/2) eventHeight = kTimelineVerticalDiff/2;
            
            // Do events overlap
            //
            if (aStartPosition - aStartMarker <= 2.0f) {
                repeatNumber++;
            }else if (aStartPosition < anEndMarker) {
                repeatNumber++;
            }else{
                repeatNumber = 0;
                [sameTimeEvents removeAllObjects];
            }
            
            // Refresh the markers
            //
            aStartMarker    = aStartPosition;
            anEndMarker     = anEndPosition;
            
            // Set event view frame
            //
            CGFloat eventWidth      = (dayWidth - kTimelineHorizontalLineDiff) / (repeatNumber+1);
            CGFloat eventOriginX    = dayOffset + kTimelineHorizontalLineDiff * 0.5;
            CGRect eventFrame       = CGRectMake(eventOriginX + (repeatNumber*eventWidth),
                                           aStartPosition,
                                           eventWidth,
                                           eventHeight);
            anEventView.frame = CGRectIntegral(eventFrame);
            
            // Update vertical range of timeline where there are events
            //
            if(eventFrame.origin.y < aSpan->min) {
                aSpan->min = eventFrame.origin.y;
            }
            CGFloat aBottom = eventFrame.origin.y + eventFrame.size.height;
            if(aBottom > aSpan->max) {
                aSpan->max = aBottom;
            }
            
            // Add event view to scrollView
            //
            anEventView.delegate = self;
            [anEventView setNeedsDisplay];
            [self.scrollView addSubview:anEventView];
            
            // Same time event calculations
            //
            for(int i = [sameTimeEvents count]-1; i >= 0; i--) {
                TKCalendarDayEventView* sameTimeEvent = [sameTimeEvents objectAtIndex:i];
                CGRect newFrame     = sameTimeEvent.frame;
                newFrame.size.width = eventWidth;
                newFrame.origin.x   = eventOriginX + (i*eventWidth);
                sameTimeEvent.frame = CGRectIntegral(newFrame);
            }
            [sameTimeEvents addObject:anEventView];
        }else{
            FXDebugLog(kFXLogActiveSync, @"loadDate invalid event: %@", anEventView.startDate);
        }
    }
    
    return anAllDayEventY;
}

- (void)reloadDays:(BOOL)aSetPosition
{
    NSDate* aCurrentDay = NULL;
    
    [lock lock];
    @try {
        // Remove all previous event views
        //
        for (id view in self.scrollView.subviews) {
            if(![NSStringFromClass([view class])isEqualToString:@"TKTimelineView"]) {
                [view removeFromSuperview];
            }
        }
        for(TKCalendarDayEventView* anAllDayEventView in self.allDayEvents) {
            [anAllDayEventView removeFromSuperview];
        }
        
        // Get first date to be displayed on this timeline
        //
        aCurrentDay = [self currentDay];
        if(aCurrentDay == NULL) {
            aCurrentDay = [NSDate date];
        }
        
        // If week view we need to set date to first day of week
        //
        if(numDays > 1) {
#warning FIXME should use the ActiveSync first day of week here
			int aWeekDay = [self.calendar weekday:aCurrentDay] - self.firstDayOfTheWeek;
			
            if(aWeekDay > 0) {
                aCurrentDay = [self.calendar addDays:-aWeekDay toDate:aCurrentDay];
            }
        }
        
        // Hold first day on the timeline
        //
        self.currentDay = aCurrentDay;
        
        // Today's date
        //
        NSDate* aCurrentDate    = [NSDate date];
        NSDate* aTimelessToday  = [self.calendar timelessDate:aCurrentDate];
        BOOL anIsToday          = FALSE;
        
        // Initialize event view vertical range
        //
        CGSize aScrollViewSize  = self.scrollView.contentSize;
        ESpan aSpan;
        aSpan.min   = aScrollViewSize.height;
        aSpan.max   = 0.0f;
        
        // Allocate event view storage
        //
        self.events             = [NSMutableArray arrayWithCapacity:5];
        self.allDayEvents       = [NSMutableArray array];

        // Add event views for each date to the timeline, usually 1 or 7 days
        //
        CGFloat anAllDayEventY = 0.0f;
        NSDate* aDate = [aCurrentDay copy];
        for(int i = 0 ; i < numDays ; ++i) {
            // Is date being loaded today?
            //
            NSDate* aTimelessCurrent = [self.calendar timelessDate:aDate];
            if([aTimelessCurrent compare:aTimelessToday] == NSOrderedSame) {
                anIsToday = TRUE;
            }
            
            CGFloat y = [self loadDate:aDate dayOfWeek:i isToday:anIsToday span:&aSpan];
            if(y > anAllDayEventY) {
                anAllDayEventY = y;
            }
            aDate = [[Calendar calendar] addDays:1 toDate:aDate];
        }
        
        if(!hasTitleBar) {
            anAllDayEventY = kMinEventHeight;
        }
        
        // Adjust title bar height for all day events
        //
        CGRect aRect;
        aRect.origin.x      = 0.0f;
        aRect.origin.y      = titleBarHeight;
        aRect.size.width    = self.frame.size.width;
        aRect.size.height   = anAllDayEventY;
        self.allDayScrollView.frame = aRect;
        
        CGSize aSize = self.allDayScrollView.contentSize;
        aSize.height = aRect.size.height;
        aSize.width  = self.scrollView.contentSize.width;
        self.allDayScrollView.contentSize = aSize;
        
        CGRect anEventRect = CGRectMake(0.0f, 0.0f, aSize.width, aSize.height);
        self.allDayEventView.frame = anEventRect;
        
        if(anAllDayEventY > 0.0f) {
            [self addSubview:self.allDayScrollView];
        }else{
            [self.allDayScrollView removeFromSuperview];
        }

        CGFloat aDelta = aRect.origin.y + aRect.size.height;
        aRect = self.frame;
        aRect.size.height   -= aDelta;
        aRect.origin.y      += aDelta;
        self.scrollView.frame = aRect;

        // Add or remove overlay view for displaying current time
        //
        _timelineView.isToday = anIsToday;
        if(anIsToday) {
            UIView* anOverlayView = self.overlayView;
            if(!anOverlayView) {
                anOverlayView = [self overlayView];
            }
            [self.scrollView addSubview:self.overlayView];
            
            TKDateInformation aDateInfo = [self.calendar dateInformationFromDate:[NSDate date]];
            aDateInfo.minute += 1;
            aDateInfo.second = 0;
            NSDate* aDate = [self.calendar dateFromDateInformation:aDateInfo];

            self.timer = [[NSTimer alloc] initWithFireDate:aDate
                                                  interval:60.0f
                                                    target:self
                                                  selector:@selector(_minuteTimer:)
                                                  userInfo:nil
                                                   repeats:TRUE];
            [[NSRunLoop mainRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
        }else{
            if(self.overlayView) {
                [self.overlayView removeFromSuperview];
            }
            if(self.timer) {
                [self.timer invalidate];
                self.timer = nil;
            }
        }
        
        // Position timeline to some reasonable place
        //
        if(aSetPosition && !isPop) {
            CGPoint aContentOffset = CGPointMake(0.0f, 0.0f);
            if(anIsToday) {
                // Center frame on current time
                //
                CGSize aFrameSize = self.scrollView.frame.size;
                CGFloat aVerticalOffset = [self.timelineView positionForDate:aCurrentDate calendar:[Calendar calendar]];
                aContentOffset.y = aVerticalOffset - aFrameSize.height * 0.5f;
                // Limit check because this will go negative if current time is near edge of scroll view
                if(aContentOffset.y < 0.0) {
                    aContentOffset.y = 0.0;
                }
            }else if(aSpan.max != 0.0f) {
                // Position top of frame to first event minus a small fudge offset
                //
                if(aSpan.min > 10.0f) {
                    aSpan.min -= 10.0f;
                }
                aContentOffset.y = aSpan.min;
            }else{
                // Center frame on noon
                //
                CGSize aFrameSize = self.scrollView.frame.size;
                CGFloat verticalCenter = aScrollViewSize.height * 0.5f;
                aContentOffset.y = verticalCenter - aFrameSize.height * 0.5f;
            }
            
            // Limit check to end of day
            //
            if(aContentOffset.y + self.scrollView.frame.size.height > self.scrollView.contentSize.height) {
                aContentOffset.y = self.scrollView.contentSize.height - self.scrollView.frame.size.height;
            }
            
            [self.scrollView setContentOffset:aContentOffset animated:TRUE];
        }else{
            isPop = FALSE;
        }
        
        // Make sure everything redraws
        //
        [_timelineView setNeedsDisplay];
        [allDayEventView setNeedsDisplay];
        [_overlayView setNeedsDisplay];

    }@catch(NSException* e) {
		[self _logException:@"reloadDays" exception:e];
    }
    [lock unlock];
}

////////////////////////////////////////////////////////////////////////////////
// View Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark View Utilities

- (CGRect)rectForPoint:(CGPoint)aPoint date:(NSDate*)aDate
{
    CGRect aRect;
    
    int aDay = [self dayForPoint:aPoint];
    aRect.origin.x      = kTimelineEventHorizontalOffset + aDay * dayWidth;
    
    CGFloat aStartPosition = [_timelineView positionForDate:aDate calendar:self.calendar];
    aRect.origin.y      = aStartPosition;
    
    aRect.size.width    = dayWidth;
    aRect.size.height   = kTimelineVerticalDiff;
    
    return aRect;
}

- (int)dayForPoint:(CGPoint)aPoint
{
    int aDay;
    
    if(numDays <= 1) {
        aDay = 0;
    }else{
        aDay = (aPoint.x - kTimelineEventHorizontalOffset) / dayWidth;
    }
    
    return aDay;
}

- (CGFloat)hourForPoint:(CGPoint)aPoint
{
    CGFloat anHour;
    
    if(aPoint.y <= kTimelineVerticalOffset) {
        anHour  = 0.0f;
    }else{
        anHour  = (aPoint.y - kTimelineVerticalOffset) / kTimelineVerticalDiff;
    }
    
    return anHour;
}

- (NSDate*)dateForPoint:(CGPoint)aPoint
{
    // Time of day based on tap Y
    //
    CGFloat aTime           = [self hourForPoint:aPoint];
    NSInteger anHour        = (int)aTime;

	CGFloat aMinutePart     = aTime - (CGFloat)anHour;
    NSUInteger aMinutes     = aMinutePart * 60.0f;
    if(aMinutes >= 30) {
        aMinutes = 30;
	}else{
        aMinutes = 0;
    }
	NSDate* aTimePart = [[DateUtil getSingleton] dateFromString24HourSystemHourAndMinuteOnly:[NSString stringWithFormat:@"%i:%i", anHour, aMinutes]];
	
    // Date based on tap X if this is a multiday view
    //
    NSDate* aDatePart = [self currentDay];
    if(numDays > 1) {
        int aDay = [self dayForPoint:aPoint];
        if(aDay > 0) {
            aDatePart = [[Calendar calendar ] addDays:aDay toDate:aDatePart];
        }
    }
    
	return [[DateUtil getSingleton] dateWithDatePart:aDatePart andTimePart:aTimePart];
}

////////////////////////////////////////////////////////////////////////////////
// Tap Detecting View
////////////////////////////////////////////////////////////////////////////////
#pragma mark Tap Detecting View

- (void)tapDetectingView:(TapDetectingView *)aView gotSingleTapAtPoint:(CGPoint)aTapPoint
{
    if([aView isKindOfClass:[TKCalendarDayEventView class]]) {
        if(aView && aView == tempEventView) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(calendarDayTimelineView:createEventForEventView:)]) {
                [self.delegate calendarDayTimelineView:self createEventForEventView:(TKCalendarDayEventView *)aView];
            }else{
                FXDebugLog(kFXLogActiveSync, @"eventViewWasSelected invalid");
            }
        }else{
            if([tempEventView superview]) {
                [tempEventView removeFromSuperview];
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(calendarDayTimelineView:eventViewWasSelected:)]) {
                [self.delegate calendarDayTimelineView:self eventViewWasSelected:(TKCalendarDayEventView *)aView];
            }else{
                FXDebugLog(kFXLogActiveSync, @"createEventForEventView invalid");
            }
        }
    }else{
        CGPoint aPointInTimeLine = [aView convertPoint:aTapPoint toView:self.scrollView];
        if(aPointInTimeLine.x >= kTimelineEventHorizontalOffset) {
            NSDate* aDate   = [self dateForPoint:aPointInTimeLine];
            CGRect aRect    = [self rectForPoint:aPointInTimeLine date:aDate];
            if(!tempEventView) {
                tempEventView = [[TKCalendarDayEventView alloc] initWithFrame:aRect];
                tempEventView.delegate = self;
            }else{
                tempEventView.frame = aRect;
            }
            tempEventView.startDate = aDate;
            tempEventView.endDate   = [[Calendar calendar] addToDate:aDate hours:1 minutes:0];
            if(![tempEventView superview]) {
                [self.scrollView addSubview:tempEventView];
            }
        }
    }
}

- (void)tapDetectingView:(TapDetectingView *)aView gotDoubleTapAtPoint:(CGPoint)aTapPoint
{
	CGPoint aPointInTimeLine = CGPointZero;
	if (aView == _timelineView) {
		aPointInTimeLine = aTapPoint;
		//FXDebugLog(kFXLogActiveSync, @"Double Tapped TimelineView at point %@", NSStringFromCGPoint(pointInTimeLine));
	}else{
		aPointInTimeLine = [aView convertPoint:aTapPoint toView:self.scrollView];
		//FXDebugLog(kFXLogActiveSync, @"Double Tapped EventView at point %@", NSStringFromCGPoint(pointInTimeLine));
	}
    
	if (self.delegate && [self.delegate respondsToSelector:@selector(calendarDayTimelineView:eventDateWasSelected:)]) {
		[self.delegate calendarDayTimelineView:self eventDateWasSelected:[self dateForPoint:aPointInTimeLine]];
	}
}

////////////////////////////////////////////////////////////////////////////////
// Drawing
////////////////////////////////////////////////////////////////////////////////
#pragma mark Drawing

- (void)drawRect:(CGRect)rect 
{
    // Drawing code
}

- (UIImage*)loadImage:(NSString*)aFileName 
{
    UIImage* anImage = nil;
    
    NSString* aResourcePath = [[FXLSafeZone getResourceBundle] resourcePath];
    anImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", aResourcePath, aFileName]];
    if(anImage) {
    }else{
        FXDebugLog(kFXLogActiveSync, @"Image failed to load: %@/%@", aResourcePath, aFileName);
    }
    
    return anImage;
}

- (UIImageView*)topBackground
{
	if(topBackground==nil) {
        UIImage* anImage = [self loadImage:@"Month Grid Top Bar.png"];
		topBackground = [[UIImageView alloc] initWithImage:anImage];
	}
	return topBackground;
}

- (UILabel*)labelWithX:(CGFloat)anX width:(CGFloat)aWidth
{
    UILabel* aLabel = [[UILabel alloc] initWithFrame:CGRectMake(anX, 0.0f, aWidth, titleBarHeight)];
    aLabel.textAlignment = NSTextAlignmentCenter;
    aLabel.backgroundColor = [UIColor clearColor];
    aLabel.font = [UIFont systemFontOfSize:kHeaderFontSize];
    aLabel.textColor = [UIColor colorWithRed:59.0f/255.0f green:73.0f/255.0f blue:88.0f/255.0f alpha:1.0f];
    return aLabel;
}

- (NSArray*)buildLabels
{
    if(numDays == 1) {
        UILabel* aLabel = [self labelWithX:0.0f width:kViewWidth];
        self.dateLabels = [NSMutableArray arrayWithObject:aLabel];
    }else if(numDays > 1) {
        self.dateLabels = [NSMutableArray arrayWithCapacity:numDays];
        CGFloat anX = kTimelineEventHorizontalOffset;
        for(int i = 0 ; i < numDays ; ++i) {
            UILabel* aLabel = [self labelWithX:anX width:dayWidth];
            [self.dateLabels addObject:aLabel];
            anX += dayWidth;
        }
    }
    return self.dateLabels;
}

- (void)next:(id)sender 
{
	NSDate *tomorrow = [self.currentDay dateByAddingDays:numDays];
    [self setDate:tomorrow];
}

- (void)prev:(id)sender 
{
	NSDate *yesterday = [self.currentDay dateByAddingDays:-numDays];
    [self setDate:yesterday];
}

- (UIButton*)leftArrow
{
	if(leftArrow==nil) {
		leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		leftArrow.tag = 0;
		[leftArrow addTarget:self action:@selector(prev:) forControlEvents:UIControlEventTouchUpInside];
        UIImage* anImage = [UIImage imageNamedTK:@"Month Calendar Left Arrow"];
		[leftArrow setImage:anImage forState:0];
		leftArrow.frame = CGRectMake(0, 3, kArrowWidth, titleBarHeight);
	}
	return leftArrow;
}

- (UIButton*)rightArrow
{
	if(rightArrow==nil ){
		rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
		rightArrow.tag = 1;
		[rightArrow addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
		rightArrow.frame = CGRectMake(self.frame.size.width-45, 3, kArrowWidth, titleBarHeight);
        UIImage* anImage = [UIImage imageNamedTK:@"Month Calendar Right Arrow"];
		[rightArrow setImage:anImage forState:0];
	}
	return rightArrow;
}

- (UIImageView*)shadow
{
	if(shadow==nil) {
		shadow = [[UIImageView alloc] initWithImage:[self loadImage:@"Month Calendar Shadow.png"]];
	}
	return shadow;
}

////////////////////////////////////////////////////////////////////////////////
// UIScrollViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    if(offset.x != scrollX) {
        scrollX = offset.x;
        self.allDayScrollView.contentOffset = CGPointMake(offset.x, 0.0f);
    }
}

@end

