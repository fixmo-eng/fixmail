/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "TKCalendarYearViewController.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "NSDate+TKCategory.h"
#import "TKCalendarMonthView.h"


@interface TKCalendarYearViewController () {
}

@end

@implementation TKCalendarYearViewController

// Properties
@synthesize monthViews;
@synthesize viewType;
@synthesize year;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

// Constants
static const CGFloat kSpacerX = 2.0f;
static const CGFloat kSpacerY = 2.0f;

// Static
static CGSize sViewSize;

+ (void)setSize:(CGSize)aSize
{
    sViewSize.height    = aSize.height;
    sViewSize.width     = aSize.width;
}

+ (CGSize)size
{
    return sViewSize;
}

-(void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;
	if (self.monthViews) {
		for (TKCalendarMonthView *monthView in self.monthViews) {
			[monthView setFirstDayOfTheWeek:_firstDayOfTheWeek];
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

-(void) viewDidLoad
{
	[super viewDidLoad];
    tileImage = [self loadImage:@"Month Calendar Date Transparent Tile.png"];
	
    NSDate* aDate = [[Calendar calendar] newYearsDayForDate:[NSDate date]];
    [self changeYearToDate:aDate];
}

- (TKCalendarMonthView*)createWithSize:(CGSize)aSize origin:(CGPoint)aPoint date:(NSDate*)aDate
{
    EMonthViewType aMonthViewType = kMonthViewColor;
    
    TKCalendarMonthView* aMonthView = [[TKCalendarMonthView alloc] initWithSize:aSize
                                                                         origin:aPoint
                                                                           date:aDate
                                                                       viewType:self.viewType
                                                                  monthViewType:aMonthViewType
                                                                      tileImage:tileImage
                                                                   firstDayOfTheWeek:self.firstDayOfTheWeek];
    aMonthView.delegate = self;
	aMonthView.dataSource = self;
	[self.view addSubview:aMonthView];
	[aMonthView rebuildWithReloadEvents:TRUE];
    
    return aMonthView;
}

- (UIImage*)loadImage:(NSString*)aFileName
{
    NSString* aResourcePath = [[FXLSafeZone getResourceBundle] resourcePath];
    return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", aResourcePath, aFileName]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if(self.monthViews.count != 12) {
        return;
    }
    int rows;
    int cols;
    CGSize aSize;
    switch(toInterfaceOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            rows = 4;
            cols = 3;
            aSize = CGSizeMake(sViewSize.width/rows - kSpacerX, sViewSize.height/cols - kSpacerY);
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            rows = 3;
            cols = 4;
            aSize = CGSizeMake(sViewSize.width/cols - kSpacerX, sViewSize.height/rows - kSpacerY);
            break;
    }
    
    int i = 0;
    for(int y = 0 ; y < rows ; ++y) {
        for(int x = 0 ; x < cols ; ++x) {
            CGPoint anOrigin = CGPointMake(x * (aSize.width+kSpacerX) + kSpacerX*0.5f, y * (aSize.height+kSpacerY));
            TKCalendarMonthView* aMonthView = [self.monthViews objectAtIndex:i];
            CGRect aRect;
            aRect.origin = anOrigin;
            aRect.size   = aSize;
            aMonthView.frame = aRect;
            [aMonthView setNeedsDisplay];
            ++i;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)changeYearToDate:(NSDate*)aDate
{
    Calendar* aCalendar = [Calendar calendar];
    self.year = [aCalendar newYearsDayForDate:aDate];

    NSDate* aLastDayOfYear = [aCalendar lastOfYearForDate:self.year];

    [self cacheEventsFromDate:self.year toDate:aLastDayOfYear];
    
    if(self.monthViews) {
        [self.monthViews removeAllObjects];
    }else{
        monthViews = [[NSMutableArray alloc] initWithCapacity:12];
    }
    
    int rows;
    int cols;
    if(sViewSize.width > sViewSize.height) {
        rows = 3;
        cols = 4;
    }else{
        rows = 4;
        cols = 3;
    }
    
    CGSize aSize = CGSizeMake(sViewSize.width/cols - kSpacerX, sViewSize.height/rows - kSpacerY);
    
    aDate = self.year;
    for(int y = 0 ; y < rows ; ++y) {
        for(int x = 0 ; x < cols ; ++x) {
            CGPoint anOrigin = CGPointMake(x * (aSize.width+kSpacerX) + kSpacerX*0.5f, y * (aSize.height+kSpacerY));
            TKCalendarMonthView* aMonthView = [self createWithSize:aSize origin:anOrigin date:aDate];
            [self.monthViews addObject:aMonthView];
            
            aDate = [aCalendar nextMonthForDate:aDate];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Virtual override
////////////////////////////////////////////////////////////////////////////////
#pragma mark Virtual override

- (void) cacheEventsFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate
{
    FXDebugLog(kFXLogFIXME, @"FIXME cacheEventsFromDate");
}


@end