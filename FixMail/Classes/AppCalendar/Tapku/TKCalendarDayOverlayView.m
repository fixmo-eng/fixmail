/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TKCalendarDayOverlayView.h"
#import "Calendar.h"
#import "TKCalendarDayTimelineView.h"
#import "TKTimelineGlobal.h"
#import "TKTimelineView.h"
#import "DateUtil.h"

@implementation OverlayView

@synthesize textColor;
@synthesize lineColor;


#define HORIZONTAL_LINE_DIFF 10.0f

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when xibless (interface buildder)
- (id)initWithFrame:(CGRect)frame
{
    if(!(self=[super initWithFrame:frame])) return nil;
    [self setupCustomInitialisation];
    return self;
}

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when using xib (interface buildder)
- (id)initWithCoder:(NSCoder *)decoder
{
    if(!(self=[super initWithCoder:decoder])) return nil;
    [self setupCustomInitialisation];
	return self;
}

- (void)setupCustomInitialisation
{
    self.textColor = [[UIColor alloc]initWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f];
    self.lineColor = [[UIColor alloc]initWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    //CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
    CGContextSetLineWidth(context, 0.5);
    CGContextTranslateCTM(context, -0.5, -0.5); // Translate context for clear line
    
    NSDate* aCurrentDate = [NSDate date];
    CGFloat aHorizontalOffset = 10;
    CGFloat aVerticalOffset = [self.timelineView positionForDate:aCurrentDate calendar:[Calendar calendar]];
    
    // Filled rectangle under time string so it doesn't jumble with times on timeline
    CGRect timeRect  = CGRectMake(aHorizontalOffset, aVerticalOffset - ((kTimelineFontSize + 4.0)*0.5),
                                  kTimelineTimeWidth * 2, kTimelineFontSize + kTimelineFontSpacer);
    CGContextSetRGBFillColor(context, 1.0f, 1.0f, 1.0f, 1.0f);
    CGContextFillRect(context, timeRect);
    
    
    // Current time as a string
    //
    [self.textColor set];
	
	//This should give 24Hour time if so inclined
	NSString *time = [[DateUtil getSingleton] stringFromDateTimeOnlyShort:aCurrentDate];
    UIFont* timeFont = [UIFont boldSystemFontOfSize:kTimelineFontSize];
    [time drawInRect:CGRectIntegral(timeRect)
            withFont:timeFont
       lineBreakMode:NSLineBreakByWordWrapping
           alignment:NSTextAlignmentRight];
    

    [self.lineColor set];

    // Outline rect around time string
    //
    timeRect.size.width += 4.0f;
    CGContextStrokeRect(context, timeRect);

    // Current time as a horizontal line
    //
    aHorizontalOffset = timeRect.origin.x + timeRect.size.width;
    CGContextBeginPath(context);
    CGContextMoveToPoint(context,    aHorizontalOffset,      aVerticalOffset);
    CGContextAddLineToPoint(context, self.bounds.size.width, aVerticalOffset);
    CGFloat dash1[] = {2.0f, 1.0f};
    CGContextSetLineDash(context, 0.0f, dash1, 2);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
}

@end