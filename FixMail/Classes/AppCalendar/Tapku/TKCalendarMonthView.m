//
//  TKCalendarMonthView.m
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "Calendar.h"
#import "CalendarController.h"
#import "CalendarDayController.h"
#import "ErrorAlert.h"
#import "NSDate+TKCategory.h"
#import "TKCalendarMonthView.h"
#import "TKGlobal.h"
#import "TKCalendarMonthTiles.h"
#import "UIImage+TKCategory.h"
#import "DateUtil.h"

// Constants
static const CGFloat kTitleHeight       = 24.0f;
static const CGFloat kTitleBarHeight    = 38.0f;
static const CGFloat kArrowWidth        = 48.0f;
static const CGFloat kDayFontHeight     = 15.0f;
static const CGFloat kArrowOffsetY      = 6.0f; 

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthView 
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthView 

@interface TKCalendarMonthView (private)
@property (strong,nonatomic) UIScrollView*  tileBox;
@property (strong,nonatomic) UIImageView*   topBackground;
@property (strong,nonatomic) UILabel*       monthYear;
@property (strong,nonatomic) UIButton*      leftArrow;
@property (strong,nonatomic) UIButton*      rightArrow;
@property (strong,nonatomic) UIImageView*   shadow;
@end

@implementation TKCalendarMonthView

@synthesize delegate;
@synthesize dataSource;
@synthesize eventsArray;
@synthesize scrollDirection;

@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

-(void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;
	if (currentTile) {
		[currentTile setFirstDayOfTheWeek:_firstDayOfTheWeek];
	}
}

- (void)setTileSize:(CGSize)aSize
{
    if(currentTile) {
        CGRect aRect = currentTile.frame;
        aRect.size = aSize;
        currentTile.frame = aRect;
        [currentTile setTileSize:aSize];
        tileWidth = currentTile.tileWidth;
        tileHeight = currentTile.tileHeight;
        aRect = self.frame;
        aRect.size.width = currentTile.frame.size.width;
        aRect.size.height = currentTile.frame.size.height;
        self.frame = aRect;
        aRect.origin.x = 0.0f;
        aRect.origin.y = kTitleBarHeight;
        self.tileBox.frame = aRect;
        self.topBackground.frame = CGRectMake(0, 0, self.bounds.size.width, self.topBackground.frame.size.height);
        self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21+kTitleBarHeight,
                                       self.bounds.size.width, self.shadow.frame.size.height);
        [self buildTitleBar:currentTile.monthDate];
        [currentTile setNeedsDisplay];
    }
}

- (void)buildTitleBar:(NSDate*)aDate
{
    NSArray* aSubViews = [self.topBackground subviews];
    for(UIView* aView in aSubViews) {
        [aView removeFromSuperview];
    }
	
	DateUtil *dateUtil = [DateUtil getSingleton];
    
    // Title
    self.monthYear.text = [NSString stringWithFormat:@"%@ %@", [dateUtil stringFromDateMonthOnly:aDate], [dateUtil stringFromDateYearOnly:aDate]];
    CGRect aRect = self.monthYear.frame;
    aRect.size.width = self.topBackground.frame.size.width;
    self.monthYear.frame = aRect;
	[self addSubview:self.monthYear];
    
    if(viewType == kCalendarMonth && !isIPad) {
        [self addSubview:self.leftArrow];
        
        CGFloat aWidth = tileWidth * 7.0f;
		rightArrow.frame = CGRectMake(aWidth - kArrowWidth, 0, kArrowWidth, kTitleHeight);
        [self addSubview:self.rightArrow];
    }
    
    // Build array of strings for day columns
    //	
	NSArray *shortWeekdaySymbols = [dateUtil arrayOfStringsShortWeekdaySymbols];
	int y = self.firstDayOfTheWeek - 1;
	
	NSMutableArray *orderedWeekdaySymbols;
	if (y <= 0 || y > shortWeekdaySymbols.count) {
		orderedWeekdaySymbols = [shortWeekdaySymbols mutableCopy];
	} else {
		orderedWeekdaySymbols = [[NSMutableArray alloc] initWithCapacity:shortWeekdaySymbols.count];
		for (int i = 0; i < shortWeekdaySymbols.count; i++) {
			[orderedWeekdaySymbols addObject:[shortWeekdaySymbols objectAtIndex:y]];
			
			if (y == shortWeekdaySymbols.count - 1) {
				y = 0;
			} else {
				y++;
			}
		}
	}
    
	int i = 0;
	for(NSString* s in orderedWeekdaySymbols) {
		UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(tileWidth * i,
                                                                   kTitleBarHeight-kDayFontHeight,
                                                                   tileWidth, kDayFontHeight)];
		[self.topBackground addSubview:label];
		label.text = s;
		label.textAlignment     = NSTextAlignmentCenter;
		label.shadowColor       = [UIColor whiteColor];
		label.shadowOffset      = CGSizeMake(0, 1);
		label.font              = [UIFont systemFontOfSize:11];
		label.backgroundColor   = [UIColor clearColor];
		label.textColor         = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
		i++;
	}
}

- (id)initWithSize:(CGSize)aSize
            origin:(CGPoint)anOrigin
              date:(NSDate*)aDate
          viewType:(ECalendarViewType)aCalendarViewType
     monthViewType:(EMonthViewType)aMonthViewType
         tileImage:(UIImage*)aTileImage
     firstDayOfTheWeek:(int)firstDayOfTheWeek
{
	if (!(self = [super initWithFrame:CGRectZero])) return nil;
    
    isIPad = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad;

    viewType        = aCalendarViewType;
    monthViewType   = aMonthViewType;
    scrollDirection = kScrollHorizontal;

    tileImage       = aTileImage;
        
    lock = [[NSLock alloc] init];

	self.firstDayOfTheWeek = firstDayOfTheWeek;
    
    // Tile view
    //
	currentTile = [[TKCalendarMonthTiles alloc] initWithDelegate:self
                                                            size:aSize
                                                            date:aDate
                                                        viewType:viewType
                                                   monthViewType:monthViewType 
                                                       tileImage:aTileImage
                                                          events:nil
                                                  titleBarHeight:kTitleBarHeight
                                                startDayOn:self.firstDayOfTheWeek];
    tileWidth   = currentTile.tileWidth;
    tileHeight  = currentTile.tileHeight;
	[currentTile setTarget:self action:@selector(tile:)];
	
    // Set my frame
    //
	CGRect r = CGRectMake(anOrigin.x, anOrigin.y, self.tileBox.bounds.size.width, self.tileBox.bounds.size.height + self.tileBox.frame.origin.y);
	self.frame = r;
	
    // Title bar
    //
	[self addSubview:self.topBackground];
	self.topBackground.frame = CGRectMake(0, 0, self.bounds.size.width, self.topBackground.frame.size.height);
	[self.tileBox addSubview:currentTile];
	[self addSubview:self.tileBox];
	
    // Shadow below calendar
    //
    if(viewType == kCalendarMonth) {
        self.shadow.frame = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.bounds.size.width, self.shadow.frame.size.height);
        [self addSubview:self.shadow];
    }
    
    [self buildTitleBar:aDate];

	return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:@"Calendar Month View" where:where exception:e];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (void)prev
{
	[self changeToMonth:[[Calendar calendar] previousMonthForDate:currentTile.monthDate] isNext:FALSE];
}

- (void)next
{
	[self changeToMonth:[[Calendar calendar] nextMonthForDate:currentTile.monthDate] isNext:TRUE];
}

- (NSDate*)dateForMonthChange:(UIView*)sender 
{
	BOOL isNext = (sender.tag == 1);
    Calendar* aCalendar         = [Calendar calendar];
	NSDate* nextMonth = isNext  ? [aCalendar nextMonthForDate:currentTile.monthDate]
                                : [aCalendar previousMonthForDate:currentTile.monthDate];
	TKDateInformation nextInfo  = [aCalendar dateInformationFromDate:nextMonth];
	NSDate* localNextMonth      = [aCalendar dateFromDateInformation:nextInfo];
	
	return localNextMonth;
}

- (void)changeMonthAnimation:(UIView*)sender
{
	BOOL isNext = (sender.tag == 1);
    Calendar* aCalendar         = [Calendar calendar];
	NSDate* nextMonth = isNext  ? [aCalendar nextMonthForDate:currentTile.monthDate]
                                : [aCalendar previousMonthForDate:currentTile.monthDate];
	[self changeToMonth:nextMonth isNext:isNext];
}

- (void)changeToMonth:(NSDate*)aDate isNext:(BOOL)isNext
{
    Calendar* aCalendar         = [Calendar calendar];
    aDate                       = [aCalendar firstOfMonthForDate:aDate];
	TKDateInformation nextInfo  = [aCalendar dateInformationFromDate:aDate];
	NSDate* localNextMonth      = [aCalendar dateFromDateInformation:nextInfo];
	
	NSArray* dates  = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:aDate startOn:self.firstDayOfTheWeek];
	NSArray* ar     = [self.dataSource calendarMonthView:self eventsFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
	TKCalendarMonthTiles* newTile = [[TKCalendarMonthTiles alloc] initWithDelegate:self
                                                                              size:self.bounds.size
                                                                              date:aDate
                                                                          viewType:viewType
                                                                     monthViewType:monthViewType
                                                                         tileImage:tileImage
                                                                            events:ar
                                                                    titleBarHeight:kTitleBarHeight
                                                                  startDayOn:self.firstDayOfTheWeek];
    tileWidth   = newTile.tileWidth;
    tileHeight  = newTile.tileHeight;
	[newTile setTarget:self action:@selector(tile:)];
	
	int overlap =  0;
	if(isNext) {
		overlap = [newTile.monthDate isEqualToDate:[dates objectAtIndex:0]] ? 0 : tileHeight;
	}else{
		overlap = [currentTile.monthDate compare:[dates lastObject]] !=  NSOrderedDescending ? tileHeight : 0;
	}
	
	if(self.scrollDirection == kScrollHorizontal){
        float x = isNext ? currentTile.bounds.size.width - overlap : newTile.bounds.size.width * -1 + overlap +2;
        newTile.frame = CGRectMake(x, 0, newTile.frame.size.width, newTile.frame.size.height);
    }else{
        float y = isNext ? currentTile.bounds.size.height - overlap : newTile.bounds.size.height * -1 + overlap +2;
        newTile.frame = CGRectMake(0, y, newTile.frame.size.width, newTile.frame.size.height);
    }
	newTile.alpha = 0;
	[self.tileBox addSubview:newTile];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.1];
	newTile.alpha = 1;

	[UIView commitAnimations];

	self.userInteractionEnabled = NO;
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDidStopSelector:@selector(animationEnded)];
	[UIView setAnimationDelay:0.1];
	[UIView setAnimationDuration:0.4];
	
	if(isNext){
        if(self.scrollDirection == kScrollHorizontal) {
            currentTile.frame   = CGRectMake(-1 * currentTile.bounds.size.width + overlap + 2, 0.0f,
                                             currentTile.frame.size.width, currentTile.frame.size.height);
        }else{
            currentTile.frame   = CGRectMake(0, -1 * currentTile.bounds.size.height + overlap + 2,
                                             currentTile.frame.size.width, currentTile.frame.size.height);
        }
		newTile.frame       = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame  = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame          = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		self.shadow.frame   = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
	}else{
		newTile.frame       = CGRectMake(0, 1, newTile.frame.size.width, newTile.frame.size.height);
		self.tileBox.frame  = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, newTile.frame.size.height);
		self.frame          = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
        if(self.scrollDirection == kScrollHorizontal) {
            currentTile.frame   = CGRectMake(newTile.frame.size.width - overlap, 0.0f,
                                             currentTile.frame.size.width, currentTile.frame.size.height);
		}else{
            currentTile.frame   = CGRectMake(0,  newTile.frame.size.height - overlap,
                                             currentTile.frame.size.width, currentTile.frame.size.height);

        }
		self.shadow.frame   = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
	}
	[UIView commitAnimations];
	
	oldTile     = currentTile;
	currentTile = newTile;

	DateUtil *dateUtil = [DateUtil getSingleton];
	monthYear.text = [NSString stringWithFormat:@"%@ %@", [dateUtil stringFromDateMonthOnly:localNextMonth], [dateUtil stringFromDateYearOnly:localNextMonth]];
}

- (void)adjustFrame
{
    self.tileBox.frame  = CGRectMake(self.tileBox.frame.origin.x, self.tileBox.frame.origin.y, self.tileBox.frame.size.width, currentTile.frame.size.height);
    self.frame          = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
    self.shadow.frame   = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
}

- (void)changeMonth:(UIButton *)sender
{	
	NSDate* newDate = [self dateForMonthChange:sender];
	if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)]
    && ![self.delegate calendarMonthView:self monthShouldChange:newDate animated:YES] )
		return;
	
	if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)] ) 
		[self.delegate calendarMonthView:self monthWillChange:newDate animated:YES];
	
	[self changeMonthAnimation:sender];
    
	if([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
		[self.delegate calendarMonthView:self monthDidChange:currentTile.monthDate animated:YES];
}

- (void)changeToMonthForDate:(NSDate*)aDate
{
    NSDate* aMonthDate = [self monthDate];
    if([aDate monthsBetweenDate:aMonthDate] != 0) {
        if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)]
            && ![self.delegate calendarMonthView:self monthShouldChange:aDate animated:YES] )
            return;
        
        if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)] )
            [self.delegate calendarMonthView:self monthWillChange:aDate animated:YES];
        
        BOOL anIsNext = [aDate compare:aMonthDate] == NSOrderedDescending;
        [self changeToMonth:aDate isNext:anIsNext];
        
        if([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
            [self.delegate calendarMonthView:self monthDidChange:currentTile.monthDate animated:YES];
    }else{
        Calendar* aCalendar = [Calendar calendar];
        TKDateInformation info = [aCalendar dateInformationFromDate:aDate];
        [currentTile selectDay:info.day];
    }
}

- (void)animationEnded
{
	self.userInteractionEnabled = YES;
	[oldTile removeFromSuperview];
	oldTile = nil;
}

- (NSDate*)dateSelected
{
	return [currentTile dateSelected];
}

- (NSDate*)monthDate
{
	return currentTile.monthDate;
}

- (NSInteger)month
{
	return currentTile.month;
}

- (NSInteger)year
{
	return currentTile.year;
}

- (void)rebuild
{
    [currentTile rebuild];
}

- (void)selectDate:(NSDate*)date
{
    if(!date) {
        [currentTile selectDay:0];
		return;
    }
    
    Calendar* aCalendar = [Calendar calendar];
	TKDateInformation info = [aCalendar dateInformationFromDate:date];
	NSDate *month = [aCalendar firstOfMonthForDate:date];
	
	if([month isEqualToDate:[currentTile monthDate]]) {
		[currentTile selectDay:info.day];
		return;
	}else if(viewType == kCalendarMonth) {
		if([delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)]
        && ![self.delegate calendarMonthView:self monthShouldChange:month animated:YES])
			return;
		
		if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)])
			[self.delegate calendarMonthView:self monthWillChange:month animated:YES];
		
		NSArray* dates  = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:month startOn:self.firstDayOfTheWeek];
		NSArray* data   = [self.dataSource calendarMonthView:self eventsFromDate:[dates objectAtIndex:0] toDate:[dates lastObject]];
		TKCalendarMonthTiles *newTile = [[TKCalendarMonthTiles alloc] initWithDelegate:self
                                                                                  size:self.bounds.size
                                                                                  date:month
                                                                              viewType:viewType
                                                                         monthViewType:monthViewType
                                                                             tileImage:tileImage
                                                                                events:data
                                                                        titleBarHeight:kTitleBarHeight
                                                                      startDayOn:self.firstDayOfTheWeek];
        tileWidth   = newTile.tileWidth;
        tileHeight  = newTile.tileHeight;
		[newTile setTarget:self action:@selector(tile:)];
		[currentTile removeFromSuperview];
		currentTile = newTile;
		[self.tileBox addSubview:currentTile];
        
		self.tileBox.frame  = CGRectMake(0, tileHeight, newTile.frame.size.width, newTile.frame.size.height);
		self.frame          = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, self.tileBox.frame.size.height+self.tileBox.frame.origin.y);
		self.shadow.frame   = CGRectMake(0, self.frame.size.height-self.shadow.frame.size.height+21, self.shadow.frame.size.width, self.shadow.frame.size.height);
		
		DateUtil *dateUtil = [DateUtil getSingleton];
		self.monthYear.text = [NSString stringWithFormat:@"%@ %@",[dateUtil stringFromDateMonthOnly:date],[dateUtil stringFromDateYearOnly:date]];
            
		[currentTile selectDay:info.day];
		
		if([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
			[self.delegate calendarMonthView:self monthDidChange:date animated:NO];
	}
}

- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    [lock lock];
    @try {
        if(!self.eventsArray || aReloadEvents) {
            NSArray* aDates  = [TKCalendarMonthTiles rangeOfDatesInMonthGrid:[currentTile monthDate] startOn:self.firstDayOfTheWeek];
            self.eventsArray = [self.dataSource calendarMonthView:self eventsFromDate:[aDates objectAtIndex:0] toDate:[aDates lastObject]];
        }
        TKCalendarMonthTiles* aCalendarMonthTiles = [[TKCalendarMonthTiles alloc] initWithDelegate:self
                                                                                              size:self.bounds.size
                                                                                              date:[currentTile monthDate]
                                                                                          viewType:viewType
                                                                                     monthViewType:monthViewType
                                                                                         tileImage:tileImage
                                                                                            events:self.eventsArray
                                                                                    titleBarHeight:kTitleBarHeight
                                                                                  startDayOn:self.firstDayOfTheWeek];
        tileWidth   = aCalendarMonthTiles.tileWidth;
        tileHeight  = aCalendarMonthTiles.tileHeight;
        [aCalendarMonthTiles setTarget:self action:@selector(tile:)];
        
        [self.tileBox addSubview:aCalendarMonthTiles];
        [currentTile removeFromSuperview];
        currentTile = aCalendarMonthTiles;
    }@catch (NSException* e) {
        [self _logException:@"reload" exception:e];
    }
    [lock unlock];
}

////////////////////////////////////////////////////////////////////////////////
// CalendarMonthTouchDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark CalendarMonthTouchDelegate

- (BOOL)dateSelected:(NSDate*)aDate touch:(UITouch *)aTouch
{
    BOOL aDateSelected = FALSE;
    @try {
        aDateSelected = [self.delegate calendarMonthView:self dateSelected:aDate touch:aTouch];
    }@catch (NSException* e) {
        [self _logException:@"dateSelected" exception:e];
    }
    return aDateSelected;
}

- (void)dayTapped:(int)aDay changeMonthInDirection:(int)aDirection touch:aTouch
{
    UIButton* b = aDirection > 1 ? self.rightArrow : self.leftArrow;
    
    NSDate* newMonth = [self dateForMonthChange:b];
    if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthShouldChange:animated:)]
        && ![delegate calendarMonthView:self monthShouldChange:newMonth animated:YES])
        return;
    
    if ([self.delegate respondsToSelector:@selector(calendarMonthView:monthWillChange:animated:)])
        [self.delegate calendarMonthView:self monthWillChange:newMonth animated:YES];
    
    [self changeMonthAnimation:b];
        
    Calendar* aCalendar = [Calendar calendar];
    TKDateInformation info = [aCalendar dateInformationFromDate:[currentTile monthDate]];
    info.day = aDay;
    
    NSDate* dateForMonth = [aCalendar dateFromDateInformation:info];
    [currentTile selectDay:aDay];
    
    if([self.delegate respondsToSelector:@selector(calendarMonthView:dateSelected:)])
        [self.delegate calendarMonthView:self dateSelected:dateForMonth touch:aTouch];
    
    if([self.delegate respondsToSelector:@selector(calendarMonthView:monthDidChange:animated:)])
        [self.delegate calendarMonthView:self monthDidChange:dateForMonth animated:YES];
}

- (void)dateEntered:(NSDate*)aDate location:(CGPoint)aLocation
{
    [self.delegate calendarMonthView:self dateEntered:aDate location:aLocation];
}

- (void)eventViewTapped:(TKCalendarDayEventView*)anEventView location:(CGPoint)aLocation
{
    [self.delegate calendarMonthView:self eventViewSelected:anEventView location:aLocation];
}

- (void)eventViewEntered:(TKCalendarDayEventView*)anEventView location:(CGPoint)aLocation
{
    [self.delegate calendarMonthView:self eventViewEntered:anEventView location:aLocation];
}

////////////////////////////////////////////////////////////////////////////////
// Locking
////////////////////////////////////////////////////////////////////////////////
#pragma mark Locking

- (void)lock
{
    [lock lock];
}

- (void)unlock
{
    [lock unlock];
}

////////////////////////////////////////////////////////////////////////////////
// UI
////////////////////////////////////////////////////////////////////////////////
#pragma mark UI

- (UIImage*)loadImage:(NSString*)aFileName
{
    UIImage* anImage = NULL;
    
    NSString* aResourcePath = [[FXLSafeZone getResourceBundle] resourcePath];
    anImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", aResourcePath, aFileName]];  
    if(!anImage) {
        FXDebugLog(kFXLogActiveSync, @"Calendar image failed to load: %@ %@", aResourcePath, aFileName);
    }
    
    return anImage;
}

- (UIImageView*)topBackground
{
	if(topBackground == nil) {
		topBackground = [[UIImageView alloc] initWithImage:[self loadImage:@"Month Grid Top Bar.png"]];
	}
	return topBackground;
}

- (UILabel*)monthYear
{
	if(monthYear == nil) {
		monthYear = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tileBox.frame.size.width, kTitleHeight)];
		
		monthYear.textAlignment     = NSTextAlignmentCenter;
		monthYear.backgroundColor   = [UIColor clearColor];
		monthYear.font              = [UIFont boldSystemFontOfSize:22];
		monthYear.textColor         = [UIColor colorWithRed:59/255. green:73/255. blue:88/255. alpha:1];
	}
	return monthYear;
}

- (UIButton*)leftArrow
{
	if(leftArrow == nil) {
		leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        
		leftArrow.tag = 0;
		[leftArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
		[leftArrow setImage:[self loadImage:@"Month Calendar Left Arrow.png"] forState:0];
		leftArrow.frame = CGRectMake(0, kArrowOffsetY, kArrowWidth, kTitleHeight);
	}
	return leftArrow;
}

- (UIButton*)rightArrow
{
	if(rightArrow == nil) {
		rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        
		rightArrow.tag = 1;
		[rightArrow addTarget:self action:@selector(changeMonth:) forControlEvents:UIControlEventTouchUpInside];
        CGFloat aWidth = tileWidth * 7.0f;
		rightArrow.frame = CGRectMake(aWidth - kArrowWidth, kArrowOffsetY, kArrowWidth, kTitleHeight);
		[rightArrow setImage:[self loadImage:@"Month Calendar Right Arrow.png"] forState:0];
	}
	return rightArrow;
}

- (UIScrollView*)tileBox
{
	if(tileBox == nil) {
        CGFloat aWidth = tileWidth * 7.0f;
		tileBox = [[UIScrollView alloc] initWithFrame:CGRectMake(0, kTitleBarHeight, aWidth, currentTile.frame.size.height)];
	}
	return tileBox;
}

- (UIImageView*)shadow
{
	if(shadow == nil) {
		shadow = [[UIImageView alloc] initWithImage:[self loadImage:@"Month Calendar Shadow.png"]];
	}
	return shadow;
}

@end