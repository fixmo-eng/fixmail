/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecurrenceEditDelegate.h"
#import "DatePickerDelegate.h"

@class BaseFolder;
@class Event;
@class NumberPickerController;
@class RecurrenceTypeSheet;


@interface RecurrenceEditController : UITableViewController <UIActionSheetDelegate, RecurrenceEditDelegate, DatePickerDelegate> {
    BOOL                        isPush;
}

// Properties
@property (nonatomic, strong) Event*                    event;
@property (nonatomic, strong) NSMutableArray*           dayOfWeekButtons;
@property (nonatomic, strong) NSMutableArray*           weekOfMonthButtons;

@property (nonatomic, strong) RecurrenceTypeSheet*      recurrenceTypeSheet;
@property (nonatomic, strong) NumberPickerController*   numberPickerController;

@end