/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecurrenceTypeSheet.h"
#import "Event.h"
#import "EventRecurrence.h"

@implementation RecurrenceTypeSheet

@synthesize event;
@synthesize tableView;
@synthesize indexPath;
@synthesize delegate;

// Action Sheet types
typedef enum
{
    kRepeatNever            = 0,
    kRepeatDaily            = 1,
    kRepeatWeekly           = 2,
    kRepeatMonthly          = 3,
    kRepeatMonthlyOnNthDay  = 4,
    kRepeatYearly           = 5,
    kRepeatYearlyOnNthDay   = 6
} ERepeatActions;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView
     navController:(UINavigationController*)aNavController
             event:(Event*)anEvent
         tableView:(UITableView*)aTableView
         indexPath:(NSIndexPath*)anIndexPath
{
    if (self = [super init]) {
        self.event      = anEvent;
        self.tableView  = aTableView;
        self.indexPath  = anIndexPath;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:FXLLocalizedStringFromTable(@"NEVER", @"EventRecurrence", @"Choosing type of event recurrence - never")
                                           otherButtonTitles:FXLLocalizedStringFromTable(@"DAILY", @"EventRecurrence", @"Choosing type of event recurrence - daily"),
															FXLLocalizedStringFromTable(@"WEEKLY", @"EventRecurrence", @"Choosing type of event recurrence - weekly"),
															FXLLocalizedStringFromTable(@"MONTHLY", @"EventRecurrence", @"Choosing type of event recurrence - monthly"),
															FXLLocalizedStringFromTable(@"MONTHLY_ON_NTH_DAY", @"EventRecurrence", @"Choosing type of event recurrence - monthly on nth day"),
															FXLLocalizedStringFromTable(@"YEARLY", @"EventRecurrence", @"Choosing type of event recurrence - yearly"),
															FXLLocalizedStringFromTable(@"YEARLY_ON_NTH_DAY", @"EventRecurrence", @"Choosing type of event recurrence - yearly on nth day"),
                                                             nil];
        if(IS_IPAD()) {
            [anActionSheet showInView:aView];
        }else{
            [anActionSheet showFromToolbar:aNavController.toolbar];
        }
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ERepeatActions aRepeatAction = (ERepeatActions)buttonIndex;
    ERecurrenceType aRecurrenceType;
    switch(aRepeatAction) {
        case kRepeatDaily:
            aRecurrenceType = kRecurrenceDaily;
            break;
        case kRepeatWeekly:
            aRecurrenceType = kRecurrenceWeekly;
            break;
        case kRepeatMonthly:
            aRecurrenceType = kRecurrenceMonthly;
            break;
        case kRepeatYearly:
            aRecurrenceType = kRecurrenceYearly;
            break;
        case kRepeatMonthlyOnNthDay:
            aRecurrenceType = kRecurrenceMonthlyOnNthDay;
            break;
        case kRepeatYearlyOnNthDay:
            aRecurrenceType = kRecurrenceYearlyOnNthDay;
            break;
        case kRepeatNever:
            aRecurrenceType = kRecurrenceNone;
            break;
    }
    
    [self.delegate setRecurrenceType:aRecurrenceType];
    
}

@end