/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ReminderEditController.h"
#import "Event.h"

@implementation ReminderEditController

// Properties
@synthesize event;

// Types
//
typedef enum
{
    kAlertNone                  = 0,
    kAlert0Minutes              = 1,
    kAlert5Minutes              = 2,
    kAlert10Minutes             = 3,
    kAlert15Minutes             = 4,
    kAlert30Minutes             = 5,
    kAlert1Hour                 = 6,
    kAlert2Hours                = 7,
    kAlert1Day                  = 8,
    kAlert2Days                 = 9
} EAlertActions;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
        
    BOOL hasCheck = FALSE;
    switch((EAlertActions)indexPath.row) {
        case kAlertNone:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"NONE", @"ReminderEditController", @"Chosen option not to have a reminder for an event");
            if(event.reminderMinutes == -1) hasCheck = TRUE;
            break;
        case kAlert0Minutes:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"0_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 0 minutes before an event");
            if(event.reminderMinutes == 0) hasCheck = TRUE;
            break;
        case kAlert5Minutes:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"5_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 5 minutes before an event");
            if(event.reminderMinutes == 5) hasCheck = TRUE;
            break;
        case kAlert10Minutes:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"10_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 10 minutes before an event");
            if(event.reminderMinutes == 10) hasCheck = TRUE;
            break;
        case kAlert15Minutes:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"15_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 15 minutes before an event");
            if(event.reminderMinutes == 15) hasCheck = TRUE;
            break;
        case kAlert30Minutes:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"30_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 30 minutes before an event");
            if(event.reminderMinutes == 30) hasCheck = TRUE;
            break;
        case kAlert1Hour:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"1_HOUR", @"ReminderEditController", @"Chosen option to have a reminder 1 hour before an event");
            if(event.reminderMinutes == 60) hasCheck = TRUE;
            break;
        case kAlert2Hours:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"2_HOURS", @"ReminderEditController", @"Chosen option to have a reminder 2 hours before an event");
            if(event.reminderMinutes == 120) hasCheck = TRUE;
            break;
        case kAlert1Day:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"1_DAY", @"ReminderEditController", @"Chosen option to have a reminder 1 day before an event");
            if(event.reminderMinutes ==  24 * 60) hasCheck = TRUE;
            break;
        case kAlert2Days:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"2_DAYS", @"ReminderEditController", @"Chosen option to have a reminder 2 days before an event");
            if(event.reminderMinutes == 48 * 60) hasCheck = TRUE;
            break;
    }
            
    if(hasCheck) {
        cell.accessoryType          = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType          = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSInteger originalReminderMins = event.reminderMinutes;
    
    switch((EAlertActions)indexPath.row) {
        case kAlertNone:
            event.reminderMinutes = -1;
            break;
        case kAlert0Minutes:
            event.reminderMinutes = 0;
            break;
        case kAlert5Minutes:
            event.reminderMinutes = 5;
            break;
        case kAlert10Minutes:
            event.reminderMinutes = 10;
            break;
        case kAlert15Minutes:
            event.reminderMinutes = 15;
            break;
        case kAlert30Minutes:
            event.reminderMinutes = 30;
            break;
        case kAlert1Hour:
            event.reminderMinutes = 60;
            break;
        case kAlert2Hours:
            event.reminderMinutes = 120;
            break;
        case kAlert1Day:
            event.reminderMinutes = 24 * 60;
            break;
        case kAlert2Days:
            event.reminderMinutes = 48 * 60;
            break;
    }
    
    if (event.reminderMinutes != originalReminderMins && [self.changeDelegate respondsToSelector:@selector(reminderEditController:didChangeEvent:)])
    {
        [self.changeDelegate reminderEditController:self didChangeEvent:self.event];
    }
    
    [self.tableView reloadData];
}

@end
