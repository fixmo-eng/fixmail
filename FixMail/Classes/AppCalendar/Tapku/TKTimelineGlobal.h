/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

static const CGFloat kTimelineFontSize                 = 14.0;      // Height of the font
static const CGFloat kTimelineFontSpacer               = 4.0f;      // Height of the spacer between lines of text
static const CGFloat kTimelineVerticalDiff             = 50.0;
static const CGFloat kTimelineHorizontalOffset         = 4.0;
static const CGFloat kTimelineVerticalOffset           = 5.0;
static const CGFloat kTimelineVerticalMargin           = 150.0f;     // Height of the gray areas above and below the time line, to fill the scroll view bounce
static const CGFloat kTimelineTimeWidth                = 20.0f;
static const CGFloat kTimelinePeriodWidth              = 26.0f;
static const CGFloat kTimelineHorizontalLineDiff       = 10.0;

static const CGFloat kTimelineEventHorizontalOffset    = kTimelineHorizontalOffset + kTimelineTimeWidth + kTimelinePeriodWidth + kTimelineHorizontalLineDiff;

@interface TKTimelineGlobal : NSObject

@end
