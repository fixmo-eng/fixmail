/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "TKCalendarMonthTiles.h"
#import "CalendarController.h"
#import "CalendarTheme.h"
#import "TKCalendarDayEventView.h"

@interface TKCalendarMonthTiles (private)
@property (strong,nonatomic) UIImageView*   selectedImageView;
@property (strong,nonatomic) UILabel*       currentDay;
@property (strong,nonatomic) UILabel*       dot;
@end

@implementation TKCalendarMonthTiles

// Properties
@synthesize monthTouchDelegte;
@synthesize monthDate;
@synthesize selectedDate;
@synthesize month;
@synthesize year;
@synthesize events;
@synthesize tileWidth;
@synthesize tileHeight;

// Constants
static const CGFloat kDateWidth         = 47.0f;
static const CGFloat kDateHeight        = 45.0f;

static const CGFloat kEventFontSize     = 13.0;
static const CGFloat kEventStringHeight = kEventFontSize + 3.0f;
static const CGFloat kDotFontSize       = 18.0;
static const CGFloat kDateFontSize      = 22.0;
static const CGFloat kTimeBorderWidth   = 3.0f;

// Static
static UIImage*        sSelectedImage       = nil;
static UIImage*        sTodayImage          = nil;
static UIImage*        sTodaySelectedImage  = nil;

+ (NSArray*)rangeOfDatesInMonthGrid:(NSDate*)date startOn:(int)firstDayOfTheWeek
{
	// FIXME: Generalize to any first day of the week
    Calendar* aCalendar = [Calendar calendar];
    
	TKDateInformation info = [aCalendar dateInformationFromDate:date];
	info.day    = 1;
	info.hour   = 0;
	info.minute = 0;
	info.second = 0;
    
	NSDate* currentMonth    = [aCalendar dateFromDateInformation:info];
	NSDate* previousMonth   = [aCalendar previousMonthForDate:currentMonth];
	NSDate* nextMonth       = [aCalendar nextMonthForDate:currentMonth];
    
    info = [aCalendar dateInformationFromDate:currentMonth];
    NSDate* firstDate;
	if(info.weekday > 1 && (firstDayOfTheWeek == 1)) {
		TKDateInformation info2 = [aCalendar dateInformationFromDate:previousMonth];
		int preDayCnt   = [previousMonth daysBetweenDate:currentMonth];
		info2.day       = preDayCnt - info.weekday + 2;
		firstDate       = [aCalendar dateFromDateInformation:info2];
	} else if ((firstDayOfTheWeek == 2) && (info.weekday != 2)) {
		TKDateInformation info2 = [aCalendar dateInformationFromDate:previousMonth];
		int preDayCnt = [previousMonth daysBetweenDate:currentMonth];
		if(info.weekday==1){
			info2.day = preDayCnt - 5;
		}else{
			info2.day = preDayCnt - info.weekday + 3;
		}
		firstDate = [aCalendar dateFromDateInformation:info2];
	}else{
		firstDate = currentMonth;
	}
	
	int daysInMonth = [currentMonth daysBetweenDate:nextMonth];
	info.day = daysInMonth;
    
	NSDate* lastInMonth = [aCalendar dateFromDateInformation:info];
	TKDateInformation lastDateInfo = [aCalendar dateInformationFromDate:lastInMonth];
    NSDate* lastDate;
	if((lastDateInfo.weekday < 7) && (firstDayOfTheWeek == 1)) {
		lastDateInfo.day = 7 - lastDateInfo.weekday;
		lastDateInfo.month++;
		lastDateInfo.weekday = 0;
		if(lastDateInfo.month > 12) {
			lastDateInfo.month = 1;
			lastDateInfo.year++;
		}
        lastDateInfo.hour = 23;
        lastDateInfo.minute = 59;
        lastDateInfo.second = 59;
		lastDate = [aCalendar dateFromDateInformation:lastDateInfo];
	} else if ((firstDayOfTheWeek == 2) && (lastDateInfo.weekday != 1)) {
		lastDateInfo.day = 8 - lastDateInfo.weekday;
		lastDateInfo.month++;
		if(lastDateInfo.month>12) {
            lastDateInfo.month = 1;
            lastDateInfo.year++;
        }
        lastDateInfo.hour = 23;
        lastDateInfo.minute = 59;
        lastDateInfo.second = 59;
		lastDate = [aCalendar dateFromDateInformation:lastDateInfo];
	}else{
		lastDate = lastInMonth;
	}
	
	return [NSArray arrayWithObjects:firstDate, lastDate, nil];
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (void)setTileSize:(CGSize)aSize
{
    TKDateInformation dateInfo = [[Calendar calendar] dateInformationFromDate:monthDate];
    
    // Number of rows varies between 5 and 6 depending on number of days in month and how weeks are aligned
    //
	int numRows = (daysInMonth + dateInfo.weekday - 1);
		
	if((dateInfo.weekday == 1) && (self.firstDayOfTheWeek == 2)) {
        numRows = daysInMonth + 6;
    }
	
	if(self.firstDayOfTheWeek == 2) numRows--;
	numRows = (numRows / 7) + ((numRows % 7 == 0) ? 0:1);
    
    // Tile size
    //
    tileWidth       = aSize.width  / 7.0f;
    if(isIPad) {
        tileHeight      = (aSize.height - titleBarHeight) / numRows;
    }else{
        tileHeight      = 40.0f;
    }
    
    // Frame size
    //
    CGFloat aWidth  = aSize.width;
    CGFloat aHeight = numRows * tileHeight  + 1.0f;
	self.frame      = CGRectMake(0, 1.0, aWidth, aHeight);
    
    if(selectedDay) {
        [self selectDay:selectedDay];
    }
}

- (id)initWithDelegate:(NSObject<CalendarMonthTouchDelegate>*)aMonthTouchDelegate
                  size:(CGSize)aSize
                  date:(NSDate*)date
              viewType:(ECalendarViewType)aCalendarViewType
         monthViewType:(EMonthViewType)aMonthViewType
             tileImage:(UIImage*)aTileImage
                events:(NSArray*)anEvents
        titleBarHeight:(CGFloat)aTitleBarHeight
      startDayOn:(int)firstDayOfTheWeek
{
	if(!(self=[super initWithFrame:CGRectZero])) return nil;
    
    isIPad                  = IS_IPAD();

    self.monthTouchDelegte  = aMonthTouchDelegate;
    calendarViewType        = aCalendarViewType;
    monthViewType           = aMonthViewType;
    
    tileImage               = aTileImage;
    titleBarHeight          = aTitleBarHeight;
    
    CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
    self.backgroundColor = aCalendarTheme.backColor;

    // Calendar setup
    //
	self.events     = anEvents;
	monthDate       = date;
	
    Calendar* aCalendar = [Calendar calendar];
    month           = [aCalendar monthOfYear:monthDate];
    year            = [aCalendar year:monthDate];

	TKDateInformation dateInfo = [aCalendar dateInformationFromDate:monthDate];
	firstWeekdayInMonth    = dateInfo.weekday;
	
	daysInMonth     = [[aCalendar nextMonthForDate:monthDate] daysBetweenDate:monthDate];
	
    [self setTileSize:aSize];
	
    TKDateInformation todayInfo = [aCalendar dateInformationFromDate:[NSDate date]];
    
	today = dateInfo.month == todayInfo.month && dateInfo.year == todayInfo.year ? todayInfo.day : -5;
	
	[self setFirstDayOfTheWeek:firstDayOfTheWeek];
    
    // Image views
    if(!sTodayImage) {
        sTodayImage         = [self loadImage:@"Month Calendar Today Tile.png"];
        sTodaySelectedImage = [self loadImage:@"Month Calendar Today Selected Tile.png"];
        sSelectedImage      = [self loadImage:@"Month Calendar Date Tile Selected.png"];
        //self.notSelectedImage   = [self loadImage:@"Month Calendar Date Tile Gray.png"];
    }

    [self.selectedImageView addSubview:self.currentDay];
    switch(monthViewType) {
        case kMonthViewColor:
        case kMonthViewEvents:
            break;
        case kMonthViewDot:
            [self.selectedImageView addSubview:self.dot];
            break;
    }
    
    // Multitouch
    //
	self.multipleTouchEnabled = NO;
    
    if(isIPad && calendarViewType == kCalendarMonth) {
        [self buildDetailEventView:anEvents];
    }

	return self;
}

-(void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;
	
    Calendar* aCalendar = [Calendar calendar];
	NSDate* prev    = [aCalendar previousMonthForDate:monthDate];
	
	// Ranges for dates proir to first of month and after last day of month
    //
	int preDayCnt = [prev daysBetweenDate:monthDate];
	if ((firstDayOfTheWeek == 1) && (firstWeekdayInMonth > 1)) {
		firstOfPrev = preDayCnt - firstWeekdayInMonth + 2;
		lastOfPrev  = preDayCnt;
	} else if ((firstDayOfTheWeek == 2) && (firstWeekdayInMonth != 2)) {
		if(firstWeekdayInMonth == 1) {
			firstOfPrev = preDayCnt - 5;
		}else{
			firstOfPrev = preDayCnt - firstWeekdayInMonth + 3;
		}
		lastOfPrev = preDayCnt;
	} else {
		
		// need more options
		firstOfPrev = -1;
		lastOfPrev = 0;
	}
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: TKCalendarMonthView %@ %@: %@", where, [e name], [e reason]);
}

- (void)rebuild
{
    if(isIPad) {
        for (id view in self.subviews) {
            if(![NSStringFromClass([view class])isEqualToString:@"TKTimelineView"]) {
                [view removeFromSuperview];
            }
        }
        
        [self buildDetailEventView:self.events];
    }
}

- (void)buildDetailEventView:(NSArray*)anEvents
{
    CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
    UIColor* aClearColor = aCalendarTheme.backClearColor;
    
    NSUInteger aDaysCount = anEvents.count;
    if(aDaysCount > 0) {
        NSDate* aDate = [[Calendar calendar] addDays:-(lastOfPrev-firstOfPrev+1) toDate:self.monthDate];
        
        CGFloat aMaxTileHeight = tileHeight - 12;
        for(NSUInteger i = 0; i < aDaysCount; i++) {
            NSArray* anEventsForDay = [anEvents objectAtIndex:i];
            NSUInteger aCount = anEventsForDay.count;
            if(aCount > 0) {
                CGRect r            = [self rectForCellAtIndex:i];
                CGRect aRect        = r;
                CGFloat deltaY      = 0.0f;
                CGFloat aHeight     = (aMaxTileHeight) / aCount;
                if(aHeight < kEventStringHeight) {
                    aHeight = kEventStringHeight;
                }
                CGFloat aWidth      = r.size.width - 2.0f;
                aRect.origin.x      = r.origin.x + aWidth;
                aRect.size.height   = aHeight;
                aRect.size.width    = tileWidth - aWidth;
                
                NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
                
                for(NSUInteger j = 0; j < aCount; j++) {
                    aRect.origin.y = r.origin.y + deltaY;
                    Event* anEvent = [anEventsForDay objectAtIndex:j];
                    
                    NSString* aTitle = anEvent.title;
                    if(deltaY + aHeight > aMaxTileHeight) {
                        aTitle = @"...";
                    }
                    
                    TKCalendarDayEventView* anEventView;
                    anEventView = [TKCalendarDayEventView eventViewWithFrame:aRect
                                                                       event:anEvent
                                                                   startDate:[anEvent startTimeForDate:aTimelessDate]
                                                                     endDate:[anEvent endTimeForDate:aTimelessDate]
                                                                       title:aTitle
                                                                    location:anEvent.location
                                                                  eventStyle:kEventStyleText];
                    anEventView.backgroundColor = aClearColor;
                    [self addSubview:anEventView];
                    anEventView.delegate = self;
                    
                    if(deltaY + aHeight > aMaxTileHeight) {
                        break;
                    }
                    
                    deltaY += aHeight;
                }
            }
            aDate = [[Calendar calendar] addDays:1 toDate:aDate];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Draw
////////////////////////////////////////////////////////////////////////////////
#pragma mark Draw

- (CGRect)rectForCellAtIndex:(int)index
{
    CGRect aRect;
    
	int row = index / 7;
	int col = index % 7;
    
    if(calendarViewType == kCalendarMonth) {
        aRect = CGRectMake(col * tileWidth, row * tileHeight+6, kDateWidth, kDateHeight);
    }else{
        aRect = CGRectMake(col * tileWidth, row * tileHeight, tileWidth, tileHeight);
    }
    
    return aRect;
}

- (void)drawTileInRect:(CGRect)r day:(int)day events:(NSArray*)anEvents dateColor:(UIColor*)aDateColor font:(UIFont*)f1 font2:(UIFont*)f2
{
	NSString* str = [NSString stringWithFormat:@"%d",day];
	r.size.height -= 2;
    [aDateColor set];
	[str drawInRect:r withFont:f1 lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
    
    switch(monthViewType) {
        case kMonthViewColor:
            break;
        case kMonthViewEvents:
        {
#if 0
            CGRect aRect        = r;
            CGFloat deltaY      = 0.0f;
            CGFloat aWidth      = r.size.width + 2.0f;
            aRect.origin.x      = r.origin.x + aWidth;
            aRect.size.height   = kEventStringHeight;
            aRect.size.width    = tileWidth - aWidth;
            for(Event* anEvent in anEvents) {
                if([anEvent isHoliday]) {
                    [holidayColorDark set];
                }else if(anEvent.meetingJSON.length > 2) {
                    [meetingColorDark set];
                }else{
                    [eventColorDark set];
                }
                aRect.origin.y = r.origin.y + deltaY;
                [anEvent.title drawInRect:aRect withFont:f2 lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentLeft];
                deltaY += kEventStringHeight;
                if(deltaY + kEventStringHeight > tileHeight-kEventStringHeight) {
                    aRect.origin.y = r.origin.y + deltaY;
                    [@"..." drawInRect:aRect withFont:f2 lineBreakMode:UILineBreakModeTailTruncation alignment:UITextAlignmentLeft];
                    break;
                }
            }
#endif
            break;
        }
        case kMonthViewDot:
            if(anEvents.count > 0) {
                r.size.height = 10;
                r.origin.y += 18;
                [@"•" drawInRect:r withFont:f2 lineBreakMode:NSLineBreakByWordWrapping alignment:NSTextAlignmentCenter];
            }
            break;
    }
}

- (UIImage*)loadImage:(NSString*)aFileName
{
    NSString* aResourcePath = [[FXLSafeZone getResourceBundle] resourcePath];
    return [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", aResourcePath, aFileName]];
}

- (void)drawColor
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    int index = 0;
    if(firstOfPrev > 0) {
        for(int i = firstOfPrev; i<= lastOfPrev; i++) {
            index++;
        }
    }
    
    UIFont* font    = [UIFont boldSystemFontOfSize:kDateFontSize];
    UIFont* font2;
    if(monthViewType == kMonthViewEvents) {
        font2 = [UIFont systemFontOfSize:kEventFontSize];
    }else{
        font2 = [UIFont boldSystemFontOfSize:kDotFontSize];
    }
    CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
    UIColor* aHolidayColor      = aCalendarTheme.holidayColorLight;
    UIColor* aMeetingColor      = aCalendarTheme.meetingColorLight;
    UIColor* anEventColor       = aCalendarTheme.eventColorLight;
    UIColor* aDateInverseColor  = aCalendarTheme.dateInverseColor;
    UIColor* aDateColor         = aCalendarTheme.dateColor;

    [aDateColor set];

    for(int i = 1; i <= daysInMonth; i++) {
        CGRect r = [self rectForCellAtIndex:index];
        
        NSArray* anEvents = [events objectAtIndex:index];
        if(anEvents.count > 0) {
            UIColor* aColor = anEventColor;
            for(Event* anEvent in anEvents) {
                if([anEvent isHoliday]) {
                    aColor = aHolidayColor; break;
                }else if(anEvent.isMeeting) {
                    aColor = aMeetingColor; break;
                }
            }
            CGRect aFillRect;
            aFillRect.origin.x      = r.origin.x + 1.0f;
            aFillRect.origin.y      = r.origin.y + 1.0f;
            aFillRect.size.width    = tileWidth  - 2.0f;
            aFillRect.size.height   = tileHeight - 2.0f;
            
            CGContextSetFillColorWithColor(context, aColor.CGColor);
            CGContextFillRect(context, aFillRect);
        }
        
        if(today != i) {
            [self drawTileInRect:r day:i events:anEvents dateColor:aDateColor font:font font2:font2];
        }else{
            [self drawTileInRect:r day:i events:anEvents dateColor:aDateInverseColor font:font font2:font2];
        }
        index++;
    }
}

- (void)drawRect:(CGRect)rect
{
    @try {
        switch(monthViewType) {
            case kMonthViewEvents:
            case kMonthViewDot:
            {
                CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
                UIColor* aDateColor         = aCalendarTheme.dateColor;
                UIColor* aDateOffColor      = aCalendarTheme.dateOffColor;
                UIColor* aDateInverseColor  = aCalendarTheme.dateInverseColor;

                CGContextRef context = UIGraphicsGetCurrentContext();
                CGRect r = CGRectMake(0, 0, tileWidth, tileHeight);
                
                UIFont* font    = [UIFont boldSystemFontOfSize:kDateFontSize];
                UIFont* font2;
                if(monthViewType == kMonthViewEvents) {
                    font2 = [UIFont systemFontOfSize:kEventFontSize];
                }else{
                    font2 = [UIFont boldSystemFontOfSize:kDotFontSize];
                }
                
                // Month view tile background image
                //
                CGContextDrawTiledImage(context, r, tileImage.CGImage);
                
                // Highlight if tile is today
                //
                if(today > 0) {
                    int pre     = firstOfPrev < 0 ?  0 : lastOfPrev - firstOfPrev + 1;
                    int index   = today +  pre-1;
                    int row     = index / 7;
                    int column  = (index % 7);
                    [self dateHighlightForDay:today row:row column:column isSelect:NO];
                }
                
                int anEventCount = events.count;
                int index = 0;
                
                // Draw dates for days prior to the first of the month
                //
                if(calendarViewType == kCalendarMonth) {
                    if(firstOfPrev > 0) {
                        for(int i = firstOfPrev; i<= lastOfPrev; i++) {
                            r = [self rectForCellAtIndex:index];
                            if(index < anEventCount) {
                                [self drawTileInRect:r day:i events:[events objectAtIndex:index] dateColor:aDateOffColor font:font font2:font2];
                            }else{
                                FXDebugLog(kFXLogCalendar, @"TKCalendarMonthTiles drawRect pre out of range: %d %d", index, anEventCount);
                            }
                            index++;
                        }
                    }
                }else{
                    if(firstOfPrev > 0) {
                        for(int i = firstOfPrev; i<= lastOfPrev; i++) {
                            index++;
                        }
                    }
                }
                
                // Draw dates for days in this month
                //
                [aDateColor set];
                for(int i = 1; i <= daysInMonth; i++) {
                    r = [self rectForCellAtIndex:index];
                    if(index < anEventCount) {
                        if(today != i) {
                            [self drawTileInRect:r day:i events:[events objectAtIndex:index] dateColor:aDateColor font:font font2:font2];
                        }else{
                            [self drawTileInRect:r day:i events:[events objectAtIndex:index] dateColor:aDateInverseColor font:font font2:font2];
                        }
                    }else{
                        FXDebugLog(kFXLogCalendar, @"TKCalendarMonthTiles drawRect out of range: %d %d", index, anEventCount);
                    }
                    index++;
                }
                
                // Draw dates for days after the end of this month
                //
                if(calendarViewType == kCalendarMonth) {
                    int i = 1;
                    while(index % 7 != 0) {
                        r = [self rectForCellAtIndex:index] ;
                        if(index < anEventCount) {
                            [self drawTileInRect:r day:i events:[events objectAtIndex:index] dateColor:aDateOffColor font:font font2:font2];
                        }else{
                            FXDebugLog(kFXLogCalendar, @"TKCalendarMonthTiles drawRect post out of range: %d %d", index, anEventCount);
                        }
                        i++;
                        index++;
                    }
                }
                break;
            }
            case kMonthViewColor:
                [self drawColor];
                break;
        }
    }@catch (NSException* e) {
        [self _logException:@"drawRect" exception:e];
    }
}

- (UILabel*)currentDay
{
	if(currentDay == nil) {
        CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
		CGRect r = self.selectedImageView.bounds;
		r.origin.y -= 2;
		currentDay = [[UILabel alloc] initWithFrame:r];
		currentDay.text             = @"1";
		currentDay.textColor        = aCalendarTheme.dateInverseColor;
		currentDay.backgroundColor  = aCalendarTheme.backClearColor;
		currentDay.font             = [UIFont boldSystemFontOfSize:kDateFontSize];
		currentDay.textAlignment    = NSTextAlignmentCenter;
		currentDay.shadowColor      = aCalendarTheme.shadowColor;
		currentDay.shadowOffset     = CGSizeMake(0, -1);
	}else if(!isIPad) {
        CGRect aRect = currentDay.frame;
        aRect.size.height = tileHeight;
        aRect.size.width  = tileWidth;
        currentDay.frame = aRect;
    }
	return currentDay;
}

- (UILabel*)dot
{
	if(dot == nil) {
        CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
		CGRect r = self.selectedImageView.bounds;
		r.origin.y      += 29;
		r.size.height   -= 31;
		dot = [[UILabel alloc] initWithFrame:r];
		dot.text            = @"•";
		dot.textColor       = aCalendarTheme.dateInverseColor;
		dot.backgroundColor = aCalendarTheme.backClearColor;
		dot.font            = [UIFont boldSystemFontOfSize:kDotFontSize];
		dot.textAlignment   = NSTextAlignmentCenter;
		dot.shadowColor     = aCalendarTheme.shadowColor;
		dot.shadowOffset    = CGSizeMake(0, -1);
    }else if(!isIPad) {
        CGRect aRect = dot.frame;
        //aRect.size.height = tileHeight;
        aRect.size.width  = tileWidth;
        dot.frame = aRect;
    }
	return dot;
}

- (UIImageView*)selectedImageView
{
	if(selectedImageView == nil) {
		UIImage *img = [sSelectedImage stretchableImageWithLeftCapWidth:1 topCapHeight:0];
		selectedImageView       = [[UIImageView alloc] initWithImage:img];
        CGRect aRect;
        if(kDateWidth > tileWidth || kDateHeight > tileHeight) {
            aRect = CGRectMake(0, 0, tileWidth, tileHeight);
        }else{
            aRect = CGRectMake(0, 0, kDateWidth, kDateHeight);
        }
        if(isIPad) {
            aRect.size.height   -= kTimeBorderWidth;
            aRect.size.width    -= kTimeBorderWidth;
        }
        selectedImageView.frame = aRect;
	}else if(!isIPad) {
        CGRect aRect = selectedImageView.frame;
        aRect.size.height = tileHeight;
        aRect.size.width  = tileWidth;
        selectedImageView.frame = aRect;
    }
	return selectedImageView;
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (void)setTarget:(id)t action:(SEL)a
{
	target = t;
	action = a;
}

- (void)dateHighlightForDay:(int)day row:(int)row column:(int)column isSelect:(BOOL)anIsSelect
{
    if(day == today) {
        self.currentDay.shadowOffset = CGSizeMake(0, 1);
        self.dot.shadowOffset = CGSizeMake(0, 1);
        self.selectedImageView.image = sTodaySelectedImage;
        markWasOnToday = YES;
    }else if(markWasOnToday) {
        self.dot.shadowOffset = CGSizeMake(0, -1);
        self.currentDay.shadowOffset = CGSizeMake(0, -1);
        self.selectedImageView.image = [sSelectedImage stretchableImageWithLeftCapWidth:1 topCapHeight:0];
        markWasOnToday = NO;
    }

    if(day != today) {
        [self addSubview:self.selectedImageView];
    }
    self.currentDay.text = [NSString stringWithFormat:@"%d",day];

    if(monthViewType == kMonthViewDot) {
        if([events count] > 0) {
            NSUInteger aMark = row * 7 + column;
            if(aMark < [events count]) {
                if([[events objectAtIndex:aMark] count])
                    [self.selectedImageView addSubview:self.dot];
                else
                    [self.dot removeFromSuperview];
            }else{
                FXDebugLog(kFXLogActiveSync, @"Event out of bounds: %d: %d", aMark, [events count]);
            }
        }else{
            [self.dot removeFromSuperview];
        }
    }

    CGRect r = self.selectedImageView.frame;
    r.origin.x = (column * tileWidth);
    r.origin.y = (row * tileHeight);
    if(isIPad) {
        r.origin.x      += kTimeBorderWidth;
        r.origin.y      += kTimeBorderWidth;
    }else{
        r.size.height = tileHeight;
        r.size.width  = tileWidth;
    }
    self.selectedImageView.frame = r;
    
    if(day == today && !anIsSelect) {
        [sTodayImage drawInRect:r];
    }
}

- (void)selectDay:(int)day
{
    if(!day) {
        [self.selectedImageView setHidden:TRUE];
    }else{
        [self.selectedImageView setHidden:FALSE];
    }
    @try {
        int pre     = firstOfPrev < 0 ?  0 : lastOfPrev - firstOfPrev + 1;
        int tot     = day + pre;
        int row     = tot / 7;
        int column  = (tot % 7)-1;
        
        selectedDay     = day;
        selectedPortion = 1;

        [self dateHighlightForDay:day row:row column:column isSelect:YES];

    }@catch (NSException* e) {
        [self _logException:@"selectDay" exception:e];
    }
}

- (NSDate*)dateForDay:(int)aDay portion:(int)aPortion
{
	if(aDay < 1 || aPortion != 1) {
        FXDebugLog(kFXLogActiveSync, @"dateSelected date not set");
        return nil;
    }
	
    Calendar* aCalendar = [Calendar calendar];
    TKDateInformation info = [aCalendar dateInformationFromDate:monthDate];
    info.hour   = 0;
    info.minute = 0;
    info.second = 0;
    info.day    = aDay;
    NSDate* d = [aCalendar dateFromDateInformation:info];
    
    [CalendarController controller].date = d;
    
	return d;
}

- (NSDate*)dateSelected
{
    return [self dateForDay:selectedDay portion:selectedPortion];
}

////////////////////////////////////////////////////////////////////////////////
// UIResponder touch handler
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIResponder touch handler

- (void)reactToTouch:(UITouch*)touch down:(BOOL)down
{
    // Limit touch to bounds of calendar view
    //
    CGPoint p = [touch locationInView:self];
    if(p.x > self.bounds.size.width  || p.x < 0
       || p.y > self.bounds.size.height || p.y < 0) {
        return;
    }
    
    if(!down) return;
    
    @try {
        int column  = p.x / tileWidth;
        int row     = p.y / tileHeight;
        
        int day = 1, portion = 0;
        
        if(row == (int) (self.bounds.size.height / tileHeight)) row --;
        
        int fir = firstWeekdayInMonth - 1;
		
        if ((self.firstDayOfTheWeek == 2) && fir == 0) fir = 7;
        if (self.firstDayOfTheWeek == 2) fir--;
        
        if (row==0 && column < fir) {
            day = firstOfPrev + column;
        } else {
            portion = 1;
            day = row * 7 + column  - firstWeekdayInMonth+2;
            if (self.firstDayOfTheWeek == 2) day++;
            if ((self.firstDayOfTheWeek == 2) && fir==6) day -= 7;
        }
        
        if(portion > 0 && day > daysInMonth) {
            portion = 2;
            day = day - daysInMonth;
        }

        if(portion == 1) {
            NSDate* aDate = [self dateForDay:day portion:portion];
            if([self.monthTouchDelegte dateSelected:aDate touch:(UITouch*)touch]) {
                [self dateHighlightForDay:day row:row column:column isSelect:YES];
                self.selectedDate   = aDate;
                selectedDay         = day;
                selectedPortion     = portion;
            }
        }else if(down) {
            [self.monthTouchDelegte dayTapped:day changeMonthInDirection:portion touch:touch];
        }
    }@catch (NSException* e) {
        [self _logException:@"reactToTouch" exception:e];
    }
}

- (int)dayForPoint:(CGPoint)aPoint portion:(int*)aPortion
{
    if(aPoint.x > self.bounds.size.width  || aPoint.x < 0
       || aPoint.y > self.bounds.size.height || aPoint.y < 0) {
        *aPortion = -1;
        return 0;
    }
    
    int column  = aPoint.x / tileWidth;
    int row     = aPoint.y / tileHeight;
    
    int day = 1, portion = 0;
    
    if(row == (int) (self.bounds.size.height / tileHeight)) row --;
    
    int fir = firstWeekdayInMonth - 1;
    if ((self.firstDayOfTheWeek == 2) && fir == 0) fir = 7;
    if (self.firstDayOfTheWeek == 2) fir--;
    
    if (row==0 && column < fir) {
        day = firstOfPrev + column;
    } else {
        portion = 1;
        day = row * 7 + column  - firstWeekdayInMonth+2;
        if (self.firstDayOfTheWeek == 2) day++;
        if ((self.firstDayOfTheWeek == 2) && fir==6) day -= 7;
    }
    if(portion > 0 && day > daysInMonth) {
        portion = 2;
        day = day - daysInMonth;
    }
    
    *aPortion = portion;
    
    return day;
}

////////////////////////////////////////////////////////////////////////////////
// UIResponder overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIResponder overrides

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.selectedDate = nil;
	[self reactToTouch:[touches anyObject] down:NO];
}

- (NSDate*)dateForPoint:(CGPoint)aPoint
{
    int aPortion = -1;
    int aDay = [self dayForPoint:aPoint portion:&aPortion];
    
    Calendar* aCalendar = [Calendar calendar];
    NSDate* aMonthDate;
    switch(aPortion) {
        case 0:
            aMonthDate = [aCalendar previousMonthForDate:monthDate];
            break;
        case 1:
            aMonthDate = monthDate;
            break;
        case 2:
            aMonthDate = [aCalendar nextMonthForDate:monthDate];
            break;
    }
    TKDateInformation aDateInfo = [aCalendar dateInformationFromDate:aMonthDate];
    aDateInfo.hour   = 0;
    aDateInfo.minute = 0;
    aDateInfo.second = 0;
    aDateInfo.day    = aDay;
    NSDate* aDate = [aCalendar dateFromDateInformation:aDateInfo];
    return aDate;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self reactToTouch:[touches anyObject] down:NO];
    NSArray* aTouches = [touches allObjects];
    UITouch* aTouch = [aTouches lastObject];
    CGPoint aPoint = [aTouch locationInView:self];
    NSDate* aDate = [self dateForPoint:aPoint];
    if(![aDate isEqualToDate:self.selectedDate]) {
        [self.monthTouchDelegte dateEntered:aDate location:aPoint];
        self.selectedDate = aDate;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self reactToTouch:[touches anyObject] down:YES];
}

////////////////////////////////////////////////////////////////////////////////
// TapDetectingViewDelegate (TKCalendarDayEventView touches)
////////////////////////////////////////////////////////////////////////////////
#pragma mark TapDetectingViewDelegate (TKCalendarDayEventView touches)6969mOrdor

- (void)tapDetectingView:(TapDetectingView *)aTapDetectingView touchMovedToView:(UIView *)aView location:(CGPoint)aTapPoint
{
    if([aView isKindOfClass:[TKCalendarDayEventView class]]) {
        [self.monthTouchDelegte eventViewEntered:(TKCalendarDayEventView*)aView location:aTapPoint];
    }else{
        [self.monthTouchDelegte eventViewEntered:nil location:aTapPoint];
    }
}

- (void)tapDetectingView:(TapDetectingView *)aView gotSingleTapAtPoint:(CGPoint)aTapPoint
{
    if([aView isKindOfClass:[TKCalendarDayEventView class]]) {
        [self.monthTouchDelegte eventViewTapped:(TKCalendarDayEventView*)aView location:aTapPoint];
    }else{
    }
}

@end

