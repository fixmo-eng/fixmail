/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecurrenceEditDelegate.h"

@class Event;

@interface RecurrenceTypeSheet : NSObject<UIActionSheetDelegate>
{
    NSObject<RecurrenceEditDelegate>*   delegate;
    Event*                              event;
    UITableView*                        tableView;
    NSIndexPath*                        indexPath;
}

// Properties
@property (nonatomic, strong) NSObject<RecurrenceEditDelegate>* delegate;
@property (nonatomic, strong) Event*                            event;
@property (nonatomic, strong) UITableView*                      tableView;
@property (nonatomic, strong) NSIndexPath*                      indexPath;

// Construct
- (id)initWithView:(UIView*)aView
     navController:(UINavigationController*)aNavController
             event:(Event*)anEvent
         tableView:(UITableView*)aTableView
         indexPath:(NSIndexPath*)anIndexPath;

@end