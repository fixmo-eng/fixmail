/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "TKTimelineAllDayView.h"

@implementation TKTimelineAllDayView

@synthesize numDays;
@synthesize dayWidth;
@synthesize offsetX;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when xibless (interface buildder)
- (id)initWithFrame:(CGRect)frame
{
    if(!(self=[super initWithFrame:frame])) return nil;
    return self;
}

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when using xib (interface buildder)
- (id)initWithCoder:(NSCoder *)decoder
{
    if(!(self=[super initWithCoder:decoder])) return nil;
	return self;
}


////////////////////////////////////////////////////////////////////////////////
// Drawing
////////////////////////////////////////////////////////////////////////////////
#pragma mark Drawing

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // vertical lines if week view
    //
    if(numDays > 1) {
        CGFloat x = self.offsetX;
        CGFloat y = self.bounds.size.height;
        for(int i = 0 ; i < numDays ; ++i) {
            CGContextBeginPath(context);
            CGContextMoveToPoint(context,    x, 0.0f);
            CGContextAddLineToPoint(context, x, y);
            CGContextStrokePath(context);
            x += dayWidth;
        }
        CGContextBeginPath(context);
        CGContextMoveToPoint(context,    0.0f, y);
        CGContextAddLineToPoint(context, self.bounds.size.width, y);
        CGContextStrokePath(context);
    }
}

@end