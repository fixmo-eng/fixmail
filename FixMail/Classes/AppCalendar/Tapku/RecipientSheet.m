/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecipientSheet.h"
#import "Event.h"

@implementation RecipientSheet

@synthesize delegate;
@synthesize attendee;

// Action Sheet types
typedef enum
{
    kRecipientDelete            = 0,
    kRecipientEmail             = 1,
    kRecipientInvite            = 2
} ERecipientActions;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView
          delegate:(NSObject<RecipientDelegate>*)aDelegate
          attendee:(Attendee *)anAttendee
{
    if (self = [super init]) {
        self.delegate   = aDelegate;
        self.attendee   = anAttendee;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:FXLLocalizedStringFromTable(@"DELETE", @"RecipientSheet", @"Option to delete the contact from the event when click on contact while creating or editing an event")
                                           otherButtonTitles:
                                                FXLLocalizedStringFromTable(@"EMAIL", @"RecipientSheet", @"Option to email contact when click recipient while creating or editing an event"),
                                                FXLLocalizedStringFromTable(@"SEND_INVITATION", @"RecipientSheet", @"Option to send invitation to contact when click on contact while creating or editing an event"), nil];
        [anActionSheet showInView:aView];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	switch((ERecipientActions)buttonIndex) {
        case kRecipientDelete:
            [delegate recipientDelete:attendee];
            break;
        case kRecipientEmail:
            [delegate recipientEmail:attendee];
            break;
        case kRecipientInvite:
            [delegate recipientInvite:attendee];
            break;
        default:
            break;
    }
}

@end
