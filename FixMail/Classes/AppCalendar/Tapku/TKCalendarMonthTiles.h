/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "Calendar.h"
#import "CalendarMonthTouchDelegate.h"
#import "TKCalendarMonthView.h"
#import "TapDetectingView.h"

@interface TKCalendarMonthTiles : UIView <TapDetectingViewDelegate>
{
    NSObject<CalendarMonthTouchDelegate>*  monthTouchDelegte;
    
    ECalendarViewType   calendarViewType;
    EMonthViewType      monthViewType;
    
    NSDate*         monthDate;
    NSInteger       month;
    NSInteger       year;
    
	id              target;
	SEL             action;
	
	int             firstOfPrev;
    int             lastOfPrev;
	NSArray*        events;
	int             today;
	BOOL            markWasOnToday;
	
    NSDate*         selectedDate;
	int             selectedDay;
    int             selectedPortion;
	
	int             firstWeekdayInMonth;
    int             daysInMonth;
    BOOL            isIPad;
    
    CGFloat         tileWidth;
    CGFloat         tileHeight;
    CGFloat         titleBarHeight;
    
    UIImage*        tileImage;
	UILabel*        dot;
	UILabel*        currentDay;
    
	UIImageView*    selectedImageView;
}

@property (nonatomic,strong) NSObject<CalendarMonthTouchDelegate>*  monthTouchDelegte;

@property (nonatomic,strong) NSDate*    monthDate;
@property (nonatomic,strong) NSDate*    selectedDate;

@property (nonatomic) NSInteger         year;
@property (nonatomic) NSInteger         month;

@property (nonatomic,strong) NSArray*   events;

@property (nonatomic) CGFloat           tileWidth;
@property (nonatomic) CGFloat           tileHeight;
@property (nonatomic) int firstDayOfTheWeek;


+ (NSArray*)rangeOfDatesInMonthGrid:(NSDate*)date startOn:(int)firstDayOfTheWeek;

- (void)setTileSize:(CGSize)aSize;

- (id)initWithDelegate:(NSObject<CalendarMonthTouchDelegate>*)aMonthTouchDelegate
                  size:(CGSize)aSize
                  date:(NSDate*)date
              viewType:(ECalendarViewType)aCalendarViewType
         monthViewType:(EMonthViewType)aMonthViewType
             tileImage:(UIImage*)aTileImage
                events:(NSArray*)anEvents
        titleBarHeight:(CGFloat)aTitleBarHeight
      startDayOn:(int)firstDayOfTheWeek;

- (void)setTarget:(id)target action:(SEL)action;
- (void)selectDay:(int)day;
- (NSDate*)dateSelected;

- (void)rebuild;

@end
