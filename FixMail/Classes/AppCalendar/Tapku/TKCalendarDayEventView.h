//
//  ODCalendarDayEventView.h
//  Created by Devin Ross on 7/28/09.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import <QuartzCore/QuartzCore.h>
#import "TapDetectingView.h"

@class Event;

typedef enum
{
    kEventStyleBalloon  = 0,
    kEventStyleText     = 1,
} EEventStyle;

@interface TKCalendarDayEventView : TapDetectingView {
    Event*          _event;
    EEventStyle     _eventStyle;
    
	NSDate*         _startDate;
	NSDate*         _endDate;
	NSString*       _title;
	NSString*       _location;
    
	UIColor*        balloonColorTop;
	UIColor*        balloonColorBottom;
	UIColor*        textColor;
    
    CGGradientRef   gradient;
}

@property (nonatomic, strong) Event*    event;
@property (nonatomic) EEventStyle       eventStyle;
@property (nonatomic, copy) NSDate*     startDate;
@property (nonatomic, copy) NSDate*     endDate;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) UIColor*  balloonColorTop;
@property (nonatomic, strong) UIColor*  balloonColorBottom;
@property (nonatomic, strong) UIColor*  textColor;

+ (id)eventViewWithFrame:(CGRect)frame
                   event:(Event*)anEvent
               startDate:(NSDate *)startDate
                 endDate:(NSDate *)endDate
                   title:(NSString *)title
                location:(NSString *)location
              eventStyle:(EEventStyle)anEventStyle;

- (void)setGradient;

@end
