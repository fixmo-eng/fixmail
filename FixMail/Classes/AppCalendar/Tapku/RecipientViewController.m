/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecipientViewController.h"
#import "EmailAddress.h"
#import "SearchRunner.h"

@implementation RecipientViewController

// Properties
@synthesize searchResults;
@synthesize delegate;
@synthesize searchText;
@synthesize emailAddress;

// Constants
static const int kMinRows       = 5;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

#if 0
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super FXinitWithNibName:nibNameOrNil];
    if (self) {
    }
    return self;
}
#endif

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: RecipientViewController %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Internal
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal

- (EmailAddress*)_emailAddressForRow:(int)aRow
{
    EmailAddress* anEmailAddress = nil;
    
    int anOffset = kMinRows - self.searchResults.count;
    if(self.searchResults.count < kMinRows) {
        aRow -= anOffset;
    }
    if(aRow >= 0) {
        anEmailAddress = [self.searchResults objectAtIndex:aRow];
    }
    
    return anEmailAddress;
}

- (void)_recipientNoMatch
{
    self.searchResults = nil;
    [delegate recipientNoMatch];
}

- (void)_deliverAutocompleteResult:(NSArray*)aResults
{    
    NSMutableArray* aResultArray = [NSMutableArray arrayWithCapacity:[aResults count]];
    
    @try {
        for(int i = aResults.count-1 ; i >= 0 ; --i) {
            NSDictionary* aResult = [aResults objectAtIndex:i];
            NSString* aName = [aResult objectForKey:@"name"];
            if([aName length] > 0) {
            }else{
                aName = @"";
            }
            NSString* anAddressStrings = [aResult objectForKey:@"emailAddresses"];
            NSArray* anAddressArray = [anAddressStrings componentsSeparatedByString:@","];
            for(__strong NSString* anAddressString in anAddressArray) {
#warning FIXME String cleaning
                // this is awful, clean these strings before they go in to contact database and deliver them properly formatted
                // from SearchRunner
                anAddressString = [anAddressString stringByReplacingOccurrencesOfString:@"'" withString:@""];
                anAddressString = [anAddressString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//                anAddressString = [NSString stringWithFormat:@"<%@>", anAddressString];
                EmailAddress* anEmailAdress = [[EmailAddress alloc] initWithName:aName address:anAddressString];
                [aResultArray addObject:anEmailAdress]; 
            }
        }
    }@catch (NSException* e) {
        [self _logException:@"_deliverAutocompleteResult" exception:e];
    }
    if(aResultArray.count > 0) {
        self.searchResults = aResultArray;
        [self.tableView reloadData];
        [delegate recipientShow];
    }else{
        NSString* aSearchText = self.searchText;
        NSRange anAtRange = [aSearchText rangeOfString:@"@"];
        if(anAtRange.location != NSNotFound && anAtRange.location < [aSearchText length]-1) {
            if(!self.emailAddress) {
                self.emailAddress = [[EmailAddress alloc] initWithName:@"" address:aSearchText];
            }else{
                self.emailAddress.address = aSearchText;
            }
            self.searchResults = [NSArray arrayWithObject:self.emailAddress];
            [self.tableView reloadData];
            [delegate recipientShow];
        }else{
            [self _recipientNoMatch];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Engine
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Engine

- (void)deliverAutocompleteResult:(NSArray*)aResults
{
    [self performSelectorOnMainThread:@selector(_deliverAutocompleteResult:) withObject:aResults waitUntilDone:FALSE];
}

- (void)search:(NSString*)aText
{
    //[self cancel];
    
    @try {
        self.searchText = aText;
        if (aText.length > 0) {
            // Start a search against the contacts database
            //
            [[SearchRunner getSingleton] autocomplete:[NSString stringWithFormat:@"%@*", aText] withDelegate:self];
        } else {
            [self _recipientNoMatch];
        }
    }@catch (NSException* e) {
        [self _logException:@"search" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [delegate recipientViewDismissed];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int aNumberOfRows = self.searchResults.count;
    if(!aNumberOfRows) {
    }else if(aNumberOfRows < kMinRows) {
        aNumberOfRows = kMinRows;
    }
    return aNumberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0 && self.searchResults.count > 0) {
        EmailAddress* anEmailAddress = [self _emailAddressForRow:indexPath.row];
        if(anEmailAddress) {
            if(anEmailAddress.name.length > 0) {
                cell.textLabel.text         = anEmailAddress.name;
                cell.detailTextLabel.text   = anEmailAddress.address;
            }else{
                cell.textLabel.text         = anEmailAddress.address;
            }
            cell.imageView.image        = nil;
        }else{
            cell.textLabel.text         = @"";
            cell.detailTextLabel.text   = @"";
        }
	}else{
        cell.textLabel.text         = @"";
        cell.detailTextLabel.text   = @"";
    }
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EmailAddress* anEmailAddress = [self _emailAddressForRow:indexPath.row];
    if(anEmailAddress) {
        [delegate recipientSelected:anEmailAddress];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}

@end
