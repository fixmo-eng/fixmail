//
//  TKCalendarMonthView.h
//  Created by Devin Ross on 6/10/10.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "CalendarController.h"
#import "CalendarMonthTouchDelegate.h"

@class ASCalendarFolder;
@class TKCalendarMonthTiles;
@protocol TKCalendarMonthViewDelegate, TKCalendarMonthViewDataSource;
@class TKCalendarYearViewController;

typedef enum
{
    kMonthViewDot       = 0,
    kMonthViewEvents    = 1,
    kMonthViewColor     = 2,
} EMonthViewType;

/** `TKCalendarMonthView` imitates the month grid in the Calendar app on iPhone. */
@interface TKCalendarMonthView : UIView <CalendarMonthTouchDelegate> {
	TKCalendarMonthTiles*   currentTile;
    TKCalendarMonthTiles*   oldTile;
    
    NSArray*                eventsArray;    // Array with an array of events for each day in the month
    
    ECalendarViewType       viewType;       // Month or year view
    EMonthViewType          monthViewType;  
    
    EScrollDirection        scrollDirection;
    
    NSLock*                 lock;
    
    CGFloat                 tileWidth;
    CGFloat                 tileHeight;
    
    UIImage*                tileImage;
	UIButton*               leftArrow;
    UIButton*               rightArrow;
	UIImageView*            topBackground;
    UIImageView*            shadow;
	UILabel*                monthYear;
	UIScrollView*           tileBox;
	
    BOOL                    isIPad;
}


- (void)setTileSize:(CGSize)aSize;

/** Initialize a Calendar Month Grid.
 @param sunday Flag to setup the grid with Monday or Sunday as the leftmost day.
 @return A `TKCalendarMonthView` object or nil.
 */

- (id)initWithSize:(CGSize)aSize
            origin:(CGPoint)anOrigin
              date:(NSDate*)aDate
          viewType:(ECalendarViewType)aViewType
     monthViewType:(EMonthViewType)aMonthViewType
         tileImage:(UIImage*)aTileImage
     firstDayOfTheWeek:(int)firstDayOfTheWeek; // Sunday = 1, Monday = 2, ... , Saturday = 7

@property (nonatomic,weak) id <TKCalendarMonthViewDelegate>   delegate;
@property (nonatomic,weak) id <TKCalendarMonthViewDataSource> dataSource;
@property (nonatomic,strong) NSArray*                           eventsArray;
@property (nonatomic) EScrollDirection                          scrollDirection;
@property (nonatomic) int firstDayOfTheWeek;

/** The current date highlighted on the month grid.
 @return An `NSDate` object set to the month, year and day of the current selection.
 */
- (NSDate*)dateSelected;

/** The current month date being displayed. 
 @return An `NSDate` object set to the month and year of the current month grid.
 */
- (NSDate*)monthDate;
- (NSInteger)month;
- (NSInteger)year;

/** Selects a specific date in the month grid. 
 @param date The date that will be highlighed.
 */
- (void)selectDate:(NSDate*)date;

- (void)prev;
- (void)next;

- (void)rebuild;

/** Reloads the current month grid. */
- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents;

- (void)changeToMonthForDate:(NSDate*)aDate;

- (void)adjustFrame;

@end

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDelegate

/** The delegate of a `TKCalendarMonthView` object must adopt the `TKCalendarMonthViewDelegate` protocol. */ 
@protocol TKCalendarMonthViewDelegate <NSObject>
@optional

/** The highlighed date changed.
 @param monthView The calendar month view.
 @param date The highlighted date.
 */ 
- (BOOL)calendarMonthView:(TKCalendarMonthView*)monthView dateSelected:(NSDate*)date touch:(UITouch*)aTouch;

- (void)calendarMonthView:(TKCalendarMonthView*)monthView dateEntered:(NSDate*)date location:(CGPoint)aLocation;

- (void)calendarMonthView:(TKCalendarMonthView*)monthView eventViewEntered:(TKCalendarDayEventView*)anEventview location:(CGPoint)aLocation;

- (void)calendarMonthView:(TKCalendarMonthView*)monthView eventViewSelected:(TKCalendarDayEventView*)anEventview location:(CGPoint)aLocation;


/** The calendar should change the current month to grid shown.
 @param monthView The calendar month view.
 @param month The month date.
 @param animated Animation flag
 @return YES if the month should change. NO otherwise
 */ 
- (BOOL)calendarMonthView:(TKCalendarMonthView*)monthView monthShouldChange:(NSDate*)month animated:(BOOL)animated;

/** The calendar will change the current month to grid shown.
 @param monthView The calendar month view.
 @param month The month date.
 @param animated Animation flag
 */ 
- (void)calendarMonthView:(TKCalendarMonthView*)monthView monthWillChange:(NSDate*)month animated:(BOOL)animated;

/** The calendar did change the current month to grid shown.
 @param monthView The calendar month view.
 @param month The month date.
 @param animated Animation flag
 */ 
- (void)calendarMonthView:(TKCalendarMonthView*)monthView monthDidChange:(NSDate*)month animated:(BOOL)animated;
@end


////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDataSource

/** The data source of a `TKCalendarMonthView` object must adopt the `TKCalendarMonthViewDataSource` protocol. */ 
@protocol TKCalendarMonthViewDataSource <NSObject>

/** A data source that will correspond to events for the calendar month grid for a particular month.
 
 @param monthView The calendar month grid.
 @param startDate The first date shown by the calendar month grid.
 @param lastDate The last date shown by the calendar month grid.
 @return Returns an array of Event objects corresponding to the number of days specified in the start and last day parameters.
 
 */
- (NSArray*) calendarMonthView:(TKCalendarMonthView*)monthView eventsFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate;

@end