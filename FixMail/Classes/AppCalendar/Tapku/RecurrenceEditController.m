/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "RecurrenceEditController.h"
#import "Calendar.h"
#import "DatePickerController.h"
#import "DatePickerControllerIPhone.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventRecurrence.h"
#import "NumberPickerController.h"
#import "RecurrenceTypeSheet.h"

// Per MS-ASCAL
//
//  DayOfWeek can apply to:
//          kRecurrenceDaily            (Not sure how this works)
//          kRecurrenceWeekly
//          kRecurrenceMonthlyOnNthDay
//          kRecurrenceYearlyOnNthDay
//  DayOfMonth MUST be provided for:    (though I think the startDate provides this so maybe not a MUST)
//          kRecurrenceMonthly
//          kRecurrenceYearly
//  WeekOfMonthc MUST be provided for:
//          kRecurrenceMonthlyOnNthDay
//          kRecurrenceYearlyOnNthDay
//  MonthOfYear MUST be provided for:   (though I think the startDate provides this so maybe not a MUST)
//          kRecurrenceYearly
//          kRecurrenceYearlyOnNthDay

@implementation RecurrenceEditController

// Properties
@synthesize event;
@synthesize dayOfWeekButtons;
@synthesize weekOfMonthButtons;
@synthesize numberPickerController;
@synthesize recurrenceTypeSheet;

// Types
//
typedef enum
{
    kSectionRecurrenceType      = 0,
    kSectionInterval            = 1,
    kSectionRepeatOn            = 2,
    kSectionStarting            = 3,
    kSectionUntil               = 4,
    kSectionLast                = 5
} ERecurrenceSection;


typedef enum
{
    kUntilForever               = 0,
    kUntilOccurences            = 1,
    kUntilDate                  = 2,
    kUntilLast                  = 3
} ERecurrenceUntil;

// Constants
static NSString* kDayOfWeekCell = @"DayOfWeekCell";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewWillAppear:(BOOL)animated
{
    if(isPush) {
        [self.tableView reloadData];
        isPush = FALSE;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_RECURRENCE_EXCEPTION", @"ErrorAlert", @"RecurrenceEditController error alert view message when exception occured") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CALENDAR_RECURRENCE", @"ErrorAlert", @"RecurrenceEditController error alert view title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////
// RecurrenceEditDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark RecurrenceEditDelegate

- (void)setRecurrenceType:(ERecurrenceType)aRecurrenceType
{
    if(self.event.recurrence && aRecurrenceType == self.event.recurrence.recurrenceType) {
        return;
    }
    
    if(aRecurrenceType == kRecurrenceNone) {
        self.event.recurrence = nil;
    }else{
        if(!self.event.recurrence) {
            self.event.recurrence = [[EventRecurrence alloc] init];
        }
        event.recurrence.recurrenceType = aRecurrenceType;
        if(event.recurrence.interval == 0) {
            event.recurrence.interval = 1;
        }
        
        [self setRecurrenceOptionsForDate:event.startDate];
    }
    [self.tableView reloadData];
}

- (void)_redrawUntilSection
{
    NSArray* anArray = [NSArray arrayWithObjects:
                        [NSIndexPath indexPathForRow:kUntilForever      inSection:kSectionUntil],
                        [NSIndexPath indexPathForRow:kUntilOccurences   inSection:kSectionUntil],
                        [NSIndexPath indexPathForRow:kUntilDate         inSection:kSectionUntil],
                        nil];
    [self.tableView reloadRowsAtIndexPaths:anArray withRowAnimation:UITableViewRowAnimationAutomatic];
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (void)changeInterval:(NSNumber*)aNumber
{
    NSInteger aValue = [aNumber intValue];
    self.event.recurrence.interval = aValue;
    NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionInterval];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)changeStartDate:(NSDate*)aDate
{
    self.event.startDate = aDate;
    self.event.endDate   = [self.event endDateForStartDate:aDate];
    NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:0 inSection:kSectionStarting];
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)changeUntilDate:(NSDate*)aDate
{
    self.event.recurrence.untilDate = aDate;
    self.event.recurrence.occurences = 0;

    [self _redrawUntilSection];
}

- (void)changeUntilOccurrences:(NSNumber*)aNumber
{
    NSInteger aValue = [aNumber intValue];
    self.event.recurrence.occurences = aValue;
    self.event.recurrence.untilDate = nil;
    [self _redrawUntilSection];
}

- (void)doHighlight:(UIButton*)aButton
{
    [aButton setHighlighted:TRUE];
}

- (void)doUnhighlight:(UIButton*)aButton
{
    [aButton setHighlighted:FALSE];
}

////////////////////////////////////////////////////////////////////////////////
// Recurrence Options
////////////////////////////////////////////////////////////////////////////////
#pragma mark Recurrence Options

- (void)setRecurrenceOptionsForDate:(NSDate*)aDate
{
    EventRecurrence* aRecurrence = event.recurrence;
    if(aRecurrence) {
        if(!aRecurrence.startTimelessDate) {
            aRecurrence.startTimelessDate = aDate;
        }
        
        switch(event.recurrence.recurrenceType) {
            case kRecurrenceDaily:
            case kRecurrenceNone:
                aRecurrence.dayOfWeek   = 0;
                aRecurrence.dayOfMonth  = 0;
                aRecurrence.weekOfMonth = 0;
                aRecurrence.monthOfYear = 0;
                break;
            case kRecurrenceWeekly:
                aRecurrence.dayOfWeek   = [[Calendar calendar] dayOfWeek:event.startDate];
                aRecurrence.dayOfMonth  = 0;
                aRecurrence.weekOfMonth = 0;
                aRecurrence.monthOfYear = 0;
                break;
            case kRecurrenceMonthly:
            case kRecurrenceYearly:
                aRecurrence.dayOfWeek   = 0;
                aRecurrence.dayOfMonth  = 0;
                aRecurrence.weekOfMonth = 0;
                aRecurrence.monthOfYear = 0;
                break;
            case kRecurrenceMonthlyOnNthDay:
                aRecurrence.dayOfWeek   = [[Calendar calendar] dayOfWeek:event.startDate];
                aRecurrence.dayOfMonth  = 0;
                aRecurrence.weekOfMonth = [[Calendar calendar] weekOfMonth:event.startDate];
                aRecurrence.monthOfYear = 0;
                break;
            case kRecurrenceYearlyOnNthDay:
                aRecurrence.dayOfWeek   = [[Calendar calendar] dayOfWeek:event.startDate];
                aRecurrence.dayOfMonth  = 0;
                aRecurrence.weekOfMonth = [[Calendar calendar] weekOfMonth:event.startDate];
                aRecurrence.monthOfYear = [[Calendar calendar] monthOfYear:event.startDate];
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Day of week
////////////////////////////////////////////////////////////////////////////////
#pragma mark Day of week

- (BOOL)toggleDayOfWeek:(EDayOfWeek)aDayOfWeek
{
    BOOL doHighlight = FALSE;

    EventRecurrence* aRecurrence = self.event.recurrence;
    if(self.event.isEditable) {
        if((aRecurrence.dayOfWeek & aDayOfWeek) != 0) {
            aRecurrence.dayOfWeek &= ~aDayOfWeek;
        }else{
            aRecurrence.dayOfWeek |= aDayOfWeek;
            doHighlight = TRUE;
        }
    }else{
        doHighlight = (aRecurrence.dayOfWeek & aDayOfWeek);
    }
    
    return doHighlight;
}

- (void)toggleDayOfWeekPressed:(UIButton*)aButton
{
    BOOL doHighlight = FALSE;
    
    if(aButton == [dayOfWeekButtons objectAtIndex:0]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekMonday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:1]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekTuesday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:2]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekWednesday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:3]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekThursday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:4]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekFriday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:5]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekSaturday];
    }else if(aButton == [dayOfWeekButtons objectAtIndex:6]) {
        doHighlight = [self toggleDayOfWeek:kDayOfWeekSunday];
    }
    if(doHighlight) {
        [self performSelector:@selector(doHighlight:) withObject:aButton afterDelay:0];
    }else{
        [self performSelector:@selector(doUnhighlight:) withObject:aButton afterDelay:0];
    }
}

- (BOOL)setDayOfWeek:(EDayOfWeek)aDayOfWeek
{
    BOOL doHighlight = FALSE;
    
    EventRecurrence* aRecurrence = self.event.recurrence;
    if((aRecurrence.dayOfWeek & aDayOfWeek) != 0) {
        doHighlight = TRUE;
    }else{
    }
    
    return doHighlight;
}

- (void)setDayOfWeekPressed:(int)anIndex button:(UIButton*)aButton
{
    BOOL doHighlight = FALSE;
    
    switch(anIndex) {
        case 0:
            doHighlight = [self setDayOfWeek:kDayOfWeekMonday]; break;
        case 1:
            doHighlight = [self setDayOfWeek:kDayOfWeekTuesday]; break;
        case 2:
            doHighlight = [self setDayOfWeek:kDayOfWeekWednesday]; break;
        case 3:
            doHighlight = [self setDayOfWeek:kDayOfWeekThursday]; break;
        case 4:
            doHighlight = [self setDayOfWeek:kDayOfWeekFriday]; break;
        case 5:
            doHighlight = [self setDayOfWeek:kDayOfWeekSaturday]; break;
        case 6:
            doHighlight = [self setDayOfWeek:kDayOfWeekSunday]; break;
    }
    if(doHighlight) {
        [self performSelector:@selector(doHighlight:) withObject:aButton afterDelay:0];
    }else{
        [self performSelector:@selector(doUnhighlight:) withObject:aButton afterDelay:0];
    }
}

- (UITableViewCell*)dayOfWeekControl:(NSArray*)anItems
{
    UITableViewCell* aCell = [self.tableView dequeueReusableCellWithIdentifier:kDayOfWeekCell];
    if(aCell == nil) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kDayOfWeekCell];
        
        CGRect aRect = [aCell contentView].frame;
        UIControl* aControl = [[UIControl alloc] initWithFrame:aRect];
        aControl.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
        aControl.autoresizesSubviews      = YES;
        
        if(!self.dayOfWeekButtons) {
            self.dayOfWeekButtons = [NSMutableArray arrayWithCapacity:7];
        }else{
            [self.dayOfWeekButtons removeAllObjects];
        }
        aRect.size.width = aRect.size.width / anItems.count;
        for(NSString* aString in anItems) {
            UIButton* aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            aButton.frame = aRect;
            aButton.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
            aButton.autoresizesSubviews      = YES;
            
            [aButton setTitle:aString forState:UIControlStateNormal];
            [aControl addSubview:aButton];
            aRect.origin.x += aRect.size.width;
            
            [dayOfWeekButtons addObject:aButton];
            [aButton addTarget:self action:@selector(toggleDayOfWeekPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        [[aCell contentView] addSubview:aControl];
    }
    
    int i = 0;
    for(UIButton* aButton in dayOfWeekButtons) {
        [self setDayOfWeekPressed:i button:aButton];
        i++;
    }
    
    return aCell;
}

- (UITableViewCell*)createDayOfWeekInCell
{
    NSArray* anItems = [NSArray arrayWithObjects:FXLLocalizedStringFromTable(@"M", @"RecurrenceEditController", @"First letter of day of the week - Monday"),
												FXLLocalizedStringFromTable(@"T", @"RecurrenceEditController", @"First letter of day of the week - Tuesday"),
												FXLLocalizedStringFromTable(@"W", @"RecurrenceEditController", @"First letter of day of the week - Wednesday"),
												FXLLocalizedStringFromTable(@"T2", @"RecurrenceEditController", @"First letter of day of the week - Thursday"),
												FXLLocalizedStringFromTable(@"F", @"RecurrenceEditController", @"First letter of day of the week - Friday"),
												FXLLocalizedStringFromTable(@"S", @"RecurrenceEditController", @"First letter of day of the week - Saturday"),
												FXLLocalizedStringFromTable(@"S2", @"RecurrenceEditController", @"First letter of day of the week - Sunday"), nil];
    return [self dayOfWeekControl:anItems];
}

////////////////////////////////////////////////////////////////////////////////
// Week of month
////////////////////////////////////////////////////////////////////////////////
#pragma mark Week of month

- (void)toggleWeekOfMonthPressed:(UIButton*)aPressedButton
{
    if(self.event.isEditable) {
        NSUInteger aCount = weekOfMonthButtons.count;
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            UIButton* aButton = [self.weekOfMonthButtons objectAtIndex:i];
            if(aButton == aPressedButton) {
                EventRecurrence* aRecurrence = self.event.recurrence;
                aRecurrence.weekOfMonth = i+1;
                [self performSelector:@selector(doHighlight:) withObject:aButton afterDelay:0];
            }else{
                [self performSelector:@selector(doUnhighlight:) withObject:aButton afterDelay:0];
            }
        }
    }else{
        NSUInteger aCount = weekOfMonthButtons.count;
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            UIButton* aButton = [self.weekOfMonthButtons objectAtIndex:i];
            if(aButton == aPressedButton) {
                if(i == self.event.recurrence.weekOfMonth) {
                    [self performSelector:@selector(doHighlight:) withObject:aButton afterDelay:0];
                }else{
                    [self performSelector:@selector(doUnhighlight:) withObject:aButton afterDelay:0];
                }
            }
        }
    }
}

- (void)setWeekOfMonthPressed
{
    EventRecurrence* aRecurrence = self.event.recurrence;

    NSUInteger aCount = weekOfMonthButtons.count;
    for(NSUInteger i = 0 ; i < aCount ; ++i) {
        UIButton* aButton = [self.weekOfMonthButtons objectAtIndex:i];
        if(i == aRecurrence.weekOfMonth) {
            [self performSelector:@selector(doHighlight:) withObject:aButton afterDelay:0];
        }else{
            [self performSelector:@selector(doUnhighlight:) withObject:aButton afterDelay:0];
        }
    }
}

- (UIControl*)weekOfMonthControl:(UITableViewCell*)cell items:(NSArray*)anItems
{
    CGRect aRect = [cell contentView].frame;
    UIControl* aControl = [[UIControl alloc] initWithFrame:aRect];
    aControl.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin;
    aControl.autoresizesSubviews      = YES;
    
    if(!self.weekOfMonthButtons) {
        self.weekOfMonthButtons = [NSMutableArray arrayWithCapacity:5];
    }else{
        [self.weekOfMonthButtons removeAllObjects];
    }
    aRect.size.width = aRect.size.width / anItems.count;
    for(NSString* aString in anItems) {
        UIButton* aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        aButton.frame = aRect;
        aButton.autoresizingMask         = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        aButton.autoresizesSubviews      = YES;

        
        [aButton setTitle:aString forState:UIControlStateNormal];
        [aControl addSubview:aButton];
        aRect.origin.x += aRect.size.width;
        
        [weekOfMonthButtons addObject:aButton];
        [aButton addTarget:self action:@selector(toggleWeekOfMonthPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self setWeekOfMonthPressed];

    [[cell contentView] addSubview:aControl];
    
    return aControl;
}

- (void)createWeekOfMonthInCell:(UITableViewCell*)aCell
{
    NSArray* anItems = [NSArray arrayWithObjects:FXLLocalizedStringFromTable(@"1", @"RecurrenceEditController", @"Index for weeks of the month - 1"),
												FXLLocalizedStringFromTable(@"2", @"RecurrenceEditController", @"Index for weeks of the month - 2"),
												FXLLocalizedStringFromTable(@"3", @"RecurrenceEditController", @"Index for weeks of the month - 3"),
												FXLLocalizedStringFromTable(@"4", @"RecurrenceEditController", @"Index for weeks of the month - 4"),
												FXLLocalizedStringFromTable(@"5", @"RecurrenceEditController", @"Index for weeks of the month - 5"), nil];
    [self weekOfMonthControl:aCell items:anItems];
}

- (void)pushViewController:(UIViewController*)aViewController
{
    isPush = TRUE;
    [self.navigationController pushViewController:aViewController animated:YES];
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString* aString = nil;
    if(section == kSectionRepeatOn) {
        switch(self.event.recurrence.recurrenceType) {
            case kRecurrenceNone:
                break;
            case kRecurrenceDaily:
                break;
            case kRecurrenceWeekly:
                aString = FXLLocalizedStringFromTable(@"DAYS_OF_WEEK", @"RecurrenceEditController", @"Title of section for choosing on which day of the week the weekly recurrence should occur");
                break;
            case kRecurrenceMonthly:
                break;
            case kRecurrenceYearly:
                aString = FXLLocalizedStringFromTable(@"DAY_OF_YEAR", @"RecurrenceEditController", @"Title of section for choosing on which day of the year the yearly recurrence should occur");
                break;
            case kRecurrenceYearlyOnNthDay:
            case kRecurrenceMonthlyOnNthDay:
                aString = FXLLocalizedStringFromTable(@"WEEK_OF_MONTH", @"RecurrenceEditController", @"Title of section for choosing on which week of the month the monthly or yearly recurrence on nth day should occur");
        }
    }else if(section == kSectionUntil) {
        aString = FXLLocalizedStringFromTable(@"UNTIL", @"RecurrenceEditController", @"Title of section for choosing until when the event recurrence should occur");
    }
    return aString;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numSections = kSectionLast;
    
    if(!self.event.recurrence || self.event.recurrence.recurrenceType == kRecurrenceNone) {
        numSections = 1;
    }else{
        numSections = kSectionLast;
    }
    
    return numSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRows = 0;
    
    switch(section) {
        case kSectionRepeatOn:
        {
            switch(self.event.recurrence.recurrenceType) {
                case kRecurrenceMonthlyOnNthDay:
                case kRecurrenceYearlyOnNthDay:
                    numRows = 2;
                    break;
                case kRecurrenceWeekly:
                    numRows = 1;
                    break;
                case kRecurrenceDaily:
                case kRecurrenceMonthly:
                case kRecurrenceYearly:
                    numRows = 0;
                    break;
                case kRecurrenceNone:
                    numRows = 0;
                    break;
            }
            break;
        }
        case kSectionRecurrenceType:
        case kSectionInterval:
        case kSectionStarting:
            numRows = 1;
            break;
        case kSectionUntil:
            numRows = kUntilLast;
            break;
        case kSectionLast:
            numRows = 0;
            break;
    }

    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    EventRecurrence* aRecurrence = self.event.recurrence;
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }else{
        // Remove Text Field overlay if there is one
        //
        UIView* aContentView = cell.contentView;
        NSArray* aSubViews = aContentView.subviews;
        if(aSubViews.count > 0) {
            CGRect aContentViewRect = aContentView.frame;
            aContentViewRect.origin.x = 0.0f;
            aContentView.frame = aContentViewRect;
            for(UIView* aView in aSubViews) {
                [aView removeFromSuperview];
            }
        }
    }
    
	switch((ERecurrenceSection)indexPath.section) {
        case kSectionRecurrenceType:
            cell.accessoryType          = UITableViewCellAccessoryNone;
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"REPEAT", @"RecurrenceEditController", @"Title of controller for choosing whether or not event repeats and the frequency of a recurrence");
            if(self.event.recurrence) {
                cell.detailTextLabel.text   =  [aRecurrence typeAsString];
            }else{
                cell.detailTextLabel.text   = FXLLocalizedStringFromTable(@"NO", @"RecurrenceEditController", @"If the event does not recur, mark REPEAT as NO");
            }
            cell.accessoryType          = UITableViewCellAccessoryNone;
            break;
        case kSectionInterval:
        {
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"INTERVAL", @"RecurrenceEditController", @"Title of controller for choosing how often an event repeats");
            cell.detailTextLabel.text   = [aRecurrence intervalAsString];
            cell.accessoryType          = UITableViewCellAccessoryNone;
            break;
        }
        case kSectionRepeatOn:
        {
            switch(self.event.recurrence.recurrenceType) {
                case kRecurrenceNone:
                    break;
                case kRecurrenceDaily:
                    // NOTE: Docs imply DayOfWeek chould be set for this but not sure how
                    break;
                case kRecurrenceWeekly:
                {
                    cell = [self createDayOfWeekInCell];
                    break;
                }
                case kRecurrenceMonthly:
                {
                    // NOTE: Docs imply DayOfMonth should be set for this but startDate should tell this
                    break;
                }
                case kRecurrenceYearly:
                {
                    // NOTE: Docs imply DayOfMonth and MonthOfYear should be set for this but startDate should tell this
                    break;
                }
                case kRecurrenceMonthlyOnNthDay:
                case kRecurrenceYearlyOnNthDay:
                {
                    if(indexPath.row == 0) {
                        [self createWeekOfMonthInCell:cell];
                        break;
                    }else if(indexPath.row == 1) {
                        cell = [self createDayOfWeekInCell];
                    }else if(indexPath.row == 2) {
                        // Docs imply  MonthOfYear UI if this is kRecurrenceYearlyOnNthDay
                    }
                }
            }
            cell.accessoryType          = UITableViewCellAccessoryNone;
            break;
        }
        case kSectionStarting:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"STARTING", @"RecurrenceEditController", @"Title of controller for choosing when the recurrence of an event should start");
            cell.detailTextLabel.text   = [[Event dateFormatter] stringFromDate:self.event.recurrence.startTimelessDate];
            cell.accessoryType          = UITableViewCellAccessoryNone;
            break;
        case kSectionUntil:
            switch(indexPath.row) {
                case kUntilForever:
                    cell.textLabel.text         = FXLLocalizedStringFromTable(@"FOREVER", @"RecurrenceEditController", @"Title of option in REPEAT UNTIL section for event to repeat forever");
                    if(self.event.recurrence.occurences == 0 && !self.event.recurrence.untilDate) {
                        cell.accessoryType          = UITableViewCellAccessoryCheckmark;
                    }else{
                        cell.accessoryType          = UITableViewCellAccessoryNone;
                    }
                    cell.detailTextLabel.text = @"";
                    break;
                case kUntilOccurences:
                    cell.textLabel.text         = FXLLocalizedStringFromTable(@"OCCURENCES", @"RecurrenceEditController", @"Title of option in REPEAT UNTIL section for event to repeat a specific number of times");
                    if(self.event.recurrence.occurences) {
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d", self.event.recurrence.occurences];
                    }else{
                        cell.detailTextLabel.text = @"";
                    }
                    if(self.event.recurrence.occurences > 1) {
                        cell.accessoryType          = UITableViewCellAccessoryCheckmark;
                    }else{
                        cell.accessoryType          = UITableViewCellAccessoryNone;
                    }
                    break;
                case kUntilDate:
                    cell.textLabel.text         = FXLLocalizedStringFromTable(@"UNTIL_DATE", @"RecurrenceEditController", @"Title of option in REPEAT UNTIL section for event to repeat forever");
                    if(self.event.recurrence.untilDate) {
                        cell.detailTextLabel.text = [[Event dateFormatterTimeless] stringFromDate:self.event.recurrence.untilDate];
                    }else{
                        cell.detailTextLabel.text = @"";
                    }
                    if(self.event.recurrence.untilDate) {
                        cell.accessoryType          = UITableViewCellAccessoryCheckmark;
                    }else{
                        cell.accessoryType          = UITableViewCellAccessoryNone;
                    }
                    break;
            }
            break;
        case kSectionLast:
            break;
    }

    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!self.event.isEditable) {
        return;
    }
    @try {
        EventRecurrence* aRecurrence = self.event.recurrence;
        switch((ERecurrenceSection)indexPath.section) {
            case kSectionRecurrenceType:
            {
                self.recurrenceTypeSheet = [[RecurrenceTypeSheet alloc] initWithView:self.view
                                                                navController:self.navigationController
                                                                               event:self.event
                                                                           tableView:tableView
                                                                           indexPath:indexPath];
                self.recurrenceTypeSheet.delegate = self;
                break;
            }
            case kSectionInterval:
            {
                CGRect aLocation = [tableView rectForRowAtIndexPath:indexPath];
                self.numberPickerController = [NumberPickerController numberPickerForViewController:self rect:aLocation value:aRecurrence.interval min:1 max:100 action:@selector(changeInterval:)];
                break;
            }
            case kSectionRepeatOn:
                break;
            case kSectionStarting:
            {
                // Insure recurrence start date is set
                //
                if(!self.event.recurrence.startTimelessDate) {
                    self.event.recurrence.startTimelessDate = self.event.startDate;
                }
                
                if(IS_IPAD()) {
                    CGRect location = [tableView rectForRowAtIndexPath:indexPath];
                    [DatePickerController datePickerForViewController:self rect:location date:self.event.recurrence.startTimelessDate action:@selector(changeStartDate:)];
                }else{
                    DatePickerControllerIPhone* aController = [DatePickerControllerIPhone datePickerForViewController:self
                                                                                                                event:event
                                                                                                                field:kDatePickerStart];
                    aController.delegate         = self;
                    aController.title            = self.event.title;
                    aController.section          = kSectionStarting;
                    aController.label            = FXLLocalizedStringFromTable(@"RECUR_STARTING", @"RecurrenceEditController", @"Title of DatePickerControllerIPhone to choose when event should start recurring");
                    aController.isRecurrenceEdit = TRUE;
                    [self pushViewController:aController];
                }
                break;
            }
            case kSectionUntil:
                switch(indexPath.row) {
                    case kUntilForever:
                    {
                        aRecurrence.occurences  = 0;
                        aRecurrence.untilDate   = nil;
                        [self _redrawUntilSection];
                        break;
                    }
                    case kUntilOccurences:
                    {
                        CGRect aLocation = [tableView rectForRowAtIndexPath:indexPath];
                        self.numberPickerController = [NumberPickerController numberPickerForViewController:self rect:aLocation value:aRecurrence.occurences min:1 max:100 action:@selector(changeUntilOccurrences:)];
                        break;
                    }
                    case kUntilDate:
                    {
                        if(!self.event.recurrence.untilDate) {
    #warning FIXME should add one recurrence interval to this
                            self.event.recurrence.untilDate = [NSDate dateWithTimeInterval:0 sinceDate:self.event.startDate];
                        }
                        CGRect location = [tableView rectForRowAtIndexPath:indexPath];
                        if(IS_IPAD()) {
                            DatePickerController* aController = [DatePickerController datePickerForViewController:self
                                                                                                             rect:location
                                                                                                             date:self.event.recurrence.untilDate
                                                                                                           action:@selector(changeUntilDate:)];
                            aController.datePicker.datePickerMode = UIDatePickerModeDate;
                        }else{
                            DatePickerControllerIPhone* aController = [DatePickerControllerIPhone datePickerForViewController:self
                                                                                                                        event:event
                                                                                                                        field:kDatePickerStart];
                            aController.delegate         = self;
                            aController.title            = self.event.title;
                            aController.section          = kSectionUntil;
                            aController.label            = FXLLocalizedStringFromTable(@"RECUR_UNTIL", @"RecurrenceEditController", @"Title of DatePickerControllerIPhone to choose when event should stop recurring");
                            aController.isRecurrenceEdit = TRUE;
                            [self pushViewController:aController];
                        }
                        break;
                    }
                }
                break;
            case kSectionLast:
                break;
        }
    }@catch(NSException* e) {
        [self logException:@"didSelectRowAtIndexPath" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// DatePickerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark DatePickerDelegate

- (void)setDate:(NSDate*)aDate section:(int)aSection row:(int)aRow
{
    switch(aSection) {
        case kSectionStarting:
            self.event.recurrence.startTimelessDate = aDate;
            break;
        case kSectionUntil:
            self.event.recurrence.untilDate         = aDate;
            break;
    }
}

- (NSDate*)dateForSection:(int)aSection row:(int)aRow
{
    NSDate* aDate = nil;
    switch(aSection) {
        case kSectionStarting:
            aDate = self.event.recurrence.startTimelessDate;
            break;
        case kSectionUntil:
            aDate = self.event.recurrence.untilDate;
            break;
    }
    return aDate;
}


@end
