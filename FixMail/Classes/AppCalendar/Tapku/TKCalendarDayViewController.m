//
//  ODCalendarDayViewController.m
//  Created by Devin Ross on 7/28/09.
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */

#import "TKCalendarDayViewController.h"
#import "CalendarController.h"
#import "TKCalendarDayEventView.h"
#import "TKCalendarDayTimelineViewDelegate.h"

@implementation TKCalendarDayViewController
@synthesize calendarDayTimelineView;
@synthesize numDays;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

////////////////////////////////////////////////////////////////////////////////
// View 
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)dealloc 
{
	self.calendarDayTimelineView = nil;
}

- (void)viewDidUnload 
{
	self.calendarDayTimelineView = nil;
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	if (!self.firstDayOfTheWeek) {
		self.firstDayOfTheWeek = 1;
	}
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.calendarDayTimelineView.superview != self.view) {
        [self.view addSubview:self.calendarDayTimelineView];
    }
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (TKCalendarDayTimelineView *)calendarDayTimelineView
{
	if (!_calendarDayTimelineView) {
		_calendarDayTimelineView = [[TKCalendarDayTimelineView alloc]initWithFrame:self.view.bounds numDays:self.numDays];
        [_calendarDayTimelineView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
        
		_calendarDayTimelineView.delegate = self;
	}
	return _calendarDayTimelineView;
}

////////////////////////////////////////////////////////////////////////////////
// Setters 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Setters

- (void)setDate:(NSDate*)aDate
{
    [[CalendarController controller] setDate:aDate];
    [self.calendarDayTimelineView setDate:aDate];
}

- (void)setDelegate:(NSObject<TKCalendarDayTimelineViewDelegate>*)aDelegate
{
    [self.calendarDayTimelineView setDelegate:aDelegate];
}

- (void)setFirstDayOfTheWeek:(int)firstDayOfTheWeek
{
	_firstDayOfTheWeek = firstDayOfTheWeek;
	if (self.calendarDayTimelineView) {
		[self.calendarDayTimelineView setFirstDayOfTheWeek:_firstDayOfTheWeek];
	}
}


////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDelegate 
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDelegate

- (NSArray *)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventsForDate:(NSDate *)eventDate
{
    FXDebugLog(kFXLogFIXME, @"FIXME TKCalendarDayTimelineViewDelegate eventsForDate");
	
	return [NSArray arrayWithObjects:nil];
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView
{
	NSLog(@"FIXME TKCalendarDayTimelineViewDelegate: EventViewWasSelected");
}

@end
