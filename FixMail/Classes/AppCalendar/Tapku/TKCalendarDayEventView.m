//
//  ODCalendarDayEventView.m
//  Created by Devin Ross on 7/28/09.
//
/*
 
 tapku.com || http://github.com/devinross/tapkulibrary
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 */
#import "TKCalendarDayEventView.h"
#import "CalendarController.h"
#import "CalendarTheme.h"
#import "Event.h"
#import "TKTimelineGlobal.h"

@implementation TKCalendarDayEventView

// Properties
@synthesize event=_event;
@synthesize eventStyle=_eventStyle;
@synthesize startDate=_startDate;
@synthesize endDate=_endDate;
@synthesize title=_title;
@synthesize location=_location;
@synthesize balloonColorTop;
@synthesize balloonColorBottom;
@synthesize textColor;

// Constants
static const CGFloat kEventFontSize     = 13.0;

+ (id)eventViewWithFrame:(CGRect)frame
                   event:(Event*)anEvent
               startDate:(NSDate *)startDate
                 endDate:(NSDate *)endDate
                   title:(NSString *)title
                location:(NSString *)location
              eventStyle:(EEventStyle)anEventStyle
{
	TKCalendarDayEventView* anEventView = [[TKCalendarDayEventView alloc]initWithFrame:frame event:anEvent];
    
    anEventView.eventStyle  = anEventStyle;
	anEventView.startDate   = startDate;
	anEventView.endDate     = endDate;
	anEventView.title       = title;
	anEventView.location    = location;
	
	return anEventView;
}

// The designated initializer. Override to perform setup that is required before the view is loaded.
// Only when xibless (interface buildder)
- (id)initWithFrame:(CGRect)frame event:anEvent
{
    if(self = [super initWithFrame:frame]) {
        [self setupCustomInitialisation:anEvent];
    }
    return self;
}

- (void)dealloc
{
    if(gradient) {
        CFRelease(gradient);
        gradient = nil;
    }
}

- (void)setupCustomInitialisation:(Event*)anEvent
{
	// Initialization code
    self.eventStyle = kEventStyleBalloon;
    self.event      = anEvent;
	self.startDate  = nil;
	self.endDate    = nil;
	self.title      = nil;
	self.location   = nil;
	
	twoFingerTapIsPossible  = FALSE;
    
    CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
    UIColor* aColor;
    if([anEvent isHoliday]) {
        aColor = aCalendarTheme.holidayColorDark;
    }else if(anEvent.isMeeting) {
        aColor  = aCalendarTheme.meetingColorDark;
    }else{
        aColor  = aCalendarTheme.eventColorDark;
    }
    self.balloonColorTop    = aColor;
    self.balloonColorBottom = nil;
    self.textColor          = aCalendarTheme.dateInverseColor;
    [self setGradient];
}

- (void)setGradient
{
    if(balloonColorTop != balloonColorBottom) {
		UIColor* aLowerColor = nil;
		if(!balloonColorBottom) {
            // If there is no bottom color set, we get a darker version of the top one
            //
			const CGFloat* components = CGColorGetComponents([balloonColorTop CGColor]);
			aLowerColor = [UIColor colorWithRed:components[0]-0.25f green:components[1]-0.25f blue:components[2]-0.25f alpha:components[3]];
		}else{
			aLowerColor = balloonColorBottom;
		}
        
		// Create alinear gradient from the colors
        //
		CFArrayRef colors           = (__bridge CFArrayRef)[NSArray arrayWithObjects:(id)[balloonColorTop CGColor], (id)[aLowerColor CGColor], nil];
		CGColorSpaceRef aColorspace = CGColorSpaceCreateDeviceRGB();
		gradient                    = CGGradientCreateWithColors(aColorspace, colors, NULL);
		CFRelease(aColorspace);
	}else if(gradient) {
        CFRelease(gradient);
        gradient = nil;
    }
}

- (void)drawRect:(CGRect)rect
{
	CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);	
	
    @try {
        if(_eventStyle == kEventStyleBalloon) {        
            self.alpha = 0.8;
            CALayer *layer = [self layer];
            layer.masksToBounds = YES;
            [layer setCornerRadius:5.0];
            // You can even add a border
            [layer setBorderWidth:0.5];
            CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
            [layer setBorderColor:[aCalendarTheme.eventBorder CGColor]];
            
            if (balloonColorTop == balloonColorBottom) {
                // Background is simple fill
                self.backgroundColor = balloonColorTop;
            }else{
                // If we have 2 different colors, we draw a gradient
                //
                if(!gradient) {
                    [self setGradient];
                }
                CGContextDrawLinearGradient(context, gradient, CGPointMake(0.0f,0.0f), CGPointMake(0.0f,self.bounds.size.height), 0);	
            }
            
            // Set shadow
            //
            CGContextSetShadowWithColor(context,  CGSizeMake(0.0, 1.0), 0.7, [aCalendarTheme.shadowColor CGColor]);
            
            // Set text color
            //
            [textColor set];
            
            // Title
            //
            CGFloat availableHeight = self.bounds.size.height - kTimelineVerticalOffset - kTimelineVerticalOffset;

            CGSize titleSize = CGSizeZero;
            if (self.title) {
                CGRect titleRect = CGRectMake(self.bounds.origin.x + kTimelineHorizontalOffset,
                                              self.bounds.origin.y + kTimelineVerticalOffset,
                                              self.bounds.size.width - 2 * kTimelineHorizontalOffset,
                                              availableHeight);
                titleSize = [self.title drawInRect:CGRectIntegral(titleRect) 
                                          withFont:[UIFont boldSystemFontOfSize:kTimelineFontSize]
                                     lineBreakMode:(availableHeight < kTimelineVerticalDiff ? NSLineBreakByTruncatingTail  : NSLineBreakByWordWrapping)
                                         alignment:NSTextAlignmentLeft];
            }
            
            // Location
            //
            if (titleSize.height + kTimelineFontSize < availableHeight) {
                if (self.location) {
                    CGRect locationRect = CGRectMake(self.bounds.origin.x + kTimelineHorizontalOffset,
                                                     self.bounds.origin.y + kTimelineVerticalOffset,
                                                     self.bounds.size.width - 2 * kTimelineHorizontalOffset,
                                                     availableHeight);
                    locationRect.origin.y += titleSize.height;
                    locationRect.size.height -= titleSize.height;
                    UILineBreakMode breaking = (locationRect.size.height < kTimelineFontSize + kTimelineVerticalOffset ? NSLineBreakByTruncatingTail : NSLineBreakByWordWrapping);
                    [self.location drawInRect:CGRectIntegral(locationRect) 
                                  withFont:[UIFont systemFontOfSize:kTimelineFontSize]
                             lineBreakMode:breaking
                                 alignment:NSTextAlignmentLeft];
                }
            }
        }else{
            [UIFont systemFontOfSize:kEventFontSize];

            CalendarTheme* aCalendarTheme = [[CalendarController controller] calendarTheme];
            Event* anEvent = self.event;
            if([anEvent isHoliday]) {
                [aCalendarTheme.holidayColorDark set];
            }else if(anEvent.isMeeting) {
                [aCalendarTheme.meetingColorDark set];
            }else{
                [aCalendarTheme.eventColorDark set];
            }
            
            // Title
            //
            CGFloat availableHeight = self.bounds.size.height - kTimelineVerticalOffset - kTimelineVerticalOffset;
            
            CGSize titleSize = CGSizeZero;
            NSString* aTitle = self.title;
            if(aTitle.length > 0) {
            }else{
                aTitle = FXLLocalizedStringFromTable(@"NO_TITLE_CALENDAR_DAY_EVENT_VIEW", @"Calendar", @"Default title when no preset title in TKCalendarDayEventView");
            }
            CGRect titleRect = CGRectMake(self.bounds.origin.x + kTimelineHorizontalOffset,
                                          self.bounds.origin.y,
                                          self.bounds.size.width - 2 * kTimelineHorizontalOffset,
                                          availableHeight);
            titleSize = [aTitle drawInRect:CGRectIntegral(titleRect)
                                  withFont:[UIFont boldSystemFontOfSize:kTimelineFontSize]
                             lineBreakMode:NSLineBreakByTruncatingTail
                                 alignment:NSTextAlignmentLeft];
        }
    }@catch(NSException* e) {
		FXDebugLog(kFXLogCalendar, @"TKCalendarDayEventView drawRect: %@ %@", [e name], [e reason]);
    }
    
	CGContextRestoreGState(context);
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (void)writeString:(NSMutableString*)string tag:(NSString*)tag value:(NSString*)value
{
    if(!value) {
        value = @"nil";
    }
	[string appendString:[NSString stringWithFormat:@"\t%@ = \"%@\";\n", tag, value]];
}

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];

    [self writeString:string		tag:@"title"                value:self.title];
    if(self.location.length > 0) {
        [self writeString:string    tag:@"location"             value:self.location];
    }

	[self writeString:string		tag:@"startDate"            value:[self.startDate description]];
	[self writeString:string		tag:@"endDate"              value:[self.endDate description]];
    
    [self writeString:string		tag:@"balloonColorTop"      value:[self.balloonColorTop description]];
    if(self.balloonColorBottom) {
        [self writeString:string    tag:@"balloonColorBottom"   value:[self.balloonColorBottom description]];
    }

    [string appendString:@"}\n"];
    
    return string;
}

@end
