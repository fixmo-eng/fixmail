/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "InviteCell.h"
#import "ASEmail.h"

@implementation InviteCell

@synthesize email;
@synthesize titleLabel;
@synthesize fromLabel;

- (id)initWithStyle:(UITableViewCellStyle)aStyle reuseIdentifier:(NSString *)aReuseIdentifier
{
	self = [super initWithStyle:aStyle reuseIdentifier:aReuseIdentifier];
    if (self) {
    }
    
    return self;
}

- (void)loadEmail:(ASEmail *)anEmail
{
    self.email = anEmail;
    if(anEmail) {
        self.titleLabel.text  = anEmail.subject;
        self.fromLabel.text   = anEmail.from;
    }else{
        self.titleLabel.text  = nil;
        self.fromLabel.text   = nil;
    }
}

@end
