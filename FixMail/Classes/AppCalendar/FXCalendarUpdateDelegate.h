//
//  FXCalendarUpdateDelegate.h
//  FixMail
//
//  Created by Sean Langley on 2013-07-02.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Event.h"

@protocol FXCalendarUpdateDelegate <NSObject>

- (void) didUpdateToDate:(NSDate*)date;
- (void) didSelectEvent:(Event*)event;

@end
