//
//  FXMeetingViewTitleCell.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMeetingViewTitleCell.h"

@interface FXMeetingViewTitleCell()
{
    float _titleHeight;
}
@property (nonatomic,retain)UILabel* titleLabel;

@end

@implementation FXMeetingViewTitleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectNull];
        _titleLabel.numberOfLines = 0;
        _titleLabel.backgroundColor = [UIColor clearColor];
        
        [self.contentView addSubview:_titleLabel];
        }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(float)heightRequiredForCell
{
    [self calculateHeights];
    return _titleHeight+5+5;
}

-(void)layoutSubviews
{
    [self calculateHeights];
    
    CGRect titleRect = CGRectMake(20,5,self.frame.size.width - 40 ,_titleHeight);
    _titleLabel.frame = titleRect;
}

-(void)prepareForReuse
{
    _title = @"";
}

-(void)setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = _title;
    
    [self calculateHeights];
}



-(void)calculateHeights
{
    CGSize size = [_title sizeWithFont:_titleLabel.font constrainedToSize:CGSizeMake(self.frame.size.width - 40,9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    _titleHeight = size.height;
    
}


@end
