//
//  FXCalendarDelegate.m
//  FixMail
//
//  Created by Colin Biggin on 2013-01-25.
//
//

#import "FXCalendarDelegate.h"
#import "ASAccount.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
#import "BaseAccount.h"
#import "ASCalendarFolder.h"
#import "CalendarController.h"
#import "ASConfigViewController.h"
#import "FXCNotificationsManager.h"

@interface FXCalendarDelegate ()
@property (nonatomic, strong) UINavigationController *rootViewController;
@end

@implementation FXCalendarDelegate
@synthesize rootViewController = _rootViewController;

+ (void) initialize
{
	return;
}

+ (NSString*) applicationIcon
{
	return @"FixMailRsrc.bundle/SafeGuard_Calendar_Icon";
}

+ (NSString*) applicationName
{
	return FXLLocalizedString(@"Calendar", @"Calendar app name displayed to the user");
}

-(BaseFolder*) getCalendarFolder
{
    BaseFolder *calendarFolder = nil;

    BaseAccount *currentAccount = [BaseAccount currentLoggedInAccount];
    if (currentAccount != nil) {
        NSArray *folders = [currentAccount foldersForFolderType:kFolderTypeCalendar];
        if (folders.count == 1) {
            calendarFolder = (ASFolder *)folders[0];
        }else if(folders.count == 0){
            FXDebugLog(kFXLogCalendar, @"No calender folder");
        }else{
            calendarFolder = (ASFolder *)folders[0];
            FXDebugLog(kFXLogFIXME, @"More than one calender folder");
        }
    }
    return calendarFolder;
}

- (UIViewController *) rootViewController
{ 
	if (!_rootViewController) {
	//	UIColor* aColor = [aNavigationController.navigationBar tintColor];
		UIColor *aColor = [UIColor colorWithWhite:80.0/255.0 alpha:1.0];//[UIColor blackColor];

		// get the size from the main window
		CGSize aSize = [UIApplication sharedApplication].keyWindow.bounds.size;

		if([BaseAccount accounts].count == 0) {
			[ASAccount loadExistingEmailAccounts];
		}

		// get the folder from the base account
		BaseFolder *calendarFolder = [self getCalendarFolder];
        [FXCNotificationsManager.instance setUpFolderDelegateForFolder:calendarFolder];


		CalendarController* aCalendarController = [CalendarController createControllerWithFolder:(ASCalendarFolder *)calendarFolder size:aSize color:aColor];
			
		UIViewController *theController = [aCalendarController calendarViewControllerForDate:[NSDate date] folder:(ASCalendarFolder *)calendarFolder size:aSize];

		_rootViewController = [[UINavigationController alloc] initWithRootViewController:theController];
		_rootViewController.navigationBar.tintColor = [UIColor blackColor];
	}
	return _rootViewController;
}

- (BOOL) containedApplication:(id <SZLApplicationContainer>)applicationContainer didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
{
    // Start ping for this account
    ASAccount* anASAccount = (ASAccount*)[BaseAccount currentLoggedInAccount];
    [anASAccount startPing];
    
    return YES;
}

- (void) containedApplicationWillTerminate:(id <SZLApplicationContainer>)applicationContainer
{
    [FXCNotificationsManager.instance tearDownCalendarDelegate];
    [ASAccount removePings];
}

- (void) containedApplicationDidBecomeActive:(id <SZLApplicationContainer>)applicationContainer
{
	if ([BaseAccount accountsConfigured] < 1) {
		UINavigationController *configurationController = [ASConfigViewController accountConfigurationControllerWithDelegate:self];
		[self.rootViewController presentViewController:configurationController animated:YES completion:nil];
	}
}

#pragma mark - SZLConfigurationDelegate Methods
-(void)configurationDidFail:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationWasCancelled:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

-(void)configurationDidSucceed:(id <SZLApplicationContainerDelegate>)application
{
	[self.rootViewController dismissViewControllerAnimated:YES completion:^(void) {
		[[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainer];
	}];
}

@end
