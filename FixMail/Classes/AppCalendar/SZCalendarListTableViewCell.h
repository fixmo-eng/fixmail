//
//  SZCalendarListTableViewCell.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-25.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface SZCalendarListTableViewCell : UITableViewCell
@property (nonatomic,weak) Event* event;
@end
