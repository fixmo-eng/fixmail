//
//  FXCalendarAgendaViewController.m
//  FixMail
//
//  Created by Sean Langley on 2013-07-02.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarAgendaViewController.h"

#import "FXCalendarDayViewController.h"
#import "FXCalendarListViewController.h"
#import "CalendarController.h"
#import "EventEditController.h"

#import "DateUtil.h"

@interface FXCalendarAgendaViewController ()
@property  FXCalendarListViewController* listViewController;
@property  FXCalendarDayViewController* dayViewController;
@end

@implementation FXCalendarAgendaViewController
@synthesize  firstDayOfTheWeek;

- (void)prev
{
    [self.dayViewController prev];
}

- (void)next
{
    [self.dayViewController next];
}

- (void)today
{
    [self.dayViewController setDate:[NSDate date]];
    [self.listViewController scrollToDate:[NSDate date]];
}

- (void)addEvent
{
    NSDate* addedDate = [[self dayViewController] currentDate];
    
    if (!addedDate)
    {
        addedDate = [NSDate date];
    }
    
    NSInteger unitFlags = (NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit);
    NSDateComponents* dateComponents = [[[DateUtil getSingleton] currentCalendar] components:unitFlags fromDate: addedDate];
    
    NSDate* aDate = [[[DateUtil getSingleton] currentCalendar] dateFromComponents:dateComponents];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:self.folder date:aDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:nc animated:YES completion:nil];

}

- (void)editEvent:(Event*)anEvent
{
    
}

- (void)setDate:(NSDate*)aDate
{
    [self.dayViewController setDate:aDate];
    [self.listViewController scrollToDate:aDate];

}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
    [[CalendarController controller] setCalendarViewControllerForDate:self.currentDate
                                                               folder:self.folder
                                                                 size:self.view.bounds.size
                                                 navigationController:self.navigationController];
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.listViewController = [[FXCalendarListViewController alloc] init];
    self.listViewController.currentDate = self.currentDate;
    self.listViewController.folder = self.folder;
    self.listViewController.view.frame = CGRectInset(self.listView.bounds,1,1);
    self.listViewController.updateDelegate = self;
    
    
    [self.listViewController willMoveToParentViewController:self];
    [self addChildViewController:self.listViewController];
    self.listView.outlineColor = [UIColor lightGrayColor];
    [self.listView addSubview:self.listViewController.view];
    
    
    self.dayViewController = [[FXCalendarDayViewController alloc] init ];//]WithNibName:@"" bundle:[FXLSafeZone getResourceBundle]];
    self.dayViewController.numDays = 1;

    self.dayViewController.folder = self.folder;
    self.dayViewController.view.frame = CGRectInset(self.dayView.bounds,1,1);
    [self.dayViewController setDate: self.currentDate];
    self.dayViewController.view.clipsToBounds = YES;

    
    [self.dayViewController willMoveToParentViewController:self];
    [self addChildViewController:self.listViewController];
    self.dayView.outlineColor = [UIColor lightGrayColor];
    [self.dayView addSubview:self.dayViewController.view];

    
    self.titleView.text = [[DateUtil getSingleton] stringFromDateMonthAndYear:self.currentDate];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if(self.navigationController)
    {
        [[CalendarController controller] topToolbar:self.navigationItem];
    
          NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
        self.toolbarItems = aToolBarItems;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationItem.titleView = nil;

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[CalendarController controller] calendarSelector:self.navigationItem];
    
}

- (void) didUpdateToDate:(NSDate*)date
{
    self.titleView.text = [[DateUtil getSingleton] stringFromDateMonthAndYear:date];
}

-(void) didSelectEvent:(Event *)event
{
    NSDate* date = [event startDate];
    [self.dayViewController setDate:date];
}



@end
