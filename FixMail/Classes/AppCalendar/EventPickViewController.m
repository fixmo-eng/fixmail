 /*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "EventPickViewController.h"
#import "DateUtil.h"
#import "Event.h"
#import "EventEngine.h"
#import "SearchRunner.h"

@implementation EventPickViewController

// Properties
@synthesize folder;
@synthesize searchResults;
@synthesize delegate;
@synthesize searchText;
@synthesize event;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        dateUtil = [DateUtil getSingleton];
    }
    return self;
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: EventPickViewController %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Internal
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal

- (void)_eventNoMatch
{
    self.searchResults = nil;
    [delegate pickerNoMatch];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Search result callback
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Search result callback

- (void)_deliverSearchResult:(NSArray*)aResults
{
    @try {
        if(aResults.count > 0) {
            self.searchResults = aResults;
            [self.tableView reloadData];
            [delegate pickerShow];
        }else{
            [self _eventNoMatch];
        }
    }@catch (NSException* e) {
        [self _logException:@"_deliverSearchResult" exception:e];
    }
}

- (void)deliverSearchResult:(NSArray*)aResults
{
    [self performSelectorOnMainThread:@selector(_deliverSearchResult:) withObject:aResults waitUntilDone:FALSE];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Search
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Search

- (void)search:(NSString*)aText
{
    @try {
        self.searchText = aText;
        if (aText.length > 0) {
            NSString* aQuery;
            if(aText.length > 2) {
                aQuery = [NSString stringWithFormat:@"%%%@%%", aText];
            }else{
                aQuery = [NSString stringWithFormat:@"%@%%", aText];
            }
            // Start a search against the contacts database
            //
            [[EventEngine engine] searchFor:aQuery folder:(BaseFolder*)self.folder withDelegate:self];
        } else {
            [self _eventNoMatch];
        }
    }@catch (NSException* e) {
        [self _logException:@"search" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    [delegate pickerViewDismissed];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0 && self.searchResults.count > indexPath.row) {
        Event* anEvent = [self.searchResults objectAtIndex:indexPath.row];
        if(anEvent) {
            cell.textLabel.text         = anEvent.title;
            NSDate* aStartDate = anEvent.startDate;
            if(aStartDate) {
                cell.detailTextLabel.text   = [dateUtil humanDate:aStartDate];
            }else{
                cell.detailTextLabel.text   = @"";
            }
            cell.imageView.image        = nil;
        }else{
            cell.textLabel.text         = @"";
            cell.detailTextLabel.text   = @"";
        }
	}else{
        cell.textLabel.text         = @"";
        cell.detailTextLabel.text   = @"";
    }
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && self.searchResults.count > indexPath.row) {
        Event* anEvent = [self.searchResults objectAtIndex:indexPath.row];
        if(anEvent) {
            if(delegate) {
                [delegate pickerSelected:anEvent];
            }
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}

@end
