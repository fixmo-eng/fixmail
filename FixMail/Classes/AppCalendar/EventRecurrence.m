/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "EventRecurrence.h"
#import "Calendar.h"
#import "ErrorAlert.h"
#import "EventEngine.h"
#import "EventException.h"
#import "FastWBXMLParser.h"
#import "NSDate+TKCategory.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation EventRecurrence

// Properties
@synthesize recurrenceType;  
@synthesize interval;
@synthesize firstDayOfWeek;
@synthesize dayOfWeek;
@synthesize weekOfMonth;  
@synthesize monthOfYear; 
@synthesize dayOfMonth;
@synthesize daysOfMonth;
@synthesize occurences;
@synthesize untilDate;
@synthesize exceptions;
@synthesize startTimelessDate;
@synthesize endDate;

// Constants
static NSString* kRecurrenceType      = @"type";
static NSString* kRecurrenceInterval  = @"ival";
static NSString* kFirstDayOfWeek      = @"FDoW";
static NSString* kDayOfWeek           = @"DoW";
static NSString* kWeekOfMonth         = @"WoM";
static NSString* kMonthOfYear         = @"MoY";
static NSString* kDayOfMonth          = @"DoM";
static NSString* kDaysOfMonth         = @"DsoM";
static NSString* kOccurences          = @"occ";
static NSString* kUntilDate           = @"until";
static NSString* kExceptions          = @"except";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithRecurrenceType:(ERecurrenceType)aRecurrenceType
                    interval:(NSUInteger)anInterval
{
    if (self = [super init]) {
        self.recurrenceType     = kRecurrenceNone;
        self.interval           = anInterval;
    }
    return self;
}

- (id)initWithJSON:(NSString*)aRecurrenceJSON
{
    if (self = [super init]) {
        @try {
            NSDictionary* aDictionary = [aRecurrenceJSON JSONValue];
            self.recurrenceType     = [self intFromDictionary:aDictionary key:kRecurrenceType];
            self.interval           = [self intFromDictionary:aDictionary key:kRecurrenceInterval];
            self.firstDayOfWeek     = [self intFromDictionary:aDictionary key:kFirstDayOfWeek];
            self.dayOfWeek          = [self intFromDictionary:aDictionary key:kDayOfWeek];
            self.weekOfMonth        = [self intFromDictionary:aDictionary key:kWeekOfMonth];
            self.monthOfYear        = [self intFromDictionary:aDictionary key:kMonthOfYear];
            self.dayOfMonth         = [self intFromDictionary:aDictionary key:kDayOfMonth];
            self.daysOfMonth        = [aDictionary objectForKey:kDaysOfMonth];
            self.occurences         = [self intFromDictionary:aDictionary key:kOccurences];
            
            NSString* anExceptionString = [aDictionary objectForKey:kExceptions];
            if(anExceptionString.length > 0) {
                @try {
                    NSArray* anArray = [anExceptionString JSONValue];
                    if(anArray.count > 0) {
                        self.exceptions = [NSMutableArray arrayWithCapacity:anArray.count];
                        for(NSDictionary* aDictionary in anArray) {
                            EventException* anException = [[EventException alloc] init];
                            [anException fromDictionary:aDictionary];
                            [self.exceptions addObject:anException]; 
                        }
                    }
                }@catch (NSException* e) {
                    [self logException:@"initWithJSON exception" exception:e];
                }
            }

            NSString* anUntilString = [aDictionary objectForKey:kUntilDate];
            if(anUntilString.length > 0) {
                self.untilDate = [[EventEngine dateFormatter] dateFromString:anUntilString];
            }
        }@catch (NSException* e) {
            [self logException:@"initWithJSON" exception:e];
        }
    }
    return self;
}


- (void)copyToObject:(EventRecurrence*)anEventRecurrence zone:(NSZone*)zone
{
    anEventRecurrence.recurrenceType     = self.recurrenceType;
    anEventRecurrence.interval           = self.interval;
    anEventRecurrence.firstDayOfWeek     = self.firstDayOfWeek;
    anEventRecurrence.dayOfWeek          = self.dayOfWeek;
    anEventRecurrence.dayOfMonth         = self.dayOfMonth;
    anEventRecurrence.daysOfMonth        = self.daysOfMonth;
    anEventRecurrence.weekOfMonth        = self.weekOfMonth;
    anEventRecurrence.monthOfYear        = self.monthOfYear;
    anEventRecurrence.occurences         = self.occurences;
    anEventRecurrence.untilDate          = [self.untilDate copy];
    anEventRecurrence.startTimelessDate  = [self.startTimelessDate copy];
    anEventRecurrence.endDate            = [self.endDate copy];
    anEventRecurrence.exceptions         = [[NSMutableArray alloc] initWithArray:self.exceptions copyItems:TRUE];
}

- (id)copyWithZone:(NSZone*)zone
{
	EventRecurrence* anEventRecurrence = [[EventRecurrence allocWithZone:zone] init];
	[self copyToObject:anEventRecurrence zone:zone];
	return anEventRecurrence;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EVENT_RECURRENCE", @"ErrorAlert", @"EventRecurrence error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EVENT_RECURRENCE", @"ErrorAlert", nil) message:anErrorMessage];

}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (NSString*)asString
{
    NSString* aString = nil;
    
    aString = [self typeAsString];
    if(self.dayOfWeek) {
        
    }
    if(self.dayOfMonth) {
        
    }
    if(self.daysOfMonth) {
        
    }
    
    return aString;
}

- (NSString*)typeAsString
{
    NSString* aString;
    
    switch(self.recurrenceType) {
        case kRecurrenceDaily:
            aString = FXLLocalizedStringFromTable(@"DAILY", @"EventRecurrence", @"Event recurrence type as a string - daily"); break;
        case kRecurrenceWeekly:
            aString = FXLLocalizedStringFromTable(@"WEEKLY", @"EventRecurrence", @"Event recurrence type as a string - weekly"); break;
        case kRecurrenceMonthly:
            aString = FXLLocalizedStringFromTable(@"MONTHLY", @"EventRecurrence", @"Event recurrence type as a string - monthly"); break;
        case kRecurrenceYearly:
            aString = FXLLocalizedStringFromTable(@"YEARLY", @"EventRecurrence", @"Event recurrence type as a string - yearly"); break;
        case kRecurrenceMonthlyOnNthDay:
            aString = FXLLocalizedStringFromTable(@"MONTHLY_ON_DAY", @"EventRecurrence", @"Event recurrence type as a string - monthly on nth day"); break;
        case kRecurrenceYearlyOnNthDay:
            aString = FXLLocalizedStringFromTable(@"YEARLY_ON_DAY", @"EventRecurrence", @"Event recurrence type as a string - yearly on nth day"); break;
        case kRecurrenceNone:
            aString = FXLLocalizedStringFromTable(@"NO_RECURRENCE", @"EventRecurrence", @"Event recurrence type as a string - none"); break;
    }
    
    return aString;
}

- (NSString*)intervalAsString
{
    NSString* aString = @"";
    if(self.interval > 1) {
        switch(self.recurrenceType) {
            case kRecurrenceDaily:
                aString = FXLLocalizedStringFromTable(@"DAYS", @"EventRecurrence", @"Interval type between instances of a recurring event - days"); break;
            case kRecurrenceWeekly:
                aString =  FXLLocalizedStringFromTable(@"WEEKS", @"EventRecurrence", @"Interval type between instances of a recurring event - weeks"); break;
            case kRecurrenceMonthly:
            case kRecurrenceMonthlyOnNthDay:
                aString =  FXLLocalizedStringFromTable(@"MONTHS", @"EventRecurrence", @"Interval type between instances of a recurring event - months"); break;
            case kRecurrenceYearly:
            case kRecurrenceYearlyOnNthDay:
                aString =  FXLLocalizedStringFromTable(@"YEARS", @"EventRecurrence", @"Interval type between instances of a recurring event - years"); break;
            case kRecurrenceNone:
                break;
        }
    }else{
        switch(self.recurrenceType) {
            case kRecurrenceDaily:
                aString =  FXLLocalizedStringFromTable(@"DAY", @"EventRecurrence", @"Interval type between instances of a recurring event - a day"); break;
            case kRecurrenceWeekly:
                aString =  FXLLocalizedStringFromTable(@"WEEK", @"EventRecurrence", @"Interval type between instances of a recurring event - a week"); break;
            case kRecurrenceMonthly:
                aString =  FXLLocalizedStringFromTable(@"MONTH", @"EventRecurrence", @"Interval type between instances of a recurring event - a month"); break;
            case kRecurrenceYearly:
                aString =  FXLLocalizedStringFromTable(@"YEAR", @"EventRecurrence", @"Interval type between instances of a recurring event - a year"); break;
            case kRecurrenceMonthlyOnNthDay:
                aString = FXLLocalizedStringFromTable(@"MONTH", @"EventRecurrence", nil); break;
            case kRecurrenceYearlyOnNthDay:
                aString = FXLLocalizedStringFromTable(@"YEAR", @"EventRecurrence", nil); break;
            case kRecurrenceNone:
                break;
        }
    }
    return [NSString stringWithFormat:FXLLocalizedStringFromTable(@"%d((lengthOfInterval))_%@((typeOfInterval))", @"EventRecurrence", @"Giving the length of interval and the type of interval as a string, eg. 5 months or 1 day"), self.interval, aString];
}

- (void)dayOfWeekFromString:(NSString*)aString
{
    if([aString isEqualToString:@"SU"]) {
        dayOfWeek |= kDayOfWeekSunday;
    }else if([aString isEqualToString:@"MO"]) {
        dayOfWeek |= kDayOfWeekMonday;
    }else if([aString isEqualToString:@"TU"]) {
        dayOfWeek |= kDayOfWeekTuesday;
    }else if([aString isEqualToString:@"WE"]) {
        dayOfWeek |= kDayOfWeekWednesday;
    }else if([aString isEqualToString:@"TH"]) {
        dayOfWeek |= kDayOfWeekThursday;
    }else if([aString isEqualToString:@"FR"]) {
        dayOfWeek |= kDayOfWeekFriday;
    }else if([aString isEqualToString:@"SA"]) {
        dayOfWeek |= kDayOfWeekSaturday;
    }
}

- (void)changedEvent:(Event*)anEvent
{
    if(self.occurences > 1 && self.untilDate) {
        FXDebugLog(kFXLogFIXME, @"FIXME EventRecurrence occurences and untilDate are mutually exclusive: %@", self);
    }
    
    Calendar* aCalendar = [Calendar calendar];
    self.startTimelessDate = [aCalendar timelessDate:anEvent.startDate];
    
    switch(self.recurrenceType) {
        case kRecurrenceDaily:
            break;
        case kRecurrenceWeekly:
            break;
        case kRecurrenceMonthly:
            self.dayOfMonth = [aCalendar dayOfMonth:anEvent.startDate];
            break;
        case kRecurrenceYearly:
            self.dayOfMonth  = [aCalendar dayOfMonth:anEvent.startDate];
            self.monthOfYear = [aCalendar monthOfYear:anEvent.startDate];
            break;
        case kRecurrenceMonthlyOnNthDay:
            break;
        case kRecurrenceYearlyOnNthDay:
            break;
        case kRecurrenceNone:
            break;
    }
    
    if(self.occurences > 1) {
        int anOccurences = self.occurences - 1;
        if(self.interval > 1) {
            anOccurences *= self.interval;
        }
        switch(self.recurrenceType) {
            case kRecurrenceDaily:
                self.endDate = [aCalendar addDays:anOccurences toDate:anEvent.endDate];
                break;
            case kRecurrenceWeekly:
                self.endDate = [aCalendar addWeeks:anOccurences toDate:anEvent.endDate];
                break;
            case kRecurrenceMonthly:
                self.endDate = [aCalendar addMonths:anOccurences toDate:anEvent.endDate];
                break;
            case kRecurrenceYearly:
                self.endDate = [aCalendar addYears:anOccurences toDate:anEvent.endDate];
                break;
            case kRecurrenceMonthlyOnNthDay:
#warning FIXME kRecurrenceMonthlyOnNthDay occurences
                self.endDate = [aCalendar addMonths:anOccurences toDate:anEvent.endDate];
                FXDebugLog(kFXLogFIXME, @"FIXME EventRecurrence occurences kRecurrenceMonthlyOnNthDay");
                break;
            case kRecurrenceYearlyOnNthDay:
#warning FIXME kRecurrenceYearlyOnNthDay occurences
                self.endDate = [aCalendar addYears:anOccurences toDate:anEvent.endDate];
                FXDebugLog(kFXLogFIXME, @"FIXME EventRecurrence occurences kRecurrenceYearlyOnNthDay");
                break;
            case kRecurrenceNone:
                // Shouldn't happen
                self.endDate = anEvent.endDate;
                break;
        }
        //FXDebugLog(kFXLogActiveSync, @"EventRecurrence changedEvent %@ %@ occurrences %d %@", anEvent.startDate, anEvent.endDate, self.occurences, self.endDate);
    }else if(self.untilDate) {
        self.endDate = self.untilDate;
    }else if(self.occurences == 1) {
        // Shouldn't happen
        self.endDate = anEvent.endDate;
    }else{
        // Recurs indefinitely, should I set this to nil or a very large date?
        self.endDate = nil;
    }
}

- (void)addException:(EventException*)anException
{
    if(!self.exceptions) {
        self.exceptions = [NSMutableArray arrayWithCapacity:1];
    }
    [self.exceptions addObject:anException];
}
    
////////////////////////////////////////////////////////////////////////////////
// JSON
////////////////////////////////////////////////////////////////////////////////
#pragma mark JSON

- (NSString*)asJSON
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:6];
    [aDictionary setObject:[NSNumber numberWithInt:self.recurrenceType]             forKey:kRecurrenceType];
    if(self.interval > 0) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.interval]       forKey:kRecurrenceInterval];
    }
    if(self.firstDayOfWeek > 0) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.firstDayOfWeek] forKey:kFirstDayOfWeek];
    }
    if(self.dayOfWeek > 0) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.dayOfWeek]      forKey:kDayOfWeek];
    }
    if(self.weekOfMonth != 0) {
        [aDictionary setObject:[NSNumber numberWithInt:self.weekOfMonth]            forKey:kWeekOfMonth];
    }
    if(self.monthOfYear) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.monthOfYear]    forKey:kMonthOfYear];
    }
    if(self.dayOfMonth > 0) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.dayOfMonth]     forKey:kDayOfMonth];
    }
    if(self.daysOfMonth.count > 0) {
        [aDictionary setObject:self.daysOfMonth                                     forKey:kDaysOfMonth];
    }
    if(self.occurences > 0) {
        [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.occurences]     forKey:kOccurences];
    }
    if(self.untilDate) {
        [aDictionary setObject:[[EventEngine dateFormatter] stringFromDate:self.untilDate]  forKey:kUntilDate];
    }
    
    // Exceptions
    if(self.exceptions.count > 0) {
        [aDictionary setObject:[EventException exceptionsAsJSON:self.exceptions] forKey:kExceptions];
    }
    
    return[aDictionary JSONRepresentation];
}

- (NSString*)asICal
{
    NSMutableString* aString = [NSMutableString string];
    
    // RRULE/Recurrence type
    //
    NSString* aTypeString;
    switch(self.recurrenceType) {
        case kRecurrenceDaily:
            aTypeString=@"DAILY";
            break;
        case kRecurrenceWeekly:
            aTypeString=@"WEEKLY";
            break;
        case kRecurrenceMonthly:
        case kRecurrenceMonthlyOnNthDay:
            aTypeString=@"MONTHLY";
            break;
        case kRecurrenceYearly:
        case kRecurrenceYearlyOnNthDay:
            aTypeString=@"YEARLY";
            break;
        case kRecurrenceNone:
            aTypeString=@"NONE";
            break;
    }
    [aString appendFormat:@"RRULE:FREQ=%@;", aTypeString];
    
    // Interval
    //
    if(self.interval > 1) {
        [aString appendFormat:@"INTERVAL=%d;", self.interval];
    }
    
    // Until
    //
    if(self.untilDate) {
        [aString appendFormat:@"UNTIL=%@;", [[Event iCalFormatterZulu] stringFromDate:self.untilDate]];
    }else if(self.occurences > 0) {
        [aString appendFormat:@"COUNT=%d;", self.occurences];
    }
    
    // Day Of Week
    //
    EDayOfWeek aDayOfWeek = self.dayOfWeek;
    if(aDayOfWeek) {
        NSMutableArray* aStrings = [NSMutableArray arrayWithCapacity:7];
        if(aDayOfWeek & kDayOfWeekMonday) {
            [aStrings addObject:@"MO"];
        }
        if(aDayOfWeek & kDayOfWeekTuesday) {
            [aStrings addObject:@"TU"];
        }
        if(aDayOfWeek & kDayOfWeekWednesday) {
            [aStrings addObject:@"WE"];
        }
        if(aDayOfWeek & kDayOfWeekThursday) {
            [aStrings addObject:@"TH"];
        }
        if(aDayOfWeek & kDayOfWeekFriday) {
            [aStrings addObject:@"FR"];
        }
        if(aDayOfWeek & kDayOfWeekSaturday) {
            [aStrings addObject:@"SA"];
        }
        if(aDayOfWeek & kDayOfWeekSunday) {
            [aStrings addObject:@"SU"];
        }
        NSUInteger aCount = aStrings.count;
        if(aCount > 0) {
            [aString appendString:@"BYDAY="];
            for(int i = 0 ; i < aCount ; ++i) {
                NSString* aDayString = [aStrings objectAtIndex:i];
                if(self.weekOfMonth != 0) {
                    if(i < aCount-1) {
                        [aString appendFormat:@"%d%@,", self.weekOfMonth, aDayString];
                    }else{
                        [aString appendFormat:@"%d%@;", self.weekOfMonth, aDayString];
                    }
                }else{
                    if(i < aCount-1) {
                        [aString appendFormat:@"%@,", aDayString];
                    }else{
                        [aString appendFormat:@"%@;", aDayString];
                    }
                }
            }
        }
    }
    
    // Day Of Month
    //
    if(self.dayOfMonth) {
        [aString appendFormat:@"BYMONTHDAY=%d;", self.dayOfMonth];
    }
    
    // Month of Year
    //
    if(self.monthOfYear) {
        [aString appendFormat:@"BYMONTH=%d;", self.monthOfYear];
    }
    
    // Day of Year
    //BYYEARDAY=1,100,200
    
    // Start of Week
    // WKST=MO
    
    // Trim trailing semicolon
    //
    NSRange aRange = NSMakeRange(aString.length-1, 1);
    [aString replaceOccurrencesOfString:@";" withString:@"" options:0 range:aRange];
    
    // New Line
    [aString appendString:@"\n"];
    
    if(self.exceptions.count > 0) {
        FXDebugLog(kFXLogFIXME, @"FIXME Exception iCal RECURRENCE-ID");
#warning FIXME iCal Exceptions
    }
    
    return aString;
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case EMAIL_RECURRENCE_TYPE:
            case CALENDAR_RECURRENCE_TYPE:
                self.recurrenceType = (ERecurrenceType)[aParser getUnsignedInt];
                break;
            case EMAIL_RECURRENCE_INTERVAL:
            case CALENDAR_RECURRENCE_INTERVAL:
                self.interval = [aParser getUnsignedInt];
                break;
            case CALENDAR_FIRST_DAY_OF_WEEK:
            case EMAIL2_FIRST_DAY_OF_WEEK:
                self.firstDayOfWeek = (EFirstDayOfWeek)[aParser getUnsignedInt];
                break;
            case EMAIL_RECURRENCE_DAYOFWEEK:
            case CALENDAR_RECURRENCE_DAYOFWEEK:
                self.dayOfWeek = (EDayOfWeek)[aParser getUnsignedInt];
                break;
            case EMAIL_RECURRENCE_WEEKOFMONTH:
            case CALENDAR_RECURRENCE_WEEKOFMONTH:
                self.weekOfMonth = [aParser getInt];
                break;
            case EMAIL_RECURRENCE_MONTHOFYEAR:
            case CALENDAR_RECURRENCE_MONTHOFYEAR:
                self.monthOfYear = [aParser getUnsignedInt];
                break;
            case EMAIL_RECURRENCE_DAYOFMONTH:
            case CALENDAR_RECURRENCE_DAYOFMONTH:
                self.dayOfMonth = [aParser getUnsignedInt];
                break;
            case EMAIL_RECURRENCE_UNTIL:
            case CALENDAR_RECURRENCE_UNTIL:
            {
                NSString* aString = [aParser getString];
                self.untilDate = [[EventEngine dateFormatter] dateFromString:aString];
                break;
            }
            case EMAIL_RECURRENCE_OCCURRENCES:
            case CALENDAR_RECURRENCE_OCCURRENCES:
                self.occurences = [aParser getUnsignedInt];
                break;
            case CALENDAR_CALENDAR_TYPE:
            {
                ECalendarType aCalendarType = (ECalendarType)[aParser getUnsignedInt];
                switch(aCalendarType) {
                    case kCalendarTypeDefault:
                    case kCalendarTypeGregorian:
                    case kCalendarTypeGregorianUS:
                        break;
                    default:
                        FXDebugLog(kFXLogFIXME, @"FIXME Unsupported CALENDAR_TYPE: %d", aCalendarType);
                        break;
                }
                break;
            }
            case CALENDAR_IS_LEAP_MONTH:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_IS_LEAP_MONTH: %@", [aParser getString]);
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
    NSMutableString* string = [NSMutableString stringWithString:@"Recurrence{\n"];
    
    // Recurrence Type
    //
    NSString* aString;
    switch(self.recurrenceType) {
        case kRecurrenceDaily:
            aString = @"daily"; break;
        case kRecurrenceWeekly:
            aString = @"weekly"; break;
        case kRecurrenceMonthly:
            aString = @"monthly"; break;
        case kRecurrenceYearly:
            aString = @"yearly"; break;
        case kRecurrenceMonthlyOnNthDay:
            aString = @"monthlyOnNthDay"; break;
        case kRecurrenceYearlyOnNthDay:
            aString = @"yearlyOnNthDay"; break;
        case kRecurrenceNone:
            aString = @"none"; break;
    }
    [self writeString:string        tag:kRecurrenceType     value:aString];
    
    // Interval
    if(self.interval > 0) {
        [self writeUnsigned:string  tag:@"interval"         value:self.interval];
    }
    
    // First day of week
    //
    switch(self.firstDayOfWeek) {
        case kFirstDayOfWeekMonday:
            aString = @"Monday"; break;
            break;
        case kFirstDayOfWeekTuesday:
            aString = @"Tuesday"; break;
            break;
        case kFirstDayOfWeekWednesday:
            aString = @"Wednesday"; break;
            break;
        case kFirstDayOfWeekThursday:
            aString = @"Thursday"; break;
            break;
        case kFirstDayOfWeekFriday:
            aString = @"Friday"; break;
            break;
        case kFirstDayOfWeekSaturday:
            aString = @"Saturday"; break;
            break;
        case kFirstDayOfWeekSunday:
            aString = @"Sunday"; break;
            break;
    }
    [self writeString:string  tag:@"firstDayOfWeek"         value:aString];

    // Day of Week
    //
    if(self.dayOfWeek != 0x00) {
        NSMutableString* aString = [NSMutableString stringWithString:@""];

        if(self.dayOfWeek  == kDayOfWeekLastDayOfMonth) {
            [aString appendString:@"Last Day Of Month"];
        }else{
            if(self.dayOfWeek & kDayOfWeekMonday)           [aString appendString:@"Monday "];
            if(self.dayOfWeek & kDayOfWeekTuesday)          [aString appendString:@"Tuesday "];
            if(self.dayOfWeek & kDayOfWeekWednesday)        [aString appendString:@"Wednesday "];
            if(self.dayOfWeek & kDayOfWeekThursday)         [aString appendString:@"Thursday "];
            if(self.dayOfWeek & kDayOfWeekFriday)           [aString appendString:@"Friday "];
            if(self.dayOfWeek & kDayOfWeekSaturday)         [aString appendString:@"Saturday "];
            if(self.dayOfWeek & kDayOfWeekSunday)           [aString appendString:@"Sunday "];
        }
        [self writeString:string  tag:@"dayOfWeek"          value:aString];
    }
    
    if(self.weekOfMonth != 0) {
        [self writeInt:string  tag:@"weekOfMonth"           value:self.weekOfMonth];
    }
    if(self.monthOfYear) {
        [self writeUnsigned:string  tag:@"monthOfYear"      value:self.monthOfYear];
    }
    if(self.dayOfMonth > 0) {
        [self writeUnsigned:string  tag:@"dayOfMonth"       value:self.dayOfMonth];
    }
    if(self.daysOfMonth.count > 0) {
        [self writeString:string    tag:@"daysOfMonth"      value:@"..."];
    }
    
    // Until - occurences and until should be mutually exclusive
    //
    if(self.occurences > 0) {
        [self writeUnsigned:string  tag:@"occurences"       value:self.occurences];
    }
    if(self.untilDate) {
        [self writeString:string    tag:@"until"            value:[self.untilDate descriptionWithLocale:[NSLocale currentLocale]]];
    }
    
    if(self.exceptions.count > 0) {
        for(EventException* anException in self.exceptions) {
            [string appendString:[anException description]];
        }
    }

    [string appendString:@"}\n"];

	return string;
}

@end
