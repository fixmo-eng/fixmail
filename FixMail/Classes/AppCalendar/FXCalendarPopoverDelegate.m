//
//  FXCalendarPopoverDelegate.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-13.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarPopoverDelegate.h"

@implementation FXCalendarPopoverDelegate


- (UIPopoverArrowDirection)popoverDirection:(CGRect)aRect inView:(UIView*)inView
{
    UIPopoverArrowDirection aDirection;
    if(aRect.origin.x > inView.bounds.size.width * 0.5f) {
        aDirection = UIPopoverArrowDirectionRight;
    }else{
        aDirection = UIPopoverArrowDirectionLeft;
    }
    return aDirection;
}


- (UIPopoverController*)createPopover:(CGRect)aRect viewController:(UIViewController*)aViewController popoverDelegate:(id<UIPopoverControllerDelegate>) popoverDelegate presentingView:(UIView*)view
{
    UIPopoverController * popover = [[UIPopoverController alloc] initWithContentViewController:aViewController];
    popover.delegate = popoverDelegate;
    popover.popoverContentSize = CGSizeMake(320,480);
    
    aRect.size.width    = 10.0f;
    aRect.size.height   = 10.0f;
    
    UIPopoverArrowDirection aDirection = [self popoverDirection:aRect inView:view];
    [popover presentPopoverFromRect:aRect
                                  inView:view
                permittedArrowDirections:aDirection
                                animated:TRUE];
    return popover;
}


@end
