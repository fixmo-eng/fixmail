 //
//  FXCalendarListController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-17.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarListViewController.h"
#import "ASCalendarFolder.h"
#import "CalendarController.h"
#import "EventEngine.h"
#import "DateUtil.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "EventEditController.h"
#import "FXMeetingViewController.h"
#import "SZCalendarListTableViewCell.h"

@interface FXCalendarListViewController()
@property (nonatomic,assign) float lastVerticalContentOffset;
@property (nonatomic,assign) BOOL isExtendingData;

@end

@implementation FXCalendarListViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    if ((self = [super initWithStyle:style]))
    {
        [self.tableView registerClass:[SZCalendarListTableViewCell class] forCellReuseIdentifier:@"FXCalendarListViewController"];
    }
    return self;
}

-(void) dealloc   
{
    [self.folder removeDelegate:self];
}

#pragma mark View Lifecycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.folder addDelegate:self];
    self.tableView.showsVerticalScrollIndicator = NO;
    
    NSDate* startDate = [Calendar.calendar addDays:-7 toDate:self.currentDate];
    [EventEngine.engine eventCacheExtendToDate:startDate folder:self.folder];
    [self.tableView reloadData];
    
    if ([self.tableView numberOfSections])
        [self scrollToDate:self.currentDate];

    __weak FXCalendarListViewController *weakSelf = self;
    
    // setup infinite scrolling
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowsAtBottom];
    }];
}

-(void) setCurrentDate:(NSDate *)currentDate
{
    _currentDate = [[Calendar calendar] timelessDate:currentDate];
}

-(void)scrollToDate:(NSDate*)date
{
    NSDate* timelessDate = [[Calendar calendar] timelessDate:date];

    NSInteger section = [[EventEngine.engine sortedKeys] indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([timelessDate isEqualToDate:obj])
        {
            *stop = YES;
            return YES;
        }
        else
            return NO;
            
    }];
    if (section != NSNotFound)
    {
        NSIndexPath* ip = [NSIndexPath indexPathForRow:0 inSection:section];
        [self.tableView scrollToRowAtIndexPath:ip atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
}

-(void)insertRowsAtBottom
{
    
    self.isExtendingData = YES;
    __weak FXCalendarListViewController *weakSelf = self;
    
    NSDate* endDate = [Calendar.calendar addDays:7 toDate:[EventEngine.engine endDate]];
    
    [EventEngine.engine eventCacheExtendToDate:endDate folder:weakSelf.folder completion:^{
        [weakSelf.tableView reloadData];
        self.isExtendingData = NO ;
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
        
    }];
    


}

-(void)insertRowsAtTop
{
    __weak FXCalendarListViewController *weakSelf = self;
    self.isExtendingData = YES ;

    
    NSDate* startDate = [Calendar.calendar addDays:-7 toDate:[EventEngine.engine startDate]];
    
    __block  NSInteger oldSectionCount = [[EventEngine.engine sortedKeys] count];
    
    [EventEngine.engine eventCacheExtendToDate:startDate folder:weakSelf.folder completion:^{
        
        [weakSelf.tableView.infiniteScrollingView stopAnimating];
        NSArray* array = [EventEngine.engine sortedKeys];
        NSInteger numberNewSections = [array count] - oldSectionCount;
        NSInteger numberOfNewRows = 0;
        
        for (NSInteger index =0; index < numberNewSections; index++)
        {
            numberOfNewRows += [[EventEngine.engine eventCacheEventsForDate:array[index] ] count];
        }
                
        [weakSelf.tableView reloadData];
        CGPoint newOffset = weakSelf.tableView.contentOffset;
        newOffset.y += (float)(numberNewSections * 20 + numberOfNewRows * 44);
        weakSelf.tableView.contentOffset = newOffset;
        self.isExtendingData = NO ;

    }];
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    NSIndexPath* ipath = [self.tableView indexPathsForVisibleRows][0];
    NSDate* sectionDate = [[EventEngine.engine sortedKeys] objectAtIndex:ipath.section];
    _currentDate = sectionDate;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    
    if (!self.isExtendingData)
    {
    
        float threshold = self.tableView.contentSize.height * 0.20;
     
        if (self.tableView.contentOffset.y > _lastVerticalContentOffset)
        {
            //scrolling down
            if (self.tableView.contentOffset.y > self.tableView.contentSize.height-threshold)
            {
                [self insertRowsAtBottom];
            }

        }
        else
        {
            if (self.tableView.contentOffset.y <
                threshold)
            {
                [self insertRowsAtTop];
            }

        }
    }
    _lastVerticalContentOffset = self.tableView.contentOffset.y;
    
    if (self.updateDelegate && [self.updateDelegate respondsToSelector:@selector(didUpdateToDate:)])
    {

        NSArray* ips = [self.tableView indexPathsForVisibleRows];
        NSIndexPath* ipath = ips[0];
        NSDate* sectionDate = [[EventEngine.engine sortedKeys] objectAtIndex:ipath.section];
        [self.updateDelegate didUpdateToDate:sectionDate];
    }
}


-(void) viewWillDisappear
{
    [self.folder removeDelegate:self];
}

-(void) setFolder:(ASCalendarFolder*)inFolder
{
    _folder = inFolder;
    [_folder addDelegate:self];
}

#pragma mark FolderDelegate

-(void)folderSyncing:(BaseFolder *)aFolder
{
    
}

-(void)folderSynced:(BaseFolder *)aFolder
{
 
    [self.tableView reloadData];
}

-(void)newObjects:(NSArray *)anObjects
{
    [self.tableView reloadData];
}

-(void)changeObject:(BaseObject *)anObject
{
     [self.tableView reloadData];
}

-(void)deleteUid:(NSString *)aUid
{
     [self.tableView reloadData];
}


-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate* sectionData = [[EventEngine.engine sortedKeys] objectAtIndex:indexPath.section];
    Event* eventData = [[EventEngine.engine eventCacheEventsForDate:sectionData] objectAtIndex:indexPath.row];
    SZCalendarListTableViewCell* cell = (SZCalendarListTableViewCell*)[aTableView dequeueReusableCellWithIdentifier:@"FXCalendarListViewController"];
    cell.event = eventData;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDate* sectionKey = [[EventEngine.engine sortedKeys] objectAtIndex:section];
    return [[EventEngine.engine eventCacheEventsForDate:sectionKey] count];

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[EventEngine.engine sortedKeys] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDate* sectionKey = [[EventEngine.engine sortedKeys] objectAtIndex:section];
    return [DateUtil.getSingleton stringFromDateOnlyMedium:sectionKey];

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.updateDelegate && [self.updateDelegate respondsToSelector:@selector(didSelectEvent:)])
    {
        NSDate* sectionData = [[EventEngine.engine sortedKeys] objectAtIndex:indexPath.section];
        Event* eventData = [[EventEngine.engine eventCacheEventsForDate:sectionData] objectAtIndex:indexPath.row];
        [self.updateDelegate didSelectEvent:eventData];
    }

}


@end
