//
//  FXCalendarPhoneListViewController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-26.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarPhoneListViewController.h"
#import "CalendarController.h"
#import "DateUtil.h"
#import "EventEngine.h"
#import "FXMeetingViewController.h"
#import "EventEditController.h"

@interface FXCalendarPhoneListViewController ()

@end

@implementation FXCalendarPhoneListViewController
@synthesize firstDayOfTheWeek;

+(FXCalendarPhoneListViewController*) controller
{
    FXCalendarPhoneListViewController* aController = [[FXCalendarPhoneListViewController alloc] initWithStyle:UITableViewStylePlain];
    aController.tableView.dataSource = aController;
    aController.tableView.delegate = aController;
    return aController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if(self.navigationController) {
        [[CalendarController controller] topToolbar:self.navigationItem];
        
        // Bottom tool bar
        //
        if(IS_IPAD()) {
            NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
            self.toolbarItems = aToolBarItems;
        }else{
            UIDeviceOrientation anOrientation = [UIDevice currentDevice].orientation;
            switch(anOrientation) {
                case UIDeviceOrientationLandscapeRight:
                case UIDeviceOrientationLandscapeLeft:
                {
                    self.navigationController.toolbarHidden = YES;
                    break;
                }
                case UIDeviceOrientationPortrait:
                case UIDeviceOrientationPortraitUpsideDown:
                default:
                {
                    self.navigationController.toolbarHidden = NO;
                    NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
                    self.toolbarItems = aToolBarItems;
                    break;
                }
            }
        }
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[CalendarController controller] calendarSelector:self.navigationItem];
    
}


#pragma mark CalendarDelegate

- (void)prev
{
    //no-op
}

- (void)next
{
    //no-op
}

- (void)today
{
    [self setDate:[NSDate date]];
}

- (void)addEvent
{
    
    NSDate* addedDate = self.currentDate;
    
    if (!addedDate)
    {
        addedDate = [NSDate date];
    }
    
    NSInteger unitFlags = (NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit);
    NSDateComponents* dateComponents = [[[DateUtil getSingleton] currentCalendar] components:unitFlags fromDate: addedDate];
    
    NSDate* aDate = [[[DateUtil getSingleton] currentCalendar] dateFromComponents:dateComponents];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:self.folder date:aDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    [self.navigationController presentViewController:nc animated:YES completion:nil];
    
}

- (void)editEvent:(Event*)anEvent
{
    
}

- (void)setDate:(NSDate*)aDate
{
    self.currentDate = aDate;
    [self scrollToDate:self.currentDate];
}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
    ECalendarViewType aCalendarViewType = (ECalendarViewType)aSegmentedControl.selectedSegmentIndex;
    [[CalendarController controller] setCalendarViewControllerOfViewType:aCalendarViewType
                                                                 forDate:self.currentDate
                                                                  folder:self.folder
                                                                    size:self.view.bounds.size
                                                    navigationController:self.navigationController];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDate* sectionData = [[EventEngine.engine sortedKeys] objectAtIndex:indexPath.section];
    
    Event* eventData = [[EventEngine.engine eventCacheEventsForDate:sectionData] objectAtIndex:indexPath.row];
    FXMeetingViewController* aMeetingVC = [FXMeetingViewController controllerWithEvent:eventData];
    [self.navigationController pushViewController:aMeetingVC animated:YES];
    
}



@end
