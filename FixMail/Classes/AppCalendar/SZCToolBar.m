//
//  SZCToolBar.m
//  FixMail
//
//  Created by Sean Langley on 2013-05-23.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "SZCToolBar.h"

@implementation SZCToolBar

- (void)drawRect:(CGRect)rect
{
    [[UIColor clearColor] set]; // or clearColor etc
    CGContextFillRect(UIGraphicsGetCurrentContext(), rect);
}

@end
