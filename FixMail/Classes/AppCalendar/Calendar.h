/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "Event.h"
#import "NSDate+TKCategory.h"

@class DateInfo;

// Types
typedef enum
{
    kCalendarTypeDefault                        = 0,
    kCalendarTypeGregorian                      = 1,
    kCalendarTypeGregorianUS                    = 2,
    kCalendarTypeJapenseEmperor                 = 3,
    kCalendarTypeTaiwan                         = 4,
    kCalendarTypeKoreanTangun                   = 5,
    kCalendarTypeHijri                          = 6,
    kCalendarTypeThai                           = 7,
    kCalendarTypeHebrewLunar                    = 8,
    kCalendarTypeGregorianMiddleEastFrench      = 9,
    kCalendarTypeGregorianArabic                = 10,
    kCalendarTypeGregorianTransliteratedEnglish = 11,
    kCalendarTypeGregorianTransliteratedFrench  = 12,
    kCalendarTypeReserved1                      = 13,
    kCalendarTypeJapenseLunar                   = 14,
    kCalendarTypeChineseLunar                   = 15,
    kCalendarTypeSakaEraReserved                = 16,
    kCalendarTypeChineseLunarReserved           = 17,
    kCalendarTypeKoreanLunarReserved            = 18,
    kCalendarTypeJapaneseRokuyouReserved        = 19,
    kCalendarTypeKoreanLunar                    = 20,
    kCalendarTypeReserved2                      = 21,
    kCalendarTypeReserved3                      = 22,
    kCalendarTypeUmalQuraReserved               = 23
} ECalendarType;

typedef enum
{
    kCalendarList           = 0,
    kCalendarDayWeek        = 1,
    kCalendarMonth          = 2,
    kCalendarYear           = 3,
} ECalendarViewType;

@interface Calendar : BaseObject {
    NSCalendar*          calendar;
}

@property (nonatomic, strong) NSCalendar*   calendar;

// Factory
+ (Calendar*)calendar;

// Date Interface
- (void)dateInfo:(DateInfo*)aDateInfo forDate:(NSDate*)aDate;
- (EDayOfWeek)dayOfWeek:(NSDate*)aDate;
- (NSString*)dayOfWeekAsICAL:(NSDate*)aDate;
- (int)weekday:(NSDate*)aDate;
- (int)dayOfMonth:(NSDate*)aDate;
- (int)weekOfMonth:(NSDate*)aDate;
- (int)monthOfYear:(NSDate*)aDate;
- (int)year:(NSDate*)aDate;
- (int)weeksInMonth:(NSDate*)aDate;
- (int)lastDayOfMonthForDate:(NSDate*)aDate;

- (NSInteger)daysBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate;
- (NSInteger)weeksBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate;
- (NSInteger)monthsBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate;
- (NSInteger)yearsBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate;

// Interface
- (BOOL)doesEvent:(Event*)anEvent matchDate:(NSDate*)aDate;
- (BOOL)isDate:(NSDate*)aDate1 sameDayAsDate:(NSDate*)aDate2;
- (BOOL)doesEvent:(Event*)anEvent matchDateInfo:(DateInfo*)aDateInfo;

- (NSDate*)newYearsDayForDate:(NSDate*)aDate;
- (NSDate*)timelessDate:(NSDate*)aDate;
- (NSDateComponents*)datelessTimeComponents:(NSDate*)aDate;

- (NSDate*)addToDate:(NSDate*)aDate hours:(int)anHours minutes:(int)aMinutes;
- (NSDate*)addDays:(int)aDays toDate:(NSDate*)aDate;
- (NSDate*)addWeeks:(int)aWeeks toDate:(NSDate*)aDate;
- (NSDate*)addMonths:(int)aMonths toDate:(NSDate*)aDate;
- (NSDate*)addYears:(int)aYears toDate:(NSDate*)aDate;
- (NSDate*)dateByAddingComponents:(NSDateComponents*)aComponents toDate:(NSDate*)aDate;

- (NSDate*)firstOfMonthForDate:(NSDate*)aDate;
- (NSDate*)lastOfMonthForDate:(NSDate*)aDate;
- (NSDate*)lastOfYearForDate:(NSDate*)aDate;
- (NSDate*)nextMonthForDate:(NSDate*)aDate;
- (NSDate*)previousMonthForDate:(NSDate*)aDate;

- (TKDateInformation)dateInformationFromDate:(NSDate*)aDate;
- (NSDate*)dateFromDateInformation:(TKDateInformation)info;

- (NSDate*)dateFromSystemTime:(SystemTime*)aSystemTime;

@end
