/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

@class TKCalendarDayEventView;

@protocol CalendarMonthTouchDelegate <NSObject>

- (void)dayTapped:(int)aDay changeMonthInDirection:(int)aDirection touch:(UITouch*)aTouch;

- (BOOL)dateSelected:(NSDate*)aDate touch:(UITouch*)aTouch;
- (void)dateEntered:(NSDate*)aDate location:(CGPoint)aLocation;

- (void)eventViewTapped:(TKCalendarDayEventView*)anEventView location:(CGPoint)aLocation;
- (void)eventViewEntered:(TKCalendarDayEventView*)anEventView location:(CGPoint)aLocation;

@end
