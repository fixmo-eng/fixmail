//
//  FXCDayCell.m
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-04-29.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "FXCDayCell.h"
#import "DateUtil.h"

#define kWeekendBackgroundColor [UIColor colorWithHue:0 saturation:0 brightness:0.95 alpha:1.0]
#define kWeekdayBackgroundColor [UIColor whiteColor]
#define kLineColor [UIColor colorWithHue:0.000 saturation:0.000 brightness:0.6 alpha:1]
#define kTodayColor [UIColor colorWithRed:89.0f/255.0f green:142.0f/255.0f blue:223.0f/255.0f alpha:1]
#define kTodayTextColor [UIColor whiteColor]


#define kTextFont [UIFont systemFontOfSize:12]
#define kBoldTextFont [UIFont boldSystemFontOfSize:12]

#define kDrawingEventsInitialY  (14.0f + 6.0f + 14.0f) //text size, + 6 point gap + header size

@interface FXCDayCell ()
{
    CGGradientRef gradient;
}
@property (nonatomic,strong) NSArray* eventRects;

@end

@implementation FXCDayCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
		CGFloat colors[] =
		{
			97.0f/255.0f, 159.0f/255.0f, 206.0f/255.0f, 1.00,
            111.0f/255.0f,  182.0f/255.0f, 238.0f/255.0f, 1.00,
		};
		gradient = CGGradientCreateWithColorComponents(rgb, colors, NULL, sizeof(colors)/(sizeof(colors[0])*4));
		CGColorSpaceRelease(rgb);
        _selectedIndex = 999999;
    }
    return self;
}

-(void) setEvents:(NSArray *)events
{
    NSMutableArray* eventRects = [NSMutableArray arrayWithCapacity:events.count];
    _events = events;
    
    float y = kDrawingEventsInitialY;
    for (id object in _events)
    {
        CGRect rect = CGRectMake(6.0f,y,self.frame.size.width - 12.0f, 14.0f );
        [eventRects addObject:[NSValue valueWithCGRect:rect]];
        y+= 14.0f;
    }
    _eventRects = [NSArray arrayWithArray:eventRects];
}

-(id) eventObjectAtPoint:(CGPoint)point
{
    for (NSValue* rectVal in self.eventRects)
    {
        CGRect rect = [rectVal CGRectValue];
        if (CGRectContainsPoint(rect, point))
        {
            NSInteger idx = [self.eventRects indexOfObject:rectVal];
            self.selectedIndex = idx;
            [self setNeedsDisplay];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                self.selectedIndex = 99999;
                [self setNeedsDisplay];
            });

            return self.events[idx];
        }
    }
    return nil;
}

-(void)dealloc
{
    if (gradient)
    {
        CGGradientRelease(gradient);
    }
}

-(void) prepareForReuse
{
    [super prepareForReuse];
    self.dayOfWeek = nil;
    self.number = 0;
    self.weekend = NO;
    self.placeHolder = NO;
    self.today = NO;
    self.events = nil;
    self.selectedIndex = 99999;
}

- (void)drawRect:(CGRect)rect
{
    NSString* str;
    if (self.dayOfWeek)
    {
        str = [NSString stringWithFormat:@"%@ %d",self.dayOfWeek, self.number];
    }
    else
    {
        str = [NSString stringWithFormat:@"%d",self.number];
    }

    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(ctx, kLineColor.CGColor);

    if (self.weekend)
    {
        CGContextSetFillColorWithColor(ctx, kWeekendBackgroundColor.CGColor);
    }
    else
    {
        CGContextSetFillColorWithColor(ctx, [UIColor whiteColor].CGColor);
    }
    CGContextFillRect(ctx, rect);
    
    
    CGContextSetStrokeColorWithColor(ctx, kLineColor.CGColor);
    CGContextStrokeRect(ctx, rect);

    CGSize textSize = [str sizeWithFont:kTextFont];

    CGContextSetFillColorWithColor(ctx, kLineColor.CGColor);

    //Draw Today info with gradient behind it.
    if (self.today)
    {
        CGContextSaveGState(ctx);

        CGRect backRect = CGRectMake(self.bounds.origin.x + 1, self.bounds.origin.y + 1, self.bounds.size.width - 2, textSize.height + 6 + 6);
        CGContextSetFillColorWithColor(ctx, kTodayColor.CGColor);
        CGContextFillRect(ctx,backRect);
        
        
        CGContextSetFillColorWithColor(ctx, kTodayTextColor.CGColor);
        
        CGContextClipToRect(ctx, backRect);
        CGContextDrawLinearGradient(ctx, gradient, CGPointMake(backRect.origin.x, backRect.origin.y), CGPointMake(backRect.origin.x, backRect.origin.y + backRect.size.height), 0);
        
        CGContextSetShadowWithColor(ctx, CGSizeMake(0,0), 2, [UIColor blackColor].CGColor);
        
        [str drawInRect:CGRectMake(self.bounds.size.width - 6 - textSize.width, 6, textSize.width, textSize.height) withFont:kTextFont];
        textSize = [@"Today" sizeWithFont:kBoldTextFont];
        [@"Today" drawInRect:CGRectMake(6, 6, textSize.width, textSize.height) withFont:kBoldTextFont];

        CGContextSetStrokeColorWithColor(ctx, kTodayColor.CGColor);
        CGContextStrokeRect(ctx,backRect);
        CGContextRestoreGState(ctx);

    }
    else
    {
        //Draw date info with no gradient.
        [str drawInRect:CGRectMake(self.bounds.size.width - 6 - textSize.width, 6, textSize.width, textSize.height) withFont:kTextFont];
    }
    
    float y = 14.0f + 6.0f + 12.0f;
    NSInteger idx = 0;
    while (idx < [self.events count])
    {
        id object = self.events[idx];
        CGRect rect = [self.eventRects[idx] CGRectValue];
        if (self.selectedIndex == idx)
        {
            CGContextSaveGState(ctx);
            CGContextSetFillColorWithColor(ctx, [UIColor colorWithWhite:0.905 alpha:1.000].CGColor);
            CGContextFillRect(ctx, rect);
            CGContextRestoreGState(ctx);
        }
        NSDate* time = [object performSelector:@selector(startDate)];
        NSString* timeStr =[DateUtil.getSingleton stringFromDateTimeOnlyShort:time];
        NSString* str = [NSString stringWithFormat:@"• %@-%@",timeStr,[object valueForKey:@"title"]];
        [str drawAtPoint:rect.origin forWidth:self.frame.size.width - 12 withFont:kTextFont minFontSize:12.0f actualFontSize:nil lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentNone];
        y+= 14.0f;
        idx++;
    }
}

@end
