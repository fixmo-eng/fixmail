/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "MeetingRequest.h"
#import "ASAccount.h"
#import "Attendee.h"
#import "CalendarEmail.h"
#import "EmailProcessor.h"
#import "EventRecurrence.h"
#import "FastWBXMLParser.h"
#import "NSData+Base64.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation MeetingRequest

// Properties
@synthesize startDate;
@synthesize endDate;
@synthesize timeStamp;

@synthesize location;
@synthesize organizer;
@synthesize globalObjectId;

@synthesize meetingMessageType;
@synthesize instanceType;
@synthesize sensitivity;
@synthesize busyStatus;

@synthesize reminderMinutes;
@synthesize recurrence;

// Flags Stored
static const NSUInteger kFlagsStoredMask    = 0x0000ffff;

static const NSUInteger kEventIsAllDay      = 0x00000001;
static const NSUInteger kResponseRequested  = 0x00000002;
static const NSUInteger kDisallowNewTime    = 0x00000004;

static const NSUInteger kIsRequest          = 0x00000010;
static const NSUInteger kIsCanceled         = 0x00000020;
static const NSUInteger kIsAccepted         = 0x00000040;
static const NSUInteger kIsTentativeAccepted= 0x00000080;
static const NSUInteger kIsDeclined         = 0x00000100;

// Constants
static NSString* kVCalUidString             = @"vCal-Uid";
static NSString* kStartDate                 = @"startDate";
static NSString* kEndDate                   = @"endDate";
static NSString* kTimeStamp                 = @"timeStamp";

static NSString* kLocation                  = @"location";
static NSString* kOrganizer                 = @"organizer";
static NSString* kGlobalObjectId            = @"globalObjectId";

static NSString* kMeetingMessageType        = @"meetingMessageType";
static NSString* kInstanceType              = @"instanceType";
static NSString* kSensitivity               = @"sensitivity";
static NSString* kBusyStatus                = @"busyStatus";
static NSString* kReminderMinutes           = @"reminderMinutes";
static NSString* kFlags                     = @"flags";

static NSString* kRecurrence                = @"recurrence";

static NSString* kFixmo                     = @"Fixmo";


////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSDateFormatter* sDateFormatter = NULL;

+ (NSDateFormatter*)dateFormatter
{
    if(!sDateFormatter)  {
        // This the ActiveSync date format for meeting request, it seems to be different from calendar events
        //      Z is for Zulu (a.k.a. GMT)
        //      20120823T150000Z
        //
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sDateFormatter = [[NSDateFormatter alloc] init];
        [sDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
        [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sDateFormatter;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
    }
    return self;
}

- (id)initWithJSON:(NSString*)aJSON
{
    if (self = [super init]) {
        NSDictionary* aDictionary = [aJSON JSONValue];
        
        NSString* aStartDateString = [aDictionary objectForKey:kStartDate];
        if(aStartDateString.length > 0) {
            self.startDate = [[MeetingRequest dateFormatter] dateFromString:aStartDateString];
        }
        NSString* aEndDateString = [aDictionary objectForKey:kStartDate];
        if(aEndDateString.length > 0) {
            self.endDate = [[MeetingRequest dateFormatter] dateFromString:aEndDateString];
        }
        NSString* aTimeStampString = [aDictionary objectForKey:kTimeStamp];
        if(aTimeStampString.length > 0) {
            self.timeStamp = [[MeetingRequest dateFormatter] dateFromString:aTimeStampString];
        }
        
        self.location           = [aDictionary objectForKey:kLocation];
        self.organizer          = [aDictionary objectForKey:kOrganizer];
        self.globalObjectId     = [aDictionary objectForKey:kGlobalObjectId];
        
        self.meetingMessageType = [self unsignedIntFromDictionary:aDictionary key:kMeetingMessageType];
        self.instanceType       = [self unsignedIntFromDictionary:aDictionary key:kInstanceType];
        self.sensitivity        = [self unsignedIntFromDictionary:aDictionary key:kSensitivity];
        self.busyStatus         = [self unsignedIntFromDictionary:aDictionary key:kBusyStatus];
        self.reminderMinutes    = [self intFromDictionary:aDictionary         key:kReminderMinutes];
        flags                   = [self unsignedIntFromDictionary:aDictionary key:kFlags];
        
        NSString* aRecurrenceJSON = [aDictionary objectForKey:kRecurrence];
        if(aRecurrenceJSON.length > 0) {
            EventRecurrence* anEventRecurrence = [[EventRecurrence alloc] initWithJSON:aRecurrenceJSON];
            self.recurrence = anEventRecurrence; 
        }
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// JSON
////////////////////////////////////////////////////////////////////////////////
#pragma mark JSON

- (NSString*)asJSON
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:6];
    
    if(self.startDate) {
        [aDictionary setObject:[[MeetingRequest dateFormatter] stringFromDate:self.startDate]  forKey:kStartDate];
    }
    if(self.endDate) {
        [aDictionary setObject:[[MeetingRequest dateFormatter] stringFromDate:self.endDate]  forKey:kEndDate];
    }
    if(self.timeStamp) {
        [aDictionary setObject:[[MeetingRequest dateFormatter] stringFromDate:self.timeStamp]  forKey:kTimeStamp];
    }
    
    if(self.location) {
        [aDictionary setObject:self.location        forKey:kLocation];
    }
    if(self.organizer) {
        [aDictionary setObject:self.organizer       forKey:kOrganizer];
    }
    if(self.globalObjectId) {
        [aDictionary setObject:self.globalObjectId  forKey:kGlobalObjectId];
    }
    
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.meetingMessageType]     forKey:kMeetingMessageType];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.instanceType]           forKey:kInstanceType];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.sensitivity]            forKey:kSensitivity];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:self.busyStatus]             forKey:kBusyStatus];
    [aDictionary setObject:[NSNumber numberWithInt:self.reminderMinutes]                forKey:kReminderMinutes];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:flags]                       forKey:kFlags];

    if(self.recurrence) {
        NSString* aRecurrenceString = [self.recurrence asJSON];
        [aDictionary setObject:aRecurrenceString  forKey:kRecurrence];
    }
    return[aDictionary JSONRepresentation];
}


////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (NSString*)eventUid
{
    NSString* anEventUid = nil;
    
    NSData* aData       = [NSData dataFromBase64String:self.globalObjectId];
    NSData* aVCalData   = [NSData dataWithBytes:[kVCalUidString UTF8String] length:kVCalUidString.length];
    NSRange aRange      = [aData rangeOfData:aVCalData options:0 range:NSMakeRange(0, aData.length)];
    if(aRange.length > 0) {
        // Request is from something other than Exchange, like Google Calendar so we need to find the
        // UID from the VCalendar attacment
        //
        NSUInteger aLocation = aRange.location += (kVCalUidString.length + 4);
        aRange = NSMakeRange(aLocation, aData.length-aLocation-1);
        NSData* aVCalUid = [aData subdataWithRange:aRange];
        anEventUid = [[NSString alloc] initWithData:aVCalUid encoding:NSUTF8StringEncoding];
        //FXDebugLog(kFXLogActiveSync, @"eventUid vCal: %@", anEventUid);
    }else{
        // Debug print
        //
        //if(aData.length > 0) {
        //    const unsigned char* aBytes = (const unsigned char* )[aData bytes];
        //    for(int i = 0 ; i < aData.length ; ++i) {
        //        printf("%02x ", aBytes[i]);
        //    }
        //    printf("\n");
        //}
        
        // Request is from Exchange, convert bytes to hex
        //
        NSUInteger aLength = aData.length*2;
        NSMutableData* anASCIIData = [NSMutableData dataWithCapacity:aLength];
        anASCIIData.length = aLength;
        
        unsigned char* anASCII      = (unsigned char* )[anASCIIData mutableBytes];
        const unsigned char* aBytes = (const unsigned char* )[aData bytes];
        const char hex[16]          = "0123456789ABCDEF";
        int j = 0;
        for(int i = 0 ; i < aData.length ; ++i, j+=2) {
            unsigned char c = aBytes[i];
            anASCII[j]   = hex[c >> 4];
            anASCII[j+1] = hex[c & 0xf];
        }
        anEventUid = [[NSString alloc] initWithData:anASCIIData encoding:NSUTF8StringEncoding];
        //FXDebugLog(kFXLogCalendar, @"eventUid Exchange: %@", anEventUid);
    }
    
    return anEventUid;
}

- (NSString*)asICalForAccount:(BaseAccount*)anAccount subject:(NSString*)aSubject
{
    float anICalVersion         = 2.0f;
    NSString* aCalendarString   = @"GREGORIAN";
    
    NSMutableString* aString = [NSMutableString stringWithCapacity:2048];
    [aString appendString:@"BEGIN:VCALENDAR\n"];
    
    // Header
    //
    NSBundle* aBundle = [NSBundle mainBundle];
    NSString* aVersion  = [aBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* anAppName = [aBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    [aString appendFormat:@"PRODID:-//%@//%@ %@//%@\n", kFixmo, anAppName, aVersion, [[NSLocale preferredLanguages] objectAtIndex:0]];
    [aString appendFormat:@"VERSION:%3.1f\n",   anICalVersion];
    [aString appendFormat:@"CALSCALE:%@\n",     aCalendarString];
    
    // Method
    //
    NSString* aMethodString;
    EICalMethod aMethod = kICalMethodReply;
    switch(aMethod) {
        case kICalMethodRequest:
            aMethodString = @"REQUEST"; break;
        case kICalMethodPublish:
            aMethodString = @"PUBLISH"; break;
        case kICalMethodReply:
            aMethodString = @"REPLY";
            break;
        case kICalMethodCounter:
            aMethodString = @"COUNTER"; break;
        case kICalMethodCancel:
            aMethodString = @"CANCEL"; break;
    }
    [aString appendFormat:@"METHOD:%@\n",       aMethodString];
    
    // Time Zone
    //
#ifdef FEATURE_TIMEZONE
    TimeZoneInfo* aTimeZoneInfo = [self iCalTimeZone:aString];
#endif
    
    // Event
    //
    [aString appendString:@"BEGIN:VEVENT\n"]; {
        // Dates
        //
#ifdef FEATURE_TIMEZONE
        NSDateFormatter* aFormatter = [Event iCalFormatter];
        NSString* aDateString = [aFormatter stringFromDate:self.startDate];
        [aString appendFormat:@"DTSTART;TZID=%@:%@\n",  aTimeZoneInfo.name, aDateString];
        
        aDateString = [aFormatter stringFromDate:self.endDate];
        [aString appendFormat:@"DTEND;TZID=%@:%@\n",    aTimeZoneInfo.name, aDateString];
        
        aDateString = [aFormatter stringFromDate:[NSDate date]];
        [aString appendFormat:@"DTSTAMP;TZID=%@:%@\n",    aTimeZoneInfo.name, aDateString];
        
        // Current date isn't exactly right here
        //[aString appendFormat:@"CREATED;TZID=%@:%@\n",          aDateString];
        
        // Last modified
        //[aString appendFormat:@"LAST-MODIFIED;TZID=%@:%@\n",    aDateString];
#else
        NSDateFormatter* aFormatter = [Event iCalFormatterZulu];
        NSString* aDateString = [aFormatter stringFromDate:self.startDate];
        [aString appendFormat:@"DTSTART:%@\n", aDateString];
        
        aDateString = [aFormatter stringFromDate:self.endDate];
        [aString appendFormat:@"DTEND:%@\n",  aDateString];
        
        aDateString = [aFormatter stringFromDate:[NSDate date]];
        [aString appendFormat:@"DTSTAMP:%@\n",          aDateString];
        
        // Current date isn't exactly right here
        //[aString appendFormat:@"CREATED:%@\n",          aDateString];
        
        // Last modified
        //[aString appendFormat:@"LAST-MODIFIED:%@\n",    aDateString];
#endif
        
        // Recurrence
        //
        if (self.recurrence && self.recurrence.recurrenceType != kRecurrenceNone) {
            [aString appendString:[self.recurrence asICal]];
        }
        
        // Organizer
        //
        Attendee* anOrganizer = [[Attendee alloc] initWithString:self.organizer];
        [aString appendFormat:@"ORGANIZER;CN=\"%@\":mailto:%@\n", anOrganizer.name, anOrganizer.address];
        
        // UID
        //
        [aString appendFormat:@"UID:%@\n", self.eventUid];
        
        // Attendees
        //
        Attendee* anAttendee = [[Attendee alloc] initWithName:anAccount.name address:anAccount.emailAddress];
        if([self isAccepted]) {
            anAttendee.attendeeStatus = kAttendeeResponseAccept;
        }else if([self isDeclined]) {
            anAttendee.attendeeStatus = kAttendeeResponseDecline;
        }else if([self isTentativeAccepted]) {
            anAttendee.attendeeStatus = kAttendeeResponseTentative;
        }

        [aString appendString:[anAttendee asICal:FALSE]];
         
        // Description
        [aString appendFormat:@"DESCRIPTION:%@\n",  aSubject];

        // Summary
        [aString appendFormat:@"SUMMARY:%@\n",      aSubject];
    
        // Location
        //
        if(self.location.length > 0) {
            [aString appendFormat:@"LOCATION:%@\n",     self.location];
        }
        
        // Sequence
        //
        [aString appendFormat:@"SEQUENCE:%d\n",         1];             // NOTE: sequence hardwired, M$ doesn't put this in MeetingRequest
        
        // Status
        //
        NSString* aStatusString;
        EICalStatus aStatus = kICalStatusConfirmed;
        switch(aStatus) {
            case kICalStatusCancelled:
                aStatusString = @"CANCELLED"; break;
            case kICalStatusConfirmed:
                aStatusString = @"CONFIRMED"; break;
            case kICalStatusTentative:
                aStatusString = @"TENTATIVE"; break;
                
        }
        [aString appendFormat:@"STATUS:%@\n", aStatusString];
        
        [aString appendString:@"TRANSP:OPAQUE\n"];
        
        [aString appendString:@"CLASS:PUBLIC\n"];

        // Microsoft/ActiveSync specific
        //
        [aString appendString:@"X-MICROSOFT-CDO-OWNERAPPTID:2006263773\n"];
        [aString appendString:@"X-MICROSOFT-CDO-IMPORTANCE:1\n"];
        [aString appendString:@"X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE\n"];
        [aString appendString:@"X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\n"];
        [aString appendString:@"X-MICROSOFT-CDO-APPT-SEQUENCE:1\n"];
        [aString appendString:@"X-MICROSOFT-CDO-INSTTYPE:0\n"];
        [aString appendString:@"X-MICROSOFT-DISALLOW-COUNTER:FALSE\n"];
        if([self isAllDay]) {
            [aString appendString:@"X-MICROSOFT-CDO-ALLDAYEVENT:TRUE\n"];
        }else{
            [aString appendString:@"X-MICROSOFT-CDO-ALLDAYEVENT:FALSE\n"];
        }
    }
    [aString appendString:@"END:VEVENT\n"];
    
    [aString appendString:@"END:VCALENDAR\n"];
    
    //FXDebugLog(kFXLogActiveSync, @"ical: %@\n", aString);
    return [CalendarEmail iCalPostProcess:aString];
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parsers
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag messageClass:(NSString*)aMessageClass
{
    NSDateFormatter* aDateFormatter = [MeetingRequest dateFormatter];
    
    if([aMessageClass hasSuffix:@"Request"]) {
        [self setIsRequest:TRUE];
    }else if([aMessageClass hasSuffix:@"Canceled"]) {
        [self setIsCanceled:TRUE];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Schedule meeting unknown: %@", aMessageClass);
    }

    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
        // Dates
            case EMAIL_START_TIME:
            {
                NSString* aString = [aParser getString];
                self.startDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case EMAIL_END_TIME:
            {
                NSString* aString = [aParser getString];
                self.endDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case EMAIL_DTSTAMP:
            {
                NSString* aString = [aParser getString];
                self.timeStamp = [aDateFormatter dateFromString:aString];
                break;
            }
            case EMAIL_TIME_ZONE:
                // Not exactly sure what to do with this
                //
                /*NSString* aString = */[aParser getString];
                //[self _parseTimeZone:aString];
                break;
              
        // Strings
            case EMAIL_LOCATION:
                self.location = [aParser getString];
                break;
            case EMAIL_ORGANIZER:
                self.organizer = [aParser getString];
                break;
            case EMAIL_GLOBAL_OBJID:
                self.globalObjectId = [aParser getString];
                break;
            case EMAIL_REMINDER:
                self.reminderMinutes = [aParser getInt];
                break;
             
        // Recurrence
            case EMAIL_RECURRENCE_ID:
                FXDebugLog(kFXLogActiveSync, @"EMAIL_RECURRENCE_ID: %@", [aParser getString]);
                break;
            case EMAIL_RECURRENCES:
                break;
            case EMAIL_RECURRENCE:
            {
                EventRecurrence* anEventRecurrence = self.recurrence;
                if(!anEventRecurrence) {
                    anEventRecurrence = [[EventRecurrence alloc] init];
                    self.recurrence = anEventRecurrence; 
                }
                [anEventRecurrence parser:aParser tag:tag];
                break;
            }
   
            // Types
            case EMAIL2_MEETING_MESSAGE_TYPE:
                self.meetingMessageType = (EMeetingMessageType)[aParser getInt];
                break;
            case EMAIL_INSTANCE_TYPE:
                self.instanceType = (EInstanceType)[aParser getInt];
                break;
            case EMAIL_SENSITIVITY:
                self.sensitivity = (ESensitivity)[aParser getInt];
                break;
            case EMAIL_INTD_BUSY_STATUS:
                busyStatus = (EBusyStatus)[aParser getInt];
                break;
                
            // Flags
            case EMAIL_ALL_DAY_EVENT:
                [self setIsAllDay:[aParser getBool]];
                break;
            case EMAIL_RESPONSE_REQUESTED:
                [self setResponseRequested:[aParser getBool]];
                break;
            case EMAIL_DISALLOW_NEW_TIME_PROPOSAL:
                [self setDisallowNewTime:[aParser getBool]];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
    //FXDebugLog(kFXLogActiveSync, @"Meeting request: %@", self);
}

////////////////////////////////////////////////////////////////////////////////
// Flags Stored
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags

- (NSUInteger)flagsToStore
{
    return flags & kFlagsStoredMask;
}

- (BOOL)isAllDay
{
	return (flags & kEventIsAllDay) != 0;
}

- (void)setIsAllDay:(BOOL)state
{
	if(state)	flags |= kEventIsAllDay;
	else		flags &= ~kEventIsAllDay;
}

- (BOOL)responseRequested
{
	return (flags & kResponseRequested) != 0;
}

- (void)setResponseRequested:(BOOL)state
{
	if(state)	flags |= kResponseRequested;
	else		flags &= ~kResponseRequested;
}

- (BOOL)disallowNewTime
{
	return (flags & kDisallowNewTime) != 0;
}

- (void)setDisallowNewTime:(BOOL)state
{
	if(state)	flags |= kDisallowNewTime;
	else		flags &= ~kDisallowNewTime;
}

- (BOOL)isRequest
{
	return (flags & kIsRequest) != 0;
}

- (void)setIsRequest:(BOOL)state
{
	if(state)	flags |= kIsRequest;
	else		flags &= ~kIsRequest;
}

- (BOOL)isCanceled
{
	return (flags & kIsCanceled) != 0;
}

- (void)setIsCanceled:(BOOL)state
{
	if(state)	flags |= kIsCanceled;
	else		flags &= ~kIsCanceled;
}

- (BOOL)isAccepted
{
	return (flags & kIsAccepted) != 0;
}

- (void)setAccepted
{
	flags |= kIsAccepted;
    flags &= ~kIsTentativeAccepted;
	flags &= ~kIsDeclined;
}

- (BOOL)isTentativeAccepted
{
	return (flags & kIsTentativeAccepted) != 0;
}

- (void)setTentativeAccepted
{
    flags |= kIsTentativeAccepted;
    flags &= ~kIsAccepted;
	flags &= ~kIsDeclined;
}

- (BOOL)isDeclined
{
	return (flags & kIsDeclined) != 0;
}

- (void)setDeclined
{
	flags |= kIsDeclined;
    flags &= ~kIsAccepted;
	flags &= ~kIsTentativeAccepted;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
    // Dates
    [self writeString:string        tag:kStartDate              value:[self.startDate descriptionWithLocale:[NSLocale currentLocale]]];
    [self writeString:string        tag:kEndDate                value:[self.endDate   descriptionWithLocale:[NSLocale currentLocale]]];
    [self writeString:string        tag:kTimeStamp              value:[self.timeStamp   descriptionWithLocale:[NSLocale currentLocale]]];
    
    // Strings
    [self writeString:string        tag:kLocation               value:self.location];
    [self writeString:string        tag:kOrganizer              value:self.organizer];
    [self writeString:string        tag:kGlobalObjectId         value:self.globalObjectId];
    
    // Types
    NSString* aString;
    switch(self.instanceType) {
        case kSingleAppointment:
            aString = @"Single appointment"; break;
        case kMasterRecurringAppointment:
            aString = @"Master recurring"; break;
        case kSingleInstanceOfRecurring:
            aString = @"Single instance of recurring"; break;
        case kExceptionToRecurring:
            aString = @"Exception to recurring"; break;
    }
    [self writeString:string        tag:kInstanceType           value:aString];
    
    switch(self.meetingMessageType) {
        case kMeetingMessageTypeSilentUpdate:
            aString = @"Silent update"; break;
        case kMeetingMessageTypeInitialRequest:
            aString = @"Initial request"; break;
        case kMeetingMessageTypeFullUpdate:
            aString = @"Full update"; break;
        case kMeetingMessageTypeInformationalUpdate:
            aString = @"Informational update"; break;
        case kMeetingMessageTypeOutdated:
            aString = @"Outdated"; break;
        case kMeetingMessageTypeDelegatorCopy:
            aString = @"Master recurring"; break;
        case kMeetingMessageTypeDelegated:
            aString = @"Delegated"; break;

    }
    [self writeString:string        tag:kMeetingMessageType     value:aString];

    switch(self.sensitivity) {
        case kSensitivityNormal:
            aString = @"Normal"; break;
        case kSensitivityPersonal:
            aString = @"Personal"; break;
        case kSensitivityPrivate:
            aString = @"Private"; break;
        case kSensitivityConfidential:
            aString = @"Confidential"; break;
    }
    [self writeString:string        tag:kSensitivity            value:aString];
    
    switch(self.busyStatus) {
        case kBusyStatusFree:
            aString = @"Free"; break;
        case kBusyStatusTentative:
            aString = @"Tentative"; break;
        case kBusyStatusBusy:
            aString = @"Busy"; break;
        case kBusyStatusOutOfOffice:
            aString = @"OutOfOffice"; break;
    }
    [self writeString:string        tag:kBusyStatus             value:aString];

    // Int
    [self writeInt:string           tag:kReminderMinutes        value:self.reminderMinutes];

    // Flags
    if([self isAllDay]) {
        [self writeBoolean:string   tag:@"isAllDay"             value:TRUE];
    }
    if([self responseRequested]) {
        [self writeBoolean:string   tag:@"ResponseRequested"    value:TRUE];
    }
    if([self disallowNewTime]) {
        [self writeBoolean:string   tag:@"DisallowNewTime"      value:TRUE];
    }
    
    if([self isRequest]) {
        [self writeBoolean:string   tag:@"isRequest"            value:TRUE];
    }
    if([self isCanceled]) {
        [self writeBoolean:string   tag:@"isCanceled"           value:TRUE];
    }
    if([self isAccepted]) {
        [self writeBoolean:string   tag:@"isAccepted"           value:TRUE];
    }
    if([self isTentativeAccepted]) {
        [self writeBoolean:string   tag:@"isTentativeAccepted"  value:TRUE];
    }
    if([self isDeclined]) {
        [self writeBoolean:string   tag:@"isDeclined"           value:TRUE];
    }
    
    // Recurrence
    if(self.recurrence) {
        [string appendString:[self.recurrence description]];
    }
    
	[string appendString:@"}\n"];
	
	return string;
}

@end
