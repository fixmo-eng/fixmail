 /* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "FXCalendarDayViewController.h"
#import "ASCalendarFolder.h"
#import "BaseAccount.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "DateInfo.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventEditController.h"
#import "EventEngine.h"
#import "FXMeetingViewController.h"
#import "ProgressView.h"
#import "TKTimelineView.h"
#import "DateUtil.h"
#import "FXCalendarPopoverDelegate.h"

@interface FXCalendarDayViewController()
//This is a strong delegate on purpose
//it is a delegate created in init as opposed to be assigned from outside
@end

@implementation FXCalendarDayViewController

// Properties
@synthesize folder;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}


- (void)dealloc 
{
    folder = nil;
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_DAY_EXCEPTION", @"ErrorAlert", @"Calendar Day Controller error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CALENDAR_DAY", @"ErrorAlert", @"Calendar Day Controller error alert view title")  message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// View 
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        [self loadGestures];
        
        self.view.backgroundColor = [UIColor whiteColor];

    }@catch (NSException* e) {
        [self logException:@"viewDidLoad" exception:e];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        
    [self.calendarDayTimelineView configureForOrientation:self.interfaceOrientation
                                                    frame:self.view.frame
                                     navigationController:self.navigationController];
    [self.calendarDayTimelineView setupCustomInitialisation];
    
    [self setDate:[[CalendarController controller] date]];

    [self.view setNeedsDisplay];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    @try {
        [super viewWillDisappear:animated];
        
        self.navigationItem.titleView = nil;
    }@catch (NSException* e) {
        [self logException:@"viewWillDisappear" exception:e];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
//{
//    [self configureForOrientation:interfaceOrientation];
//}



////////////////////////////////////////////////////////////////////////////////
// Gestures
////////////////////////////////////////////////////////////////////////////////
#pragma mark Gestures

- (void)didSwipeLeft:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    @try {
        [self next];
    }@catch (NSException* e) {
        [self logException:@"didSwipeLeft" exception:e];
    }
}

- (void)didSwipeRight:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    @try {
        [self prev];
    }@catch (NSException* e) {
        [self logException:@"didSwipeRight" exception:e];
    }
}

- (void)loadGestures
{
    // Swipe
    //
    UISwipeGestureRecognizer* aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight:)];
    aSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
    aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft:)];
    aSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setFolder:(ASCalendarFolder*)aFolder
{
    if(folder) {
        [folder removeDelegate:self];
    }
    if(aFolder) {
        folder = aFolder;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (void)pushViewController:(UIViewController*)anViewController
{

    self.navigationController.navigationBarHidden = FALSE;
    [self.navigationController pushViewController:anViewController animated:YES];
}

////////////////////////////////////////////////////////////////////////////////
// CalendarDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark CalendarDelegate

- (void)prev
{
    @try {
        [self.calendarDayTimelineView prev:self];
    }@catch (NSException* e) {
        [self logException:@"prev" exception:e];
    }
}

- (void)next
{
    @try {
        [self.calendarDayTimelineView next:self];
    }@catch (NSException* e) {
        [self logException:@"next" exception:e];
    }
}

- (void)today
{
    @try {
        [self setDate:[NSDate date]];
    }@catch (NSException* e) {
        [self logException:@"today" exception:e];
    }
}

-(NSDate*)currentDate
{
    return self.calendarDayTimelineView.currentDay;
}

- (void)addEvent
{
    
    NSDate* addedDate = self.calendarDayTimelineView.currentDay;
    
    if (!addedDate)
    {
        addedDate = [NSDate date];
    }
    
    NSInteger unitFlags = (NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit);
    NSDateComponents* dateComponents = [[[DateUtil getSingleton] currentCalendar] components:unitFlags fromDate: addedDate];
    
    NSDate* aDate = [[[DateUtil getSingleton] currentCalendar] dateFromComponents:dateComponents];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:aDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nc animated:YES completion:nil];

}

- (void)editEvent:(Event*)anEvent
{
    EventEditController* anEventEditController = [EventEditController controllerForEvent:anEvent
                                                                         eventEditorType:kEventEditorChangeEvent
                                                                         dateForInstance:anEvent.startDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)setDate:(NSDate*)aDate
{
    [CalendarController controller].date = aDate;
    [self.calendarDayTimelineView setDate:aDate];
}


- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    TKCalendarDayTimelineView* aDayTimelineView = self.calendarDayTimelineView;

    if(aReloadEvents) {
        NSDate* aStartDate = aDayTimelineView.currentDay;
        NSDate* anEndDate   = [[Calendar calendar] addDays:aDayTimelineView.numDays toDate:aStartDate];
        [[EventEngine engine] eventCacheFromDate:aStartDate toDate:anEndDate folder:self.folder];
    }
    
    [aDayTimelineView reloadDays:FALSE/*setPosition*/];
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder;
{
}

- (void)folderSynced:(BaseFolder*)aFolder;
{
    [self rebuildWithReloadEvents:TRUE];
}

- (void)newObjects:(NSArray*)anObjects
{
    BOOL dayTimelineChanged = FALSE;
    
    [[EventEngine engine] eventCacheAddEvents:anObjects];

    TKCalendarDayTimelineView* aDayTimelineView = self.calendarDayTimelineView;
    [aDayTimelineView.lock lock];
    @try {
        Calendar* aCalendar = [Calendar calendar];
        NSDate* aDate = aDayTimelineView.currentDay;
        for(int i = 0 ; i < aDayTimelineView.numDays ; ++i) {
            DateInfo* aDateInfo = [[DateInfo alloc] initWithDate:aDate];
            for(Event* anEvent in anObjects) {
                if([aCalendar doesEvent:anEvent matchDateInfo:aDateInfo]) {
                    //FXDebugLog(kFXLogActiveSync, @"newObjects %@ start: %@ end: %@", aDayTimelineView.currentDay, anEvent.startDate, anEvent.endDate);
                    dayTimelineChanged = TRUE;
                    break;
                }
            }
            aDate = [aCalendar addDays:1 toDate:aDate];
        }
    }@catch (NSException* e) {
        [self logException:@"newObjects" exception:e];
    }
    [aDayTimelineView.lock unlock];
    if(dayTimelineChanged) {
        [self rebuildWithReloadEvents:FALSE];
    }
}

- (void)changeObject:(BaseObject*)aBaseObject
{
#if 1
    @try {
        Event* anEvent = (Event*)aBaseObject;
        
        // Delete all instances of object from event cache
        [[EventEngine engine] eventCacheDeleteUid:anEvent.uid];
        
        [[EventEngine engine] eventCacheAddEvents:[NSArray arrayWithObject:aBaseObject]];

        [self rebuildWithReloadEvents:FALSE];
    }@catch (NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
#else
    BOOL dayTimelineChanged = FALSE;

#warning FIXME This probably doesn't work. Should delete and readd like the month view does
    TKCalendarDayTimelineView* aDayTimelineView = self.calendarDayTimelineView;

    [aDayTimelineView.lock lock];
    @try {
        Event* anEvent = (Event*)aBaseObject;
        Calendar* aCalendar = [Calendar calendar];
        if(aDayTimelineView.currentDay) {
            BOOL isVisible = FALSE;
            NSArray* anEventViews = aDayTimelineView.events;
            for(TKCalendarDayEventView* anEventView in anEventViews) {
                Event* aVisibleEvent = [anEventView event];
                 if([anEvent.uid isEqualToString:aVisibleEvent.uid]) {
                     isVisible = TRUE;
                     break;
                 }
            }
            
            DateInfo* aDateInfo = [[DateInfo alloc] initWithDate:aDayTimelineView.currentDay];
            if([aCalendar doesEvent:anEvent matchDateInfo:aDateInfo]) {
                if(isVisible) {
                    // Not sure I need to do this, may just need to do a reload
                    //
                    NSArray* anEventViews =[aDayTimelineView.delegate calendarDayTimelineView:aDayTimelineView eventsForDate:aDayTimelineView.currentDay];
                    for(TKCalendarDayEventView* anEventView in anEventViews) {
                        Event* aVisibleEvent = [anEventView event];
                        if([anEvent.uid isEqualToString:aVisibleEvent.uid]) {
                            anEventView.title       = anEvent.title;
                            anEventView.location    = anEvent.location;
                            anEventView.startDate   = anEvent.startDate;
                            anEventView.endDate     = anEvent.endDate;
                            break;
                        }
                    }
                    dayTimelineChanged = TRUE;
                }else{
                    dayTimelineChanged = TRUE;
                }
            }else if(isVisible) {
                dayTimelineChanged = TRUE;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
    [aDayTimelineView.lock unlock];
    if(dayTimelineChanged) {
        [self rebuildWithReloadEvents:FALSE];
    }
#endif
}

- (void)deleteUid:(NSString*)aUid
{
    @try {
        [[EventEngine engine] eventCacheDeleteUid:aUid];
        
        [self rebuildWithReloadEvents:FALSE];
    }@catch (NSException* e) {
        [self logException:@"calendarDayTimelineView deleteUid" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDataSource

- (NSArray*)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventsForDate:(NSDate *)aDate
{
    NSMutableArray* aTKEvents = NULL;

    @try {
        // Range check to see if date is currently cached, if not load another month
        //
        EventEngine* anEngine = [EventEngine engine];
        [anEngine eventCacheExtendToDate:aDate folder:folder];
        
        // Build event views for all events on a given day in the data dictionary
        //
        NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
        NSArray* anEvents = [EventEngine.engine eventCacheEventsForDate:aTimelessDate];
        NSUInteger aCount = [anEvents count];
        if(anEvents && aCount > 0) {
            aTKEvents = [NSMutableArray arrayWithCapacity:aCount];
            for(int i = 0 ; i < aCount ; ++i) {
                Event* anEvent = [anEvents objectAtIndex:i];
                //FXDebugLog(kFXLogActiveSync, @"eventsForDate event: %@", anEvent);
                
                TKCalendarDayEventView *aTKEvent;
                aTKEvent = [TKCalendarDayEventView eventViewWithFrame:CGRectZero
                                                                event:anEvent
                                                            startDate:[anEvent startTimeForDate:aTimelessDate]
                                                              endDate:[anEvent endTimeForDate:aTimelessDate]
                                                                title:anEvent.title
                                                             location:anEvent.location
                                                           eventStyle:kEventStyleBalloon];
                [aTKEvents addObject:aTKEvent];
            }
        }
    }@catch (NSException* e) {
        [self logException:@"calendarDayTimelineView eventsForDate" exception:e];
    }

    return aTKEvents;
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDelegate

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView
{
    // Launch event editor/view for a specific event
    //
    [[CalendarController controller] setDate:eventView.startDate];
    Event* anEvent = eventView.event;
    FXMeetingViewController* aMeetingVC = [FXMeetingViewController controllerWithEvent:anEvent];
    if (IS_IPAD())
    {
        UINavigationController* nc = [[UINavigationController alloc]initWithRootViewController:aMeetingVC];
        nc.toolbarHidden = NO;
        
        CGRect newRect = CGRectOffset(eventView.frame, - calendarDayTimeline.scrollView.contentOffset.x
                                      , - calendarDayTimeline.scrollView.contentOffset.y +44);
        self.popover = [self.calendarPopoverDelegate createPopover:newRect viewController:nc popoverDelegate:self presentingView:calendarDayTimeline];
    }
    else
    {
        
        [self.navigationController pushViewController:aMeetingVC animated:YES];
    }
    
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline createEventForEventView:(TKCalendarDayEventView *)eventView
{
    // Launch event editor/view to create a new event
    //
    [[CalendarController controller] setDate:eventView.startDate];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:self.folder date:eventView.startDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nc animated:YES completion:nil];
}


- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventDateWasSelected:(NSDate*)anEventDate
{
    [[CalendarController controller] setDate:anEventDate];
    
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:anEventDate];
    Event* anEvent      = anEventEditController.event;
    anEvent.startDate   = anEventDate;
    anEvent.endDate     = [[Calendar calendar] addToDate:anEventDate hours:1 minutes:0];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nc animated:YES completion:nil];

}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popover = nil;
}


@end
