//
//  FXTableViewSubtitleCell.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-12.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXTableViewSubtitleCell.h"

@implementation FXTableViewSubtitleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
