//
//  FXCMonthViewController.m
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-04-29.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "FXCMonthViewController.h"
#import "FXCDayCell.h"
#import "DateUtil.h"
#import "OGiDateInformation.h"
#import "FXCMonthHeaderView.h"
#import "FXCCollectionView.h"

#define kNumberOfExtraPages 1

@interface FXCMonthViewController ()
@property (nonatomic,strong) NSDate* fromDate;
@property (nonatomic,strong) NSDate* toDate;

@property (nonatomic,strong) NSMutableArray* dataArray;

@end

@implementation FXCMonthViewController

#pragma mark • Init

- (instancetype)initWithCollectionViewLayout:(UICollectionViewLayout *)layout
{
    if ((self = [super initWithCollectionViewLayout:layout]))
    {
        
        [self scrollToDate:[NSDate date] animate:NO];

        _dataArray = [NSMutableArray arrayWithCapacity:[self numberOfSectionsInCollectionView:nil]];

    }
    return self;
}

+(instancetype)superController
{
    UICollectionViewFlowLayout* layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;

    
    id svc = [[[self class] alloc] initWithCollectionViewLayout:layout];
    FXCMonthViewController* vc = (FXCMonthViewController*)svc;
    vc.collectionView = [[FXCCollectionView alloc] initWithFrame:vc.collectionView.frame collectionViewLayout:layout];
    vc.collectionView.pagingEnabled = YES;
    vc.collectionView.dataSource = vc;
    vc.collectionView.delegate = vc;
    vc.collectionView.showsHorizontalScrollIndicator = NO;
    vc.collectionView.showsVerticalScrollIndicator = NO;
    vc.view.backgroundColor = [UIColor whiteColor];

    
    [vc.collectionView registerClass:[FXCDayCell class] forCellWithReuseIdentifier:@"FXCDayCell"];
    [vc.collectionView registerClass:[FXCMonthHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"FXCMonthHeaderView"];

    return svc;
}

#pragma mark • Lifecycle

-(void) resetCollectionViewBounds
{
    CGRect bounds = self.view.bounds;
    if (fmodf(bounds.size.width, 7) > 0)
    {
        bounds.size.width= floorf(bounds.size.width/7.0f) * 7.0f;
    }
    
    bounds = CGRectInset(bounds,7,7);
    
    self.collectionView.frame = bounds;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self resetCollectionViewBounds];
}

-(void)collectionViewWillLayoutSubviews:(FXCCollectionView *)collectionView
{
    if (self.collectionView.contentOffset.y < 0.0f) {
		[self appendPastDates];
	}
	
	if (self.collectionView.contentOffset.y > (self.collectionView.contentSize.height - CGRectGetHeight(self.collectionView.bounds))) {
		[self appendFutureDates];
	}
}

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
    UICollectionViewFlowLayout *cvLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    
	[cvLayout invalidateLayout];
    [self resetCollectionViewBounds];
	[cvLayout prepareLayout];
    [self.collectionView reloadData];
    
}

-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    UICollectionViewFlowLayout *cvLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;

	[cvLayout invalidateLayout];
    [self resetCollectionViewBounds];
	[cvLayout prepareLayout];
    [self.collectionView reloadData];


}



-(void) layoutViews
{
    [self resetCollectionViewBounds];
    UICollectionViewFlowLayout* lo = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    
    lo.minimumInteritemSpacing = 0;
    lo.minimumLineSpacing = 0;
}


-(void)viewDidLayoutSubviews
{
    [self scrollToDate:[NSDate date] animate:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark • Scrolling

-(void) refreshData
{
    [self scrollToDate:_currentDate animate:NO];
}

-(void)setCurrentDate:(NSDate *)currentDate
{
    _currentDate = currentDate;
    [self scrollToDate:_currentDate animate:NO];
}

-(void) scrollToDate:(NSDate*)date animate:(BOOL) animate
{
    NSDate *newDate = [[DateUtil.getSingleton currentCalendar] dateFromComponents:[[DateUtil.getSingleton currentCalendar]  components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:date]];
    _currentDate = newDate;

    NSDateComponents *components = [NSDateComponents new];
    components.month = -kNumberOfExtraPages;
    _fromDate = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:components toDate:newDate options:0];
    
    components.month = kNumberOfExtraPages;
    _toDate = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:components toDate:newDate options:0];

    [self requestEventData:nil];
    [self layoutViews];
    [self.collectionView reloadData];
    
    NSInteger section = [self numberOfSectionsInCollectionView:self.collectionView]/2;
    NSInteger lastItem = [self collectionView:self.collectionView numberOfItemsInSection:section] - 1;
    
    NSIndexPath * ip = [NSIndexPath indexPathForItem:lastItem inSection:section];
    
    [self.collectionView scrollToItemAtIndexPath:ip atScrollPosition:UICollectionViewScrollPositionBottom animated:animate];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSIndexPath* ip = [[self.collectionView indexPathsForVisibleItems] objectAtIndex:[[self.collectionView indexPathsForVisibleItems] count]-1];
    NSDate* date = [self dateForFirstDayInSection:ip.section];
    self.currentDate = [[DateUtil.getSingleton currentCalendar] dateFromComponents:[[DateUtil.getSingleton currentCalendar]  components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:date]];
}


#pragma mark • UICollectionView DataSource Delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize headerSize = [self collectionView:collectionView layout:collectionViewLayout referenceSizeForHeaderInSection:indexPath.section];
    
    CGRect collectionViewRect = self.collectionView.bounds;
    NSDate* date = [self dateForFirstDayInSection:indexPath.section];
    CGSize retSize = CGSizeMake(collectionViewRect.size.width/7.0f, (collectionViewRect.size.height-headerSize.height)/[date numberOfWeeksInMonth] );
    return retSize;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    //before, current, after
	return kNumberOfExtraPages + 1 + kNumberOfExtraPages;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSDate* date = [self dateForFirstDayInSection:section];
    NSInteger numberWeeks = [date numberOfWeeksInMonth];
    return 7 * numberWeeks;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(collectionView.bounds.size.width,100);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    FXCMonthHeaderView* hv = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"FXCMonthHeaderView" forIndexPath:indexPath];
    
    //We need something in the middle of the month, and not right at the beginning
    NSIndexPath* ip = [NSIndexPath indexPathForItem:7 inSection:indexPath.section];
    NSDate* date = [self dateForIndexPath:ip];

    hv.text = [DateUtil.getSingleton stringFromDateMonthAndYear:date];
    return hv;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FXCDayCell* cv = [collectionView dequeueReusableCellWithReuseIdentifier:@"FXCDayCell" forIndexPath:indexPath];
    
    NSDate* day = [self dateForIndexPath:indexPath];
    
    cv.weekend = ![day isWeekDay];
    cv.number = [day getDayOfTheMonth];

    if ( indexPath.item < 7)
    {
        cv.dayOfWeek = [DateUtil.getSingleton stringFromDateShortDayOfWeekOnly:day];
    }
    else
    {
        cv.dayOfWeek = @"";
    }

    if ([day isToday])
    {
        cv.dayOfWeek = [day monthString];
        cv.today = YES;
    }
    
    NSArray* dataArray = [self dataForIndexPath:indexPath];
    cv.events = dataArray;
    
    [cv setNeedsDisplay];
//    [cv setNeedsLayout];

    return cv;
}


#pragma  mark • Internal

- (NSDate *) dateForFirstDayInSection:(NSInteger)section
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = section;

	return [[DateUtil.getSingleton currentCalendar ] dateByAddingComponents:dateComponents toDate:self.fromDate options:0];
}

-(void) requestEventData:(NSDateComponents*)components
{
    NSInteger numSections = [self numberOfSectionsInCollectionView:self.collectionView];
    
    self.dataArray = [NSMutableArray arrayWithCapacity:3];
    
    for (NSInteger section = 0; section < numSections; section++ )
    {
        NSInteger daysInSection = [self collectionView:self.collectionView numberOfItemsInSection:section];
        
        NSDate* startDate = [self dateForFirstDayInSection:section];
        //adjust for days at beginning of first week
        NSUInteger weekday = [[DateUtil.getSingleton currentCalendar] components:NSWeekdayCalendarUnit fromDate:startDate].weekday;
        
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        dateComponents.day = - (weekday - 1);
        
        NSDate* adjustedStartDate = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:dateComponents toDate:startDate options:0];
        NSDate* lastDate = [adjustedStartDate dateByAddingDays:daysInSection-1];
        
        NSArray* array = [self.eventDelegate monthViewController:self eventsForDatesFrom:adjustedStartDate toDate:lastDate];
        if (array)
            [self.dataArray addObject:array];
    }

}

-(NSArray*) dataForIndexPath:(NSIndexPath*) indexPath
{
    NSArray* outerArray = [self.dataArray objectAtIndex:indexPath.section];
    NSArray* innerArray = [outerArray objectAtIndex:indexPath.item];
    return innerArray;
}

- (void) shiftDatesByComponents:(NSDateComponents *)components
{
	
	UICollectionView *cv = self.collectionView;
	UICollectionViewFlowLayout *cvLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
	
	NSArray *visibleCells = [self.collectionView visibleCells];
	if (![visibleCells count])
		return;
	
	NSIndexPath *fromIndexPath = [cv indexPathForCell:((UICollectionViewCell *)visibleCells[0]) ];
	NSInteger fromSection = fromIndexPath.section;
	NSDate *fromSectionOfDate = [self dateForFirstDayInSection:fromSection];
	CGPoint fromSectionOrigin = [self.collectionView convertPoint:[cvLayout layoutAttributesForItemAtIndexPath:fromIndexPath].frame.origin fromView:cv];
	
	_fromDate = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:components toDate:self.fromDate options:0];
	_toDate = [[DateUtil.getSingleton currentCalendar ] dateByAddingComponents:components toDate:self.toDate options:0];

    [cv reloadData];
	[cvLayout invalidateLayout];
	[cvLayout prepareLayout];
	
	NSInteger toSection = [[DateUtil.getSingleton currentCalendar ] components:NSMonthCalendarUnit fromDate:[self dateForFirstDayInSection:0] toDate:fromSectionOfDate options:0].month;
	UICollectionViewLayoutAttributes *toAttrs = [cvLayout layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:toSection]];
	CGPoint toSectionOrigin = [self.collectionView convertPoint:toAttrs.frame.origin fromView:cv];
	
	[cv setContentOffset:(CGPoint) {
		cv.contentOffset.x,
		cv.contentOffset.y + (toSectionOrigin.y - fromSectionOrigin.y)
	}];
}

-(NSDate*) dateForIndexPath:(NSIndexPath*)indexPath
{
    NSDate* retDate = nil;
    
    NSDate *firstDayInMonth = [self dateForFirstDayInSection:indexPath.section];
	NSUInteger weekday = [[DateUtil.getSingleton currentCalendar] components:NSWeekdayCalendarUnit fromDate:firstDayInMonth].weekday;
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.day = indexPath.item - (weekday - 1);
    
    retDate = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:dateComponents toDate:firstDayInMonth options:0];
    
    return retDate;
}

-(void) appendFutureDates
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = kNumberOfExtraPages;

    [self requestEventData:dateComponents];
    [self shiftDatesByComponents:dateComponents];
}

-(void) appendPastDates
{
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.month = -kNumberOfExtraPages;
    [self requestEventData:dateComponents];    
    [self shiftDatesByComponents:dateComponents];
}

@end
