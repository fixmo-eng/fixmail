/*
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "EventEngine.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "Attendee.h"
#import "BaseAccount.h"
#import "Calendar.h"
#import "CommitObjects.h"
#import "DateInfo.h"
#import "ErrorAlert.h"
#import "EmailProcessor.h"
#import "Event.h"
#import "EventDBAccessor.h"
#import "EventRecurrence.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@interface EventEngine ()
{
    dispatch_queue_t dispatchQueue;
}
@property (strong,nonatomic) NSMutableDictionary*       dataDictionary;

@end

@implementation EventEngine

// Properties
@synthesize operationQueue;
@synthesize startDate;
@synthesize endDate;

// Constants
static NSInteger kEventDatabaseVersion   = 2;

static const int kCalendarDayCapacity = 35; // 5 weeks * 7 days in a calendar usually

static const NSString* kQuery           = @"query";
static const NSString* kDelegate        = @"delegate";
static const NSString* kFolder          = @"folder";

// Static

static sqlite3_stmt*        eventInsertStmt         = nil;
static sqlite3_stmt*        eventForUidStmt         = nil;
static sqlite3_stmt*        eventForEventUidStmt    = nil;
static sqlite3_stmt*        eventFindStmt           = nil;
static sqlite3_stmt*        eventFindRecurringStmt  = nil;
static sqlite3_stmt*        eventSearchStmt         = nil;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSDateFormatter* sDateFormatter = NULL;

+ (NSDateFormatter*)dateFormatter
{
    if(!sDateFormatter)  {  
        // This the ActiveSync standard date format for events, Z is for Zulu (a.k.a. GMT)
        //      20120823T150000Z
        // Similar to AS email format but different, email has dashes, colon delimiters
        //
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sDateFormatter = [[NSDateFormatter alloc] init];
        [sDateFormatter setDateFormat:@"yyyyMMdd'T'HHmmss'Z'"];
        [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sDateFormatter;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


+ (EventEngine*)engine
{
    static EventEngine* sEventEngine = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(!sEventEngine) {
            sEventEngine = [[EventEngine alloc] init];
        }
        

    });

    return sEventEngine;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
        operationQueue = [[NSOperationQueue alloc] init];
        [operationQueue setMaxConcurrentOperationCount:1];
        dispatchQueue = dispatch_queue_create("com.fixmo.fixmail.EventEngine", nil);
    }
    
    return self;
}



////////////////////////////////////////////////////////////////////////////////////////////
// DBaseEngine Virtual overrides
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Virtual overrides

- (void)open:(BaseAccount*)anAccount
{
    sqlite3* aDatabase = nil;
    
    EventDBAccessor* aDBAccessor = [EventDBAccessor sharedManager];
    if([aDBAccessor isOpen]) {
    }else{
        aDatabase = [aDBAccessor database];
        if(aDatabase) {
            int aDatabaseVersion = [aDBAccessor userVersion];
            if(aDatabaseVersion < kEventDatabaseVersion) {
                if(aDatabaseVersion > 0) {
                    [self handleError:[NSString stringWithFormat:FXLLocalizedStringFromTable(@"EVENT_DATABASE_OUT_OF_DATE._WAS_V.%d,_NOW_V.%d", @"ErrorAlert", @"Error alert view message when event database out of date"),
                                       aDatabaseVersion, kEventDatabaseVersion]];
                    NSArray* aFolders = [anAccount foldersForFolderType:kFolderTypeCalendar];
                    for(ASFolder* aFolder in aFolders) {
                        [aFolder setNeedsInitialSync];
                    }
                    [self deleteAccount];
                }
                [aDBAccessor setUserVersion:kEventDatabaseVersion];
            }
            [EventEngine tableCheck];
        }else{
            [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENT_DATABASE_OPEN_FAILED:", @"ErrorAlert", @"Error alert view message when event database open fails"), [aDBAccessor databaseFilepath]]];
            return;
        }
    }
}

- (unsigned long long)spaceUsed
{
    unsigned long long aSpaceUsed = 0;
    
    NSString* aDBasePath = [[EventDBAccessor sharedManager] databaseFilepath];
    aSpaceUsed += [DBaseEngine sizeForPath:aDBasePath];
    
    return aSpaceUsed;
}

- (void)shutDown
{
    // Cancel all search and counting operations.  Must let all commit and update ops complete
    //
    NSArray* anOps = self.operationQueue.operations;
    for(NSInvocationOperation* anOp in anOps) {
        NSInvocation* anInvocation = anOp.invocation;
        if(anInvocation.selector == @selector(commitObjects:)) {
        }else{
            [anOp cancel];
        }
    }
    [self.operationQueue waitUntilAllOperationsAreFinished];

    [EventEngine clearPreparedStatements];
    [[EventDBAccessor sharedManager] close];
}

- (void)deleteAccount
{
    [self shutDown];
    NSString* aFilePath = [[EventDBAccessor sharedManager] databaseFilepath];
    [[NSFileManager defaultManager] removeItemAtPath:aFilePath error:nil];
}

- (NSString*)displayName
{
    return @"Calendar";
}

- (void)queueCommitObjects:(CommitObjects *)aCommitObjects
{
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(commitObjects:) object:aCommitObjects];
    [self.operationQueue addOperation:anOp];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EVENT_DATABASE", @"ErrorAlert", @"EventEngine error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EVENT_DATABASE", @"ErrorAlert", @"EventEngine error alert view exception message") message:anErrorMessage];
}

+ (void)handleError:(NSString*)anErrorMessage
{
    FXDebugLog(kFXLogCalendar, @"Event Database Error: %@", anErrorMessage);
    [[EventEngine engine] performSelectorOnMainThread:@selector(_handleError:) withObject:anErrorMessage waitUntilDone:FALSE];
}

////////////////////////////////////////////////////////////////////////////////
// DataBase
////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase

+ (void)clearPreparedStatements
{
	if(eventInsertStmt) {
		sqlite3_finalize(eventInsertStmt);
		eventInsertStmt = nil;
	}
    if(eventForUidStmt) {
		sqlite3_finalize(eventForUidStmt);
		eventForUidStmt = nil;
	}
    if(eventForEventUidStmt) {
		sqlite3_finalize(eventForEventUidStmt);
		eventForEventUidStmt = nil;
	}
    if(eventFindStmt) {
		sqlite3_finalize(eventFindStmt);
		eventFindStmt = nil;
	}
    if(eventFindRecurringStmt) {
		sqlite3_finalize(eventFindRecurringStmt);
		eventFindRecurringStmt = nil;
	}
    if(eventSearchStmt) {
		sqlite3_finalize(eventSearchStmt);
		eventSearchStmt = nil;
	}
}

////////////////////////////////////////////////////////////////////////////////
// DataBase Schema
//      NOTE: The following methods must be kept in sync!!!!
//      NOTE: recurrenceJSON must always be the first field
////////////////////////////////////////////////////////////////////////////////
#pragma mark DataBase Schema

+ (void)eventExec:(NSString*)aSQLString
{
    char* anErrorMsg;
    int aResult = sqlite3_exec([[EventDBAccessor sharedManager] database], [aSQLString UTF8String] , NULL, NULL, &anErrorMsg);
	if(aResult != SQLITE_OK) {
        FXDebugLog(kFXLogActiveSync, @"event exec Failed: %@", aSQLString);
        [self handleError:[NSString stringWithFormat:@"%@ %s",FXLLocalizedStringFromTable(@"EVENT_EXEC_FAILED:", @"ErrorAlert", @"Error alert view message when event exec failed"), anErrorMsg]];
	}
}

+ (void)tableCheck
{
    NSString* aCreateString =
    @"CREATE TABLE IF NOT EXISTS event "
            "(uid               VARCHAR[40] PRIMARY KEY, "
            "eventUid           VARCHAR[20], "
            "folderUid          VARCHAR[20], "
            "title              VARCHAR[256], "
            "location           VARCHAR[256], "
            "startDate          REAL, "
            "endDate            REAL, "
            "organizerName      VARCHAR[256], "
            "organizerEmail     VARCHAR[256], "
            "reminderMinutes    INTEGER, "
            "flags              INTEGER, "
            "notes              TEXT, "
            "recurrenceJSON     TEXT, "
            "meetingJSON        TEXT, "
            "json               TEXT)";
    [self eventExec:aCreateString];
    [self eventExec:@"CREATE INDEX IF NOT EXISTS event_startDate on event (startDate desc);"];
    [self eventExec:@"CREATE INDEX IF NOT EXISTS event_endDate on event (endDate desc);"];
    [self eventExec:@"CREATE INDEX IF NOT EXISTS folder_uid on event (folderUid);"];
}

static NSInteger sortAttendees(id obj1, id obj2, void *context)
{
    NSComparisonResult aComparisonResult;
    
	BOOL reverse = *((BOOL *)context);
	Attendee* anAttendee1 = (Attendee*)obj1;
	Attendee* anAttendee2 = (Attendee*)obj2;
    if ((NSInteger *)reverse == NO) {
        if(anAttendee1.attendeeSort > anAttendee2.attendeeSort) {
            aComparisonResult = NSOrderedDescending;
        }else if(anAttendee1.attendeeSort < anAttendee2.attendeeSort) {
            aComparisonResult = NSOrderedAscending;
        }else{
            aComparisonResult = [anAttendee1.name compare:anAttendee2.name];
        }
    }else{
        if(anAttendee2.attendeeSort > anAttendee1.attendeeSort) {
            aComparisonResult = NSOrderedDescending;
        }else if(anAttendee2.attendeeSort < anAttendee1.attendeeSort) {
            aComparisonResult = NSOrderedAscending;
        }else{
            aComparisonResult = [anAttendee1.name compare:anAttendee2.name];
        }
    }
    return aComparisonResult;
}

- (void)expandAttendees:(Event*)anEvent json:(NSString*)aMeetingJSON
{
    @try {
        NSArray* anArray = [aMeetingJSON JSONValue];
        for(NSDictionary* aDictionary in anArray) {
            Attendee* anAttendee = [[Attendee alloc] init];
            [anAttendee fromDictionary:aDictionary];
            [anEvent updateAttendeeStatus:anAttendee];
            [anEvent addAttendee:anAttendee];
        }
        BOOL aReverseSort = FALSE;
        anEvent.attendees = [[anEvent.attendees sortedArrayUsingFunction:sortAttendees context:&aReverseSort] mutableCopy];
        
        if(anEvent.attendees.count == 1) {
            //FXDebugLog(kFXLogCalendar, @"Event with only 1 attendee: %@", anEvent);
        }
        
    }@catch (NSException* e) {
        [self logException:@"expandAttendees" exception:e];
    }
}

- (void)_loadEvent:(Event*)anEvent folder:(BaseFolder*)aFolder statement:(sqlite3_stmt*)aStatement
{
    anEvent.uid             = getStringForField(aStatement,    0);
    // recurrenceJSON is field 1 and loaded outside of this
    anEvent.eventUid        = getStringForField(aStatement,    2);
    //NSString* folderUid   = getStringForField(aStatement,    3);
    anEvent.title           = getStringForField(aStatement,    4);
    anEvent.location        = getStringForField(aStatement,    5);
    anEvent.startDate       = getDateForField(aStatement,      6);
    anEvent.endDate         = getDateForField(aStatement,      7);
    anEvent.organizerName   = getStringForField(aStatement,    8);
    anEvent.organizerEmail  = getStringForField(aStatement,    9);
    anEvent.reminderMinutes = sqlite3_column_int(aStatement,   10);
    anEvent.flags           = sqlite3_column_int(aStatement,   11);
    anEvent.notes           = getStringForField(aStatement,    12);
    NSString* aMeetingJSON  = getStringForField(aStatement,    13);
    if(aMeetingJSON.length > 0) {
        [self expandAttendees:anEvent json:aMeetingJSON];
    }
    anEvent.json            = getStringForField(aStatement,    14);
    
    BOOL isEditable = anEvent.organizerEmail.length == 0
                  || [anEvent.organizerEmail isEqualToString:[[aFolder account] emailAddress]];
    [anEvent setIsEditable:isEditable];
    //if([anEvent.title isEqualToString:@"Qqq"]) {
    //    FXDebugLog(kFXLogActiveSync, @"event editable: %d %@ %@", isEditable, anEvent.organizerEmail, [[aFolder account] emailAddress]);
    //}

    if(!anEvent.startDate || !anEvent.endDate) {
        FXDebugLog(kFXLogActiveSync, @"_loadEvent invalid: %@", anEvent);
        //[anEvent.folder deleteUid:anEvent.uid updateServer:TRUE];
        //[anEvent.folder commitObjects];
    }

    [anEvent changed];
}

- (void)_storeEvent:(Event*)anEvent statement:(sqlite3_stmt*)aStatement
{
    NSString* startDateString   = [[EventEngine dateFormatter] stringFromDate:anEvent.startDate];
    NSString* endDateString     = [[EventEngine dateFormatter] stringFromDate:anEvent.endDate];
    NSString* aRecurrenceAsJSON;
    if(anEvent.recurrence) {
        aRecurrenceAsJSON = [anEvent.recurrence asJSON];
    }else{
        aRecurrenceAsJSON = @"";
    }
    
    NSString* aMeetingJSON;
    if(anEvent.attendees.count > 0) {
        aMeetingJSON = [Attendee attendeesAsJSON:anEvent.attendees];
    }else{
        aMeetingJSON = @"";
    }

    sqlite3_bind_text(aStatement, 1,   [anEvent.uid UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 2,   [aRecurrenceAsJSON UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 3,   [anEvent.eventUid UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 4,   [anEvent.folder.uid UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 5,   [anEvent.title UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 6,   [anEvent.location UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 7,   [startDateString UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 8,   [endDateString UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 9,   [anEvent.organizerName UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 10,  [anEvent.organizerEmail UTF8String], -1, NULL);
    sqlite3_bind_int(aStatement,  11,   anEvent.reminderMinutes);
    sqlite3_bind_int(aStatement,  12,   anEvent.flags);
    sqlite3_bind_text(aStatement, 13,  [anEvent.notes UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 14,  [aMeetingJSON UTF8String], -1, NULL);
    sqlite3_bind_text(aStatement, 15,  [anEvent.json UTF8String], -1, NULL);
}

- (NSString*)_eventFieldsAsString
{
    return @"uid, "
            "recurrenceJSON, "
            "eventUid, "
            "folderUid, "
            "title, "
            "location, "
            "startDate, "
            "endDate, "
            "organizerName, "
            "organizerEmail, "
            "reminderMinutes, "
            "flags, "
            "notes, "
            "meetingJSON, "
            "json ";
}

- (NSString*)_insertFieldsAsString
{
    //       1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
    return @"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
}

////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (NSString*)databaseError
{ 
    return [NSString stringWithUTF8String:sqlite3_errmsg([[EventDBAccessor sharedManager] database])];
}

static NSString* getStringForField(sqlite3_stmt* aStatement, int aFieldNum)
{
    NSString* aString = NULL;
    
    const char* str = (const char *)sqlite3_column_text(aStatement, aFieldNum);
    if(str != nil) {	
        aString = [NSString stringWithUTF8String:str]; 
    }else{
        aString = @"";
    }
    
    return aString;
}

static NSDate* getDateForField(sqlite3_stmt* aStatement, int aFieldNum)
{
    NSDate* aDate;
    const char* str = (const char *)sqlite3_column_text(aStatement, aFieldNum);
    if(str != nil) {
        NSString* aDateString = [NSString stringWithUTF8String:str];
        aDate = [[EventEngine dateFormatter] dateFromString:aDateString];
        //FXDebugLog(kFXLogActiveSync, @"getDateForField: %@ %@", dateString, [date description]);
    }else{
        aDate = [NSDate date];
        FXDebugLog(kFXLogActiveSync, @"getDateForField date string invalid");
    }
    return aDate;
}

- (Event*)_eventForDate:(NSDate*)aDate events:(NSMutableArray*)anEvents
{
    // Pop an nonrecurring event and return it if it matches the date
    //
    Event* anEvent = nil;
    
    if(anEvents.count > 0) {
        anEvent = [anEvents lastObject];
        //FXDebugLog(kFXLogActiveSync, @"eventForDate: %@", anEvent);
        if(anEvent) {
            if([[Calendar calendar] doesEvent:anEvent matchDate:aDate]) {
                [anEvents removeLastObject];
            }else{
                anEvent = nil;
            }
        }else{
            anEvent = nil;
        }
    }
    
    return anEvent;
}

- (void)addEventsForDate:(NSDate*)aDate
                 toArray:(NSMutableArray*)anArray
                  events:(NSMutableArray*)anEvents
         recurringEvents:(NSArray*)aRecurringEvents
{
    // Check for events which occur only on this day
    //
    Event* anEvent;
    if(anEvents.count > 0) {
        while((anEvent = [self _eventForDate:aDate events:anEvents]) != nil) {
            //FXDebugLog(kFXLogActiveSync, @"date: %@ %@", [aDate description], [anEvent.startDate description]);
            [anArray addObject:anEvent];
        }
    }
    
    // Process all recurring events to see if they recurr on this day
    //
    if(aRecurringEvents.count > 0) {
        DateInfo* aDateInfo = [[DateInfo alloc] initWithDate:aDate];
        Calendar* aCalendar = [Calendar calendar];
        for(anEvent in aRecurringEvents) {
            if(anEvent && [aCalendar doesEvent:anEvent matchDateInfo:aDateInfo]) {
                [anArray addObject:anEvent];
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Database Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Database Interface

- (Event*)eventForUid:(NSString*)aUid folder:(BaseFolder*)aFolder
{
    Event* anEvent = nil;
    
    @try {
        if(!eventForUidStmt) {
            NSString *aStatementString = [NSString stringWithFormat:
                      @"SELECT %@"
                      "FROM event "
                      "WHERE uid = ? "
                      "LIMIT 1;", [self _eventFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aStatementString UTF8String], -1, &eventForUidStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENT_FOR_UID_FAILED:", @"ErrorAlert", @"Error alert view message when event for uid failed with specific database error"), [self databaseError]]];
            }	
        }
        
        sqlite3_bind_text(eventForUidStmt, 1,   [aUid UTF8String], -1, NULL);
        
        if(sqlite3_step (eventForUidStmt) == SQLITE_ROW) {
            anEvent = [[Event alloc] initWithFolder:aFolder];

            NSString* aRecurrenceJSON  = getStringForField(eventForUidStmt, 1);
            if(aRecurrenceJSON.length > 1) {
                EventRecurrence* aRecurrence = [[EventRecurrence alloc] initWithJSON:aRecurrenceJSON];
                anEvent.recurrence = aRecurrence; 
            }else{
            }
            [self _loadEvent:anEvent folder:aFolder statement:eventForUidStmt];
            if([EmailProcessor debugEvent]) {
                FXDebugLog(kFXLogActiveSync, @"eventForUid: %@", anEvent);
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"eventForUid not found: %@", aUid);
        }
        sqlite3_reset(eventForUidStmt);
        
    }@catch (NSException* e) {
        [self logException:@"eventForUid" exception:e];
    }
    
    return anEvent;
}

- (Event*)eventForEventUid:(NSString*)aGlobalObjectID folder:(BaseFolder*)aFolder
{
    Event* anEvent = nil;
    
    @try {
        if(!eventForEventUidStmt) {
            NSString *aStatementString = [NSString stringWithFormat:
                                          @"SELECT %@"
                                          "FROM event "
                                          "WHERE eventUid = ? "
                                          "LIMIT 1;", [self _eventFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aStatementString UTF8String], -1, &eventForEventUidStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENT_FOR_EVENT_UID_FAILED:", @"ErrorAlert", @"Error alert view message when event for event uid failed with specific database error"), [self databaseError]]];
            }
        }
        
        sqlite3_bind_text(eventForEventUidStmt, 1,   [aGlobalObjectID UTF8String], -1, NULL);
        
        if(sqlite3_step (eventForEventUidStmt) == SQLITE_ROW) {
            anEvent = [[Event alloc] initWithFolder:aFolder];
            
            NSString* aRecurrenceJSON  = getStringForField(eventForEventUidStmt, 1);
            if(aRecurrenceJSON.length > 1) {
                EventRecurrence* aRecurrence = [[EventRecurrence alloc] initWithJSON:aRecurrenceJSON];
                anEvent.recurrence = aRecurrence;
            }else{
            }
            [self _loadEvent:anEvent folder:aFolder statement:eventForEventUidStmt];
            if([EmailProcessor debugEvent]) {
                FXDebugLog(kFXLogActiveSync, @"eventForEventUID: %@", anEvent);
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"eventForEventUID not found: %@", aGlobalObjectID);
        }
        sqlite3_reset(eventForEventUidStmt);
        
    }@catch (NSException* e) {
        [self logException:@"eventForEventUID" exception:e];
    }
    
    return anEvent;
}

- (NSMutableArray*)eventsForTimeRange:(NSDate*)aStartDate endDate:(NSDate*)anEndDate folder:(BaseFolder*)aFolder
{
    NSMutableArray* anEvents = nil;
    
    if(!aStartDate || !anEndDate || !aFolder) {
        FXDebugLog(kFXLogCalendar, @"eventsForTimeRange invalid: %@ %@ %@", aStartDate, anEndDate, aFolder);
        return anEvents;
    }
    @try {
        BOOL aDebugEvent = [EmailProcessor debugEvent];

        if(!eventFindStmt) {
            NSString *aStatementString = [NSString stringWithFormat:
                      @"SELECT %@"
                      "FROM event "
                      "WHERE folderUid = ? "
                      " AND (startDate >= ? AND endDate <= ?) "	
                      "ORDER BY startDate DESC;", [self _eventFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aStatementString UTF8String], -1, &eventFindStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENTS_FOR_TIME_RANGE_FAILED:", @"ErrorAlert", @"Error alert view message when events for time range failed"), [self databaseError]]];
            }else if(aDebugEvent) {
                FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange: %@", aStatementString);
            }
        }
        
        NSString* aStartDateString  = [[EventEngine dateFormatter] stringFromDate:aStartDate];
        NSString* anEndDateString   = [[EventEngine dateFormatter] stringFromDate:anEndDate];

        sqlite3_bind_text(eventFindStmt, 1, [aFolder.uid UTF8String],      -1, NULL);
        sqlite3_bind_text(eventFindStmt, 2, [aStartDateString UTF8String], -1, NULL);
        sqlite3_bind_text(eventFindStmt, 3, [anEndDateString UTF8String],  -1, NULL);

        anEvents = [NSMutableArray arrayWithCapacity:30]; 
        while(sqlite3_step (eventFindStmt) == SQLITE_ROW) {
            NSString* aRecurrenceJSON  = getStringForField(eventFindStmt, 1);
            if(aRecurrenceJSON.length > 1) {
                // We don't want recurring events for this
            }else{
                Event* anEvent = [[Event alloc] initWithFolder:aFolder];
                [self _loadEvent:anEvent folder:aFolder statement:eventFindStmt];
                switch(anEvent.meetingStatus) {
                    case kMeetingStatusMeetingCanceled:
                        //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancelled: %@", anEvent.title);
                        break;
                    case kMeetingStatusMeetingCanceledRecieved:
                        //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancel received: %@", anEvent.title);
                        break;
                    default:
                        [anEvents addObject:anEvent];
                        break;
                }
                if(aDebugEvent) {
                    FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange: %@", anEvent);
                }
            }
        }
        sqlite3_reset(eventFindStmt);

    }@catch (NSException* e) {
        [self logException:@"eventsForTimeRange" exception:e];
    }
    
    return anEvents;
}

- (NSMutableArray*)eventsRecurring:(BaseFolder*)aFolder
{
    NSMutableArray* anEvents = nil;
    
    if(!aFolder) {
        FXDebugLog(kFXLogCalendar, @"eventsRecurring invalid: %@", aFolder);
        return anEvents;
    }
    @try {
        BOOL aDebugEvent = [EmailProcessor debugEvent];
        
        if(!eventFindRecurringStmt) {
            NSString *aStatementString = [NSString stringWithFormat:
                    @"SELECT %@ "
                    "FROM event "
                    "WHERE folderUid = ? "
                    " AND LENGTH(recurrenceJSON) > 1 "	
                    "ORDER BY startDate;", [self _eventFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aStatementString UTF8String], -1, &eventFindRecurringStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENTS_RECURRING_FAILED:", @"ErrorAlert", @"Error alert view message when events recurring failed"), [self databaseError]]];
            }else if(aDebugEvent) {
                FXDebugLog(kFXLogActiveSync, @"eventsRecurring: %@", aStatementString);
            }
        }
        
        sqlite3_bind_text(eventFindRecurringStmt, 1, [aFolder.uid UTF8String],      -1, NULL);
        
        anEvents = [NSMutableArray arrayWithCapacity:30]; 
        while(sqlite3_step (eventFindRecurringStmt) == SQLITE_ROW) {
            NSString* aRecurrenceJSON  = getStringForField(eventFindRecurringStmt, 1);
            if(aRecurrenceJSON.length > 1) {
                Event* anEvent = [[Event alloc] initWithFolder:aFolder];
                EventRecurrence* anEventRecurrence = [[EventRecurrence alloc] initWithJSON:aRecurrenceJSON];
                anEvent.recurrence = anEventRecurrence;
                [self _loadEvent:anEvent folder:aFolder statement:eventFindRecurringStmt];
                switch(anEvent.meetingStatus) {
                    case kMeetingStatusMeetingCanceled:
                        //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancelled: %@", anEvent.title);
                        break;
                    case kMeetingStatusMeetingCanceledRecieved:
                        //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancel received: %@", anEvent.title);
                        break;
                    default:
                        [anEvents addObject:anEvent];
                        break;
                }
                if(aDebugEvent) {
                    FXDebugLog(kFXLogActiveSync, @"eventsRecurring: %@", anEvent);
                }
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME recurring event without JSON string: '%@'", aRecurrenceJSON);
            }
        }
        sqlite3_reset(eventFindRecurringStmt);
        
    }@catch (NSException* e) {
        [self logException:@"eventsRecurring" exception:e];
    }
    
    return anEvents;
}

- (void)performSearch:(NSDictionary*)aParams
{
    NSMutableArray* anEvents = nil;
    NSString* aString = [aParams objectForKey:kQuery];
    BaseFolder* aFolder = [aParams objectForKey:kFolder];
    id aDelegate = [aParams objectForKey:kDelegate];
    if(aString.length == 0 || !aDelegate || !aFolder) {
        FXDebugLog(kFXLogActiveSync, @"performSearch invalid: %@", aParams);
        return;
    }
    
    @try {
        BOOL aDebugEvent = [EmailProcessor debugEvent];
        
        if(!eventSearchStmt) {
            NSString *aStatementString = [NSString stringWithFormat:
                                          @"SELECT %@"
                                            "FROM event "
                                            "WHERE folderUid = ? "
                                            "AND (title LIKE ?) "
                                            "ORDER BY startDate;", [self _eventFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aStatementString UTF8String], -1, &eventSearchStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENT_SEARCH_FAILED:", @"ErrorAlert", @"Error alert view message when event search failed"), [self databaseError]]];
            }else if(aDebugEvent) {
                FXDebugLog(kFXLogActiveSync, @"performSearch: %@", aStatementString);
            }
        }
    
        sqlite3_bind_text(eventSearchStmt, 1, [aFolder.uid UTF8String], -1, NULL);
        sqlite3_bind_text(eventSearchStmt, 2, [aString UTF8String],     -1, NULL);
        
        anEvents = [NSMutableArray arrayWithCapacity:30];
        while(sqlite3_step (eventSearchStmt) == SQLITE_ROW) {
            Event* anEvent = [[Event alloc] initWithFolder:aFolder];
            [self _loadEvent:anEvent folder:aFolder statement:eventSearchStmt];
            switch(anEvent.meetingStatus) {
                case kMeetingStatusMeetingCanceled:
                    //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancelled: %@", anEvent.title);
                    break;
                case kMeetingStatusMeetingCanceledRecieved:
                    //FXDebugLog(kFXLogActiveSync, @"eventsForTimeRange cancel received: %@", anEvent.title);
                    break;
                default:
                    [anEvents addObject:anEvent];
                    break;
            }
            if(aDebugEvent) {
                FXDebugLog(kFXLogActiveSync, @"performSearch: %@", anEvent);
            }
        }
        sqlite3_reset(eventSearchStmt);
        
        if([aDelegate respondsToSelector:@selector(deliverSearchResult:)])	{
            [aDelegate performSelector:@selector(deliverSearchResult:) withObject:anEvents];
        }
        
    }@catch (NSException* e) {
        [self logException:@"performSearch" exception:e];
    }
}

- (void)searchFor:(NSString *)aQuery folder:(BaseFolder*)aFolder withDelegate:(id)aDelegate
{
	//Create the queryOp object
	NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
	[params setObject:aQuery forKey:kQuery];
	[params setObject:aDelegate forKey:kDelegate];
    [params setObject:aFolder forKey:kFolder];

	//Invoke local search
	NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performSearch:) object:params];
	[self.operationQueue addOperation:anOp];
}

- (void)storeEvent:(Event*)anEvent
{
    @try {
        if(eventInsertStmt == nil) {
            NSString* aString = [NSString stringWithFormat:
                    @"INSERT OR REPLACE INTO event(%@) VALUES (%@);", [self _eventFieldsAsString], [self _insertFieldsAsString]];
            int aResult = sqlite3_prepare_v2([[EventDBAccessor sharedManager] database], [aString UTF8String], -1, &eventInsertStmt, nil);
            if (aResult != SQLITE_OK) {
                [self handleError:[NSString stringWithFormat:@"%@ %@", FXLLocalizedStringFromTable(@"EVENT_STORE_FAILED:", @"ErrorAlert", @"Error alert view message when storing of event failed"), [self databaseError]]];
                return;
            }
        }
        
        [self _storeEvent:anEvent statement:eventInsertStmt];
        if (sqlite3_step(eventInsertStmt) != SQLITE_DONE)	{
            FXDebugLog(kFXLogActiveSync, @"INSERT INTO event FAIL: '%s':\n%@", sqlite3_errmsg([[EventDBAccessor sharedManager] database]), anEvent);
        }else if([EmailProcessor debugEvent]) {
            FXDebugLog(kFXLogActiveSync, @"INSERT INTO event: %@", anEvent);
        }
        sqlite3_reset(eventInsertStmt);
    }@catch (NSException* e) {
        [self logException:@"storeEvent" exception:e];
    }
}

- (void)deleteEventWithUid:(NSString*)aUid
{
    NSString* aSQLString = [NSString stringWithFormat:@"DELETE FROM event WHERE uid='%@';", aUid];
    
    char* errorMsg;
    int aResult = sqlite3_exec([[EventDBAccessor sharedManager] database], [aSQLString UTF8String] , NULL, NULL, &errorMsg);
	if(aResult != SQLITE_OK) {
        [self handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"EVENT_DELETE_FAILED:", @"ErrorAlert", @"Error alert view message when deletion of event failed"), errorMsg]];
	}else if([EmailProcessor debugEvent]) {
        FXDebugLog(kFXLogActiveSync, @"deleteEventWithUid: %@", aUid);
    }
}

- (void)deleteDatabaseForFolderUid:(NSString*)aUid
{
    NSString* aSQLString = [NSString stringWithFormat:@"DELETE FROM event WHERE folderUid='%@';", aUid];
    
    char* errorMsg;
    int aResult = sqlite3_exec([[EventDBAccessor sharedManager] database], [aSQLString UTF8String] , NULL, NULL, &errorMsg);
	if(aResult != SQLITE_OK) {
        [self handleError:[NSString stringWithFormat:@"%@ %s", FXLLocalizedStringFromTable(@"EVENT_DATABASE_DELETE_FAILED:", @"ErrorAlert", @"Error alert view message when deletion of event from database failed"), errorMsg]];
	}else if([EmailProcessor debugEvent]) {
        FXDebugLog(kFXLogActiveSync, @"deleteDatabaseForFolder: %@", aUid);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Event Cache
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Cache

-(void) createSortedIndex
{
    self.sortedKeys = [[self.dataDictionary allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDate* obj1Date = obj1;
        NSDate* obj2Date = obj2;
        
        return [obj1Date compare:obj2Date];
    }];

}

- (void)eventCacheAddEvents:(NSArray*)anObjects
{
    NSUInteger aCount = anObjects.count;
    NSMutableArray* anEvents         = [[NSMutableArray alloc]initWithCapacity:aCount];
    NSMutableArray* aRecurringEvents = [[NSMutableArray alloc]initWithCapacity:aCount];
    for(Event* anEvent in anObjects) {
        if([anEvent recurrence]) {
            [aRecurringEvents addObject:anEvent];
        }else{
            [anEvents addObject:anEvent];
        }
    }
    
    [self eventCacheBuildWithEvents:anEvents
                    recurringEvents:aRecurringEvents
                           isAppend:TRUE
                          startDate:self.startDate
                            endDate:self.endDate];
    
}

- (BOOL)eventCacheDeleteUid:(NSString*)aUid
{
    BOOL didDelete = FALSE;
    
    for(NSDate* aDate in self.dataDictionary) {
        NSMutableArray* anEvents = [self.dataDictionary objectForKey:aDate];
        for(Event* anEvent in anEvents) {
            if([anEvent.uid isEqualToString:aUid]) {
                didDelete = TRUE;
                [anEvents removeObject:anEvent];
                break;
            }
        }
    }
    return didDelete;
}

- (void)eventCacheExtendToDate:(NSDate*)aDate folder:(BaseFolder*)aFolder
{
    // FIXME - This may make cached events grow to a very large size if user moves though a long time period
    // probably should start removing events from the side the user is moving away from
    //

    Calendar* aCalendar = [Calendar calendar];
    if(!self.startDate) {
        NSDate* aStartDate  = [aCalendar timelessDate:aDate];
        NSDate* anEndDate   = [aCalendar addDays:7 toDate:aStartDate];
        [self eventCacheFromDate:aStartDate toDate:anEndDate folder:aFolder];
        
    }else if([aDate compare:self.startDate] == NSOrderedAscending || [aDate compare:self.endDate] == NSOrderedDescending) {

        // Figure out if new date is before or after current cache and extend the date range in the correct direction
        //
        NSDate* aStartDate;
        NSDate* anEndDate;
        if([aDate compare:self.startDate] == NSOrderedAscending) {
            anEndDate = self.startDate;
            self.startDate = aStartDate = [aCalendar addMonths:-1 toDate:anEndDate];
        }else{
            aStartDate = self.endDate;
            self.endDate = anEndDate = [aCalendar addMonths:1 toDate:aStartDate];
        }
        
        // Load the event cache for the new time range
        //
        NSMutableArray* anEvents         = [self eventsForTimeRange:self.startDate endDate:self.endDate folder:aFolder];
        NSMutableArray* aRecurringEvents = [self eventsRecurring:aFolder];
        [self eventCacheBuildWithEvents:anEvents
                        recurringEvents:aRecurringEvents
                               isAppend:FALSE
                              startDate:self.startDate
                                endDate:self.endDate];
    }
}

-(void) eventCacheExtendToDate:(NSDate *)aDate folder:(BaseFolder *)aFolder completion:(void (^)(void))block
{

    __block id weakSelf = self;

    dispatch_async(dispatchQueue, ^{
        [weakSelf eventCacheExtendToDate:aDate folder:aFolder];
        dispatch_async(dispatch_get_main_queue(), ^{
            block();
        });
        
    });
    
    
}


- (NSArray*)eventsArrayFromDate:(NSDate*)aStartDate toDate:anEndDate
{
    NSMutableArray* anEventsArray = [NSMutableArray array];
    
    // Scan through the days in the calendar and set the dates which have events to show and inidicator
    // and attach events to days
    //
    Calendar* aCalendar = [Calendar calendar];
    NSDate* aDate       = aStartDate;
    
    while(1) {
        NSMutableArray* anArray = [self.dataDictionary objectForKey:aDate];
        if(!anArray) {
            anArray = [NSMutableArray arrayWithCapacity:0];
        }
        
        [anEventsArray addObject:anArray];

        // Move to the next day
        //
        TKDateInformation info = [aCalendar dateInformationFromDate:aDate];
		info.day++;
		aDate = [aCalendar dateFromDateInformation:info];
		if([aDate compare:anEndDate] == NSOrderedDescending) break;
    }
    
    return anEventsArray;
}

- (void)eventCacheFromDate:(NSDate*)aStartDate toDate:(NSDate*)anEndDate folder:(BaseFolder*)aFolder
{
    self.startDate  = aStartDate;
    self.endDate    = anEndDate;
    
    // get events from database for this time range including recurrences
    //
    NSMutableArray* anEvents         = [self eventsForTimeRange:aStartDate endDate:anEndDate folder:aFolder];
    NSMutableArray* aRecurringEvents = [self eventsRecurring:aFolder];
    
    // Dictionary with an an array object for each date which has events
    self.dataDictionary = [NSMutableDictionary dictionaryWithCapacity:kCalendarDayCapacity];
    
    [self eventCacheBuildWithEvents:anEvents
                    recurringEvents:aRecurringEvents
                           isAppend:FALSE
                          startDate:self.startDate
                            endDate:self.endDate];
}

- (void)eventCacheBuildWithEvents:(NSMutableArray*)anEvents
                  recurringEvents:(NSArray*)aRecurringEvents
                         isAppend:(BOOL)isAppend
                        startDate:(NSDate*)aStartDate
                          endDate:(NSDate*)anEndDate
{
    // Scan through the days in the calendar and set the dates which have events to show and inidicator
    // and attach events to days
    //
    Calendar* aCalendar = [Calendar calendar];
    NSDate* aDate       = aStartDate;

    while(1) {
        NSMutableArray* anArray = [self.dataDictionary objectForKey:aDate];
        if(!anArray) {
            anArray = [NSMutableArray arrayWithCapacity:0];
        }else if(!isAppend) {
            [anArray removeAllObjects];
        }
        
        // Put the events for this date in the array
        //
        [self addEventsForDate:aDate toArray:anArray events:anEvents recurringEvents:aRecurringEvents];
        
        if (anArray.count > 0)
            [self.dataDictionary setObject:anArray forKey:aDate];
        
        // Move to the next day
        //
        TKDateInformation info = [aCalendar dateInformationFromDate:aDate];
		info.day++;
		aDate = [aCalendar dateFromDateInformation:info];
		if([aDate compare:anEndDate] == NSOrderedDescending) break;
    }
    [self createSortedIndex];
}

- (NSArray*) eventCacheEventsForDate:(NSDate*)timelessDate
{
    NSArray* anEvents = [self.dataDictionary objectForKey:timelessDate];
    return anEvents;
}

- (void)eventCacheDebug
{
#ifdef DEBUG
    for(NSObject* aKey in self.dataDictionary) {
        NSArray* anArray = [self.dataDictionary objectForKey:aKey];
        FXDebugLog(kFXLogActiveSync, @"%d", anArray.count);
    }
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////
// Commit changes to database
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Commit changes to database

- (DBaseEngine*)databaseEngine
{
    return [EventEngine engine];
}

- (void)commitObjects:(CommitObjects*)aCommitObjects
{
    @try {
        BaseFolder* aFolder = aCommitObjects.folder;
        
        // Objects Added
        //
        NSArray* anObjectsAdded = aCommitObjects.objectsAdded;
        if(anObjectsAdded.count > 0) {
            [self _commitEvents:anObjectsAdded folder:aFolder];
        }
        
        // Objects Changed
        //
        NSArray* anObjectsChanged = aCommitObjects.objectsChanged;
        if(anObjectsChanged.count > 0) {
            [aFolder commitChanges:anObjectsChanged];
        }
        
        // Objects Deleted
        //
        NSArray* anObjectsDeleted = aCommitObjects.objectsDeleted;
        if(anObjectsDeleted.count > 0) {
            for(NSString* aUidString in anObjectsDeleted) {
                [self deleteEventWithUid:aUidString];
            }
        }
        
        if(aCommitObjects.target && aCommitObjects.action) {
            [aCommitObjects.target performSelectorOnMainThread:aCommitObjects.action withObject:aCommitObjects waitUntilDone:FALSE];
        }
    }@catch (NSException* e) {
        [self logException:@"commitObjects" exception:e];
    }
}

- (void)_commitEvents:(NSArray*)anEvents folder:(BaseFolder*)aFolder
{
    if(![EventDBAccessor beginTransaction]) {
        FXDebugLog(kFXLogActiveSync, @"commitEvents beginTransaction failed");
    }
    
    for(Event* anEvent in anEvents) {
        [self storeEvent:anEvent];
    }
    
    if(![EventDBAccessor endTransaction]) {
        FXDebugLog(kFXLogActiveSync, @"commitEvents beginTransaction failed");
    }
}

@end
