//
//  FXCDayCell.h
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-04-29.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXCDayCell : UICollectionViewCell
@property (nonatomic,retain) NSString* dayOfWeek;
@property (nonatomic,assign) int number;
@property (nonatomic,assign) BOOL weekend;
@property (nonatomic,assign) BOOL placeHolder;
@property (nonatomic,assign) BOOL today;
@property (nonatomic,strong) NSArray* events;
@property (nonatomic,assign) NSInteger selectedIndex;

-(id) eventObjectAtPoint:(CGPoint)point;

@end
