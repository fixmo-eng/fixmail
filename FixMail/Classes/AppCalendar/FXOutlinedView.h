//
//  FXOutlinedView.h
//  FixMail
//
//  Created by Sean Langley on 2013-07-03.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXOutlinedView : UIView
@property UIColor* outlineColor;

@end
