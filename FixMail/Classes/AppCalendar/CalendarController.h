/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "Calendar.h"
#import "CalendarDelegate.h"
#import "FolderDelegate.h"
#import "PickerDelegate.h"
#import "SearchRunner.h"

@class ASCalendarFolder;
@class CalendarTheme;
@class EmailSearch;
@class EventPickViewController;
@class InvitePickViewController;
@class MeetingViewController;
@class MKNumberBadgeView;
 
typedef enum
{
    kScrollHorizontal   = 0,
    kScrollVertical     = 1
} EScrollDirection;

@interface CalendarController : BaseObject <UISearchBarDelegate, FolderDelegate, PickerDelegate, SearchManagerDelegate>
{
    UINavigationController*     navigationController;
    MKNumberBadgeView*          inviteBadge;
    
    EventPickViewController*    eventPicker;
    InvitePickViewController*   invitePicker;
    MeetingViewController*      meetingView;
    
    UIBarButtonItem*            pickerBarButton;
    UIBarButtonItem*            searchBarButton;
    UIBarButtonItem*            inviteBarButton;

    BOOL                        isPad;
    BOOL                        invitePopoverVisible;
}

@property (nonatomic,strong) UIViewController<CalendarDelegate>*          calendarDelegate;
@property (nonatomic,strong) ASCalendarFolder*          folder;

@property (nonatomic,strong) NSMutableArray*            invitationEmails;
@property (nonatomic,strong) NSMutableDictionary*       invitationDictionary;
@property (nonatomic,strong) NSDate*                    date;

@property (nonatomic,strong) UISegmentedControl*        segmentedControl;
@property (nonatomic,strong) UIBarButtonItem*           segmentedBarButtonItem;
@property (nonatomic,strong) UIBarButtonItem*           addEventButtonItem;

@property (nonatomic,strong) EventPickViewController*   eventPicker;
@property (nonatomic,strong) InvitePickViewController*  invitePicker;
@property (nonatomic,strong) MeetingViewController*     meetingView;
@property (nonatomic,strong) UIPopoverController*       popover;
@property (nonatomic,strong) CalendarTheme*             calendarTheme;

// Factory
+ (CalendarController*)controller;
+ (CalendarController*)createControllerWithFolder:(ASCalendarFolder*)aFolder size:(CGSize)aSize color:(UIColor*)aColor;

- (void)replaceViewController:(UIViewController*)aViewController splitViewController:(UISplitViewController*)aSplitViewController;

- (UIViewController*)calendarViewControllerForDate:(NSDate*)aDate
                                            folder:(ASCalendarFolder*)aFolder
                                              size:(CGSize)aSize;
- (UIViewController*)calendarViewControllerOfViewType:(ECalendarViewType)aCalendarType
                                              forDate:(NSDate*)aDate
                                               folder:(ASCalendarFolder*)aFolder
                                                 size:(CGSize)aSize;

- (void)setCalendarViewControllerForDate:(NSDate*)aDate
                                  folder:(ASCalendarFolder*)aFolder
                                    size:(CGSize)aSize
                    navigationController:(UINavigationController*)aNavigationController;
- (void)setCalendarViewControllerOfViewType:(ECalendarViewType)aCalendarType
                                    forDate:(NSDate*)aDate
                                     folder:(ASCalendarFolder*)aFolder
                                       size:(CGSize)aSize
                       navigationController:(UINavigationController*)aNavigationController;

- (void)setIsReachable:(BOOL)isReachable;

- (void)topToolbar:(UINavigationItem*)aNavigationitem;
- (void)calendarSelector:(UINavigationItem*)aNavigationitem ;
- (NSArray*)bottomToolbar:(UINavigationController*)aNavigationController;

- (void)setSelectedCalendar:(ECalendarViewType)aViewType;

-(int)firstDayOfTheWeek;

@end
