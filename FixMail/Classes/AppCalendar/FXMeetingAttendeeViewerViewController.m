//
//  FXMeetingAttendeeViewerViewController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-12.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMeetingAttendeeViewerViewController.h"
#import "FXTableViewSubtitleCell.h"
#import "Attendee.h"

@interface FXMeetingAttendeeViewerViewController ()

@end

@implementation FXMeetingAttendeeViewerViewController

+(id)controller
{
    return [[[self class] alloc] initWithStyle:UITableViewStyleGrouped];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        [self.tableView registerClass:[FXTableViewSubtitleCell class]  forCellReuseIdentifier:@"FXMeetingAttendeeViewerViewController"];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Attendees";

 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.event.attendees count];
}

-(NSString*) attendeeStatusString:(Attendee*)attendee
{
    switch ( attendee.attendeeStatus)
    {
        case kAttendeeResponseTentative:
            return @"Tentative";
            break;
        case kAttendeeResponseAccept:
            return @"Accepted";
            break;
        case kAttendeeResponseDecline:
            return @"Declined";
            break;
        case kAttendeeResponseNotResponded:
            return @"Not Responded";
            break;
        default:
        case kAttendeeResponseUnknown:
            return @"Unknown response";
            break;

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FXMeetingAttendeeViewerViewController";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Attendee* attendee = [self.event.attendees objectAtIndex:indexPath.row];
    if (attendee.name && ![attendee.name isEqualToString:@""])
    {
        cell.textLabel.text = attendee.name;
    }
    else
    {
        cell.textLabel.text = attendee.address;
    }
    cell.detailTextLabel.text = [self attendeeStatusString:attendee];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Attendee* attendee = [self.event.attendees objectAtIndex:indexPath.row];
    UIViewController* vc = [attendee asContactViewController];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];

}

@end
