/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TimeZoneInfo.h"
#import "Calendar.h"

@implementation TimeZoneInfo

// Properties
@synthesize hasDaylightSavingsTime;
@synthesize timeZone;
@synthesize date;
@synthesize name;

@synthesize daylightOffsetMinutes;
@synthesize daylightAbbreviation;
@synthesize daylightTranstionDate;

@synthesize standardOffsetMinutes;
@synthesize standardAbbreviation;
@synthesize standardTranstionDate;

// Constants
static const int kSecondsPerMinute          = 60;
static const int kMinutesPerHour            = 60;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (TimeZoneInfo*)timeZoneInfo:(NSTimeZone*)aTimeZone forDate:(NSDate*)aDate
{
    TimeZoneInfo* aTimeZoneInfo = [[TimeZoneInfo alloc] initWithTimeZone:aTimeZone forDate:aDate];
    return aTimeZoneInfo;
}

+ (TimeZoneInfo*)timeZoneInfoWithName:(NSString*)aTimeZoneName forDate:(NSDate*)aDate
{
    TimeZoneInfo* aTimeZoneInfo = [[TimeZoneInfo alloc] initWithTimeZoneName:aTimeZoneName forDate:aDate];
    return aTimeZoneInfo;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (void)setup
{
    self.name       = self.timeZone.name;
    
    NSDate* aDate = [[Calendar calendar] newYearsDayForDate:self.date];
    if([self.timeZone isDaylightSavingTimeForDate:aDate]) {
        aDate = [self.timeZone nextDaylightSavingTimeTransitionAfterDate:aDate];
    }
    
    self.standardOffsetMinutes  = (NSInteger)[self.timeZone secondsFromGMTForDate:aDate] / kSecondsPerMinute;
    self.standardAbbreviation   = [self.timeZone abbreviationForDate:aDate];
    self.daylightTranstionDate  = [self.timeZone nextDaylightSavingTimeTransitionAfterDate:aDate];
    
    aDate = self.daylightTranstionDate;
    if([self.timeZone isDaylightSavingTimeForDate:aDate]) {
        self.hasDaylightSavingsTime = TRUE;
        self.daylightOffsetMinutes  = (NSInteger)[self.timeZone secondsFromGMTForDate:aDate] / kSecondsPerMinute;
        self.daylightAbbreviation   = [self.timeZone abbreviationForDate:aDate];
        self.standardTranstionDate  = [self.timeZone nextDaylightSavingTimeTransitionAfterDate:aDate];
    }else{
        self.hasDaylightSavingsTime = FALSE;
    }
}

- (id)initWithTimeZone:(NSTimeZone*)aTimeZone forDate:(NSDate*)aDate
{
    if (self = [super init]) {
        self.date       = aDate;
        self.timeZone   = aTimeZone;
        [self setup];
    }
    return self;
}

- (id)initWithTimeZoneName:(NSString*)aTimeZoneName forDate:(NSDate*)aDate
{
    if (self = [super init]) {
        // NSArray* aNames = [NSTimeZone knownTimeZoneNames];
        // for(NSString* aName in aNames) {
        //     FXDebugLog(kFXLogActiveSync, @"%@", aName);
        // }
        self.date       = aDate;
        self.timeZone   = [NSTimeZone timeZoneWithName:aTimeZoneName];
        [self setup];
    }
    return self;
}


- (NSString*)asTimeOffsetString:(NSInteger)anOffset
{
    NSString* aString;
    if(anOffset < 0) {
        aString = [NSString stringWithFormat:@"%03d%02d", anOffset/kMinutesPerHour, anOffset%kMinutesPerHour];
    }else{
        aString = [NSString stringWithFormat:@"%02d%02d", anOffset/kMinutesPerHour, anOffset%kMinutesPerHour];
    }
    return aString;
}

- (NSString*)daylightOffsetAsString
{
    return [self asTimeOffsetString:daylightOffsetMinutes];
}

- (NSString*)standardOffsetAsString
{
    return [self asTimeOffsetString:standardOffsetMinutes];
}

@end