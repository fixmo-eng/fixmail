/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarMonthController.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "CalendarDayController.h"
#import "DateInfo.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventEditController.h"
#import "EventEngine.h"
#import "EventViewController.h"
#import "FXMeetingViewController.h"
#import "NSDate+TKCategory.h"
#import "ProgressView.h"
#import "Reachability.h"
#import "TKCalendarDayEventView.h"
#import "DateUtil.h"
#import "FXCDayCell.h"

#import "TKCalendarMonthView.h"
#import "FXCalendarPopoverDelegate.h"
#import "FXCalendarDayViewPopoverContentController.h"

@interface  CalendarMonthController ()
@property (nonatomic,strong) FXCalendarPopoverDelegate* calendarPopoverDelegate;
@end

@implementation CalendarMonthController

// Properties
@synthesize folder;
@synthesize eventViewController;
@synthesize dayController;
@synthesize popover;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

// Constants
static const int kSecondsInHour = 60 * 60;
static const CGFloat kPopoverWidth = 320.f;

////////////////////////////////////////////////////////////////////////////////
// Factory 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (CalendarMonthController*)controller
{
    CalendarMonthController* sController = NULL;

    if(!sController) {
        sController = [CalendarMonthController superController];
        sController.eventDelegate = sController;
    }

    return sController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}

-(id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout
{
    if ((self = [super initWithCollectionViewLayout:layout]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}

- (void)dealloc 
{

    if(folder) {
        [folder removeDelegate:self];
    }
    folder = nil;
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_MONTH", @"ErrorAlert", @"Calendar Month Controller error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"CALENDAR_MONTH_ERROR", @"ErrorAlert", @"Calendar Month Controller error alert view title") 
                                                          message:anErrorMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL", @"Calendar", @"Calendar Month Controller error alert view cancel button title")
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView*)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// View 
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    [self loadGestures];
    
    [self setCurrentDate:[NSDate date]];
}

- (void)viewDidUnload
{
	[super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    // Navigation tool bar
    [[CalendarController controller] topToolbar:self.navigationItem];
    
    // Bottom tool bar
    //
    NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
    self.toolbarItems = aToolBarItems;
    
    if(folder) {
        [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0.0f];
        [folder addDelegate:self];
    }
    
    [self startReachablility];

}

- (void)viewDidAppear:(BOOL)animated
{
    // Calendar selector
    [[CalendarController controller] calendarSelector:self.navigationItem];
    [super viewDidAppear:animated];
    [self setDate:[[CalendarController controller] date]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self stopReachability];
    
    if(folder) {
        [folder removeDelegate:self];
    }
    
    self.navigationItem.titleView = nil;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            [[CalendarController controller] setIsReachable:TRUE];
            break;
        case NotReachable:
            [[CalendarController controller] setIsReachable:FALSE];
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Gestures
////////////////////////////////////////////////////////////////////////////////

#pragma mark Gestures
- (void)didTap:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    CGPoint aLocation = [aTapGestureRecognizer locationInView:self.view];
    
    //view -> collectionView point
    
    CGPoint cLocation = [self.view convertPoint:aLocation toView:self.collectionView];
    NSIndexPath* ip = [self.collectionView indexPathForItemAtPoint:cLocation];
    
    FXCDayCell* cell = (FXCDayCell*)[self.collectionView cellForItemAtIndexPath:ip];
    CGPoint cellLocation = [self.view convertPoint:aLocation toView:cell];
    
    Event* eventTapped = [cell eventObjectAtPoint:cellLocation];
    
    //Tapped on a day, not on a specific event.
    if (eventTapped == nil)
    {
        NSDate* date = [self dateForIndexPath:ip];
        [self dayViewAsPopoverForDate:date location:aLocation];
        return;
    }
    
    //Tapped on a specific event.
    if(self.popover) {
        [self.popover dismissPopoverAnimated:TRUE];
        [self performSelector:@selector(popoverControllerDidDismissPopover:) withObject:self];
    }else{
        [self eventViewAsPopover:eventTapped location:aLocation];
    }


    
    FXDebugLog(kFXLogCalendar, @"tap: %f %f", aLocation.x, aLocation.y);
}

- (void)loadGestures
{
    // Tap
    //
    UITapGestureRecognizer* aTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)];
    aTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:aTapGestureRecognizer];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setFolder:(ASCalendarFolder*)aFolder
{
    if(folder) {
        [folder removeDelegate:self];
    }
    if(aFolder) {
        folder = aFolder;
    }
}

- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    [self refreshData];
}


////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (NSDate*)date:(NSDate*)d withHours:(int)hours withMinutes:(int)minutes
{
    int tzOffset    = -[[NSTimeZone localTimeZone] secondsFromGMT];       // FIXME cache to optimize
    int offset      = hours * kSecondsInHour + minutes * 60 + tzOffset;
    return [d dateByAddingTimeInterval:offset];
}

////////////////////////////////////////////////////////////////////////////////
// CalendarDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark CalendarDelegate

- (void)prev
{
    NSDateComponents* nextMonth = [[NSDateComponents alloc] init];
    nextMonth.month = -1;
    NSDate* date = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:nextMonth toDate:self.currentDate options:0];
    [self scrollToDate:date animate:YES];
}

- (void)next
{
    NSDateComponents* nextMonth = [[NSDateComponents alloc] init];
    nextMonth.month = 1;
    NSDate* date = [[DateUtil.getSingleton currentCalendar] dateByAddingComponents:nextMonth toDate:self.currentDate options:0];
    [self scrollToDate:date animate:YES];
}

- (void)today
{    
    NSDate* now = [NSDate date];
    [self scrollToDate:now animate:YES];
}

- (void)addEvent
{
    NSDate* dateToAddEventTo = [NSDate date];

    if(self.daySelected)
    {
        dateToAddEventTo = self.daySelected;
    }
    else if (  [Calendar.calendar monthOfYear:self.currentDate] != [Calendar.calendar monthOfYear:dateToAddEventTo] || [Calendar.calendar year:self.currentDate] != [Calendar.calendar year:dateToAddEventTo])
    {
        dateToAddEventTo = self.currentDate;
    }
    
    NSInteger unitFlags = (NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit);
    NSDateComponents* dateComponents = [[[DateUtil getSingleton] currentCalendar] components:unitFlags fromDate: dateToAddEventTo];
    
    NSDate* aDate = [[[DateUtil getSingleton] currentCalendar] dateFromComponents:dateComponents];
    
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:aDate];
    
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self.navigationController presentViewController:nc animated:YES completion:nil];

}

- (void)editEvent:(Event*)anEvent
{
    EventEditController* anEventEditController = [EventEditController controllerForEvent:anEvent
                                                                         eventEditorType:kEventEditorChangeEvent
                                                                         dateForInstance:anEvent.startDate];
    [self setPopoverWithEventEditController:anEventEditController];
}

- (void)setDate:(NSDate*)aDate
{
    self.currentDate = aDate;
}

- (void)selectCalendarOfViewType:(ECalendarViewType)aCalendarViewType
{
    [[CalendarController controller] setCalendarViewControllerOfViewType:aCalendarViewType
                                                                 forDate:self.currentDate
                                                                  folder:folder
                                                                    size:self.view.bounds.size
                                                    navigationController:self.navigationController];
}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
    [self selectCalendarOfViewType:aSegmentedControl.selectedSegmentIndex];
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder;
{
}

- (void)folderSynced:(BaseFolder*)aFolder;
{
//    self.monthView.eventsArray = nil;
    [self rebuildWithReloadEvents:TRUE];
}

- (void)newObjects:(NSArray*)anObjects
{
    [[EventEngine engine] eventCacheAddEvents:anObjects];
    [self rebuildWithReloadEvents:FALSE];
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    Event* anEvent = (Event*)aBaseObject;

    // Delete all instances of objects from event cache
    [[EventEngine engine] eventCacheDeleteUid:anEvent.uid];

    // Add it back in again
    [self newObjects:[NSArray arrayWithObject:anEvent]];
}

- (void)deleteUid:(NSString*)aUid
{
    BOOL didDelete = [[EventEngine engine] eventCacheDeleteUid:aUid];
    if(didDelete) {
        [self rebuildWithReloadEvents:FALSE];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Popover Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Popover Utilities
- (void)dayViewAsPopoverForDate:(NSDate*)aDate location:(CGPoint)aLocation
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        FXCalendarDayViewPopoverContentController* aDayController = [FXCalendarDayViewPopoverContentController controller];
        UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:aDayController];
        nc.toolbarHidden = NO;
        self.dayController = aDayController;
        aDayController.delegate = aDayController;
        [aDayController setFolder:folder];
        
        self.daySelected = aDate;
        [aDayController setDate:aDate];
        CGRect aRect = CGRectMake(0.0f, 0.0f, kPopoverWidth, 480.f);
        aDayController.contentSizeForViewInPopover = aRect.size;
        aRect.origin.x      = aLocation.x;
        aRect.origin.y      = aLocation.y;
        self.popover = [self.calendarPopoverDelegate createPopover:aRect viewController:nc popoverDelegate:self presentingView:self.view];
    }
    else
    {
        FXDebugLog(kFXLogFIXME, @"FIXME dayViewAsPopover not on iPad");
    }
}


- (CGRect)popverRectForEventView:(Event*)event location:(CGPoint)aLocation
{
    CGFloat aHeight = self.eventViewController.view.frame.size.height;
    CGRect aRect = CGRectMake(0.0f, 0.0f, kPopoverWidth, aHeight);
    self.eventViewController.contentSizeForViewInPopover = aRect.size;
    aRect.origin.x      = aLocation.x;
    aRect.origin.y      = aLocation.y;
    
    return aRect;
}

- (void)eventViewAsPopover:(Event*)event location:(CGPoint)aLocation
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        self.eventViewController = [FXMeetingViewController controllerWithEvent:event];

        UINavigationController* nc =[[UINavigationController alloc] initWithRootViewController:self.eventViewController];
        nc.toolbarHidden = NO;
        
        CGRect aRect = [self popverRectForEventView:event location:aLocation];
        self.popover = [self.calendarPopoverDelegate createPopover:aRect viewController:nc popoverDelegate:self presentingView:self.view];
        
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME eventViewAsPopoverForDate not on iPad");
    }
}

#pragma mark FXCMonthViewControllerDelegate

-(NSArray*) monthViewController:(FXCMonthViewController*) monthViewController eventsForDatesFrom:(NSDate*)fromDate toDate:(NSDate*)toDate
{
    NSArray* anArray = nil;
    EventEngine* anEngine = [EventEngine engine];
    [anEngine eventCacheFromDate:fromDate toDate:toDate folder:folder];
    anArray = [anEngine eventsArrayFromDate:fromDate toDate:toDate];
	return anArray;
}
-(NSString*) monthViewControllerKeyPathForTitle:(FXCMonthViewController *)monthViewController
{
    return @"title";
}


- (void)setPopoverWithEventEditController:(EventEditController*)anEventEditController
{
    anEventEditController.popover = self.popover;
    [self.popover setContentViewController:anEventEditController animated:TRUE];
}




#pragma mark TKCalendarMonthViewDelegate



- (void)calendarMonthView:(TKCalendarMonthView*)monthView dateEntered:(NSDate*)aDate location:(CGPoint)aPoint
{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        if(!self.dayController || !self.popover) {
            [self dayViewAsPopoverForDate:aDate location:aPoint];
        }else{
            
        }
    }
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView eventViewEntered:(TKCalendarDayEventView *)anEventView location:(CGPoint)aLocation
{
    @try {
        if(!anEventView) {
            [self.popover dismissPopoverAnimated:TRUE];
            [self performSelector:@selector(popoverControllerDidDismissPopover:) withObject:self];
        }else if(!self.eventViewController || !self.popover) {
            [self eventViewAsPopover:anEventView.event location:aLocation];
        }else{
            CGRect aRect = [self popverRectForEventView:anEventView.event location:aLocation];
            
            self.popover = [self.calendarPopoverDelegate createPopover:aRect viewController:self.eventViewController popoverDelegate:self presentingView:self.collectionView];
        }
    }@catch(NSException* e) {
        [self logException:@"eventViewEntered" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDataSource

- (NSArray*)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventsForDate:(NSDate *)aDate
{
    NSMutableArray* aTKEvents = NULL;
    
    @try {
        // Range check to see if date is currently cached, if not load another month
        //
        EventEngine* anEngine = [EventEngine engine];
        [anEngine eventCacheExtendToDate:aDate folder:folder];
        
        // Build event views for all events on a given day in the data dictionary
        //
        NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
        NSArray* anEvents = [EventEngine.engine eventCacheEventsForDate:aTimelessDate];
        NSUInteger aCount = [anEvents count];
        if(anEvents && aCount > 0) {
            aTKEvents = [NSMutableArray arrayWithCapacity:aCount];
            for(int i = 0 ; i < aCount ; ++i) {
                Event* anEvent = [anEvents objectAtIndex:i];
                
                TKCalendarDayEventView *aTKEvent;
                aTKEvent = [TKCalendarDayEventView eventViewWithFrame:CGRectZero
                                                                event:anEvent
                                                            startDate:[anEvent startTimeForDate:aTimelessDate]
                                                              endDate:[anEvent endTimeForDate:aTimelessDate]
                                                                title:anEvent.title
                                                             location:anEvent.location
                                                           eventStyle:kEventStyleBalloon];
                [aTKEvents addObject:aTKEvent];
            }
        }
    }@catch (NSException* e) {
        [self logException:@"eventsForDate" exception:e];
    }
    
    return aTKEvents;
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDelegate


- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView
{
    @try {
        // Launch event editor/view for a specific event
        //
        Event* anEvent = eventView.event;
        // Show event in MeetingView if it is a meeting and its not a meeting I organized
        //
        FXMeetingViewController* aMeetingVC = [FXMeetingViewController controllerWithEvent:anEvent];
        [self.popover setContentViewController:aMeetingVC animated:TRUE];
    }@catch (NSException* e) {
        [self logException:@"eventViewWasSelected" exception:e];
    }
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline createEventForEventView:(TKCalendarDayEventView *)eventView
{
    @try {
        // Launch event editor/view to create a new event
        //
        [[CalendarController controller] setDate:eventView.startDate];
        EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:eventView.startDate];
        [self setPopoverWithEventEditController:anEventEditController];
    }@catch (NSException* e) {
        [self logException:@"createEventForEventView" exception:e];
    }
}


- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventDateWasSelected:(NSDate*)anEventDate
{
    [[CalendarController controller] setDate:anEventDate];
    
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:anEventDate];
    Event* anEvent      = anEventEditController.event;
    anEvent.startDate   = anEventDate;
    anEvent.endDate     = [[Calendar calendar] addToDate:anEventDate hours:1 minutes:0];
    [self setPopoverWithEventEditController:anEventEditController];
}
////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popover = nil;
    self.daySelected = nil;
}

@end
