/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarYearController.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "CalendarDayController.h"
#import "DateInfo.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventContainer.h"
#import "EventEditController.h"
#import "EventEngine.h"
#import "NSDate+TKCategory.h"
#import "ProgressView.h"
#import "Reachability.h"
#import "FXCalendarPopoverDelegate.h"

@interface CalendarYearController()
@property (nonatomic,strong) FXCalendarPopoverDelegate* calendarPopoverDelegate;
@end

@implementation CalendarYearController

// Properties
@synthesize folder;
@synthesize dayController;
@synthesize dayViewPopover;

// Constants
static const int kSecondsInHour = 60 * 60;
static const CGFloat kPopoverWidth = 320.f;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (CalendarYearController*)controller
{
    CalendarYearController* sController = NULL;
    
    if(!sController) {
        sController = [[CalendarYearController alloc] initWithNibName:@"CalendarYearView" bundle:[FXLSafeZone getResourceBundle]];
        sController.viewType = kCalendarYear;
    }

    return sController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        self.calendarPopoverDelegate = [[FXCalendarPopoverDelegate alloc] init];
    }
    return self;
}


- (void)dealloc
{
    if(folder) {
        [folder removeDelegate:self];
    }
    folder = nil;

}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_YEAR_EXCEPTION", @"ErrorAlert", @"Calendar Year error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CALENDAR_YEAR", @"ErrorAlert", @"Calendar Month Controller error alert view title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        
        [self loadGestures];

		if (!self.firstDayOfTheWeek) {
			self.firstDayOfTheWeek = 1;
		}
        [self setDate:[NSDate date]];
    }@catch(NSException* e) {
        [self logException:@"viewDidLoad" exception:e];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    @try {
        [super viewWillAppear:animated];
        
        // Navigation tool bar
        [[CalendarController controller] topToolbar:self.navigationItem];
        
        // Bottom tool bar
        //
        NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
        self.toolbarItems = aToolBarItems;
        
        if(folder && !isPushed) {
            [folder addDelegate:self];
			
            CalendarController* aCalendarController = [CalendarController controller];
            NSDate* aDate = [aCalendarController date];
			
            if(self.year && [self.year isEqualToDate:[[Calendar calendar] newYearsDayForDate:aDate]]) {
                [self setDate:aDate];
            }else{
                [self changeYearToDate:aDate];
            }
        }
        
        [self startReachablility];
        
    }@catch(NSException* e) {
        [self logException:@"viewWillAppear" exception:e];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    @try {
        [[CalendarController controller] calendarSelector:self.navigationItem];

        isPushed = FALSE;
        
        [super viewDidAppear:animated];
        
        [self setDate:[[CalendarController controller] date]];
    }@catch(NSException* e) {
        [self logException:@"viewDidAppear" exception:e];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    @try {
        [super viewWillDisappear:animated];
        
        [self stopReachability];
        
        if(folder && !isPushed) {
            [folder removeDelegate:self];
        }
        
        self.navigationItem.titleView = nil;
    }@catch(NSException* e) {
        [self logException:@"viewWillDisappear" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            [[CalendarController controller] setIsReachable:TRUE];
            break;
        case NotReachable:
            [[CalendarController controller] setIsReachable:FALSE];
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Gestures
////////////////////////////////////////////////////////////////////////////////
#pragma mark Gestures

- (void)didSwipeLeft:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    [self next];
}

- (void)didSwipeRight:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    [self prev];
}

- (void)didSwipeUp:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    [self next];
}

- (void)didSwipeDown:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    [self prev];
}

- (void)loadGestures
{
    // Swipe
    //
    UISwipeGestureRecognizer* aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight:)];
    aSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
    aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft:)];
    aSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
    aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeDown:)];
    aSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
    aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeUp:)];
    aSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setFolder:(ASCalendarFolder*)aFolder
{
    if(folder) {
        [folder removeDelegate:self];
    }
    if(aFolder) {
        folder = aFolder;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (void)pushEventEditController:(EventEditController*)anEventEditController
{
    isPushed = TRUE;
    [self.navigationController pushViewController:anEventEditController animated:YES];
}

- (NSDate*)date:(NSDate*)d withHours:(int)hours withMinutes:(int)minutes
{
    int tzOffset    = -[[NSTimeZone localTimeZone] secondsFromGMT];       // FIXME cache to optimize
    int offset      = hours * kSecondsInHour + minutes * 60 + tzOffset;
    return [d dateByAddingTimeInterval:offset];
}

////////////////////////////////////////////////////////////////////////////////
// CalendarDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark CalendarDelegate

- (void)prev
{
    @try {
        [self changeYearToDate:[[Calendar calendar] addYears:-1 toDate:self.year]];
    }@catch(NSException* e) {
        [self logException:@"prev" exception:e];
    }
}

- (void)next
{
    @try {
        [self changeYearToDate:[[Calendar calendar] addYears:1 toDate:self.year]];
    }@catch(NSException* e) {
        [self logException:@"next" exception:e];
    }
}

- (void)today
{
    @try {
        Calendar* aCalendar = [Calendar calendar];
        
        NSDate* aDate1 = [aCalendar newYearsDayForDate:self.year];
        
        NSDate* aToday = [NSDate date];
        NSDate* aDate2 = [aCalendar newYearsDayForDate:aToday];
        
        if([aDate1 isEqualToDate:aDate2]) {
            [self setDate:aToday];
        }else{
            [self changeYearToDate:aToday];
        }
    }@catch(NSException* e) {
        [self logException:@"today" exception:e];
    }
}

- (void)addEvent
{
    @try {
        NSDate* aDate = [[Calendar calendar] addToDate:self.dateSelected hours:12 minutes:0];
        EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:aDate];
        [self pushEventEditController:anEventEditController];
    }@catch(NSException* e) {
        [self logException:@"addEvent" exception:e];
    }
}

- (void)editEvent:(Event*)anEvent
{
    @try {
        EventEditController* anEventEditController = [EventEditController controllerForEvent:anEvent
                                                                             eventEditorType:kEventEditorChangeEvent
                                                                             dateForInstance:anEvent.startDate];
        [self pushEventEditController:anEventEditController];
    }@catch(NSException* e) {
        [self logException:@"editEvent" exception:e];
    }
}

- (void)setDate:(NSDate*)aDate
{
    @try {
        DateInfo* aDateInfo = [[DateInfo alloc] initWithDate:aDate];
        for(TKCalendarMonthView* aMonthView in self.monthViews) {
            if(aDateInfo.monthOfYear == aMonthView.month) {
                selectedMonthView = aMonthView;
                [aMonthView selectDate:aDate];
                break;
            }
        }
    }@catch(NSException* e) {
        [self logException:@"setDate" exception:e];
    }
}

- (NSDate*)dateSelected
{
    @try {
        NSDate* aDate = nil;

        if(selectedMonthView) {
            aDate = selectedMonthView.dateSelected;
        }
        
        return aDate;
    }@catch(NSException* e) {
        [self logException:@"dateSelected" exception:e];
    }
}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
    @try {
        [[CalendarController controller] setCalendarViewControllerForDate:self.dateSelected
                                                                   folder:folder
                                                                     size:self.view.bounds.size
                                                     navigationController:self.navigationController];
    }@catch(NSException* e) {
        [self logException:@"selectCalendarType" exception:e];
    }
}

- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    for(TKCalendarMonthView* aMonthView in self.monthViews) {
        NSDate* aDate = aMonthView.dateSelected;
        [aMonthView rebuildWithReloadEvents:aReloadEvents];
        if(aDate) {
            [aMonthView selectDate:aDate];
        }
    }
    [self.view setNeedsDisplay];
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder;
{
}

- (void)folderSynced:(BaseFolder*)aFolder;
{
    NSDate* aLastDayOfYear = [[Calendar calendar] lastOfYearForDate:self.year];
    [self cacheEventsFromDate:self.year toDate:aLastDayOfYear];
    [self rebuildWithReloadEvents:TRUE];
}

- (void)newObjects:(NSArray*)anObjects
{
    @try {
        [[EventEngine engine] eventCacheAddEvents:anObjects];
        [self rebuildWithReloadEvents:FALSE];
    }@catch(NSException* e) {
        [self logException:@"newObjects" exception:e];
    }
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    @try {
        Event* anEvent = (Event*)aBaseObject;
        
        // Delete all instances of objects from event cache
        [[EventEngine engine] eventCacheDeleteUid:anEvent.uid];
        
        // Add it back in again
        [self newObjects:[NSArray arrayWithObject:anEvent]];
    }@catch(NSException* e) {
        [self logException:@"changeObject" exception:e];
    }
}

- (void)deleteUid:(NSString*)aUid
{
    @try {
        BOOL didDelete = [[EventEngine engine] eventCacheDeleteUid:aUid];
        if(didDelete) {
            EventEngine* anEngine = [EventEngine engine];
            // This is called to update the tableArray that indicates if there is a dot on the date
            // on the month view, probably should do this in eventCacheDeleteUid instead
            //
            [anEngine eventCacheBuildWithEvents:nil
                                recurringEvents:nil
                                       isAppend:TRUE
                                      startDate:anEngine.startDate
                                        endDate:anEngine.endDate];
            [self rebuildWithReloadEvents:FALSE];
        }
    }@catch(NSException* e) {
        [self logException:@"deleteUid" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarYearViewController Override
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarYearViewController Override

- (void)cacheEventsFromDate:(NSDate*)startDate toDate:(NSDate*)lastDate
{
    @try {
        [[EventEngine engine] eventCacheFromDate:startDate toDate:lastDate folder:self.folder];
    } @catch(NSException* e) {
        [self logException:@"cacheEventsFromDate" exception:e];
    }
}

- (void)dayViewAsPopoverForDate:(NSDate*)aDate location:(CGPoint)aLocation monthView:(TKCalendarMonthView*)aMonthView
{
    @try {
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {

            FXCalendarDayViewPopoverContentController* aDayController = [FXCalendarDayViewPopoverContentController controller];
            UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:aDayController];
            nc.toolbarHidden = NO;
            self.dayController = aDayController;
            aDayController.delegate = aDayController;
            [aDayController setFolder:self.folder];
            
            [aDayController setDate:aDate];
            CGRect aRect = CGRectMake(0.0f, 0.0f, kPopoverWidth, 480.f);
            aDayController.contentSizeForViewInPopover = aRect.size;
            aRect.origin.x      = aLocation.x;
            aRect.origin.y      = aLocation.y;
            self.dayViewPopover = [self.calendarPopoverDelegate createPopover:aRect  viewController:nc popoverDelegate:self presentingView:self.view];
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME dayViewAsPopover not on iPad");
        }
    }@catch(NSException* e) {
        [self logException:@"calendarMonthView eventViewAsPopover" exception:e];
    }
}

- (void)eventViewAsPopover:(TKCalendarDayEventView*)anEventView location:(CGPoint)aLocation
{
    @try {
        // Not implemented
    }@catch(NSException* e) {
        [self logException:@"eventViewAsPopover" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDelegate

- (BOOL)calendarMonthView:(TKCalendarMonthView*)aMonthView dateSelected:(NSDate*)aDate touch:(UITouch *)aTouch
{
    BOOL aSelectDate = FALSE;
    @try {
        if(self.dayViewPopover) {
            [self.dayViewPopover dismissPopoverAnimated:TRUE];
            [self performSelector:@selector(popoverControllerDidDismissPopover:) withObject:self];
        }else{
            if(aMonthView != selectedMonthView) {
                if(selectedMonthView) {
                    [selectedMonthView selectDate:nil];
                }
                selectedMonthView = aMonthView;
            }
            [self dayViewAsPopoverForDate:aDate location:[aTouch locationInView:self.view] monthView:aMonthView];
            aSelectDate = TRUE;
        }
    } @catch(NSException* e) {
        [self logException:@"dateSelected" exception:e];
    }
    return aSelectDate;
}

- (BOOL)calendarMonthView:(TKCalendarMonthView*)aMonthView monthShouldChange:(NSDate*)aDate animated:(BOOL)animated
{
    return FALSE;
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView monthDidChange:(NSDate*)aDate animated:(BOOL)animated
{
    // Not implemented
}

- (void)calendarMonthView:(TKCalendarMonthView*)monthView dateEntered:(NSDate*)aDate location:(CGPoint)aPoint
{
    // Not implemented
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView eventViewSelected:(TKCalendarDayEventView *)anEventView location:(CGPoint)aLocation
{
    // Not implemented
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView eventViewEntered:(TKCalendarDayEventView *)anEventView location:(CGPoint)aLocation
{
    // Not implemented
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDataSource

- (NSArray*)calendarMonthView:(TKCalendarMonthView*)aMonthView eventsFromDate:(NSDate*)aStartDate toDate:(NSDate*)anEndDate
{
    NSArray* anArray = nil;
    
    @try {
        anArray = [[EventEngine engine] eventsArrayFromDate:aStartDate toDate:anEndDate];
    }@catch(NSException* e) {
        [self logException:@"calendarYearView eventsFromDate" exception:e];
    }
    
	return anArray;
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDataSource

- (NSArray*)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventsForDate:(NSDate *)aDate
{
    NSMutableArray* aTKEvents = NULL;
    
    @try {
        // Range check to see if date is currently cached, if not load another month
        //
        EventEngine* anEngine = [EventEngine engine];
        [anEngine eventCacheExtendToDate:aDate folder:folder];
        
        // Build event views for all events on a given day in the data dictionary
        //
        NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
        NSArray* anEvents = [EventEngine.engine eventCacheEventsForDate:aTimelessDate];

        NSUInteger aCount = [anEvents count];
        if(anEvents && aCount > 0) {
            aTKEvents = [NSMutableArray arrayWithCapacity:aCount];
            for(int i = 0 ; i < aCount ; ++i) {
                Event* anEvent = [anEvents objectAtIndex:i];
                //FXDebugLog(kFXLogActiveSync, @"eventsForDate event: %@", anEvent);
                
                TKCalendarDayEventView *aTKEvent;
                aTKEvent = [TKCalendarDayEventView eventViewWithFrame:CGRectZero
                                                                event:anEvent
                                                            startDate:[anEvent startTimeForDate:aTimelessDate]
                                                              endDate:[anEvent endTimeForDate:aTimelessDate]
                                                                title:anEvent.title
                                                             location:anEvent.location
                                                           eventStyle:kEventStyleBalloon];
                [aTKEvents addObject:aTKEvent];
            }
        }
    }@catch (NSException* e) {
        [self logException:@"eventsForDate" exception:e];
    }
    
    return aTKEvents;
}


////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.dayViewPopover = nil;
}

@end