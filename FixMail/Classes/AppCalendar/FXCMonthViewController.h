//
//  FXCMonthViewController.h
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-04-29.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXCCollectionView.h"

@class FXCMonthViewController;

@protocol FXCMonthViewCalendarEventProtocol <NSObject>
-(NSArray*) monthViewController:(FXCMonthViewController*) monthViewController eventsForDatesFrom:(NSDate*)fromDate toDate:(NSDate*)toDate;
-(NSString*) monthViewControllerKeyPathForTitle:(FXCMonthViewController *)monthViewController;
-(BOOL)calendarMonthView:(FXCMonthViewController*) monthView dateSelected:(NSDate*)aDate touch:(UITouch *)aTouch;
@end

@interface FXCMonthViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout,FXCCollectionViewDelegate>

+(instancetype)superController;

@property (nonatomic,assign) id<FXCMonthViewCalendarEventProtocol> eventDelegate;
@property (nonatomic,strong) NSDate* currentDate;

-(void) scrollToDate:(NSDate*)date animate:(BOOL) animate;
-(NSDate*) dateForIndexPath:(NSIndexPath*)indexPath;
-(void) refreshData;
@end
