//
//  FXMeetingViewTitleCell.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXMeetingViewTitleCell : UITableViewCell

@property (nonatomic,strong) NSString* title;

-(float)heightRequiredForCell;

@end
