/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "DatePickerController.h"
#import "DatePickerControllerIPhone.h"

@implementation DatePickerController

// Properties
@synthesize datePicker;
@synthesize popover;
@synthesize target;
@synthesize action;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

+ (DatePickerController*)datePickerForViewController:(UIViewController*)aViewController rect:(CGRect)aCellRect date:(NSDate*)aDate action:(SEL)anAction
{
    // This doesn't work on iPhone because of UIPopoverController
    //
    DatePickerController* aDatePicker = [[DatePickerController alloc] initWithNibName:@"DatePickerView" bundle:[FXLSafeZone getResourceBundle]];
    CGRect aRect = aDatePicker.view.frame;
    aDatePicker.contentSizeForViewInPopover = aRect.size;
    aDatePicker.date   = aDate;
    aDatePicker.target = aViewController;
    aDatePicker.action = anAction;
    
    UIPopoverController* aPopover = [[UIPopoverController alloc] initWithContentViewController:aDatePicker];
    aRect.origin.y      = aCellRect.origin.y + aCellRect.size.height * 0.5f;
    aRect.origin.x      = 100;
    aRect.size.width    = 10;
    aRect.size.height   = 10;
    [aPopover presentPopoverFromRect:aRect inView:aViewController.view permittedArrowDirections:UIPopoverArrowDirectionLeft animated:TRUE];
    aDatePicker.popover = aPopover;
    
    return aDatePicker;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

#if 0
// Temporarily disabled, causes infinite loop
//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [self FXinitWithNibName:nibNameOrNil];
    if (self) {
    }
    return self;
}
#endif

- (void)dealloc
{
    action = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    datePicker.minuteInterval = 5;
	datePicker.locale = [NSLocale autoupdatingCurrentLocale];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (IBAction)changeDate:(UIDatePicker*)aDatePicker
{
    if(target && action) {
        SuppressPerformSelectorLeakWarning([target performSelector:action withObject:datePicker.date]);
    }else{
        FXDebugLog(kFXLogActiveSync, @"DatePickerController action invalid");
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setDate:(NSDate *)aDate
{
    datePicker.date = aDate;
}

@end
