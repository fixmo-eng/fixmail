//
//  FXCNotificationsManager.h
//  FixMail
//
//  Created by Sean Langley on 2013-07-08.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FolderDelegate.h"
#import "BaseFolder.h"

@interface FXCNotificationsManager : NSObject <FolderDelegate>

+(instancetype) instance;
-(void) setUpFolderDelegateForFolder:(BaseFolder*)folder;
-(void) tearDownCalendarDelegate;

-(void) removeLocalNotifications;
-(void) populateLocalNotifications;


@end
