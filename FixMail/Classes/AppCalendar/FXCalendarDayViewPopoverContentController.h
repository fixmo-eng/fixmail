//
//  FXCalendarDayViewPopoverContentController.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-14.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarDayViewController.h"

@interface FXCalendarDayViewPopoverContentController : FXCalendarDayViewController
+ (FXCalendarDayViewPopoverContentController*)controller;

@end
