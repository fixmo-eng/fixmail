/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarTheme.h"

@implementation CalendarTheme

@synthesize dateColor;
@synthesize dateOffColor;
@synthesize dateInverseColor;
@synthesize backColor;
@synthesize backClearColor;
@synthesize shadowColor;
@synthesize holidayColorDark;
@synthesize holidayColorLight;
@synthesize meetingColorDark;
@synthesize meetingColorLight;
@synthesize eventColorDark;
@synthesize eventColorLight;
@synthesize eventBorder;

- (id)init
{
    if (self = [super init]) {
        self.dateColor          = [UIColor colorWithRed:59.0f/255.0f green:73.0f/255.0f blue:88.0f/255.0f alpha:1.0f];
        self.dateOffColor       = [UIColor grayColor];
        self.dateInverseColor   = [UIColor whiteColor];
        
        self.backColor          = [UIColor whiteColor];
        self.backClearColor     = [UIColor clearColor];
        self.shadowColor        = [UIColor darkGrayColor];
        
        CGFloat aNoColor        = 0.0f;
        CGFloat aHalfColor      = 128.0f/255.0f;
        CGFloat aPastelColor    = 180.0f/255.0f;
        CGFloat aFullColor      = 255.0f/255.0f;
        
        // Purple
        //
        self.eventColorDark     = [UIColor colorWithRed:aHalfColor    green:aNoColor      blue:aHalfColor    alpha:1.0f];
        self.eventColorLight    = [UIColor colorWithRed:aFullColor    green:aPastelColor  blue:aFullColor    alpha:1.0f];
#if 1
        // All events the same color
        //
        self.holidayColorDark   = eventColorDark;
        self.holidayColorLight  = eventColorLight;
        
        self.meetingColorDark   = eventColorDark;
        self.meetingColorLight  = eventColorLight;
#else
        // Holidays and meetings are different colors
        //
        // Blue
        //
        self.holidayColorDark   = [UIColor colorWithRed:aNoColor      green:aNoColor      blue:aFullColor    alpha:1.0f];
        self.holidayColorLight  = [UIColor colorWithRed:aPastelColor  green:aFullColor    blue:aFullColor    alpha:1.0f];
        
        // Gold
        self.meetingColorDark   = [UIColor colorWithRed:200.0f/255.0f green:aHalfColor    blue:aNoColor      alpha:1.0f];
        self.meetingColorLight  = [UIColor colorWithRed:aFullColor    green:aFullColor    blue:aPastelColor  alpha:1.0f];
#endif
        
        self.eventBorder        = [UIColor lightGrayColor];
    }
    return self;
}

@end
