/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "PickerDelegate.h"
#import "FolderDelegate.h"

@class ASCalendarFolder;
@class DateUtil;
@class ASEmail;

@interface InvitePickViewController : UITableViewController <FolderDelegate, UIPopoverControllerDelegate> {
    
    ASCalendarFolder*               folder;
    NSMutableArray*                 searchResults;
    NSObject<PickerDelegate>*       delegate;
    ASEmail*                        email;
    
    DateUtil*                       dateUtil;
}

// Properties
@property (nonatomic,strong) ASCalendarFolder*              folder;
@property (strong) NSMutableArray*                          searchResults;
@property (nonatomic, strong) NSObject<PickerDelegate>*     delegate;
@property (strong) ASEmail*                                 email;

// Interface
- (void)deliverSearchResults:(NSArray*)aResults;

@end