/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "DatePickerDelegate.h"
#import "Event.h"


@interface DatePickerControllerIPhone : UIViewController {
    NSObject<DatePickerDelegate>*   delegate;
    int                             section;
    int                             row;
    NSString*                       label;
    NSDate*                         actualStartTime;
    BOOL                            isRecurrenceEdit;
    Event*                          event;
    EDatePickerField                field;
    IBOutlet UIDatePicker*  __weak  datePicker;
    IBOutlet UITableView*   __weak  tableView;
    UISwitch*                       allDaySwitch;
}

@property (nonatomic, strong) NSObject<DatePickerDelegate>* delegate;
@property (nonatomic) int                                   section;
@property (nonatomic) int                                   row;
@property (nonatomic, strong) NSString*                     label;
@property (nonatomic, strong) NSDate*                       actualStartTime;
@property (nonatomic, weak, readonly) UIDatePicker*         datePicker;
@property (nonatomic) BOOL                                  isRecurrenceEdit;
@property (nonatomic, strong) Event*                        event;
@property (nonatomic, weak, readonly) UITableView*          tableView;

// Factory
+ (DatePickerControllerIPhone*)datePickerForViewController:(UIViewController*)aViewController event:(Event*)anEvent field:(EDatePickerField)aField;

// Actions
- (IBAction)changeDate:(UIDatePicker*)aDatePicker;

// Interface
- (void)setDate:(NSDate *)date;
- (void)setField:(EDatePickerField)aField;

@end