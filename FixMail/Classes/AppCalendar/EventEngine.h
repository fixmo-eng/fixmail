/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "DBaseEngine.h"

@class BaseFolder;
@class ASCalendarFolder;
@class CommitObjects;
@class Event;

@interface EventEngine : DBaseEngine {
    dispatch_queue_t queue;
}

@property (nonatomic, readonly) NSOperationQueue*       operationQueue;
@property (strong,nonatomic) NSArray*                   sortedKeys;
@property (copy,nonatomic) NSDate*                      startDate;
@property (copy,nonatomic) NSDate*                      endDate;

// Factory
+ (EventEngine*)engine;

+ (NSDateFormatter*)dateFormatter;
+ (void)clearPreparedStatements;

// Database Schema
+ (void)tableCheck;

// Utilities
- (void)addEventsForDate:(NSDate*)aDate
                 toArray:(NSMutableArray*)anArray
                  events:(NSMutableArray*)anEvents
         recurringEvents:(NSArray*)aRecurringEvents;

// Database Interface
- (Event*)eventForUid:(NSString*)aUid folder:(BaseFolder*)aFolder;
- (Event*)eventForEventUid:(NSString*)aGlobalObjectID folder:(BaseFolder*)aFolder;

- (NSMutableArray*)eventsForTimeRange:(NSDate*)aStartTime endDate:(NSDate*)anEndDate folder:(BaseFolder*)aFolder;
- (NSMutableArray*)eventsRecurring:(BaseFolder*)aFolder;

- (void)searchFor:(NSString *)aQuery folder:(BaseFolder*)aFolder withDelegate:(id)aDelegate;

- (void)storeEvent:(Event*)anEvent;
- (void)deleteEventWithUid:(NSString*)aUid;
- (void)deleteDatabaseForFolderUid:(NSString*)aUid;

// Event Cache
- (void)eventCacheAddEvents:(NSArray*)anObjects;
- (BOOL)eventCacheDeleteUid:(NSString*)aUid;
- (void)eventCacheExtendToDate:(NSDate*)aDate folder:(BaseFolder*)aFolder;
- (NSArray*)eventsArrayFromDate:(NSDate*)aStartDate toDate:anEndDate;
- (void)eventCacheFromDate:(NSDate*)aStartDate toDate:anEndDate folder:(BaseFolder*)aFolder;
- (void)eventCacheBuildWithEvents:(NSMutableArray*)anEvents
                  recurringEvents:(NSArray*)aRecurringEvents
                         isAppend:(BOOL)isAppend
                        startDate:(NSDate*)aStartDate
                          endDate:(NSDate*)anEndDate;
- (NSArray*) eventCacheEventsForDate:(NSDate*)date;

-(void) eventCacheExtendToDate:(NSDate *)aDate folder:(BaseFolder *)aFolder completion:(void (^)(void))block;


// Commit Interface
- (void)commitObjects:(CommitObjects*)aCommitObjects;



@end
