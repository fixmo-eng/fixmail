/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "Event.h"

@interface DateInfo : BaseObject {
    NSDate*         date;
    EDayOfWeek      dayOfWeek;
    NSUInteger      weekOfMonth;
    NSUInteger      weekDayOrdinal;
    NSUInteger      monthOfYear;
    NSUInteger      dayOfMonth;
}

// Properties
@property (nonatomic,strong) NSDate*    date;
@property (nonatomic) EDayOfWeek        dayOfWeek;
@property (nonatomic) NSUInteger        weekOfMonth;
@property (nonatomic) NSUInteger        weekDayOrdinal;
@property (nonatomic) NSUInteger        monthOfYear;
@property (nonatomic) NSUInteger        dayOfMonth;

// Construct/Destruct
- (id)initWithDate:(NSDate*)aDate;

@end
