/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "EmailAddress.h"

typedef enum
{
    kAttendeeResponseUnknown        = 0,
    kAttendeeResponseTentative      = 2,
    kAttendeeResponseAccept         = 3,
    kAttendeeResponseDecline        = 4,
    kAttendeeResponseNotResponded   = 5
} EAttendeeStatus;

typedef enum
{
    kAttendeeTypeUnknown            = 0,
    kAttendeeTypeRequired           = 1,
    kAttendeeTypeOptional           = 2,
    kAttendeeTypeResource           = 3
} EAttendeeType;

typedef enum
{
    kAttendeeSortSelf,
    kAttendeeSortOrganizer,
    kAttendeeSortAccepted,
    kAttendeeSortTentative,
    kAttendeeSortNotResponded,
    kAttendeeSortDeclined,
    kAttendeeSortResource
} EAttendeeSort;

@interface Attendee : EmailAddress {
    EAttendeeStatus     attendeeStatus;
    EAttendeeType       attendeeType;
    EAttendeeSort       attendeeSort;
}

// Properties
@property (nonatomic) EAttendeeStatus   attendeeStatus;
@property (nonatomic) EAttendeeType     attendeeType;
@property (nonatomic) EAttendeeSort     attendeeSort;

// Static
+ (NSString*)attendeesAsJSON:(NSArray*)anAttendees;

// Construct
- (id)init;
- (id)initWithName:(NSString*)aName address:(NSString*)anAddress;

// Interace
- (NSDictionary*)asDictionary;
- (void)fromDictionary:(NSDictionary*)aDictionary;

- (NSString*)asToString;
- (NSString*)asNameString;

- (NSString*)asICal:(BOOL)anIsInvite;

@end
