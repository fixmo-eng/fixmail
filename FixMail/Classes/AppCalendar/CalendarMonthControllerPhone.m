/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarMonthControllerPhone.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "Calendar.h"
#import "CalendarController.h"
#import "CalendarDayController.h"
#import "DateInfo.h"
#import "ErrorAlert.h"
#import "Event.h"
#import "EventEditController.h"
#import "EventEngine.h"
#import "EventViewController.h"
#import "FXMeetingViewController.h"
#import "NSDate+TKCategory.h"
#import "ProgressView.h"
#import "Reachability.h"
#import "TKCalendarDayEventView.h"
#import "TKCalendarDayTimelineView.h"
#import "DateUtil.h"

@interface CalendarMonthControllerPhone ()
@property (nonatomic,retain) NSDate* daySelected;

@end

@implementation CalendarMonthControllerPhone

// Properties
@synthesize folder;
@synthesize eventViewController;
@synthesize dayController;
@synthesize popover;
@synthesize firstDayOfTheWeek = _firstDayOfTheWeek;

// Constants
static const int kSecondsInHour = 60 * 60;
static const CGFloat kPopoverWidth = 320.f;

////////////////////////////////////////////////////////////////////////////////
// Factory 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (CalendarMonthControllerPhone*)controller
{
    CalendarMonthControllerPhone* sController = NULL;
    if(!sController) {
        sController = [[CalendarMonthControllerPhone alloc] initWithNibName:@"CalendarMonthView" bundle:[FXLSafeZone getResourceBundle]];
        sController.viewType = kCalendarMonth;
    }

    return sController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct 
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (void)dealloc 
{

    if(folder) {
        [folder removeDelegate:self];
    }
    folder = nil;
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_MONTH", @"ErrorAlert", @"Calendar Month Controller error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:FXLLocalizedStringFromTable(@"CALENDAR_MONTH_ERROR", @"ErrorAlert", @"Calendar Month Controller error alert view title") 
                                                          message:anErrorMessage
                                                         delegate:self
                                                cancelButtonTitle:FXLLocalizedStringFromTable(@"CANCEL", @"Calendar", @"Calendar Month Controller error alert view cancel button title")
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView*)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// View 
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
	[super viewDidLoad];
    
    [self loadGestures];
    
    // Set current date by default
    @try {
        [self.monthView selectDate:[NSDate date]];
    }@catch(NSException* e) {
        [self logException:@"viewDidLoad" exception:e];
    }
}

- (void)viewDidUnload
{
	[super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    
    @try {
        // Navigation tool bar
        [[CalendarController controller] topToolbar:self.navigationItem];
        
        // Bottom tool bar
        //
        NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
        self.toolbarItems = aToolBarItems;
		
        if(folder && !isPushed) {
            [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0.0f];
            [folder addDelegate:self];
            CalendarController* aCalendarController = [CalendarController controller];
            [self.monthView changeToMonthForDate:[aCalendarController date]];
        }
        
        [self startReachablility];

    }@catch(NSException* e) {
        [self logException:@"viewWillAppear" exception:e];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    @try {
        // Calendar selector
        [[CalendarController controller] calendarSelector:self.navigationItem];
        
        isPushed = FALSE;

        [super viewDidAppear:animated];
        
        [self setDate:[[CalendarController controller] date]];
    }@catch(NSException* e) {
        [self logException:@"viewDidAppear" exception:e];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    @try {
        [super viewWillDisappear:animated];
        
        [self stopReachability];
        
        if(folder && !isPushed) {
            [folder removeDelegate:self];
        }
        
        self.navigationItem.titleView = nil;
    }@catch(NSException* e) {
        [self logException:@"viewWillDisappear" exception:e];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    CalendarController* aController = [CalendarController controller];
    UIDeviceOrientation anInterfaceOrientation = [UIDevice currentDevice].orientation;
    switch(anInterfaceOrientation) {
        case UIDeviceOrientationLandscapeLeft:
        case UIDeviceOrientationLandscapeRight:
            [self selectCalendarOfViewType:kCalendarDayWeek];
            break;
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationPortraitUpsideDown:
            [aController setSelectedCalendar:kCalendarMonth];
            break;
        default:
            FXDebugLog(kFXLogCalendar, @"FIXME CalendarMonthController orientation");
            break;
    }
    [self selectCalendarType:nil];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    @try {
        if(self.monthView) {
            CGRect aRect            = self.view.frame;
            self.monthView.frame    = aRect;
            [self.monthView setTileSize:aRect.size];
            [self.monthView rebuild];
        }
    }@catch(NSException* e) {
        [self logException:@"calendarMonthView willRotateToInterfaceOrientation" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            [[CalendarController controller] setIsReachable:TRUE];
            break;
        case NotReachable:
            [[CalendarController controller] setIsReachable:FALSE];
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Gestures
////////////////////////////////////////////////////////////////////////////////
#pragma mark Gestures

- (void)didSwipeLeft:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    @try {
        self.monthView.scrollDirection = kScrollHorizontal;
        [self next];
    } @catch(NSException* e) {
        [self logException:@"didSwipeLeft" exception:e];
    }
}

- (void)didSwipeRight:(UITapGestureRecognizer*)aTapGestureRecognizer
{
    @try {
        self.monthView.scrollDirection = kScrollHorizontal;
        [self prev];
    } @catch(NSException* e) {
        [self logException:@"didSwipeRight" exception:e];
    }
}


- (void)loadGestures
{
    // Swipe left/right
    //
    UISwipeGestureRecognizer* aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight:)];
    aSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
    aSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft:)];
    aSwipeGestureRecognizer.direction =  UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:aSwipeGestureRecognizer];
    
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setFolder:(ASCalendarFolder*)aFolder
{
    if(folder) {
        [folder removeDelegate:self];
    }
    if(aFolder) {
        folder = aFolder;
    }
}

- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    NSDate* aDate = self.monthView.dateSelected;
    [self.monthView rebuildWithReloadEvents:aReloadEvents];
    if(aDate) {
        [self.monthView selectDate:aDate];
    } 
}

- (void)pushViewController:(UIViewController*)aViewController
{
    isPushed = TRUE;
    [self presentViewController:aViewController animated:YES completion:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (NSDate*)date:(NSDate*)d withHours:(int)hours withMinutes:(int)minutes
{
    int tzOffset    = -[[NSTimeZone localTimeZone] secondsFromGMT];       // FIXME cache to optimize
    int offset      = hours * kSecondsInHour + minutes * 60 + tzOffset;
    return [d dateByAddingTimeInterval:offset];
}

////////////////////////////////////////////////////////////////////////////////
// CalendarDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark CalendarDelegate

- (void)prev
{
    [self.monthView prev];
}

- (void)next
{
    [self.monthView next];
}

- (void)today
{
    [self.monthView changeToMonthForDate:[NSDate date]];
}

- (void)addEvent
{
    NSDate* dateToAddEventTo = [NSDate date];

    if (self.monthView.dateSelected)
    {
        dateToAddEventTo = self.monthView.dateSelected;
    }
    else if (self.monthView.monthDate)
    {
        dateToAddEventTo = self.monthView.monthDate;
    }
    
    NSInteger unitFlags = (NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit);
    NSDateComponents* dateComponents = [[[DateUtil getSingleton] currentCalendar] components:unitFlags fromDate: dateToAddEventTo];
    
    NSDate* aDate = [[[DateUtil getSingleton] currentCalendar] dateFromComponents:dateComponents];
    EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:aDate];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:anEventEditController];
    nc.modalPresentationStyle = UIModalPresentationFormSheet;
    nc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self pushViewController:nc];

}

- (void)editEvent:(Event*)anEvent
{
    EventEditController* anEventEditController = [EventEditController controllerForEvent:anEvent
                                                                         eventEditorType:kEventEditorChangeEvent
                                                                         dateForInstance:anEvent.startDate];
    [self setPopoverWithEventEditController:anEventEditController];
}

- (void)setDate:(NSDate*)aDate
{
    [self.monthView selectDate:aDate];
}

- (void)selectCalendarOfViewType:(ECalendarViewType)aCalendarViewType
{

    [[CalendarController controller] setCalendarViewControllerOfViewType:aCalendarViewType
                                                                 forDate:self.monthView.dateSelected
                                                                  folder:folder
                                                                    size:self.view.bounds.size
                                                    navigationController:self.navigationController];
}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
    [self selectCalendarOfViewType:aSegmentedControl.selectedSegmentIndex];
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder;
{
}

- (void)folderSynced:(BaseFolder*)aFolder;
{
    self.monthView.eventsArray = nil;
    [self rebuildWithReloadEvents:TRUE];
}

- (void)newObjects:(NSArray*)anObjects
{
    [[EventEngine engine] eventCacheAddEvents:anObjects];
    [self rebuildWithReloadEvents:FALSE];
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    Event* anEvent = (Event*)aBaseObject;

    // Delete all instances of objects from event cache
    [[EventEngine engine] eventCacheDeleteUid:anEvent.uid];

    // Add it back in again
    [self newObjects:[NSArray arrayWithObject:anEvent]];
}

- (void)deleteUid:(NSString*)aUid
{
    BOOL didDelete = [[EventEngine engine] eventCacheDeleteUid:aUid];
    if(didDelete) {
        [self rebuildWithReloadEvents:FALSE];
    }
}


////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDataSource

- (NSArray*)calendarMonthView:(TKCalendarMonthView*)monthView eventsFromDate:(NSDate*)aStartDate toDate:(NSDate*)anEndDate
{
    NSArray* anArray = nil;
    
    EventEngine* anEngine = [EventEngine engine];
    [anEngine eventCacheFromDate:aStartDate toDate:anEndDate folder:folder];
    anArray = [anEngine eventsArrayFromDate:aStartDate toDate:anEndDate];
    
	return anArray;
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDelegate

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView monthDidChange:(NSDate*)aDate animated:(BOOL)animated
{

}

- (BOOL)calendarMonthView:(TKCalendarMonthView*)monthView dateSelected:(NSDate*)aDate touch:(UITouch *)aTouch
{
    BOOL aSelectDate = FALSE;
    return aSelectDate;
}

- (void)calendarMonthView:(TKCalendarMonthView*)monthView dateEntered:(NSDate*)aDate location:(CGPoint)aPoint
{
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView eventViewSelected:(TKCalendarDayEventView *)anEventView location:(CGPoint)aLocation
{
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView eventViewEntered:(TKCalendarDayEventView *)anEventView location:(CGPoint)aLocation
{
    if(!anEventView) {
    }else if(!self.eventViewController || !self.popover) {
        [self.eventViewController loadEvent:anEventView.event startDate:anEventView.startDate];
    }
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDataSource
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDataSource

- (NSArray*)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventsForDate:(NSDate *)aDate
{
    NSMutableArray* aTKEvents = NULL;
    
    @try {
        // Range check to see if date is currently cached, if not load another month
        //
        EventEngine* anEngine = [EventEngine engine];
        [anEngine eventCacheExtendToDate:aDate folder:folder];
        
        // Build event views for all events on a given day in the data dictionary
        //
        NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
        NSArray* anEvents = [EventEngine.engine eventCacheEventsForDate:aTimelessDate];
        NSUInteger aCount = [anEvents count];
        if(anEvents && aCount > 0) {
            aTKEvents = [NSMutableArray arrayWithCapacity:aCount];
            for(int i = 0 ; i < aCount ; ++i) {
                Event* anEvent = [anEvents objectAtIndex:i];
                //FXDebugLog(kFXLogActiveSync, @"eventsForDate event: %@", anEvent);
                
                TKCalendarDayEventView *aTKEvent;
                aTKEvent = [TKCalendarDayEventView eventViewWithFrame:CGRectZero
                                                                event:anEvent
                                                            startDate:[anEvent startTimeForDate:aTimelessDate]
                                                              endDate:[anEvent endTimeForDate:aTimelessDate]
                                                                title:anEvent.title
                                                             location:anEvent.location
                                                           eventStyle:kEventStyleBalloon];
                [aTKEvents addObject:aTKEvent];
            }
        }
    }@catch (NSException* e) {
        [self logException:@"eventsForDate" exception:e];
    }
    
    return aTKEvents;
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarDayTimelineViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarDayTimelineViewDelegate

- (void)setPopoverWithEventEditController:(EventEditController*)anEventEditController
{
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView
{
    @try {
        // Launch event editor/view for a specific event
        //
        Event* anEvent = eventView.event;
        // Show event in MeetingView if it is a meeting and its not a meeting I organized
        //
        FXMeetingViewController* aMeetingVC = [FXMeetingViewController controllerWithEvent:anEvent];
        [self.popover setContentViewController:aMeetingVC animated:TRUE];
    }@catch (NSException* e) {
        [self logException:@"eventViewWasSelected" exception:e];
    }
}

- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline createEventForEventView:(TKCalendarDayEventView *)eventView
{
    @try {
        // Launch event editor/view to create a new event
        //
        [[CalendarController controller] setDate:eventView.startDate];
        EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:eventView.startDate];
        [self setPopoverWithEventEditController:anEventEditController];
    }@catch (NSException* e) {
        [self logException:@"createEventForEventView" exception:e];
    }
}


- (void)calendarDayTimelineView:(TKCalendarDayTimelineView*)calendarDayTimeline eventDateWasSelected:(NSDate*)anEventDate
{
    @try {
        [[CalendarController controller] setDate:anEventDate];
        
        //FXDebugLog(kFXLogActiveSync, @"eventDateWasSelected: %@", anEventDate);
        EventEditController* anEventEditController = [EventEditController createEventForFolder:folder date:anEventDate];
        Event* anEvent      = anEventEditController.event;
        anEvent.startDate   = anEventDate;
        anEvent.endDate     = [[Calendar calendar] addToDate:anEventDate hours:1 minutes:0];
        [self setPopoverWithEventEditController:anEventEditController];
    }@catch (NSException* e) {
        [self logException:@"eventDateWasSelected" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popover = nil;
}

@end
