//
//  FXCalendarAgendaViewController.h
//  FixMail
//
//  Created by Sean Langley on 2013-07-02.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASCalendarFolder.h"
#import "CalendarDelegate.h"
#import "FXCalendarUpdateDelegate.h"
#import "FXOutlinedView.h"

@interface FXCalendarAgendaViewController : UIViewController <CalendarDelegate, FXCalendarUpdateDelegate>

@property (nonatomic,strong) ASCalendarFolder* folder;
@property (nonatomic,strong) NSDate* currentDate;

@property (weak, nonatomic) IBOutlet FXOutlinedView *listView;
@property (weak, nonatomic) IBOutlet FXOutlinedView *dayView;

@property (weak, nonatomic) IBOutlet UILabel *titleView;
@end
