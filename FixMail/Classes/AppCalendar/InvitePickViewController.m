/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "InvitePickViewController.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "ASEmail.h"
#import "CalendarController.h"
#import "DateUtil.h"
#import "Event.h"
#import "MeetingRequest.h"
#import "MeetingRequestCell.h"
#import "EventEngine.h"
#import "SearchRunner.h"

@implementation InvitePickViewController

// Properties
@synthesize folder;
@synthesize searchResults;
@synthesize delegate;
@synthesize email;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        dateUtil = [DateUtil getSingleton];
    }
    return self;
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: InvitePickViewController %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self connectToInbox];
}

- (void)viewWillDisppear:(BOOL)animated
{
    [self disconnectFromInbox];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Internal
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal

- (void)_emailNoMatch
{
    self.searchResults = nil;
    if(delegate) [delegate pickerNoMatch];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

static NSInteger sortByDate(id obj1, id obj2, void *context)
{
	BOOL reverse = *((BOOL *)context);
    ASEmail* anEmail1 = (ASEmail*)obj1;
	NSDate* aDate1 = anEmail1.meetingRequest.timeStamp;
    ASEmail* anEmail2 = (ASEmail*)obj2;
	NSDate* aDate2 = anEmail2.meetingRequest.timeStamp;
    if ((NSInteger *)reverse == NO) {
        return [aDate1 compare:aDate2];
    }
    return [aDate2 compare:aDate1];
}

- (void)deliverSearchResults:(NSArray*)aResults
{
    @try {
        if(aResults.count > 0) {
            //for(ASEmail* anEmail in aResults) {
            //    FXDebugLog(kFXLogActiveSync, @"_deliverSearchResults %@ %@ \"%@\" %@ %@", anEmail.folder.displayName, anEmail.from, anEmail.subject, anEmail.attachmentJSON, anEmail.meetingRequest);
            //}
            if(!self.searchResults) {
                self.searchResults = [[NSMutableArray alloc] initWithCapacity:aResults.count];
            }
            [self.searchResults addObjectsFromArray:aResults];
        
            BOOL aReverseSort = TRUE;
            NSArray* anEventViews = [self.searchResults sortedArrayUsingFunction:sortByDate context:&aReverseSort];
            [self.searchResults removeAllObjects];
            [self.searchResults addObjectsFromArray:anEventViews];
            [self.tableView reloadData];
            if(delegate) [delegate pickerShow];
        }else{
            [self _emailNoMatch];
        }
    }@catch (NSException* e) {
        [self _logException:@"_deliverSearchResults" exception:e];
    }
}

- (void)_insertIndexPath:(NSIndexPath*)anIndexPath
{
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath] withRowAnimation:UITableViewRowAnimationTop];
}


////////////////////////////////////////////////////////////////////////////////
// FolderDelegate on Email Inbox
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate on Email Inbox

- (void)folderSyncing:(BaseFolder*)aFolder
{
}

- (void)folderSynced:(BaseFolder*)aFolder
{
}

- (void)newObjects:(NSArray*)anObjects
{
    for(ASEmail* anEmail in anObjects) {
        if(anEmail.meetingRequest && !anEmail.meetingRequest.isCanceled) {
            [self.searchResults insertObject:anEmail atIndex:0];
            NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self performSelectorOnMainThread:@selector(_insertIndexPath:) withObject:anIndexPath waitUntilDone:FALSE];
        }
    }
}

- (void)changeObject:(BaseObject*)aBaseObject
{
}

- (void)_deleteIndexPath:(NSIndexPath*)anIndexPath
{
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:anIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    if(self.searchResults.count == 0) {
        [[CalendarController controller] pickerDismiss];
    }
}

- (void)deleteUid:(NSString*)aUid
{
    for(NSUInteger i = 0 ; i < self.searchResults.count ; ++i) {
        ASEmail* anEmail = [self.searchResults objectAtIndex:i];
        if([anEmail.uid isEqualToString:aUid]) {
            [self.searchResults removeObjectAtIndex:i];
            NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self performSelectorOnMainThread:@selector(_deleteIndexPath:) withObject:anIndexPath waitUntilDone:FALSE];
            break;
        }
    }
}

- (void)connectToInbox
{
    NSArray* aFolders = [self.folder.account foldersForFolderType:kFolderTypeInbox];
    if(aFolders.count == 1) {
        BaseFolder* anInboxFolder = [aFolders objectAtIndex:0];
        [anInboxFolder addDelegate:self];
    }else{
        FXDebugLog(kFXLogActiveSync, @"InvitePickView disconnectFromInbox filed, no inbox");
    }
}

- (void)disconnectFromInbox
{
    NSArray* aFolders = [self.folder.account foldersForFolderType:kFolderTypeInbox];
    if(aFolders.count == 1) {
        BaseFolder* anInboxFolder = [aFolders objectAtIndex:0];
        [anInboxFolder removeDelegate:self];
    }else{
        FXDebugLog(kFXLogActiveSync, @"InvitePickView disconnectFromInbox failed, no inbox");
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIPopoverControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIPopoverControllerDelegate

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if(delegate) [delegate pickerViewDismissed];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

- (MeetingRequestCell*)createInviteCellFromNib
{
	NSArray* nibContents = [[FXLSafeZone getResourceBundle] loadNibNamed:@"MeetingRequestCell" owner:self options:nil];
	NSEnumerator* aNibEnumerator = [nibContents objectEnumerator];
	MeetingRequestCell* anInviteCell = nil;
	NSObject* nibItem = nil;
	while ((nibItem = [aNibEnumerator nextObject]) != nil) {
		if([nibItem isKindOfClass: [MeetingRequestCell class]]) {
			anInviteCell = (MeetingRequestCell*)nibItem;
			break;
		}
	}
	return anInviteCell;
}

- (void)response:(id)aPid
{
    UISegmentedControl* aSegmentedControl = (UISegmentedControl*)aPid;
    
    MeetingRequestCell* aCell = nil;
    UIView* aView = [[aSegmentedControl superview] superview];
    if([aView isKindOfClass:[MeetingRequestCell class]]) {
        aCell = (MeetingRequestCell*)aView;
    }else{
        FXDebugLog(kFXLogCalendar, @"MeetingRequestcell response invalid");
        return;
    }
    
    if(aCell.email) {
        switch(aSegmentedControl.selectedSegmentIndex) {
            case kResponseAccept:
                [aCell.email meetingUserResponse:kUserResponseAccept];
                break;
            case kResponseDecline:
                [aCell.email meetingUserResponse:kUserResponseDecline];
                break;
            case kResponseTentative:
                [aCell.email meetingUserResponse:kUserResponseTentativelyAccept];
                break;
        }
    }else{
        FXDebugLog(kFXLogCalendar, @"InvitePickView response email invalid");
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MeetingRequestCell";
    
    MeetingRequestCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [self createInviteCellFromNib];
        CGRect aCellRect = cell.frame;
        
        UISegmentedControl* aSegmentedControl;
        NSArray* aTitles = [Event attendeeResponseStrings];
        aSegmentedControl = [[UISegmentedControl alloc] initWithItems:aTitles];
        [aSegmentedControl addTarget:self action:@selector(response:) forControlEvents:UIControlEventValueChanged];
        CGRect aRect   = aSegmentedControl.frame;
        aRect.size.height -= 8.0f;
        aRect.origin.x = aCellRect.size.width - aRect.size.width - 10.0f;
        aRect.origin.y = aCellRect.size.height - aRect.size.height - 3.0f;
        aSegmentedControl.frame = aRect;
        cell.segmentedControl = aSegmentedControl;
        [cell.contentView addSubview:aSegmentedControl];
    }
    
    if(indexPath.section == 0 && self.searchResults.count > indexPath.row) {
        ASEmail* anEmail = [self.searchResults objectAtIndex:indexPath.row];
        BOOL isCanceled = [anEmail.meetingRequest isCanceled];
        if([anEmail.meetingRequest responseRequested]) {
            [cell.segmentedControl setHidden:FALSE];
            [cell.canceledLabel    setHidden:TRUE];
        }else{
            [cell.segmentedControl setHidden:TRUE];
            [cell.canceledLabel    setHidden:!isCanceled];
        }

        [cell loadEmail:anEmail];
	}else{
        [cell loadEmail:nil];
    }
	
    return cell;
}

@end