 /*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarController.h"
#import "AppSettings.h"
#import "ASAccount.h"
#import "ASCalendarFolder.h"
#import "ASEmailFolder.h"
#import "ASItemOperations.h"
#import "CalendarDayController.h"
#import "CalendarEmail.h"
#import "CalendarMonthController.h"
#import "CalendarMonthTableController.h"
#import "CalendarTheme.h"
#import "CalendarWeekController.h"
#import "CalendarYearController.h"
#import "EmailSearch.h"
#import "ErrorAlert.h"
#import "EventEngine.h"
#import "EventPickViewController.h"
#import "InvitePickViewController.h"
#import "MeetingRequest.h"
#import "MeetingViewController.h"
#import "MKNumberBadgeView.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
#import "SZCToolBar.h"
#import "FXCalendarPhoneListViewController.h"
#import "FXCalendarAgendaViewController.h"

#import "CoolButton.h"

@implementation CalendarController

@synthesize calendarDelegate;
@synthesize folder;
@synthesize invitationEmails;
@synthesize invitationDictionary;
@synthesize date;
@synthesize segmentedControl;
@synthesize segmentedBarButtonItem;
@synthesize addEventButtonItem;
@synthesize eventPicker;
@synthesize invitePicker;
@synthesize meetingView;
@synthesize popover;
@synthesize calendarTheme;

// Constants
static const NSUInteger kNumInviteButton = 2;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static CalendarController* sCalendarController = NULL;
static CGSize sSize;

+ (CalendarController*)controller
{
    return sCalendarController;
}

+ (CalendarController*)createControllerWithFolder:(ASCalendarFolder*)aFolder size:(CGSize)aSize color:(UIColor*)aColor;
{
    sSize = aSize;
    sCalendarController = [[CalendarController alloc] initWithFolder:aFolder color:aColor];
    return sCalendarController;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(ASCalendarFolder*)aFolder color:(UIColor*)aColor
{
    if (self = [super init]) {
        self.folder = aFolder;
        isPad = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad;
        self.segmentedControl = [self createSegmentedController:aColor];
        
        // Color Theme
        self.calendarTheme = [[CalendarTheme alloc] init];
        
        // Load open invitation emails from InBox
        //
        self.invitationEmails = [NSMutableArray arrayWithCapacity:20];
        self.invitationDictionary = [NSMutableDictionary dictionaryWithCapacity:20];
        [self invitationSearch];
        
        // Monitor incoming mails to inBox for .ics attachments or meeting requests/responses
        //
        [self connectToInbox];
		
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationLocaleDidChange:) name:NSCurrentLocaleDidChangeNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [self disconnectFromInbox];
}

- (UISegmentedControl*)createSegmentedController:(UIColor*)aColor
{
    // Segmented Control centered in title view of nav bar
    NSArray* aTitles;
    if(isPad) {
        aTitles = [NSArray arrayWithObjects:
                   FXLLocalizedStringFromTable(@"LIST", @"Calendar", @"Segmented Control option - display by list"),
                   FXLLocalizedStringFromTable(@"WEEK", @"Calendar", @"Segmented Control option - display by week"),
                   FXLLocalizedStringFromTable(@"MONTH", @"Calendar", @"Segmented Control option - display by month"),
                   FXLLocalizedStringFromTable(@"YEAR", @"Calendar", @"Segmented Control option - display by year"), nil];
    }else{
        aTitles = [NSArray arrayWithObjects:
                   FXLLocalizedStringFromTable(@"LIST", @"Calendar", @"Segmented Control option - display by list"),
                   FXLLocalizedStringFromTable(@"DAY", @"Calendar", @"Segmented Control option - display by day"),
                   FXLLocalizedStringFromTable(@"MONTH", @"Calendar", @"Segmented Control option - display by month"), nil];
    }
    UISegmentedControl* aSegmentedControl = [[UISegmentedControl alloc] initWithItems:aTitles];
    aSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    aSegmentedControl.tintColor = aColor;
        
    ECalendarViewType aViewType = [AppSettings calendarViewType];
    [aSegmentedControl setSelectedSegmentIndex:aViewType];
    
    if(!isPad) {
        self.segmentedBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aSegmentedControl];
    }

    return aSegmentedControl;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_ERROR_EXCEPTION", @"ErrorAlert", @"Calendar error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CALENDAR_ERROR_TITLE", @"ErrorAlert", @"Calendar error alert view title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (ECalendarViewType)selectedCalendar
{
    ECalendarViewType aViewType = kCalendarMonth;
    
    if(self.segmentedControl) {
        aViewType = self.segmentedControl.selectedSegmentIndex;
    }
    
    return aViewType;
}

- (void)setSelectedCalendar:(ECalendarViewType)aViewType
{
    NSInteger selectedIndex = aViewType;
    
    self.segmentedControl.selectedSegmentIndex = selectedIndex;
        
}

- (void)replaceViewController:(UIViewController*)aViewController
          splitViewController:(UISplitViewController*)aSplitViewController
{
    NSMutableArray* aViewControllers = [[aSplitViewController viewControllers] mutableCopy];
    UINavigationController* aNavigationController = [aViewControllers objectAtIndex:1];
    [self replaceViewController:aViewController navigationController:aNavigationController];
}

- (void)replaceViewController:(UIViewController*)aViewController
         navigationController:(UINavigationController*)aNavigationController
{
    UINavigationItem* aNavigationItem = aNavigationController.navigationItem;
    NSMutableArray* aViewControllers = [[aNavigationController viewControllers] mutableCopy];
    NSUInteger aLastIndex = aViewControllers.count - 1;
    [aViewControllers replaceObjectAtIndex:aLastIndex withObject:aViewController];
    if(isPad) {
        aNavigationItem.titleView = nil;
    }
    [aNavigationController setViewControllers:aViewControllers animated:NO];
}

- (UIViewController*)calendarViewControllerOfViewType:(ECalendarViewType)aCalendarType
                                              forDate:(NSDate*)aDate
                                               folder:(ASCalendarFolder*)aFolder
                                                 size:(CGSize)aSize
{
    UIViewController<CalendarDelegate>* aViewController = nil;
    
    self.date   = aDate;
    self.folder = aFolder;
	
	int firstDayOfTheWeek = [self firstDayOfTheWeek];
    NSInteger calendarType = aCalendarType;
    
    switch(calendarType) {
        case kCalendarMonth:
            
        {
            [TKCalendarMonthViewController setSize:aSize];
            id<CalendarDelegate> aMonthController;
            if(isPad) {
                aMonthController = [CalendarMonthController controller]
                ;
                [(CalendarMonthController*)aMonthController setFolder:self.folder];
            }else{
                UIDevice* aDevice = [UIDevice currentDevice];
                if(aDevice.orientation == UIDeviceOrientationLandscapeLeft
                || aDevice.orientation == UIDeviceOrientationLandscapeRight) {
                    CalendarDayController* aDayController = [CalendarWeekController controller];
					aDayController.firstDayOfTheWeek = firstDayOfTheWeek;
                    [aDayController setFolder:(ASCalendarFolder*)aFolder];
                    aViewController = (UIViewController<CalendarDelegate>*)aDayController;
                    break;
                }
                aMonthController = [CalendarMonthTableController controller];
				aMonthController.firstDayOfTheWeek = firstDayOfTheWeek;
                [(CalendarMonthTableController*)aMonthController setFolder:aFolder];
                
            }
            aViewController = (UIViewController<CalendarDelegate>*)aMonthController;
            break;
        }
        case kCalendarDayWeek:
        {
            CalendarDayController* aDayController;
            UIDevice* aDevice = [UIDevice currentDevice];
            if(aDevice.userInterfaceIdiom == UIUserInterfaceIdiomPad
            || aDevice.orientation == UIDeviceOrientationLandscapeLeft
            || aDevice.orientation == UIDeviceOrientationLandscapeRight) {
                aDayController = [CalendarWeekController controller];
            }else{
                aDayController = [CalendarDayController controller];
            }
			aDayController.firstDayOfTheWeek = firstDayOfTheWeek;
            [aDayController setFolder:(ASCalendarFolder*)aFolder];
            aViewController = (UIViewController<CalendarDelegate>*)aDayController;
            break;
        }
        case kCalendarYear:
        {
            [TKCalendarYearViewController setSize:aSize];
            CalendarYearController* aYearController = [CalendarYearController controller];
			aYearController.firstDayOfTheWeek = firstDayOfTheWeek;
            [aYearController setFolder:aFolder];
            aViewController = (UIViewController<CalendarDelegate>*)aYearController;
            break;
        }
        case kCalendarList:
        {
            if ( isPad)
            {
                FXCalendarAgendaViewController* agendaController = [[FXCalendarAgendaViewController alloc] initWithNibName:@"FXCalendarAgendaViewController" bundle:[FXLSafeZone getResourceBundle]];
                agendaController.folder = aFolder;
                agendaController.currentDate = aDate;
                aViewController = agendaController;                
            }
            else
            {
                FXCalendarPhoneListViewController* aListController = [FXCalendarPhoneListViewController controller];
                aViewController = aListController;
                aListController.folder = aFolder;
                aListController.currentDate = aDate;
            }
        }
    }
    calendarDelegate = aViewController;

    return aViewController;
}

- (UIViewController*)calendarViewControllerForDate:(NSDate*)aDate
                                            folder:(ASCalendarFolder*)aFolder
                                              size:(CGSize)aSize
{
    ECalendarViewType aCalendarViewType = (ECalendarViewType)self.segmentedControl.selectedSegmentIndex;
    [AppSettings setCalendarViewType:aCalendarViewType];
    return [self calendarViewControllerOfViewType:aCalendarViewType forDate:aDate folder:aFolder size:aSize];
}

- (void)setCalendarViewControllerForDate:(NSDate*)aDate
                                  folder:(ASCalendarFolder*)aFolder
                                    size:(CGSize)aSize
                    navigationController:(UINavigationController*)aNavigationController
{
    UIViewController* aViewController = [self calendarViewControllerForDate:aDate folder:aFolder size:aSize];
    
    [self replaceViewController:aViewController navigationController:aNavigationController];    
}

- (void)setCalendarViewControllerOfViewType:(ECalendarViewType)aCalendarType
                                    forDate:(NSDate*)aDate
                                     folder:(ASCalendarFolder*)aFolder
                                       size:(CGSize)aSize
                       navigationController:(UINavigationController*)aNavigationController
{
    UIViewController* aViewController = [self calendarViewControllerOfViewType:aCalendarType forDate:aDate folder:aFolder size:aSize];
    
    [self replaceViewController:aViewController navigationController:aNavigationController];
}

- (void)calendarSelector:(UINavigationItem*)aNavigationitem
{
    [segmentedControl removeTarget:nil action:@selector(selectCalendarType:) forControlEvents:UIControlEventValueChanged];
    if(isPad) {
        aNavigationitem.titleView = segmentedControl;
    }
    [segmentedControl addTarget:self.calendarDelegate action:@selector(selectCalendarType:) forControlEvents:UIControlEventValueChanged];
}

- (void)done
{
    [[ASCalendarFolder splitViewController] dismissViewControllerAnimated:TRUE completion:nil];
}

-(int)firstDayOfTheWeek
{
	// Note: Could also fetch this from AppSettings calendarStartDaysOn
	return [[NSCalendar currentCalendar] firstWeekday];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Toolbars
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Toolbars

- (UIButton*)createInviteButton
{
    
    // Allocate UIButton
    
    CoolButton* aButton = [CoolButton  buttonWithType:UIButtonTypeCustom];
    [aButton setTitle:FXLLocalizedStringFromTable(@"INVITES", @"Calendar", @"Title for Invites button for viewing list of pending invitations") forState:UIControlStateNormal];
    
    [aButton addTarget:self action:@selector(doInvites:) forControlEvents:UIControlEventTouchUpInside];
    [aButton setButtonColor:[UIColor colorWithWhite:80.0/255.0 alpha:1.0]];
    aButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
    
    CGSize titleSize = [aButton.titleLabel.text sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0f]];
    aButton.frame =  CGRectMake(0, 0, titleSize.width + 20, 30);
    
    
    inviteBadge = [[MKNumberBadgeView alloc] initWithFrame:CGRectMake(titleSize.width, -15.0f, 40.0f, 40.0f)];
    [aButton addSubview:inviteBadge]; //Add NKNumberBadgeView as a subview on UIButton
    
    [self updateInvitationCount];
    
    return aButton;
}

- (void)setIsReachable:(BOOL)isReachable
{
    self.addEventButtonItem.enabled = isReachable;
}

- (void)topToolbar:(UINavigationItem*)aNavigationitem
{
    if(isPad) {
        // Left
        // Done
        //
        SZCToolBar* aToolBar = [[SZCToolBar alloc] initWithFrame:CGRectMake(0.0, 0.0f, 120, 44.0f)];
        aToolBar.translucent = YES;
        
#if defined(FIXMAIL_CONTAINER) && !defined(FIXMAIL_SIDEMENU)
        UIBarButtonItem* aButtonItem = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];


#else
        UIBarButtonItem* aButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                    target:self
                                                                    action:@selector(done)];
#endif

        // Invites
        //
        UIButton* aButton = [self createInviteButton];
        inviteBarButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        inviteBarButton.tintColor = [UIColor colorWithWhite:80.0/255.0 alpha:1.0];
        
        //FIXME: TEMP
        //aButton.enabled = YES;
        //inviteBarButton.enabled = YES;
        
        [aToolBar setItems:[NSArray arrayWithObjects:aButtonItem, inviteBarButton, nil] animated:NO];
        UIBarButtonItem* aToolBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aToolBar];
        aToolBarButtonItem.tintColor = [UIColor colorWithWhite:80.0/255.0 alpha:1.0];
        [aNavigationitem setLeftBarButtonItem:aToolBarButtonItem animated:NO];
       
        // Right
        // Search
        //
        CGRect aRect = CGRectMake(0.0f, 0.0f, 200.0f, 24.0f);
        UISearchBar* aSearchBar = [[UISearchBar alloc] initWithFrame:aRect];
        aSearchBar.delegate = self;
        aSearchBar.showsCancelButton = TRUE;
        searchBarButton = [[UIBarButtonItem alloc] initWithCustomView:aSearchBar];
        aNavigationitem.rightBarButtonItem = searchBarButton;
    }else{
		// Left
		// Exit
		//
        UIBarButtonItem* anExitButton = [[SZLConcreteApplicationContainer sharedApplicationContainer] exitContainerBarButtonItem];

		// Right
        // Add
        //
        self.addEventButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                            target:self.calendarDelegate
                                                                            action:@selector(addEvent)];
        [aNavigationitem setLeftBarButtonItem:anExitButton animated:NO];
        [aNavigationitem setRightBarButtonItem:self.addEventButtonItem animated:NO];
    }
}

- (NSArray*)bottomToolbar:(UINavigationController*)aNavigationController
{
    navigationController = aNavigationController;
    aNavigationController.toolbarHidden = NO;
    aNavigationController.toolbar.tintColor = [UIColor colorWithWhite:80.0/255.0 alpha:1.0];    
    // Today button
    //
    UIBarButtonItem* aTodayButton = [[UIBarButtonItem alloc] initWithTitle:FXLLocalizedStringFromTable(@"TODAY", @"Calendar", @"Bar button item for bringing user back to today")
                                                                     style:UIBarButtonItemStyleBordered
                                                                    target:self.calendarDelegate
                                                                    action:@selector(today)];
    
    // Flexible Spacers
    //
    UIBarButtonItem* aFlexibleItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem* aFlexibleItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

    NSArray* anItems;
    if(isPad) {
        // Left Arrow
        //
        UIImage* anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/Month Calendar Left Arrow"];
        UIBarButtonItem* aLeftArrow = [[UIBarButtonItem alloc] initWithImage:anImage
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:self.calendarDelegate
                                                                      action:@selector(prev)];
        
        // Fixed Spacer
        //
        UIBarButtonItem* aFixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
        
        // Right Arrow
        //
        anImage = [UIImage imageNamed:@"FixMailRsrc.bundle/Month Calendar Right Arrow"];
        UIBarButtonItem* aRightArrow = [[UIBarButtonItem alloc] initWithImage:anImage
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self.calendarDelegate
                                                                       action:@selector(next)];
    
        
        // Add
        //
        self.addEventButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                     target:self.calendarDelegate
                                                                                     action:@selector(addEvent)];
        
        anItems = [NSArray arrayWithObjects:
                            aTodayButton,
                            aFlexibleItem1,
                            aLeftArrow, aFixedItem, aRightArrow,
                            aFlexibleItem2,
                            self.addEventButtonItem, nil];
    }else{
        // Invitation inbox
        UIButton* aButton = [self createInviteButton];
        inviteBarButton = [[UIBarButtonItem alloc] initWithCustomView:aButton];
        
        anItems = [NSArray arrayWithObjects:
                   aTodayButton,
                   aFlexibleItem1,
                   self.segmentedBarButtonItem,
                   aFlexibleItem2,
                   inviteBarButton,
                   nil];
    }
    
    return anItems;
}

////////////////////////////////////////////////////////////////////////////////
// Event Picker for search
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Picker for search

- (void)eventPickerPopover:(CGRect)aCellRect
{
    @try {
        EventPickViewController* aPicker = [[EventPickViewController alloc] initWithNibName:@"EventPickView" bundle:[FXLSafeZone getResourceBundle]];
        aPicker.delegate = self;
        aPicker.folder   = self.folder;
        CGRect aRect = aPicker.view.frame;
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            aPicker.contentSizeForViewInPopover = aRect.size;
            self.popover = [[UIPopoverController alloc] initWithContentViewController:aPicker];
            aRect.origin.y      = 0.0f;
            aRect.origin.x      = calendarDelegate.view.frame.size.width - 80.0f;
            aRect.size.width    = 10.0f;
            aRect.size.height   = 10.0f;
            self.popover.delegate = aPicker;
        }else{   
            [navigationController pushViewController:aPicker animated:YES];
        }
        self.eventPicker = aPicker;
    }@catch (NSException* e) {
        [self logException:@"eventPickerPopover" exception:e];
    }
}

- (void)doSearch:(UISearchBar*)aSearchBar
{
    NSString* aString = [NSString stringWithString:aSearchBar.text];
    if(!self.eventPicker) {
        [self eventPickerPopover:aSearchBar.frame];
    }
    pickerBarButton = searchBarButton;
    [self.eventPicker search:aString];
}

////////////////////////////////////////////////////////////////////////////////
// SearchManagerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark SearchManagerDelegate

- (void)deliverSearchResults:(NSArray *)anEmails
{
    @try {
        if(anEmails.count > 0) {
            [self newObjects:anEmails];
        }
    }@catch (NSException* e) {
        [self logException:@"deliverSearchResults" exception:e];
    }
}

- (void)deliverAdditionalResults:(NSNumber*)inMoreResults
{
}

- (BOOL)shouldDeliverObject:(NSString *)aUid
{
    return TRUE;
}

//////////////////////////////////////////////////////////////////////////////
// Invitation Search
////////////////////////////////////////////////////////////////////////////////
#pragma mark Invitation Search
 
- (void)invitationSearch
{
    @try {
        NSArray* aFolders = [self.folder.account foldersForFolderType:kFolderTypeInbox];
        if(aFolders.count == 1) {
            BaseFolder* anInboxFolder = [aFolders objectAtIndex:0];
            EmailSearch* anEmailSearch = [[EmailSearch alloc] initWithFolder:anInboxFolder delegate:self];
            [[SearchRunner getSingleton] meetingSearch:anEmailSearch];
        }else{
            FXDebugLog(kFXLogActiveSync, @"invitationSearch failed, no inbox");
        }
    }@catch (NSException* e) {
        [self logException:@"invitationSearch" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Invite Picker
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Picker for search

- (void)invitePickerPopover:(CGRect)aViewRect
{
    @try {
        InvitePickViewController* aPicker = [[InvitePickViewController alloc] initWithNibName:@"InvitePickView" bundle:[FXLSafeZone getResourceBundle]];
        aPicker.delegate = self;
        aPicker.folder   = self.folder;
        CGRect aRect = aPicker.view.frame;
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            aPicker.contentSizeForViewInPopover = aRect.size;
            self.popover = [[UIPopoverController alloc] initWithContentViewController:aPicker];
            self.popover.delegate = aPicker;
        }else{
            [navigationController pushViewController:aPicker animated:YES];
        }
        self.invitePicker = aPicker;
    }@catch (NSException* e) {
        [self logException:@"invitePickerPopover" exception:e];
    }
}

- (void)updateInvitationCount
{
    NSUInteger aCount = self.invitationEmails.count;
    if(aCount > 0) {
        [inviteBadge setHidden:FALSE];
        inviteBadge.value = aCount;
    }else{
        [inviteBadge setHidden:TRUE];
    }
}

- (void)doInvites:(UIBarButtonItem*)aBarButtonItem
{
    if(!invitePopoverVisible) {
        NSUInteger aCount = self.invitationEmails.count;
        if(aCount > 0) {
            if(!self.invitePicker) {
                CGRect aRect = CGRectMake(100.0, 0.0f, 10.0f, 0.0f);
                [self invitePickerPopover:aRect];
                [self.invitePicker performSelectorOnMainThread:@selector(deliverSearchResults:) withObject:self.invitationEmails waitUntilDone:FALSE];
            }else if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
                [navigationController pushViewController:self.invitePicker animated:YES];
            }

            pickerBarButton = inviteBarButton;
            if(isPad) {
                invitePopoverVisible = TRUE;
            }
        }else{
            // No open invitations, should probably tell user this
        }
    }else{
        invitePopoverVisible = FALSE;
        [self pickerDismiss];
    }
}

////////////////////////////////////////////////////////////////////////////////
// Meeting View
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Picker for search

- (void)meetingViewPopover:(ASEmail*)anEmail
{
    @try {
        MeetingViewController* aMeetingView = [MeetingViewController controllerWithEmail:anEmail];
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            CGRect aRect = aMeetingView.view.frame;
            aMeetingView.contentSizeForViewInPopover = aRect.size;
            aMeetingView.pickerDelegate = self;
            self.popover = [[UIPopoverController alloc] initWithContentViewController:aMeetingView];
            self.popover.delegate = aMeetingView;
            pickerBarButton = inviteBarButton;
            [self pickerShow];
        }else{
            [navigationController pushViewController:aMeetingView animated:YES];
        }
        self.meetingView = aMeetingView;
    }@catch (NSException* e) {
        [self logException:@"aMeetingView" exception:e];
    }
}

//- (void)parseICS:(ASItemOperations *)anItemOp
//{
//    NSArray* anEvents = [self.folder loadEventsFromICSData:anItemOp.fileData];
//    if(anEvents.count >= 1) {
//        Event* anICSEvent = [anEvents objectAtIndex:0];
//        ASEmail* anEmail = (ASEmail*)anItemOp.email;
//        FXDebugLog(kFXLogCalendar, @"parseCancelICS: %@", anICSEvent.eventUid);
//    }else{
//        FXDebugLog(kFXLogCalendar, @"parseCancelICS failed");
//    }
//}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate on Email Inbox
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate on Email Inbox

- (void)folderSyncing:(BaseFolder*)aFolder;
{
}

- (void)folderSynced:(BaseFolder*)aFolder;
{
}

- (void)newObjects:(NSArray*)anObjects
{
    for(ASEmail* anEmail in anObjects) {
        BOOL isInvitation = FALSE;
        
        switch(anEmail.messageClass) {
            case kMessageClassNote:
                // Ordinary email, the common case, do nothing
                break;
            case kMessageClassScheduleMeetingCanceled:
                FXDebugLog(kFXLogActiveSync, @"Meeting cancelled");
                break;
            case kMessageClassScheduleMeetingResponseNeg:
                FXDebugLog(kFXLogActiveSync, @"Meeting declined");
                break;
            case kMessageClassScheduleMeetingResponsePos:
                FXDebugLog(kFXLogActiveSync, @"Meeting accepted");
                break;
            case kMessageClassScheduleMeetingResponseTent:
                FXDebugLog(kFXLogActiveSync, @"Meeting tentative");
                break;
            case kMessageClassNotificationMeeting:
                FXDebugLog(kFXLogActiveSync, @"Meeting notification");
                break;
            case kMessageClassScheduleMeetingRequest:
                FXDebugLog(kFXLogActiveSync, @"Meeting request");
                break;
            case kMessageClassScheduleMeetingUnknown:
                FXDebugLog(kFXLogActiveSync, @"Meeting schedule unknown");
                break;
            case kMessageClassNone:
            default:
                break;
        }
        
        if(anEmail.meetingRequest) {
            MeetingRequest* aMeetingRequest = anEmail.meetingRequest;
            
            if(aMeetingRequest.recurrence) {
                // FIXME should figure out recurrence duration and compare that to current date
            }else{
                NSComparisonResult aComparison = [aMeetingRequest.endDate compare:[NSDate date]];
                if(aComparison == NSOrderedAscending) {
                    // This meeting is already over, ignore it
                    continue;
                }
            }
            
            //ASItemOperations* anItemOp = [self.folder loadICalendarAttachment:anEmail];
            //if(anItemOp) {
            //    anItemOp.target = self;
            //    anItemOp.selector = @selector(parseICS:);
            //}else{
            //    FXDebugLog(kFXLogCalendar, @"no ICS attacment");
            //}
            
            if([aMeetingRequest isRequest]) {
                //FXDebugLog(kFXLogActiveSync, @"isRequest");
            }
            if([aMeetingRequest isCanceled]) {
                // Filter out cancellation emails
                //FXDebugLog(kFXLogCalendar, @"Cancel meeting request: %@ %@", anEmail.subject, aMeetingRequest);
                [CalendarEmail removeMeeting:anEmail folder:self.folder];
                continue;
            }else if([aMeetingRequest isAccepted]) {
                //FXDebugLog(kFXLogActiveSync, @"isAccepted");
            }else if([aMeetingRequest isTentativeAccepted]) {
                //FXDebugLog(kFXLogActiveSync, @"isTentativeAccepted");
            }else if([aMeetingRequest isDeclined]) {
                //FXDebugLog(kFXLogActiveSync, @"isDeclined");
            }

            // Exchange is parsing the ICS file for us so we dont need to handle ICS attachment
            //
            isInvitation = TRUE;
            ASEmail* anExistingEmail = [self.invitationDictionary objectForKey:anEmail.meetingRequest.globalObjectId];
            if(anExistingEmail) {
                NSComparisonResult aComparison = [anExistingEmail.datetime compare:anEmail.datetime];
                if(aComparison == NSOrderedAscending) {
                    // This is newer so go with it
                    if(anEmail.meetingRequest.globalObjectId.length > 0) {
                        [self.invitationDictionary setObject:anEmail forKey:anEmail.meetingRequest.globalObjectId];
                    }else{
                        FXDebugLog(kFXLogCalendar, @"Calendar Meeting Request invalid globalObjectId: %@", anEmail);
                    }
                    [self.invitationEmails removeObject:anExistingEmail];
                }else if(aComparison == NSOrderedDescending) {
                    // Older, ignore it
                    continue;
                }else{
                    // Probably shouldn't happen
                    continue;
                }
            }else{
                if(anEmail.meetingRequest.globalObjectId.length > 0) {
                    [self.invitationDictionary  setObject:anEmail forKey:anEmail.meetingRequest.globalObjectId];
                }else{
                    FXDebugLog(kFXLogCalendar, @"Calendar Meeting Request invalid globalObjectId: %@", anEmail);
                }
            }
            //FXDebugLog(kFXLogActiveSync, @"MeetingRequest email:\n%@", anEmail);
        }else if(anEmail.hasAttachments) {
            // Server isn't building the ActiveSync meeting request so we need to load and parse
            // the .ics attachment ourselves
            //
            if([anEmail hasAttachmentWithSuffix:@".ics"]) {
                isInvitation = TRUE;
                [self.folder loadICalendarAttachment:anEmail];
            }
        }
        
        if(isInvitation) {
            [self.invitationEmails addObject:anEmail];
        }
    }
    [self updateInvitationCount];
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    // not sure I need to do this if delete is working
    //ASEmail* anEmail = (ASEmail*)aBaseObject;
    //[self deleteUid:anEmail.uid];
}

- (void)deleteUid:(NSString*)aUid
{
    for(ASEmail* anEmail in self.invitationEmails) {
        if([anEmail.uid isEqualToString:aUid]) {
            [self.invitationEmails removeObject:anEmail];
            if(anEmail.meetingRequest.globalObjectId) {
                [self.invitationDictionary removeObjectForKey:anEmail.meetingRequest.globalObjectId];
            }
            [self updateInvitationCount];
            break;
        }
    }
}

- (void)connectToInbox
{
    NSArray* aFolders = [self.folder.account foldersForFolderType:kFolderTypeInbox];
    if(aFolders.count == 1) {
        BaseFolder* anInboxFolder = [aFolders objectAtIndex:0];
        [anInboxFolder addDelegate:self];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Calendar disconnectFromInbox filed, no inbox");
    }
}

- (void)disconnectFromInbox
{
    NSArray* aFolders = [self.folder.account foldersForFolderType:kFolderTypeInbox];
    if(aFolders.count == 1) {
        BaseFolder* anInboxFolder = [aFolders objectAtIndex:0];
        [anInboxFolder removeDelegate:self];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Calendar disconnectFromInbox failed, no inbox");
    }
}

////////////////////////////////////////////////////////////////////////////////
// UISearchBarDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self doSearch:searchBar];
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return TRUE;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return TRUE;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return TRUE;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self doSearch:searchBar];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
}

////////////////////////////////////////////////////////////////////////////////
// EventPickDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark EventPickDelegate

- (void)pickerShow
{
    if(self.popover) {
        [self.popover presentPopoverFromBarButtonItem:pickerBarButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    //}else if(self.recipientView) {
        //[self.view addSubview:self.recipientView];
    }else{
        FXDebugLog(kFXLogActiveSync, @"eventShow no recipientView");
    }
}

- (void)pickerDismiss
{
    if(self.popover) {
        [self.popover dismissPopoverAnimated:TRUE];
        self.popover   = nil;
    //}else if(self.recipientView) {
        //[self.recipientView removeFromSuperview];
    }else{
        FXDebugLog(kFXLogActiveSync, @"eventPickDismiss no view");
    }
    if(self.eventPicker) {
        self.eventPicker    = nil;
    }else if(self.invitePicker) {
        self.invitePicker   = nil;
        invitePopoverVisible = FALSE;
    }
}

- (void)pickerNoMatch
{
    [self pickerDismiss];
}

- (void)pickerSelected:(BaseObject*)anObject
{
    [self pickerDismiss];
    
    if([anObject isKindOfClass:[ASEmail class]]) {
        ASEmail* anEmail = (ASEmail*)anObject;
        FXDebugLog(kFXLogActiveSync, @"Picked email: %@", anEmail);
        [self meetingViewPopover:anEmail];
        [self pickerShow];
    }else if([anObject isKindOfClass:[Event class]]) {
        Event* anEvent = (Event*)anObject;
        if(calendarDelegate) {
            [calendarDelegate editEvent:anEvent];
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"pickerSelected unknown object: %@", anObject);
    }
}

- (void)pickerViewDismissed
{
    self.popover   = nil;
    if(self.eventPicker) {
        self.eventPicker    = nil;
    }else if(self.invitePicker) {
        self.invitePicker   = nil;
        invitePopoverVisible = FALSE;
    }else if(self.meetingView) {
        self.meetingView   = nil;
        invitePopoverVisible = FALSE;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

-(void)notificationLocaleDidChange:(NSNotification *)notification
{
	self.calendarDelegate.firstDayOfTheWeek = [self firstDayOfTheWeek];
}

@end
