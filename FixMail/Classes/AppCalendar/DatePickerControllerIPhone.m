/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "DatePickerControllerIPhone.h"
#import "Calendar.h"
#import "ErrorAlert.h"
#import "EventRecurrence.h"

@implementation DatePickerControllerIPhone

// Properties
@synthesize delegate;
@synthesize section;
@synthesize row;
@synthesize label;
@synthesize actualStartTime;
@synthesize datePicker;
@synthesize isRecurrenceEdit;
@synthesize event;
@synthesize tableView;

// Constants
static NSString* kSwitchCell            = @"SwitchCell";

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory
 
+ (DatePickerControllerIPhone*)datePickerForViewController:(UIViewController*)aViewController event:(Event*)anEvent field:(EDatePickerField)aField
{
    // This should work on iPhone and iPad though its not a particularly ideal UI for iPad
    //
    DatePickerControllerIPhone* aDatePicker = [[DatePickerControllerIPhone alloc] initWithNibName:@"DatePickerViewIPhone" bundle:[FXLSafeZone getResourceBundle]];
    aDatePicker.event           = anEvent;
    aDatePicker.field           = aField;
    aDatePicker.datePicker.minuteInterval  = 5;
    
    aDatePicker.toolbarItems = [aViewController.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
    
    return aDatePicker;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"DATE_PICKER", @"ErrorAlert", @"DatePickerController error alert title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"DATE_PICKER", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    datePicker.minuteInterval = 5;
	datePicker.locale = [NSLocale autoupdatingCurrentLocale];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self setField:field]; 
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (void)_updateDates
{
    NSArray* anArray;
    if(self.isRecurrenceEdit) {
        NSIndexPath* anIndexPathStart   = [NSIndexPath indexPathForRow:0 inSection:0];
        anArray = [NSArray arrayWithObjects:anIndexPathStart, nil];
    }else{
        if(event.isAllDay) {
            NSIndexPath* anIndexPathStart   = [NSIndexPath indexPathForRow:kDatePickerStart inSection:0];
            anArray = [NSArray arrayWithObjects:anIndexPathStart, nil];
        }else{
            NSIndexPath* anIndexPathStart   = [NSIndexPath indexPathForRow:kDatePickerStart inSection:0];
            NSIndexPath* anIndexPathEnd     = [NSIndexPath indexPathForRow:kDatePickerEnd inSection:0];
            anArray = [NSArray arrayWithObjects:anIndexPathStart, anIndexPathEnd, nil];
        }
    }
    [self.tableView reloadRowsAtIndexPaths:anArray withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:field inSection:0] animated:TRUE scrollPosition:FALSE];
}

- (IBAction)changeDate:(UIDatePicker*)aDatePicker
{
    @try {
        NSDate* aNewDate = datePicker.date;
        
        if(self.isRecurrenceEdit) {
            if(delegate) {
                [delegate setDate:aNewDate section:section row:row];
            }
            [self _updateDates];
        }else{
            switch(field) {
                case kDatePickerStart:
                    if(delegate) {
                        [delegate setDate:aNewDate section:section row:kDatePickerStart];
                    }
                    [self _updateDates];
                    break;
                case kDatePickerEnd:
                    if(delegate) {
                        [delegate setDate:aNewDate section:section row:kDatePickerEnd];
                    }
                    [self _updateDates];
                    break;
                case kDatePickerAllDay:
                    break;
            }
        }
    }@catch (NSException* e) {
        [self logException:@"changeDate" exception:e];
    }
}

- (void)startDateCell:(UITableViewCell*)aCell
{
    if(self.label.length > 0) {
        aCell.textLabel.text         = self.label;
    }else if(self.event.isAllDay) {
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"DATE", @"EventEditController", @"Title of field for choosing date when event is all day");
    }else{
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"START", @"EventEditController", @"Title of field for choosing start date for event");
    }
    
    NSDate* aDate;
    if(self.isRecurrenceEdit) {
        aDate = [delegate dateForSection:self.section row:self.row];
    }else{
        aDate = [delegate dateForSection:self.section row:kDatePickerStart];
    }
    
    NSString* aDateString;
    if(self.event.isAllDay) {
        aDateString = [[Event dateFormatterTimeless] stringFromDate:aDate];
    }else{
        aDateString = [[Event dateFormatter] stringFromDate:aDate];
    }
    aCell.detailTextLabel.text   = aDateString;
    aCell.accessoryType          = UITableViewCellAccessoryNone;
}

- (void)endDateCell:(UITableViewCell*)aCell
{
    NSDate* aDate = [delegate dateForSection:self.section row:kDatePickerEnd];

    NSString* aDateString;
    if(self.event.isAllDay) {
        aCell.textLabel.text         = @"";
        aDateString = @"";
    }else{
        aCell.textLabel.text         = FXLLocalizedStringFromTable(@"END", @"EventEditController", @"Title of field for choosing end date for event");
        aDateString = [[Event dateFormatter] stringFromDate:aDate];
    }
    aCell.detailTextLabel.text   = aDateString;
    aCell.accessoryType          = UITableViewCellAccessoryNone;
}

- (UITableViewCell*)allDaySwitchCell:(UITableView*)aTableView
{
    UITableViewCell* aCell = [aTableView dequeueReusableCellWithIdentifier:kSwitchCell];
    if( aCell == nil ) {
        aCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kSwitchCell];
        aCell.textLabel.text = FXLLocalizedStringFromTable(@"ALL_DAY", @"EventEditController", @"Title of switch for choosing whether event lasts all day or not");
        aCell.selectionStyle = UITableViewCellSelectionStyleNone;
        allDaySwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
        aCell.accessoryView  = allDaySwitch;
        [allDaySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    }
    [allDaySwitch setOn:self.event.isAllDay animated:NO];
    
    return aCell;
}

- (void)switchChanged:(id)sender
{
    UISwitch* aSwitch = sender;
    BOOL isAllDay = aSwitch.on;
    if(isAllDay != self.event.isAllDay) {
        self.event.isAllDay = isAllDay;
        [self.tableView reloadData];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setDate:(NSDate *)aDate
{
    datePicker.date = aDate;
}

- (void)setField:(EDatePickerField)aField
{
    @try {
        if(self.isRecurrenceEdit) {
            field = 0;
            [self setDate:[delegate dateForSection:self.section row:kDatePickerStart]];
        }else{
            field = aField;
            switch(aField) {
                case kDatePickerStart:
                    [self setDate:[delegate dateForSection:self.section row:kDatePickerStart]];
                    break;
                case kDatePickerEnd:
                    [self setDate:[delegate dateForSection:self.section row:kDatePickerEnd]];
                    break;
                case kDatePickerAllDay:
                    break;
            }
        }
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:aField inSection:0] animated:TRUE scrollPosition:FALSE];
    }@catch (NSException* e) {
        [self logException:@"setField" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows;
    if(self.isRecurrenceEdit) {
        numRows = 1;
    }else{
        if(self.event.isAllDay) {
            numRows = 2;
        }else{
            numRows = 3;
        }
    }
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    switch((EDatePickerField)indexPath.row) {
        case kDatePickerStart:
            [self startDateCell:cell];
            break;
        case kDatePickerEnd:
            if(self.event.isAllDay) {
                cell = [self allDaySwitchCell:tableView];
            }else{
                [self endDateCell:cell];
            }
            break;
        case kDatePickerAllDay:
            cell = [self allDaySwitchCell:tableView];
            if(self.event.isAllDay) {
                datePicker.datePickerMode = UIDatePickerModeDate;
            }else{
                datePicker.datePickerMode = UIDatePickerModeDateAndTime;
            }
            break;
    }

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch((EDatePickerField)indexPath.row) {
        case kDatePickerStart:
        {
            field = kDatePickerStart;
            [self setDate:[delegate dateForSection:self.section row:kDatePickerStart]];
            break;
        }
        case kDatePickerEnd:
        {
            field = kDatePickerEnd;
            [self setDate:[delegate dateForSection:self.section row:kDatePickerEnd]];
            break;
        }
        case kDatePickerAllDay:
        {
            break;
        }
    }
}

@end
