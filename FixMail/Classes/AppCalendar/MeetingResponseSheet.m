#if 0 // Obsolete
/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "MeetingResponseSheet.h"
#import "ASEmail.h"

@implementation MeetingResponseSheet

@synthesize email;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView email:(ASEmail*)anEmail
{
    if (self = [super init]) {
        self.email      = anEmail;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:nil
                                           otherButtonTitles:
                         FXLLocalizedStringFromTable(@"ACCEPT", @"Meeting", @"Response to meeting - accept"),
                         FXLLocalizedStringFromTable(@"TENTATIVE", @"Meeting", @"Response to meeting - tentative"),
                         FXLLocalizedStringFromTable(@"DECLINE", @"Meeting", @"Response to meeting - decline"), nil];
        [anActionSheet showInView:aView];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    EUserResponse aUserResponse = buttonIndex + 1;
	switch(aUserResponse) {
        case kUserResponseAccept:
            [self.email meetingUserResponse:kUserResponseAccept];
            break;
        case kUserResponseTentativelyAccept:
            [self.email meetingUserResponse:kUserResponseTentativelyAccept];
            break;
        case kUserResponseDecline:
            [self.email meetingUserResponse:kUserResponseDecline];
            break;
        default:
            FXDebugLog(kFXLogActiveSync, @"MeetingUserReponse invalid: %d", aUserResponse);
            break;
    }
}

@end
#endif