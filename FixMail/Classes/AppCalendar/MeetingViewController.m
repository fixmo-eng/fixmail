/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "MeetingViewController.h"
#import "ASAccount.h"
#import "ASEmail.h"
#import "Attendee.h"
#import "CalendarController.h"
#import "ErrorAlert.h"
#import "EventRecurrence.h"
#import "MeetingRequest.h"
#import "Reachability.h"

@implementation MeetingViewController

// Properties
@synthesize email;
@synthesize pickerDelegate;

// Constants
static const CGFloat kMarginY       = 12.0f;
static const CGFloat kPopoverWidth  = 320.f;

+ (MeetingViewController*)controllerWithEmail:(ASEmail*)anEmail
{
    MeetingViewController* aVC = [MeetingViewController controller];
    [aVC loadEmail:anEmail];
    return aVC;
}

+ (MeetingViewController*)controllerWithEvent:(Event*)anEvent
{
    MeetingViewController* aVC = [MeetingViewController controller];
    [aVC loadEvent:anEvent];
    return aVC;
}

+ (MeetingViewController*)controller
{
    MeetingViewController* anMeetingViewController = nil;
    
    NSArray* nibContents = [[FXLSafeZone getResourceBundle] loadNibNamed:@"MeetingView" owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject* nibItem = NULL;
    while ( (nibItem = [nibEnumerator nextObject]) != NULL) {
        if ( [nibItem isKindOfClass: [MeetingViewController class]]) {
            anMeetingViewController = (MeetingViewController*) nibItem;
            break;
        }
    }
    
    return anMeetingViewController;
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startReachablility];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    [self stopReachability];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            self.segmentedControl.enabled = TRUE;
            break;
        case NotReachable:
            self.segmentedControl.enabled = FALSE;
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.email.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"MEETING", @"ErrorAlert", @"MeetingViewController error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"MEETING", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (IBAction)response:(UISegmentedControl*)aSegmentedControl
{
    switch(aSegmentedControl.selectedSegmentIndex)
    {
        case kResponseAccept:
            [self.email meetingUserResponse:kUserResponseAccept];
            break;
        case kResponseDecline:
            [self.email meetingUserResponse:kUserResponseDecline];
            break;
        case kResponseTentative:
            [self.email meetingUserResponse:kUserResponseTentativelyAccept];
            break;
    }
    if(self.pickerDelegate) {
        [self.pickerDelegate pickerDismiss];
    }else{
        // FIXME - Pop controller on iPhone?
    }
}

- (void)setResponseWithAttendee:(Attendee*)anAttendee segmentedControl:(UISegmentedControl*)aSegmentedControl
{
    if(anAttendee) {
        switch(anAttendee.attendeeStatus) {
            case kAttendeeResponseAccept:
                aSegmentedControl.selectedSegmentIndex = kResponseAccept;
                break;
            case kAttendeeResponseDecline:
                aSegmentedControl.selectedSegmentIndex = kResponseDecline;
                break;
            case kAttendeeResponseTentative:
                aSegmentedControl.selectedSegmentIndex = kResponseTentative;
                break;
            case kAttendeeResponseNotResponded:
            case kAttendeeResponseUnknown:
                aSegmentedControl.selectedSegmentIndex = -1;
                break;
        }
   }
}

- (void)setResponseWithMeetingRequest:(MeetingRequest*)aMeetingRequest segmentedControl:(UISegmentedControl*)aSegmentedControl
{
    if(aMeetingRequest.isAccepted) {
        self.segmentedControl.selectedSegmentIndex = kResponseAccept;
    }else if(aMeetingRequest.isDeclined) {
        self.segmentedControl.selectedSegmentIndex = kResponseDecline;
    }else if(aMeetingRequest.isTentativeAccepted) {
        self.segmentedControl.selectedSegmentIndex = kResponseTentative;
    }else{
        self.segmentedControl.selectedSegmentIndex = -1;
    }
}

- (void)setResponse
{
    if(self.email) {
        [self setResponseWithMeetingRequest:self.email.meetingRequest segmentedControl:self.segmentedControl];
    }else if(self.event) {
        for(Attendee* anAttendee in self.event.attendees) {
            if(anAttendee.attendeeSort == kAttendeeSortSelf) {
                [self setResponseWithAttendee:anAttendee segmentedControl:self.segmentedControl];
            }
        }
    }else{
         self.segmentedControl.selectedSegmentIndex = -1;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (UILabel*)createLabel:(CGRect)aRect
{
    UILabel* aLabel = [[UILabel alloc] initWithFrame:aRect];
    
    [self.view addSubview:aLabel];
    
    return aLabel;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (CGFloat)setPosition:(UIView*)aView y:(CGFloat)aY
{
    CGRect aRect = aView.frame;
    aRect.origin.y = aY;
    aView.frame = aRect;
    aView.hidden = FALSE;
    
    return aY + aRect.size.height + kMarginY;
}

- (void)loadEmail:(ASEmail*)anEmail
{
    self.email = anEmail;
    
    @try {
        MeetingRequest* aMeetingRequest = anEmail.meetingRequest;
        
        CGRect aRect = self.view.frame;

        // Subject
        //
        CGFloat aY = self.subjectLabel.frame.origin.y;
        CGFloat aWidth;
        if(!self.inMailView) {
            self.subjectLabel.text = anEmail.subject;
            aY = [self setPosition:self.subjectLabel y:aY];
            aWidth = kPopoverWidth;
        }else{
            self.subjectLabel.hidden = TRUE;
            aWidth = self.view.superview.frame.size.width;
        }
        aRect.size.width = aWidth;
        
        // From
        //
        if(!self.inMailView && anEmail.from.length > 0) {
            self.organizerLabel.text = anEmail.from;
            aY = [self setPosition:self.fromView y:aY];
        }else{
            [self.fromView setHidden:TRUE];
        }
        

        if(aMeetingRequest) {
            // Location
            //
            if(aMeetingRequest.location.length > 0) {
                self.locationLabel.text = aMeetingRequest.location;
                aY = [self setPosition:self.whereView y:aY];
            }else{
                self.whereView.hidden = TRUE;
            }
            
            // Starting
            //
            if(aMeetingRequest.startDate) {
                self.whenLabel.text = [[Event dateFormatter] stringFromDate:anEmail.meetingRequest.startDate];
                aY = [self setPosition:self.whenView y:aY];
            }else{
                self.whenView.hidden = TRUE;
            }
            
            // Recurrence
            //
            if(aMeetingRequest.recurrence) {
                NSString* aString = [aMeetingRequest.recurrence asString];
                if(aString.length > 0) {
                    self.recurrenceLabel.text = aString;
                    aY = [self setPosition:self.recurrenceLabel y:aY];
                }
            }else{
                self.recurrenceLabel.hidden = TRUE;
            }
        }else{
            // Look for ICS file
        }

        // Accept/Decline segmentedControl
        //
        aY = [self setPosition:self.segmentedControl y:aY];
        [self setResponse];
        
        self.webView.hidden         = TRUE;
        self.attendeeLabel.hidden   = TRUE;
        self.attendee.hidden        = TRUE;

        if(self.inMailView) {
            aRect.size.height   = aY;
            self.view.frame = aRect;
        }else{
            // Set view frame to size of controls in it
            //
            aRect.size.width    = kPopoverWidth;
            aRect.size.height   = aY;
            self.view.superview.frame = aRect;

            aRect.origin.x  = 0.0f;
            aRect.origin.y  = 0.0f;
            self.view.frame = aRect;
        }
        
    }@catch (NSException* e) {
        [self logException:@"loadEmail" exception:e];
    }
}

- (void)loadEvent:(Event*)anEvent
{
    self.event = anEvent;
    
    @try {
        CGRect aRect = self.view.frame;
        
        // Subject
        //
        CGFloat aY = self.subjectLabel.frame.origin.y;
        CGFloat aWidth;
        if(!self.inMailView) {
            self.subjectLabel.text = anEvent.title;
            aY = [self setPosition:self.subjectLabel y:aY];
            aWidth = kPopoverWidth;
        }else{
            self.subjectLabel.hidden = TRUE;
            aWidth = self.view.superview.frame.size.width;
        }
        aRect.size.width = aWidth;
        
        // From
        //
        if(!self.inMailView && anEvent.organizerName.length > 0) {
            self.organizerLabel.text = anEvent.organizerName;
            aY = [self setPosition:self.fromView y:aY];
        }else{
            [self.fromView setHidden:TRUE];
        }
        
        
        // Location
        //
        if(anEvent.location.length > 0) {
            self.locationLabel.text = anEvent.location;
            aY = [self setPosition:self.whereView y:aY];
        }else{
            self.whereView.hidden = TRUE;
        }
        
        // Starting
        //
        if(anEvent.startDate) {
            self.whenLabel.text = [[Event dateFormatter] stringFromDate:anEvent.startDate];
            aY = [self setPosition:self.whenView y:aY];
        }else{
            self.whenView.hidden = TRUE;
        }
        
        // Recurrence
        //
        if(anEvent.recurrence) {
            NSString* aString = [anEvent.recurrence asString];
            if(aString.length > 0) {
                self.recurrenceLabel.text = aString;
                aY = [self setPosition:self.recurrenceLabel y:aY];
            }
        }else{
            self.recurrenceLabel.hidden = TRUE;
        }
        
        // Accept/Decline segmentedControl
        //
        aY = [self setPosition:self.segmentedControl y:aY];
        [self setResponse];
        
        self.webView.hidden         = TRUE;
        self.attendeeLabel.hidden   = TRUE;
        self.attendee.hidden        = TRUE;
        
        if(self.inMailView) {
            aRect.size.height   = aY;
            self.view.frame = aRect;
        }else{
            // Set view frame to size of controls in it
            //
            aRect.size.width    = kPopoverWidth;
            aRect.size.height   = aY;
            self.view.superview.frame = aRect;
            
            aRect.origin.x  = 0.0f;
            aRect.origin.y  = 0.0f;
            self.view.frame = aRect;
        }
    }@catch (NSException* e) {
        [self logException:@"loadEvent" exception:e];
    }
}

@end