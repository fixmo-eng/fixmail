/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "Event.h"
#import "Attendee.h"
#import "BaseAccount.h"
#import "BaseFolder.h"
#import "Calendar.h"
#import "CalendarEmail.h"
#import "DateUtil.h"
#import "ErrorAlert.h"
#import "EventDBAccessor.h"
#import "EventEngine.h"
#import "EventException.h"
#import "EventRecurrence.h"
#import "FastWBXMLParser.h"
#import "NSData+Base64.h"
#import "NSString+SBJSON.h"
#import "NSObject+SBJSON.h"
#import "TimeZoneInfo.h"

@implementation Event

// Properties
@synthesize folder;
@synthesize uid;
@synthesize eventUid;
@synthesize title;
@synthesize location;
@synthesize startDate;
@synthesize endDate;
@synthesize created;
@synthesize lastModified;
@synthesize reminderMinutes;
@synthesize sequence;
@synthesize organizerName;
@synthesize organizerEmail;
@synthesize flags;
@synthesize recurrence;
@synthesize json;
@synthesize notesBodyType;
@synthesize notes;
@synthesize attendees;

// Flags Stored
static const NSUInteger kFlagsStoredMask    = 0x0000ffff;

static const NSUInteger kEventIsAllDay      = 0x00000001;
static const NSUInteger kBusyStatusMask     = 0x00000006;
static const NSUInteger kBusyStatusShift    = 1;
static const NSUInteger kSensitivityMask    = 0x00000018;
static const NSUInteger kSensitivityShift   = 3;
static const NSUInteger kMeetingStatusMask  = 0x000000E0;
static const NSUInteger kMeetingStatusShift = 5;
static const NSUInteger kEventIsEditable    = 0x00000100;
static const NSUInteger kEventIsHoliday     = 0x00000200;

// Keys
static NSString* kUid                       = @"uid";
static NSString* kEventUid                  = @"eventUid";

static NSString* kTitle                     = @"title";
static NSString* kLocation                  = @"location";
static NSString* kStartDate                 = @"startDate";
static NSString* kEndDate                   = @"endDate";
static NSString* kCreated                   = @"created";
static NSString* kLastModified              = @"lastModified";

static NSString* kSensitivity               = @"sensitivity";
static NSString* kBusyStatus                = @"busyStatus";
static NSString* kMeetingStatus             = @"meetingStatus";

static NSString* kReminderMinutes           = @"reminderMinutes";
static NSString* kSequence                  = @"sequence";
static NSString* kOrganizerName             = @"organizerName";
static NSString* kOrganizerEmail            = @"organizerEmail";
static NSString* kJson                      = @"json";
static NSString* kNotes                     = @"notes";

// Constants
static const int kStandardNameLength        = 64;
static const int kDaylightNameLength        = 64;
static const int kSecondsPerMinute          = 60;
static const int kMinutesPerHour            = 60;
static NSString* kFixmo                     = @"Fixmo";


////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static NSDateFormatter* sICalFormatter          = NULL;
static NSDateFormatter* sICalFormatterZulu      = NULL;
static NSDateFormatter* sICalFormatterDate      = NULL;
static NSDateFormatter* sDateFormatter          = NULL;
static NSDateFormatter* sDateFormatterTimeless  = NULL;

+ (NSDateFormatter*)iCalFormatter
{
    if(!sICalFormatter)  {
        // This the ActiveSync date format for meeting request, it seems to be different from calendar events
        //      Z is for Zulu (a.k.a. GMT)
        //      20120823T150000Z
        //
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sICalFormatter = [[NSDateFormatter alloc] init];
        [sICalFormatter setDateFormat:@"yyyyMMdd'T'HHmmss"];
        [sICalFormatter setTimeZone:[NSTimeZone localTimeZone]];
    }
    return sICalFormatter;
}

+ (NSDateFormatter*)iCalFormatterZulu
{
    if(!sICalFormatterZulu)  {
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sICalFormatterZulu = [[NSDateFormatter alloc] init];		
        [sICalFormatter setDateFormat:@"yyyyMMdd'T'HHmmss'Z'"];
        [sICalFormatterZulu setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sICalFormatterZulu;
}

+ (NSDateFormatter*)iCalFormatterDate
{
    if(!sICalFormatterDate)  {
        [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
        sICalFormatterDate = [[NSDateFormatter alloc] init];
        [sICalFormatter setDateFormat:@"yyyyMMdd'"];
        [sICalFormatterDate setTimeZone:[NSTimeZone localTimeZone]];
    }
    return sICalFormatterDate;
}

+ (NSDateFormatter*)dateFormatter
{
    if(!sDateFormatter) {
        sDateFormatter = [[NSDateFormatter alloc] init];
        [sDateFormatter setTimeStyle:NSDateFormatterShortStyle];
        [sDateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [sDateFormatter setDoesRelativeDateFormatting:TRUE];
    }
    return sDateFormatter;
}

+ (NSDateFormatter*)dateFormatterTimeless
{
    if(!sDateFormatterTimeless) {
        sDateFormatterTimeless = [[NSDateFormatter alloc] init];
        [sDateFormatterTimeless setTimeStyle:NSDateFormatterNoStyle];
        [sDateFormatterTimeless setDateStyle:NSDateFormatterMediumStyle];
        [sDateFormatterTimeless setDoesRelativeDateFormatting:TRUE];
    }
    return sDateFormatterTimeless;
}

+ (NSArray*)attendeeResponseStrings
{
    // NOTE: Must stay in sync with EResponseActions typedef in Event.h
    //
    return [NSArray arrayWithObjects:FXLLocalizedStringFromTable(@"MAYBE", @"Event", @"Response from an attendee about attending an event - Maybe"),
                                    FXLLocalizedStringFromTable(@"DECLINE", @"Event", @"Response from an attendee about attending an event - Decline"),
                                    FXLLocalizedStringFromTable(@"ACCEPT", @"Event", @"Response from an attendee about attending an event - Accept"), nil];
}

+ (NSString*)createUUID
{
    CFUUIDRef uuid = CFUUIDCreate(nil);
    NSString *uuidString = (NSString*)CFBridgingRelease(CFUUIDCreateString(nil, uuid));
    CFRelease(uuid);
    return uuidString;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(BaseFolder*)aFolder
               title:(NSString*)aTitle
            location:(NSString*)aLocation
           startDate:(NSDate*)aStartDate
             endDate:(NSDate*)anEndDate
{
    if (self = [super init]) {
        self.folder             = aFolder;
        
        self.title              = aTitle;
        self.location           = aLocation;
        
        self.startDate          = aStartDate;
        self.endDate            = anEndDate;
        
        self.reminderMinutes    = -1;
        self.sequence           = 0;
    }
    return self;
}

- (id)initWithFolder:(BaseFolder*)aFolder
{
    if (self = [super init]) {
        self.folder             = aFolder;
        self.reminderMinutes    = -1;
    }
    
    return self;
}

- (void)dealloc
{
    folder = NULL;
    flags = 0;
    
}

- (void)copyToObject:(Event*)anEvent zone:(NSZone*)zone
{
    anEvent.folder             = self.folder;
    anEvent.uid                = [self.uid mutableCopy];
    anEvent.eventUid           = [self.eventUid mutableCopy];
    anEvent.title              = [self.title mutableCopy];
    anEvent.location           = [self.location mutableCopy];
    
    anEvent.startDate          = [self.startDate copy];
    anEvent.endDate            = [self.endDate copy];
    anEvent.created            = [self.created copy];
    anEvent.lastModified       = [self.lastModified copy];
    
    anEvent.organizerName      = [self.organizerName mutableCopy];
    anEvent.organizerEmail     = [self.organizerEmail mutableCopy];
    anEvent.notes              = [self.notes mutableCopy];
    anEvent.json               = [self.json mutableCopy];

    anEvent.notesBodyType      = self.notesBodyType;
    anEvent.reminderMinutes    = self.reminderMinutes;
    anEvent.sequence           = self.sequence;
    anEvent.flags              = self.flags;
    
    anEvent.attendees          = [[NSMutableArray alloc] initWithArray:self.attendees copyItems:TRUE];
    anEvent.recurrence         = [self.recurrence copy];
}

- (id)mutableCopyWithZone:(NSZone*)zone
{
	Event* anEvent = [[Event allocWithZone:zone] init];
	[self copyToObject:anEvent zone:zone];
	return anEvent;
}

- (id)mutableCopy
{
	return [self mutableCopyWithZone:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"EVENT", @"ErrorAlert", @"Event error alert view title") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"EVENT", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)changed
{
    if(self.isMeeting) {
        // Bump sequence counter for vCalendar if this event has attendees
        sequence++;
    }
    
    if(self.recurrence) {
        // Update recurrence
        [self.recurrence changedEvent:self];
    }
}

- (NSDate*)endDateForStartDate:(NSDate*)aDate
{
    NSTimeInterval aTimeInterval = [self.endDate timeIntervalSinceDate:self.startDate];
    return [aDate dateByAddingTimeInterval:aTimeInterval];
}

- (NSDate*)startDateForEndDate:(NSDate*)aDate
{
    NSTimeInterval aTimeInterval = [self.startDate timeIntervalSinceDate:self.endDate];
    return [aDate dateByAddingTimeInterval:aTimeInterval];
}

-(NSString*)dateAndTimeString
{
    NSMutableString* aString = [NSMutableString stringWithString:@""];
    
    DateUtil *du = [DateUtil getSingleton];
    [aString appendFormat:@"%@ %@ ", FXLLocalizedStringFromTable(@"START", @"Event", @"Part of string specifying the start of an event"), [du shortDate:self.startDate]];
    [aString appendFormat:@"%@ %@ ", FXLLocalizedStringFromTable(@"END", @"Event", @"Part of string specifying the end of an event"), [du shortDate:self.endDate]];
    if(self.recurrence) {
        [aString appendFormat:@"%@", [self.recurrence typeAsString]];
    }
    return aString;
}

- (NSString*)asString
{
    NSMutableString* aString = [NSMutableString stringWithString:@""];
    
    // When
    //
    [aString appendFormat:@"%@\n", [self dateAndTimeString]];
    
    // Where
    //
    if(self.location.length > 0) {
        [aString appendFormat:@"%@\t%@\n", FXLLocalizedStringFromTable(@"WHERE", @"Event", @"Part of string specifying the location of an event"), self.location];
    }
    
    // Notes
    //
    if(self.notes.length > 0) {
        [aString appendString:self.notes];
        [aString appendString:@"\n"];
    }
    
    // Attendees
    if(self.attendees.count > 0) {
        [aString appendFormat:@"\%@\n", FXLLocalizedStringFromTable(@"ATTENDEES", @"Event", @"Part of string specifying who is attending an event")];
        for(Attendee* anAttendee in self.attendees) {
            NSString* aToString = [anAttendee asToString];
            if(aToString.length > 0) {
                [aString appendFormat:@"\t%@\n", aToString];
            }
        }
    }
    
    // New line
    //
    [aString appendString:@"\n"];
    
    return aString;
}

////////////////////////////////////////////////////////////////////////////////
// Attendee
////////////////////////////////////////////////////////////////////////////////
#pragma mark Attendee

- (BOOL)isMeeting
{
    // NOTE: could change this to be true if there are two or more attendees but for
    // now we call it a meeting if there are any attendees
    return self.attendees.count > 0;
}

- (BOOL)isMyMeeting
{
    if([self isMeeting]) {
        for(Attendee* anAttendee in self.attendees) {
            if(anAttendee.attendeeSort == kAttendeeSortOrganizer) {
                if([anAttendee.address isEqualToString:self.folder.account.emailAddress]) {
                    return TRUE;
                }
            }
        }
    }
    return FALSE;
}

- (void)updateAttendeeStatus:(Attendee*)anAttendee
{
    if([anAttendee.address isEqualToString:self.folder.account.emailAddress]) {
        anAttendee.attendeeSort = kAttendeeSortSelf;
    }else if([anAttendee.address isEqualToString:self.organizerEmail]) {
        anAttendee.attendeeSort = kAttendeeSortOrganizer;
    }else if(anAttendee.attendeeType == kAttendeeTypeResource) {
        anAttendee.attendeeSort = kAttendeeSortResource;
    }else{
        switch(anAttendee.attendeeStatus) {
            case kAttendeeResponseAccept:
                anAttendee.attendeeSort = kAttendeeSortAccepted; break;
            case kAttendeeResponseTentative:
                anAttendee.attendeeSort = kAttendeeSortTentative; break;
            case kAttendeeResponseDecline:
                anAttendee.attendeeSort = kAttendeeSortDeclined; break;
            case kAttendeeResponseNotResponded:
            case kAttendeeResponseUnknown:
                anAttendee.attendeeSort = kAttendeeSortNotResponded; break;
        }
    }
}

- (void)addAttendee:(Attendee*)anAttendee
{
    [self updateAttendeeStatus:anAttendee];
    if(!self.attendees) {
        self.attendees = [[NSMutableArray alloc] initWithCapacity:3];
    }
    if(![self.attendees containsObject:anAttendee]) {
        [self.attendees addObject:anAttendee];
    }else{
        FXDebugLog(kFXLogActiveSync, @"addAttendee alread added");
    }
}

- (void)removeAttendee:(Attendee*)anAttendee
{
    [self.attendees removeObject:anAttendee];
}

////////////////////////////////////////////////////////////////////////////////
// Date
////////////////////////////////////////////////////////////////////////////////
#pragma mark Date

- (NSDate*)startTimeForDate:(NSDate*)timelessDate
{
    NSDate* aDate = nil;
    
    if(self.recurrence) {
        @try {
            Calendar* aCalendar = [Calendar calendar];

            // Exceptions
            //
#warning FIXME not sure this exception stuff is correct
            EventRecurrence* aRecurrence = self.recurrence;
            if(aRecurrence.exceptions.count > 0) {
                for(EventException* anException in aRecurrence.exceptions) {
                    if(anException.startDate) {
                        if([aCalendar isDate:timelessDate sameDayAsDate:anException.startDate]) {
                            return anException.startDate;
                        }
                    }else if(anException.exceptionStartTime) {
                        if([aCalendar isDate:timelessDate sameDayAsDate:anException.exceptionStartTime]) {
                            return anException.exceptionStartTime;
                        }
                    }else{
                        FXDebugLog(kFXLogCalendar, @"startTimeForDate exception invalid: %@", anException);
                    }
                }
            }
                       
            // Simple Recurrence
            //
            NSCalendar* anNSCalendar = [aCalendar calendar];
            NSDateComponents* components1 = [anNSCalendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self.startDate];
            NSDateComponents* components2 = [anNSCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:timelessDate];
            [components1 setYear:[components2 year]];
            [components1 setMonth:[components2 month]];
            [components1 setDay:[components2 day]];
            aDate = [anNSCalendar dateFromComponents:components1];
        }@catch (NSException* e) {
            [self logException:@"startTimeForDate" exception:e];
        }
    }else{
        aDate = self.startDate;
    }

    return aDate;
}

- (NSDate*)endTimeForDate:(NSDate*)timelessDate
{
    NSDate* aDate = nil;
    
    if(self.recurrence) {
        @try {
            Calendar* aCalendar = [Calendar calendar];
            
            // Exceptions
            //
#warning FIXME not sure this exception stuff is correct
            EventRecurrence* aRecurrence = self.recurrence;
            if(aRecurrence.exceptions.count > 0) {
                for(EventException* anException in aRecurrence.exceptions) {
                    if(anException.endDate) {
                        if([aCalendar isDate:timelessDate sameDayAsDate:anException.endDate]) {
                            return anException.endDate;
                        }
                    }else if(anException.exceptionStartTime) {
#warning FIXME Compute endTimeForDate from exceptionStartTime
                        FXDebugLog(kFXLogCalendar, @"endTimeForDate FIXME exceptionStartTime: %@", anException);
                    }else{
                        FXDebugLog(kFXLogCalendar, @"endTimeForDate exception invalid: %@", anException);
                    }
                }
            }
            
            // Simple Recurrence
            //
            NSCalendar* aNSCalendar = [[Calendar calendar] calendar];
            NSDateComponents* components1 = [aNSCalendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self.endDate];
            NSDateComponents* components2 = [aNSCalendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:timelessDate];
            [components1 setYear:[components2 year]];
            [components1 setMonth:[components2 month]];
            [components1 setDay:[components2 day]];
            aDate = [aNSCalendar dateFromComponents:components1];
        }@catch (NSException* e) {
            [self logException:@"endTimeForDate" exception:e];
        }
    }else{
        aDate = self.endDate;
    }

    return aDate;
}

////////////////////////////////////////////////////////////////////////////////
// iCal/vCal
////////////////////////////////////////////////////////////////////////////////
#pragma mark iCal/vCal
    
- (NSString*)asICalendar:(EICalMethod)aMethod status:(EICalStatus)aStatus
{
    // Microsoft ActiveSync version: MS-OXCICAL
    // vCal spec: http://www.imc.org/pdi/pdiproddev.html
    // iCal spec: RFC 5545
    //
    // iCal/.ics is version 2.0, vCal/.vcs is 1.0
    // Outlook 2003 probably prefers vCal
    //
    float anICalVersion         = 2.0f;
    NSString* aCalendarString   = @"GREGORIAN";

    NSMutableString* aString = [NSMutableString stringWithCapacity:2048];
    [aString appendString:@"BEGIN:VCALENDAR\n"];
    
    // Header
    //
    NSBundle* aBundle = [NSBundle mainBundle];
    NSString* aVersion  = [aBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString* anAppName = [aBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];

    [aString appendFormat:@"PRODID:-//%@//%@ %@//%@\n", kFixmo, anAppName, aVersion, [[NSLocale preferredLanguages] objectAtIndex:0]];
    [aString appendFormat:@"VERSION:%3.1f\n",   anICalVersion];
    [aString appendFormat:@"CALSCALE:%@\n",     aCalendarString];
    
    // Method
    //
    BOOL isReply = FALSE;
    NSString* aMethodString;
    switch(aMethod) {
        case kICalMethodRequest:
            aMethodString = @"REQUEST"; break;
        case kICalMethodPublish:
            aMethodString = @"PUBLISH"; break;
        case kICalMethodReply:
            aMethodString = @"REPLY";
            isReply = TRUE;
            break;
        case kICalMethodCounter:
            aMethodString = @"COUNTER"; break;
        case kICalMethodCancel:
            aMethodString = @"CANCEL"; break;
    }
    [aString appendFormat:@"METHOD:%@\n",       aMethodString];
    
    // Time Zone
    //
#ifdef FEATURE_TIMEZONE
    TimeZoneInfo* aTimeZoneInfo = [self iCalTimeZone:aString];
#endif

    // Event
    //
    [aString appendString:@"BEGIN:VEVENT\n"]; {
        // Dates
        //
#ifdef FEATURE_TIMEZONE
        NSDateFormatter* aFormatter = [Event iCalFormatter];
        NSString* aDateString = [aFormatter stringFromDate:self.startDate];
        [aString appendFormat:@"DTSTART;TZID=%@:%@\n",  aTimeZoneInfo.name, aDateString];
        
        aDateString = [aFormatter stringFromDate:self.endDate];
        [aString appendFormat:@"DTEND;TZID=%@:%@\n",    aTimeZoneInfo.name, aDateString];
#else
        NSDateFormatter* aFormatter = [Event iCalFormatterZulu];

        NSString* aDateString = [aFormatter stringFromDate:self.startDate];
        [aString appendFormat:@"DTSTART:%@\n", aDateString];
        
        aDateString = [aFormatter stringFromDate:self.endDate];
        [aString appendFormat:@"DTEND:%@\n",  aDateString];
#endif
        
        // Recurrence
        //
        if (self.recurrence && self.recurrence.recurrenceType != kRecurrenceNone) {
            [aString appendString:[self.recurrence asICal]];
        }
        
        aDateString = [[Event iCalFormatterZulu] stringFromDate:[NSDate date]];
        [aString appendFormat:@"DTSTAMP:%@\n",          aDateString];

        // Organizer
        //
        [aString appendFormat:@"ORGANIZER;CN=%@:mailto:%@\n", self.organizerName, self.organizerEmail];
        
        // UID
        //
        if(self.eventUid.length == 0) {
            self.eventUid = [NSString stringWithFormat:@"%@@fixmo.com", [Event createUUID]];
        }
        [aString appendFormat:@"UID:%@\n", self.eventUid];
        
        // Attendees
        //
        if(self.attendees.count > 0) {
            BOOL anIsInvite = (aMethod == kICalMethodRequest);
            if(isReply) {
                for(Attendee* anAttendee in self.attendees) {
                    if(anAttendee.attendeeSort == kAttendeeSortSelf) {
                        [aString appendString:[anAttendee asICal:anIsInvite]];
                        break;
                    }
                }
            }else{
                for(Attendee* anAttendee in self.attendees) {
                    [aString appendString:[anAttendee asICal:anIsInvite]];
                }
            }
        }

        if(!self.created) {
            self.created         = [NSDate date];
        }
        
        // Current date isn't exactly right here
        aDateString = [aFormatter stringFromDate:self.created];
        [aString appendFormat:@"CREATED:%@\n",          aDateString];
        
        // Description
        if(self.notes.length > 0) {
            [aString appendFormat:@"DESCRIPTION:%@\n",  self.notes];
        }
        
        // Last modified
        if(!self.lastModified) {
            self.lastModified    = self.created;
        }
        aDateString = [aFormatter stringFromDate:self.lastModified];
        [aString appendFormat:@"LAST-MODIFIED:%@\n",    aDateString];
        
        // Location
        //
        if(self.location.length > 0) {
            [aString appendFormat:@"LOCATION:%@\n",     self.location];
        }
        
        // Sequence
        //
        [aString appendFormat:@"SEQUENCE:%d\n",         self.sequence];

        // Status
        //
        NSString* aStatusString;
        switch(aStatus) {
            case kICalStatusCancelled:
                aStatusString = @"CANCELLED"; break;
            case kICalStatusConfirmed:
                aStatusString = @"CONFIRMED"; break;
            case kICalStatusTentative:
                aStatusString = @"TENTATIVE"; break;
                
        }
        [aString appendFormat:@"STATUS:%@\n", aStatusString];

        // Summary
        //
        if(self.title.length > 0) {
            [aString appendFormat:@"SUMMARY:%@\n",      self.title];
        }
        
        [aString appendString:@"TRANSP:OPAQUE\n"];
        
        // Reminder
        //
        if(self.reminderMinutes >= 0) {
           // [self icalAlarm:aString];
        }
        
#warning FIXME iCal Misc fields
        //LANGUAGE=en-US:
        //CLASS:PUBLIC
        //PRIORITY:5
        
        // Microsoft/ActiveSync specific
        //
        if([self isAllDay]) {
            [aString appendString:@"X-MICROSOFT-CDO-ALLDAYEVENT:TRUE"];
        }
        //X-MICROSOFT-CDO-APPT-SEQUENCE:0
        //X-MICROSOFT-CDO-OWNERAPPTID:-197023780
        //X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE
        //X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY
        //X-MICROSOFT-CDO-IMPORTANCE:1
        //X-MICROSOFT-CDO-INSTTYPE:1
        //X-MICROSOFT-DISALLOW-COUNTER:FALSE
    }
    [aString appendString:@"END:VEVENT\n"];
    
    [aString appendString:@"END:VCALENDAR\n"];
    
    //FXDebugLog(kFXLogActiveSync, @"ical: %@\n", aString);
    return [CalendarEmail iCalPostProcess:aString];
}

- (TimeZoneInfo*)iCalTimeZone:(NSMutableString*)aString
{
    NSDateFormatter* aFormatter = [Event iCalFormatter];
    
    NSDate* aDate = [NSDate date];
    TimeZoneInfo* aTimeZoneInfo = [TimeZoneInfo timeZoneInfo:[NSTimeZone localTimeZone] forDate:aDate];
    
    // Use this to test a zone with no DST
    //TimeZoneInfo* aTimeZoneInfo = [TimeZoneInfo timeZoneInfoWithName:@"America/Phoenix"  forDate:aDate];
    
    Calendar* aCalendar = [Calendar calendar];
    
    [aString appendString:@"BEGIN:VTIMEZONE\n"]; {
        [aString appendFormat:@"TZID:%@\n",           aTimeZoneInfo.name];
        [aString appendFormat:@"X-LIC-LOCATION:%@\n", aTimeZoneInfo.name];
        
        if(aTimeZoneInfo.hasDaylightSavingsTime) {
            [aString appendString:@"BEGIN:DAYLIGHT\n"]; {
                NSDate* aTranstionDateToDaylight = aTimeZoneInfo.daylightTranstionDate;
                [aString appendFormat:@"TZOFFSETFROM:%@\n", [aTimeZoneInfo standardOffsetAsString]];
                [aString appendFormat:@"TZOFFSETTO:%@\n",   [aTimeZoneInfo daylightOffsetAsString]];
                [aString appendFormat:@"TZNAME:%@\n",       [aTimeZoneInfo daylightAbbreviation]];
                [aString appendFormat:@"DTSTART:%@\n",      [aFormatter stringFromDate:aTranstionDateToDaylight]];
                [aString appendFormat:@"RRULE:FREQ=YEARLY;BYMONTH=%d;BYDAY=%d%@\n",
                 [aCalendar monthOfYear:aTranstionDateToDaylight],
                 [aCalendar weekOfMonth:aTranstionDateToDaylight],
                 [aCalendar dayOfWeekAsICAL:aTranstionDateToDaylight]];
            } [aString appendString:@"END:DAYLIGHT\n"];
        }
        
        [aString appendString:@"BEGIN:STANDARD\n"]; {
            NSDate* aTranstionDateToStandard = aTimeZoneInfo.standardTranstionDate;
            if(aTimeZoneInfo.hasDaylightSavingsTime) {
                [aString appendFormat:@"TZOFFSETFROM:%@\n", [aTimeZoneInfo daylightOffsetAsString]];
            }
            [aString appendFormat:@"TZOFFSETTO:%@\n",   [aTimeZoneInfo standardOffsetAsString]];
            [aString appendFormat:@"TZNAME:%@\n",       [aTimeZoneInfo standardAbbreviation]];
            if(aTimeZoneInfo.hasDaylightSavingsTime) {
                [aString appendFormat:@"DTSTART:%@\n",  [aFormatter stringFromDate:aTranstionDateToStandard]];
                [aString appendFormat:@"RRULE:FREQ=YEARLY;BYMONTH=%d;BYDAY=%d%@\n",
                 [aCalendar monthOfYear:aTranstionDateToStandard],
                 [aCalendar weekOfMonth:aTranstionDateToStandard],
                 [aCalendar dayOfWeekAsICAL:aTranstionDateToStandard]];
            }
        }[aString appendString:@"END:STANDARD\n"];
    }
    [aString appendString:@"END:VTIMEZONE\n"];
    
    return aTimeZoneInfo;
}

- (void)icalAlarm:(NSMutableString*)aString
{
    [aString appendString:@"BEGIN:VALARM\n"];
    [aString appendString:@"ACTION:DISPLAY\n"];
    [aString appendString:@"DESCRIPTION:REMINDER\n"];
    [aString appendFormat:@"TRIGGER;RELATED=START:-PT%dM\n", self.reminderMinutes];
    [aString appendString:@"END:VALARM\n"];
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)_baseBodyParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case BASE_TYPE:
                self.notesBodyType = (EBodyType)[aParser getInt];
                break;
            case BASE_DATA:
                self.notes = [aParser getString];
                break;
            case BASE_ESTIMATED_DATA_SIZE:
            {
                NSInteger anEstimatedSize = [aParser getInt];
                #pragma unused(anEstimatedSize)
                //FXDebugLog(kFXLogActiveSync, @"Event estimated size: %d", anEstimatedSize);
                break;
            }
            case BASE_TRUNCATED:
            {
                BOOL aTruncated = [aParser getBoolTraceable];
                #pragma unused(aTruncated)
                //FXDebugLog(kFXLogActiveSync, @"Event truncated: %d", aTruncated);
                break;
            }
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)_attendeeParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    @try {
        NSMutableArray* anAttendees = [NSMutableArray arrayWithCapacity:20];
        Attendee* anAttendee = nil;
        ASTag tag;
        while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
            switch(tag) {
                case CALENDAR_ATTENDEE:
                    if(anAttendee) {
                        [self updateAttendeeStatus:anAttendee];
                        [anAttendees addObject:anAttendee];
                    }
                    anAttendee = [[Attendee alloc] init];
                    break;
                case CALENDAR_ATTENDEE_EMAIL:
                    anAttendee.address = [aParser getString];
                    break;
                case CALENDAR_ATTENDEE_NAME:
                    anAttendee.name = [aParser getString];
                    break;
                case CALENDAR_ATTENDEE_TYPE:
                    // Note this element is optional, Google Calendar often doesn't send it
                    anAttendee.attendeeType = (EAttendeeType)[aParser getInt];
                    break;
                case CALENDAR_ATTENDEE_STATUS:
                    anAttendee.attendeeStatus = (EAttendeeStatus)[aParser getInt];
                    break;
                default:
                    [aParser skipTag];
                    break;
            }
        }
        if(anAttendee) {
            [self updateAttendeeStatus:anAttendee];
            [anAttendees addObject:anAttendee];
        }
        self.attendees      = anAttendees;
    }@catch (NSException* e) {
        [self logException:@"Attendee" exception:e];
    }
}

- (void)_parseTimeZone:(NSString*)aString
{
    // Its not really clear what this field is good for except to waste bandwidth. It does that very well :)
    //
    // Base64 encoded timezone defined in [MS-ASDTYPE]
    //
    // Bias (4 bytes): The value of this field is a LONG, as specified in [MS-DTYP].
    //      The offset from UTC, in minutes. For example, the bias for Pacific Time (UTC-8) is 480.
    // StandardName (64 bytes): The value of this field is an array of 32 WCHARs, as specified in [MS-DTYP].
    //      It contains an optional description for standard time. Any unused WCHARs in the array MUST be set to 0x0000.
    // StandardDate (16 bytes): The value of this field is a SYSTEMTIME structure, as specified in [MS-DTYP].
    //      It contains the date and time when the transition from DST to standard time occurs.
    // StandardBias (4 bytes): The value of this field is a LONG.
    //      It contains the number of minutes to add to the value of the Bias field during standard time.
    // DaylightName (64 bytes): The value of this field is an array of 32 WCHARs.
    //      It contains an optional description for DST. Any unused WCHARs in the array MUST be set to 0x0000.
    // DaylightDate (16 bytes): The value of this field is a SYSTEMTIME structure.
    //      It contains the date and time when the transition from standard time to DST occurs.
    // DaylightBias (4 bytes): The value of this field is a LONG.
    //      It contains the number of minutes to add to the value of the Bias field during DST.
    // The TimeZone structure is encoded using base64 encoding prior to being inserted in an XML element.
    // Elements with a TimeZone structure MUST be encoded and transmitted as [WBXML1.2] inline strings.
    //
    Calendar* aCalendar = [Calendar calendar];
    NSData* aData = [NSData dataFromBase64String:aString];
    if(aData.length == sizeof(int)*3 + sizeof(SystemTime)*2 + kStandardNameLength + kDaylightNameLength) {
        int aBias;
        [aData getBytes:&aBias length:sizeof(int)];
        int anOffset = sizeof(int);
        
        unsigned short aStandardNameWchar[kStandardNameLength/sizeof(unsigned short)];
        [aData getBytes:aStandardNameWchar range:NSMakeRange(anOffset, kStandardNameLength)];
         NSString* aStandardName = [NSString stringWithFormat:@"%S", (const unsigned short*)aStandardNameWchar];
        anOffset += kStandardNameLength;
        
        SystemTime aStandardSystemTime;
        [aData getBytes:&aStandardSystemTime range:NSMakeRange(anOffset, sizeof(SystemTime))];
        NSDate* aStandardDate = [aCalendar dateFromSystemTime:&aStandardSystemTime];
        anOffset += sizeof(SystemTime);

        int aStandardBias;
        [aData getBytes:&aStandardBias range:NSMakeRange(anOffset, sizeof(int))];
        anOffset += sizeof(int);
        
        unsigned short aDaylightNameWchar[kDaylightNameLength/sizeof(unsigned short)];
        [aData getBytes:aDaylightNameWchar range:NSMakeRange(anOffset, kDaylightNameLength)];
        NSString* aDaylightName = [NSString stringWithFormat:@"%S", (const unsigned short*)aDaylightNameWchar];
        anOffset += kDaylightNameLength;
        
        SystemTime aDaylightSystemTime;
        [aData getBytes:&aDaylightSystemTime range:NSMakeRange(anOffset, sizeof(SystemTime))];
        NSDate* aDaylightDate = [aCalendar dateFromSystemTime:&aDaylightSystemTime];
        anOffset += sizeof(SystemTime);
        
        int aDaylightBias;
        [aData getBytes:&aDaylightBias range:NSMakeRange(anOffset, sizeof(int))];
        //anOffset += sizeof(int);
        
        FXDebugLog(kFXLogActiveSync, @"bias %d\nstandardName %@ standarddDate %@ standardBias %d\ndaylightName %@ daylightData %@ daylightBias %d",
                 aBias,
                 aStandardName, aStandardDate, aStandardBias,
                 aDaylightName, aDaylightDate, aDaylightBias);
    }else{
        FXDebugLog(kFXLogActiveSync, @"_parseTimeZone invalid length: %d", aData.length);
    }
}

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];
    
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case BASE_BODY:
                [self _baseBodyParser:aParser tag:tag];
                break;
            case CALENDAR_DTSTAMP:
            {
                /*NSString* aString = */[aParser getString];
                //NSDate* aDate = [aDateFormatter dateFromString:aString];
                //FXDebugLog(kFXLogActiveSync, @"CALENDAR_DTSTAMP: %@ %@", aString, aDate);
                break;
            }
            case CALENDAR_START_TIME:
            {
                NSString* aString = [aParser getString];
                self.startDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case CALENDAR_END_TIME:
            {
                NSString* aString = [aParser getString];
                self.endDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case CALENDAR_LOCATION:
                self.location = [aParser getString];
                break;
            case CALENDAR_SUBJECT:
                self.title = [aParser getString];
                break;
            case CALENDAR_UID:
                self.eventUid = [aParser getString];
                break;
            case CALENDAR_SENSITIVITY:
                [self setSensitivity:(ESensitivity)[aParser getInt]];
                break;
            case CALENDAR_BUSY_STATUS:
                [self setBusyStatus:(EBusyStatus)[aParser getInt]];
                break;
            case CALENDAR_ALL_DAY_EVENT:
                [self setIsAllDay:[aParser getBool]];
                break;
            case CALENDAR_REMINDER:
                self.reminderMinutes = [aParser getUnsignedInt];
                break;
            case CALENDAR_MEETING_STATUS:
                [self setMeetingStatus:(EMeetingStatus)[aParser getInt]];
                break;
            case CALENDAR_TIME_ZONE:
            {
                // Not exactly sure what to do with this
                //
                /*NSString* aString = */[aParser getString];
                //[self _parseTimeZone:aString];
                break;
            }
            case CALENDAR_ORGANIZER_EMAIL:
            {
                self.organizerEmail = [aParser getString];
                BOOL isEditable = [self.organizerEmail isEqualToString:[[folder account] userName]];
                [self setIsEditable:isEditable];
                break;
            }
            case CALENDAR_ORGANIZER_NAME:
                self.organizerName = [aParser getString];
                break;
            case CALENDAR_RECURRENCE:
            {
                EventRecurrence* anEventRecurrence = self.recurrence;
                if(!anEventRecurrence) {
                    anEventRecurrence = [[EventRecurrence alloc] init];
                    self.recurrence = anEventRecurrence; 
                }
                [anEventRecurrence parser:aParser tag:tag];
                break;
            }
            case CALENDAR_EXCEPTIONS:
                break;
            case CALENDAR_EXCEPTION:
            {
                EventException* anEventException = [[EventException alloc] init];
                [anEventException parser:aParser tag:tag event:self];
                if(self.recurrence) {
                    [self.recurrence addException:anEventException];
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME no recurrence for excption");
                }
                break;
            }
            case CALENDAR_ATTENDEES:
                [self _attendeeParser:aParser tag:tag];
                break;
            case CALENDAR_FIRST_DAY_OF_WEEK:
            {
                EFirstDayOfWeek aFirstDayOfWeek = (EFirstDayOfWeek)[aParser getUnsignedInt];
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_FIRST_DAY_OF_WEEK %d", aFirstDayOfWeek);
                break;
            }
            case CALENDAR_CALENDAR_TYPE:
            {
                ECalendarType aCalendarType = (ECalendarType)[aParser getUnsignedInt];
                switch(aCalendarType) {
                    case kCalendarTypeDefault:
                    case kCalendarTypeGregorian:
                    case kCalendarTypeGregorianUS:
                        break;
                    default:
                        FXDebugLog(kFXLogFIXME, @"FIXME Unsupported CALENDAR_TYPE: %d", aCalendarType);
                        break;
                }
                break;
            }
            case CALENDAR_RESPONSE_TYPE:
            {
                EResponseType aResponseType = (EResponseType)[aParser getIntTraceable];
                #pragma unused(aResponseType)
                //FXDebugLog(kFXLogActiveSync, @"CALENDAR_RESPONSE_TYPE %d", aResponseType);
                break;
            }
            case CALENDAR_RESPONSE_REQUESTED:
            {
                BOOL aResponseRequested = [aParser getBoolTraceable];
                #pragma unused(aResponseRequested)
                //FXDebugLog(kFXLogActiveSync, @"CALENDAR_RESPONSE_REQUESTED %d", aResponseRequested);
                break;
            }
            case CALENDAR_APPOINTMENT_REPLY_TIME:
            {
                NSString* aString = [aParser getString];
                NSDate* aDate = [aDateFormatter dateFromString:aString];
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_APPOINTMENT_REPLY_TIME %@", aDate);
                break;
            }
            case CALENDAR_DISALLOW_NEW_TIME_PROPOSAL:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_DISALLOW_NEW_TIME_PROPOSAL %d", [aParser getBool]);
                break;
            case CALENDAR_IS_LEAP_MONTH:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_IS_LEAP_MONTH: %@", [aParser getString]);
                break;
            case CALENDAR_ONLINE_MEETING_CONF_LINK:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_ONLINE_MEETING_CONF_LINK: %@", [aParser getString]);
                break;
            case CALENDAR_ONLINE_MEETING_EXTERNAL_LINK:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_ONLINE_MEETING_EXTERNAL_LINK: %@", [aParser getString]);
                break;
            case BASE_NATIVE_BODY_TYPE:
                self.notesBodyType = (EBodyType)[aParser getInt];
                break;
            default:
                if(tag == AS_END_DOCUMENT) {
                    break;
                }else{
                    [aParser skipTag];
                }
                break;
        }
    }
    //FXDebugLog(kFXLogActiveSync, @"Event:\n%@", self);
}

////////////////////////////////////////////////////////////////////////////////
// Flags Stored
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags

- (NSUInteger)flagsToStore
{
    return flags & kFlagsStoredMask;
}

- (BOOL)isAllDay
{
	return (flags & kEventIsAllDay) != 0;
}

- (void)setIsAllDay:(BOOL)state
{
	if(state)	flags |= kEventIsAllDay;
	else		flags &= ~kEventIsAllDay;
}

- (BOOL)isEditable
{
	return (flags & kEventIsEditable) != 0;
}

- (void)setIsEditable:(BOOL)state
{
	if(state)	flags |= kEventIsEditable;
	else		flags &= ~kEventIsEditable;
}

- (BOOL)isHoliday
{
	return (flags & kEventIsHoliday) != 0;
}

- (void)setIsHoliday:(BOOL)state
{
	if(state)	flags |= kEventIsHoliday;
	else		flags &= ~kEventIsHoliday;
}

////////////////////////////////////////////////////////////////////////////////
// State
////////////////////////////////////////////////////////////////////////////////
#pragma mark State

- (EBusyStatus)busyStatus
{
    EBusyStatus aBusyStatus = (flags & kBusyStatusMask) >> kBusyStatusShift;
	return aBusyStatus;
}

- (void)setBusyStatus:(EBusyStatus)aBusyStatus
{
    flags &= ~kBusyStatusMask;
	flags |= (aBusyStatus << kBusyStatusShift);
}

- (ESensitivity)sensitivity
{
    ESensitivity aSensitivity = (flags & kSensitivityMask) >> kSensitivityShift;
    return aSensitivity;
}

- (void)setSensitivity:(ESensitivity)aSensitivity
{
    flags &= ~kSensitivityMask;
	flags |= (aSensitivity << kSensitivityShift);
}

- (EMeetingStatus)meetingStatus
{
    EMeetingStatus aMeetingStatus = (flags & kMeetingStatusMask) >> kMeetingStatusShift;
    return aMeetingStatus;
}

- (void)setMeetingStatus:(EMeetingStatus)aMeetingStatus
{
    switch(aMeetingStatus) {
        case kMeetingStatusIsAMeeting:
            break;
        case kMeetingStatusMeetingCanceledRecieved:
            break;
        case kMeetingStatusMeetingCanceled:
            break;
        case kMeetingStatusMeetingReceived:
            break;
        case kMeetingStatusNotAMeeting:
            break;
        case kMeetingStatusSameAs1:
            aMeetingStatus = kMeetingStatusIsAMeeting;
            break;
        case kMeetingStatusSameAs3:
            aMeetingStatus = kMeetingStatusMeetingReceived;
            break;
        case kMeetingStatusSameAs5:
            aMeetingStatus = kMeetingStatusMeetingCanceled;
            break;
        case kMeetingStatusSameAs7:
            aMeetingStatus = kMeetingStatusMeetingCanceledRecieved;
            break;
    }
    flags &= ~kMeetingStatusMask;
	flags |= (aMeetingStatus << kMeetingStatusShift);
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
    NSMutableString* string = [NSMutableString stringWithString:[super description]];
    
    [string appendString:@"\n{\n"];
    
    [self writeString:string		tag:@"folder"       value:[self.folder displayName]];
    [self writeString:string		tag:kUid            value:self.uid];
    [self writeString:string		tag:kEventUid       value:self.eventUid];

    [self writeString:string		tag:kTitle          value:self.title];
    if(self.location.length > 0) {
        [self writeString:string    tag:kLocation       value:self.location];
    }
    [self writeString:string        tag:kStartDate      value:[self.startDate descriptionWithLocale:[NSLocale currentLocale]]];
    [self writeString:string        tag:kEndDate        value:[self.endDate   descriptionWithLocale:[NSLocale currentLocale]]];
    [self writeString:string        tag:kCreated        value:[self.created   descriptionWithLocale:[NSLocale currentLocale]]];
    [self writeString:string        tag:kLastModified   value:[self.lastModified   descriptionWithLocale:[NSLocale currentLocale]]];
    
    [self writeString:string        tag:kOrganizerName  value:self.organizerName];
    [self writeString:string        tag:kOrganizerEmail value:self.organizerEmail];

    // Sensitivity
    //
    NSString* aString;
    switch(self.sensitivity) {
        case kSensitivityNormal:
            aString = @"Normal"; break;
        case kSensitivityConfidential:
            aString = @"Confidential"; break;
        case kSensitivityPersonal:
            aString = @"Personal"; break;
        case kSensitivityPrivate:
            aString = @"Private"; break;
            break;
    }
    [self writeString:string        tag:kSensitivity    value:aString];
    
    // Busy Status
    //
    switch(self.busyStatus) {
        case kBusyStatusBusy:
            aString = @"Busy"; break;
        case kBusyStatusFree:
            aString = @"Free"; break;
        case kBusyStatusOutOfOffice:
            aString = @"Out of office"; break;
        case kBusyStatusTentative:
            aString = @"Tentative"; break;
            break;
    }
    [self writeString:string        tag:kBusyStatus     value:aString];
    
    // Meeting Status
    //
    switch(self.meetingStatus) {
        case kMeetingStatusNotAMeeting:
            aString = @"Not a meeting"; break;
        case kMeetingStatusIsAMeeting:
        case kMeetingStatusSameAs1:
            aString = @"Is a meeting"; break;
        case kMeetingStatusMeetingReceived:
        case kMeetingStatusSameAs3:
            aString = @"Meeting received"; break;
        case kMeetingStatusMeetingCanceled:
        case kMeetingStatusSameAs5:
            aString = @"Canceled"; break;
        case kMeetingStatusMeetingCanceledRecieved:
        case kMeetingStatusSameAs7:
            aString = @"Canceled received"; break;
            break;
    }
    [self writeString:string        tag:kMeetingStatus  value:aString];

    // Reminder
    //
    if(self.reminderMinutes >= 0) {
        [self writeInt:string       tag:kReminderMinutes value:self.reminderMinutes];
    }
    [self writeInt:string       tag:kSequence value:self.sequence];

    // Flags
    //
    if([self isAllDay]) {
        [self writeBoolean:string   tag:@"isAllDay"     value:TRUE];
    }
    if([self isEditable]) {
        [self writeBoolean:string   tag:@"isEditable"   value:TRUE];
    }
    
    // Notes
    //
    if(self.notes.length > 0) {
        switch(self.notesBodyType) {
            case kBodyTypeHTML:
                break;
            case kBodyTypeMIME:
                break;
            case kBodyTypeRTF:
                break;
            case kBodyTypePlainText:
                break;
            case kBodyTypeNone:
                break;
        }
        [self writeString:string    tag:kNotes      value:self.notes];
    }
    
    // Recurrence
    //
    if(self.recurrence) {
        [string appendString:[self.recurrence description]];
    }
    
    // Meeting attendees
    //
    if(self.attendees.count > 0) {
        for(Attendee* anAttendee in self.attendees) {
            NSString* aString = [anAttendee description];
            [string appendString:aString];
        }
    }
    
    // Unused future expansion
    //
    if(self.json.length > 0) {
        [self writeString:string    tag:kJson       value:self.json];
    }
    
    return string;
}

@end