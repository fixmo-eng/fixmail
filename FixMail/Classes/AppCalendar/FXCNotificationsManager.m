//
//  FXCNotificationsManager.m
//  FixMail
//
//  Created by Sean Langley on 2013-07-08.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCNotificationsManager.h"
#import "ASFolder.h"
#import "ASCalendarFolder.h"
#import "BaseAccount.h"
#import "Calendar.h"
#import "DateUtil.h"
#import "AppSettings.h"



#define kDaysToNotify 4
#define kNumberOfNotificationsToSet 60

#import "EventEngine.h"

@interface FXCNotificationsManager ()
@property (nonatomic,weak) ASFolder* calendarFolder;
@property (nonatomic,assign) NSInteger numberNotificationsUsed;
@end


@implementation FXCNotificationsManager

+(instancetype) instance
{
    static id _instance =nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[FXCNotificationsManager alloc] init];
    });
    return _instance;
}

-(void) setUpFolderDelegateForFolder:(ASFolder*)folder
{
    self.calendarFolder = folder;
    [self.calendarFolder addDelegate:self];
    [self removeLocalNotifications];
    [self populateLocalNotifications];

}

-(void) tearDownCalendarDelegate
{
    [self.calendarFolder removeDelegate:self];
    self.calendarFolder = nil;
}


- (void)folderSynced:(BaseFolder*)aFolder
{
    [self removeLocalNotifications];
    [self populateLocalNotifications];
}


-(void) removeLocalNotifications
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}


-(void) populateLocalNotifications
{
    if ([AppSettings calendarReminders])
    {
        self.numberNotificationsUsed = 0;
        NSDate* now = [NSDate date];
        NSDate* endDate = [[Calendar calendar] addDays:14 toDate:now];
        [EventEngine.engine eventCacheFromDate:now toDate:endDate folder:self.calendarFolder];

        NSArray* eventArray = [EventEngine.engine eventsArrayFromDate:now toDate:endDate];

        NSDate* date = now;
        for (NSArray* dayArray in eventArray)
        {
            [self createLocalNotificationForDate:date fromArray:dayArray];
            date = [[Calendar calendar] addDays:1 toDate:date];
        }
    }
}

-(void)createLocalNotificationForDate:(NSDate*) date fromArray:(NSArray*)array
{
    NSDateComponents* dateComponents = [[DateUtil.getSingleton currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    for (Event* event in array)
    {
        NSDateComponents* eventComponents = [[DateUtil.getSingleton currentCalendar] components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSTimeZoneCalendarUnit fromDate:event.startDate];
        eventComponents.year = dateComponents.year;
        eventComponents.month = dateComponents.month;
        eventComponents.day = dateComponents.day;
        
        NSDate* eventDate = [[DateUtil.getSingleton currentCalendar] dateFromComponents:eventComponents];
        NSDate* notificationDate = [[Calendar calendar] addToDate:eventDate hours:0 minutes:-event.reminderMinutes];
        
        UILocalNotification* not = [[UILocalNotification alloc] init];
        not.fireDate = notificationDate;
        not.timeZone = eventComponents.timeZone;
        not.alertBody = [NSString stringWithFormat:@"SafeZone Calendar Reminder (%d minutes)",event.reminderMinutes ];
        
        NSDictionary* dict = @{@"uid":event.uid, @"type":@"calendar", @"date":eventDate,@"reminderMinutes":[NSNumber numberWithInteger:event.reminderMinutes]};
        not.userInfo = dict;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:not];
        self.numberNotificationsUsed++;

        if (self.numberNotificationsUsed >= kNumberOfNotificationsToSet)
        {
            break;
        }
    }
}

//NO-OPS

- (void)folderSyncing:(BaseFolder*)aFolder
{
    NSLog(@"");
}

- (void)newObjects:(NSArray*)anObjects
{
    [self removeLocalNotifications];
    [self populateLocalNotifications];
}

- (void)changeObject:(BaseObject*)anObject
{
     NSLog(@"");
    [self removeLocalNotifications];
    [self populateLocalNotifications];
    
}
- (void)deleteUid:(NSString*)aUid
{
     NSLog(@"");
}



@end
