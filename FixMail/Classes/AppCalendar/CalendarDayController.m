//
//  CalendarDayController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-14.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "CalendarDayController.h"
#import "CalendarController.h"
#import "EventEditController.h"
#import "FXMeetingViewController.h"
#import "ASCalendarFolder.h"
#import "Reachability.h"
#import "BaseAccount.h"


@interface CalendarDayController ()

@end

@implementation CalendarDayController

+ (CalendarDayController*)controller
{
    CalendarDayController* sController = NULL;
    
    if(!sController) {
        sController = [[CalendarDayController alloc] initWithNibName:@"CalendarDayView" bundle:[FXLSafeZone getResourceBundle]];
        sController.numDays = 1;
    }
    return sController;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self startReachablility];
    
    if(self.navigationController) {
        [[CalendarController controller] topToolbar:self.navigationItem];
        
        // Bottom tool bar
        //
        if(IS_IPAD()) {
            NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
            self.toolbarItems = aToolBarItems;
        }else{
            UIDeviceOrientation anOrientation = [UIDevice currentDevice].orientation;
            switch(anOrientation) {
                case UIDeviceOrientationLandscapeRight:
                case UIDeviceOrientationLandscapeLeft:
                {
                    self.navigationController.toolbarHidden = YES;
                    break;
                }
                case UIDeviceOrientationPortrait:
                case UIDeviceOrientationPortraitUpsideDown:
                default:
                {
                    self.navigationController.toolbarHidden = NO;
                    NSArray* aToolBarItems = [[CalendarController controller] bottomToolbar:self.navigationController];
                    self.toolbarItems = aToolBarItems;
                    break;
                }
            }
        }
    }

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    if(self.navigationController) {
        [[CalendarController controller] calendarSelector:self.navigationItem];
    }

}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopReachability];
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    [self configureForOrientation:interfaceOrientation];
}

- (void)configureForOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
        if(!IS_IPAD()) {
            CalendarController* aController = [CalendarController controller];
            switch(toInterfaceOrientation) {
                case UIInterfaceOrientationLandscapeLeft:
                case UIInterfaceOrientationLandscapeRight:
                    self.navigationController.toolbarHidden = YES;
                    break;
                case UIInterfaceOrientationPortrait:
                case UIInterfaceOrientationPortraitUpsideDown:
                {
                    if(aController.segmentedControl.selectedSegmentIndex == kCalendarMonth) {
                        self.navigationController.navigationBarHidden = FALSE;
                        [self selectCalendarOfViewType:kCalendarMonth];
                        return;
                    }else{
                        self.navigationController.toolbarHidden = NO;
                    }
                    break;
                }
            }
        }
        TKCalendarDayTimelineView* aTimeLineView = self.calendarDayTimelineView;
        [aTimeLineView configureForOrientation:toInterfaceOrientation
                                         frame:self.view.frame
                          navigationController:self.navigationController];
        [aTimeLineView setupCustomInitialisation];
}

- (void)selectCalendarOfViewType:(ECalendarViewType)aViewType
{
        [[CalendarController controller] setCalendarViewControllerOfViewType:aViewType
                                                                     forDate:self.calendarDayTimelineView.currentDay
                                                                      folder:self.folder
                                                                        size:self.calendarDayTimelineView.bounds.size
                                                        navigationController:self.navigationController];
}

- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl
{
        [[CalendarController controller] setCalendarViewControllerForDate:self.calendarDayTimelineView.currentDay
                                                                   folder:self.folder
                                                                     size:self.calendarDayTimelineView.bounds.size
                                                     navigationController:self.navigationController];
}

////////////////////////////////////////////////////////////////////////////////
// Reachability
////////////////////////////////////////////////////////////////////////////////
#pragma mark Reachability

- (void)updateReachability:(Reachability*)aReachability
{
    switch(aReachability.currentReachabilityStatus) {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
            [[CalendarController controller] setIsReachable:TRUE];
            break;
        case NotReachable:
            [[CalendarController controller] setIsReachable:FALSE];
            break;
    }
}

- (void)startReachablility
{
    Reachability* aReachability = self.folder.account.reachability;
    [[NSNotificationCenter defaultCenter] addObserverForName:kReachabilityChangedNotification object:aReachability queue:nil
                                                  usingBlock:^(NSNotification *inNotification) {
                                                      [self updateReachability:inNotification.object];
                                                  }];
    [self updateReachability:aReachability];
}

- (void)stopReachability
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}


@end
