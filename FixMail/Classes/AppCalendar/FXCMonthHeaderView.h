//
//  FXCMonthHeaderView.h
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-05-02.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FXCMonthHeaderView : UICollectionReusableView
@property (nonatomic,strong) NSString* text;
@end
