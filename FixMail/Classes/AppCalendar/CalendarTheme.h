/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

@interface CalendarTheme : NSObject  {
    UIColor*                    dateColor;
    UIColor*                    dateOffColor;
    UIColor*                    dateInverseColor;
    
    UIColor*                    backColor;
    UIColor*                    backClearColor;
    UIColor*                    shadowColor;

    UIColor*                    holidayColorDark;
    UIColor*                    holidayColorLight;
    
    UIColor*                    meetingColorDark;
    UIColor*                    meetingColorLight;
    
    UIColor*                    eventColorDark;
    UIColor*                    eventColorLight;
    
    UIColor*                    eventBorder;
}

@property (strong,nonatomic) UIColor*       dateColor;
@property (strong,nonatomic) UIColor*       dateOffColor;
@property (strong,nonatomic) UIColor*       dateInverseColor;
@property (strong,nonatomic) UIColor*       backColor;
@property (strong,nonatomic) UIColor*       backClearColor;
@property (strong,nonatomic) UIColor*       shadowColor;
@property (strong,nonatomic) UIColor*       holidayColorDark;
@property (strong,nonatomic) UIColor*       holidayColorLight;
@property (strong,nonatomic) UIColor*       meetingColorDark;
@property (strong,nonatomic) UIColor*       meetingColorLight;
@property (strong,nonatomic) UIColor*       eventColorDark;
@property (strong,nonatomic) UIColor*       eventColorLight;
@property (strong,nonatomic) UIColor*       eventBorder;

@end
