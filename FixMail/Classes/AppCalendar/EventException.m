/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "EventException.h"
#import "Attendee.h"
#import "Event.h"
#import "EventEngine.h"
#import "FastWBXMLParser.h"
#import "NSObject+SBJSON.h"
#import "NSString+SBJSON.h"

@implementation EventException

@synthesize exceptionIsDeleted;
@synthesize exceptionStartTime;
@synthesize title;
@synthesize location;
@synthesize reminder;
@synthesize startDate;
@synthesize endDate;
@synthesize attendees;

// Constants
static NSString* kExceptionIsDeleted        = @"isDeleted";
static NSString* kExceptionStartTime        = @"est";
static NSString* kTitle                     = @"title";
static NSString* kLocation                  = @"location";
static NSString* kReminder                  = @"reminder";
static NSString* kStartDate                 = @"startDate";
static NSString* kEndDate                   = @"endDate";
static NSString* kAttendees                 = @"att";


////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (NSString*)exceptionsAsJSON:(NSArray*)anExceptions
{
    NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:anExceptions.count];
    for(EventException* anException in anExceptions) {
        [anArray addObject:[anException asDictionary]];
    }
    return [anArray JSONRepresentation];
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithExceptionStartTime:(NSDate*)anExceptionStartTime
{
    if (self = [super init]) {
        self.exceptionStartTime     = anExceptionStartTime;
    }
    return self;
}

- (id)initWithJSON:(NSDictionary*)aDictionary
{
    if (self = [super init]) {
        NSString* anDateString = [aDictionary objectForKey:kExceptionStartTime];
        if(anDateString.length > 0) {
            self.exceptionStartTime = [[EventEngine dateFormatter] dateFromString:anDateString];
        }
    }
    return self;
}

- (void)copyToObject:(EventException*)anEventException zone:(NSZone*)zone
{
    anEventException.exceptionIsDeleted     = self.exceptionIsDeleted;
    anEventException.title                  = [self.title copy];
    anEventException.location               = [self.location copy];
    anEventException.reminder               = self.reminder;
    anEventException.startDate              = [self.startDate copy];
    anEventException.endDate                = [self.endDate copy];
    anEventException.attendees              = [self.attendees copy];
}

- (id)copyWithZone:(NSZone*)zone
{
	EventException* anEventException = [[EventException allocWithZone:zone] init];
	[self copyToObject:anEventException zone:zone];
	return anEventException;
}

////////////////////////////////////////////////////////////////////////////////
// JSON
////////////////////////////////////////////////////////////////////////////////
#pragma mark JSON

- (NSDictionary*)asDictionary
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];

    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    if(self.exceptionIsDeleted) {
        [aDictionary setObject:[NSNumber numberWithBool:self.exceptionIsDeleted] forKey:kExceptionIsDeleted];
    }
    if(self.exceptionStartTime) {
        NSString* aString = [aDateFormatter stringFromDate:self.exceptionStartTime];
        [aDictionary setObject:aString forKey:kExceptionStartTime];
    }
    
    if(self.title) {
        [aDictionary setObject:self.title forKey:kTitle];
    }
    if(self.location) {
        [aDictionary setObject:self.location forKey:kLocation];
    }
    if(self.reminder) {
        [aDictionary setObject:[NSNumber numberWithInt:self.reminder] forKey:kReminder];
    }
    
    if(self.startDate) {
        NSString* aString = [aDateFormatter stringFromDate:self.startDate];
        [aDictionary setObject:aString forKey:kStartDate];
    }
    if(self.endDate) {
        NSString* aString = [aDateFormatter stringFromDate:self.endDate];
        [aDictionary setObject:aString forKey:kEndDate];
    }
    
    if(self.attendees.count > 0) {
        [aDictionary setObject:[Attendee attendeesAsJSON:self.attendees] forKey:kAttendees];
    }
    return aDictionary;
}

- (void)fromDictionary:(NSDictionary*)aDictionary
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];

    self.exceptionIsDeleted = [self boolFromDictionary:aDictionary key:kExceptionIsDeleted];
    
    NSString* aString      = [aDictionary objectForKey:kExceptionStartTime];
    if(aString.length > 0) {
        self.exceptionStartTime = [aDateFormatter dateFromString:aString];
    }
    
    self.title      = [aDictionary objectForKey:kTitle];
    self.location   = [aDictionary objectForKey:kLocation];
    
    self.reminder   = [self intFromDictionary:aDictionary key:kReminder];
    
    aString      = [aDictionary objectForKey:kStartDate];
    if(aString.length > 0) {
        self.startDate = [aDateFormatter dateFromString:aString];
    }
    
    aString      = [aDictionary objectForKey:kEndDate];
    if(aString.length > 0) {
        self.endDate = [aDateFormatter dateFromString:aString];
    }
    
    self.attendees      = [aDictionary objectForKey:kAttendees];
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)_baseBodyParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag event:(Event*)anEvent
{
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case BASE_TYPE:
            {
#warning FIXME exception body type
                EBodyType aBodyType = (EBodyType)[aParser getInt];
                if(aBodyType != anEvent.notesBodyType) {
                    FXDebugLog(kFXLogActiveSync, @"BASE_TYPE: %d", aBodyType);
                }
                break;
            }
            case BASE_DATA:
            {
#warning FIXME exception body
                NSString* aBodyData = [aParser getString];
                if(![aBodyData isEqualToString:anEvent.notes]) {
                    FXDebugLog(kFXLogActiveSync, @"BASE_DATA: %@", aBodyData);
                }
                break;
            }
            case BASE_ESTIMATED_DATA_SIZE:
            {
                NSInteger anEstimatedSize = [aParser getInt];
                #pragma unused(anEstimatedSize)
                //FXDebugLog(kFXLogActiveSync, @"Event estimated size: %d", anEstimatedSize);
                break;
            }
            case BASE_TRUNCATED:
            {
                BOOL aTruncated = [aParser getBoolTraceable];
                #pragma unused(aTruncated)
                //FXDebugLog(kFXLogActiveSync, @"Event truncated: %d", aTruncated);
                break;
            }
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)_attendeeParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag event:(Event*)anEvent
{
    NSMutableArray* anAttendees = [NSMutableArray arrayWithCapacity:20];
    Attendee* anAttendee = nil;
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case CALENDAR_ATTENDEE:
                if(anAttendee) {
                    [anAttendees addObject:anAttendee];
                }
                anAttendee = [[Attendee alloc] init];
                break;
            case CALENDAR_ATTENDEE_EMAIL:
                anAttendee.address = [aParser getString];
                break;
            case CALENDAR_ATTENDEE_NAME:
                anAttendee.name = [aParser getString];
                break;
            case CALENDAR_ATTENDEE_TYPE:
                anAttendee.attendeeType = (EAttendeeType)[aParser getInt];
                break;
            case CALENDAR_ATTENDEE_STATUS:
                anAttendee.attendeeStatus = (EAttendeeStatus)[aParser getInt];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
    if(anAttendee) {
#warning FIXME should check if attendee is already on list before adding again
        [anAttendees addObject:anAttendee];        
        NSArray* anAttendeesEvent     = anEvent.attendees;
        if(anAttendeesEvent.count == anAttendees.count) {
            for(NSUInteger i = 0 ; i < anAttendees.count ; ++i) {
                Attendee* anAttendeeEvent   = [anAttendeesEvent objectAtIndex:i];
                Attendee* anAttendeeExcept  = [anAttendees objectAtIndex:i];
                if([anAttendeeEvent.name isEqualToString:anAttendeeExcept.name]) {
                    if(anAttendeeEvent.attendeeStatus != anAttendeeExcept.attendeeStatus) {
                        FXDebugLog(kFXLogActiveSync, @"Exception attendee status change %d to %d", anAttendeeEvent.attendeeStatus, anAttendeeExcept.attendeeStatus);
                    }
                    if(anAttendeeEvent.attendeeType != anAttendeeExcept.attendeeType) {
                        FXDebugLog(kFXLogActiveSync, @"Exception attendee type change %d to %d", anAttendeeEvent.attendeeStatus, anAttendeeExcept.attendeeStatus);
                    }
                    if(![anAttendeeEvent.address isEqualToString:anAttendeeExcept.address]) {
                        FXDebugLog(kFXLogActiveSync, @"Exception attendee address change %@ to %@", anAttendeeEvent.address, anAttendeeExcept.address);
                    }
                }else{
                    FXDebugLog(kFXLogActiveSync, @"Exception attendee change %@ to %@", anAttendeeEvent.name, anAttendeeExcept.name);
                }
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"Exception attendee count change: %d to%d", anAttendeesEvent.count, anAttendees.count);
        }
        self.attendees = anAttendees;
    }
}

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag event:(Event*)anEvent
{
    NSDateFormatter* aDateFormatter = [EventEngine dateFormatter];
    
    ASTag tag;
    while ((tag = [aParser nextTag:anEndTag]) != AS_END) {
        switch(tag) {
            case CALENDAR_EXCEPTION_START_TIME:
            {
                NSString* aString = [aParser getString];
                self.exceptionStartTime = [aDateFormatter dateFromString:aString];
                break;
            }
            case CALENDAR_EXCEPTION_IS_DELETED:
                self.exceptionIsDeleted = [aParser getBool];
                break;
            case CALENDAR_START_TIME:
            {
                NSString* aString = [aParser getString];
                self.startDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case CALENDAR_END_TIME:
            {
                NSString* aString = [aParser getString];
                self.endDate = [aDateFormatter dateFromString:aString];
                break;
            }
            case CALENDAR_DTSTAMP:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_DTSTAMP: %@", [aParser getString]);
                break;
            case CALENDAR_SUBJECT:
            {
                NSString* aTitle = [aParser getString];
                if(![aTitle isEqualToString:anEvent.title]) {
                    self.title = aTitle;
                }
                break;
            }
            case CALENDAR_LOCATION:
            {
                NSString* aLocation = [aParser getString];
                if(![aLocation isEqualToString:anEvent.location]) {
                    self.location = aLocation;
                }
                break;
            }
            case CALENDAR_SENSITIVITY:
            {
                ESensitivity aSensitivity = (ESensitivity)[aParser getInt];
                if(aSensitivity != anEvent.sensitivity) {
                    FXDebugLog(kFXLogActiveSync, @"CALENDAR_SENSITIVITY: %d", aSensitivity);
                }
                break;
            }
            case CALENDAR_BUSY_STATUS:
            {
                EBusyStatus aBusyStatus = (EBusyStatus)[aParser getInt];
                if(aBusyStatus != anEvent.busyStatus) {
                    FXDebugLog(kFXLogActiveSync, @"CALENDAR_BUSY_STATUS: %d", aBusyStatus);
                }
                break;
            }
            case CALENDAR_MEETING_STATUS:
            {
                EMeetingStatus aMeetingStatus = (EMeetingStatus)[aParser getInt];
                if(aMeetingStatus != anEvent.meetingStatus) {
                    FXDebugLog(kFXLogActiveSync, @"CALENDAR_MEETING_STATUS: %d", aMeetingStatus);
                }
                break;
            }
            case CALENDAR_ALL_DAY_EVENT:
            {
                BOOL anIsAllDay = [aParser getBool];
                if(anIsAllDay != anEvent.isAllDay) {
                    FXDebugLog(kFXLogActiveSync, @"CALENDAR_ALL_DAY_EVENT: %d", anIsAllDay);
                }
                break;
            }
            case CALENDAR_ONLINE_MEETING_CONF_LINK:
                FXDebugLog(kFXLogFIXME, @"CALENDAR_ONLINE_MEETING_CONF_LINK: %@", [aParser getString]);
                break;
            case CALENDAR_ONLINE_MEETING_EXTERNAL_LINK:
                FXDebugLog(kFXLogFIXME, @"CALENDAR_ONLINE_MEETING_EXTERNAL_LINK: %@", [aParser getString]);
                break;
            case CALENDAR_CATEGORIES:
                break;
            case CALENDAR_CATEGORY:
                FXDebugLog(kFXLogActiveSync, @"CALENDAR_CATEGORY: %@", [aParser getString]);
                break;
            case BASE_BODY:
                [self _baseBodyParser:aParser tag:tag event:anEvent];
                break;
            case CALENDAR_ATTENDEES:
                [self _attendeeParser:aParser tag:tag event:anEvent];
                break;
            case CALENDAR_REMINDER:
                self.reminder = [aParser getInt];
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
    NSMutableString* aString = [NSMutableString string];

    [aString appendString:@"Exception {\n"];

    if(self.exceptionIsDeleted) {
        [self writeBoolean:aString    tag:@"exceptionIsDeleted" value:self.exceptionIsDeleted];
    }
    if(self.exceptionStartTime) {
        [self writeString:aString    tag:@"exceptionStartTime"  value:[self.exceptionStartTime descriptionWithLocale:[NSLocale currentLocale]]];
    }
    
    if(self.title) {
        [self writeString:aString    tag:@"title"               value:self.title];
    }
    if(self.location) {
        [self writeString:aString    tag:@"location"            value:self.location];
    }
    if(self.reminder != 0) {
        [self writeUnsigned:aString  tag:@"reminder"            value:self.reminder];
    }
    
    if(self.startDate) {
        [self writeString:aString    tag:@"startTime"           value:[self.startDate descriptionWithLocale:[NSLocale currentLocale]]];
    }
    if(self.endDate) {
        [self writeString:aString    tag:@"endTime"             value:[self.endDate descriptionWithLocale:[NSLocale currentLocale]]];
    }
    
    if(self.attendees.count) {
        [self writeInt:aString       tag:@"attendee"            value:self.attendees.count];
    }
    
    [aString appendString:@"}\n"];
    
	return aString;
}

@end
