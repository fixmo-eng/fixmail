/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "Attendee.h"
#import "NSObject+SBJSON.h"

@implementation Attendee

// Properties
@synthesize attendeeStatus;
@synthesize attendeeType;
@synthesize attendeeSort;

// Constants
static NSString* kNameKey           = @"n";
static NSString* kEmailKey          = @"e";
static NSString* kAttendeeStatus    = @"s";
static NSString* kAttendeeType      = @"t";

////////////////////////////////////////////////////////////////////////////////
// Static
////////////////////////////////////////////////////////////////////////////////
#pragma mark Static

+ (NSString*)attendeesAsJSON:(NSArray*)anAttendees
{
    NSMutableArray* anArray = [NSMutableArray arrayWithCapacity:anAttendees.count];
    for(Attendee* anAttendee in anAttendees) {
        [anArray addObject:[anAttendee asDictionary]];
    }
    return [anArray JSONRepresentation];
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
        self.attendeeStatus = kAttendeeResponseNotResponded;
        self.attendeeType   = kAttendeeTypeRequired;
    }
    return self;
}

- (id)initWithName:(NSString*)aName address:(NSString*)anAddress
{
    if (self = [super initWithName:aName address:anAddress]) {
        self.attendeeStatus = kAttendeeResponseNotResponded;
        self.attendeeType   = kAttendeeTypeRequired;
    }
    return self;
}


- (void)copyToObject:(Attendee*)anAttendee zone:(NSZone*)zone
{
    anAttendee.name             = [self.name mutableCopy];
    anAttendee.address          = [self.address mutableCopy];
    anAttendee.attendeeStatus   = self.attendeeStatus;
    anAttendee.attendeeType     = self.attendeeType;
    anAttendee.attendeeSort     = self.attendeeSort;
}

- (id)copyWithZone:(NSZone*)zone
{
	Attendee* anAttendee = [[Attendee allocWithZone:zone] init];
	[self copyToObject:anAttendee zone:zone];
	return anAttendee;
}

- (id)copy
{
	return [self copyWithZone:nil];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (NSDictionary*)asDictionary
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:4];
    if([self.name length] > 0) {
        [aDictionary setObject:self.name forKey:kNameKey];
    }
    
    if([self.address length] > 0) {
        [aDictionary setObject:self.address forKey:kEmailKey];
    }
    
    [aDictionary setObject:[NSNumber numberWithInt:attendeeStatus] forKey:kAttendeeStatus];
    [aDictionary setObject:[NSNumber numberWithInt:attendeeType] forKey:kAttendeeType];

    return aDictionary;
}

- (void)fromDictionary:(NSDictionary*)aDictionary
{
    self.name           = [aDictionary objectForKey:kNameKey];
    self.address        = [aDictionary objectForKey:kEmailKey];
    self.attendeeStatus = (EAttendeeStatus)[self intFromDictionary:aDictionary key:kAttendeeStatus];
    self.attendeeType   = (EAttendeeStatus)[self intFromDictionary:aDictionary key:kAttendeeType];
}

- (NSString*)asToString
{
    NSString* aToString;
    
    if(self.name.length > 0) {
        aToString = [NSString stringWithFormat:@"%@ <%@>", self.name, self.address];
    }else if(self.address.length > 0) {
        aToString = self.address;
    }else{
        FXDebugLog(kFXLogActiveSync, @"Invalid attendee in asToString: %@", self);
        aToString = @"";
    }
    return aToString;
}

- (NSString*)asNameString
{
    NSString* aNameString;
    
    if(self.name.length > 0) {
        aNameString = self.name;
    }else if(self.address.length > 0) {
        NSArray* aStrings = [self.address componentsSeparatedByString:@"@"];
        if(aStrings.count > 0) {
            aNameString = [aStrings objectAtIndex:0];
        }else{
            aNameString = self.address;
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"Invalid attendee in asNameString: %@", self);
        aNameString = @"";
    }
    return aNameString;
}

- (NSString*)asICal:(BOOL)anIsInvite
{
    NSMutableString* aString = nil;
    
    if(self.address.length == 0) {
        FXDebugLog(kFXLogCalendar, @"Attendee asIcal address invalid: %@", self);
        return aString;
    }
    
    //NSString* aRSVP     = @"TRUE";  // FIXME
    //int aNumGuests      = 0;        // FIXME
    
    // Role and Type
    //
    NSString* aType     = @"INDIVIDUAL"; // Default
    
    //NSString* aType   = @"GROUP";
    NSString* aRole;
    switch(self.attendeeType) {
        case kAttendeeTypeUnknown:
        case kAttendeeTypeOptional:
            aRole = @"OPT-PARTICIPANT";
            break;
        case kAttendeeTypeRequired:
            aRole = @"REQ-PARTICIPANT";
            break;
        case kAttendeeTypeResource:
            aRole = @"NON-PARTICIPANT";
            aType = @"RESOURCE";
            //aType = @"ROOM";
            break;
    }
    
    // partstat-event
    // partstat-todo & partstat-jour not implemented
    //
    NSString* aPartStat;
    switch(self.attendeeStatus) {
        case kAttendeeResponseAccept:
            aPartStat = @"ACCEPTED";
            break;
        case kAttendeeResponseTentative:
            aPartStat = @"TENTATIVE";
            break;
        case kAttendeeResponseNotResponded:
        case kAttendeeResponseUnknown:
            aPartStat = @"NEEDS-ACTION";
            break;
        case kAttendeeResponseDecline:
            aPartStat = @"DECLINED";
            break;
    }
    
    NSString* aNameString = [self asNameString];
    if(anIsInvite) {
        aString = [NSMutableString stringWithFormat:@"ATTENDEE;CN=\"%@\";CUTYPE=%@;RSVP=TRUE;PARTSTAT=%@:mailto:%@\n",
                   aNameString, aType, aPartStat, self.address];
    }else{
        aString = [NSMutableString stringWithFormat:@"ATTENDEE;CUTYPE=%@;ROLE=%@;PARTSTAT=%@;CN=\"%@\":mailto:%@\n",
                   aType, aRole, aPartStat, aNameString, self.address];
    }

    return aString;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];

    [string appendString:@"{\n"];
    NSString* aString;
    switch(self.attendeeStatus) {
        case kAttendeeResponseAccept:
            aString = @"accept"; break;
        case kAttendeeResponseDecline:
            aString = @"decline"; break;
        case kAttendeeResponseUnknown:
            aString = @"unkown"; break;
        case kAttendeeResponseTentative:
            aString = @"tentative"; break;
        case kAttendeeResponseNotResponded:
            aString = @"notResponded"; break;
        default:
            aString = @"unknown"; break;
    }
    [self writeString:string		tag:@"attendeeStatus"            value:aString];
    
    if(self.attendeeType != kAttendeeTypeUnknown) {
        switch(attendeeType) {
            case kAttendeeTypeRequired:
                aString = @"required"; break;
            case kAttendeeTypeOptional:
                aString = @"optional"; break;
            case kAttendeeTypeResource:
                aString = @"resource"; break;
            default:
                aString = @"unknown"; break;
        }
        [self writeString:string		tag:@"attendeeType"             value:aString];
    }

	[string appendString:@"}\n"];

	return string;
}

@end
