/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "Calendar.h"
#import "DateInfo.h"
#import "Event.h"
#import "EventException.h"
#import "EventRecurrence.h"

@implementation Calendar

// Properties
@synthesize calendar;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

static Calendar* sCalendar = nil;

+ (Calendar*)calendar
{
    if(!sCalendar) {
        sCalendar = [[Calendar alloc] init];
    }
    return sCalendar;
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
        calendar = [NSCalendar autoupdatingCurrentCalendar];
    }
    
    return self;
}


////////////////////////////////////////////////////////////////////////////////
// Date Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Date Utilities

- (void)dateInfo:(DateInfo*)aDateInfo forDate:(NSDate*)aDate
{
    NSDateComponents* aComponents = [calendar components:
                                                   NSDayCalendarUnit
                                                 | NSWeekOfMonthCalendarUnit
                                                 | NSWeekdayCalendarUnit
                                                 | NSWeekdayOrdinalCalendarUnit
                                                 | NSMonthCalendarUnit
                                                fromDate:aDate];
    aDateInfo.dayOfMonth     = aComponents.day;
    switch(aComponents.weekday) {
        case 1:
            aDateInfo.dayOfWeek = kDayOfWeekSunday;
            break;
        case 2:
            aDateInfo.dayOfWeek = kDayOfWeekMonday;
            break;
        case 3:
            aDateInfo.dayOfWeek = kDayOfWeekTuesday;
            break;
        case 4:
            aDateInfo.dayOfWeek = kDayOfWeekWednesday;
            break;
        case 5:
            aDateInfo.dayOfWeek = kDayOfWeekThursday;
            break;
        case 6:
            aDateInfo.dayOfWeek = kDayOfWeekFriday;
            break;
        case 7:
            aDateInfo.dayOfWeek = kDayOfWeekSaturday;
            break;
        default:
            FXDebugLog(kFXLogActiveSync, @"dayOfWeek invalid");
            break;
    }
    aDateInfo.weekDayOrdinal = aComponents.weekdayOrdinal;
    aDateInfo.weekOfMonth    = aComponents.weekOfMonth;
    aDateInfo.monthOfYear    = aComponents.month;
}

- (EDayOfWeek)dayOfWeek:(NSDate*)aDate
{
    EDayOfWeek aDayOfWeek;
    
	NSDateComponents* aComponents = [calendar components:NSWeekdayCalendarUnit fromDate:aDate];
    switch( aComponents.weekday) {
        case 1:
            aDayOfWeek = kDayOfWeekSunday;
            break;
        case 2:
            aDayOfWeek = kDayOfWeekMonday;
            break;
        case 3:
            aDayOfWeek = kDayOfWeekTuesday;
            break;
        case 4:
            aDayOfWeek = kDayOfWeekWednesday;
            break;
        case 5:
            aDayOfWeek = kDayOfWeekThursday;
            break;
        case 6:
            aDayOfWeek = kDayOfWeekFriday;
            break;
        case 7:
            aDayOfWeek = kDayOfWeekSaturday;
            break;
        default:
            FXDebugLog(kFXLogActiveSync, @"dayOfWeek invalid");
            aDayOfWeek = kDayOfWeekSunday;
            break;
    }
	return aDayOfWeek;
}

- (NSString*)dayOfWeekAsICAL:(NSDate*)aDate
{
    NSString* aString;
    NSDateComponents* aComponents = [calendar components:NSWeekdayCalendarUnit fromDate:aDate];
    switch( aComponents.weekday) {
        case 1:
            aString = @"SU";
            break;
        case 2:
            aString = @"MO";
            break;
        case 3:
            aString = @"TU";
            break;
        case 4:
            aString = @"WE";
            break;
        case 5:
            aString = @"TH";
            break;
        case 6:
            aString = @"FR";
            break;
        case 7:
            aString = @"SA";
            break;
        default:
            FXDebugLog(kFXLogActiveSync, @"dayOfWeek invalid");
            break;
    }
	return aString;
}


- (int)weekday:(NSDate*)aDate
{
	NSDateComponents* aComponents = [calendar components:NSWeekdayCalendarUnit fromDate:aDate];
	return aComponents.weekday;
}

- (int)dayOfMonth:(NSDate*)aDate
{
	NSDateComponents* aComponents = [calendar components:NSDayCalendarUnit fromDate:aDate];
	return aComponents.day;
}

- (int)weekOfMonth:(NSDate*)aDate
{
	NSDateComponents* aComponents = [calendar components:NSDayCalendarUnit fromDate:aDate];
    return aComponents.day / 7 + 1;
}

- (int)monthOfYear:(NSDate*)aDate
{
	NSDateComponents* aComponents = [calendar components:NSMonthCalendarUnit fromDate:aDate];
	return aComponents.month;
}

- (int)year:(NSDate*)aDate
{
	NSDateComponents* aComponents = [calendar components:NSYearCalendarUnit fromDate:aDate];
	return aComponents.year;
}

- (int)weeksInMonth:(NSDate *)aDate
{
    NSRange aRange = [calendar rangeOfUnit:NSWeekCalendarUnit
                                    inUnit:NSMonthCalendarUnit
                                   forDate:aDate];
    return aRange.length;
}

- (int)lastDayOfMonthForDate:(NSDate*)aDate
{
    NSRange aRange = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:aDate];
    return aRange.length;
}

- (NSInteger)daysBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate
{
    NSTimeInterval time = [aSecondDate timeIntervalSinceDate:aFirstDate];
    return ((abs(time) / (60.0f * 60.0f * 24.0f)) + 0.5f);
}

- (NSInteger)weeksBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate
{
    NSTimeInterval time = [aSecondDate timeIntervalSinceDate:aFirstDate];
    return ((abs(time) / (60.0f * 60.0f * 24.0f * 7.0f)) + 0.5f);
}

- (NSInteger)monthsBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate
{
    // Dates need to be timeless or you will get off by one errors
    NSDateComponents* aComponents = [calendar components:NSMonthCalendarUnit
                                                fromDate:aFirstDate
                                                  toDate:aSecondDate
                                                 options:0];
    return [aComponents month];
}

- (NSInteger)yearsBetweenDates:(NSDate*)aFirstDate date:(NSDate*)aSecondDate
{
    // Dates need to be timeless or you will get off by one errors
    NSDateComponents* aComponents = [calendar components:NSYearCalendarUnit
                                                fromDate:aFirstDate
                                                  toDate:aSecondDate
                                                 options:0];
    return [aComponents year];
}

- (NSDate*)firstOfMonthForDate:(NSDate*)aDate
{
	TKDateInformation info = [self dateInformationFromDate:aDate];
	info.day    = 1;
	info.minute = 0;
	info.second = 0;
	info.hour   = 0;
	return [self dateFromDateInformation:info];
}

- (NSDate*)lastOfMonthForDate:(NSDate*)aDate
{
    NSDateComponents* aComponents = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit fromDate:aDate];     
    NSRange aRange = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:aDate];
    [aComponents setDay:aRange.length];
    NSDate* aLastDate = [calendar dateFromComponents:aComponents];
    return aLastDate;
}

- (NSDate*)lastOfYearForDate:(NSDate*)aDate
{
    NSDateComponents* aComponents = [calendar components:NSYearCalendarUnit fromDate:aDate];
    [aComponents setMonth:12];
    [aComponents setDay:1];
    NSDate* aLastMonthDate = [calendar dateFromComponents:aComponents];
    int aLastDayOfMonth = [self lastDayOfMonthForDate:aLastMonthDate];
    [aComponents setDay:aLastDayOfMonth];
    NSDate* aLastDate = [calendar dateFromComponents:aComponents];
    return aLastDate;
}

- (NSDate*)nextMonthForDate:(NSDate*)aDate
{
	TKDateInformation info = [self dateInformationFromDate:aDate];
	info.month++;
	if(info.month>12){
		info.month = 1;
		info.year++;
	}
	info.minute = 0;
	info.second = 0;
	info.hour   = 0;
	
	return [self dateFromDateInformation:info];
}

- (NSDate*)previousMonthForDate:(NSDate*)aDate
{
	TKDateInformation info = [self dateInformationFromDate:aDate];
	info.month--;
	if(info.month<1){
		info.month = 12;
		info.year--;
	}
	
	info.minute = 0;
	info.second = 0;
	info.hour   = 0;
	return [self dateFromDateInformation:info];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (BOOL)doesEvent:(Event*)anEvent matchDate:(NSDate*)aDate
{
    if(anEvent.recurrence) {
        FXDebugLog(kFXLogFIXME, @"FIXME don't call this on recurring events");
    }
    NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:aDate];
    NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:anEvent.startDate];
    BOOL isMatch = ([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
    return isMatch;
}

- (BOOL)isDate:(NSDate*)aDate1 sameDayAsDate:(NSDate*)aDate2
{
    NSDateComponents* components1 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:aDate1];
    NSDateComponents* components2 = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:aDate2];
    BOOL isSame = ([components1 year] == [components2 year] && [components1 month] == [components2 month] && [components1 day] == [components2 day]);
    return isSame;
}

- (BOOL)doesEvent:(Event*)anEvent matchDateInfo:(DateInfo*)aDateInfo
{
    BOOL isMatch = FALSE;

    NSDate* aDate = aDateInfo.date;
    EventRecurrence* aRecurrence = anEvent.recurrence;
    if(!aRecurrence || aRecurrence.recurrenceType == kRecurrenceNone) {
        // Not a recurring event, just check for date match
        //
        if([self isDate:aDate sameDayAsDate:anEvent.startDate]
        || [self isDate:aDate sameDayAsDate:anEvent.endDate]) {
            isMatch = TRUE;
        }
    }else{
        // Range check calendar date against recurrering event dates
        //
        if([aDate compare:anEvent.recurrence.startTimelessDate] == NSOrderedAscending) {
            // Calendar date is before recurring event's timeless start date
            return FALSE;
        }else if(anEvent.recurrence.endDate && [aDate compare:anEvent.recurrence.endDate] == NSOrderedDescending) {
            // Calendar date is after recurrening event's end date due to occurrence or until limit
            return FALSE;
        }
        
        // Does the recurrence have exceptions
        //
        if(aRecurrence.exceptions.count > 0) {
            for(EventException* anException in aRecurrence.exceptions) {
#warning FIXME not sure this error  check is correct
                if(anException.startDate) {
                    if([self isDate:aDate sameDayAsDate:anException.startDate]) {
                        return TRUE;
                    }
                }else{
                    FXDebugLog(kFXLogCalendar, @"doesEvent matchDateInfo no startDate: %@", anException);
                }
            }
            for(EventException* anException in aRecurrence.exceptions) {
                if(anException.exceptionStartTime) {
                    if([self isDate:aDate sameDayAsDate:anException.exceptionStartTime]) {
                        return FALSE;
                    }
                }else{
                    FXDebugLog(kFXLogCalendar, @"doesEvent matchDateInfo no exception start time: %@", anException);
                }
            }
        }

        // Check event against a specific occurence type
        //
        switch(aRecurrence.recurrenceType) {
            case kRecurrenceDaily:
                isMatch = true;
                break;
            case kRecurrenceWeekly:
                if(aRecurrence.dayOfWeek != 0) {
                    if(aRecurrence.dayOfWeek & aDateInfo.dayOfWeek) {
                        if(aRecurrence.interval <= 1) {
                            isMatch = TRUE;
                        }else{
                            int aWeeks = [self weeksBetweenDates:anEvent.startDate date:aDate];
                            if(aWeeks % aRecurrence.interval == 0) {
                                isMatch = TRUE;
                            }
                        }
                    }
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME kRecurrenceWeekly without day of week: %@", anEvent.title);
                }
                break;
            case kRecurrenceMonthly:
                if(aRecurrence.dayOfMonth != 0) {
                    if(aRecurrence.dayOfMonth == aDateInfo.dayOfMonth) {
                        if(aRecurrence.interval <= 1) {
                            isMatch = TRUE;
                        }else{
                            int aMonths = [self monthsBetweenDates:[self timelessDate:anEvent.startDate] date:aDate];
                            if(aMonths % aRecurrence.interval == 0) {
                                isMatch = TRUE;
                            }
                        }
                    }
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME kRecurrenceMonthly without day of month: %@", anEvent.title);
                }
                break;
            case kRecurrenceMonthlyOnNthDay:
                if(aRecurrence.dayOfWeek != 0 && aRecurrence.weekOfMonth != 0) {
                    int aWeekOfMonth;
                    if(aRecurrence.weekOfMonth < 0) {
                        aWeekOfMonth = [self weeksInMonth:aDate] + aRecurrence.weekOfMonth + 1;
                    }else{
                        aWeekOfMonth = aRecurrence.weekOfMonth;
                    }
                    if(aRecurrence.dayOfWeek == aDateInfo.dayOfWeek && aWeekOfMonth == aDateInfo.weekDayOrdinal) {
                        if(aRecurrence.interval <= 1) {
                            isMatch = TRUE;
                        }else{
                            int aMonths = [self monthsBetweenDates:[self timelessDate:anEvent.startDate] date:aDate];
                            if(aMonths % aRecurrence.interval == 0) {
                                isMatch = TRUE;
                            }
                        }
                    }
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME kRecurrenceMonthlyOnNthDay without day of week or week of month: %@", anEvent.title);
                }
                break;
            case kRecurrenceYearly:
                if(aRecurrence.dayOfMonth != 0 && aRecurrence.monthOfYear != 0) {
                    if(aRecurrence.dayOfMonth == aDateInfo.dayOfMonth && aRecurrence.monthOfYear == aDateInfo.monthOfYear) {
                        if(aRecurrence.interval <= 1) {
                            isMatch = TRUE;
                        }else{
                            int aYears = [self yearsBetweenDates:[self timelessDate:anEvent.startDate] date:aDate];
                            if(aYears % aRecurrence.interval == 0) {
                                isMatch = TRUE;
                            }
                        }
                    }
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME kRecurrenceYearly without day of month or month of year: %@", anEvent.title);
                }
                break;
            case kRecurrenceYearlyOnNthDay:
                if(aRecurrence.dayOfWeek != 0
                && (aRecurrence.weekOfMonth != 0 || aRecurrence.dayOfMonth != 0 || aRecurrence.daysOfMonth.count > 0)
                && aRecurrence.monthOfYear != 0) {
                    if(aRecurrence.dayOfWeek   == aDateInfo.dayOfWeek
                    && aRecurrence.monthOfYear == aDateInfo.monthOfYear) {
                        if(aRecurrence.weekOfMonth != 0) {
                            int aWeekOfMonth;
                            if(aRecurrence.weekOfMonth < 0) {
                                aWeekOfMonth = [self weeksInMonth:aDate] + aRecurrence.weekOfMonth + 1;
                            }else{
                                aWeekOfMonth = aRecurrence.weekOfMonth;
                            }
                            if(aWeekOfMonth != aDateInfo.weekDayOrdinal) {
                                break;
                            }
                        }else if(aRecurrence.dayOfMonth) {
                            if(aRecurrence.dayOfMonth != aDateInfo.dayOfMonth) {
                                break;
                            }
                        }else if(aRecurrence.daysOfMonth.count > 0) {
                            BOOL aMatch = FALSE;
                            for(NSNumber* aNumber in aRecurrence.daysOfMonth) {
                                if(aNumber.intValue == aDateInfo.dayOfMonth) {
                                    aMatch = true;
                                    break;
                                }
                            }
                            if(!aMatch) break;
                        }

                        if(aRecurrence.interval <= 1) {
                            isMatch = TRUE;
                        }else{
                            int aYears = [self yearsBetweenDates:[self timelessDate:anEvent.startDate] date:aDate];
                            if(aYears % aRecurrence.interval == 0) {
                                isMatch = TRUE;
                            }
                        }
                    }
                }else{
                    FXDebugLog(kFXLogFIXME, @"FIXME kRecurrenceYearlyOnNthDay without dayOfWeek, weekOfMonth or monthOfYear : %@", anEvent.title);
                }
                break;
            case kRecurrenceNone:
                break;
        }
    }
    
    return isMatch;
}

- (NSDate*)addToDate:(NSDate*)aDate hours:(int)anHours minutes:(int)aMinutes
{
    NSDate* aNewDate;
    
    NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.hour       = anHours;
	aComponents.minute     = aMinutes;
    
    aNewDate = [calendar dateByAddingComponents:aComponents toDate:aDate options:0];
    
    return aNewDate;
}

- (NSDate*)addDays:(int)aDays toDate:(NSDate*)aDate;
{
    NSDate* aNewDate;
    
    NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.day = aDays;
    aNewDate = [calendar dateByAddingComponents:aComponents toDate:aDate options:0];
    
    return aNewDate;
}

- (NSDate*)addWeeks:(int)aWeeks toDate:(NSDate*)aDate
{
    NSDate* aNewDate;
    
    NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.week = aWeeks;
    aNewDate = [calendar dateByAddingComponents:aComponents toDate:aDate options:0];
    
    return aNewDate;
}

- (NSDate*)addMonths:(int)aMonths toDate:(NSDate*)aDate
{
    NSDate* aNewDate;
    
    NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.month = aMonths;
    aNewDate = [calendar dateByAddingComponents:aComponents toDate:aDate options:0];
    
    return aNewDate;
}

- (NSDate*)addYears:(int)aYears toDate:(NSDate*)aDate
{
    NSDate* aNewDate;
    
    NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.year = aYears;
    aNewDate = [calendar dateByAddingComponents:aComponents toDate:aDate options:0];
    
    return aNewDate;
}

- (NSDate*)dateByAddingComponents:(NSDateComponents*)aComponents toDate:(NSDate*)aDate
{
    return[calendar dateByAddingComponents:aComponents toDate:aDate options:0];
}

- (NSDate*)newYearsDayForDate:(NSDate*)aDate
{
	NSDateComponents *comp = [calendar components:(NSYearCalendarUnit) fromDate:aDate];
	return [calendar dateFromComponents:comp];
}

- (NSDate*)timelessDate:(NSDate*)aDate
{
	NSDateComponents *comp = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:aDate];
	return [calendar dateFromComponents:comp];
}

- (NSDateComponents*)datelessTimeComponents:(NSDate*)aDate
{
	return [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:aDate];
}

- (TKDateInformation)dateInformationFromDate:(NSDate*)aDate
{
	TKDateInformation info;
    
	NSDateComponents* aComponents = [calendar components:(  NSYearCalendarUnit
                                                          | NSMonthCalendarUnit
                                                          | NSDayCalendarUnit
                                                          | NSHourCalendarUnit
                                                          | NSMinuteCalendarUnit
                                                          | NSSecondCalendarUnit
                                                          | NSWeekdayCalendarUnit
                                                          | NSTimeZoneCalendarUnit)
                                                fromDate:aDate];
	info.day        = aComponents.day;
	info.month      = aComponents.month;
	info.year       = aComponents.year;
	
	info.hour       = aComponents.hour;
	info.minute     = aComponents.minute;
	info.second     = aComponents.second;
	
	info.weekday    = aComponents.weekday;
	
	return info;
}

- (NSDate*)dateFromDateInformation:(TKDateInformation)info
{
	NSDateComponents *comp = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSTimeZoneCalendarUnit) fromDate:[NSDate date]];
	
	comp.day        = info.day;
	comp.month      = info.month;
	comp.year       = info.year;
	comp.hour       = info.hour;
	comp.minute     = info.minute;
	comp.second     = info.second;
	comp.timeZone   = [calendar timeZone];
	
	return [calendar dateFromComponents:comp];
}

- (NSDate*)dateFromSystemTime:(SystemTime*)aSystemTime
{
    NSDate* aDate;
    
	NSDateComponents* aComponents = [[NSDateComponents alloc] init];
	aComponents.day        = aSystemTime->wDay;
	aComponents.month      = aSystemTime->wMonth;
	aComponents.year       = aSystemTime->wYear;
	aComponents.hour       = aSystemTime->wHour;
	aComponents.minute     = aSystemTime->wMinute;
	aComponents.second     = aSystemTime->wSecond;
	
	aDate = [calendar dateFromComponents:aComponents];
    return aDate;
}

@end
