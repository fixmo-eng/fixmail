#import <UIKit/UIKit.h>

@class FXCCollectionView;
@protocol FXCCollectionViewDelegate <UICollectionViewDelegate>

- (void) collectionViewWillLayoutSubviews:(FXCCollectionView *)collectionView;

@end

@interface FXCCollectionView : UICollectionView

@end
