/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "AvailabilityEditController.h"
#import "Event.h"

@implementation AvailabilityEditController


// Types
//
typedef enum
{
    kAvailabilityBusy           = 0,
    kAvailabilityFree           = 1,
    kAvailabilityTentative      = 2,
    kAvailabilityOutOfOffice    = 3
} EAvailabilityActions;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
        
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.accessoryType          = UITableViewCellAccessoryCheckmark;

    switch((EAvailabilityActions)indexPath.row) {
        case kAvailabilityBusy:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"BUSY", @"Calendar", @"Displayed availability - busy");
            if(self.event.busyStatus == kBusyStatusBusy) {
                cell.accessoryType          = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType          = UITableViewCellAccessoryNone;
            }
            break;
        case kAvailabilityFree:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"FREE", @"Calendar", @"Displayed availability - free");
            if(self.event.busyStatus == kBusyStatusFree) {
                cell.accessoryType          = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType          = UITableViewCellAccessoryNone;
            }
            break;
        case kAvailabilityTentative:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"TENTATIVE", @"Calendar", @"Displayed availability - tentative");
            if(self.event.busyStatus == kBusyStatusTentative) {
                cell.accessoryType          = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType          = UITableViewCellAccessoryNone;
            }
            break;
        case kAvailabilityOutOfOffice:
            cell.textLabel.text         = FXLLocalizedStringFromTable(@"OUT_OF_OFFICE", @"Calendar", @"Displayed availability - out of office");
            if(self.event.busyStatus == kBusyStatusOutOfOffice) {
                cell.accessoryType          = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType          = UITableViewCellAccessoryNone;
            }
            break;
    }

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EBusyStatus originalStatus = self.event.busyStatus;
    
    switch((EAvailabilityActions)indexPath.row) {
        case kAvailabilityBusy:
            self.event.busyStatus = kBusyStatusBusy;
            break;
        case kAvailabilityFree:
            self.event.busyStatus = kBusyStatusFree;
            break;
        case kAvailabilityTentative:
            self.event.busyStatus = kBusyStatusTentative;
            break;
        case kAvailabilityOutOfOffice:
            self.event.busyStatus = kBusyStatusOutOfOffice;
            break;
    }
    

    if (originalStatus != self.event.busyStatus &&
        [self.changeDelegate respondsToSelector:@selector(availabilityEditController:didModifyEvent:)])
    {
        [self.changeDelegate availabilityEditController:self didModifyEvent:self.event];
    }
    
    [self.tableView reloadData];
}

@end
