/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "EventViewController.h"
#import "CalendarMonthController.h"
#import "Event.h"
#import "EventEditController.h"

@implementation EventViewController

// Properties
@synthesize event;
@synthesize monthController;
@synthesize startDate;

// Constants
static const CGFloat kMarginY       = 12.0f;
static const CGFloat kPopoverWidth  = 320.f;

+ (EventViewController*)controller
{
    EventViewController* anEventViewController = nil;
    
    NSArray* nibContents = [[FXLSafeZone getResourceBundle] loadNibNamed:@"EventView" owner:self options:NULL];
    NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
    NSObject* nibItem = NULL;
    while ( (nibItem = [nibEnumerator nextObject]) != NULL) {
        if ( [nibItem isKindOfClass: [EventViewController class]]) {
            anEventViewController = (EventViewController*) nibItem;
            break;
        }
    }
    
    return anEventViewController;
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (IBAction)edit:(id)sender
{
    EventEditController* anEventEditController = [EventEditController controllerForEvent:self.event
                                                                         eventEditorType:kEventEditorChangeEvent
                                                                         dateForInstance:self.startDate];
    [anEventEditController editEvent];
    if(IS_IPAD()) {
        UINavigationController* aNavController = [[UINavigationController alloc]initWithRootViewController:anEventEditController];
        anEventEditController.popover = self.monthController.popover;
        [self.monthController.popover setContentViewController:aNavController animated:TRUE];
        [self.monthController.popover setPopoverContentSize:CGSizeMake(320, 640) animated:TRUE];
    }else{
        [self.monthController pushViewController:anEventEditController];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (UILabel*)createLabel:(CGRect)aRect
{
    UILabel* aLabel = [[UILabel alloc] initWithFrame:aRect];
    
    [self.view addSubview:aLabel];
    
    return aLabel;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)loadEvent:(Event*)anEvent startDate:(NSDate*)aDate
{
    self.event = anEvent;
    self.startDate = aDate;

    titleLabel.text = anEvent.title;
    CGRect aLabelRect = titleLabel.frame;
    
    CGRect aRect = self.view.frame;
    
    aLabelRect.origin.y += aLabelRect.size.height + kMarginY;
    aLabelRect.size.width = kPopoverWidth - aLabelRect.origin.x;
    UILabel* aLabel = [self createLabel:aLabelRect];
    aLabel.text = [[Event dateFormatter] stringFromDate:self.startDate];
    
    if(anEvent.location.length > 0) {
        aLabelRect.origin.y += aLabelRect.size.height + kMarginY;
        UILabel* aLabel = [self createLabel:aLabelRect];
        aLabel.text = anEvent.location;
    }
    
    if(anEvent.notes.length > 0) {
        aLabelRect.origin.y += aLabelRect.size.height + kMarginY;
        UILabel* aLabel = [self createLabel:aLabelRect];
        aLabel.text = anEvent.notes;
    }
    
    aRect.size.width    = kPopoverWidth;
    aRect.size.height   = aLabelRect.origin.y + aLabelRect.size.height + kMarginY;
    self.view.superview.frame = aRect;

    aRect.origin.x = 0.0f;
    aRect.origin.y = 0.0f;
    self.view.frame = aRect;
}

@end
