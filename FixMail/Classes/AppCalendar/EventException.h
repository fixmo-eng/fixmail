/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "AS.h"

@class Event;
@class FastWBXMLParser;

@interface EventException : BaseObject {
}

@property (nonatomic) BOOL              exceptionIsDeleted;
@property (nonatomic,strong) NSDate*    exceptionStartTime;
@property (nonatomic,strong) NSString*  title;
@property (nonatomic,strong) NSString*  location;
@property (nonatomic) NSUInteger        reminder;
@property (nonatomic,strong) NSDate*    startDate;
@property (nonatomic,strong) NSDate*    endDate;
@property (nonatomic,strong) NSMutableArray*    attendees;

// Static
+ (NSString*)exceptionsAsJSON:(NSArray*)anExceptions;

// Construct/Destruct
- (id)initWithExceptionStartTime:(NSDate*)anExceptionStartTime;
- (id)initWithJSON:(NSDictionary*)aDictionary;

// JSON
- (NSDictionary*)asDictionary;
- (void)fromDictionary:(NSDictionary*)aDictionary;

// ActiveSync Parser
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag event:(Event*)anEvent;

// Debug
- (NSString*)description;

@end
