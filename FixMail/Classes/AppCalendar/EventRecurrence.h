/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"
#import "AS.h"
#import "Event.h"

@class EventException;

@interface EventRecurrence : BaseObject {
    ERecurrenceType recurrenceType;
    NSUInteger      interval;
    NSUInteger      firstDayOfWeek;
    EDayOfWeek      dayOfWeek;
    NSInteger       weekOfMonth;
    NSUInteger      monthOfYear; 
    NSUInteger      dayOfMonth;
    NSArray*        daysOfMonth;

    // occurences and untilDate are mutually exclusive
    NSUInteger      occurences;
    NSDate*         untilDate;
    
    // Exceptions
    NSMutableArray* exceptions;
    
    // Transient properties
    NSDate*         startTimelessDate;
    NSDate*         endDate;
}

// Properties
@property (nonatomic) ERecurrenceType   recurrenceType;  
@property (nonatomic) NSUInteger        interval;
@property (nonatomic) NSUInteger        firstDayOfWeek;
@property (nonatomic) EDayOfWeek        dayOfWeek;  
@property (nonatomic) NSInteger         weekOfMonth;  
@property (nonatomic) NSUInteger        monthOfYear;  
@property (nonatomic) NSUInteger        dayOfMonth;
@property (nonatomic,strong) NSArray*   daysOfMonth;
@property (nonatomic) NSUInteger        occurences;
@property (nonatomic,strong) NSDate*    untilDate;
@property (strong) NSMutableArray*      exceptions;

@property (nonatomic,strong) NSDate*    startTimelessDate;
@property (nonatomic,strong) NSDate*    endDate;


// Construct
- (id)initWithRecurrenceType:(ERecurrenceType)aRecurrenceType
                    interval:(NSUInteger)anInterval;
- (id)initWithJSON:(NSString*)aRecurrenceJSON;

// Interface
- (NSString*)asString;
- (NSString*)typeAsString;
- (NSString*)intervalAsString;
- (void)dayOfWeekFromString:(NSString*)aString;
- (void)changedEvent:(Event*)anEvent;
- (void)addException:(EventException*)anException;

// JSON
- (NSString*)asJSON;

// iCal
- (NSString*)asICal;

// ActiveSync Parser
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag;

// Debug
- (NSString*)description;

@end
