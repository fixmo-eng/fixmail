/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarDelegate.h"
#import "FolderDelegate.h"
#import "TKCalendarDayTimelineViewDelegate.h"
#import "TKCalendarYearViewController.h"
#import "FXCalendarDayViewPopoverContentController.h"

@class ASCalendarFolder;
@class CalendarDayController;

@interface CalendarYearController : TKCalendarYearViewController
    <CalendarDelegate,
    FolderDelegate,
    TKCalendarDayTimelineViewDelegate,
    UIPopoverControllerDelegate>
{
    ASCalendarFolder*       folder;
    
    UIPopoverController*        dayViewPopover;
    
    BOOL                    isPushed;
}

@property (nonatomic,readonly) ASCalendarFolder* folder;
@property (nonatomic,strong) FXCalendarDayViewPopoverContentController*     dayController;
@property (nonatomic,strong) UIPopoverController*       dayViewPopover;


// Factory
+ (CalendarYearController*)controller;

// Interface
- (void)setFolder:(ASCalendarFolder *)folder;

@end