/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

@interface NumberPickerController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    IBOutlet UIPickerView*  picker;
}

@property (nonatomic, strong) NSObject*             target;
@property (nonatomic, assign) SEL                   action;
@property (nonatomic, assign) int                   min;
@property (nonatomic, assign) int                   max;
@property (nonatomic, strong) UIPopoverController*  popoverController;

// Factory
+ (NumberPickerController*)numberPickerForViewController:(UIViewController*)aViewController
                                                    rect:(CGRect)aCellRect
                                                   value:(int)value
                                                     min:(int)aMin
                                                     max:(int)aMax
                                                  action:(SEL)anAction;

- (void)setValue:(int)aValue;

@end
