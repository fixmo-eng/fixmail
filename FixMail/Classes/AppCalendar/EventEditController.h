/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASComposeDelegate.h"
#import "ASSyncEventDelegate.h"
#import "DatePickerDelegate.h"
#import "RecipientDelegate.h"

@class AvailabilitySheet;
@class BaseFolder;
@class DatePickerController;
@class Event;
@class RecipientViewController;
@class RecipientSheet;

typedef enum
{
    kEventEditorChangeEvent     = 0,
    kEventEditorAddEvent        = 1,
} EEventEditorType;

@interface EventEditController : UITableViewController <UIActionSheetDelegate, UITextViewDelegate,
    RecipientDelegate, ASComposeDelegate, ASSyncEventDelegate, DatePickerDelegate> {

    // Flags
    BOOL                        isPush;
    BOOL                        isIPad;
    BOOL                        isEditing;
    
    // Meeting UI
    CGRect                      recipientPopoverRect;
    
    // Date Picker
    UIView*                     datePickerView;
    
    // Editing UI in the table view
    UITextField*                currentTextField;
    UITextView*                 currentTextView;
    UISwitch*                   allDaySwitch;
}

// Properties
@property (nonatomic, strong) NSDate*                   actualStartTime;
@property (nonatomic, strong) Event*                    originalEvent;
@property (nonatomic, strong) Event*                    event;
@property (nonatomic) EEventEditorType                  eventEditorType;

@property (nonatomic, strong) UISegmentedControl*       meetingSegmentedControl;// Meeting UI
@property (nonatomic, strong) UIBarButtonItem*          editButtonItem;
@property (nonatomic, strong) UIBarButtonItem*          deleteButtonItem;

@property (nonatomic, strong) RecipientViewController*  recipientPicker;        
@property (nonatomic, strong) UIView*                   recipientView;
@property (nonatomic, strong) UIPopoverController*      recipientPopover;
@property (nonatomic, strong) NSMutableArray*           acceptanceButtons;
@property (nonatomic, strong) UIViewController*         datePickerController;   // Date Picker
@property (nonatomic, strong) NSMutableArray*           barButtonItems;         // Nav bar UI
@property (nonatomic, strong) AvailabilitySheet*        availabilitySheet;      // Sheets
@property (nonatomic, strong) RecipientSheet*           recipientSheet;
@property (nonatomic, weak) UIPopoverController*        popover;



// Factory
+ (EventEditController*)controllerForEvent:(Event*)anEvent
                           eventEditorType:(EEventEditorType)anEventEditorType
                           dateForInstance:(NSDate*)aDate;
+ (EventEditController*)createEventForFolder:(BaseFolder*)aFolder date:(NSDate*)aDate;

- (void)loadEvent:(Event*)anEvent;
- (void)editEvent;

@end
