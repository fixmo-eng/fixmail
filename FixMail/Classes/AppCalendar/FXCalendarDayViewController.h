/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "CalendarDelegate.h"
#import "FolderDelegate.h"
#import "TKCalendarDayViewController.h"
#import "FXCalendarPopoverDelegate.h"

@class ASCalendarFolder;

@interface FXCalendarDayViewController : TKCalendarDayViewController <TKCalendarDayTimelineViewDelegate, CalendarDelegate, FolderDelegate,UIPopoverControllerDelegate> {
    ASCalendarFolder*       folder;
    
}

@property (nonatomic,readonly) ASCalendarFolder*      folder;
@property (nonatomic, strong) UIPopoverController* popover;
@property (nonatomic,strong) id<FXCalendarPopoverDelegateProtocol> calendarPopoverDelegate;

-(NSDate*)currentDate;

- (void)setFolder:(ASCalendarFolder *)folder;

//CalendarDelegate
- (void)prev;
- (void)next;
- (void)today;
- (void)addEvent;
- (void)editEvent:(Event*)anEvent;
- (void)setDate:(NSDate*)aDate;
- (void)selectCalendarType:(UISegmentedControl*)aSegmentedControl;


@end
