/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "Event.h"

@interface DatePickerController : UIViewController {
    IBOutlet UIDatePicker*  __weak datePicker;
    UIPopoverController*    popover;
    NSObject*               target;
    SEL                     action;
}

@property (weak, nonatomic, readonly) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPopoverController*  popover;
@property (nonatomic, strong) NSObject*             target;
@property (nonatomic, assign) SEL                   action;

// Factory
+ (DatePickerController*)datePickerForViewController:(UIViewController*)aViewController rect:(CGRect)aCellRect date:(NSDate*)aDate action:(SEL)anAction;


// Actions
- (IBAction)changeDate:(UIDatePicker*)aDatePicker;

// Interface
- (void)setDate:(NSDate *)date;

@end
