//
//  FXCalendarPopoverDelegate.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-13.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FXCalendarPopoverDelegateProtocol <NSObject>

//- (UIPopoverArrowDirection)popoverDirection:(CGRect)aRect inView:(UIView*)inView;
- (UIPopoverController*)createPopover:(CGRect)aRect viewController:(UIViewController*)aViewController popoverDelegate:(id<UIPopoverControllerDelegate>) popoverDelegate presentingView:(UIView*)view;
@end

@interface FXCalendarPopoverDelegate : NSObject <FXCalendarPopoverDelegateProtocol>
- (UIPopoverArrowDirection)popoverDirection:(CGRect)aRect inView:(UIView*)inView;
- (UIPopoverController*)createPopover:(CGRect)aRect viewController:(UIViewController*)aViewController popoverDelegate:(id<UIPopoverControllerDelegate>) popoverDelegate presentingView:(UIView*)view;


@end
