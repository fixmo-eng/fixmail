/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "AS.h"
#import "ASEmail.h"

@class Attendee;
@class BaseFolder;
@class FastWBXMLParser;
@class EventRecurrence;

typedef enum
{
    kRecurrenceNone             = -1,
    kRecurrenceDaily            = 0,
    kRecurrenceWeekly           = 1,
    kRecurrenceMonthly          = 2,
    kRecurrenceMonthlyOnNthDay  = 3,
    kRecurrenceYearly           = 5,
    kRecurrenceYearlyOnNthDay   = 6
} ERecurrenceType;

typedef enum
{
    kDayOfWeekSunday            = 0x01,
    kDayOfWeekMonday            = 0x02,
    kDayOfWeekTuesday           = 0x04,
    kDayOfWeekWednesday         = 0x08,
    kDayOfWeekThursday          = 0x10,
    kDayOfWeekFriday            = 0x20,
    kDayOfWeekSaturday          = 0x40,
    kDayOfWeekLastDayOfMonth    = 0x7f
} EDayOfWeek;

typedef enum
{
    kFirstDayOfWeekSunday       = 0,
    kFirstDayOfWeekMonday       = 1,
    kFirstDayOfWeekTuesday      = 2,
    kFirstDayOfWeekWednesday    = 3,
    kFirstDayOfWeekThursday     = 4,
    kFirstDayOfWeekFriday       = 5,
    kFirstDayOfWeekSaturday     = 6,
} EFirstDayOfWeek;

typedef enum
{
    kSensitivityNormal          = 0,
    kSensitivityPersonal        = 1,
    kSensitivityPrivate         = 2,
    kSensitivityConfidential    = 3,
} ESensitivity;

typedef enum
{
    kBusyStatusFree             = 0,
    kBusyStatusTentative        = 1,
    kBusyStatusBusy             = 2,
    kBusyStatusOutOfOffice      = 3,
} EBusyStatus;

typedef enum
{
    kMeetingStatusNotAMeeting               = 0,
    kMeetingStatusIsAMeeting                = 1,
    kMeetingStatusMeetingReceived           = 3,
    kMeetingStatusMeetingCanceled           = 5,
    kMeetingStatusMeetingCanceledRecieved   = 7,
    kMeetingStatusSameAs1                   = 9,
    kMeetingStatusSameAs3                   = 11,
    kMeetingStatusSameAs5                   = 13,
    kMeetingStatusSameAs7                   = 15,
} EMeetingStatus;

// Types
typedef enum
{
    kResponseTentative  = 0,
    kResponseDecline    = 1,
    kResponseAccept     = 2
} EResponseActions;

typedef enum
{
    kResponseTypeNone           = 0,
    kResponseTypeOrganizer      = 1,
    kResponseTypeTentative      = 2,
    kResponseTypeAccepted       = 3,
    kResponseTypeDeclined       = 4,
    kResponseTypeNotRespoded    = 5
} EResponseType;

typedef enum
{
    kICalMethodRequest,
    kICalMethodPublish,
    kICalMethodReply,
    kICalMethodCounter,
    kICalMethodCancel
} EICalMethod;

typedef enum
{
    kICalStatusConfirmed,
    kICalStatusCancelled,
    kICalStatusTentative,

} EICalStatus;


typedef struct
{
    unsigned short wYear;
    unsigned short wMonth;
    unsigned short wDayOfWeek;
    unsigned short wDay;
    unsigned short wHour;
    unsigned short wMinute;
    unsigned short wSecond;
    unsigned short wMilleseconds;
} SystemTime;

@interface Event : BaseObject

// Properties
@property (nonatomic,strong) BaseFolder*        folder;
@property (nonatomic,strong) NSString*          uid;
@property (nonatomic,strong) NSString*          eventUid;

@property (nonatomic, strong) NSString*         title;
@property (nonatomic, strong) NSString*         location;

@property (nonatomic, strong) NSDate*           startDate;
@property (nonatomic, strong) NSDate*           endDate;
@property (nonatomic, strong) NSDate*           created;
@property (nonatomic, strong) NSDate*           lastModified;

@property (nonatomic) int                       reminderMinutes;
@property (nonatomic) int                       sequence;

@property (nonatomic, strong) NSString*         organizerName;
@property (nonatomic, strong) NSString*         organizerEmail;

@property (nonatomic) NSUInteger                flags;

@property (nonatomic, strong) EventRecurrence*  recurrence;
@property (nonatomic, strong) NSString*         json;

@property (nonatomic) EBodyType                 notesBodyType;
@property (nonatomic, strong) NSString*         notes;

@property (strong) NSMutableArray*              attendees;

// Factory
+ (NSDateFormatter*)iCalFormatter;
+ (NSDateFormatter*)iCalFormatterZulu;
+ (NSDateFormatter*)iCalFormatterDate;
+ (NSDateFormatter*)dateFormatter;
+ (NSDateFormatter*)dateFormatterTimeless;

+ (NSArray*)attendeeResponseStrings;
+ (NSString*)createUUID;

// Construct
- (id)initWithFolder:(BaseFolder*)aFolder
               title:(NSString*)aTitle
            location:(NSString*)aLocation
           startDate:(NSDate*)aStartDate
             endDate:(NSDate*)anEndDate;

- (id)initWithFolder:(BaseFolder*)aFolder;
- (void)copyToObject:(Event*)anEvent zone:(NSZone*)zone;

// Interface
- (void)changed;
- (NSDate*)endDateForStartDate:(NSDate*)aDate;
- (NSDate*)startDateForEndDate:(NSDate*)aDate;
- (NSString*)asString;
- (NSString*)dateAndTimeString;


// Attendee
- (BOOL)isMeeting;
- (BOOL)isMyMeeting;
- (void)updateAttendeeStatus:(Attendee*)anAttendee;
- (void)addAttendee:(Attendee*)anAttendee;
- (void)removeAttendee:(Attendee*)anAttendee;

// Date
- (NSDate*)startTimeForDate:(NSDate*)timelessDate;
- (NSDate*)endTimeForDate:(NSDate*)timelessDate;

// iCal/vCal
- (NSString*)asICalendar:(EICalMethod)aMethod status:(EICalStatus)aStatus;

// ActiveSync Parser
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;

// Flags
- (BOOL)isAllDay;
- (void)setIsAllDay:(BOOL)state;

- (BOOL)isEditable;
- (void)setIsEditable:(BOOL)state;

- (BOOL)isHoliday;
- (void)setIsHoliday:(BOOL)state;

// State
- (EBusyStatus)busyStatus;
- (void)setBusyStatus:(EBusyStatus)aBusyStatus;

- (ESensitivity)sensitivity;
- (void)setSensitivity:(ESensitivity)aSensitivity;

- (EMeetingStatus)meetingStatus;
- (void)setMeetingStatus:(EMeetingStatus)aMeetingStatus;

// Debug
- (NSString*)description;

@end