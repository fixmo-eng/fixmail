/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "CalendarMonthTableController.h"
#import "ErrorAlert.h"
#import "EventContainer.h"
#import "EventEditController.h"
#import "EventEngine.h"
#import "MeetingViewController.h"
#import "DateUtil.h"
#import "TKCalendarMonthView.h"
#import "FXMeetingViewController.h"

@implementation CalendarMonthTableController

// Properties
@synthesize eventTable;
@synthesize tableView;

////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (CalendarMonthTableController*)controller
{
    CalendarMonthTableController* sController = NULL;

    if(!sController) {
        sController = [[CalendarMonthTableController alloc] initWithNibName:@"CalendarMonthTableView" bundle:[FXLSafeZone getResourceBundle]];
        sController.viewType = kCalendarMonth;
    }
    
    return sController;
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
	[super viewDidLoad];
    
	CGFloat y = self.monthView.frame.origin.y + self.monthView.frame.size.height;
	CGFloat height = [TKCalendarMonthViewController size].height - y;
    
	UITableView* aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, y, self.view.bounds.size.width, height) style:UITableViewStylePlain];
    aTableView.backgroundColor = [UIColor whiteColor];
	aTableView.delegate = self;
	aTableView.dataSource = self;
	aTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
	[self.view addSubview:aTableView];
	[self.view sendSubviewToBack:aTableView];
    
    self.tableView = aTableView;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
	self.tableView.delegate     = nil;
	self.tableView.dataSource   = nil;
	self.tableView              = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    
    if(self.folder && !isPushed) {
        [self eventTableBuild:self.monthView.dateSelected];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.monthView adjustFrame ];
    [self _updateTableOffset:NO];
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"CALENDAR_MONTH_TABLE_EXCEPTION", @"ErrorAlert", @"Calendar Month Table Controller error alert view exception message") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"CALENDAR_MONTH_TABLE", @"ErrorAlert", @"Calendar Month Controller error alert view title") message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView*)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Event Table
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event Table

static NSInteger sortByStartDate(id obj1, id obj2, void *context)
{
    BOOL reverse = *((BOOL *)context);
    NSDate* aDate1 = [(EventContainer*)obj1 actualStartTime];
    NSDate* aDate2 = [(EventContainer*)obj2 actualStartTime];
    if ((NSInteger *)reverse == NO) {
        return [aDate1 compare:aDate2];
    }
    return [aDate2 compare:aDate1];
}

- (void)eventTableBuild:(NSDate*)aDate
{
    if(aDate) {
        @try {
            NSArray* anEvents = [EventEngine.engine eventCacheEventsForDate:aDate];
            if(anEvents && anEvents.count > 0) {
                NSDate* aTimelessDate = [[Calendar calendar] timelessDate:aDate];
                NSMutableArray* anEventContainers = [NSMutableArray arrayWithCapacity:anEvents.count];
                for(Event* anEvent in anEvents) {
                    EventContainer* anEventContainer = [[EventContainer alloc] initWithEvent:anEvent timelessDate:aTimelessDate];
                    [anEventContainers addObject:anEventContainer];
                }
                
                BOOL aReverseSort = FALSE;
                NSArray* anArray = [anEventContainers sortedArrayUsingFunction:sortByStartDate context:&aReverseSort];
                self.eventTable = [anArray mutableCopy];
            }else{
                self.eventTable = nil;
            }
        }@catch(NSException* e) {
            [self logException:@"eventTableBuild" exception:e];
        }
    }else{
        self.eventTable = nil;
    }
    
   	[self.tableView reloadData];
}

- (void)eventTableDeleteUid:(NSString*)aUID
{
    @try {
        NSUInteger aCount = self.eventTable.count;
        for(NSUInteger i = 0 ; i < aCount ; ++i) {
            EventContainer* anEventContainer = [self.eventTable objectAtIndex:i];
            if([anEventContainer.event.uid isEqualToString:aUID]) {
                NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self.eventTable removeObjectAtIndex:i];
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
                [self.tableView endUpdates];
                break;
            }
        }
    }@catch(NSException* e) {
        [self logException:@"deleteFromEventTable" exception:e];
    }
}

- (void)_updateTableOffset:(BOOL)animated
{
	if(animated){
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:0.4];
		[UIView setAnimationDelay:0.1];
	}
	
	float y = CGRectGetMaxY(self.monthView.frame);
	self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, y, self.tableView.frame.size.width, self.view.frame.size.height - y );
	
	if(animated) [UIView commitAnimations];
}

////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event FolderDelegate

- (void)rebuildWithReloadEvents:(BOOL)aReloadEvents
{
    [super rebuildWithReloadEvents:aReloadEvents];

    [self eventTableBuild:self.monthView.dateSelected];
}

////////////////////////////////////////////////////////////////////////////////
// UITableView delegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark Event UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.eventTable.count;
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    UITableViewCell* cell = [tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    if(indexPath.row < self.eventTable.count) {
        EventContainer* anEventContainer = [self.eventTable objectAtIndex:indexPath.row];
        NSString* aTitle = anEventContainer.event.title;
		
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [[DateUtil getSingleton] stringFromDateTimeOnlyShort:anEventContainer.actualStartTime], aTitle];
    }else{
        FXDebugLog(kFXLogActiveSync, @"CalenderMonthTable cellForRowAtIndexPath out of bounds");
    }
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventContainer* anEventContainer = [self.eventTable objectAtIndex:indexPath.row];
    Event* anEvent = anEventContainer.event;
    FXMeetingViewController* controller = [FXMeetingViewController controllerWithEvent:anEvent];
    [self.navigationController pushViewController:controller animated:YES];
        
}

////////////////////////////////////////////////////////////////////////////////
// TKCalendarMonthViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TKCalendarMonthViewDelegate

- (BOOL)calendarMonthView:(TKCalendarMonthView*)aMonthView dateSelected:(NSDate*)aDate touch:(UITouch*)aTouch
{
    [super calendarMonthView:aMonthView dateSelected:(NSDate*)aDate touch:aTouch];
    
    [self eventTableBuild:aDate];
    
    return TRUE;
}

- (void)calendarMonthView:(TKCalendarMonthView*)aMonthView monthDidChange:(NSDate*)aDate animated:(BOOL)animated
{
    [super calendarMonthView:aMonthView monthDidChange:aDate animated:animated];
    
    [self _updateTableOffset:animated];
    [self eventTableBuild:aDate];
}

@end
