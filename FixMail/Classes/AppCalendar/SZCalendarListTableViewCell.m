//
//  SZCalendarListTableViewCell.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-25.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "SZCalendarListTableViewCell.h"
#import "DateUtil.h"
 
@interface SZCalendarListTableViewCell()
@property UILabel* timeLabel;
@property UILabel* titleLabel;
@property UILabel* locationLabel;
@end

@implementation SZCalendarListTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.textAlignment = NSTextAlignmentRight;
        _timeLabel.font = [UIFont systemFontOfSize:16];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize:16];
        
        _locationLabel = [[UILabel alloc] init];
        _locationLabel.textColor = [UIColor grayColor];
        _locationLabel.font = [UIFont systemFontOfSize:14];
        
        [self.contentView addSubview:_timeLabel];
        [self.contentView addSubview:_titleLabel];
        [self.contentView addSubview:_locationLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


//A lot of magic numbers in here.
//Basically the time label is on the left with padding of 5 all around the label, with height of cell, and 75px wide
//Title and location labels are the same size, stacked on top of each other with 5 px between, and 5 all around other sides
//except when there is no location, in that case, the location Label is hidden, and the title label fills the screen

-(void)layoutSubviews
{
    [super layoutSubviews];
    self.locationLabel.hidden = NO;
    self.timeLabel.frame = CGRectMake(5,5,75,self.bounds.size.height-10);
    
    if (_event.location && ![_event.location isEqualToString:@""])
    {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame)+10, 5, CGRectGetWidth(self.frame) - CGRectGetMaxX(self.timeLabel.frame) - 5  - 5 -5 , CGRectGetHeight(self.frame)/2 - 5);
        self.locationLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame)+10, CGRectGetHeight(self.timeLabel.frame)/2.0 + 5, CGRectGetWidth(self.frame) - CGRectGetMaxX(self.timeLabel.frame) - 5  - 5 -5, CGRectGetHeight(self.frame)/2.0 - 5);
    }
    else
    {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame)+10, 5, CGRectGetWidth(self.frame) - CGRectGetMaxX(self.timeLabel.frame) - 5  - 5 -5, CGRectGetHeight(self.frame) - 10);
        self.locationLabel.hidden = YES;
    }
}

-(void) setEvent:(Event*) inEvent
{
    _event = inEvent;
    self.timeLabel.text = [[DateUtil getSingleton] stringFromDateTimeOnlyShort:_event.startDate];
    self.locationLabel.text = _event.location;
    self.titleLabel.text = _event.title;
    
    [self layoutSubviews];
}


@end
