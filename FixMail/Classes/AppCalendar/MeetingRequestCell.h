/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

@class ASEmail;

@interface MeetingRequestCell : UITableViewCell {
    ASEmail*            email;
    
    IBOutlet UILabel*   titleLabel;
    IBOutlet UILabel*   fromLabel;
    IBOutlet UILabel*   whenLabel;
    IBOutlet UILabel*   canceledLabel;
    IBOutlet UIButton*  removeButton;

    UISegmentedControl* segmentedControl;
}

@property (nonatomic,strong) ASEmail*               email;
@property (nonatomic,readonly) UILabel*             canceledLabel;
@property (nonatomic,strong) UISegmentedControl*    segmentedControl;


// Construct
- (id)initWithStyle:(UITableViewCellStyle)aStyle reuseIdentifier:(NSString *)aReuseIdentifier;

// Interface
- (void)loadEmail:(ASEmail*)anEmail;

- (IBAction)removeCanceled:(id)sender;

@end