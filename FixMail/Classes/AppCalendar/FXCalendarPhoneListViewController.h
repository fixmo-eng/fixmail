//
//  FXCalendarPhoneListViewController.h
//  FixMail
//
//  Created by Sean Langley on 2013-06-26.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXCalendarListViewController.h"

@interface FXCalendarPhoneListViewController : FXCalendarListViewController <CalendarDelegate>
+(FXCalendarPhoneListViewController*) controller;

@end
