//
//  FXCMonthHeaderView.m
//  CalendarMonthView
//
//  Created by Sean Langley on 2013-05-02.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#import "FXCMonthHeaderView.h"
@interface FXCMonthHeaderView ()
@property (nonatomic,strong) UILabel* label;

@end

@implementation FXCMonthHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        
    }
    return self;
}

-(UILabel*) label
{
    if ( !_label)
    {
        CGRect labelRect = CGRectInset(self.bounds,40,0);
        _label = [[UILabel alloc] initWithFrame:labelRect];
        _label.backgroundColor = [UIColor whiteColor];
        _label.font = [UIFont boldSystemFontOfSize:36];
        self.backgroundColor = [UIColor whiteColor];
        
    }
    return _label;
}

-(void) layoutSubviews
{
    [super layoutSubviews];
//    self.label.frame = self.bounds;
    if (!self.label.superview)
    {
        [self addSubview:self.label];
    }
}

-(void) setText:(NSString*)text
{
    self.label.text = text;
}

-(NSString*)text
{
    return self.label.text;
}



@end
