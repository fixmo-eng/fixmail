#import "FXCCollectionView.h"

@implementation FXCCollectionView
@dynamic delegate;

- (void) layoutSubviews {
	
    id del = self.delegate;
    if ([del conformsToProtocol:@protocol(FXCCollectionViewDelegate)])
    {
        [del collectionViewWillLayoutSubviews:self];
    }
	[super layoutSubviews];		
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    return [super initWithCoder:aDecoder];
}

-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    return [super initWithFrame:frame collectionViewLayout:layout];
}

@end
