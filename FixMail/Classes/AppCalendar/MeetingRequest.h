/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"
#import "AS.h"
#import "Event.h"

@class BaseAccount;
@class EventRecurrence;
@class FastWBXMLParser;

// Types
typedef enum {
    kMeetingMessageTypeSilentUpdate         = 0,
    kMeetingMessageTypeInitialRequest       = 1,
    kMeetingMessageTypeFullUpdate           = 2,
    kMeetingMessageTypeInformationalUpdate  = 3,
    kMeetingMessageTypeOutdated             = 4,
    kMeetingMessageTypeDelegatorCopy        = 5,
    kMeetingMessageTypeDelegated            = 6
} EMeetingMessageType;

typedef enum {
    kSingleAppointment          = 0,
    kMasterRecurringAppointment = 1,
    kSingleInstanceOfRecurring  = 2,
    kExceptionToRecurring       = 3
} EInstanceType;

@interface MeetingRequest : BaseObject {
    NSDate*             startDate;
    NSDate*             endDate;
    NSDate*             timeStamp;

    NSString*           location;
    NSString*           organizer;
    NSString*           globalObjectId;

    EMeetingMessageType meetingMessageType;
    EInstanceType       instanceType;
    ESensitivity        sensitivity;
    EBusyStatus         busyStatus;
    
    int                 reminderMinutes;
    
    EventRecurrence*    recurrence;
    
    NSUInteger          flags;
}

// Properties
@property (nonatomic, strong) NSDate*           startDate;
@property (nonatomic, strong) NSDate*           endDate;
@property (nonatomic, strong) NSDate*           timeStamp;

@property (nonatomic, strong) NSString*         location;
@property (nonatomic, strong) NSString*         organizer;
@property (nonatomic, strong) NSString*         globalObjectId;

@property (nonatomic) EMeetingMessageType       meetingMessageType;
@property (nonatomic) EInstanceType             instanceType;
@property (nonatomic) ESensitivity              sensitivity;
@property (nonatomic) EBusyStatus               busyStatus;

@property (nonatomic) int                       reminderMinutes;

@property (nonatomic, strong) EventRecurrence*  recurrence;

// Factory
+ (NSDateFormatter*)dateFormatter;

// Construct
- (id)init;
- (id)initWithJSON:(NSString*)aJSON;

// JSON
- (NSString*)asJSON;

// iCal
- (NSString*)asICalForAccount:(BaseAccount*)anAccount subject:(NSString*)aSubject;

// ActiveSync Parser
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag messageClass:(NSString*)aMessageClass;

// Interface
- (NSString*)eventUid;

// Flags
- (BOOL)isAllDay;
- (void)setIsAllDay:(BOOL)state;

- (BOOL)responseRequested;
- (void)setResponseRequested:(BOOL)state;

- (BOOL)disallowNewTime;
- (void)setDisallowNewTime:(BOOL)state;

- (BOOL)isRequest;
- (void)setIsRequest:(BOOL)state;

- (BOOL)isCanceled;
- (void)setIsCanceled:(BOOL)state;

- (BOOL)isAccepted;
- (void)setAccepted;

- (BOOL)isTentativeAccepted;
- (void)setTentativeAccepted;

- (BOOL)isDeclined;
- (void)setDeclined;

@end
