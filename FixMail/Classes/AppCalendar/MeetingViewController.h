/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "PickerDelegate.h"

@class ASEmail;
@class CalendarController;
@class Event;

@interface MeetingViewController : UIViewController <UIPopoverControllerDelegate>

// Properties
@property (nonatomic, strong) NSObject <PickerDelegate>*    pickerDelegate;

@property (nonatomic, strong) ASEmail*                      email;
@property (nonatomic, strong) Event*                        event;

@property (nonatomic, strong) NSDate*                       startDate;

@property (nonatomic) BOOL                                  inMailView;

@property (nonatomic, weak) IBOutlet UILabel*               subjectLabel;

@property (nonatomic, weak) IBOutlet UIView*                fromView;
@property (nonatomic, weak) IBOutlet UILabel*               organizerLabel;

@property (nonatomic, weak) IBOutlet UIView*                whereView;
@property (nonatomic, weak) IBOutlet UILabel*               locationLabel;

@property (nonatomic, weak) IBOutlet UIView*                whenView;
@property (nonatomic, weak) IBOutlet UILabel*               whenLabel;
@property (nonatomic, weak) IBOutlet UILabel*               recurrenceLabel;

@property (nonatomic, weak) IBOutlet UISegmentedControl*    segmentedControl;

@property (nonatomic, weak) IBOutlet UIWebView*             webView;

@property (nonatomic, weak) IBOutlet UILabel*               attendeeLabel;
@property (nonatomic, weak) IBOutlet UILabel*               attendee;


// Factory
+ (MeetingViewController*)controllerWithEmail:(ASEmail*)anEmail;
+ (MeetingViewController*)controllerWithEvent:(Event*)anEvent;
+ (MeetingViewController*)controller;

// Interface
- (void)loadEmail:(ASEmail*)anEmail;

- (IBAction)response:(UISegmentedControl*)aSegmentedControl;

@end
