//
//  FXOutlinedView.m
//  FixMail
//
//  Created by Sean Langley on 2013-07-03.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXOutlinedView.h"

@implementation FXOutlinedView


- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (self.outlineColor)
    {
        CGRect outlineRect = self.bounds;
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        UIGraphicsPushContext(ctx);
        CGContextSetStrokeColorWithColor(ctx, [self.outlineColor CGColor]);
        CGContextStrokeRectWithWidth(ctx, outlineRect, 2);

        UIGraphicsPopContext();
    }
}

@end
