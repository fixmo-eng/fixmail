//
//  FXMeetingViewController.m
//  FixMail
//
//  Created by Sean Langley on 2013-06-10.
//  Copyright (c) 2013 Fixmo. All rights reserved.
//

#import "FXMeetingViewController.h"  

#import "FXMeetingViewTitleCell.h"
#import "FXMeetingViewOrganizerTableViewCell.h"
#import "FXMeetingAttendeeViewerViewController.h"
#import "AvailabilityEditController.h"
#import "ReminderEditController.h"
#import "EventRecurrence.h"
#import "EmailAddress.h"
#import "Attendee.h"
#import "Event.h"
#import "ASFolder.h"
#import "EventEditController.h"

typedef enum {
    FXMeetingViewRowTitle=0,
     FXMeetingViewRowLocation,
    FXMeetingViewRowTiming,
    FXMeetingViewRowOrganizer ,
    FXMeetingViewRowInvitees ,
    FXMeetingViewRowAvailability,
    FXMeetingViewRowNotes,
//    FXMeetingViewRowShowAllNotes,
    FXMeetingViewRowAlert,

    
    FXMeetingViewRowLast,
    
    
} FXMeetingViewRow;




@interface FXMeetingViewController ()
{
    FXMeetingViewTitleCell* sizingCell;
}
@property (nonatomic,retain) UISegmentedControl* meetingSegmentedControl;
@end

@implementation FXMeetingViewController

+ (FXMeetingViewController*)controllerWithEvent:(Event*)anEvent
{
    FXMeetingViewController* aVC = [[FXMeetingViewController alloc] initWithStyle:UITableViewStyleGrouped];
    aVC.hidesBottomBarWhenPushed = NO;
    [aVC setEvent:anEvent];
    return aVC;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self.tableView  registerClass:[FXMeetingViewTitleCell class] forCellReuseIdentifier:@"FXMeetingViewRowTitle"];
        [self.tableView  registerClass:[FXMeetingViewTitleCell class] forCellReuseIdentifier:@"FXMeetingViewRowLocation"];
        [self.tableView  registerClass:[FXMeetingViewTitleCell class] forCellReuseIdentifier:@"FXMeetingViewRowNotes"];
        [self.tableView  registerClass:[FXMeetingViewTitleCell class] forCellReuseIdentifier:@"FXMeetingViewRowTiming"];
        [self.tableView  registerClass:[UITableViewCell class] forCellReuseIdentifier:@"FXMeetingViewController"];
        [self.tableView  registerClass:[FXMeetingViewOrganizerTableViewCell class] forCellReuseIdentifier:@"FXMeetingViewRowOrganizer"];
        [self.tableView  registerClass:[FXMeetingViewOrganizerTableViewCell class] forCellReuseIdentifier:@"FXMeetingViewRowAvailability"];
        [self.tableView  registerClass:[FXMeetingViewOrganizerTableViewCell class] forCellReuseIdentifier:@"FXMeetingViewRowAlert"];

        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Event Details"; //BARE_STRING
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.meetingSegmentedControl = [[UISegmentedControl alloc] initWithItems:[Event attendeeResponseStrings]];
    self.meetingSegmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;

    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem* bbi = [[UIBarButtonItem alloc] initWithCustomView:self.meetingSegmentedControl];
    [self setToolbarItems:@[flexibleSpace,bbi,flexibleSpace] animated:NO ];
    [self.meetingSegmentedControl addTarget:self action:@selector(response:) forControlEvents:UIControlEventValueChanged];
        
}

-(void) editEvent:(id)sender
{

    EventEditController* controller = [EventEditController controllerForEvent:self.event eventEditorType:kEventEditorChangeEvent dateForInstance:self.event.startDate];
    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [controller editEvent];
    if ( IS_IPAD())
    {
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    }
    
    [self.navigationController presentViewController:navController animated:YES completion:nil];

}

-(void)viewWillAppear:(BOOL)animated
{    
    [self setResponse:[self attendeeForSelf]];
    if ([self.event isEditable])
    {
        UIBarButtonItem* rightBar = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editEvent:)];
        self.navigationItem.rightBarButtonItem = rightBar;
        self.toolbarItems = nil;
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return FXMeetingViewRowLast;
}

-(UITableViewCell*) getCellForTableView:(UITableView*)tableView atIndexPath:(NSIndexPath*)indexPath
{
    NSString* reUseIdentifier  = @"FXMeetingViewController";
    switch (indexPath.row)
    {
        case FXMeetingViewRowTitle:
            reUseIdentifier = @"FXMeetingViewRowTitle";
            break;
        case FXMeetingViewRowLocation:
            reUseIdentifier = @"FXMeetingViewRowLocation";
            break;
        case FXMeetingViewRowTiming:
            reUseIdentifier= @"FXMeetingViewRowTiming";
            break;
        case FXMeetingViewRowNotes:
            reUseIdentifier = @"FXMeetingViewRowNotes";
            break;
        case FXMeetingViewRowOrganizer:
            reUseIdentifier = @"FXMeetingViewRowOrganizer";
            break;
        case FXMeetingViewRowAvailability:
            reUseIdentifier = @"FXMeetingViewRowAvailability";
            break;
        case FXMeetingViewRowAlert:
            reUseIdentifier = @"FXMeetingViewRowAlert";
            break;
        default:
            break;

    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reUseIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reUseIdentifier];
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell* cell = [self getCellForTableView:tableView atIndexPath:indexPath];
    
    cell.textLabel.text = @"";
    cell.detailTextLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.userInteractionEnabled = YES;
    switch (indexPath.row)
    {
        case FXMeetingViewRowTitle:
        {
            ((FXMeetingViewTitleCell*)cell).title = self.event.title;
            cell.userInteractionEnabled = NO;

            break;
        }
        case FXMeetingViewRowLocation:
            ((FXMeetingViewTitleCell*)cell).title = self.event.location;
            cell.userInteractionEnabled = NO;
            break;
            
        case FXMeetingViewRowTiming:
        {
            ((FXMeetingViewTitleCell*)cell).title =[self.event dateAndTimeString];
            cell.userInteractionEnabled = NO;
            break;
        }
        case FXMeetingViewRowOrganizer:
            cell.textLabel.text = @"Invitation from"; //BARE_STRING
            cell.detailTextLabel.text = self.event.organizerName;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            break;
            
        case FXMeetingViewRowInvitees:
            cell.textLabel.text= [NSString stringWithFormat:@"%d attendees",[self.event.attendees count]]; //BARE_STRING
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            break;
        case FXMeetingViewRowAlert:
            cell.textLabel.text = FXLLocalizedStringFromTable(@"Reminder", @"ReminderEditController", @"Title for view controller");
            
            switch(self.event.reminderMinutes)
            {
                default:
                case -1:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"NONE", @"ReminderEditController", @"Chosen option not to have a reminder for an event");
                    break;
                case 0:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"0_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 0 minutes before an event");
                    break;
                case 5:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"5_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 5 minutes before an event");
                    break;
                case 10:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"10_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 10 minutes before an event");
                    break;
                case 15:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"15_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 15 minutes before an event");
                    break;
                case 30:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"30_MINUTES", @"ReminderEditController", @"Chosen option to have a reminder 30 minutes before an event");
                    break;
                case 60:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"1_HOUR", @"ReminderEditController", @"Chosen option to have a reminder 1 hour before an event");
                    break;
                case 120:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"2_HOURS", @"ReminderEditController", @"Chosen option to have a reminder 2 hours before an event");
                    break;
                case 24*60:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"1_DAY", @"ReminderEditController", @"Chosen option to have a reminder 1 day before an event");
                    break;
                case 2*24*60:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"2_DAYS", @"ReminderEditController", @"Chosen option to have a reminder 2 days before an event");
                    break;
            }

            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            break;
        case FXMeetingViewRowNotes:
            ((FXMeetingViewTitleCell*)cell).title = self.event.notes;
            break;
        case FXMeetingViewRowAvailability:
            cell.textLabel.text = @"Availability";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            switch(self.event.busyStatus)
            {
                case kBusyStatusBusy:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"BUSY", @"Calendar", @"Displayed availability - busy");
                    break;
                case kBusyStatusFree:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"FREE", @"Calendar", @"Displayed availability - free");
                    break;
                case kBusyStatusTentative:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"TENTATIVE", @"Calendar", @"Displayed availability - tentative");
                    break;
                case kBusyStatusOutOfOffice:
                    cell.detailTextLabel.text         = FXLLocalizedStringFromTable(@"OUT_OF_OFFICE", @"Calendar", @"Displayed availability - out of office");
                    break;
            }
            break;
    }
    // Configure the cell...
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!sizingCell)
    {
        sizingCell = (FXMeetingViewTitleCell*) [tableView dequeueReusableCellWithIdentifier:@"FXMeetingViewRowTitle"];
        sizingCell.frame = CGRectInset(sizingCell.frame,20.0,0);
    }
    [sizingCell prepareForReuse];
    

    switch (indexPath.row)
    {
        case FXMeetingViewRowTitle:
        {
            ((FXMeetingViewTitleCell*)sizingCell).title = self.event.title;
            return sizingCell.heightRequiredForCell;

            break;
        }
        case FXMeetingViewRowLocation:
            ((FXMeetingViewTitleCell*)sizingCell).title = self.event.location;
            return sizingCell.heightRequiredForCell;

            break;
            
        case FXMeetingViewRowTiming:
        {
            ((FXMeetingViewTitleCell*)sizingCell).title = [self.event dateAndTimeString];
            return sizingCell.heightRequiredForCell;
            break;
        }
        case FXMeetingViewRowNotes:
            if ([self.event.notes length] > 0)
            {
                ((FXMeetingViewTitleCell*)sizingCell).title = self.event.notes;
                return sizingCell.heightRequiredForCell;
            }
            else
                return 0;
            break;

        case FXMeetingViewRowOrganizer:
        case FXMeetingViewRowInvitees:
        case FXMeetingViewRowAlert:
        default:
            return 44;
            break;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    switch (indexPath.row)
    {
        case FXMeetingViewRowOrganizer:
        {
            EmailAddress* email = [[EmailAddress alloc] initWithName:self.event.organizerName address:self.event.organizerEmail];
            UIViewController* vc = [email asContactViewController];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case FXMeetingViewRowInvitees:
        {
            FXMeetingAttendeeViewerViewController* vc = [FXMeetingAttendeeViewerViewController controller];
            vc.event = self.event;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            break;
        }
        case FXMeetingViewRowAvailability:
        {
            AvailabilityEditController* aController = [[AvailabilityEditController alloc] initWithNibName:@"AvailabilityView" bundle:[FXLSafeZone getResourceBundle]];
            aController.event = self.event;
            aController.changeDelegate = self;
            aController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:aController animated:YES];
            break;
        }
        case FXMeetingViewRowAlert:
        {
            ReminderEditController* aController = [[ReminderEditController alloc] initWithNibName:@"ReminderView" bundle:[FXLSafeZone getResourceBundle]];
            aController.event = self.event;
            aController.hidesBottomBarWhenPushed = YES;
            aController.title = @"Reminder";
            aController.changeDelegate = self;
            [self.navigationController pushViewController:aController animated:YES ];
            break;
        }
        default:
            break;
            
    }


}

- (Attendee*)attendeeForSelf
{
    for(Attendee* anAttendee in self.event.attendees) {
        if(anAttendee.attendeeSort == kAttendeeSortSelf) {
            return anAttendee;
        }
    }
    FXDebugLog(kFXLogActiveSync, @"attendeeForSelf invalid");
    return nil;
}

- (Attendee*)attendeeForOrganizer
{
    for(Attendee* anAttendee in self.event.attendees) {
        if(anAttendee.attendeeSort == kAttendeeSortOrganizer) {
            return anAttendee;
        }
    }
    FXDebugLog(kFXLogActiveSync, @"attendeeForOrganizer invalid");
    return nil;
}

- (void)response:(UISegmentedControl*)aSegmentedControl
{
    Attendee* anAttendee = [self attendeeForSelf];
    switch(aSegmentedControl.selectedSegmentIndex)
    {
        case kResponseAccept:
            anAttendee.attendeeStatus = kAttendeeResponseAccept;
            break;
        case kResponseTentative:
            anAttendee.attendeeStatus = kAttendeeResponseTentative;
            break;
        case kResponseDecline:
            anAttendee.attendeeStatus = kAttendeeResponseDecline;
            break;
    }
    
    [self.event.folder changedObject:self.event];
    [self.event.folder commitObjects];
}

- (void)setResponse:(Attendee*)anAttendee
{
    if(anAttendee) {
        switch(anAttendee.attendeeStatus) {
            case kAttendeeResponseAccept:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseAccept;
                break;
            case kAttendeeResponseDecline:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseDecline;
                break;
            case kAttendeeResponseTentative:
                self.meetingSegmentedControl.selectedSegmentIndex = kResponseTentative;
                break;
            case kAttendeeResponseNotResponded:
            case kAttendeeResponseUnknown:
                self.meetingSegmentedControl.selectedSegmentIndex = -1;
                break;
        }
    }
}

#pragma mark Data change Delegates

-(void)availabilityEditController:(AvailabilityEditController *)availabilityEditController didModifyEvent:(Event *)event
{
    [self.event.folder changedObject:self.event];
    [self.event.folder commitObjects];
    [self.tableView reloadData];
}

-(void)reminderEditController:(ReminderEditController *)controller didChangeEvent:(Event *)event
{
    [self.event.folder changedObject:self.event];
    [self.event.folder commitObjects];
    [self.tableView reloadData];
    
}


@end
