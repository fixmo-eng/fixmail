//
//  FMSplitViewDelegate.h
//  FixMail
//
//  Created by Sean Langley on 11/14/12.
//
//

#import <Foundation/Foundation.h>
#import "MGSplitViewController.h"

@interface FMSplitViewDelegate : NSObject <MGSplitViewControllerDelegate>
@property (nonatomic,strong) IBOutlet MGSplitViewController* splitViewController;
@property (nonatomic,strong) IBOutlet UIViewController* detailViewController;
@property (nonatomic,strong) IBOutlet UIViewController* masterViewController;

@property (nonatomic,strong,readonly) UIPopoverController *navigationPopoverController;
@property (nonatomic,strong,readonly) UINavigationController* detailNavController;
@property (nonatomic,strong,readonly) UIBarButtonItem *navigationPaneButtonItem;

-(void)updateNavigationButton;
@end
