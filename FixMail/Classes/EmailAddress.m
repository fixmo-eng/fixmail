/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "EmailAddress.h"
#import "FXMEmailSecurity.h"
#import "ASContact.h"
#import "FXDatabase.h"
#import "showContactVC.h"
#import "editContactVC.h"

@interface EmailAddress ()
@property (nonatomic, strong) NSTimer *typingTimer;
@end

@implementation EmailAddress

// Constants
static const NSString* kNameKey = @"n";
static const NSString* kEmailKey = @"e";

// taken from StackOverflow:
// http://stackoverflow.com/questions/800123/best-practices-for-validating-email-address-in-objective-c-on-ios-2-0

// another one is:
// NSString *regExPattern =
//	@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
//	@"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
//	@"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
//	@"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
//	@"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
//	@"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
//	@"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"; 
// but this seems a bit of overkill
static NSString *const kEmailRegExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$";

@synthesize typingTimer;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithName:(NSString*)aName address:(NSString*)anAddress
{
    if (self = [super init]) {
		_certExists		= NO;
		_addressIsValid = NO;
        self.name       = aName;
        self.address    = anAddress;
		[self checkNameForEmailAddress];
    }
    return self;
}

- (id)initWithString:(NSString*)aNameAndAddress
{
    self = [super init];
	if (!self) return nil;

	NSCharacterSet* aContactCleaner = [NSCharacterSet characterSetWithCharactersInString:@" \">"];

	NSString* aName     = NULL;
	NSString* anAddress = NULL;
	NSArray* aFields = [aNameAndAddress componentsSeparatedByString:@"<"];
	NSUInteger aCount = [aFields count];
	for(int i = 0 ; i < aCount ; ++i) {
		NSString* aString = [aFields objectAtIndex:i];
		switch(i) {
			case 0:
				aName = [aString stringByTrimmingCharactersInSet:aContactCleaner];
				break;
			case 1:
				anAddress = [aString stringByTrimmingCharactersInSet:aContactCleaner];
				break;
		}
	}

	if(aName.length > 0 && anAddress.length > 0) {
		self.name       = aName;
		self.address    = anAddress;
	} else if(anAddress.length > 0) {
		self.address    = anAddress;
	} else {
		self.address    = [EmailAddress extractEmailAddress:aNameAndAddress];
		if (self.address.length == 0) {
			self.name = aNameAndAddress;
		}
	}
	[self checkNameForEmailAddress];
	FXDebugLog(kFXLogASEmail, @"EmailAddress initWithString (%@): %@ <%@>", aNameAndAddress, _name, _address);

    return self;
}

- (NSDictionary*)asDictionary
{
    NSMutableDictionary* aDictionary = [NSMutableDictionary dictionaryWithCapacity:2];
    if([_name length] > 0) {
        [aDictionary setObject:_name forKey:kNameKey];
    }
    
    if([_address length] > 0) {
        [aDictionary setObject:_address forKey:kEmailKey];
    }
    
    return aDictionary;
}

- (NSString *) fullEmailAddress
{
	NSString *theAddress = nil;
	if (([_name length] > 0) && ([_address length] > 0)) {
		// make sure that they're not the same string
		theAddress = [NSString stringWithFormat:@"%@ <%@>", _name, _address];
	} else if ([_address length] > 0) {
		theAddress = _address;
	}
	return theAddress;
}

- (BOOL) isValidEmailAddress
{
	return _addressIsValid;
}

- (void) checkNameForEmailAddress
{
	// DO NOT allow the name to be a valid email address - will screw up emails
	if ([EmailAddress isProperRFCEmailAddress:self.name]) {
		self.name = @"";
	}

	return;
}

- (BOOL) hasCertificate
{
	if (!_certExists) {
		_certExists = [FXMEmailSecurity hasCertificateForEmail:self.address];
	}
	return _certExists;
}

- (void) setAddress:(NSString *)inAddress
{
	_address = inAddress;
	_addressIsValid = [EmailAddress isProperRFCEmailAddress:_address];
}

- (ASContact *)asExistingContact
{	
	ASContact *contact = nil;
	NSMutableArray *contacts = [FXDatabase contactListForEmailAddress:self.address];
	if (contacts.count > 0) {
		// if > 1, figure out whether to show first or give option
		contact = [contacts objectAtIndex:0];
	}	
	return contact;
}

/* Return a view controller either displaying the contact or as a new contact screen to be filled out */
- (UIViewController *)asContactViewController
{
	ASContact *contact = [self asExistingContact];
	
	NSString *storyboard = nil;
	if (IS_IPAD()) {
		storyboard = kStoryboard_Contacts_iPad;
	} else {
		storyboard = kStoryboard_Contacts_iPhone;
	}
	
	UIViewController *contactVC = nil;
	
	if (contact) {
		
		showContactVC *showContactVC = [UIStoryboard storyboardWithName:storyboard identifier:@"showContactVC"];
		showContactVC.navigationItem.title = FXLLocalizedStringFromTable(@"CONTACT", @"ASContact", @"Navigation bar title when click on contact bubble and contact does not exist, and a new contact screen is displayed");
		showContactVC.contact = contact;
		contactVC = showContactVC;
		
	} else {
		editContactVC *editContactVC = [UIStoryboard storyboardWithName:storyboard identifier:@"editContactVC"];
		editContactVC.navigationItem.title = FXLLocalizedStringFromTable(@"CREATE_NEW_CONTACT", @"ASContact", @"Navigation bar title when click on contact bubble and contact does not exist, and a new contact screen is displayed");
		contact = [[ASContact alloc] init];
		
		contact.emailAddress1 =  self.address;
		contact.lastName = @"";
		NSArray *names = [self.name componentsSeparatedByString:@" "];
		for (int i = 0; i < names.count; i++) {
			if (i == 0) {
				contact.firstName = [names objectAtIndex:i];
			} else {
				if (contact.lastName.length > 0) {
					contact.lastName = [contact.lastName stringByAppendingString:@" "];
				}
				contact.lastName = [contact.lastName stringByAppendingString:[names objectAtIndex:i]];
			}
		}
		
		[editContactVC setContact:contact newContact:YES];
		contactVC = editContactVC;
	}
	
	return contactVC;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
	[self writeString:string		tag:@"name"         value:_name];
	[self writeString:string		tag:@"address"      value:_address];

	[string appendString:@"}\n"];
	
	return string;
}

#pragma mark -

+ (NSString *) extractEmailAddress:(NSString *)inEmailAddress
{
	if (inEmailAddress.length < 6) return nil;

	NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:kEmailRegExPattern options:NSRegularExpressionCaseInsensitive error:nil];

	NSRange rangeOfFirstMatch = [regEx rangeOfFirstMatchInString:inEmailAddress options:0 range:NSMakeRange(0, inEmailAddress.length)];
	if (NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) return nil;

	return [inEmailAddress substringWithRange:rangeOfFirstMatch];
}

+ (BOOL) isProperRFCEmailAddress:(NSString *)inEmailAddress
{
	if (inEmailAddress.length < 6) return NO; // minimum would be a@b.cc

	NSRegularExpression *regEx = [NSRegularExpression regularExpressionWithPattern:kEmailRegExPattern options:NSRegularExpressionCaseInsensitive error:nil];
	NSUInteger regExMatches = [regEx numberOfMatchesInString:inEmailAddress options:0 range:NSMakeRange(0, inEmailAddress.length)];

	return (regExMatches == 0) ? NO : YES;
}

@end
