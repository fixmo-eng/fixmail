/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASContact.h"
#import "ASFolder.h"
#import "AppSettings.h"
#import "NSData+Base64.h"
#import "FXContactUtils.h"

@implementation ASContact

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithFolder:(ASFolder*)aFolder
{
    if (self = [super init]) {
        self.folder = aFolder;
    }
    
    return self;
}

- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: ASContact %@ %@: %@", where, [e name], [e reason]);
}

- (bool) isInLocalDB
{
	return (self.contactID != 0);
}

- (bool) isOnServer
{
	if (self.serverId.length == 0) return false;
	return (self.folder != nil);
}

- (bool) isCompany
{
	if ([self.firstName length] > 0) return false;
	if ([self.lastName length] > 0) return false;
	if ([self.middleName length] > 0) return false;
	if ([self.company length] > 0) return true;

	return false;
}

-(BOOL)hasName
{
	if (self.firstName.length > 0) return YES;
	if (self.lastName.length > 0) return YES;
	if (self.middleName.length > 0) return YES;
	
	return NO;
}

- (NSString *) displayName
{
	if (self.isCompany) return self.company;

	return [FXContactUtils makeFullName:self.firstName middle:self.middleName last:self.lastName];
}

- (NSString *) firstAndMiddleName
{
	if (self.isCompany) return self.company;

	NSString *theString = @"";
	if ((self.firstName.length > 0) && (self.middleName.length > 0)) {
		theString = [NSString stringWithFormat:@"%@ %@", self.firstName, self.middleName];
	} else if (self.firstName.length > 0) {
		theString = self.firstName;
	} else if (self.middleName.length > 0) {
		theString = self.middleName;
	}
	return theString;
}

- (NSString *) sortKey
{
	NSString *theSortKey = @"";
	if (self.isCompany) {
		theSortKey = self.company;
	} else {
		if ([AppSettings contactsSortOrder] == sortOrderFirstLast) {
			theSortKey = [[NSString stringWithFormat:@"%@%@", self.firstName, self.lastName] lowercaseString];
		} else {
			theSortKey = [[NSString stringWithFormat:@"%@%@", self.lastName, self.firstName] lowercaseString];
		}
	}

	return theSortKey;
}

- (NSString *) description
{
	return [NSString stringWithFormat:@"%@ %@ %@", self.displayName, self.emailAddress1, self.company];
}

- (NSString *) composeAddress:(NSString *)inStreet city:(NSString *)inCity state:(NSString *)inState code:(NSString *)inPostalCode country:(NSString *)inCountry
{
	NSString *theString = @"";
	if (inStreet.length > 0) theString = [theString stringByAppendingFormat:@"%@\n", inStreet];
	if (inCity.length > 0) theString = [theString stringByAppendingFormat:@"%@ ", inCity];
	if (inState.length > 0) theString = [theString stringByAppendingFormat:@"%@ ", inState];
	if (inPostalCode.length > 0) theString = [theString stringByAppendingFormat:@"%@\n", inPostalCode];
	if (inCountry.length > 0) theString = [theString stringByAppendingFormat:@"%@\n", inCountry];
	
	theString = [theString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	return theString;
}

- (NSString *) companyAddress
{
	return [self composeAddress:self.companyStreet
						   city:self.companyCity
						  state:self.companyState
						   code:self.companyPostalCode
						country:self.companyCountry
			];
}

- (NSString *) homeAddress
{
	return [self composeAddress:self.homeStreet
						   city:self.homeCity
						  state:self.homeState
						   code:self.homePostalCode
						country:self.homeCountry
			];
}

- (NSString *) otherAddress
{
	return [self composeAddress:self.otherStreet
						   city:self.otherCity
						  state:self.otherState
						   code:self.otherPostalCode
						country:self.otherCountry
			];
}

- (void) reset
{
	self.firstName = nil;
	self.lastName = nil;
	self.middleName = nil;
	self.alias = nil;

	self.company = nil;
	self.department = nil;
	self.title = nil;
	self.companyLocation = nil;
	self.webPage = nil;

	self.emailAddress1 = nil;
	self.emailAddress2 = nil;
	self.emailAddress3 = nil;

	self.mobilePhone = nil;
	self.pager = nil;
	self.otherPhone1 = nil;
	self.otherPhone2 = nil;

	self.companyStreet = nil;
	self.companyCity = nil;
	self.companyState = nil;
	self.companyPostalCode = nil;
	self.companyCountry = nil;
	self.companyPhone1 = nil;
	self.companyPhone2 = nil;
	self.companyFax = nil;

	self.homeStreet = nil;
	self.homeCity = nil;
	self.homeState = nil;
	self.homePostalCode = nil;
	self.homeCountry = nil;
	self.homePhone1 = nil;
	self.homePhone2 = nil;
	self.homeFax = nil;

	self.otherStreet = nil;
	self.otherCity = nil;
	self.otherState = nil;
	self.otherPostalCode = nil;
	self.otherCountry = nil;

	self.notes = nil;
	self.spouse = nil;
	self.birthday = nil;

	self.picture = nil;
	self.weightedRank = 0;
}

#define kMaximumImageSize		(32 * 1024)

- (void) setImage:(UIImage *)inImage
{
	// underlying data is actually in self.picture
	self.picture = nil;
	if (inImage == nil) return;

	NSData *imageData = [FXContactUtils getImage:inImage maxSize:kMaximumImageSize];
	if (imageData != nil) {
		self.pictureString = [imageData base64EncodedString];
		self.picture = [NSData dataWithBytes:self.pictureString.UTF8String length:self.pictureString.length];
	}

	return;
}

- (UIImage *) image
{
	if (self.picture.length == 0) return nil;

	// the data is stored as a Base64 encoded string
	NSString *theString = [[NSString alloc] initWithData:self.picture encoding:NSUTF8StringEncoding];
	NSData *theData = [NSData dataFromBase64String:theString];

	return [UIImage imageWithData:theData];
}

#pragma mark - ASSyncContact

- (NSString *) uniqueKey
{
	// CANNOT use the serverId (even though it is unique) because when
	// adding a new contact, it will be blank; but on return from the server, it will be non-blank
//	if (self.serverId.length > 0) return self.serverId;

	// so use other stuff
	if (self.emailAddress1.length > 0) return self.emailAddress1;
	else if (self.emailAddress2.length > 0) return self.emailAddress2;
	else if (self.emailAddress3.length > 0) return self.emailAddress3;

	// if all else fails, this should work
	return self.displayName;
}

#pragma mark - Searching

- (bool) findSearchString:(NSString *)inSearchString field:(NSString *)inField criteria:(NSString *)inCriteria
{	
	if (!inSearchString) {
		return false;
	}
	
	NSRange theRange;
	theRange = [inField rangeOfString:inSearchString options:NSCaseInsensitiveSearch];
	if (theRange.length == 0) return false;

	_matchedCriteria = inCriteria;
	_matchedString = inField;
	return true;
}

- (bool) matchesSearchString:(NSString *)inSearchString
{
	_matchedCriteria = nil;
	_matchedString = nil;

	if ([self findSearchString:inSearchString field:self.firstName criteria:FXLLocalizedStringFromTable(@"FIRST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by first name")]) return true;
	if ([self findSearchString:inSearchString field:self.middleName criteria:FXLLocalizedStringFromTable(@"MIDDLE_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by middle name")]) return true;
	if ([self findSearchString:inSearchString field:self.lastName criteria:FXLLocalizedStringFromTable(@"LAST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by last name")]) return true;
	if ([self findSearchString:inSearchString field:self.alias criteria:FXLLocalizedStringFromTable(@"ALIAS/NICKNAME_SEARCH", @"ASContact", @"Search criteria string when searching by alias or nickname")]) return true;
	if ([self findSearchString:inSearchString field:self.title criteria:FXLLocalizedStringFromTable(@"TITLE_SEARCH", @"ASContact", @"Search criteria string when searching by title")]) return true;
	if ([self findSearchString:inSearchString field:self.webPage criteria:FXLLocalizedStringFromTable(@"WEB_PAGE_SEARCH", @"ASContact", @"Search criteria string when searching by web page")]) return true;

	if ([self findSearchString:inSearchString field:self.emailAddress1 criteria:FXLLocalizedStringFromTable(@"EMAIL_1_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return true;
	if ([self findSearchString:inSearchString field:self.emailAddress2 criteria:FXLLocalizedStringFromTable(@"EMAIL_2_SEARCH", @"ASContact", @"Search criteria string when searching by second email address")]) return true;
	if ([self findSearchString:inSearchString field:self.emailAddress3 criteria:FXLLocalizedStringFromTable(@"EMAIL_3_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return true;

	if ([self findSearchString:inSearchString field:self.mobilePhone criteria:FXLLocalizedStringFromTable(@"MOBILE_PHONE_SEARCH", @"ASContact", @"Search criteria string when searching by mobile phone")]) return true;
	if ([self findSearchString:inSearchString field:self.pager criteria:FXLLocalizedStringFromTable(@"PAGER_SEARCH", @"ASContact", @"Search criteria string when searching by pager")]) return true;
	if ([self findSearchString:inSearchString field:self.otherPhone1 criteria:FXLLocalizedStringFromTable(@"OTHER_PHONE_1_SEARCH", @"ASContact", @"Search criteria string when searching by first other phone number")]) return true;
	if ([self findSearchString:inSearchString field:self.otherPhone2 criteria:FXLLocalizedStringFromTable(@"OTHER_PHONE_2_SEARCH", @"ASContact", @"Search criteria string when searching by second other phone number")]) return true;

	if ([self findSearchString:inSearchString field:self.companyPhone1 criteria:FXLLocalizedStringFromTable(@"COMPANY_PHONE_1_SEARCH", @"ASContact", @"Search criteria string when searching by first company phone number")]) return true;
	if ([self findSearchString:inSearchString field:self.companyPhone2 criteria:FXLLocalizedStringFromTable(@"COMPANY_PHONE_2_SEARCH", @"ASContact", @"Search criteria string when searching by second company phone number")]) return true;
	if ([self findSearchString:inSearchString field:self.companyFax criteria:FXLLocalizedStringFromTable(@"COMPANY_PHONE_3_SEARCH", @"ASContact", @"Search criteria string when searching by third company phone number")]) return true;

	if ([self findSearchString:inSearchString field:self.homePhone1 criteria:FXLLocalizedStringFromTable(@"HOME_PHONE_1_SEARCH", @"ASContact", @"Search criteria string when searching by first home phone number")]) return true;
	if ([self findSearchString:inSearchString field:self.homePhone2 criteria:FXLLocalizedStringFromTable(@"HOME_PHONE_2_SEARCH", @"ASContact", @"Search criteria string when searching by second home phone number")]) return true;
	if ([self findSearchString:inSearchString field:self.homeFax criteria:FXLLocalizedStringFromTable(@"HOME_FAX_SEARCH", @"ASContact", @"Search criteria string when searching by home fax number")]) return true;

	if ([self findSearchString:inSearchString field:self.notes criteria:FXLLocalizedStringFromTable(@"NOTES_SEARCH", @"ASContact", @"Search criteria string when searching in notes")]) return true;

	return false;
}

-(NSString *)nameMatchesSearchString:(NSString*)searchString
{
	if ([self findSearchString:searchString field:self.firstName criteria:FXLLocalizedStringFromTable(@"FIRST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by first name")]) return self.firstName;
	if ([self findSearchString:searchString field:self.middleName criteria:FXLLocalizedStringFromTable(@"MIDDLE_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by middle name")]) return self.middleName;
	if ([self findSearchString:searchString field:self.lastName criteria:FXLLocalizedStringFromTable(@"LAST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by last name")]) return self.lastName;
	
	return @"";
}

-(NSString *)emailMatchesSearchString:(NSString*)searchString
{
	if ([self findSearchString:searchString field:self.emailAddress1 criteria:FXLLocalizedStringFromTable(@"EMAIL_1_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return self.emailAddress1;
	if ([self findSearchString:searchString field:self.emailAddress2 criteria:FXLLocalizedStringFromTable(@"EMAIL_2_SEARCH", @"ASContact", @"Search criteria string when searching by second email address")]) return self.emailAddress2;
	if ([self findSearchString:searchString field:self.emailAddress3 criteria:FXLLocalizedStringFromTable(@"EMAIL_3_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return self.emailAddress3;
	
	return @"";
}

-(ERecipientSearchMatch)matchEmailOrNameSearchString:(NSString *)searchString
{
	// Might want to use predicate instead
	if ([self findSearchString:searchString field:self.firstName criteria:FXLLocalizedStringFromTable(@"FIRST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by first name")]) return kFirstNameMatch;
	if ([self findSearchString:searchString field:self.middleName criteria:FXLLocalizedStringFromTable(@"MIDDLE_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by middle name")]) return kMiddleNameMatch;
	if ([self findSearchString:searchString field:self.lastName criteria:FXLLocalizedStringFromTable(@"LAST_NAME_SEARCH", @"ASContact", @"Search criteria string when searching by last name")]) return kLastNameMatch;
	
	
	if ([self findSearchString:searchString field:self.emailAddress1 criteria:FXLLocalizedStringFromTable(@"EMAIL_1_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return kEmailAddress1Match;
	if ([self findSearchString:searchString field:self.emailAddress2 criteria:FXLLocalizedStringFromTable(@"EMAIL_2_SEARCH", @"ASContact", @"Search criteria string when searching by second email address")]) return kEmailAddress2Match;
	if ([self findSearchString:searchString field:self.emailAddress3 criteria:FXLLocalizedStringFromTable(@"EMAIL_3_SEARCH", @"ASContact", @"Search criteria string when searching by first email address")]) return kEmailAddress3Match;
	
	return 0;
	
}



- (NSString *) matchedCriteria
{
	return _matchedCriteria;
}

- (NSString *) matchedString
{
	return _matchedString;
}

- (NSArray *) tableArrayStrings
{
	// 0: phones
	NSMutableArray *phoneArray = [NSMutableArray new];
	if (self.mobilePhone.length > 0) [phoneArray addObject:[NSDictionary dictionaryWithObject:self.mobilePhone forKey:FXLLocalizedStringFromTable(@"MOBILE", @"ASContact", @"Title for mobile number field")]];
	if (self.pager.length > 0) [phoneArray addObject:[NSDictionary dictionaryWithObject:self.pager forKey:FXLLocalizedStringFromTable(@"PAGER", @"ASContact", @"Title for pager number field")]];
	if (self.otherPhone1.length > 0) [phoneArray addObject:[NSDictionary dictionaryWithObject:self.otherPhone1 forKey:FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", @"Title for first phone number field")]];
	if (self.otherPhone2.length > 0) [phoneArray addObject:[NSDictionary dictionaryWithObject:self.otherPhone2 forKey:FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", @"Title for second phone number field")]];

	// 1: email
	NSMutableArray *emailArray = [NSMutableArray new];
	if (self.emailAddress1.length > 0) [emailArray addObject:[NSDictionary dictionaryWithObject:self.emailAddress1 forKey:FXLLocalizedStringFromTable(@"EMAIL_1", @"ASContact", @"Title for first email address field")]];
	if (self.emailAddress2.length > 0) [emailArray addObject:[NSDictionary dictionaryWithObject:self.emailAddress2 forKey:FXLLocalizedStringFromTable(@"EMAIL_2", @"ASContact", @"Title for second email address field")]];
	if (self.emailAddress3.length > 0) [emailArray addObject:[NSDictionary dictionaryWithObject:self.emailAddress3 forKey:FXLLocalizedStringFromTable(@"EMAIL_3", @"ASContact", @"Title for third email address field")]];

	// 2: company address
	NSMutableArray *companyArray = [NSMutableArray new];
	if (self.companyAddress.length > 0) [companyArray addObject:[NSDictionary dictionaryWithObject:self.companyAddress forKey:FXLLocalizedStringFromTable(@"COMPANY", @"ASContact", @"Title for company address fields")]];
	if (self.companyPhone1.length > 0) [companyArray addObject:[NSDictionary dictionaryWithObject:self.companyPhone1 forKey:FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", nil)]];
	if (self.companyPhone2.length > 0) [companyArray addObject:[NSDictionary dictionaryWithObject:self.companyPhone2 forKey:FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", nil)]];
	if (self.companyFax.length > 0) [companyArray addObject:[NSDictionary dictionaryWithObject:self.companyFax forKey:FXLLocalizedStringFromTable(@"FAX", @"ASContact", @"Title for fax number field")]];

	// 3: home address
	NSMutableArray *homeArray = [NSMutableArray new];
	if (self.homeAddress.length > 0) [homeArray addObject:[NSDictionary dictionaryWithObject:self.homeAddress forKey:FXLLocalizedStringFromTable(@"HOME", @"ASContact", @"Title for home address field")]];
	if (self.homePhone1.length > 0) [homeArray addObject:[NSDictionary dictionaryWithObject:self.homePhone1 forKey:FXLLocalizedStringFromTable(@"PHONE_1", @"ASContact", nil)]];
	if (self.homePhone2.length > 0) [homeArray addObject:[NSDictionary dictionaryWithObject:self.homePhone2 forKey:FXLLocalizedStringFromTable(@"PHONE_2", @"ASContact", nil)]];
	if (self.homeFax.length > 0) [homeArray addObject:[NSDictionary dictionaryWithObject:self.homeFax forKey: FXLLocalizedStringFromTable(@"FAX", @"ASContact", nil)]];

	// 4: other address
	NSMutableArray *otherArray = [NSMutableArray new];
	if (self.otherAddress.length > 0) [otherArray addObject:[NSDictionary dictionaryWithObject:self.otherAddress forKey:FXLLocalizedStringFromTable(@"OTHER", @"ASContact", @"Title for other address field (after company and home)")]];

	// 5: other/etc
	NSMutableArray *etcArray = [NSMutableArray new];
	if (self.webPage.length > 0) [etcArray addObject:[NSDictionary dictionaryWithObject:self.webPage forKey:FXLLocalizedStringFromTable(@"URL", @"ASContact", @"Title for URL field")]];
	if (self.notes.length > 0) [etcArray addObject:[NSDictionary dictionaryWithObject:self.notes forKey:FXLLocalizedStringFromTable(@"NOTES", @"ASContact", @"Title for notes field")]];

	NSMutableArray *theArray = [NSMutableArray new];
	if (phoneArray.count > 0) [theArray addObject:phoneArray];
	if (emailArray.count > 0) [theArray addObject:emailArray];
	if (companyArray.count > 0) [theArray addObject:companyArray];
	if (homeArray.count > 0) [theArray addObject:homeArray];
	if (otherArray.count > 0) [theArray addObject:otherArray];
	if (etcArray.count > 0) [theArray addObject:etcArray];

	return theArray;
}

////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)_baseBodyParser:(FastWBXMLParser*)aParser tag:(ASTag)aNextTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:aNextTag]) != AS_END) {
        switch(tag) {
            case BASE_DATA:
            {
                FXDebugLog(kFXLogFIXME, @"ASContact BASE_DATA %@", [aParser getString]);
                break;
            }
            case BASE_TYPE:
            {
                FXDebugLog(kFXLogFIXME, @"ASContact BASE_TYPE: %d", [aParser getInt]);
                break;
            }
            case BASE_ESTIMATED_DATA_SIZE:
            {
                FXDebugLog(kFXLogFIXME, @"ASContact BASE_ESTIMATED_DATA_SIZE: %u", [aParser getUnsignedInt]);
                break;
            }
            case BASE_TRUNCATED:
            {
                FXDebugLog(kFXLogFIXME, @"ASContact BASE_TRUNCATED: %D", [aParser getBool]);
                break;
            }
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)_categoryParser:(FastWBXMLParser*)aParser tag:(ASTag)aNextTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:aNextTag]) != AS_END) {
        switch(tag) {
            case CONTACTS_CATEGORY:
                FXDebugLog(kFXLogFIXME, @"ASContact CONTACTS_CATEGORY: %@", [aParser getString]);
                break;
            default:
                [aParser skipTag];
                break;
        }
    }
}

- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag
{
    ASTag tag;
    while ((tag = [aParser nextTag:aTag]) != AS_END) {
        switch(tag) {
			case CONTACTS_FIRST_NAME:					self.firstName = [aParser getString]; break;
            case CONTACTS_LAST_NAME:					self.lastName = [aParser getString]; break;
            case CONTACTS_MIDDLE_NAME:					self.middleName = [aParser getString]; break;
			case CONTACTS_ALIAS:						self.alias = [aParser getString]; break;

			case CONTACTS_COMPANY_NAME:					self.company = [aParser getString]; break;
			case CONTACTS_DEPARTMENT:					self.department = [aParser getString]; break;
			case CONTACTS_JOB_TITLE:					self.title = [aParser getString]; break;
			case CONTACTS_OFFICE_LOCATION:				self.companyLocation = [aParser getString]; break;
			case CONTACTS_WEBPAGE:						self.webPage = [aParser getString]; break;

			case CONTACTS_EMAIL1_ADDRESS:				self.emailAddress1 = [FXContactUtils extractEmailAddress:[aParser getString]]; break;
			case CONTACTS_EMAIL2_ADDRESS:				self.emailAddress2 = [FXContactUtils extractEmailAddress:[aParser getString]]; break;
			case CONTACTS_EMAIL3_ADDRESS:				self.emailAddress3 = [FXContactUtils extractEmailAddress:[aParser getString]]; break;

			case CONTACTS_MOBILE_TELEPHONE_NUMBER:		self.mobilePhone = [aParser getString]; break;
			case CONTACTS_PAGER_NUMBER:					self.pager = [aParser getString]; break;
			case CONTACTS_RADIO_TELEPHONE_NUMBER:		self.otherPhone1 = [aParser getString]; break;
			case CONTACTS_CAR_TELEPHONE_NUMBER:			self.otherPhone2 = [aParser getString]; break;

			case CONTACTS_BUSINESS_ADDRESS_STREET:		self.companyStreet = [aParser getString]; break;
			case CONTACTS_BUSINESS_ADDRESS_CITY:		self.companyCity = [aParser getString]; break;
			case CONTACTS_BUSINESS_ADDRESS_STATE:		self.companyState = [aParser getString]; break;
			case CONTACTS_BUSINESS_ADDRESS_POSTAL_CODE:	self.companyPostalCode = [aParser getString]; break;
			case CONTACTS_BUSINESS_ADDRESS_COUNTRY:		self.companyCountry = [aParser getString]; break;
			case CONTACTS_BUSINESS_TELEPHONE_NUMBER:	self.companyPhone1 = [aParser getString]; break;
			case CONTACTS_BUSINESS2_TELEPHONE_NUMBER:	self.companyPhone2 = [aParser getString]; break;
			case CONTACTS_BUSINESS_FAX_NUMBER:			self.companyFax = [aParser getString]; break;

			case CONTACTS_HOME_ADDRESS_STREET:			self.homeStreet = [aParser getString]; break;
			case CONTACTS_HOME_ADDRESS_CITY:			self.homeCity = [aParser getString]; break;
			case CONTACTS_HOME_ADDRESS_STATE:			self.homeState = [aParser getString]; break;
			case CONTACTS_HOME_ADDRESS_POSTAL_CODE:		self.homePostalCode = [aParser getString]; break;
			case CONTACTS_HOME_ADDRESS_COUNTRY:			self.homeCountry = [aParser getString]; break;
			case CONTACTS_HOME_TELEPHONE_NUMBER:		self.homePhone1 = [aParser getString]; break;
			case CONTACTS_HOME2_TELEPHONE_NUMBER:		self.homePhone2 = [aParser getString]; break;
			case CONTACTS_HOME_FAX_NUMBER:				self.homeFax = [aParser getString]; break;

			case CONTACTS_OTHER_ADDRESS_STREET:			self.otherStreet = [aParser getString]; break;
			case CONTACTS_OTHER_ADDRESS_CITY:			self.otherCity = [aParser getString]; break;
			case CONTACTS_OTHER_ADDRESS_STATE:			self.otherState = [aParser getString]; break;
			case CONTACTS_OTHER_ADDRESS_POSTAL_CODE:	self.otherPostalCode = [aParser getString]; break;
			case CONTACTS_OTHER_ADDRESS_COUNTRY:		self.otherCountry = [aParser getString]; break;

			case CONTACTS_BODY:							self.notes = [aParser getString]; break;
			case CONTACTS_SPOUSE:						self.spouse = [aParser getString]; break;
			case CONTACTS_BIRTHDAY:						self.birthday = [aParser getString]; break;

            case CONTACTS_PICTURE:
                self.picture = [aParser getData];
                break;

            case CONTACTS_WEIGHTED_RANK:
                self.weightedRank = [aParser getInt];
                break;
                
            case CONTACTS_CATEGORIES:
                [self _categoryParser:aParser tag:tag];
                break;
                
            case BASE_BODY:
                [self _baseBodyParser:aParser tag:tag];
                break;
            case BASE_NATIVE_BODY_TYPE:
                FXDebugLog(kFXLogFIXME, @"BASE_NATIVE_BODY_TYPE: %d", [aParser getInt]);
                break;
                
            // Not Implemented
            case CONTACTS_FILE_AS:
            {
                NSString* aDisplayName = [aParser getString];
                #pragma unused(aDisplayName)
                break;
            }

            case CONTACTS_ASSISTANT_NAME:
            {
                NSString* anAssistantName = [aParser getString];
                #pragma unused(anAssistantName)
                break;
            }
            case CONTACTS_TITLE:
            {
                NSString* aTitle = [aParser getString];
                #pragma unused(aTitle)
                break;
            }
            case CONTACTS2_MANAGER_NAME:
            {
                NSString* aManagerName = [aParser getString];
                #pragma unused(aManagerName)
                break;
            }
            case CONTACTS2_IM_ADDRESS:
            {
                NSString* anIMAddress = [aParser getString];
                #pragma unused(anIMAddress)
                break;
            }
            case CONTACTS2_COMPANY_MAIN_PHONE:
            {
                NSString* aCompanyMainPhone = [aParser getString];
                #pragma unused(aCompanyMainPhone)
                break;
            }

            default:
                if(tag == AS_END_DOCUMENT) {
                    break;
                }else{
                    [aParser skipTag];
                }
                break;
        }
    }
    //FXDebugLog(kFXLogActiveSync, @"ASContact:\n%@", self);
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark - Debug

- (NSString*) debugDescription
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
	[self writeString:string		tag:@"serverId"			value:self.serverId];
	[self writeString:string		tag:@"displayName"		value:self.displayName];
    [self writeString:string		tag:@"emailAddress"		value:self.emailAddress1];
    [self writeString:string		tag:@"firstName"		value:self.firstName];
    [self writeString:string		tag:@"middleName"		value:self.middleName];
    [self writeString:string		tag:@"lastName"			value:self.lastName];
    if([self.picture length] > 0) {
        [self writeBoolean:string   tag:@"picture"			value:TRUE];
    }
    
	[string appendString:@"}\n"];
	
	return string;
}

@end
