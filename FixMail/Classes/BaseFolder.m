/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseFolder.h"
#import "AccountDBAccessor.h"
#import "BaseAccount.h"
#import "EmailProcessor.h"
#import "LoadEmailDBAccessor.h"
#import "ErrorAlert.h"
#import "SyncManager.h"
#import "SearchRunner.h"
#import "FMDatabaseQueue.h"
#import "FMDatabase.h"

@implementation BaseFolder 

// Properties
@synthesize folderType;
@synthesize account;
@synthesize parent;
@synthesize unread;

// Persistent Properties
@synthesize uid;
@synthesize displayName;

@synthesize accountNum;
@synthesize folderNum;
@synthesize folderCount;

// Object Properties
@synthesize objectsAdded;
@synthesize objectsChanged;
@synthesize objectsDeleted;

// Flags Stored
static const NSUInteger kFlagsStoredMask  = 0x0000ffff;

static const NSUInteger kIsDeleted              = 0x00000001;
static const NSUInteger kIsVisible              = 0x00000002;
static const NSUInteger kIsSyncable             = 0x00000004;
static const NSUInteger kIsInitialSynced        = 0x00000008;
static const NSUInteger kIsSyncWanted           = 0x00000010;
static const NSUInteger kHasOfflineChanges      = 0x00000020;

// Flags Transient
static const NSUInteger kIsPinging              = 0x00010000;
static const NSUInteger kIsMoreAvailable        = 0x00020000;
static const NSUInteger kIsInitialSync          = 0x00040000;
static const NSUInteger kIsSyncing              = 0x00080000;
static const NSUInteger kIsResyncing            = 0x00100000;
static const NSUInteger kIsInitialSyncInterim   = 0x00200000;
static const NSUInteger kIsCountWanted          = 0x00400000;
static const NSUInteger kIsCounting             = 0x00800000;
static const NSUInteger kIsSyncingOfflineChanges= 0x01000000;

// Persistent Store Constants
static NSString* kFolderType            = @"folderType";

static NSString* kUid                   = @"uid";
static NSString* kDisplayName           = @"displayName";       // FIXME - duplicative
static NSString* kFolderDisplayName     = @"folderDisplayName"; // with this

static NSString* kAccountNum            = @"accountNum";
static NSString* kFolderNum             = @"folderNum";
static NSString* kFolderCount           = @"folderCount";

static NSString* kDeleted               = @"deleted";
static NSString* kFlags                 = @"flags";

// Constants
static NSString* kDirty                 = @"dirty";

#if 0
// Static
static sqlite3_stmt*        folderInsertStmt   = nil;
static sqlite3_stmt*        folderFindStmt     = nil;
static sqlite3_stmt*        folderLoadStmt     = nil;
static sqlite3_stmt*        folderCountStmt    = nil;
static sqlite3_stmt*        updateSyncKeyStmt   = nil;
#endif

////////////////////////////////////////////////////////////////////////////////
// ToolBar Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark ToolBar Factory

static UIBarButtonItem*    sProgressItem   = nil;

+ (void)setProgressItem:(UIBarButtonItem*)aProgressItem
{
    sProgressItem = aProgressItem;
}

+ (UIBarButtonItem*)progressItem
{
    return sProgressItem;
}

+ (NSString*)stringForFolderType:(EFolderType)aFolderType
{
    NSString* aString = @"Undefined";
    switch(aFolderType) {
        case kFolderTypeUser:
            aString = @"User"; break;
        case kFolderTypeInbox:
            aString = @"Inbox"; break;
        case kFolderTypeDrafts:
            aString = @"Drafts"; break;
        case kFolderTypeDeleted:
            aString = @"Deleted"; break;
        case kFolderTypeSent:
            aString = @"Sent"; break;
        case kFolderTypeOutbox:
            aString = @"Outbox"; break;
        case kFolderTypeTasks:
            aString = @"Tasks"; break;
        case kFolderTypeCalendar:
            aString = @"Calendar"; break;
        case kFolderTypeContacts:
            aString = @"Contacts"; break;
        case kFolderTypeNotes:
            aString = @"Notes"; break;
        case kFolderTypeJournal:
            aString = @"Journal"; break;
        case kFolderTypeUserMailbox:
            aString = @"User Mailbox"; break;
        case kFolderTypeUserCalendar:
            aString = @"User Calendar"; break;
        case kFolderTypeUserContacts:
            aString = @"User Contacts"; break;
        case kFolderTypeUserTasks:
            aString = @"User Tasks"; break;
        case kFolderTypeUserJournal:
            aString = @"User Journal"; break;
        case kFolderTypeUserNotes:
            aString = @"User Notes"; break;
        case kFolderTypeUnknown:
            aString = @"Unknown"; break;
        case kFolderTypeRecipientInfo:
            aString = @"RecipientInfo"; break;
        case kFolderTypeLocalDrafts:
            aString = @"Local Drafts"; break;
    }
    return aString;
}

+ (void)removeAllFolders
{
    [sFolders removeAllObjects];
}

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

static NSMutableArray* sFolders = nil;

- (id)initWithAccount:(BaseAccount*)anAccount
{
	if (self = [super init]) {
        if(!sFolders) {
            sFolders = [[NSMutableArray alloc] initWithCapacity:10];
        }
        [sFolders addObject:self];
        
        account             = anAccount;
        parent              = NULL;
        
        // Persistent Store
        self.displayName    = @"";

        accountNum          = [anAccount accountNum];
        folderNum           = [sFolders count]-1;
        folderCount         = 0;
        
        // Flags
        flags               = 0x00000000;
		unreadCount			= 0;
        [self setIsVisible:TRUE];
        [self setIsSyncable:TRUE];
        [self setIsInitialSynced:FALSE];
        [self setIsSyncWanted:TRUE];
        
        // Object properties
        self.objectsAdded   = nil;
        self.objectsChanged = nil;
        
        // Local
        children            = nil;
        delegates           = nil;
        dictionary          = [[NSMutableDictionary alloc] initWithCapacity:10];

        lock                = [[NSLock alloc] init];
        dirty               = true;
	}
	
	return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)logException:(NSString*)where exception:(NSException*)e
{
    [ErrorAlert exception:FXLLocalizedStringFromTable(@"BASE_FOLDER_ERROR", @"ErrorAlert", @"Error alert view title for BaseFolder") where:where exception:e];
}

- (void)handleError:(NSString*)anErrorMessage
{
    [ErrorAlert alert:FXLLocalizedStringFromTable(@"BASE_FOLDER_ERROR", @"ErrorAlert", nil) message:anErrorMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////
// Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Utilities

- (int)getInt:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    int anInt = 0;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        anInt = [aNumber intValue];
    }else{
        FXDebugLog(kFXLogActiveSync, @"getInt invalid for: %@", aKey);
    }
    return anInt;
}

- (NSUInteger)getUnsignedInt:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    NSUInteger anInt = 0;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        anInt = [aNumber unsignedIntValue];
    }else{
        FXDebugLog(kFXLogActiveSync, @"getUnsignedInt invalid for: %@", aKey);
    }
    return anInt;
}

- (BOOL)getBool:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    BOOL aBool = false;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        aBool= [aNumber boolValue];
    }else{
        FXDebugLog(kFXLogActiveSync, @"getBool invalid for: %@", aKey);
    }
    return aBool;
}

- (int)combinedFolderNum
{
    return [EmailProcessor combinedFolderNumFor:folderNum withAccount:[account accountNum]];
}

////////////////////////////////////////////////////////////////////////////////
// Load/Store
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Store

+ (EFolderType)folderTypeFromStore:(NSDictionary*)aDictionary
{
    EFolderType aFolderType;
    
    NSNumber* aFolderTypeNumber = [aDictionary objectForKey:kFolderType];
    if(aFolderTypeNumber) {
        aFolderType = (EFolderType)[aFolderTypeNumber intValue];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME Folder no folder type: %@", aDictionary);
        aFolderType = kFolderTypeUnknown;
    }
    
    return aFolderType;
}

- (void)loadFromStore:(NSDictionary *)aDictionary
{
    self.folderType = [BaseFolder folderTypeFromStore:aDictionary];
    
    self.uid = [aDictionary objectForKey:kUid];
    if([uid length] == 0) {
        FXDebugLog(kFXLogActiveSync, @"Folder uid invalid");
    }
    
    switch (self.folderType) {
        case kFolderTypeInbox:
            self.displayName  = FXLLocalizedStringFromTable(@"INBOX", @"BaseFolder", @"Displayed name for default Inbox folder"); break;
        case kFolderTypeDrafts:
            self.displayName  = FXLLocalizedStringFromTable(@"DRAFTS", @"BaseFolder", @"Displayed name for default Drafts folder"); break;
        case kFolderTypeDeleted:
            self.displayName  = FXLLocalizedStringFromTable(@"DELETED_ITEMS",  @"BaseFolder", @"Displayed name for default Deleted Items folder"); break;
        case kFolderTypeSent:
            self.displayName  = FXLLocalizedStringFromTable(@"SENT_ITEMS", @"BaseFolder", @"Displayed name for default Sent Items folder"); break;
        case kFolderTypeOutbox:
            self.displayName  = FXLLocalizedStringFromTable(@"OUTBOX", @"BaseFolder", @"Displayed name for default Outbox folder"); break;
        case kFolderTypeTasks:
            self.displayName  = FXLLocalizedStringFromTable(@"TASKS", @"BaseFolder", @"Displayed name for default Tasks folder"); break;
        case kFolderTypeCalendar:
            self.displayName  = FXLLocalizedStringFromTable(@"CALENDAR", @"BaseFolder", @"Displayed name for default Calendar folder"); break;
        case kFolderTypeContacts:
            self.displayName  = FXLLocalizedStringFromTable(@"CONTACTS", @"BaseFolder", @"Displayed name for default Contacts folder"); break;
        case kFolderTypeNotes:
            self.displayName  = FXLLocalizedStringFromTable(@"NOTES", @"BaseFolder", @"Displayed name for default Notes folder"); break;
        case kFolderTypeJournal:
            self.displayName  = FXLLocalizedStringFromTable(@"JOURNAL", @"BaseFolder", @"Displayed name for default Journal folder"); break;
        case kFolderTypeRecipientInfo:
            self.displayName = FXLLocalizedStringFromTable(@"RECIPIENT_INFORMATION_CACHE", @"BaseFolder", @"Displayed name for default Recipient Information Cache folder");
        default:
            self.displayName = [aDictionary objectForKey:kFolderDisplayName];
            break;
    }    
    
    if([displayName length] == 0) {
        FXDebugLog(kFXLogFIXME, @"FIXME folder display name invalid");
    }
  
    accountNum  = [self getInt:aDictionary  key:kAccountNum];
                 
    NSNumber* aNumber = [aDictionary objectForKey:kFolderNum];
    if(aNumber) {
        folderNum = [aNumber intValue];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME folder number invalid");
    }
    folderCount = [self getInt:aDictionary key:kFolderCount];

    flags       = [self getUnsignedInt:aDictionary key:kFlags];
    
    // Unread message counting
    //
    [self setIsCounting:FALSE];
    [self setIsCountWanted:TRUE];
}

- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary
{
    [aDictionary setObject:[NSNumber numberWithInt:folderType]  forKey:kFolderType];

    [aDictionary setObject:uid                                  forKey:kUid];
    [aDictionary setObject:displayName                          forKey:kFolderDisplayName];
    
    [aDictionary setObject:[NSNumber numberWithInt:accountNum]  forKey:kAccountNum];
    [aDictionary setObject:[NSNumber numberWithInt:folderNum]   forKey:kFolderNum];
    [aDictionary setObject:[NSNumber numberWithInt:folderCount] forKey:kFolderCount];
    
    [aDictionary setObject:[NSNumber numberWithBool:[self isDeleted]]    forKey:kDeleted];
    
    NSUInteger aFlags = [self flagsToStore];
    [aDictionary setObject:[NSNumber numberWithUnsignedInt:aFlags]  forKey:kFlags];
}

////////////////////////////////////////////////////////////////////////////////
// Hierarchy
////////////////////////////////////////////////////////////////////////////////
#pragma mark Hierarchy

- (void)addChild:(BaseFolder*)aFolder
{
    if(!children) {
        children = [[NSMutableArray alloc] initWithCapacity:2];
    }
    
    if(![children containsObject:aFolder]) {
        [children addObject:aFolder];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Child already exists");
    }
}

- (void)removeChild:(BaseFolder*)aFolder
{
    if([children containsObject:aFolder]) {
        [children removeObject:aFolder];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Child not found to remove");
    }
}

- (BOOL)hasChildren
{
    return children.count > 0;
}

- (NSArray*)children
{
    return [NSArray arrayWithArray:children];
}

////////////////////////////////////////////////////////////////////////////////
// Delegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark Delegate

- (void)addDelegate:(NSObject<FolderDelegate>*)aFolderDelegate
{
    if(!delegates) {
        delegates = [[NSMutableArray alloc] initWithCapacity:2];
    }
    if(![delegates containsObject:aFolderDelegate]) {
        [delegates addObject:aFolderDelegate];
        //FXDebugLog(kFXLogActiveSync, @"Folder %@ addDelegate %@ delegates=%d", self.displayName, aFolderDelegate, delegates.count);
    }else{
        FXDebugLog(kFXLogActiveSync, @"adding folder delegate multiple times");
    }
}

- (void)removeDelegate:(NSObject<FolderDelegate>*)aFolderDelegate
{
    [delegates removeObject:aFolderDelegate];
    //FXDebugLog(kFXLogActiveSync, @"Folder %@ removeDelegate %@ delegates=%d", self.displayName, aFolderDelegate, delegates.count);
}

////////////////////////////////////////////////////////////////////////////////
// Objects
////////////////////////////////////////////////////////////////////////////////
#pragma mark Objects

- (void)addObject:(BaseObject *)anObject
{
    if(!self.objectsAdded) {
        self.objectsAdded = [[NSMutableArray alloc] initWithCapacity:20];
    }
    
    if(![self.objectsAdded containsObject:anObject]) {
        [self.objectsAdded addObject:anObject];
         // Folder owns this object now
    }else{
        FXDebugLog(kFXLogActiveSync, @"Object already in folder");
    }
}

- (void)addObjects:(NSArray *)anObjects
{
    if(!self.objectsAdded) {
        self.objectsAdded = [[NSMutableArray alloc] initWithCapacity:anObjects.count];
    }
    
    [self.objectsAdded addObjectsFromArray:anObjects];
}

- (void)changedObject:(BaseObject *)anObject
{
    if(!self.objectsChanged) {
        self.objectsChanged = [[NSMutableArray alloc] initWithCapacity:20];
    }
    
    if(![self.objectsChanged containsObject:anObject]) {
        [self.objectsChanged addObject:anObject];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Object changed already in folder");
    }
}

////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

// Required Override

- (DBaseEngine*)databaseEngine
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder databaseEngine override");
    return nil;
}

- (void)commit
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder commit override");
}

- (void)commitObjects
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder commitObjects");
}

- (void)commitChanges:(NSArray*)anObjects
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder commitChanges");
}

- (void)setup
{
}

- (void)foldersLoaded
{
}

- (void)changeObject:(BaseObject*)aBaseObject
          changeType:(EChange)aChangeType
        updateServer:(BOOL)updateServer
      updateDatabase:(BOOL)updateDatabase
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder changeObject");
}

- (void)deleteUid:(NSString*)aUid updateServer:(BOOL)updateServer
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder deleteUid");
}

- (void)deleteDatabaseForFolder;
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder wipeDatabase");
}

- (void)syncOfflineChanges
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseFolder syncOfflineChanges");
}

#if 0
+ (void)tableCheck
{
    NSString* aCreateString = @"CREATE TABLE IF NOT EXISTS folder "
                                    "(folderNum INTEGER PRIMARY KEY, "
                                    "accountNum INTEGER, "
                                    "uid VARCHAR(16), "
                                    "folderType INTEGER, "
                                    "folderCount INTEGER, "
                                    "syncKey VARCHAR(16), "
                                    "flags INTEGER, "
                                    "displayName VARCHAR(128), "
                                    "json TEXT)";
    char* anErrorMsg;
    int aResult = sqlite3_exec([[AccountDBAccessor sharedManager] database], [aCreateString UTF8String], NULL, NULL, &anErrorMsg);
	if (aResult != SQLITE_OK) {
		FXDebugLog(kFXLogActiveSync, @"CREATE TABLE account failed: %d %s'", aResult, anErrorMsg);
	}
}
#endif

////////////////////////////////////////////////////////////////////////////////
// Flags Stored
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Stored

- (NSUInteger)flagsToStore
{
    return flags & kFlagsStoredMask;
}

- (BOOL)isDeleted
{
	return (flags & kIsDeleted) != 0;
}

- (void)setIsDeleted:(BOOL)state
{
	if(state)	flags |= kIsDeleted;
	else		flags &= ~kIsDeleted;
}

- (BOOL)isVisible
{
	return (flags & kIsVisible) != 0;
}

- (void)setIsVisible:(BOOL)state
{
	if(state)	flags |= kIsVisible;
	else		flags &= ~kIsVisible;
}

- (BOOL)isSyncable
{
	return (flags & kIsSyncable) != 0;
}

- (void)setIsSyncable:(BOOL)state
{
	if(state)	flags |= kIsSyncable;
	else		flags &= ~kIsSyncable;
}

- (BOOL)isInitialSynced
{
	return (flags & kIsInitialSynced) != 0;
}

- (void)setIsInitialSynced:(BOOL)state
{
	if(state)	flags |= kIsInitialSynced;
	else		flags &= ~kIsInitialSynced;
}

- (BOOL)isSyncWanted
{
	return (flags & kIsSyncWanted) != 0;
}

- (void)setIsSyncWanted:(BOOL)state
{
	if(state)	flags |= kIsSyncWanted;
	else		flags &= ~kIsSyncWanted;
}

- (BOOL)hasOfflineChanges
{
	return (flags & kHasOfflineChanges) != 0;
}

- (void)setHasOfflineChanges:(BOOL)state
{
	if(state)	flags |= kHasOfflineChanges;
	else		flags &= ~kHasOfflineChanges;
}

////////////////////////////////////////////////////////////////////////////////
// Flags Transient
////////////////////////////////////////////////////////////////////////////////
#pragma mark Flags Transient

- (BOOL)isPinging
{
	return (flags & kIsPinging) != 0;
}

- (void)setIsPinging:(BOOL)state
{
	if(state)	flags |= kIsPinging;
	else		flags &= ~kIsPinging;
}

- (BOOL)isMoreAvailable
{
	return (flags & kIsMoreAvailable) != 0;
}

- (void)setIsMoreAvailable:(BOOL)state
{
	if(state)	flags |= kIsMoreAvailable;
	else		flags &= ~kIsMoreAvailable;
}

- (BOOL)isInitialSync
{
	return (flags & kIsInitialSync) != 0;
}

- (void)setIsInitialSync:(BOOL)state
{
	if(state)	flags |= kIsInitialSync;
	else		flags &= ~kIsInitialSync;
}

- (BOOL)isSyncing
{
	return (flags & kIsSyncing) != 0;
}

- (void)setIsSyncing:(BOOL)state
{
	if(state)	flags |= kIsSyncing;
	else		flags &= ~kIsSyncing;
}

- (BOOL)isResyncing
{
	return (flags & kIsResyncing) != 0;
}

- (void)setIsResyncing:(BOOL)state
{
	if(state)	flags |= kIsResyncing;
	else		flags &= ~kIsResyncing;
}


- (BOOL)isInitialSyncInterim
{
	return (flags & kIsInitialSyncInterim) != 0;
}

- (void)setIsInitialSyncInterim:(BOOL)state
{
	if(state)	flags |= kIsInitialSyncInterim;
	else		flags &= ~kIsInitialSyncInterim;
}

- (BOOL)isCountWanted
{
	return (flags & kIsCountWanted) != 0;
}

- (void)setIsCountWanted:(BOOL)state
{
	if (state) {
        if((self.isInitialSynced || !self.isSyncable) && !self.isDeleted) {
            if (self.isCounting) return;
            if (self.isCountWanted) return;
            flags |= kIsCountWanted;

            [self delayedUnreadCount];
        }
	} else {
		flags &= ~kIsCountWanted;
	}
}

- (BOOL)isCounting
{
	return (flags & kIsCounting) != 0;
}

- (void)setIsCounting:(BOOL)state
{
	if(state)	flags |= kIsCounting;
	else		flags &= ~kIsCounting;
}

- (BOOL)isSyncingOfflineChanges
{
	return (flags & kIsSyncingOfflineChanges) != 0;
}

- (void)setIsSyncingOfflineChanges:(BOOL)state
{
	if(state)	flags |= kIsSyncingOfflineChanges;
	else		flags &= ~kIsSyncingOfflineChanges;
}

////////////////////////////////////////////////////////////////////////////////
// Unread Count
////////////////////////////////////////////////////////////////////////////////
#pragma mark Unread Count

- (void) delayedUnreadCount
{
	NSInvocationOperation *theOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(performUnreadCount:) object:nil];
    NSOperationQueue* anOperationQueue = [[EmailProcessor getSingleton] operationQueue];
    [anOperationQueue addOperation:theOperation];
}

- (void) performUnreadCount:(id)inObject
{
	[self setIsCounting:TRUE];
	[self setIsCountWanted:FALSE];

	NSString *dbPath = [[LoadEmailDBAccessor sharedManager] databaseFilepath];
	FMDatabaseQueue *dbQueue = [FXLSafeZone secureDatabaseQueueWithPath:dbPath];

    NSString *queryString;
    if(self.folderType == kFolderTypeDrafts || self.folderType == kFolderTypeLocalDrafts) {
        queryString = [NSString stringWithFormat:@"select count(*) from email where folder_num = %d;", self.folderNum];
    }else{
        queryString = [NSString stringWithFormat:@"select count(*) from email where folder_num = %d and ((flags & %d) = 0);", self.folderNum, [ASEmail isReadFlag]];
    }

		[dbQueue inDatabase:^(FMDatabase *inDB) {

			[[NSNotificationCenter defaultCenter] postNotificationName:kFolderCountingNotification
																object:self
															  userInfo:nil];
			FXDebugLog(kFXLogASFolder, @"performUnreadCount (%@): Querying unread started (%d)", self.displayName, unreadCount);
			[inDB open];

			FMResultSet *theResults = [inDB executeQuery:queryString];
			if ([theResults next]) {
				unreadCount = [theResults intForColumnIndex:0];
			}

			[inDB close];
			[self setIsCountWanted:FALSE];
			[self setIsCounting:FALSE];
			FXDebugLog(kFXLogASFolder, @"performUnreadCount (%@): Querying unread finished (%d)", self.displayName, unreadCount);

			[[NSNotificationCenter defaultCenter] postNotificationName:kFolderCountedNotification
																object:self
															  userInfo:nil];
		}
	];
	FMDBRelease(dbQueue);
}

- (int) unread
{
	return unreadCount;
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
    NSMutableString* string = [NSMutableString stringWithString:[super description]];

    [string appendString:@"\n{\n"];

    [self writeString:string		tag:kFolderType     value:[BaseFolder stringForFolderType:self.folderType]];
    
    [self writeString:string		tag:kUid            value:uid];
    [self writeString:string		tag:kDisplayName    value:displayName];

    [self writeInt:string           tag:kAccountNum     value:accountNum];
    [self writeInt:string           tag:kFolderNum      value:folderNum];
    [self writeInt:string           tag:kFolderCount    value:folderCount];
    
    // Arrays
    NSUInteger aCount = [children count];
    if(aCount > 0) {
        [self writeInt:string           tag:@"children"     value:aCount];
    }
    
    aCount = [delegates count];
    if(aCount > 0) {
        [self writeInt:string           tag:@"delegates"    value:aCount];
    }
    
    aCount = [dictionary count];
    if(aCount > 0) {
        [self writeInt:string           tag:@"objects"      value:aCount];
    }
    
    aCount = [self.objectsAdded count];
    if(aCount > 0) {
        [self writeInt:string           tag:@"objectsAdded" value:aCount];
    }
    
    aCount = [self.objectsChanged count];
    if(aCount > 0) {
        [self writeInt:string           tag:@"objectsChanged" value:aCount];
    }
    
    // Flags Stored
    if(self.isDeleted)                  [self writeBoolean:string   tag:@"isDeleted"            value:YES];
    if(self.isVisible)                  [self writeBoolean:string   tag:@"isVisible"            value:YES];
    if(self.isSyncable)                 [self writeBoolean:string   tag:@"isSyncable"           value:YES];
    if(self.isInitialSynced)            [self writeBoolean:string   tag:@"isInitialSynced"      value:YES];
    if(self.isSyncWanted)               [self writeBoolean:string   tag:@"isSyncWanted"         value:YES];
    if(self.hasOfflineChanges)          [self writeBoolean:string   tag:@"hasOfflineChanges"    value:YES];
    
    // Flags transient
    if(self.isPinging)                  [self writeBoolean:string   tag:@"isPinging"            value:YES];
    if(self.isMoreAvailable)            [self writeBoolean:string   tag:@"isMoreAvailable"      value:YES];
    if(self.isInitialSync)              [self writeBoolean:string   tag:@"isInitialSync"        value:YES];
    if(self.isSyncing)                  [self writeBoolean:string   tag:@"isSyncing"            value:YES];
    if(self.isResyncing)                [self writeBoolean:string   tag:@"isResyncing"          value:YES];
    if(self.isInitialSyncInterim)       [self writeBoolean:string   tag:@"isInitialSyncInterim" value:YES];
    if(self.isCountWanted)              [self writeBoolean:string   tag:@"isCountWanted"        value:YES];
    if(self.isCounting)                 [self writeBoolean:string	tag:@"isCounting"           value:YES];
    if(self.isSyncingOfflineChanges)    [self writeBoolean:string	tag:@"isSyncingOfflineChanges"  value:YES];
    
    if(dirty) {
        [self writeBoolean:string       tag:kDirty          value:dirty];
    }
    
    return string;
}

@end
