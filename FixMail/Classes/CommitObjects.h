/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "BaseObject.h"

@class BaseFolder;

@interface CommitObjects : BaseObject {
    BaseFolder*     folder;
    NSArray*        objectsAdded;
    NSArray*        objectsChanged;
    NSArray*        objectsDeleted;
    
    SEL             action;
    NSObject*       target;
}

@property (strong,nonatomic) BaseFolder*    folder;
@property (strong,nonatomic) NSArray*       objectsAdded;
@property (strong,nonatomic) NSArray*       objectsChanged;
@property (strong,nonatomic) NSArray*       objectsDeleted;

@property (nonatomic) SEL                   action;
@property (strong,nonatomic) NSObject*      target;


// Construct/Destruct
- (id)initWithFolder:(BaseFolder*)aFolder;

@end
