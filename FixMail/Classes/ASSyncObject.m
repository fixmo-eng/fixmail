/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

#import "ASSyncObject.h"
#import "ASAccount.h"
#import "ASFolder.h"
#import "ASSync.h"
#import "WBXMLRequest.h"

@implementation ASSyncObject

@synthesize syncRequest;

//static int sQueued = 0;

- (void)queue
{
    // When a sync request enters the queue we need to remove the ASPing on this account
    // Having a sync and ping in flight appears to break various ActiveSync servers
    // Domino will produce an HTTP conflict error
    // GMail will go in to an infinite sync/ping loop when you sync read state on emails
    //
    ASAccount* anAccount = (ASAccount*)self.syncRequest.folder.account;
    if([anAccount hasPing]) {
        [anAccount removePing];
    }
    
    NSInvocationOperation* anOp = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(send) object:nil];
    NSOperationQueue* anOperationQueue = [WBXMLRequest operationQueue];
    [anOperationQueue addOperation:anOp];
    
    // Debug
    //sQueued++;
    //FXDebugLog(kFXLogActiveSync, @"ASSyncObject queued    : %d operationCount: %d", sQueued, anOperationQueue.operationCount);
}

- (void)sendAndWait:(ASSync*)aSyncRequest;
{
    self.syncRequest = aSyncRequest;
    [WBXMLRequest wait];
    
    // Debug
    //sQueued--;
    //NSOperationQueue* anOperationQueue = [WBXMLRequest operationQueue];
    //FXDebugLog(kFXLogActiveSync, @"ASSyncObject completing: %d operationCount: %d", sQueued, anOperationQueue.operationCount);
}

@end
