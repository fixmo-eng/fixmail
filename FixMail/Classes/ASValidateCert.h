/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "WBXMLRequest.h"

typedef enum {
    kValidateCertStatusSuccess                  = 1,
    kValidateCertProtocolError                  = 2,
    kValidateCertSignatureCantBeValidated       = 3,
    kValidateCertUntrustedSource                = 4,
    kValidateCertChainNotCreatedCorrectly       = 5,
    kValidateCertNotValidForSigningEmail        = 6,
    kValidateCertExpiredOrNotYetValid           = 7,
    kValidateCertTimePeriodsInconsistent        = 8,
    kValidateCertIDInChainUsedIncorrectly       = 9,
    kValidateCertInformationMissing             = 10,
    kValidateCertIDInChainUsedIncorrectly2      = 11,
    kValidateCertDoesntMatchEmailAddress        = 12,
    kValidateCertRevoked                        = 13,
    kValidateCertCantContactServerToValidate    = 14,
    kValidateCertRevokedByAuthority             = 15,
    kValidateCertRevocationCantBeDetermined     = 16,
    kValidateCertUnknownServerError             = 17
} EValidateCertStatus;

@interface ASValidateCert : WBXMLRequest {
    
}

@property (nonatomic) EValidateCertStatus       statusCodeAS;

+ (void)sendValidateCert:(ASAccount*)anAccount cert:(NSData*)aCertData;
+ (void)sendValidateCertChain:(ASAccount*)anAccount certStrings:(NSArray*)aCertStrings;

@end