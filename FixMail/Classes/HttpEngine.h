/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import <Foundation/Foundation.h>

@class HttpRequest;

@interface HttpEngine : NSObject {
    NSMutableDictionary*			connections;
    NSObject*                       delegate;
    
	NSRunLoop*						runLoop;
    
    NSThread*                       thread;
	NSCondition*					threadCondition;
	BOOL							threadTerminate;
	BOOL							threadTerminated;
	NSCondition*					threadTerminateCondition;
}

// Factory
+ (id)engine;
+ (NSString*)encodeStringSpaceToPlus:(NSString*)string;

// HTTP Request
- (NSMutableURLRequest*)createURLRequest:(NSString*)aProtocol
                                  method:(NSString*)aMethod 
                                     url:(NSString*)aUrl 
                                 headers:(NSDictionary*)aHeaders
                                  params:(NSDictionary*)aParams 
                                    body:(NSData*)aBody
                         timeoutInterval:(NSTimeInterval)aTimeoutInterval;

- (NSString*)sendRequest:(NSURLRequest*)aUrlRequest httpRequest:(HttpRequest*)anHttpRequest;

- (void)removeConnection:(NSString*)connectionIdentifier;

@end
