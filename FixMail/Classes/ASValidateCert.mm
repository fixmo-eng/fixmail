/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASValidateCert.h"
#import "ASAccount.h"
#import "WBXMLDataGenerator.h"

@implementation ASValidateCert

// Constants
static NSString* kCommand       = @"ValidateCert";
static const ASTag kCommandTag  = VALIDATE_CERT_VALIDATE_CERT;

////////////////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory

+ (void)sendValidateCert:(ASAccount*)anAccount cert:(NSData*)aCertData
{
    ASValidateCert* aValidateCert = [[ASValidateCert alloc] initWithAccount:anAccount];
    NSData* aWbxml = [aValidateCert wbxmlValidateCert:anAccount cert:aCertData];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:aValidateCert];
}

+ (void)sendValidateCertChain:(ASAccount*)anAccount certStrings:(NSArray*)aCertStrings
{
    ASValidateCert* aValidateCert = [[ASValidateCert alloc] initWithAccount:anAccount];
    NSData* aWbxml = [aValidateCert wbxmlValidateCertChain:anAccount certStrings:aCertStrings];
    
    NSMutableURLRequest* aURLRequest = [anAccount createURLRequestWithCommand:kCommand wbxml:aWbxml];
    [anAccount send:aURLRequest httpRequest:aValidateCert];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// WBXML generator
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML generator

- (NSData*)wbxmlValidateCert:(ASAccount*)anAccount cert:(NSData*)aCertData
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(VALIDATE_CERT_CERTIFICATES); {
            NSString* aCertString = [[NSString alloc] initWithData:aCertData encoding:NSUTF8StringEncoding];
            wbxml.keyValue(VALIDATE_CERT_CERTIFICATE, aCertString);
        }wbxml.end();
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

- (NSData*)wbxmlValidateCertChain:(ASAccount*)anAccount certStrings:(NSArray*)aCertStrings
{
	WBXMLDataGenerator wbxml;
    
	wbxml.start(kCommandTag); {
        wbxml.start(VALIDATE_CERT_CERTIFICATE_CHAIN); {
            for(NSString* aCertString in aCertStrings) {
                wbxml.keyValue(VALIDATE_CERT_CERTIFICATE, aCertString);
            }
        }
    }wbxml.end();
    
    return wbxml.encodedData(self, kCommandTag);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser actions

- (void)setStatus:(EValidateCertStatus)aStatus
{
    self.statusCodeAS = aStatus;
    
    switch(aStatus) {
        case kValidateCertStatusSuccess:
            break;
#ifdef DEBUG
        case kValidateCertProtocolError:
            [HttpRequest handleASError:@"Cert Protocol Error"]; break;
        case kValidateCertSignatureCantBeValidated:
            [HttpRequest handleASError:@"Cert Signature Can't Be Validated"]; break;
        case kValidateCertUntrustedSource:
            [HttpRequest handleASError:@"Cert Untrusted Source"]; break;
        case kValidateCertChainNotCreatedCorrectly:
            [HttpRequest handleASError:@"Cert Not Created Correctly"]; break;
        case kValidateCertNotValidForSigningEmail:
            [HttpRequest handleASError:@"Cert Not Valid For Signing Email"]; break;
        case kValidateCertExpiredOrNotYetValid:
            [HttpRequest handleASError:@"Cert Expired Or Not Valid"]; break;
        case kValidateCertTimePeriodsInconsistent:
            [HttpRequest handleASError:@"Cert Time Periods Inconsistent"]; break;
        case kValidateCertIDInChainUsedIncorrectly:
            [HttpRequest handleASError:@"Obtain A New Certificate"]; break;
        case kValidateCertInformationMissing:
            [HttpRequest handleASError:@"Cert Information Missing"]; break;
        case kValidateCertIDInChainUsedIncorrectly2:
            [HttpRequest handleASError:@"Cert Obtain the Correct Certificate"]; break;
        case kValidateCertDoesntMatchEmailAddress:
            [HttpRequest handleASError:@"Cert Doesn't Match Email Address"]; break;
        case kValidateCertRevoked:
            [HttpRequest handleASError:@"Cert Revoked"]; break;
        case kValidateCertCantContactServerToValidate:
            [HttpRequest handleASError:@"Cert Can't Contact Server To Validate"]; break;
        case kValidateCertRevokedByAuthority:
            [HttpRequest handleASError:@"Cert Revoked By Authority"]; break;
        case kValidateCertRevocationCantBeDetermined:
            [HttpRequest handleASError:@"Cert Revocation Status Can't Be Determined"]; break;
        case kValidateCertUnknownServerError:
            [HttpRequest handleASError:@"Cert Unknown Server Error"]; break;
        default:
            [self handleActiveSyncError:(EActiveSyncStatus)aStatus]; break;
#else
		default:
            [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"VALIDATE_CERTIFICATE_ERROR", @"ErrorAlert", @"Error alert message for Validate Cert error"), aStatus]];
            break;
#endif
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Parser
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Parser

- (void)fastParser:(FastWBXMLParser*)aParser
{
    @try {
        ASTag tag;
        while((tag = [aParser nextTag:AS_START_DOCUMENT]) != AS_END_DOCUMENT) {
            switch(tag) {
                case VALIDATE_CERT_STATUS:
                    [self setStatus:(EValidateCertStatus)[aParser getInt]];
                    break;
                case VALIDATE_CERT_CERTIFICATE:
                    break;
                default:
                    [aParser skipTag]; break;
            }
        }
    }@catch (NSException* e) {
        [HttpRequest logException:@"ResolveRecipients parser" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        NSString* aContentType = [aHeaders objectForKey:@"Content-Type"];
        if([aContentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
            
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", kCommand);
        }
    }else{
        [super handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    NSUInteger aLength = [data length];
	if(self.statusCode == kHttpOK && aLength > 0) {
        [self parseWBXMLData:data command:kCommandTag];
    }else{
        FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", kCommand, self.statusCode);
    }
    [super connectionDidFinishLoading:connection];
}

@end