/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseObject.h"
#import "NSObject+SBJSON.h"

@implementation BaseObject

////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (NSString*)asJSON
{
    NSDictionary* aDictionary = [self asDictionary];
    NSString* aJSON = [aDictionary JSONRepresentation];
    return aJSON;
}

- (void)fromJSON:(NSString*)aString
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseObject fromJSON");
}

- (NSDictionary*)asDictionary
{
    FXDebugLog(kFXLogFIXME, @"FIXME BaseObject asDictionary");
    return nil;
}

////////////////////////////////////////////////////////////////////////////////
// Dictionary/JSON Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark Dictionary/JSON Utilities

- (void)addString:(NSString*)aString toDictionary:(NSMutableDictionary*)aDictionary key:(NSString*)aKey
{
    if(!aString) {
        aString = @"";
    }
    [aDictionary setObject:aString forKey:aKey];
}

- (int)intFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    int anInt;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        anInt = [aNumber intValue];
    }else{
        anInt = 0;
    }
    
    return anInt;
}

- (NSUInteger)unsignedIntFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    NSUInteger anUnsignedInt;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        anUnsignedInt = [aNumber unsignedIntValue];
    }else{
        anUnsignedInt = 0;
    }
    
    return anUnsignedInt;
}

- (BOOL)boolFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    BOOL aBool;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        aBool = [aNumber boolValue];
    }else{
        aBool = FALSE;
    }
    
    return aBool;
}

- (double)doubleFromDictionary:(NSDictionary*)aDictionary key:(NSString*)aKey
{
    double aDouble;
    
    NSNumber* aNumber = [aDictionary objectForKey:aKey];
    if(aNumber) {
        aDouble = [aNumber doubleValue];
    }else{
        aDouble = 0.0;
    }
    
    return aDouble;
}


////////////////////////////////////////////////////////////////////////////////
// Description Utilities
////////////////////////////////////////////////////////////////////////////////
#pragma mark  Description Utilities

- (void)writeString:(NSMutableString*)string tag:(NSString*)tag value:(NSString*)value
{
    if(!value) {
        value = @"nil";
    }
	[string appendString:[NSString stringWithFormat:@"\t%@ = \"%@\";\n", tag, value]];
}

- (void)writeStringIndented:(NSMutableString*)string tag:(NSString*)tag value:(NSString*)value
{
    if(!value) {
        value = @"nil";
    }
	[string appendString:[NSString stringWithFormat:@"\t\t%@ = \"%@\";\n", tag, value]];
}

- (void)writeBoolean:(NSMutableString*)string tag:(NSString*)tag value:(BOOL)value
{
	if(value) {
		[string appendString:[NSString stringWithFormat:@"\t%@ = true;\n", tag]];
	}else{
		[string appendString:[NSString stringWithFormat:@"\t%@ = false;\n", tag]];
	}
}

- (void)writeInt:(NSMutableString*)string tag:(NSString*)tag value:(int)value
{
	[string appendString:[NSString stringWithFormat:@"\t%@ = %d;\n", tag, value]];
}

- (void)writeUnsigned:(NSMutableString*)string tag:(NSString*)tag value:(unsigned int)value
{
	[string appendString:[NSString stringWithFormat:@"\t%@ = %u;\n", tag, value]];
}

- (void)writeLongLong:(NSMutableString*)string tag:(NSString*)tag value:(long long)value
{
	[string appendString:[NSString stringWithFormat:@"\t%@ = %lld;\n", tag, value]];
}

- (void)writePointer:(NSMutableString*)string tag:(NSString*)tag pointer:(void*)pointer
{
	[string appendString:[NSString stringWithFormat:@"\t%@ = %p;\n", tag, pointer]];
}

- (void)writeDataAsString:(NSMutableString*)string tag:(NSString*)tag data:(NSData*)data
{
    NSString* aDataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	[string appendString:[NSString stringWithFormat:@"\t%@ = %@;\n", tag, aDataString]];
}

@end
