/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2013 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "ASEmail.h"

typedef enum {
    kFlagStatusCleared  = 0,
    kFlagStatusComplete = 1,
    kFlagStatusActive   = 2
} EFlagStatus;

@interface ASFlag : NSObject

@property (nonatomic,strong) NSString*      flagType;
@property (nonatomic,strong) NSDate*        startDate;
@property (nonatomic,strong) NSDate*        dueDate;

@property (nonatomic) NSInteger             reminderTime;
@property (nonatomic) EFlagStatus           flagStatus;
@property (nonatomic) unsigned int          flags;

@end
