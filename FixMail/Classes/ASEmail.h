/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
//
//  Email.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 1/16/09.
//  Copyright 2010 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "BaseObject.h"
#import "ASMeetingResponse.h"
#import "ASMeetingResponseDelegate.h"

@class ASFlag;
@class ASFolder;
@class ASEmailThread;
@class BaseFolder;
@class EmailAddress;
@class MeetingRequest;

typedef enum {
    kBodyTypeNone       = 0,
    kBodyTypePlainText  = 1,
    kBodyTypeHTML       = 2,
    kBodyTypeRTF        = 3,
    kBodyTypeMIME       = 4
} EBodyType;

typedef enum {
    kBodyFetchNone,
    kBodyFetchPreview,
    kBodyFetchFull
} EBodyFetchType;

typedef enum {
    kImportanceLow      = 0,
    kImportanceNormal   = 1,
    kImportanceHigh     = 2
} EImportance;

typedef enum {

    kMessageClassNone                           = 0,
    
    kMessageClassNote                           = 1,
    kMessageClassNoteSMIME                      = 2,
    kMessageClassNoteReceiptSMIME               = 3,
    kMessageClassNoteSMIMEMultipartSigned       = 4,

    kMessageClassPost                           = 10,

    kMessageClassScheduleMeetingRequest         = 20,
    kMessageClassScheduleMeetingCanceled        = 21,
    kMessageClassScheduleMeetingResponsePos     = 22,
    kMessageClassScheduleMeetingResponseNeg     = 23,
    kMessageClassScheduleMeetingResponseTent    = 24,
    kMessageClassScheduleMeetingUnknown         = 25,
    kMessageClassNotificationMeeting            = 26,
    
    kMessageClassReport                         = 30,
    kMessageClassReportUndeliverable            = 31,
    
    kMessageClassInfoPathForm                   = 40,
    
    kMessageClassOctelVoice                     = 50,
    kMessageClassVoiceNotes                     = 51,
    kMessageClassVoiceSharing                   = 52,
    
    kMessageClassUnknown                        = 127
} EMessageClass;

typedef enum {
    kLastVerbUnknown        = 0,
    kLastVerbReplyToSender  = 1,
    kLastVerbReplyToAll     = 2,
    kLastVerbForward        = 3
} ELastVerbExecuted;

@interface ASEmail : BaseObject  <ASMeetingResponseDelegate>

// Properties
@property (nonatomic,weak) BaseFolder*              folder;
@property (assign) int                              folderNum;

@property (nonatomic,strong) NSString*              uid;
@property (nonatomic,strong) NSDate*                datetime;

@property (nonatomic,strong) NSString*              from;
@property (nonatomic,strong) NSString*              replyTo;

@property (nonatomic,strong) NSString*              subject;

@property (nonatomic,strong) NSString*              toJSON;
@property (nonatomic,strong) NSString*              ccJSON;
@property (nonatomic,strong) NSString*              bccJSON;
@property (nonatomic,strong) NSString*              preview;

@property (nonatomic,strong) NSData*                body;
@property (nonatomic,strong) NSMutableArray*        attachments;
@property (nonatomic,strong) MeetingRequest*        meetingRequest;
@property (nonatomic,strong) ASEmailThread*         emailThread;

@property (nonatomic,strong) ASFlag*                flag;

@property (nonatomic) unsigned int                  estimatedSize;
@property (nonatomic) unsigned int                  flags;

@property (nonatomic,readwrite) EMessageClass       messageClass;
@property (nonatomic,readwrite) EBodyType           bodyType;

@property (nonatomic,readwrite,strong) NSString*    displayTo;

// Static
+ (NSDateFormatter*)dateFormatter;

// Construct/Destruct
- (id)initWithFolder:(BaseFolder*)aFolder;

// Static interface
+ (void)tableCheck;
+ (void)deleteWithUid:(NSString*)aUid;

// SQL Interface
+ (NSString*)sqlFields;
+ (NSString*)sqlFieldsNoBody;
+ (NSString*)uidFromSQL:(void*)aSQLStatement;

- (void)loadFromSQL:(void *)aSQLStatement loadBody:(BOOL)aLoadBody;

// Parser
- (void)flagParser:(FastWBXMLParser*)aParser tag:(ASTag)anEndTag;
- (void)parser:(FastWBXMLParser*)aParser tag:(ASTag)aTag;
- (void)store;

// MIME
- (NSString*)asMIMEString;

// Debug
#ifdef DEBUG
- (void)debugMIMEString:(NSString*)aMIMEString;
//- (void)debugMsgLint:aMIMEString;
- (void)debugMIMEData:(NSData*)aMIMEData;
#endif

// Getters
- (NSString*)toFlat;
- (NSString*)ccFlat;
- (NSString*)bccFlat;

- (NSString*)toAsAddressString;
- (NSString*)ccAsAddressString;
- (NSString*)bccAsAddressString;

- (NSArray*)toAsArrayOfAddressStrings;
- (NSArray*)ccAsArrayOfAddressStrings;
- (NSArray*)bccAsArrayOfAddressStrings;

- (NSArray*)toAsArrayOfEmailAddresses;
- (NSArray*)ccAsArrayOfEmailAddresses;
- (NSArray*)bccAsArrayOfEmailAddresses;

- (EmailAddress*)emailAddressFromString:(NSString*)aNameAndAddress;
- (NSString*)JSONFromAddressesString:(NSString*)anAddressString;

// Meeting Request
- (BOOL)hasMeeting;

// Attachments
- (BOOL)hasAttachments;
- (BOOL)hasAttachmentWithSuffix:(NSString*)aSuffix;
- (NSArray*)attachmentWithSuffix:(NSString*)aSuffix;
- (NSString*)attachmentsAsJSON;

// Setters
- (void)setToFromAddressString:(NSString*)anAddressString;
- (void)setCCFromAddressString:(NSString*)anAddressString;
- (void)setBCCFromAddressString:(NSString*)anAddressString;

- (void)setImportanceHigh;
- (void)setImportanceNormal;
- (void)setImportanceLow;

- (void)loadBody:(NSData*)aData;

// Overrides
- (void)changeReadStatus:(BOOL)isRead;
- (void)meetingUserResponse:(EUserResponse)aUserResponse;

// Flags Stored
- (NSUInteger)flagsToStore;

- (BOOL)isFetched;
- (void)setIsFetched:(BOOL)state;

- (BOOL)isRead;
- (BOOL)isUnread;
- (void)setIsRead:(BOOL)state;
+ (int)isReadFlag;

- (BOOL)isMIME;
- (void)setIsMIME:(BOOL)state;

- (BOOL)isHTML;
- (void)setIsHTML:(BOOL)state;

- (BOOL)isRTF;
- (void)setIsRTF:(BOOL)state;

- (BOOL)isImportant;
- (void)setIsImportant:(BOOL)state;

- (BOOL)isNotImportant;
- (void)setIsNotImportant:(BOOL)state;

- (BOOL)isFlagged;
- (void)setIsFlagged:(BOOL)state;

- (BOOL)hasOfflineChange;
- (void)setHasOfflineChange:(BOOL)state;
+ (int)hasOfflineChangeFlag;

// Flags Transient
- (BOOL)isFetching;
- (void)setIsFetching:(BOOL)state;

- (BOOL)isDeleted;
- (void)setIsDeleted:(BOOL)state;

- (BOOL)isChanged;
- (void)setIsChanged:(BOOL)state;

- (BOOL)isNew;
- (void)setIsNew:(BOOL)state;

- (BOOL)isMarked;
- (void)setIsMarked:(BOOL)state;

@end
