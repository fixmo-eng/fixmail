//
//  DateUtil.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 3/17/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "DateUtil.h"
#define DATE_UTIL_SECS_PER_DAY 86400

@interface DateUtil ()
@property (nonatomic, strong) NSDateFormatter* dateFormatter;
@property (nonatomic, strong) NSDateFormatter* dateFormatterTimeOnlyShort;
@property (nonatomic, strong) NSDateFormatter* dateFormatterDayOfWeekOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterShortDate;
@property (nonatomic, strong) NSDateFormatter* dateFormatterShortDayOfWeekOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterDateOnlyMedium;
@property (nonatomic, strong) NSDateFormatter* dateFormatterMonthAndDayOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterMonthOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterMonthAndYearOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterYearOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterWeekdayAndDateLongStyle;
@property (nonatomic, strong) NSDateFormatter* dateFormatterWeekdayAndDateTimeShortStyle;
@property (nonatomic, strong) NSDateFormatter* dateFormatter24HourSystemHourAndMinuteOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatter12HourSystemHourAndAMOrPMSymbolOnly;
@property (nonatomic, strong) NSDateFormatter* dateFormatterRFC;
@end

@implementation DateUtil

@synthesize today;
@synthesize yesterday;
@synthesize lastWeek;
@synthesize todayComponents;
@synthesize yesterdayComponents;

// Constants
static const NSUInteger kComponents = NSYearCalendarUnit | NSMonthCalendarUnit| NSWeekCalendarUnit | NSDayCalendarUnit;


-(void)refreshData {
	//TODO(gabor): Call this every hour or so to refresh what today, yesterday, etc. mean
	_currentCalendar = [NSCalendar currentCalendar];
	self.today = [NSDate date];
	self.yesterday = [today dateByAddingTimeInterval:-DATE_UTIL_SECS_PER_DAY];
	self.lastWeek = [today dateByAddingTimeInterval:-6*DATE_UTIL_SECS_PER_DAY];
	self.todayComponents = [_currentCalendar components:kComponents fromDate:today];
	self.yesterdayComponents = [_currentCalendar components:kComponents fromDate:yesterday];

	// since this is used in several places, we alloc it here
	self.dateFormatter = [[NSDateFormatter alloc] init];
	[self.dateFormatter setLocale:[NSLocale autoupdatingCurrentLocale]];

	// all others can get re-allocated as needed
	self.dateFormatterTimeOnlyShort = nil;
	self.dateFormatterDayOfWeekOnly = nil;
	self.dateFormatterShortDayOfWeekOnly = nil;
	self.dateFormatterDateOnlyMedium = nil;
	self.dateFormatterMonthAndDayOnly = nil;
	self.dateFormatterMonthOnly = nil;
	self.dateFormatterMonthAndYearOnly = nil;
	self.dateFormatterYearOnly = nil;
	self.dateFormatterWeekdayAndDateLongStyle = nil;
	self.dateFormatterWeekdayAndDateTimeShortStyle = nil;
	self.dateFormatter24HourSystemHourAndMinuteOnly = nil;
	self.dateFormatter12HourSystemHourAndAMOrPMSymbolOnly = nil;
	self.dateFormatterRFC = nil;
}

-(id)init {
	if (self = [super init]) {	
		[self refreshData];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationLocaleDidChange:) name:NSCurrentLocaleDidChangeNotification object:nil];
	}
	return self;
}

-(NSDateFormatter *)dateFormatter24HourSystemHourAndMinuteOnly
{
	if (!_dateFormatter24HourSystemHourAndMinuteOnly)
	{
		_dateFormatter24HourSystemHourAndMinuteOnly = [[NSDateFormatter alloc] init];
		[_dateFormatter24HourSystemHourAndMinuteOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
		[_dateFormatter24HourSystemHourAndMinuteOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"HH:mm" options:0 locale:[NSLocale currentLocale]]];
	}
	return _dateFormatter24HourSystemHourAndMinuteOnly;
}

+(id)getSingleton {

    static DateUtil *singleton = nil;
	static dispatch_once_t once;
	dispatch_once(&once, ^{
        singleton = [[self alloc] init];
    });
	return singleton;
}

-(void) notificationLocaleDidChange:(NSNotification*)notification
{
    [self refreshData];
}

-(NSDate *)dateFromString24HourSystemHourAndMinuteOnly:(NSString *)time
{
	NSDate* retVal = nil;
    @synchronized(self)
    {
        retVal = [self.dateFormatter24HourSystemHourAndMinuteOnly dateFromString:time];
    }
    return retVal;
}

-(NSString*) stringFromDateTimeOnlyShort:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterTimeOnlyShort)
		{
			_dateFormatterTimeOnlyShort = [[NSDateFormatter alloc] init];
			[_dateFormatterTimeOnlyShort setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterTimeOnlyShort setDateStyle:NSDateFormatterNoStyle];
			[_dateFormatterTimeOnlyShort setTimeStyle:NSDateFormatterShortStyle];
		}
        retVal = [self.dateFormatterTimeOnlyShort stringFromDate:date];
    }
    return retVal;
}

-(NSString*) stringFromDate24HourSystemHourAndMinuteOnly:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
        retVal = [self.dateFormatter24HourSystemHourAndMinuteOnly stringFromDate:date];
    }
    return retVal;
}

-(NSString*) stringFromDate12HourSystemHourAndAMOrPMSymbolOnly:(NSDate*)date;
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatter12HourSystemHourAndAMOrPMSymbolOnly)
		{
			_dateFormatter12HourSystemHourAndAMOrPMSymbolOnly = [[NSDateFormatter alloc] init];
			[_dateFormatter12HourSystemHourAndAMOrPMSymbolOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatter12HourSystemHourAndAMOrPMSymbolOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"h a" options:0 locale:[NSLocale currentLocale]]];
		}
        retVal = [self.dateFormatter12HourSystemHourAndAMOrPMSymbolOnly stringFromDate:date];
    }
    return retVal;
}

-(NSString*) stringFromDateShortDayOfWeekOnly:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterShortDayOfWeekOnly)
		{
			_dateFormatterShortDayOfWeekOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterShortDayOfWeekOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterShortDayOfWeekOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEE" options:0 locale:[NSLocale currentLocale]]];
		}
        retVal = [self.dateFormatterShortDayOfWeekOnly stringFromDate:date];
    }
    return retVal;
    
}

-(NSString*) stringFromDateDayOfWeekOnly:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterDayOfWeekOnly)
		{
			_dateFormatterDayOfWeekOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterDayOfWeekOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterDayOfWeekOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEEE" options:0 locale:[NSLocale currentLocale]]];
		}
        retVal = [self.dateFormatterDayOfWeekOnly stringFromDate:date];
    }
    return retVal;
}

-(NSString*) stringFromDateOnlyMedium:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterDateOnlyMedium)
		{
			_dateFormatterDateOnlyMedium = [[NSDateFormatter alloc] init];
			[_dateFormatterDateOnlyMedium setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterDateOnlyMedium setDateStyle:NSDateFormatterMediumStyle];
			[_dateFormatterDateOnlyMedium setTimeStyle:NSDateFormatterNoStyle];
		}
        retVal = [self.dateFormatterDateOnlyMedium stringFromDate:date];
    }
    return retVal;
}

- (NSString*) stringFromDateMonthAndDayOnly:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterMonthAndDayOnly)
		{
			_dateFormatterMonthAndDayOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterMonthAndDayOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterMonthAndDayOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMdd" options:0 locale:[NSLocale currentLocale]]];
		}
		retVal = [self.dateFormatterMonthAndDayOnly stringFromDate:date];
	}
    return retVal;
}

- (NSString*) stringFromDateMonthOnly:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterMonthOnly)
		{
			_dateFormatterMonthOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterMonthOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterMonthOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMMM" options:0 locale:[NSLocale currentLocale]]];
		}
		retVal = [self.dateFormatterMonthOnly stringFromDate:date];
	}
    return retVal;
}

- (NSString*) stringFromDateYearOnly:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterYearOnly)
		{
			_dateFormatterYearOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterYearOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterYearOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"yyyy" options:0 locale:[NSLocale currentLocale]]];
		}
		retVal = [self.dateFormatterYearOnly stringFromDate:date];
	}
    return retVal;
}

- (NSString*) stringFromDateMonthAndYear:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterMonthAndYearOnly)
		{
			_dateFormatterMonthAndYearOnly = [[NSDateFormatter alloc] init];
			[_dateFormatterMonthAndYearOnly setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterMonthAndYearOnly setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"MMMM yyyy" options:0 locale:[NSLocale currentLocale]]];
		}
		retVal = [self.dateFormatterMonthAndYearOnly stringFromDate:date];
	}
    return retVal;
}

- (NSString *) stringFromDateWeekdayAndDateLongStyle:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterWeekdayAndDateLongStyle)
		{
			_dateFormatterWeekdayAndDateLongStyle = [[NSDateFormatter alloc] init];
			[_dateFormatterWeekdayAndDateLongStyle setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterWeekdayAndDateLongStyle setDateFormat:[NSDateFormatter dateFormatFromTemplate:@"EEEEMMMMddyyyy" options:0 locale:[NSLocale currentLocale]]];
		}
		retVal = [self.dateFormatterWeekdayAndDateLongStyle stringFromDate:date];
	}
    return retVal;
}

- (NSString *) stringFromDateWeekdayAndDateTimeShortStyle:(NSDate *)date
{
	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterWeekdayAndDateTimeShortStyle)
		{
			_dateFormatterWeekdayAndDateTimeShortStyle = [[NSDateFormatter alloc] init];
			[_dateFormatterWeekdayAndDateTimeShortStyle setLocale:[NSLocale autoupdatingCurrentLocale]];
			[_dateFormatterWeekdayAndDateTimeShortStyle setDateStyle:NSDateFormatterShortStyle];
			[_dateFormatterWeekdayAndDateTimeShortStyle setTimeStyle:NSDateFormatterShortStyle];
		}
		retVal = [self.dateFormatterWeekdayAndDateTimeShortStyle stringFromDate:date];
	}
    return retVal;
}

-(NSString*) rfcStringWithDate:(NSDate *)date
{
	// default to now if no object
	NSDate *theDate = date;
	if (theDate == nil) theDate = [NSDate date];

	NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterRFC)
		{
			_dateFormatterRFC = [[NSDateFormatter alloc] init];
			_dateFormatterRFC.locale = [NSLocale autoupdatingCurrentLocale];
			_dateFormatterRFC.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss Z";
		}
		retVal = [self.dateFormatterRFC stringFromDate:theDate];
	}
    return retVal;
}

-(NSArray *) arrayOfStringsShortWeekdaySymbols
{
	NSArray* retVal;
    @synchronized(self)
    {
		retVal = [self.dateFormatter shortWeekdaySymbols];
	}
	return retVal;
}

-(BOOL)is24HourClock
{
	BOOL retVal = NO;
    @synchronized(self)
    {
		NSString *dateString = [self stringFromDateTimeOnlyShort:[NSDate date]];
		NSRange amRange = [dateString rangeOfString:[self.dateFormatterTimeOnlyShort AMSymbol]];
		NSRange pmRange = [dateString rangeOfString:[self.dateFormatterTimeOnlyShort PMSymbol]];
		retVal = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
		
	}
	return retVal;
}

-(NSString *)amSymbol
{
	return [self.dateFormatter AMSymbol];
}

-(NSString *)pmSymbol
{
	return [self.dateFormatter PMSymbol];
}

/*
 Returns with the following format: Apr 18, 2013, 10:00:00 AM
 */
 
-(NSString*)shortDate:(NSDate*)date
{
    NSString* retVal = @"";
    @synchronized(self)
    {
		if (!_dateFormatterShortDate)
		{
			_dateFormatterShortDate = [[NSDateFormatter alloc] init];
			[_dateFormatterShortDate setLocale:[NSLocale autoupdatingCurrentLocale]];
            _dateFormatterShortDate.timeStyle = NSDateFormatterMediumStyle;
            _dateFormatterShortDate.dateStyle = NSDateFormatterMediumStyle;
		}
		retVal = [self.dateFormatterShortDate stringFromDate:date];
	}
    return retVal;

}

-(NSString*)humanDate:(NSDate*)date {
	
	NSCalendar *gregorian = self.currentCalendar;
	
	NSDateComponents *dateComponents = [gregorian components:kComponents fromDate:date];
	
	if([dateComponents day]     == [todayComponents day] && 
	   [dateComponents month]   == [todayComponents month] && 
	   [dateComponents year]    == [todayComponents year]) {
		
		return [self stringFromDateTimeOnlyShort:date];
	}
	if([dateComponents day]     == [yesterdayComponents day] && 
	   [dateComponents month]   == [yesterdayComponents month] && 
	   [dateComponents year]    == [yesterdayComponents year]) {
		return FXLLocalizedString(@"yesterday", nil);
    }
    
    if([dateComponents week]    == [todayComponents week] &&
	   [dateComponents month]   == [todayComponents month] &&
	   [dateComponents year]    == [todayComponents year]) {
        return [self stringFromDateDayOfWeekOnly:date];
    }
	
	return [self stringFromDateOnlyMedium:date];
}

+(NSDate *)datetimeInLocal:(NSDate *)utcDate
{
	NSTimeZone *utc = [NSTimeZone timeZoneWithName:@"UTC"];
	
	NSTimeZone *local = [NSTimeZone localTimeZone];
	
	NSInteger sourceSeconds = [utc secondsFromGMTForDate:utcDate];
	NSInteger destinationSeconds = [local secondsFromGMTForDate:utcDate];
	
	NSTimeInterval interval =  destinationSeconds - sourceSeconds;
	NSDate *res = [[NSDate alloc] initWithTimeInterval:interval sinceDate:utcDate];
	return res;
	
}

-(NSDate *) dateWithDatePart:(NSDate *)aDate andTimePart:(NSDate *)aTime
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDateComponents *dateComponents = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:aDate];
	NSDateComponents *timeComponents = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:aTime];
	
	[dateComponents setHour:[timeComponents hour]];
	[dateComponents setMinute:[timeComponents minute]];
	
	return [calendar dateFromComponents:dateComponents];
}

+ (NSDate*)dateFromString:(NSString*)date withDateFormat:(NSString*)dateFormat
{
    if(!date) {
        return nil;
    }
    
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter dateFromString:date];
}

+ (NSString*)stringFromDate:(NSDate*)date withDateFormat:(NSString*)dateFormat
{
    NSLocale *gbLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"];
    NSString *gbFormatString = [NSDateFormatter dateFormatFromTemplate:dateFormat options:0 locale:gbLocale];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:gbFormatString];
    return [dateFormatter stringFromDate:date];
}

@end
