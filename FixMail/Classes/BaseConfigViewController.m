/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseConfigViewController.h"

@implementation BaseConfigViewController

// Properties
@synthesize account;
@synthesize firstSetup;
@synthesize accountNum;
@synthesize newAccount;
@synthesize accountSubType;

// UI Properties
@synthesize scrollView;
@synthesize imageView;
@synthesize nameField;
@synthesize userNameField;
@synthesize passwordField;
@synthesize passwordLabel;
@synthesize PCCField;
@synthesize hostnameField;
@synthesize hostStatusImageView;
@synthesize domainField;
@synthesize portField;
@synthesize activityIndicator;
@synthesize selectFoldersButton;


////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
