/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/*
 *  FIXME K9 derived work, need appropriate copyrights here
 *  Tags and strings are derived from K9 Android email client under an Apache license
 */
#import "FastWBXMLParser.h"
#import "AS.h"
#import "FastScanner.h"
#import "WBXMLRequest.h"
#import "TraceEngine.h"
#ifdef DEBUG
//#include <stdio.h>
#endif
#ifdef USE_SYBYX
#include "WBXMLParser.h"
#endif

@implementation FastWBXMLParser

// Properties
@synthesize debug;
@synthesize dataReturn;
@synthesize stringReturn;

// Globals
extern BOOL gIsFixTrace;

// Constants
static const int NOT_ENDED        = 0x80000000;
static const int NOT_FETCHED      = 0x80000000;

static const int kTagOffset       = 5;


// WBXML Global tokens

enum EASGlobalToken {
    EOF_BYTE       = -1,
    SWITCH_PAGE    = 0,
    END            = 1,
    ENTITY         = 2,
    STR_I          = 3,
    LITERAL        = 4,
    EXT_I_0        = 0x40,
    EXT_I_1        = 0x41,
    EXT_I_2        = 0x42,
    PI             = 0x43,
    LITERAL_C      = 0x44,
    EXT_T_0        = 0x80,
    EXT_T_1        = 0x81,
    EXT_T_2        = 0x82,
    STR_T          = 0x83,
    LITERAL_A      = 0x84,
    EXT_0          = 0xc0,
    EXT_1          = 0xc1,
    EXT_2          = 0xc2,
    OPAQUE         = 0xc3,
    LITERAL_AC     = 0xc4
};

enum EGetValueType {
    kGetValueAsString   = 0,
    kGetValueAsInt      = 1,
    kGetValueAsData     = 2
};

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init
{
    if (self = [super init]) {
        pageTable = [AS pageTable];
        tagTable = pageTable[0];
    }
    return self;
}

- (void)dealloc
{
    if(d) {
        FastScanner* aFastScanner = (FastScanner*)d;
        d = NULL;
        delete aFastScanner;
    }

}

////////////////////////////////////////////////////////////////////////////////////////////
// Parser Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Parser Interface

-(void)parse:(NSData*)aData command:(ASTag)aCommand request:(WBXMLRequest*)request
{
#ifdef USE_SYBYX
    [request wbxmlParser:data command:aCommand debug:[WBXMLRequest traceWBXMLResponses]];
#endif
    
    FastScanner* aFastScanner = new FastScanner;
    aFastScanner->setData([aData length], (wbyte*)[aData bytes]);
    d = (void*)aFastScanner;
    
    //[self dump];
    
    /* wbyte aVersion              = */ aFastScanner->getByte();
    /* int   aDocumentPublicID     = */ aFastScanner->getInt();
    /* int   aCharSet              = */ aFastScanner->getInt();
    /* int   aStringTableLength    = */ aFastScanner->getInt();
    
    if ([self nextTag:AS_START_DOCUMENT] == aCommand) {
        [request fastParser:self];
    }else{
        FXDebugLog(kFXLogWBXML, @"Invalid parse: 0x%x", aCommand);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Tag Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Tag Interface

- (ASTag)nextTag:(ASTag)endingTag
{
    int thisTag = endingTag & AS_PAGE_MASK;
    while ([self _getNext:kGetValueAsString] != kASTypeDone) {
        if (tokenType == kASTypeStart) {
            tag = (ASTag)(page | startTag);
            return tag;            
        } else if (tokenType == kASTypeEnd && startTag == thisTag) {
            return AS_END;
        }
    }
    return AS_END_DOCUMENT;
}

- (void)skipTag 
{
    int thisTag = startTag;
    // Just loop until we hit the end of the current tag
    while([self _getNext:kGetValueAsString] != kASTypeDone) {
        if (tokenType == kASTypeEnd && startTag == thisTag) {
            return;
        }
    }
    FXDebugLog(kFXLogFIXME, @"FIXME skipTag premature end");
}

////////////////////////////////////////////////////////////////////////////////////////////
// Getters
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Getters

- (NSString*)getString
{
    [self _getNext:kGetValueAsString];
    
    // This means there was no value given, just <Foo/>; we'll return empty string for now
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getString failed");
        return @"";
    }
    
    // Save the value
    self.stringReturn = text;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getString no end");
    }
    endTag = startTag;
    
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s%@\n", [self indent], self.stringReturn);
    }
#endif
    
    return self.stringReturn;
}

- (NSString*)getStringTraceable
{
    [self _getNext:kGetValueAsString];
    
    // This means there was no value given, just <Foo/>; we'll return empty string for now
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getString failed");
        return @"";
    }
    
    // Save the value
    self.stringReturn = text;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getString no end");
    }
    endTag = startTag;
    
    if(gIsFixTrace) {
        TraceLog(@"%s%@\n", [self indent], self.stringReturn);
    }else{
#ifdef DEBUG
        if(debug) {
            FXDebugLog(kFXLogWBXML, @"%s%@\n", [self indent], self.stringReturn);
        }
#endif
    }
    
    return self.stringReturn;
}

- (NSData*)getData
{
    [self _getNext:kGetValueAsData];
    
    // This means there was no value given, just <Foo/>; we'll return empty string for now
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getData failed");
        return NULL;
    }
    
    // Save the value
    self.dataReturn = data;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getData no end");
    }
    endTag = startTag;
    
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s <DATA BLOB>\n", [self indent]);
    }
#endif
    
    return self.dataReturn;
}

- (int)getInt 
{
    int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt failed");
        return 0;
    }
    
    // Save the value
    aValue = num;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt no end");
    }
    endTag = startTag;
   
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s%d\n", [self indent], aValue);
    }
#endif
    return aValue;
}

- (int)getIntTraceable
{
    int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt failed");
        return 0;
    }
    
    // Save the value
    aValue = num;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt no end");
    }
    endTag = startTag;
    
    if(gIsFixTrace) {
        TraceLog(@"%s%d\n", [self indent], aValue);
    }else{
#ifdef DEBUG
        if(debug) {
            FXDebugLog(kFXLogWBXML, @"%s%d\n", [self indent], aValue);
        }
#endif
    }
    return aValue;
}

- (unsigned int)getUnsignedInt 
{
    unsigned int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt failed");
        return aValue;
    }
    
    // FIXME - Not really an unsigned int, may overflow
   aValue = (unsigned int)num;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt no end");
    }
    endTag = startTag;
    
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s%u\n", [self indent], aValue);
    }
#endif
    return aValue;
}

- (unsigned int)getUnsignedIntTraceable
{
    unsigned int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt failed");
        return aValue;
    }
    
    // FIXME - Not really an unsigned int, may overflow
    aValue = (unsigned int)num;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt no end");
    }
    endTag = startTag;
    
    if(gIsFixTrace) {
        TraceLog(@"%s%u\n", [self indent], aValue);
    }else{
#ifdef DEBUG
        if(debug) {
            FXDebugLog(kFXLogWBXML, @"%s%u\n", [self indent], aValue);
        }
#endif
    }
    return aValue;
}

- (BOOL)getBool 
{
    return [self getInt] != 0;
}

- (BOOL)getBoolTraceable
{
    return [self getIntTraceable] != 0;
}

- (int)getIntIsEmpty:(BOOL*)anIsEmpty 
{
    int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        *anIsEmpty = TRUE;
        return aValue;
    }
    
    // Save the value
    aValue = num;
    *anIsEmpty = FALSE;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueInt no end");
        
    }
    endTag = startTag;
    
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s%d\n", [self indent], aValue);
    }
#endif
    return aValue;
}

- (unsigned int)getUnsignedIntIsEmpty:(BOOL*)anIsEmpty 
{
    unsigned int aValue = 0;
    
    [self _getNext:kGetValueAsInt];
    
    // This means there was no value given, return zero
    //
    if (tokenType == kASTypeEnd) {
        *anIsEmpty = TRUE;
        return aValue;
    }
    
    // Save the value
    aValue = (unsigned int)num;
    *anIsEmpty = FALSE;
    
    // Read the next token; it had better be the end of the current tag
    // If not its an error
    //
    [self _getNext:kGetValueAsString];
    if (tokenType != kASTypeEnd) {
        FXDebugLog(kFXLogWBXML, @"AS getValueUnsignedInt no end");
    }
    endTag = startTag;
    
#ifdef DEBUG
    if(gIsFixTrace) {
    }else if(debug) {
        FXDebugLog(kFXLogWBXML, @"%s%u\n", [self indent], aValue);
    }
#endif
    return aValue;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Internal 
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Internal

- (EASTokenType)_getNext:(EGetValueType)aGetValueType
{
    int savedEndTag = endTag;
    if (tokenType == kASTypeEnd) {
        if(depth > 0) {
            depth--;
            if(gIsFixTrace) {
                TraceLog(@"%s} %s\n", [self indent], nameArray[depth]);
            }else{
#ifdef DEBUG
                if(debug) {
                     FXDebugLog(kFXLogWBXML, @"%s} %s\n", [self indent], nameArray[depth]);
                }
#endif
            }
        }else{
            FXDebugLog(kFXLogWBXML, @"Tag stack underflow");
        }
    } else {
        endTag = NOT_ENDED;
    }
    
    if (noContent) {
        tokenType = kASTypeEnd;
        noContent = false;
        endTag = savedEndTag;
        return tokenType;
    }
    
    data            = NULL;
    text            = NULL;
    nextId          = NOT_FETCHED;
 
    if(d) {
        FastScanner* aFastScanner = (FastScanner*)d;
        
        EASGlobalToken anID = (EASGlobalToken)aFastScanner->getByte();
        if(anID == SWITCH_PAGE) {
            int aPage = aFastScanner->getByte();
            page = aPage << AS_PAGE_SHIFT;
            if(gIsFixTrace) {
                tagTable = pageTable[aPage];
            }else{
    #ifdef DEBUG
                tagTable = pageTable[aPage];
    #endif
            }
            anID = (EASGlobalToken)aFastScanner->getByte();
        }
        switch(anID) {
            case EOF_BYTE:
                // End of document
                tag = AS_END_DOCUMENT;
                tokenType = kASTypeDone;
                break;
                
            case END:
                // End of tag
                tokenType = kASTypeEnd;
                
                // Retrieve the now-current startTag from our stack
                startTag = endTag = startTagArray[depth];
                break;
                
            case STR_I:
                // Inline string
                tokenType = kASTypeText;
                switch(aGetValueType) {
                    case kGetValueAsString:
                        text = [self _readInlineString]; break;
                    case kGetValueAsInt:
                        num = [self _readInlineInt]; break;
                    case kGetValueAsData:
                        data = [self _readInlineData]; break;
                }
                break;
            case OPAQUE:
            {
                int aMultiByteInteger = 0;
                unsigned char aByte = aFastScanner->getByte();
                if(aByte & 0x80) {
                    aMultiByteInteger = aByte & 0x7f;
                    while((aByte = aFastScanner->getByte()) & 0x80) {
                        aMultiByteInteger = (aMultiByteInteger << 7) | (aByte & 0x7f);
                    }
                    aMultiByteInteger = (aMultiByteInteger << 7) | (aByte & 0x7f);
                }else{
                    aMultiByteInteger = aByte;
                }
                
                NSMutableData* aData = [NSMutableData dataWithLength:aMultiByteInteger];
                unsigned char* aPtr = (unsigned char*)[aData mutableBytes];
                for(int i = 0 ; i < aMultiByteInteger; ++i) {
                    aPtr[i] = aFastScanner->getByte();
                }
                data = aData;
                break;
            }
            case LITERAL:
                FXDebugLog(kFXLogFIXME, @"FIXME AS Literal");
                break;
            default:
                // Start of tag
                tokenType = kASTypeStart;
                // The tag is in the low 6 bits
                startTag = (anID & 0x3F);
                if(gIsFixTrace && startTag >= kTagOffset) {
                    const char* aName = tagTable[startTag - kTagOffset];
                    nameArray[depth] = aName;
                    TraceLog(@"%s%s {\n", [self indent], aName);
                }else{
#ifdef DEBUG
                    if(debug && startTag >= kTagOffset) {
                        const char* aName = tagTable[startTag - kTagOffset];
                        nameArray[depth] = aName;
                        FXDebugLog(kFXLogWBXML, @"%s%s {\n", [self indent], aName);
                    }
#endif
                }
                
                // If the high bit is set, there is content (a value) to be read
                noContent = (anID & 0x40) == 0;
                depth++;
                if(depth < kMaxTagDepth) {
                    // Save the startTag to our stack
                    startTagArray[depth] = startTag;
                }else{
                    FXDebugLog(kFXLogWBXML, @"Tag stack overflow");
                }
        }
    }
    
    // Return the type of data we're dealing with
    return tokenType;
}

- (int)_readInlineInt
{
    int result = 0;
 
    if(d) {
        BOOL isNegative = FALSE;
        FastScanner* aFastScanner = (FastScanner*)d;
        while (true) {
            int aByte = aFastScanner->getByte();
            // Inline strings are always terminated with a zero byte
            if(aByte == 0) {
                if(isNegative) {
                    result = -result;
                }
                return result;
            }
            if(aByte >= '0' && aByte <= '9') {
                result = (result * 10) + (aByte - '0');
            }else if(aByte == '-') {
                isNegative = TRUE;
            } else {
                FXDebugLog(kFXLogWBXML, @"readInlineInt failed");
                break;
            }
        }
        if(isNegative) {
            result = -result;
        }
    }
    return result;
}

- (NSString*)_readInlineString
{
    NSString* aString = NULL;

    if(d) {
        FastScanner* aFastScanner = (FastScanner*)d;
        const char* aStartCursor = (const char*)aFastScanner->cursor();
        while (true) {
            int aByte = aFastScanner->getByte();
            if(aByte == 0) {
                aString = [NSString stringWithUTF8String:aStartCursor];
                break;
            }
        }
    }
    return aString;
}

- (NSData*)_readInlineData
{
    NSData* aData = NULL;
    if(d) {
        FastScanner* aFastScanner = (FastScanner*)d;

        const char* aStartCursor = (const char*)aFastScanner->cursor();
        while (true) {
            int aByte = aFastScanner->getByte();
            if(aByte == 0) {
                int aLength = (const char*)aFastScanner->cursor() - aStartCursor;
                aData = [NSData dataWithBytes:aStartCursor length:aLength];
                break;
            }
        }
    }
    return aData;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (const char*)indent
{
    int aDepth = depth;
    if(aDepth >= kMaxTagDepth -1) {
        aDepth = kMaxTagDepth-1;
    }
    memset(indent, '\t', aDepth);
    indent[aDepth] = '\0';
    
    return indent;
}

#ifdef DEBUG
- (void)dump
{
    printf("\n");
    if(d) {
        FastScanner* aFastScanner = (FastScanner*)d;
        const wbyte* aBytes = aFastScanner->bytes();
        for(int i = 0 ; i < aFastScanner->length() ; ++i) {
            printf("0x%02x ", aBytes[i]);
        }
        printf("\n");
    }
}
#endif

@end
