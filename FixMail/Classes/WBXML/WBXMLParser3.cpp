/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser developed 
 *       by MediaSite Inc Research and Development Group"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */

#include "WBXMLParser.h"

#include "WBXMLParserInternalDefs.h"

size_t WBXMLParser::parseAttrStart(const byte* data, size_t length) {
	enum LocalState {
		nil,
		index,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case nil:
			_continuation._accumulator.push(saxStringNull);
			_continuation._accumulator.push(saxStringNull);

			if (*data < 0x80) {
				// place processing of a wellknown attribute start here

				TOP_CONTINUATION_STACK(done);

				SAXString attributeValue;
				SAXString attributeName;
				_continuation._accumulator.pop();
				_continuation._accumulator.pop();
				resolveAttributeName(*data, attributeName, attributeValue);
				_continuation._accumulator.push(attributeName.c_str());
				_continuation._accumulator.push(attributeValue.c_str());

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else if (*data == LITERAL) {
				TOP_CONTINUATION_STACK(index);
				_continuation._bytesConsumed++;
				PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			} else
				ASSERT(!"Invalid attrStart value");
			break;
		case index:
			// process attribute name
			_continuation._accumulator.pop();
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			_continuation._accumulator.top() = (SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32);
			_continuation._accumulator.push(saxStringNull);

			TOP_CONTINUATION_STACK(done);
			break;
        case done:
            break;
		}
		
		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::attrstart) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseAttrValue(const byte* data, size_t length) {
	enum LocalState {
		nil,
		after_string,
		after_entity,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case nil:
			switch (*data) {
			case STR_I:
			case STR_T:
				TOP_CONTINUATION_STACK(after_string);
				PUSH_CONTINUATION_STACK(WBXMLParser::string);
				break;
			case EXT_I_0:
			case EXT_I_1:
			case EXT_I_2:
			case EXT_T_0:
			case EXT_T_1:
			case EXT_T_2:
			case EXT_0:
			case EXT_1:
			case EXT_2:
				TOP_CONTINUATION_STACK(done);
				PUSH_CONTINUATION_STACK(WBXMLParser::extension);
				break;
			case ENTITY:
				TOP_CONTINUATION_STACK(after_entity);
				_continuation._bytesConsumed++;
				PUSH_CONTINUATION_STACK(WBXMLParser::entity);
				break;
			default:
				if (*data >= 0x80) {
					SAXString attributeValue;
					resolveAttributeValue(*data, attributeValue);
					_continuation._accumulator.top() += attributeValue;

					TOP_CONTINUATION_STACK(done);

					parsedHere = 1;
					_continuation._bytesConsumed++;
				} else
					TOP_CONTINUATION_STACK(done);
			}
			break;
		case after_string: {
			TOP_CONTINUATION_STACK(done);

			SAXString stringRead = _continuation._accumulator.top();
			_continuation._accumulator.pop();

			_continuation._accumulator.top() += stringRead;
	
			} break;
		case after_entity: {
			TOP_CONTINUATION_STACK(done);

			SAXString stringRead = _continuation._accumulator.top();
			_continuation._accumulator.pop();

			_continuation._accumulator.top() += stringRead;
	
			} break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::attrvalue) {
		POP_CONTINUATION_STACK;
	}
	
	return parsedBytes;
}

size_t WBXMLParser::parseExtension(const byte* data, size_t length) {
    ASSERT(getErrorHandler() != 0);
	enum LocalState {
		code,
		termstr0,
		after_termstr0,
		termstr1,
		after_termstr1,
		termstr2,
		after_termstr2,
		index0,
		after_index0,
		index1,
		after_index1,
		index2,
		after_index2,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = code;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case code:
			switch (*data) {
				case EXT_I_0:
					TOP_CONTINUATION_STACK(termstr0);
					break;
				case EXT_I_1:
					TOP_CONTINUATION_STACK(termstr1);
					break;
				case EXT_I_2:
					TOP_CONTINUATION_STACK(termstr2);
					break;
				case EXT_T_0:
					TOP_CONTINUATION_STACK(index0);
					break;
				case EXT_T_1:
					TOP_CONTINUATION_STACK(index1);
					break;
				case EXT_T_2:
					TOP_CONTINUATION_STACK(index2);
					break;
				case EXT_0:
					TOP_CONTINUATION_STACK(done);
					if (getExtensionHandler())
						getExtensionHandler()->extension(0);
					break;
				case EXT_1:
					TOP_CONTINUATION_STACK(done);
					if (getExtensionHandler())
						getExtensionHandler()->extension(1);
					break;
				case EXT_2: 
					TOP_CONTINUATION_STACK(done);
					if (getExtensionHandler())
						getExtensionHandler()->extension(2);
					break;
				default:
					getErrorHandler()->fatalError(WBXMLSAXParseException("Invalid extension code", saxStringNull, saxStringNull, 0, _continuation._bytesConsumed));
					break;
			}

			parsedHere = 1;
			_continuation._bytesConsumed++;
			break;
		case termstr0:
			TOP_CONTINUATION_STACK(after_termstr0);

			PUSH_CONTINUATION_STACK(WBXMLParser::termstr);
			break;
		case termstr1:
			TOP_CONTINUATION_STACK(after_termstr1);

			PUSH_CONTINUATION_STACK(WBXMLParser::termstr);
			break;
		case termstr2:
			TOP_CONTINUATION_STACK(after_termstr2);

			PUSH_CONTINUATION_STACK(WBXMLParser::termstr);
			break;
		case index0:
			TOP_CONTINUATION_STACK(after_index0);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case index1:
			TOP_CONTINUATION_STACK(after_index1);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case index2:
			TOP_CONTINUATION_STACK(after_index2);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case after_termstr0:
			TOP_CONTINUATION_STACK(done);
			if (getExtensionHandler())
					getExtensionHandler()->extensionInline(0, _continuation._termstr);
			break;
		case after_termstr1:
			TOP_CONTINUATION_STACK(done);
			if (getExtensionHandler())
					getExtensionHandler()->extensionInline(1, _continuation._termstr);
			break;
		case after_termstr2:
			TOP_CONTINUATION_STACK(done);
			if (getExtensionHandler())
					getExtensionHandler()->extensionInline(2, _continuation._termstr);
			break;
		case after_index0:
			TOP_CONTINUATION_STACK(done);
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			if (getExtensionHandler())
					getExtensionHandler()->extensionTableref(0, ((SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32)));
			break;
		case after_index1:
			TOP_CONTINUATION_STACK(done);
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			if (getExtensionHandler())
					getExtensionHandler()->extensionTableref(1, ((SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32)));
			break;
		case after_index2:
			TOP_CONTINUATION_STACK(done);
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			if (getExtensionHandler())
					getExtensionHandler()->extensionTableref(2, ((SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32)));
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::extension) {
		POP_CONTINUATION_STACK;
	}
	
	return parsedBytes;
	
}

size_t WBXMLParser::parseString(const byte* data, size_t length) {
    ASSERT(getErrorHandler() != 0);
	enum LocalState {
		code,
		index,
		after_index,
		termstr,
		after_termstr,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = code;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case code:
			switch (*data) {
				case STR_I:
					TOP_CONTINUATION_STACK(termstr);
					break;
				case STR_T:
					TOP_CONTINUATION_STACK(index);
					break;
				default:
					getErrorHandler()->fatalError(WBXMLSAXParseException("Invalid string code", saxStringNull, saxStringNull, 0, _continuation._bytesConsumed));
					break;
			}

			parsedHere = 1;
			_continuation._bytesConsumed++;
			break;
		case termstr:
			TOP_CONTINUATION_STACK(after_termstr);
			PUSH_CONTINUATION_STACK(WBXMLParser::termstr);
			break;
		case index:
			TOP_CONTINUATION_STACK(after_index);
			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case after_termstr:
			TOP_CONTINUATION_STACK(done);
			_continuation._accumulator.push(_continuation._termstr);
			break;
		case after_index:
			TOP_CONTINUATION_STACK(done);

			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			_continuation._accumulator.push((SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32));
			
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::string) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
	
}

size_t WBXMLParser::parseTermStr(const byte* data, size_t length) {
	enum LocalState {
		nil,
		chars,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	if (localState == nil) {
		_continuation._termstr.erase();
		TOP_CONTINUATION_STACK(chars);		
	}

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		if (*data == 0) {
			localState = done;
		} else {
			_continuation._termstr += *data;
		}

		parsedHere = 1;
		_continuation._bytesConsumed++;

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::termstr) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseEntity(const byte* data, size_t length) {
	enum LocalState {
		code,
		index,
		after_index,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = code;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case code:
			_continuation._accumulator.push(saxStringNull);
			switch (*data) {
				case ENTITY:
					TOP_CONTINUATION_STACK(index);
					break;
				default:
					getErrorHandler()->fatalError(WBXMLSAXParseException("Invalid entity code", saxStringNull, saxStringNull, 0, _continuation._bytesConsumed));
					break;
			}

			parsedHere = 1;
			_continuation._bytesConsumed++;
			break;
		case index:
			TOP_CONTINUATION_STACK(after_index);
			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case after_index:
			TOP_CONTINUATION_STACK(done);
			_continuation._accumulator.top() = (SAXChar)(_continuation._mbuint32);
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::entity) {
		POP_CONTINUATION_STACK;
	}
	
	return parsedBytes;
}

size_t WBXMLParser::parseOpaque(const byte* data, size_t length) {
	enum LocalState {
		code,
		data_length,
		bytes,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = code;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case code:
			_continuation._accumulator.push(saxStringNull);
			parsedHere = 1;
			_continuation._bytesConsumed++;
			TOP_CONTINUATION_STACK(data_length);
			break;
		case data_length:
			TOP_CONTINUATION_STACK(bytes);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case bytes: {
			size_t dataLength; 
			size_t offset = 0;
			if (_continuation._stack.top()._localStates.size() == 3) {
				ASSERT(!_continuation._accumulator.top().empty());
				offset = _continuation._stack.top()._localStates[1];
				dataLength = _continuation._stack.top()._localStates[2];
			} else {
				ASSERT(_continuation._accumulator.top().empty());
				dataLength = _continuation._mbuint32;
				_continuation._accumulator.top().reserve(dataLength);
			}

			if (dataLength > 0) {
				parsedHere  = (dataLength-offset > length) ? length : dataLength-offset;
				_continuation._accumulator.top().append((SAXChar*)data, parsedHere);
				offset += parsedHere;
				_continuation._bytesConsumed += parsedHere;
			}

			if (offset < dataLength) {
				TOP_CONTINUATION_STACK3(bytes, offset, dataLength);
			} else {
				TOP_CONTINUATION_STACK(done);
			}
			} break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::opaque) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

void WBXMLParser::resolveTagName(WellknownResolver::Token token, SAXString& tagName) {
	char buffer[256] = {0};
	_wellknownResolver->resolveTagName(token, buffer, 256);
	tagName = buffer;
}

void WBXMLParser::resolveAttributeName(WellknownResolver::Token token, SAXString& attributeName, SAXString& attributeValue) {
	char bufferN[256] = {0};
	char bufferV[256] = {0};
	_wellknownResolver->resolveAttributeName(token, bufferN, 256, bufferV, 256);
	attributeName = bufferN;
	attributeValue = bufferV;
}

void WBXMLParser::resolveAttributeValue(WellknownResolver::Token token, SAXString& attributeValue) {
	char buffer[256] = {0};
	_wellknownResolver->resolveAttributeValue(token, buffer, 256);
	attributeValue = buffer;
}

