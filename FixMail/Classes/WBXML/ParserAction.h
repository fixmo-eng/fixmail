/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

typedef enum  {
    kArgNone,
    kArgInt,
    kArgString,
    kArgObject,
    kArgArray
} EArgType;

@interface ParserAction : NSObject {
    SEL             selector;
    EArgType        argType;
    NSDictionary*   childActions;
}

- (id)initWithAction:(SEL)aSelector argType:(EArgType)anArgType childActions:(NSDictionary*)aChildActions;
- (void)dispatchStart:(id)aTarget;
- (void)dispatch:(id)aTarget string:(NSString*)aString;

@end
