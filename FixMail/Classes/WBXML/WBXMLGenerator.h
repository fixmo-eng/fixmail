/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */


#ifndef __WBXMLGENERATOR_H
#define __WBXMLGENERATOR_H
#include <stdlib.h>
#include "WellknownResolver.h"



#include <stack>

class WBXMLGenerator {
public:
	WBXMLGenerator();
	void startDocument1(unsigned int publicid, unsigned int charset, const SAXString& stringTable);
	void startDocument2(unsigned int publicidIndex, unsigned int charset, const SAXString& stringTable);
	void endDocument();
    
	void startElement1(WellknownResolver::Tag token, bool attributes, bool content);
	void startElement2(unsigned int index, bool attributes, bool content);
	void endElement();
	void startProcessingInstruction();
	void endProcessingInstruction();

	void startAttributes();
	void addAttribute1(WellknownResolver::Token attrstart);
	void addAttribute2(unsigned int index);
	void addAttributeValue(WellknownResolver::Token attrvalue);
	void addAttributeValueString1(unsigned int index);
	void addAttributeValueString2(const SAXString& termstr);
	void addAttributeValueExtension1(unsigned int code, const SAXString& termstr);
	void addAttributeValueExtension2(unsigned int code, unsigned int index);
	void addAttributeValueExtension3(unsigned int code);
	void addAttributeValueEntity(unsigned int entcode);
	void endAttributes();

	void startContent();
	void addContentValueString1(unsigned int index);
	void addContentValueString2(const SAXString& termstr);
	void addContentValueExtension1(unsigned int code, const SAXString& termstr);
	void addContentValueExtension2(unsigned int code, unsigned int index);
	void addContentValueExtension3(unsigned int code);
	void addContentValueEntity(unsigned int entcode);
	void addContentValueOpaque(const void* bytes, unsigned int length);
	void endContent();

	static const SAXString encodeMBUInt32(unsigned int number);

protected:
	virtual void outputVersion(const void* data, size_t length) = 0;
	virtual void outputPublicId(const void* data, size_t length) = 0;
	virtual void outputCharset(const void* data, size_t length) = 0;
	virtual void outputStringTable(const void* data, size_t length) = 0;
	virtual void outputBody(const void* data, size_t length) = 0;
    virtual void outputByte(unsigned char aByte) = 0;

	
	struct ElementDescription {
		WellknownResolver::Token _token;
		bool _attributes;
		bool _content;
		bool _attributesFinished;
		bool _contentFinished;
	};
	
	std::stack<ElementDescription> _elementDescriptionStack;

	size_t _stringTableLength;
	bool _bodyElementAdded;
	bool _documentStarted;
    byte _page;
};

#endif
