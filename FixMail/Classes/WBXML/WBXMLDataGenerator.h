/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "WBXMLGenerator.h"
#import "AS.h"

#ifndef ReMailIPhone_WBXMLDataGenerator_h
#define ReMailIPhone_WBXMLDataGenerator_h

@class WBXMLRequest;

static const int kWBXMLUnknownPublicID      = 0x1;
static const int KWBXMLCharSetUTF8          = 0x6a;

////////////////////////////////////////////////////////////////////////////////
// WBXML Generator
////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Generator

class WBXMLDataGenerator : public WBXMLGenerator {
public:	
    WBXMLDataGenerator() {
        static const int kCapacity = 256;
        data = [[NSMutableData alloc] initWithCapacity:kCapacity];
        
        char stringTable[] = {
        };
        
        SAXString stringTableString;
        stringTableString.assign(stringTable, sizeof(stringTable)/sizeof(stringTable[0]));
        
        startDocument1(kWBXMLUnknownPublicID, KWBXMLCharSetUTF8, stringTableString);
    }
    ~WBXMLDataGenerator() { 
        //[data release];   // Don't release the data here, the client owns it now
        data = NULL; 
    }
    
    void start(WellknownResolver::Tag tag);
    
    void keyValue(WellknownResolver::Tag tag, NSString* termstr);
    void keyValueInt(WellknownResolver::Tag tag, int anInt);
    void keyOpaque(WellknownResolver::Tag tag, NSData* aData);
    
    void tag(WellknownResolver::Tag tag);
    void end();
    
public:
    NSData* encodedData() {
        endDocument();
        return data; 
    }
    
    NSData* encodedData(WBXMLRequest* aRequest, ASTag aTopTag);
    
protected:
    void output(const void* bytes, size_t length) {
        [data appendBytes:bytes length:length];
    }
    
    virtual void outputVersion(const void* data, size_t length) {
        output(data, length);
    }
    virtual void outputPublicId(const void* data, size_t length) {
        output(data, length);
    }
    virtual void outputCharset(const void* data, size_t length) {
        output(data, length);
    }
    virtual void outputStringTable(const void* data, size_t length) {
        output(data, length);
    }
    virtual void outputBody(const void* data, size_t length) {
        output(data, length);
    }
    virtual void outputByte(unsigned char aByte) {
        output(&aByte, 1);
    }

private:
    NSMutableData*     data;
};


#endif
