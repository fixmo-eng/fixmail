/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "AS.h"
#import "HttpRequest.h"
#import "FastWBXMLParser.h"
	
// Types
//

#define kActiveSyncSuccessNotification  @"kActiveSyncSuccessNotification"
#define kActiveSyncFailNotification     @"kActiveSyncFailNotification"

typedef enum {
    kASInvalidContent                       = 101,
    kASInvalidWBXML                         = 102,
    kASInvalidXML                           = 103,
    kASInvalidDateTime                      = 104,
    kASInvalidCombinationOfIDs              = 105,
    kASInvalidIDs                           = 106,
    kASInvalidMIME                          = 107,
    kASDeviceIdMissingOrInvalid             = 108,
    kASDeviceTypeMissingOrInvalid           = 109,
    kASServerError                          = 110,
    kASServerErrorRetryLater                = 111,
    kASActiveDirectoryAccessDenied          = 112,
    kASMailboxQuotaExceeded                 = 113,
    kASMailboxServerOffline                 = 114,
    kASSendQuotaExceeded                    = 115,
    kASMessageRecipientUnresolved           = 116,
    kASMessageReplyNotAllowed               = 117,
    kASMessagePreviouslySent                = 118,
    kASMessageHasNoRecipient                = 119,
    kASMailSubmissionFailed                 = 120,
    kASMessageReplyFailed                   = 121,
    kASAttachmentIsTooLarge                 = 122,
    kASUserHasNoMailbox                     = 123,
    kASUserCannotBeAnonymous                = 124,
    kASUserPrincipalCouldNotBeFound         = 125,
    kASUserDisabledForSync                  = 126,
    kASUserOnNewMailboxCannotSync           = 127,
    kASUserOnLegacyMailboxCannotSync        = 128,
    kASDeviceIsBlockedForThisUser           = 129,
    kASAccessDenied                         = 130,
    kASAccountDisabled                      = 131,
    kASSyncStateNotFound                    = 132,
    kASSyncStateLocked                      = 133,
    kASSyncStateCorrupt                     = 134,
    kASSyncStateAlreadyExists               = 135,
    kASSyncStateVersionInvalid              = 136,
    kASCommandNotSupported                  = 137,
    kASVersionNotSupported                  = 138,
    kASDeviceNotFullyProvisionable          = 139,
    kASRemoteWipeRequested                  = 140,
    kASLegacyDeviceOnStrictPolicy           = 141,
    kASDeviceNotProvisioned                 = 142,
    kASPolicyRefresh                        = 143,
    kASInvalidPolicyKey                     = 144,
    kASExternallyManagedDevicesNotAllowed   = 145,
    kASNoRecurrenceInCalendar               = 146,
    kASUnexpectedItemClass                  = 147,
    kASRemoteServerHasNoSSL                 = 148,
    kASInvalidStoredRequest                 = 149,
    kASItemNotFound                         = 150,
    kASTooManyFolders                       = 151,
    kASNoFoldersFound                       = 152,
    kASItemLostAfterMove                    = 153,
    kASFailureInMoveOperation               = 154,
    kASMoveCommandDisallowedForNonPersist   = 155,
    kASMoveCommandInvalidDestinationFolder  = 156,
    kASUndefined157                         = 157,
    kASUndefined158                         = 158,
    kASUndefined159                         = 159,
    kASAvailabilityTooManyRecipients        = 160,
    kASAvailabilityDLLimitReached           = 161,
    kASAvailabilityTransientFailure         = 162,
    kASAvailabilityFailure                  = 163,
    kASBodyPartPreferenceTypeNotSupported   = 164,
    kASDeviceInformationRequired            = 165,
    kASInvalidAccountId                     = 166,
    kASAccountSendDisabled                  = 167,
    kASIRM_FeatureDisabled                  = 168,
    kASIRM_TransientError                   = 169,
    kASIRM_PermanentError                   = 170,
    kASIRM_InvalidTemplateID                = 171,
    kASIRM_OperationNotPermitted            = 172,
    kASNoPicture                            = 173,
    kASPictureTooLarge                      = 174,
    kASPictureLimitReached                  = 175,
    kASBodyPart_ConversationTooLarge        = 176,
    kASMaximumDevicesReached                = 177
} EActiveSyncStatus;

// Debug support
//
#ifdef DEBUG
#define USE_SYBYX
#endif
#ifdef USE_SYBYX
#import "WBXMLParserDelegate.h"
#import "ParserAction.h"

/*
@interface  EParserAction : NSObject
{
@public
    // FIXME use token instead of tag string in resolver to improve performancs
//    unsigned int    tag;
    NSString*       tag;
    SEL             selector;
    EArgType        argType;
    void*           childActions;
} EParserAction;

@end

@interface EParserKeyValue : NSObject
 {
    // FIXME use token instead of tag string in resolver to improve performancs
//    unsigned int    tag;
    NSString*       tag;
    EArgType        argType;
}

@end
*/
#endif

@interface WBXMLRequest : HttpRequest {
    NSMutableData*                      data;
    BOOL                                isQueued;
    
#ifdef USE_SYBYX
    NSObject<WBXMLParserDelegate>*      parserDelegate;
    NSMutableDictionary*                parserStartActions;
    NSMutableDictionary*                parserEndActions;
    NSString*                           characters;
#endif
}

@property (nonatomic, strong) NSString* command;
@property (nonatomic) ASTag             commandTag;
@property (nonatomic) BOOL              displayErrors;


// Debug
+ (BOOL)traceWBXMLRequests;
+ (void)setTraceWBXMLRequests:(BOOL)aTraceRequests;

+ (BOOL)traceWBXMLResponses;
+ (void)setTraceWBXMLResponses:(BOOL)aTraceResponses;

// Factory
+ (NSOperationQueue*)operationQueue;
+ (NSUInteger)operationQueueCount;
+ (WBXMLRequest*)queuedRequest;
+ (void)wait;
+ (void)deleteQueue;

- (void)setIsQueued;

// Construct/Destruct
- (id)initWithAccount:(ASAccount *)anAccount;
- (id)initWithAccount:(ASAccount *)anAccount command:(NSString*)aCommand commandTag:(ASTag)aTag;

// ActiveSync Error Handling
- (void)handleHttpErrorForAccount:(ASAccount*)anAccount
                       connection:(HttpConnection*)connection
                         response:(NSURLResponse*)response;
- (NSString*)handleActiveSyncError:(EActiveSyncStatus)aSyncStatus;

// Interface
- (void)queue;
- (void)success;
- (void)fail;

// WBXMLRequest Virtual
- (void)displayError;   // Must be overridden and implemented
- (void)send;           // Must be overridden and implemented

// HttpRequest Override
- (void)retry;

// NSURLConnection delegate, may be overridden
- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response;
- (void)connectionDidFinishLoading:(HttpConnection*)connection;

// Parser
-(void)parseWBXMLData:(NSData*)aData command:(ASTag)aCommandTag;

- (void)fastParser:(FastWBXMLParser*)aParser;

// Sybyx Parser
#ifdef USE_SYBYX
- (void)wbxmlParser:(NSData*)aData command:(ASTag)aCommand debug:(bool)aDebug;
//- (void)setParserActions:(EParserAction*)parserStartActions endActions:(EParserAction*)parserStartActions;
#endif 

// Debug
#ifdef DEBUG
+ (void)storeDebugData:(NSData*)aData;
+ (NSData*)loadDebugData;
#endif

@end

