/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */

#ifndef __SELF_ADJUSTING_DELAY
#define __SELF_ADJUSTING_DELAY

#if defined(__POSIX__)
#include <unistd.h>
#endif

#include <assert.h>
#define ASSERT(x) assert(x)

class SelfAdjustingDelay {
public:
	SelfAdjustingDelay(unsigned int initialDelay = 256, unsigned int highLimit = 1024, unsigned int lowLimit = 128) {
		ASSERT(initialDelay < highLimit);
		ASSERT(initialDelay > lowLimit);

		_delay = initialDelay;
		_lowLimit = lowLimit;
		_highLimit = highLimit;
	}
	inline unsigned int delay() {
		if (_delay*2 <= _highLimit)
			_delay *= 2;

		sleep();

		return _delay;
	}
	inline unsigned int skip() {
		if (_delay/2 >= _lowLimit)
			_delay /= 2;

		return _delay;
	}
protected:
	inline void sleep() {
#if defined(RIM)
    RimSleep(_delay / 10);
#elif defined(_MSC_VER)
	Sleep(_delay);
#elif defined(__MWERKS__) || defined(__PRCTOOLS__)
	SysTaskDelay((_delay * SysTicksPerSecond())/1000);
#elif defined(__POSIX__) // POSIX
	usleep(_delay*1000);
#endif
	}
private:
	unsigned int _delay; // milleseconds
	unsigned int _lowLimit;
	unsigned int _highLimit;
};

#endif
