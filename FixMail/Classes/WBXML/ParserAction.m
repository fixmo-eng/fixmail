/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ParserAction.h"


@implementation ParserAction


- (id)initWithAction:(SEL)aSelector argType:(EArgType)anArgType childActions:(NSDictionary*)aChildActions
{
    if(self) {
        selector        = aSelector;
        argType         = anArgType;
        childActions    = aChildActions; 
    }
    return self;
}

- (void)dealloc
{
    if(childActions) {
         childActions = NULL;
    }
}

- (void)dispatchStart:(id)aTarget
{
    SuppressPerformSelectorLeakWarning([aTarget performSelector:selector]);
}

- (void)dispatch:(id)aTarget string:(NSString*)aString;
{
    switch(argType) {
        case kArgNone:
            SuppressPerformSelectorLeakWarning([aTarget performSelector:selector]);
            break;
        case kArgInt:
        {
            int anInt = [aString intValue];
            SuppressPerformSelectorLeakWarning([aTarget performSelector:selector withObject:[NSNumber numberWithInt:anInt]]);
            break;
        case kArgString:
            SuppressPerformSelectorLeakWarning([aTarget performSelector:selector withObject:aString]);
            break;
        case kArgObject:
            // FIXME with object
            SuppressPerformSelectorLeakWarning([aTarget performSelector:selector withObject:NULL]);
            break;
        case kArgArray:
            break;
        }
    }
}

@end
