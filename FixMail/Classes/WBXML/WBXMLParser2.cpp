/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser developed 
 *       by MediaSite Inc Research and Development Group"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */
#include "WBXMLParser.h"
#include "WBXMLParserInternalDefs.h"

size_t WBXMLParser::parseBody(const byte* data, size_t length) {
	enum LocalState {
		nil,
		pi1,
		element,
		pi2,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case nil:
			if (*data == PI) {
				TOP_CONTINUATION_STACK(pi1);
			} else {
				TOP_CONTINUATION_STACK(element);
			}

			break;
		case pi1:
			TOP_CONTINUATION_STACK(element);

			PUSH_CONTINUATION_STACK(WBXMLParser::pi);
			break;
		case element:
			TOP_CONTINUATION_STACK(pi2);

			PUSH_CONTINUATION_STACK(WBXMLParser::element);
			break;
		case pi2:
			PUSH_CONTINUATION_STACK(WBXMLParser::pi);
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (length == 0 && _continuation._noMoreInput) {
		ASSERT(_continuation._stack.top()._method == WBXMLParser::body);
		TOP_CONTINUATION_STACK(done);
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::body) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseMbuint32(const byte* data, size_t length) {
	static const byte mbuint32_continuation_flag = 0x80;
	static const byte mbuint32_value = 0x7f;

	unsigned int localState;

	if (!_continuation._stack.top()._localStates.empty()) {
		localState = _continuation._stack.top()._localStates[0];
	} else {
		localState = 0;
		_continuation._stack.top()._buffer.clear();
	}

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != 1) {
		_continuation._stack.top()._buffer.push_back(*data);

		localState = (*data & mbuint32_continuation_flag) == 0;
		if (localState) {
			_continuation._mbuint32 = 0;
			std::vector<byte>::reverse_iterator it = _continuation._stack.top()._buffer.rbegin();
			for (; it != _continuation._stack.top()._buffer.rend(); it++) {
				unsigned int thisByte = (*it) & mbuint32_value;
				thisByte <<= 7*(it - _continuation._stack.top()._buffer.rbegin());
				_continuation._mbuint32 |= thisByte;
			}
			
			POP_CONTINUATION_STACK;
		}

		data++;
		length--;
		parsedBytes++;
		_continuation._bytesConsumed++;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseElement(const byte* data, size_t length) {
    ASSERT(getErrorHandler() != 0);
	enum LocalState {
		stag,
		index,
		indexAttributeFollowing,
		indexAttributeContentFollowing,
		attributeFirst,
		attributeNext,
		attributeFirstContentFollowing,
		attributeNextContentFollowing,
		content,
		after_content,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = stag;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case stag:
            if(*data == SWITCH_PAGE) {
                //printf("Switch page 0x%x\n", data[1]);
                _wellknownResolver->setPage(data[1]);
                parsedHere = 2;
                _continuation._bytesConsumed+=2;
                break;
            }

			_continuation._attributeList.clear();
			_continuation._accumulator.push(saxStringNull);

			if ((*data & ~(TAG_CONTENT_MASK|TAG_ATTRIBUTES_MASK)) > 0x04) {
				if ((*data & TAG_CONTENT_MASK)== TAG_CONTENT_MASK) {
					if ((*data & TAG_ATTRIBUTES_MASK) == TAG_ATTRIBUTES_MASK) {
						TOP_CONTINUATION_STACK(attributeFirstContentFollowing);
					} else {
						TOP_CONTINUATION_STACK(content);
					}
				} else {
					if ((*data & TAG_ATTRIBUTES_MASK) == TAG_ATTRIBUTES_MASK) {
						TOP_CONTINUATION_STACK(attributeFirst);
					} else {
						TOP_CONTINUATION_STACK(done);
					}
				}

				resolveTagName((*data & 0x3f), _continuation._accumulator.top());
				if (localState == content || localState == done) {
					if (getDocumentHandler())
						getDocumentHandler()->startElement(_continuation._accumulator.top(), _continuation._attributeList);
				}

			} else switch (*data) {
				case LITERAL:
					TOP_CONTINUATION_STACK(index);
					PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
					break;
				case LITERAL_A:
					TOP_CONTINUATION_STACK(indexAttributeFollowing);
					PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
					break;
				case LITERAL_AC:
					TOP_CONTINUATION_STACK(indexAttributeContentFollowing);
					PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
					break;
				default:
                    getErrorHandler()->fatalError(WBXMLSAXParseException("Invalid global token", saxStringNull, saxStringNull, 0, _continuation._bytesConsumed));
					break;
			}

			parsedHere = 1;
			_continuation._bytesConsumed++;

			break;
		case index:
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			_continuation._accumulator.top() = (SAXChar*)(_continuation._stringTable.c_str()+ _continuation._mbuint32);

			if (getDocumentHandler())
				getDocumentHandler()->startElement(_continuation._accumulator.top(), _continuation._attributeList);

			TOP_CONTINUATION_STACK(done);
			break;
		case indexAttributeFollowing:
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			_continuation._accumulator.top() = (SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32);
			TOP_CONTINUATION_STACK(attributeNext);
			PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			break;
		case indexAttributeContentFollowing:
			ASSERT(!_continuation._stringTable.empty());
			ASSERT(_continuation._mbuint32 < _continuation._stringTable.size());
			_continuation._accumulator.top() = (SAXChar*)(_continuation._stringTable.c_str() + _continuation._mbuint32);
			TOP_CONTINUATION_STACK(attributeNextContentFollowing);
			PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			break;
		case attributeFirst:
			TOP_CONTINUATION_STACK(attributeNext);
			PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			break;
		case attributeNext:
			if (*data == END) {
				if (getDocumentHandler())
					getDocumentHandler()->startElement(_continuation._accumulator.top(), _continuation._attributeList);

				TOP_CONTINUATION_STACK(done);

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else {
				PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			}	
			break;
		case attributeFirstContentFollowing:
			TOP_CONTINUATION_STACK(attributeNextContentFollowing);
			PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			break;
		case attributeNextContentFollowing:
			if (*data == END) {
				if (getDocumentHandler())
					getDocumentHandler()->startElement(_continuation._accumulator.top(), _continuation._attributeList);

				TOP_CONTINUATION_STACK(content);

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else {
				PUSH_CONTINUATION_STACK(WBXMLParser::attribute);
			}	
			break;
		case after_content:
			if (getDocumentHandler() && !_continuation._accumulator.top().empty())
				getDocumentHandler()->characters(_continuation._accumulator.top());
			_continuation._accumulator.pop();
		case content:
			if (*data == END) {
				TOP_CONTINUATION_STACK(done);

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else {
				TOP_CONTINUATION_STACK(after_content);
				PUSH_CONTINUATION_STACK(WBXMLParser::content);
			}	
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}
	
	if (localState == done && _continuation._stack.top()._method == WBXMLParser::element) {
		POP_CONTINUATION_STACK;

		if (getDocumentHandler())
			getDocumentHandler()->endElement(_continuation._accumulator.top());
		_continuation._accumulator.pop();
	}

	return parsedBytes;
}

size_t WBXMLParser::parseAttribute(const byte* data, size_t length) {
	enum LocalState {
		attrstart,
		attrvalue,
		done
	};
	
	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = attrstart;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case attrstart:
			TOP_CONTINUATION_STACK(attrvalue);

			PUSH_CONTINUATION_STACK(WBXMLParser::attrstart);
			break;
		case attrvalue:
			switch (*data) {
			case STR_I:
			case STR_T:
			case EXT_I_0:
			case EXT_I_1:
			case EXT_I_2:
			case EXT_T_0:
			case EXT_T_1:
			case EXT_T_2:
			case EXT_0:
			case EXT_1:
			case EXT_2:
			case ENTITY:
				PUSH_CONTINUATION_STACK(WBXMLParser::attrvalue);
				break;
			default:
				if (*data >= 0x80) {
					PUSH_CONTINUATION_STACK(WBXMLParser::attrvalue);
				} else {
					TOP_CONTINUATION_STACK(done);
				}	
				break;
			}
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::attribute) {
		POP_CONTINUATION_STACK;

		SAXString attributeValue = _continuation._accumulator.top();
		_continuation._accumulator.pop();
		_continuation._attributeList.addAttribute(_continuation._accumulator.top(), "CDATA", attributeValue);
		_continuation._accumulator.pop();
	}

	return parsedBytes;
}

size_t WBXMLParser::parseContent(const byte* data, size_t length) {
	enum LocalState {
		nil,
		after_opaque,
		after_string,
		after_extension,
		after_entity,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case nil:
			_continuation._accumulator.push(saxStringNull);
			switch (*data) {
			case OPAQUE:
				TOP_CONTINUATION_STACK(after_opaque);
				PUSH_CONTINUATION_STACK(WBXMLParser::opaque);
				break;
			case STR_I:
			case STR_T:
				TOP_CONTINUATION_STACK(after_string);
				PUSH_CONTINUATION_STACK(WBXMLParser::string);
				break;
			case EXT_I_0:
			case EXT_I_1:
			case EXT_I_2:
			case EXT_T_0:
			case EXT_T_1:
			case EXT_T_2:
			case EXT_0:
			case EXT_1:
			case EXT_2:
				TOP_CONTINUATION_STACK(after_extension);
				_continuation._bytesConsumed++;
				PUSH_CONTINUATION_STACK(WBXMLParser::extension);
				break;
			case ENTITY:
				TOP_CONTINUATION_STACK(after_entity);
				_continuation._bytesConsumed++;
				PUSH_CONTINUATION_STACK(WBXMLParser::entity);
				break;
			case PI:
				TOP_CONTINUATION_STACK(done);
				PUSH_CONTINUATION_STACK(WBXMLParser::pi);
				break;
			default:
				TOP_CONTINUATION_STACK(done);
				PUSH_CONTINUATION_STACK(WBXMLParser::element);
				break;
			} break;
		case after_string: {
			TOP_CONTINUATION_STACK(done);

			SAXString stringRead = _continuation._accumulator.top();
			_continuation._accumulator.pop();

			_continuation._accumulator.top() += stringRead;
			} break;
		case after_extension:
			TOP_CONTINUATION_STACK(done);
			// TODO: process read extension
			break;
		case after_opaque: {
			TOP_CONTINUATION_STACK(done);

			SAXString stringRead = _continuation._accumulator.top();
			_continuation._accumulator.pop();

			_continuation._accumulator.top() += stringRead;
			} break;
		case after_entity: {
			TOP_CONTINUATION_STACK(done);

			SAXString stringRead = _continuation._accumulator.top();
			_continuation._accumulator.pop();

			_continuation._accumulator.top() += stringRead;
			} break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::content) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

size_t WBXMLParser::parsePI(const byte* data, size_t length) {
	enum LocalState {
		pi,
		attrstart,
		attrvalue,
		done
	};
	
	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = pi;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case pi:
			ASSERT(*data == PI);

			TOP_CONTINUATION_STACK(attrstart);

			parsedHere = 1;
			_continuation._bytesConsumed++;
			break;
		case attrstart:
			TOP_CONTINUATION_STACK(attrvalue);

			PUSH_CONTINUATION_STACK(WBXMLParser::attrstart);
			break;
		case attrvalue:
			if (*data == END) {
				TOP_CONTINUATION_STACK(done);

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else {
				PUSH_CONTINUATION_STACK(WBXMLParser::attrvalue);
			}	
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::pi) {
		POP_CONTINUATION_STACK;

		SAXString attributeValue = _continuation._accumulator.top().c_str();
		_continuation._accumulator.pop();
		SAXString attributeName = _continuation._accumulator.top().c_str();
		_continuation._accumulator.pop();
		if (getDocumentHandler())
			getDocumentHandler()->processingInstruction(attributeName, attributeValue);
	}

	return parsedBytes;
}

