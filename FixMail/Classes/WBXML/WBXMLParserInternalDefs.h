/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser developed 
 *       by MediaSite Inc Research and Development Group"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */

#ifndef __WBXMLPARSERINTERNALDEFS_H
#define __WBXMLPARSERINTERNALDEFS_H

#ifdef __MWERKS__
#define strlen(x) StrLen(x)
#endif

#ifdef _MSC_VER
#undef OPAQUE	// undefine OPAQUE from wingdi.h
#endif

#define SWITCH_PAGE 0x00
#define END 0x01
#define ENTITY 0x02
#define STR_I 0x03
#define LITERAL 0x04
#define EXT_I_0 0x40
#define EXT_I_1 0x41
#define EXT_I_2 0x42
#define PI 0x43
#define LITERAL_C 0x44
#define EXT_T_0 0x80
#define EXT_T_1 0x81
#define EXT_T_2 0x82
#define STR_T 0x83
#define LITERAL_A 0x84
#define EXT_0 0xc0
#define EXT_1 0xc1
#define EXT_2 0xc2
#define OPAQUE 0xc3
#define LITERAL_AC 0xc4

#define TAG_ATTRIBUTES_MASK 0x80
#define TAG_CONTENT_MASK 0x40

#define TOP_CONTINUATION_STACK(state) \
			localState = state; \
			_continuation._stack.top()._localStates.resize(1); \
			_continuation._stack.top()._localStates[0] = localState

#define TOP_CONTINUATION_STACK2(state1, state2) \
			localState = state1; \
			_continuation._stack.top()._localStates.resize(2); \
			_continuation._stack.top()._localStates[0] = localState;\
			_continuation._stack.top()._localStates[1] = state2

#define TOP_CONTINUATION_STACK3(state1, state2, state3) \
			localState = state1; \
			_continuation._stack.top()._localStates.resize(3); \
			_continuation._stack.top()._localStates[0] = localState;\
			_continuation._stack.top()._localStates[1] = state2;\
			_continuation._stack.top()._localStates[2] = state3

#define PUSH_CONTINUATION_STACK(method) \
			{	Continuation::Element continuationElement; \
			continuationElement._method = method; \
			_continuation._stack.push(continuationElement); \
			parsedHere = dispatch(data, length);	}

#define POP_CONTINUATION_STACK _continuation._stack.pop()

#endif