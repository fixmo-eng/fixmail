/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */

#ifndef __WBXMLPARSER_H
#define __WBXMLPARSER_H

#include "self_adjusting_delay.hpp"
#include "WellknownResolver.h"

class ExtensionHandler {
public:
	virtual void extension(unsigned int code) = 0;
	virtual void extensionInline(unsigned int code, const SAXString& value) = 0;
	virtual void extensionTableref(unsigned int code, const SAXString& value) = 0;
};

class InputStream {
public:
	InputStream& operator>>(char& n);
	InputStream& operator>>(bool& n);
	InputStream& operator>>(short& n);
	InputStream& operator>>(unsigned short& n);
	InputStream& operator>>(int& n);
	InputStream& operator>>(unsigned int& n);
	InputStream& operator>>(long& n);
	InputStream& operator>>(unsigned long& n);
	InputStream& operator>>(void *& n);
	InputStream& operator>>(float& n);
	InputStream& operator>>(double& n);
	InputStream& operator>>(long double& n);
    virtual size_t read(void *s, size_t n) = 0;
	virtual bool eof() const = 0;
};

#include "sax.h"

#include <vector>
#include <stack>

class AttributeListImp : public AttributeList {
public:
	virtual size_t getLength() const;

	virtual SAXString getName(size_t pos) const;
	virtual SAXString getType(size_t pos) const;
	virtual SAXString getValue(size_t pos) const;

	virtual SAXString getType(const SAXString& name) const;
	virtual SAXString getValue(const SAXString& name) const;

	AttributeListImp();
	AttributeListImp(const AttributeList& atts);
	void setAttributeList(const AttributeList& atts);
	void addAttribute(const SAXString& name, const SAXString& type, const SAXString& value);
	void removeAttribute(const SAXString& name);
	void clear();
private:
	struct AttributeInfo {
		SAXString _name;
		SAXString _type;
		SAXString _value;
	};
	typedef std::deque<AttributeInfo> AttributeInfoList;

	AttributeInfoList _attributeInfoList;
};

class InputSourceImp : public InputSource {
public:
	InputSourceImp();
	virtual SAXString getPublicId() const;
	virtual void setPublicId(const SAXString& publicId);

	virtual SAXString getSystemId() const;
	virtual void setSystemId(const SAXString& systemId);

	virtual InputStream* getInputStream() const;
	virtual void setInputStream(InputStream* inputStream);
private:
	SAXString _publicId;
	SAXString _systemId;
    
	InputStream* _inputStream;
};


class WBXMLSAXException : virtual public SAXException {
public:
	WBXMLSAXException(SAXString message);
	virtual SAXString getMessage() const;
private:
	SAXString _message;
};

class WBXMLSAXParseException : virtual public SAXParseException, 
	virtual public WBXMLSAXException {
public:
	WBXMLSAXParseException(SAXString message, const Locator& locator);
	WBXMLSAXParseException(SAXString message, SAXString publicId,
		SAXString systemId, int lineNumber, int columnNumber);
	virtual SAXString getPublicId() const;
	virtual SAXString getSystemId() const;
	virtual size_t getLineNumber() const;
	virtual size_t getColumnNumber() const;
private:
	SAXString _publicId;
	SAXString _systemId;
        
	int _lineNumber;
	int _columnNumber;
};

class WBXMLParser : public Parser {
public:
	WBXMLParser();

	virtual void setLocale(const char*);
	virtual void setEntityResolver(EntityResolver* entityResolver);
	virtual void setDTDHandler(DTDHandler* dtdHandler);
	virtual void setDocumentHandler(DocumentHandler* documentHandler);
	virtual void setErrorHandler(ErrorHandler* errorHandler);
	virtual void setWellknownResolver(WellknownResolver* wellknownResolver);
	virtual void setExtensionHandler(ExtensionHandler* extensionHandler);
    
	virtual void parse(const SAXString& systemId);
	virtual void parse(const InputSource* input);

	virtual void asynchId(const SAXString& systemId);
	virtual void asynchPutBlock(const void* buf, size_t len);
	virtual void asynchExplicitlyEnd();
    
public:
	WellknownResolver* getWellknownResolver();
	void openNewContinuation();
	void closeContinuation();
    size_t parse(const byte* data, size_t length);
	size_t dispatch(const byte* data, size_t length);

protected:
	enum ParsingMethod {
		start,
		publicid,
		stringTable,
		body,
		pi,
		element,
		content,
		attribute,
		attrstart,
		attrvalue,
		opaque,
		string,
		extension,
		entity,
		mbuint32,
		termstr,
		done
	};

	struct Continuation {
		struct Element {
			ParsingMethod _method;
			std::vector<unsigned int> _localStates;
			std::vector<byte> _buffer;
		};

		std::stack<Element> _stack;
		size_t _bytesConsumed;
		unsigned int _mbuint32;
		SAXString _termstr;
		SAXString _stringTable;

		std::stack<SAXString> _accumulator;
		AttributeListImp _attributeList;
		bool _noMoreInput;
	};

	Continuation _continuation;
	unsigned int _sessionId;

	size_t parseStart(const byte* data, size_t length);
	size_t parsePublicId(const byte* data, size_t length);
	size_t parseStringTable(const byte* data, size_t length);
	size_t parseBody(const byte* data, size_t length);
	size_t parsePI(const byte* data, size_t length);
	size_t parseElement(const byte* data, size_t length);
	size_t parseContent(const byte* data, size_t length);
	size_t parseAttribute(const byte* data, size_t length);
	size_t parseAttrStart(const byte* data, size_t length);
	size_t parseAttrValue(const byte* data, size_t length);
	size_t parseString(const byte* data, size_t length);
	size_t parseExtension(const byte* data, size_t length);
	size_t parseEntity(const byte* data, size_t length);
	size_t parseOpaque(const byte* data, size_t length);
	size_t parseMbuint32(const byte* data, size_t length);
	size_t parseTermStr(const byte* data, size_t length);

	void resolveTagName(WellknownResolver::Token token, SAXString& tagName);
	void resolveAttributeName(WellknownResolver::Token token,
		SAXString& attributeName, SAXString& attributeValuePrefix);
	void resolveAttributeValue(WellknownResolver::Token token, SAXString& attributeValue);
	void resolveExtension(WellknownResolver::Token token, SAXString& extensionValue);

protected:
	EntityResolver* getEntityResolver();
	DTDHandler* getDTDHandler();
	DocumentHandler* getDocumentHandler();
	ErrorHandler* getErrorHandler();
	ExtensionHandler* getExtensionHandler();
	
private:
	SelfAdjustingDelay _delayOnNoInput;
	
private:
	EntityResolver* _entityResolver;
	DTDHandler* _dtdHandler;
	DocumentHandler* _documentHandler;
	ErrorHandler* _errorHandler;
	WellknownResolver* _wellknownResolver;
	ExtensionHandler* _extensionHandler;
};

#endif
