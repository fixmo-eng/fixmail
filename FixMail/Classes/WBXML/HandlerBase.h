/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Author: Dima Skvortsov
 *
 */

#ifndef __HANDLERBASE_H
#define __HANDLERBASE_H

#include "sax.h"

class HandlerBase : public EntityResolver, public DTDHandler, public DocumentHandler, public ErrorHandler  {
// EntityResolver methods
public:
	virtual void resolveEntity(const SAXString& publicId,
				  const SAXString& systemId,
				  InputSource*& in) { in = NULL; }
// DTDHandler
public:
	virtual void notationDecl(const SAXString& name,
				 const SAXString& publicId,
				 const SAXString& systemId) {}
	virtual void unparsedEntityDecl(const SAXString& name,
				 const SAXString& publicId,
				 const SAXString& systemId,
				 const SAXString& notationName) {}

// DocumentHandler
public:
	virtual void setDocumentLocator(const Locator &locator) {}
	virtual void startDocument() {}
	virtual void endDocument() {}
	virtual void startElement(const SAXString& name, const AttributeList &atts) {}
	virtual void endElement(const SAXString& name) {}
	virtual void characters(const SAXString& chars) {}
	virtual void ignorableWhitespace(const SAXChar* ch, size_t length) {}
	virtual void processingInstruction(const SAXString& target, const SAXString& data) {}

// ErrorHandler methods
public:
	virtual void warning(const SAXParseException& e) {}
	virtual void error(const SAXParseException& e) {}
	virtual void fatalError(const SAXParseException& e) { throw; }
};

#endif
