/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/*
 *  FIXME K9 derived work, need appropriate copyrights here
 *  Tags and strings are derived from K9 Android email client under an Apache license
 */
#import "BaseObject.h"
#import "AS.h"

@class WBXMLRequest;

typedef unsigned char wbyte;

typedef enum  {
    kASTypeDone         = 1,
    kASTypeStart        = 2,
    kASTypeEnd          = 3,
    kASTypeText         = 4,
    kASTypeEOFByte      = -1,
} EASTokenType;

#define kMaxTagDepth    64

@interface FastWBXMLParser : BaseObject {
    void*                   d;
    
    int                     depth;
    
    // The upcoming (saved) id from the stream
    int                     nextId;
    
    // The stack of tags being processed
    int                     startTagArray[kMaxTagDepth];
    
    // The following vars are available to all to avoid method calls that represent the state of
    // the parser at any given time
    int                     startTag;
    int                     endTag;
    
    // The type of the last token read
    EASTokenType            tokenType;
    
    // The current page
    int                     page;
    
    // The current tag
    ASTag                   tag;

    // Whether the current tag is associated with content (a value)
    BOOL                    noContent;
    
    // Current content values
    NSString*               text;
    int                     num;
    NSData*                 data;
    NSData*                 dataReturn;
    NSString*               stringReturn;
    
    // Debug
    BOOL                    debug;

    // The current tag table (i.e. the tag table for the current page)
    //private String[] tagTable;
    const char**            tagTable;
    const char***           pageTable;

    // The stack of names of tags being processed; used when debug = true
    const char*             nameArray[kMaxTagDepth];
    char                    indent[kMaxTagDepth];
}

@property BOOL                  debug;
@property (strong) NSData*      dataReturn;
@property (strong) NSString*    stringReturn;



// Parser Interface
-(void)parse:(NSData*)aData command:(ASTag)aCommand request:(WBXMLRequest*)aRequest;

// Tag Interface
- (ASTag)nextTag:(ASTag)endingTag;
- (void)skipTag;

// Getters
- (NSString*)getString;
- (NSString*)getStringTraceable;

- (NSData*)getData;

- (int)getInt;
- (int)getIntTraceable;

- (unsigned int)getUnsignedInt;
- (unsigned int)getUnsignedIntTraceable;

- (BOOL)getBool;
- (BOOL)getBoolTraceable;

- (int)getIntIsEmpty:(BOOL*)anIsEmpty;
- (unsigned int)getUnsignedIntIsEmpty:(BOOL*)anIsEmpty;

@end
