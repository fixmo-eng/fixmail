/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser developed 
 *       by MediaSite Inc Research and Development Group"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */
#include "WBXMLGenerator.h"

#define AS_PAGE_SHIFT 6
#define AS_PAGE_MASK 0xffffffc0
#define AS_TOKEN_MASK 0x3f

#define SWITCH_PAGE 0x00
#define END 0x01
#define ENTITY 0x02
#define STR_I 0x03
#define LITERAL 0x04
#define EXT_I_0 0x40
#define EXT_I_1 0x41
#define EXT_I_2 0x42
#define PI 0x43
#define LITERAL_C 0x44
#define EXT_T_0 0x80
#define EXT_T_1 0x81
#define EXT_T_2 0x82
#define STR_T 0x83
#define LITERAL_A 0x84
#define EXT_0 0xc0
#define EXT_1 0xc1
#define EXT_2 0xc2
#define OPAQUE 0xc3
#define LITERAL_AC 0xc4

#define TAG_ATTRIBUTES_MASK 0x80
#define TAG_CONTENT_MASK 0x40

static const int    kWBXMLVersion = 0x03;    // Version 1.3


WBXMLGenerator::WBXMLGenerator() 
: _documentStarted(false)
, _page(0)
{
}

void WBXMLGenerator::startDocument1(unsigned int publicid, unsigned int charset, const SAXString& stringTable) {
	ASSERT(_documentStarted == false);
	_documentStarted = true;
	_bodyElementAdded = false;

	SAXString data;
	data = encodeMBUInt32(kWBXMLVersion);
	outputVersion(data.c_str(), data.size());
	data = encodeMBUInt32(publicid);
	outputPublicId(data.c_str(), data.size());
	data = encodeMBUInt32(charset);
	outputCharset(data.c_str(), data.size());
	if (!stringTable.empty()) {
		data = encodeMBUInt32(stringTable.size()+1);
		outputStringTable(data.c_str(), data.size());
		outputStringTable(stringTable.c_str(), stringTable.size()+1);
		_stringTableLength = stringTable.size()+1;
	} else {
		data = encodeMBUInt32(0);
		outputStringTable(data.c_str(), data.size());
		_stringTableLength = 0;
	}
}

void WBXMLGenerator::startDocument2(unsigned int publicidIndex, unsigned int charset, const SAXString& stringTable) {
	ASSERT(_documentStarted == false);
	_documentStarted = true;
	_bodyElementAdded = false;

	ASSERT(publicidIndex < stringTable.size());
	if (publicidIndex < stringTable.size()) {
		SAXString data;
		data = encodeMBUInt32(1);
		outputVersion(data.c_str(), data.size());
		data.assign(1, (SAXChar)0);
		outputPublicId(data.c_str(), data.size());
		data = encodeMBUInt32(publicidIndex);
		outputPublicId(data.c_str(), data.size());
		data = encodeMBUInt32(charset);
		outputCharset(data.c_str(), data.size());
		if (!stringTable.empty()) {
			data = encodeMBUInt32(stringTable.size()+1);
			outputStringTable(data.c_str(), data.size());
			outputStringTable(stringTable.c_str(), stringTable.size()+1);
			_stringTableLength = stringTable.size();
		} else {
			data = encodeMBUInt32(0);
			outputStringTable(data.c_str(), data.size());
			_stringTableLength = 0;
		}
	} else
		startDocument1(1, charset, stringTable);
}

void WBXMLGenerator::endDocument() {
	ASSERT(_documentStarted == true);
	ASSERT(_bodyElementAdded == true);
	ASSERT(_elementDescriptionStack.size() == 0);
	_documentStarted = false;
}

void WBXMLGenerator::startElement1(WellknownResolver::Tag tag, bool attributes, bool content) {
	ASSERT(_documentStarted == true);

	if (_elementDescriptionStack.size() > 0) {
		ASSERT(_elementDescriptionStack.top()._content == true);
		if (_elementDescriptionStack.top()._attributes)
			ASSERT(_elementDescriptionStack.top()._attributesFinished);
	} else {
		ASSERT(_bodyElementAdded == false);
		_bodyElementAdded = true;
	}

	ElementDescription elementDescription;

    // Break tag in to code page and token
    //
    unsigned int page = tag & AS_PAGE_MASK;
    page = page >> AS_PAGE_SHIFT;
    if(page != _page) {
        outputByte(SWITCH_PAGE);
        outputByte(page);
        _page = page;
    }
    
    WellknownResolver::Token token = tag & AS_TOKEN_MASK;
	ASSERT(token < 0x3f);
	ASSERT(token > 4);

	elementDescription._token = token;
	elementDescription._attributes = attributes;
	elementDescription._content = content;
	elementDescription._attributesFinished = false;
	elementDescription._contentFinished = false;

	_elementDescriptionStack.push(elementDescription);

	if (attributes)
		token |= TAG_ATTRIBUTES_MASK;
	if (content)
		token |= TAG_CONTENT_MASK;

	outputBody(&token, 1);
}

void WBXMLGenerator::startElement2(unsigned int index, bool attributes, bool content) {
	ASSERT(_documentStarted == true);

	if (_elementDescriptionStack.size() > 0) {
		ASSERT(_elementDescriptionStack.top()._content == true);
		if (_elementDescriptionStack.top()._attributes)
			ASSERT(_elementDescriptionStack.top()._attributesFinished);
	} else {
		ASSERT(_bodyElementAdded == false);
		_bodyElementAdded = true;
	}

	ElementDescription elementDescription;

	ASSERT(index < _stringTableLength);

	byte token = LITERAL;
	elementDescription._token = token;
	elementDescription._attributes = attributes;
	elementDescription._content = content;
	elementDescription._attributesFinished = false;
	elementDescription._contentFinished = false;

	_elementDescriptionStack.push(elementDescription);

	if (attributes)
		token |= TAG_ATTRIBUTES_MASK;
	if (content)
		token |= TAG_CONTENT_MASK;

	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::endElement() {
	ASSERT(_documentStarted == true);
	ASSERT(_elementDescriptionStack.size() > 0);
	ASSERT(_elementDescriptionStack.top()._token > 0x04);

	if (_elementDescriptionStack.top()._attributes) {
		ASSERT(_elementDescriptionStack.top()._attributesFinished);
	}
	if (_elementDescriptionStack.top()._content) {
		ASSERT(_elementDescriptionStack.top()._contentFinished);
	}

	_elementDescriptionStack.pop();
}

void WBXMLGenerator::startProcessingInstruction() {
	ASSERT(_documentStarted == true);

	if (_elementDescriptionStack.size() > 0) {
		ASSERT(_elementDescriptionStack.top()._content == true);
		if (_elementDescriptionStack.top()._attributes)
			ASSERT(_elementDescriptionStack.top()._attributesFinished);
	}

	ElementDescription elementDescription;

	elementDescription._token = PI;
	elementDescription._attributes = true;
	elementDescription._content = false;
	elementDescription._attributesFinished = false;
	elementDescription._contentFinished = false;

	_elementDescriptionStack.push(elementDescription);

	byte pi = PI;
	outputBody(&pi, 1);
}

void WBXMLGenerator::endProcessingInstruction() {
	ASSERT(_documentStarted == true);
	ASSERT(_elementDescriptionStack.size() > 0);
	ASSERT(_elementDescriptionStack.top()._token == PI);
	ASSERT(_elementDescriptionStack.top()._attributes == true);
	ASSERT(_elementDescriptionStack.top()._content == false);
	ASSERT(_elementDescriptionStack.top()._attributesFinished == true);
	ASSERT(_elementDescriptionStack.top()._contentFinished == false);

	byte end = END;
	outputBody(&end, 1);
}

#define ATTRIBUTE_PRECONDITION 	\
	ASSERT(_documentStarted == true); \
	ASSERT(_elementDescriptionStack.size() > 0); \
	ASSERT(_elementDescriptionStack.top()._attributes == true); \
	ASSERT(_elementDescriptionStack.top()._attributesFinished == false)

void WBXMLGenerator::startAttributes() {
	ATTRIBUTE_PRECONDITION;
}

void WBXMLGenerator::addAttribute1(WellknownResolver::Token attrstart) {
	ATTRIBUTE_PRECONDITION;
	ASSERT(attrstart < 0x80);

	outputBody(&attrstart, 1);
}

void WBXMLGenerator::addAttribute2(unsigned int index) {
	ATTRIBUTE_PRECONDITION;
	ASSERT(index < _stringTableLength);

	byte token = LITERAL;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}
void WBXMLGenerator::addAttributeValue(WellknownResolver::Token attrvalue) {
	ATTRIBUTE_PRECONDITION;
	ASSERT(attrvalue > 0x84);

	outputBody(&attrvalue, 1);
}

void WBXMLGenerator::addAttributeValueString1(unsigned int index) {
	ATTRIBUTE_PRECONDITION;
	ASSERT(index < _stringTableLength);

	byte token = STR_T;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::addAttributeValueString2(const SAXString& termstr) {
	ATTRIBUTE_PRECONDITION;

	byte token = STR_I;
	outputBody(&token, 1);
	outputBody(termstr.c_str(), strlen(termstr.c_str())+1);
}

void WBXMLGenerator::addAttributeValueExtension1(unsigned int code, const SAXString& termstr) {
	ATTRIBUTE_PRECONDITION;

	byte token;
	switch (code) {
	case 0:
		token = EXT_I_0;
		break;
	case 1:
		token = EXT_I_1;
		break;
	case 2:
		token = EXT_I_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
	outputBody(termstr.c_str(), strlen(termstr.c_str())+1);
}

void WBXMLGenerator::addAttributeValueExtension2(unsigned int code, unsigned int index) {
	ATTRIBUTE_PRECONDITION;
	ASSERT(index < _stringTableLength);

	byte token;
	switch (code) {
	case 0:
		token = EXT_T_0;
		break;
	case 1:
		token = EXT_T_1;
		break;
	case 2:
		token = EXT_T_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::addAttributeValueExtension3(unsigned int code) {
	ATTRIBUTE_PRECONDITION;

	byte token;
	switch (code) {
	case 0:
		token = EXT_0;
		break;
	case 1:
		token = EXT_1;
		break;
	case 2:
		token = EXT_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
}
void WBXMLGenerator::addAttributeValueEntity(unsigned int entcode) {
	ATTRIBUTE_PRECONDITION;

	byte token = ENTITY;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(entcode);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::endAttributes() {
	ATTRIBUTE_PRECONDITION;
	_elementDescriptionStack.top()._attributesFinished = true;

	byte end = END;
	outputBody(&end, 1);
}

#define CONTENT_PRECONDITION \
	ASSERT(_documentStarted == true); \
	ASSERT(_elementDescriptionStack.size() > 0); \
	if (_elementDescriptionStack.top()._attributes == true) \
		ASSERT(_elementDescriptionStack.top()._attributesFinished == true); \
	ASSERT(_elementDescriptionStack.top()._content == true); \
	ASSERT(_elementDescriptionStack.top()._contentFinished == false)

void WBXMLGenerator::startContent() {
	CONTENT_PRECONDITION;
}

void WBXMLGenerator::addContentValueString1(unsigned int index) {
	CONTENT_PRECONDITION;
	ASSERT(index < _stringTableLength);

	byte token = STR_T;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::addContentValueString2(const SAXString& termstr) {
	CONTENT_PRECONDITION;

	byte token = STR_I;
	outputBody(&token, 1);
	outputBody(termstr.c_str(), strlen(termstr.c_str())+1);
}

void WBXMLGenerator::addContentValueExtension1(unsigned int code, const SAXString& termstr) {
	CONTENT_PRECONDITION;

	byte token;
	switch (code) {
	case 0:
		token = EXT_I_0;
		break;
	case 1:
		token = EXT_I_1;
		break;
	case 2:
		token = EXT_I_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
	outputBody(termstr.c_str(), strlen(termstr.c_str())+1);
}

void WBXMLGenerator::addContentValueExtension2(unsigned int code, unsigned int index) {
	CONTENT_PRECONDITION;

	ASSERT(index < _stringTableLength);

	byte token;
	switch (code) {
	case 0:
		token = EXT_T_0;
		break;
	case 1:
		token = EXT_T_1;
		break;
	case 2:
		token = EXT_T_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(index);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::addContentValueExtension3(unsigned int code) {
	CONTENT_PRECONDITION;

	byte token;
	switch (code) {
	case 0:
		token = EXT_0;
		break;
	case 1:
		token = EXT_1;
		break;
	case 2:
		token = EXT_2;
		break;
	default:
		ASSERT(!"Unknown code");
		break;
	};
	outputBody(&token, 1);
}

void WBXMLGenerator::addContentValueEntity(unsigned int entcode) {
	CONTENT_PRECONDITION;

	byte token = ENTITY;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(entcode);
	outputBody(data.c_str(), data.size());
}

void WBXMLGenerator::addContentValueOpaque(const void* bytes, unsigned int length) {
	CONTENT_PRECONDITION;

	byte token = OPAQUE;
	outputBody(&token, 1);
	SAXString data;
	data = encodeMBUInt32(length);
	outputBody(data.c_str(), data.size());
	outputBody(bytes, length);
}

void WBXMLGenerator::endContent() {
	CONTENT_PRECONDITION;

	_elementDescriptionStack.top()._contentFinished = true;
	byte end = END;
	outputBody(&end, 1);
}

const SAXString WBXMLGenerator::encodeMBUInt32(unsigned int number) {
	ASSERT(sizeof(number) == 4);

	if (number == 0) {
		SAXString toReturn;
		toReturn.assign(1, (SAXChar)0);
		return toReturn;
	}
	
	byte buffer[5];
	byte* bufferPtr = buffer;
	bool firstPass = true;
	while (number > 0x00) {
		*bufferPtr = number & 0x7f;
		if (!firstPass)
			*bufferPtr |= 0x80;
		else
			firstPass = false;
		bufferPtr++;
		number >>= 7;
	}

	SAXString result;
	result.reserve(bufferPtr - buffer);
	for (byte* it = bufferPtr-1; it != buffer-1; it-- )
		result += *it;

	return result;	
}
