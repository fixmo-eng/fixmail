/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/*
 * FIXME Derivative work from Sybyx2 example
 */
#ifndef WellknownResolverer_h
#define WellknownResolverer_h

typedef unsigned char byte;
typedef char SAXChar;

#if defined(RIM) || defined(__MWERKS__) || defined(__PRCTOOLS__)
#include "min_basic_string.h"
typedef min_basic_string SAXString;
#else
#include <string>
typedef std::basic_string<SAXChar> SAXString;
#endif

#include <assert.h>
#define ASSERT(x) assert(x)

extern const SAXString saxStringNull;
extern const SAXString publicIdNull;

class WellknownResolver {
public:
    WellknownResolver() : m_page(0) {}
    
public:
	typedef byte Token;
    typedef unsigned int Tag;
    typedef unsigned int Page;
	virtual void resolveTagName(Token token, char* tagName, size_t tagNameLength) = 0;
	virtual void resolveAttributeName(Token token,
                                      char* attributeName, size_t attributeNameLength,
                                      char* attributeValuePrefix, size_t attributeValuePrefixLength) = 0;
	virtual void resolveAttributeValue(Token token, char* attributeValue, size_t attributeValueLength) = 0;
    
public:
    void setPage(Page page)     { m_page = page; }
    Page page() const           { return m_page; }
    
private:
    Page                 m_page;
};

#endif
