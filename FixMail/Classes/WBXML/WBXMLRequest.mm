/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "WBXMLRequest.h"
#include "StringUtil.h"
#import "AS.h"
#import "ASAccount.h"
#import "ASSyncObject.h"
#import "TraceEngine.h"
#import "WBXMLQueueRequest.h"

#ifdef USE_SYBYX
#include "WBXMLHandlerBase.h"
#include "WBXMLDataGenerator.h"
#include "HandlerBase.h"
#endif

@implementation WBXMLRequest

@synthesize command;
@synthesize commandTag;
@synthesize displayErrors;

// Globals
extern BOOL gIsFixTrace;

////////////////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

#ifdef DEBUG
static BOOL kDebugTraceWBXMLRequests     = TRUE;
static BOOL kDebugTraceWBXMLResponses    = TRUE;

static NSString* kWBXMLDebugFile    = @"wbxmlDebug";
#else
static BOOL kDebugTraceWBXMLRequests     = FALSE;
static BOOL kDebugTraceWBXMLResponses    = FALSE;
#endif

+ (BOOL)traceWBXMLRequests
{
    return kDebugTraceWBXMLRequests;
}

+ (void)setTraceWBXMLRequests:(BOOL)aTraceRequests
{
    kDebugTraceWBXMLRequests = aTraceRequests;
}

+ (BOOL)traceWBXMLResponses
{
    return kDebugTraceWBXMLResponses;
}

+ (void)setTraceWBXMLResponses:(BOOL)aTraceResponses
{
    kDebugTraceWBXMLResponses = aTraceResponses;
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Request Queue
//
// ActiveSync uses a sync key on each HTTP request on a collection(folder) so the client and
// server are in agreement on the state of the folder at the time the request is made.  It is
// critical that no more than one HTTP sync request from the client use the current sync key.
// If two request use the same key the server is almost certain to respond with an invalid
// sync key error and the state of the folder will land in an undefined state.  A full initial
// sync may be required to right things
//
// You might be able to disable this queue and not have problem, but the invalid sync key error
// may still occur depending on the test setup.  It likely that it will most probably occur if
// have a slow connection to the server so it takes a long time for the HTTP response to return to
// the client increasing the change of overlapping requests with the same key.
//
// The following code puts all ActiveSync requests which use a sync key in to an NSOperationQueue
// The top request on the queue will call wait when the request is sent and then block on a
// conditional.  This stops requests behind it in the queue from being sent.  When the HTTP request
// completes and the new sync key has been received from the server, connecitonRelease will be
// called and the conditional will be released.  The syncObject will wake up and remove itself
// from the operation queue allowing the next request to be sent.
//
// Requests which don't use a sync key like ASPing will not set the isQueued flag and they will
// not use this queueing mechanism
//
// Currently all sync requests are run through one queue.  This is a less complex approach but
// it may cause network performance issues since only one request can be sent at a time.  if
// this becomes and issue then this queue could be move to ASFolder so you have a queue for each
// each folder.  Then you will just need to insure that no sync requests are issued which
// reference more than one folder.
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Request Queue

static NSOperationQueue* sOperationQueue = nil;
static NSCondition* sCondition = nil;

+ (NSOperationQueue*)operationQueue
{
    if(!sOperationQueue) {
        sOperationQueue = [[NSOperationQueue alloc] init];
        [sOperationQueue setMaxConcurrentOperationCount:1];
        sCondition = [[NSCondition alloc] init];
    }
    return sOperationQueue;
}

+ (NSUInteger)operationQueueCount
{
   return sOperationQueue.operations.count;
}

+ (WBXMLRequest*)queuedRequest
{
    WBXMLRequest* aWBXMLRequest = nil;
    
    NSOperationQueue* anOperationQueue = [WBXMLRequest operationQueue];
    NSArray* anOperations = [anOperationQueue operations];
    if(anOperations.count > 0) {
        NSInvocationOperation* anOp = [anOperations objectAtIndex:0];
        NSObject* anObject = anOp.invocation.target;
        if([anObject isKindOfClass:[ASSyncObject class]]) {
            ASSyncObject* aSyncObject = (ASSyncObject*)anObject;
            aWBXMLRequest = aSyncObject.syncRequest;
        }else if([anObject isKindOfClass:[WBXMLQueueRequest class]]) {
            WBXMLQueueRequest* aWBXMLQueueRequest = (WBXMLQueueRequest*)anObject;
            aWBXMLRequest = aWBXMLQueueRequest.wbxmlRequest;
        }else{
            FXDebugLog(kFXLogWBXML, @"queuedRequest invalid: %@", anOp);
        }
    }else{
        FXDebugLog(kFXLogWBXML, @"queuedRequest no requests queued");
    }
    return aWBXMLRequest;
}

+ (void)wait
{
    // Block waiting for connectionRelease to be called.  It will call signal
    // and wake us up.  When we return the operation will be removed from the
    // queue and the next request will be sent
    //
    [sCondition lock];
    [sCondition wait];
    [sCondition unlock];
}

+ (void)deleteQueue
{
    WBXMLRequest* aWBXMLRequest = [WBXMLRequest queuedRequest];

    if(sOperationQueue) {
        [sOperationQueue cancelAllOperations];
    }
    
    if(aWBXMLRequest) {
        [aWBXMLRequest cancel];
        [aWBXMLRequest _signal];
    }
}

- (void)setIsQueued
{
    // Only ActiveSync requests which use a sync key will call this and be queued.
    // Requests which don't use a sync key, like ASPing will not call this
    //
    isQueued = TRUE;
}

- (void)connectionRelease:(HttpConnection*)connection
{
    // Called when the HTTP request has completed and the sync key has been update
    //
    [super connectionRelease:connection];
    
    [self _signal];
}

- (void)_signal
{
    if(isQueued) {
        // If this is the only sync request in queue, restart the ASPing
        //
        NSOperationQueue* anOperationQueue = [WBXMLRequest operationQueue];
        NSArray* anOperations = [anOperationQueue operations];
        if(anOperations.count == 1) {
            WBXMLRequest* aRequest = [WBXMLRequest queuedRequest];
            if(aRequest) {
                ASAccount* anAccount = (ASAccount*)aRequest.account;
                if(anAccount.isInitialSynced) {
                    [anAccount startPing];
                }
            }else{
#ifdef DEBUG
                NSInvocationOperation* anOp = [anOperations objectAtIndex:0];
                NSObject* anObject = anOp.invocation.target;
                FXDebugLog(kFXLogFIXME, @"Invalid queued WBXMLRequest: %@", anObject);
#endif
            }
        }
        
        // If this is a queued request with a sync key unblock the request thread in wait
        // above and let it finish and remove itsself from the queue
        //
        [sCondition signal];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ASAccount *)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
        data = [[NSMutableData alloc] init];
        isQueued = FALSE;
    }
    return self;
}

- (id)initWithAccount:(ASAccount *)anAccount command:(NSString*)aCommand commandTag:(ASTag)aTag
{
    if (self = [super initWithAccount:anAccount]) {
        data = [[NSMutableData alloc] init];
        self.command    = aCommand;
        self.commandTag = aTag;
        isQueued = FALSE;
    }
    return self;
}

- (void)dealloc
{
    if(data) {
         data = NULL;
    }

    if(isQueued) {
        //FXDebugLog(kFXLogWBXML, @"WBXMLRequest remove from queue: %d", sOperationQueue.operationCount);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// ActiveSync Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ActiveSync Error Handling

- (void)handleHttpErrorForAccount:(ASAccount*)anAccount
                       connection:(HttpConnection*)connection
                         response:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
    switch(self.statusCode) {
        case kHttpNeedsProvisioning:
            [self.account needsProvisioning:self];
            break;
        case kHttpForbidden:        // This is sent if your policy key is bad, but other stuff will send it too
            [HttpRequest handleASError:[NSHTTPURLResponse localizedStringForStatusCode:self.statusCode]];
            break;
        default:
            [self handleHttpError];
            break;
    }
}

- (NSString*)handleActiveSyncError:(EActiveSyncStatus)aSyncStatus
{
    NSString* anErrorNessage = @"";
    switch(aSyncStatus) {
#ifdef DEBUG
        case kASInvalidContent:
            anErrorNessage = [HttpRequest handleASError:@"Invalid Content"]; break;
        case kASInvalidWBXML:
            anErrorNessage = [HttpRequest handleASError:@"Invalid WBXML"]; break;
        case kASInvalidXML:
            anErrorNessage = [HttpRequest handleASError:@"Invalid XML"]; break;
        case kASInvalidDateTime:
            anErrorNessage = [HttpRequest handleASError:@"Invalid DateTime"]; break;
        case kASInvalidCombinationOfIDs:
            anErrorNessage = [HttpRequest handleASError:@"Invalid Combination Of IDs"]; break;
        case kASInvalidIDs:
            anErrorNessage = [HttpRequest handleASError:@"Invalid IDs"]; break;
        case kASInvalidMIME:
            anErrorNessage = [HttpRequest handleASError:@"Invalid MIME"]; break;
        case kASDeviceIdMissingOrInvalid:
            anErrorNessage = [HttpRequest handleASError:@"Device ID Missing Or Invalid"]; break;
        case kASDeviceTypeMissingOrInvalid:
            anErrorNessage = [HttpRequest handleASError:@"Device Type Missing Or Invalid"]; break;
        case kASServerError:
            anErrorNessage = [HttpRequest handleASError:@"Server Error"]; break;
        case kASServerErrorRetryLater:
            anErrorNessage = [HttpRequest handleASError:@"Server Error Retry Later"]; break;
        case kASActiveDirectoryAccessDenied:
            anErrorNessage = [HttpRequest handleASError:@"Active Directory Access Denied"]; break;
        case kASMailboxQuotaExceeded:
            anErrorNessage = [HttpRequest handleASError:@"Mailbox Quota Exceeded"]; break;
        case kASMailboxServerOffline:
            anErrorNessage = [HttpRequest handleASError:@"Mailbox Server Offline"]; break;
        case kASSendQuotaExceeded:
            anErrorNessage = [HttpRequest handleASError:@"Send Quota Exceeded"]; break;
        case kASMessageRecipientUnresolved:
            anErrorNessage = [HttpRequest handleASError:@"Message Recipient Unresolved"]; break;
        case kASMessageReplyNotAllowed:
            anErrorNessage = [HttpRequest handleASError:@"Message Reply Not Allowed"]; break;
        case kASMessagePreviouslySent:
            anErrorNessage = [HttpRequest handleASError:@"Message Previously Sent"]; break;
        case kASMessageHasNoRecipient:
            anErrorNessage = [HttpRequest handleASError:@"Message Has No Recipient"]; break;
        case kASMailSubmissionFailed:
            anErrorNessage = [HttpRequest handleASError:@"Mail Submission Failed"]; break;
        case kASMessageReplyFailed:
            anErrorNessage = [HttpRequest handleASError:@"Message Reply Failed"]; break;
        case kASAttachmentIsTooLarge:
            anErrorNessage = [HttpRequest handleASError:@"Attachment Is Too Large"]; break;
        case kASUserHasNoMailbox:
            anErrorNessage = [HttpRequest handleASError:@"User Has No Mailbox"]; break;
        case kASUserCannotBeAnonymous:
            anErrorNessage = [HttpRequest handleASError:@"User Cannot Be Anonymous"]; break;
        case kASUserPrincipalCouldNotBeFound:
            anErrorNessage = [HttpRequest handleASError:@"User Principal Could Not Be Found"]; break;
        case kASUserDisabledForSync:
            anErrorNessage = [HttpRequest handleASError:@"User Disabled For Sync"]; break;
        case kASUserOnNewMailboxCannotSync:
            anErrorNessage = [HttpRequest handleASError:@"User On New Mailbox Cannot Sync"]; break;
        case kASUserOnLegacyMailboxCannotSync:
            anErrorNessage = [HttpRequest handleASError:@"User On Legacy Mailbox Cannot Sync"]; break;
        case kASDeviceIsBlockedForThisUser:
            anErrorNessage = [HttpRequest handleASError:@"Device Is Blocked For This User"]; break;
        case kASAccessDenied:
            anErrorNessage = [HttpRequest handleASError:@"Access Denied"]; break;
        case kASAccountDisabled:
            anErrorNessage = [HttpRequest handleASError:@"Account Disabled"]; break;
        case kASSyncStateNotFound:
            anErrorNessage = [HttpRequest handleASError:@"Sync State Not Found"]; break;
        case kASSyncStateLocked:
            anErrorNessage = [HttpRequest handleASError:@"Sync State Locked"]; break;
        case kASSyncStateCorrupt:
            anErrorNessage = [HttpRequest handleASError:@"Sync State Corrupt"]; break;
        case kASSyncStateAlreadyExists:
            anErrorNessage = [HttpRequest handleASError:@"Sync State Already Exists"]; break;
        case kASSyncStateVersionInvalid:
            anErrorNessage = [HttpRequest handleASError:@"Sync State Version Invalid"]; break;
        case kASCommandNotSupported:
            anErrorNessage = [HttpRequest handleASError:@"Command Not Supported"]; break;
        case kASVersionNotSupported:
            anErrorNessage = [HttpRequest handleASError:@"Version Not Supported"]; break;
        case kASDeviceNotFullyProvisionable:
            anErrorNessage = [HttpRequest handleASError:@"Device Not Fully Provisionable"]; break;
        case kASRemoteWipeRequested:
            anErrorNessage = [HttpRequest handleASError:@"Remote Wipe Requested"]; break;
        case kASLegacyDeviceOnStrictPolicy:
            anErrorNessage = [HttpRequest handleASError:@"Legacy Device On Strict Policy"]; break;
        case kASDeviceNotProvisioned:
            [self.account needsProvisioning:self]; break;
        case kASPolicyRefresh:
            [self.account needsProvisioning:self]; break;
        case kASInvalidPolicyKey:
            [self.account needsProvisioning:self]; break;
        case kASExternallyManagedDevicesNotAllowed:
            anErrorNessage = [HttpRequest handleASError:@"Externally Managed Devices Not Allowed"]; break;
        case kASNoRecurrenceInCalendar:
            anErrorNessage = [HttpRequest handleASError:@"No Recurrence In Calendar"]; break;
        case kASUnexpectedItemClass:
            anErrorNessage = [HttpRequest handleASError:@"Unexpected Item Class"]; break;
        case kASRemoteServerHasNoSSL:
            anErrorNessage = [HttpRequest handleASError:@"Remote Server Has No SSL"]; break;
        case kASInvalidStoredRequest:
            anErrorNessage = [HttpRequest handleASError:@"Invalid Stored Request"]; break;
        case kASItemNotFound:
            anErrorNessage = [HttpRequest handleASError:@"Item Not Found"]; break;
        case kASTooManyFolders:
            anErrorNessage = [HttpRequest handleASError:@"Too Many Folders"]; break;
        case kASNoFoldersFound:
            anErrorNessage = [HttpRequest handleASError:@"No Folders Found"]; break;
        case kASItemLostAfterMove:
            anErrorNessage = [HttpRequest handleASError:@"Item Lost After Move"]; break;
        case kASFailureInMoveOperation:
            anErrorNessage = [HttpRequest handleASError:@"Failure In Move Operation"]; break;
        case kASMoveCommandDisallowedForNonPersist:
            anErrorNessage = [HttpRequest handleASError:@"Move Command Disallowed For Non Persistent Move Action"]; break;
        case kASMoveCommandInvalidDestinationFolder:
            anErrorNessage = [HttpRequest handleASError:@"Move Command Invalid Destination Folder"]; break;
        case kASUndefined157:
            anErrorNessage = [HttpRequest handleASError:@"Undefined 157"]; break;
        case kASUndefined158:
            anErrorNessage = [HttpRequest handleASError:@"Undefined 158"]; break;
        case kASUndefined159:
            anErrorNessage = [HttpRequest handleASError:@"Undefined 159"]; break;
        case kASAvailabilityTooManyRecipients:
            anErrorNessage = [HttpRequest handleASError:@"Availability Too Many Recipient"]; break;
        case kASAvailabilityDLLimitReached:
            anErrorNessage = [HttpRequest handleASError:@"Availability DL Limit Reached"]; break;
        case kASAvailabilityTransientFailure:
            anErrorNessage = [HttpRequest handleASError:@"Availability Transient Failure"]; break;
        case kASAvailabilityFailure:
            anErrorNessage = [HttpRequest handleASError:@"Availability Failure"]; break;
        case kASBodyPartPreferenceTypeNotSupported:
            anErrorNessage = [HttpRequest handleASError:@"Body Part Preference Type Not Supported"]; break;
        case kASDeviceInformationRequired:
            anErrorNessage = [HttpRequest handleASError:@"Device Information Required"]; break;
        case kASInvalidAccountId:
            anErrorNessage = [HttpRequest handleASError:@"Invalid Account Id"]; break;
        case kASAccountSendDisabled:
            anErrorNessage = [HttpRequest handleASError:@"Account Send Disabled"]; break;
        case kASIRM_FeatureDisabled:
            anErrorNessage = [HttpRequest handleASError:@"IRM Feature Disabled"]; break;
        case kASIRM_TransientError:
            anErrorNessage = [HttpRequest handleASError:@"IRM Transient Error"]; break;
        case kASIRM_PermanentError:
            anErrorNessage = [HttpRequest handleASError:@"IRM Permanent Error"]; break;
        case kASIRM_InvalidTemplateID:
            anErrorNessage = [HttpRequest handleASError:@"IRM Invalid Template ID"]; break;
        case kASIRM_OperationNotPermitted:
            anErrorNessage = [HttpRequest handleASError:@"IRM Operation Not Permitted"]; break;
        case kASNoPicture:
            anErrorNessage = [HttpRequest handleASError:@"No Picture"]; break;
        case kASPictureTooLarge:
            anErrorNessage = [HttpRequest handleASError:@"Picture Too Large"]; break;
        case kASPictureLimitReached:
            anErrorNessage = [HttpRequest handleASError:@"Picture Limit Reached"]; break;
        case kASBodyPart_ConversationTooLarge:
            anErrorNessage = [HttpRequest handleASError:@"Body Part Conversation Too Large"]; break;
        case kASMaximumDevicesReached:
            anErrorNessage = [HttpRequest handleASError:@"Maximum Devices Reached"]; break;

        default:
            anErrorNessage = [HttpRequest handleASError:[NSString stringWithFormat:@"ActiveSync unknown error: %d", aSyncStatus]];
            break;
#else
		default:
            anErrorNessage = [HttpRequest handleASError:[NSString stringWithFormat:@"%@ %d", FXLLocalizedStringFromTable(@"ACTIVESYNC_ERROR", @"ErrorAlert", @"Error alert message for ActiveSync error"), aSyncStatus]];
            break;
#endif
			
    }
    return anErrorNessage;
}

- (void)queue
{
    [self setIsQueued];
    WBXMLQueueRequest* aWBXMLQueueRequest = [[WBXMLQueueRequest alloc] init];
    aWBXMLQueueRequest.wbxmlRequest = self;
    [aWBXMLQueueRequest queue];
}

- (void)success
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kActiveSyncSuccessNotification
                                                        object:self
                                                      userInfo:nil];
}

- (void)fail
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kActiveSyncFailNotification
                                                        object:self
                                                      userInfo:nil];
    if(self.displayErrors) {
        [self displayError];
    }
}


////////////////////////////////////////////////////////////////////////////////////////////
// WBXMLRequest virtual
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXMLRequest virtual

// Must be overridden and implemented
- (void)displayError
{
    FXDebugLog(kFXLogFIXME, @"WBXMLRequest displayError not overridden");
}

- (void)send
{
    NSAssert(0, @"WBXMLRequest send not overridden");

}

////////////////////////////////////////////////////////////////////////////////////////////
// HttpRequest override
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark HttpRequest override

- (void)retry
{
    [data setLength:0];
    [super retry];
}

////////////////////////////////////////////////////////////////////////////////////////////
// NSURLConnection delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark NSURLConnection delegate

- (void)connection:(HttpConnection*)connection didReceiveData:(NSData *)aData
{
    [data appendData:aData];
}

- (void)connection:(HttpConnection*)connection didReceiveResponse:(NSURLResponse*)response
{
    NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
    self.statusCode = (EHttpStatusCode)[resp statusCode];
	
    if(self.statusCode == kHttpOK) {
        NSDictionary* aHeaders = [resp allHeaderFields];
        //FXDebugLog(kFXLogActiveSync, @"%@ didReceiveResponse: %d headers: %@", kCommand, statusCode, aHeaders);
        self.contentType = [aHeaders objectForKey:@"Content-Type"];
        if([self.contentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
            
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML", self.command);
        }
    }else{
        [self handleHttpErrorForAccount:self.account connection:connection response:response];
    }
}

- (void)connectionDidFinishLoading:(HttpConnection*)connection
{
    if(self.commandTag > 0) {
        NSUInteger aLength = [data length];
        if(self.statusCode == kHttpOK && aLength > 0) {
            if([self.contentType isEqualToString:@"application/vnd.ms-sync.wbxml"]) {
                [self parseWBXMLData:data command:self.commandTag];
            }else{
                FXDebugLog(kFXLogActiveSync, @"%@ response is not WBXML\n%@", self.command,
                           [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
        }else{
            FXDebugLog(kFXLogActiveSync, @"%@ failed: %d", self.command, self.statusCode);
        }
    }
    [super connectionDidFinishLoading:connection];
}

////////////////////////////////////////////////////////////////////////////////
//
// Parse WBXML - The main fast parser all AS commands use to parse AS responses
//
////////////////////////////////////////////////////////////////////////////////
#pragma mark Parse WBXML

- (void)parseWBXMLData:(NSData*)aData command:(ASTag)aCommandTag
{
    if([aData length] > 0) {
        FastWBXMLParser* aParser = [[FastWBXMLParser alloc] init];
        [aParser setDebug:kDebugTraceWBXMLResponses];
        if(kDebugTraceWBXMLResponses) {
            FXDebugLog(kFXLogWBXML, @"ActiveSync response\n");
        }
        [aParser parse:aData command:aCommandTag request:self];
    }else{
        FXDebugLog(kFXLogWBXML, @"parseWBXMLData no WBXML data 0x%x", aCommandTag);
    }
}

////////////////////////////////////////////////////////////////////////////////
// Fast Parser
////////////////////////////////////////////////////////////////////////////////
#pragma mark Fast Parser

// Override
- (void)fastParser:(FastWBXMLParser*)aParser
{
    
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

#ifdef DEBUG

// These were used to store and load WBXML from a file during early development
// They probably aren't very useful now unless you have a hard problem to debug
// and want to debug on the same data over and over
//
+ (void)storeDebugData:(NSData*)aData
{
    NSString* aFilePath = [StringUtil filePathInDocumentsDirectoryForFileName:kWBXMLDebugFile];


    if([[NSFileManager defaultManager] fileExistsAtPath:aFilePath]) {
    }
    
    [aData writeToFile:aFilePath atomically:FALSE];
}

+ (NSData*)loadDebugData
{
    NSData* aData = NULL;
    
    NSString* filePath = [StringUtil filePathInDocumentsDirectoryForFileName:kWBXMLDebugFile];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        aData = [[NSData alloc] initWithContentsOfFile:filePath];
    }
    
    return aData;
}

#endif



#ifdef USE_SYBYX

////////////////////////////////////////////////////////////////////////////////
//
// WBXML Parser - Only used for debug traces of WBXML requests if enabled
// with #define USE_SYBYX and kDebugTraceWBXMLRequests is TRUE
//
////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Parser

/*
- (void)setParserActions:(EParserAction*)aParserStartActions endActions:(EParserAction*)aParserEndActions
{
	return;

    EParserAction* anAction = aParserStartActions;
    while(anAction->tag) {
        ParserAction* aParserAction = [[ParserAction alloc] initWithAction:anAction->selector 
                                                                   argType:anAction->argType 
                                                              childActions:NULL];
        [parserStartActions setObject:aParserAction forKey:anAction->tag];
        anAction++;
    }
    
    anAction = aParserEndActions;
    while(anAction->tag) {
        ParserAction* aParserAction = [[ParserAction alloc] initWithAction:anAction->selector 
                                                                   argType:anAction->argType 
                                                              childActions:NULL];
        [parserEndActions setObject:aParserAction forKey:anAction->tag];
        anAction++;
    }

}
*/

////////////////////////////////////////////////////////////////////////////////////////////
// WBXMLParserDelegate delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXMLParserDelegate delegate

- (void)startDocument
{
}

- (void)endDocument
{
}

- (void)startElement:(NSString*)aTag attributes:(NSDictionary*)anAttributes
{
    ParserAction* aParserAction = [parserStartActions objectForKey:aTag];
    if(aParserAction) {
        [aParserAction dispatchStart:self];
    }
}

- (void)endElement:(NSString*)aTag
{
    ParserAction* aParserAction = [parserEndActions objectForKey:aTag];
    if(aParserAction) {
        [aParserAction dispatch:self string:characters];
        characters = NULL;
    }else{
        //FXDebugLog(kFXLogWBXML, @"WBXMLParserDelegate tag not handled: %@", aTag);
    }
}

- (void)characters:(NSString*)aCharacters
{
    characters = aCharacters;
}

- (void)processingInstruction:(NSString*)aTarget data:(NSString*)aData
{
}



// Obsolete when attribute resolve stops using this
static void reverse(char s[])
{
    int i, j;
    char c;
    
    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

// Obsolete when attribute resolve stops using this
static void itoa(int n, char s[], int len)
{
    int i, sign;
    
    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;
    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
        if(i >= len) {
            FXDebugLog(kFXLogWBXML, @"itoa overflow");
            break;
        }
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}

class WBXMLHandler : public WBXMLHandlerBase {
public:
    virtual void resolveTagName(Token token, char* tagName, size_t tagNameLength) {
        WellknownResolver::Page aPage = page();
        if(aPage < kMaxASCodePages) {
            const char** codePage = [AS page:aPage];
            const char* name = codePage[token-5];
            if(strlen(name) < tagNameLength) {
                strcpy(tagName, name);
            }else{
                FXDebugLog(kFXLogWBXML, @"resolveTagName exceeded buffer length: %s", name);
            }
        }else{
            FXDebugLog(kFXLogWBXML, @"resolveTagName invalid code page: %d", aPage);
        }
    }
    
    virtual void resolveAttributeName(Token token,
                                      char* attributeName, size_t attributeNameLength,
                                      char* attributeValuePrefix, size_t attributeValuePrefixLength) {
        char buffer[32];
        itoa(token, buffer, 16);
        strncpy(attributeName, buffer, attributeNameLength);
    }
    
    virtual void resolveAttributeValue(Token token,
                                       char* attributeValue, size_t attributeValueLength) {
        char buffer[32];
        itoa(token, buffer, 16);
        strncpy(attributeValue, buffer, attributeValueLength);
    }
};

class Handler : public HandlerBase {
public:
    Handler(bool debug) : HandlerBase(), m_depth(0), m_obj(NULL), m_debug(debug) { }
    Handler(void* obj, bool debug) : HandlerBase(), m_depth(0), m_obj(obj), m_debug(debug) { }
    virtual ~Handler() {}
    
public:
    virtual void setDocumentLocator(const Locator &locator) {}
    virtual void startDocument() { 
        if(m_debug) FXDebugLog(kFXLogWBXML, @"ActiveSync Request\n");
        if(m_obj) {
            [(__bridge id) m_obj startDocument];
        }
    }
    virtual void endDocument() { 
        if(m_debug) FXDebugLog(kFXLogWBXML, @"Document end\n");
        if(m_obj) {
            [(__bridge id) m_obj endDocument];
        }
    }
    virtual void startElement(const SAXString& name, const AttributeList &atts) {
        if(gIsFixTrace) {
            TraceLog(@"%s%s {\n", indent(), name.c_str());
        }else if(m_debug) {
            FXDebugLog(kFXLogWBXML, @"%s%s {\n", indent(), name.c_str());
        }
        m_depth++;
        
        NSMutableDictionary* attributes = NULL;
        size_t length = atts.getLength();
        if(length > 0) {
            attributes = [NSMutableDictionary dictionaryWithCapacity:length];
            for (size_t i = 0; i < length; i++) {
                if(m_debug) FXDebugLog(kFXLogWBXML, @"%s%s = %s\n", indent(), atts.getName(i).c_str(), atts.getValue(i).c_str());
                [attributes setObject:[NSString stringWithUTF8String:atts.getValue(i).c_str()] 
                               forKey:[NSString stringWithUTF8String:atts.getName(i).c_str()]];
            }
        }
        
        if(m_obj) {
            [(__bridge id) m_obj startElement:[NSString stringWithUTF8String:name.c_str()] attributes:attributes];
        }
    }
    virtual void endElement(const SAXString& name) {
        m_depth--;
        
        if(gIsFixTrace) {
            if(strlen(name.c_str()) == 0) {
                TraceLog(@"%s} MISSING END ELEMENT\n", indent());
            }else{
                TraceLog(@"%s} %s\n", indent(), name.c_str());
            }
        }else if(m_debug) {
            if(strlen(name.c_str()) == 0) {
                FXDebugLog(kFXLogWBXML, @"%s} MISSING END ELEMENT\n", indent());
            }else{
                FXDebugLog(kFXLogWBXML, @"%s} %s\n", indent(), name.c_str());
            }
        }
        if(m_obj) {
            [(__bridge id) m_obj endElement:[NSString stringWithUTF8String:name.c_str()]];
        }
    }
    virtual void characters(const SAXString& chars) { 
        if(m_debug && !gIsFixTrace) {
            FXDebugLog(kFXLogWBXML, @"%s%s\n", indent(), chars.c_str());
        }
        if(m_obj) {
            [(__bridge id) m_obj characters:[NSString stringWithUTF8String:chars.c_str()]];
        }
    }
    virtual void ignorableWhitespace(const SAXChar* ch, size_t length) {
    }
    virtual void processingInstruction(const SAXString& target, const SAXString& data) { 
        if(m_debug) FXDebugLog(kFXLogWBXML, @"%sprocessingInstruction:  target=%s data=%s\n", indent(), target.c_str(), data.c_str());
        if(m_obj) {
            [(__bridge id) m_obj processingInstruction:[NSString stringWithUTF8String:target.c_str()]
                                         data:[NSString stringWithUTF8String:data.c_str()]];
        }
    }
    
private:
    static const int kMaxIndent = 32;
    
    const char* indent() {
        int aDepth = m_depth;
        if(aDepth >= kMaxIndent-1) {
            aDepth = kMaxIndent-1;
        }
        memset(m_indent, '\t', aDepth);
        m_indent[aDepth] = '\0';
        return m_indent;
    }
    
public:
    void*   m_obj;
    
private:
    int     m_depth;
    char    m_indent[kMaxIndent];
    bool    m_debug;
    
};

- (void)wbxmlParser:(NSData*)aData command:(ASTag)aCommand debug:(bool)aDebug;
{  
	WBXMLParser wbxmlParser;
    
	WBXMLHandler wbxmlHandler;
    Handler  handler((__bridge void*)self, aDebug);
    
	wbxmlParser.setWellknownResolver(&wbxmlHandler);
	wbxmlParser.setExtensionHandler(&wbxmlHandler);
    wbxmlParser.setErrorHandler(&handler);
    wbxmlParser.setEntityResolver(&handler);
    wbxmlParser.setDocumentHandler(&handler);
	try {
#if 1
        // Pull parser
        wbxmlParser.closeContinuation();
        wbxmlParser.openNewContinuation();
        const byte* aBytes = (const byte*)[aData bytes];
        size_t parsed = wbxmlParser.parse(aBytes, [aData length]);
        if(parsed > 0) {
        	/*size_t dispatched =*/ wbxmlParser.dispatch(NULL, 0);
        }
#else
        // Async "push parser"
        wbxmlParser.asynchId(saxStringNull);
        while (!fileInputSource.eof()) {
            size_t bytesRead = fileInputSource.read(data, 1);
            wbxmlParser.asynchPutBlock(data, bytesRead);
            if (fileInputSource.eof())
                wbxmlParser.asynchExplicitlyEnd();
        }
#endif
	} catch (SAXException& sex) {
		FXDebugLog(kFXLogWBXML, @"wbxmlParse exception %s", (const char*)sex.getMessage().c_str());
    }
}

#if 0

- (void)wbxmlLoopbackTest
{
    FXDebugLog(kFXLogWBXML, @"wbxmlLoopbackTest");
    NSData* encodedData = [self wbxmlGeneratorTest];
    [self wbxmlParser:encodedData handler:NULL];
    [encodedData release];
}

////////////////////////////////////////////////////////////////////////////////
// WBXML Generator
////////////////////////////////////////////////////////////////////////////////
#pragma mark WBXML Generator

- (NSData*)wbxmlGeneratorTest
{
    char stringTable[] = {
		'a', 'b', 'c', 0, 
		' ', 'E', 'n', 't', 'e', 'r', ' ', 'n', 'a', 'm', 'e', ':', ' '
	};
    
	SAXString stringTableString;
	stringTableString.assign(stringTable, sizeof(stringTable)/sizeof(stringTable[0]));
    
	WBXMLDataGenerator wbxml;
	wbxml.startDocument1(1, 0x6a, stringTableString);
	wbxml.startElement1(0x07, false, true); {
        wbxml.startContent(); {
            wbxml.startElement1(0x05, true, true); {
                wbxml.startAttributes(); {
                    wbxml.addAttribute1(0x09);
                    wbxml.addAttributeValueString1(0x00);
                    wbxml.addAttribute1(0x05);
                }
                wbxml.endAttributes();
            }
            wbxml.startContent(); {
                wbxml.startElement1(0x08, true, false); {
                    wbxml.startAttributes(); {
                        wbxml.addAttribute1(0x06);
                        wbxml.addAttributeValue(0x86);
                        wbxml.addAttribute1(0x08);
                        wbxml.addAttributeValueString2("xyz");
                        wbxml.addAttributeValue(0x85);
                        wbxml.addAttributeValueString2("/s");
                    }
                    wbxml.endAttributes();
                }
                wbxml.endElement();
            }
            wbxml.addContentValueString1(0x04); {
                wbxml.startElement1(0x06, true, false); {
                    wbxml.startAttributes(); {
                        wbxml.addAttribute1(0x07);
                        wbxml.addAttribute1(0x0A);
                        wbxml.addAttributeValueString2("N");
                    }
                    wbxml.endAttributes();
                }
                wbxml.endElement();
            }
            wbxml.endContent();
            wbxml.endElement();
        }
        wbxml.endContent();
    }
	wbxml.endElement();
	wbxml.endDocument();
    
    NSData* aData = wbxml.encodedData();
    [aData retain];
    
    return aData;
}
#endif

#endif


@end
