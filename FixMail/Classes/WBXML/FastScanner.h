/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#ifndef FixMail_FastScanner_h
#define FixMail_FastScanner_h

class FastScanner
{
public:
    FastScanner() : m_cursor(0), m_length(0), m_bytes(NULL) {}
    
    const wbyte* bytes() const      { return m_bytes; }
    int length() const              { return m_length; }
    const wbyte* cursor() const     { return &m_bytes[m_cursor]; }
    
    void setData(unsigned int aLength, const wbyte* aBytes) {
        m_cursor    = 0;
        m_length    = aLength;
        m_bytes     = aBytes;
    }
    
    int getByte() {
        int aByte;
        
        if(m_cursor < m_length) {
            aByte = m_bytes[m_cursor];
            m_cursor++;
        }else{
            aByte = -1/*EOF_BYTE*/;
        }
        
        return aByte;
    }
    
    int getInt()
    {
        int result = 0;
        int i;
        
        do {
            i = getByte();
            result = (result << 7) | (i & 0x7f);
        } while ((i & 0x80) != 0);
        
        return result;
    }
    
    
private:
    unsigned int            m_cursor;
    unsigned int            m_length;
    const wbyte*            m_bytes;
};
#endif
