/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Copyright (c) 2002 Interactive Intelligence, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser developed 
 *       by MediaSite Inc Research and Development Group"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Created by: Dima Skvortsov
 * Modified by: Chris Hubbard
 *
 */

#include "WBXMLParser.h"

#include "WBXMLParserInternalDefs.h"

const SAXString saxStringNull;
const SAXString publicIdNull;

size_t AttributeListImp::getLength() const {
	return _attributeInfoList.size();
}

SAXString AttributeListImp::getName(size_t pos) const {
	ASSERT(pos < getLength());
	return _attributeInfoList[pos]._name;
}
SAXString AttributeListImp::getType(size_t pos) const {
	ASSERT(pos < getLength());
	return _attributeInfoList[pos]._type;
}
SAXString AttributeListImp::getValue(size_t pos) const {
	ASSERT(pos < getLength());
	return _attributeInfoList[pos]._value;
}

SAXString AttributeListImp::getType(const SAXString& name) const {
	AttributeInfoList::const_iterator it = _attributeInfoList.begin();
	for (; it != _attributeInfoList.end(); it++) {
		if (name == (*it)._name)
			return (*it)._type;
	}
	return saxStringNull;
}
SAXString AttributeListImp::getValue(const SAXString& name) const {
	AttributeInfoList::const_iterator it = _attributeInfoList.begin();
	for (; it != _attributeInfoList.end(); it++) {
		if (name == (*it)._name)
			return (*it)._value;
	}
	return saxStringNull;
}

AttributeListImp::AttributeListImp() {
}
AttributeListImp::AttributeListImp(const AttributeList& atts) {
	setAttributeList(atts);
}
void AttributeListImp::setAttributeList(const AttributeList& atts) {
	clear();
	for (size_t counter = 0; counter < atts.getLength(); counter++)
		addAttribute(atts.getName(counter), atts.getType(counter), atts.getValue(counter));
}
void AttributeListImp::addAttribute(const SAXString& name, const SAXString& type, const SAXString& value) {
	AttributeInfo attributeInfo;
	attributeInfo._name = name;
	attributeInfo._type = type;
	attributeInfo._value = value;
	_attributeInfoList.push_back(attributeInfo);
}
void AttributeListImp::removeAttribute(const SAXString& name) {
	AttributeInfoList::iterator it = _attributeInfoList.begin();
	for (; it != _attributeInfoList.end(); it++) {
		if (name == (*it)._name)
			_attributeInfoList.erase(it);
	}
}
void AttributeListImp::clear() {
	_attributeInfoList.clear();
}

InputSourceImp::InputSourceImp() {
	_publicId = publicIdNull;
	_systemId = publicIdNull;
	_inputStream = NULL;
}
SAXString InputSourceImp::getPublicId() const {
	return _publicId;
}
void InputSourceImp::setPublicId(const SAXString& publicId) {
	_publicId = publicId;
}
SAXString InputSourceImp::getSystemId() const {
	return _systemId;
}
void InputSourceImp::setSystemId(const SAXString& systemId) {
	_systemId = systemId;
}
InputStream* InputSourceImp::getInputStream() const {
	return _inputStream;
}
void InputSourceImp::setInputStream(InputStream* inputStream) {
	_inputStream = inputStream;
}

WBXMLSAXException::WBXMLSAXException(SAXString message) : _message(message) {
}
SAXString WBXMLSAXException::getMessage() const {
	return _message;
}

WBXMLSAXParseException::WBXMLSAXParseException(SAXString message, const Locator& locator)
	: WBXMLSAXException(message), _publicId(locator.getPublicId()),
	_systemId(locator.getSystemId()), _lineNumber(locator.getLineNumber()),
	_columnNumber(locator.getColumnNumber()) {

}
WBXMLSAXParseException::WBXMLSAXParseException(SAXString message, SAXString publicId,
	SAXString systemId, int lineNumber, int columnNumber) 
	: WBXMLSAXException(message), _publicId(publicId),
	_systemId(systemId), _lineNumber(lineNumber),
	_columnNumber(columnNumber) {
}
SAXString WBXMLSAXParseException::getPublicId() const {
	return _publicId;
}
SAXString WBXMLSAXParseException::getSystemId() const {
	return _systemId;
}
size_t WBXMLSAXParseException::getLineNumber() const {
	return _lineNumber;
}
size_t WBXMLSAXParseException::getColumnNumber() const {
	return _columnNumber;
}

WBXMLParser::WBXMLParser() {
	_sessionId = 0;
	_entityResolver = NULL;
	_dtdHandler = NULL;
	_documentHandler = NULL;
	_errorHandler = NULL;
	_wellknownResolver = NULL;
	_extensionHandler = NULL;
}

void WBXMLParser::setLocale(const char*) {
}
void WBXMLParser::setEntityResolver(EntityResolver* entityResolver) {
	_entityResolver = entityResolver;
}
void WBXMLParser::setDTDHandler(DTDHandler* dtdHandler) {
	_dtdHandler = dtdHandler;
}
void WBXMLParser::setDocumentHandler(DocumentHandler* documentHandler) {
	_documentHandler = documentHandler;
}
void WBXMLParser::setErrorHandler(ErrorHandler* errorHandler) {
	_errorHandler = errorHandler;
}
void WBXMLParser::setWellknownResolver(WellknownResolver* wellknownResolver) {
	_wellknownResolver = wellknownResolver;
}
void WBXMLParser::setExtensionHandler(ExtensionHandler* extensionHandler) {
	_extensionHandler = extensionHandler;
}

EntityResolver* WBXMLParser::getEntityResolver() {
	return _entityResolver;
}
DTDHandler* WBXMLParser::getDTDHandler() {
	return _dtdHandler;
}
DocumentHandler* WBXMLParser::getDocumentHandler() {
	return _documentHandler;
}
ErrorHandler* WBXMLParser::getErrorHandler() {
	return _errorHandler;
}
WellknownResolver* WBXMLParser::getWellknownResolver() {
	return _wellknownResolver;
}
ExtensionHandler* WBXMLParser::getExtensionHandler() {
	return _extensionHandler;
}

void WBXMLParser::parse(const SAXString& systemId) {
	ASSERT(getEntityResolver() != 0);
	InputSource* inputSource;
	_entityResolver->resolveEntity(saxStringNull, systemId, inputSource);
	parse(inputSource);
	delete inputSource;
}

void WBXMLParser::parse(const InputSource* input) {
	ASSERT(getWellknownResolver() != 0);
	// close current continuation
	closeContinuation();

	// open new continuation
	openNewContinuation();

	static const size_t bufferSize = 8192;
	byte buffer[bufferSize];
	while (!input->getInputStream()->eof()) {
		size_t bytesRead = input->getInputStream()->read(buffer, bufferSize);

		if (bytesRead == 0) {
			if (input->getInputStream()->eof()) {
				dispatch(NULL, 0);
				break;
			} else {
				_delayOnNoInput.delay();
			}
		} else {
			if (input->getInputStream()->eof())
				_continuation._noMoreInput = true;

			_delayOnNoInput.skip();
			
			size_t bytesParsed = parse(buffer, bytesRead);
			ASSERT(bytesParsed == bytesRead);
		}
	}
}

void WBXMLParser::asynchId(const SAXString& systemId) {
	ASSERT(getWellknownResolver() != 0);
	// close current continuation
	closeContinuation();

	// open new continuation
	openNewContinuation();
}

void WBXMLParser::asynchPutBlock(const void* buf, size_t len) {
	parse((const byte*)buf, len);
}

void WBXMLParser::asynchExplicitlyEnd() {
	_continuation._noMoreInput = true;

	for (int i = _continuation._stack.size(); i > 0; i--)
		dispatch(NULL, 0);

	closeContinuation();
}

void WBXMLParser::openNewContinuation() {
	ASSERT(_continuation._stack.size() == 0);
	_sessionId++;
	_continuation._stringTable.erase();
	_continuation._bytesConsumed = 0;
	_continuation._mbuint32 = 0;
	Continuation::Element continuationElement;
	continuationElement._method = start;
	_continuation._stack.push(continuationElement);	
	_continuation._noMoreInput = false;
}

void WBXMLParser::closeContinuation() {
	while (_continuation._stack.size() != 0)
		_continuation._stack.pop();

	if (!_continuation._stringTable.empty())
		_continuation._stringTable.erase();
}

size_t WBXMLParser::dispatch(const byte* data, size_t length) 
{
	switch (_continuation._stack.top()._method) {
	case start:
		return parseStart(data, length);
	case publicid:
		return parsePublicId(data, length);
	case stringTable:
		return parseStringTable(data, length);
	case body:
		return parseBody(data, length);
	case element:
		return parseElement(data, length);
	case attribute:
		return parseAttribute(data, length);
	case content:
		return parseContent(data, length);
	case pi:
		return parsePI(data, length);
	case attrvalue:
		return parseAttrValue(data, length);
	case mbuint32:
		return parseMbuint32(data, length);
	case string:
		return parseString(data, length);
	case opaque:
		return parseOpaque(data, length);
	case entity:
		return parseEntity(data, length);
	case attrstart:
		return parseAttrStart(data, length);
	case extension:
		return parseExtension(data, length);
	case termstr:
		return parseTermStr(data, length);
	default:
		ASSERT(!"Unknown method");
		return 0;
	}
}

size_t WBXMLParser::parse(const byte* data, size_t length) 
{
	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && _continuation._stack.top()._method != done) {
		size_t parsedHere = dispatch(data, length);
		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseStart(const byte* data, size_t length) 
{
	enum LocalState {
		version,
		publicid,
		charset,
		strtbl,
		body,
		done
	};

	LocalState localState;
	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = version;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case version:
			if (getDocumentHandler())
				getDocumentHandler()->startDocument();

			TOP_CONTINUATION_STACK(publicid);

			parsedHere = 1;
			_continuation._bytesConsumed++;
			break;
		case publicid: {
			TOP_CONTINUATION_STACK(charset);

			PUSH_CONTINUATION_STACK(WBXMLParser::publicid);
			} break;
		case charset:
			TOP_CONTINUATION_STACK(strtbl);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case strtbl: {
			TOP_CONTINUATION_STACK(body);

			PUSH_CONTINUATION_STACK(WBXMLParser::stringTable);
			} break;
		case body:
			TOP_CONTINUATION_STACK(done);

			PUSH_CONTINUATION_STACK(WBXMLParser::body);
			break;
		case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::start) {
		POP_CONTINUATION_STACK;
		if (getDocumentHandler())
			getDocumentHandler()->endDocument();
	}
	return parsedBytes;
}

size_t WBXMLParser::parsePublicId(const byte* data, size_t length) 
{
	enum LocalState {
		nil,
		wellknown,
		index,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = nil;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case nil:
			if (*data == 0) {
				TOP_CONTINUATION_STACK(index);

				parsedHere = 1;
				_continuation._bytesConsumed++;
			} else {
				TOP_CONTINUATION_STACK(wellknown);
			}

			break;
		case index:
		case wellknown: 
			TOP_CONTINUATION_STACK(done);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::publicid) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

size_t WBXMLParser::parseStringTable(const byte* data, size_t length) 
{
	enum LocalState {
		tbllength,
		bytes,
		done
	};

	LocalState localState;

	if (!_continuation._stack.top()._localStates.empty())
		localState = (LocalState)_continuation._stack.top()._localStates[0];
	else 
		localState = tbllength;

	size_t parsedBytes = 0;
	while (data != NULL && length > 0 && localState != done) {
		size_t parsedHere = 0;
		switch (localState) {
		case tbllength:
			ASSERT(_continuation._stringTable.empty());

			TOP_CONTINUATION_STACK(bytes);

			PUSH_CONTINUATION_STACK(WBXMLParser::mbuint32);
			break;
		case bytes: {
			size_t stringTableLength; 
			size_t offset = 0;
			if (_continuation._stack.top()._localStates.size() == 3) {
				ASSERT(!_continuation._stringTable.empty());
				offset = _continuation._stack.top()._localStates[1];
				stringTableLength = _continuation._stack.top()._localStates[2];
			} else {
				ASSERT(_continuation._stringTable.empty());
				stringTableLength = _continuation._mbuint32;
				_continuation._stringTable.reserve(stringTableLength);
			}

			if (stringTableLength > 0) {
				parsedHere  = (stringTableLength-offset > length) ? length : stringTableLength-offset;
				_continuation._stringTable.insert(offset, (SAXChar*)data, parsedHere);
				offset += parsedHere;
				_continuation._bytesConsumed += parsedHere;
			}

			if (offset < stringTableLength) {
				TOP_CONTINUATION_STACK3(bytes, offset, stringTableLength);
			} else
				TOP_CONTINUATION_STACK(done);
			} break;
        case done:
            break;
		}

		ASSERT(length >= parsedHere);
		data += parsedHere;
		length -= parsedHere;
		parsedBytes += parsedHere;
	}

	if (localState == done && _continuation._stack.top()._method == WBXMLParser::stringTable) {
		POP_CONTINUATION_STACK;
	}

	return parsedBytes;
}

