/*
 * 
 * Copyright (c) 2001 MediaSite Inc Research and Development Group.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The end-user documentation included with the redistribution,
 *    if any, must include the following acknowledgment:  
 *       "This product uses Streaming WBXML Parser"
 *    Alternately, this acknowledgment may appear in the software itself,
 *    if and wherever such third-party acknowledgments normally appear.
 * 
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL MEDIASITE INC OR ITS CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * ====================================================================
 * 
 * Author: Dima Skvortsov
 *
 */

#ifndef __SAX_HXX
#define __SAX_HXX

class InputSource {
public:
    virtual ~InputSource() {}
    
public:
	virtual SAXString getPublicId() const = 0;
	virtual void setPublicId(const SAXString& publicId) = 0;

	virtual SAXString getSystemId() const = 0;
	virtual void setSystemId(const SAXString& systemId) = 0;

	virtual InputStream* getInputStream() const = 0;
	virtual void setInputStream(InputStream* inputStream) = 0;
};

class AttributeList {
public:
	virtual size_t getLength() const = 0;

	virtual SAXString getName(size_t pos) const = 0;
	virtual SAXString getType(size_t pos) const = 0;
	virtual SAXString getValue(size_t pos) const = 0;

	virtual SAXString getType(const SAXString& name) const = 0;
	virtual SAXString getValue(const SAXString& name) const = 0;
};

class SAXException {
public:
	virtual SAXString getMessage() const = 0;
};

class SAXParseException : virtual public SAXException {
public:
	virtual SAXString getPublicId() const = 0;
	virtual SAXString getSystemId() const = 0;
	virtual size_t getLineNumber() const = 0;
	virtual size_t getColumnNumber() const = 0;
};

class EntityResolver {
public:
	virtual void resolveEntity(const SAXString& publicId,
				  const SAXString& systemId,
				  InputSource*&) = 0;
};

class DTDHandler {
public:
	virtual void notationDecl(const SAXString& name,
				 const SAXString& publicId,
				 const SAXString& systemId) = 0;
	virtual void unparsedEntityDecl(const SAXString& name,
				 const SAXString& publicId,
				 const SAXString& systemId,
				 const SAXString& notationName) = 0;
};

class Locator {
public:
	virtual SAXString getPublicId() const = 0;
	virtual SAXString getSystemId() const = 0;
	virtual size_t getLineNumber() const = 0;
	virtual size_t getColumnNumber() const = 0;
};

class DocumentHandler {
public:
	virtual void setDocumentLocator(const Locator &locator) = 0;
	virtual void startDocument() = 0;
	virtual void endDocument() = 0;
	virtual void startElement(const SAXString& name, const AttributeList &atts) = 0;
	virtual void endElement(const SAXString& name) = 0;
	virtual void characters(const SAXString& chars) = 0;
	virtual void ignorableWhitespace(const SAXChar* ch, size_t length) = 0;
	virtual void processingInstruction(const SAXString& target, const SAXString& data) = 0;
};

class ErrorHandler {
public:
	virtual void warning(const SAXParseException& e) = 0;
	virtual void error(const SAXParseException& e) = 0;
	virtual void fatalError(const SAXParseException& e) = 0;
};

class Parser {
public:
	virtual void setLocale(const char*) = 0;
	virtual void setEntityResolver(EntityResolver* entityResolver) = 0;
	virtual void setDTDHandler(DTDHandler* dtdHandler) = 0;
	virtual void setDocumentHandler(DocumentHandler* documentHandler) = 0;
	virtual void setErrorHandler(ErrorHandler* errorHandler) = 0;

	virtual void parse(const SAXString& systemId) = 0;
	virtual void parse(const InputSource* input) = 0;

	virtual void asynchId(const SAXString& systemId) = 0; //for error reporting
	virtual void asynchPutBlock(const void* buf, size_t len) = 0;
	virtual void asynchExplicitlyEnd() = 0;
};

#endif
