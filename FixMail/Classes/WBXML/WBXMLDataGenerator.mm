/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#include "WBXMLDataGenerator.h"
#import "WBXMLRequest.h"

void WBXMLDataGenerator::start(WellknownResolver::Tag tag)
{
    startElement1(tag, false, true);
    startContent();
}

void WBXMLDataGenerator::keyValue(WellknownResolver::Tag tag, NSString* aString)
{
    start(tag);
    if(aString) {
        addContentValueString2([aString UTF8String]);
    }else{
        FXDebugLog(kFXLogActiveSync, @"WBXMLDataGenerator::keyValue no string: %x", tag);
        addContentValueString2("");
    }
    end();
}

void WBXMLDataGenerator::keyValueInt(WellknownResolver::Tag tag, int anInt)
{
    start(tag);
    addContentValueString2([[NSString stringWithFormat:@"%d", anInt] UTF8String]);
    end();
}

void WBXMLDataGenerator::keyOpaque(WellknownResolver::Tag tag, NSData* aData)
{
    start(tag);
    if(aData) {
        addContentValueOpaque([aData bytes], [aData length]);
    }else{
        FXDebugLog(kFXLogActiveSync, @"WBXMLDataGenerator::keyOpaque no data: %x", tag);
        addContentValueString2("");
    }
    end();
}

void WBXMLDataGenerator::end() {
    endContent();
    endElement();
}

void WBXMLDataGenerator::tag(WellknownResolver::Tag tag)
{
    startElement1(tag, false, false);
    endElement();
}

NSData* WBXMLDataGenerator::encodedData(WBXMLRequest* aRequest, ASTag aTopTag) 
{
    endDocument();
#ifdef USE_SYBYX
    if([WBXMLRequest traceWBXMLRequests]) {
        [aRequest wbxmlParser:data command:aTopTag debug:TRUE];
    }
#endif
    return data; 
}
