//
//  SZCAppDelegate.m
//  SafeGuard
//
//  Created by Anluan O'Brien on 12-03-19.
//  Copyright (c) 2012 Fixmo Inc. All rights reserved.
//

#import "SZCAppDelegate.h"
#import "SZCViewController.h"
#import "SZLApplicationContainer.h"
#import "SZLConcreteApplicationContainer.h"
//#import "SZCGuardScreenController.h"
//#import "SZCProvisioningViewController.h"
//#import "SZCSafeGuardServerConnector.h"
//#import "SZLCAC.h"
//#import "SZLCertificate.h"
//#import "SZCCACManager.h"
//#import "SZLSecureDatabase.h"
//#import "SZLSecureSettings.h"

//#import "SZCPushManager.h"


//@interface SZLSecureSettings (AppDelegate)
//-(void)wipe;
//@end

@interface SZLConcreteApplicationContainer (AppDelegate)
-(void)exitToContainerWithCompletion:(void (^)(void))completionBlock;
@end


//@interface SZLSecureDatabase (Container)
//+(void)clearKey;
//@end

@interface SZCAppDelegate ()
{
    id _observer;
}
@end


@implementation SZCAppDelegate

@synthesize window = _window;
//@synthesize viewController = _viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] name]];
//    [TestFlight takeOff:@"0ac9595c-5fda-48ef-9452-d4e0acb4c460"];
    
    
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:0
                                                            diskCapacity:0 
                                                                diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    sharedCache = nil;
    
    self.window = [[SZCUIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
#if !TARGET_IPHONE_SIMULATOR
    if ([self isJailbroken]) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The SafeZone container cannot be run on a jailbroken device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];        
    } else {
#endif
//        _lockScreenController = [[UINavigationController alloc] initWithRootViewController:[[SZCGuardScreenController alloc] initWithNibName:@"SZCGuardScreen" bundle:nil]];        
        _rootViewController = [SZLConcreteApplicationContainer sharedApplicationContainer];
        //[[SZLConcreteApplicationContainer alloc] init];
        [_rootViewController.view setFrame:_window.bounds];
        self.window.rootViewController = _rootViewController;
        
        freshLaunch = YES;
        
        [self.window makeKeyAndVisible];

        // Only register for push on launch if have registered for push - else call upon exit of pimclient


            
//        _observer = [[NSNotificationCenter defaultCenter] addObserverForName:SZCContainerDidUnlock object:nil queue:nil usingBlock:^(NSNotification *note) {
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            BOOL registeredForPush = [defaults boolForKey:userDefaultsRegisteredForPushKey];
//            
//            SZCPushManager *pushManager = [SZCPushManager PushManager];
//            if (registeredForPush || (pushManager.tryToRegisterForPush && [pushManager findEmailAddress])) {
//                [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//                [self stopObserving];
//            }
//        }];
//        
//        
//        SZCPushManager *pushManager = [SZCPushManager PushManager];
//        NSLog(@"Push Manager: %@",pushManager);
//        
//        // If app not in foreground, launched upon receiving push notification with notification part of the launchoptions
//        if (launchOptions)
//        {
//            NSDictionary *payload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
//            if (payload) {
//                [pushManager processPushNotificationAfterUnlockWithPayload:payload];
//            }
//        }
        
        
        
#if !TARGET_IPHONE_SIMULATOR
    }
#endif
    
//    SZCCACManager *manager = [SZCCACManager CACManager];
//    NSLog(@"CAC Manager: %@",manager);

    return YES;
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Usually called right after registered for push
    // Will only be called once unlocked
    
    // Save as plain string without whitespace or '<' or '>'
    NSString *newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"Device token is: %@", newToken);
    
//    [[SZCPushManager PushManager] registerApplicationForPushWithToken:newToken];

}


- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
    NSLog(@"Failed to get token from APNS, error: %@", error);
    
    // FIXME PUSH
    // method in PushManager
    //[[SZCPushManager PushManager] pro]
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setBool:NO forKey:userDefaultsRegisteredForPushKey];
//    [defaults synchronize];
//    
//    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Push Registration Failed" message:[[SZCSafeGuardServerConnector sharedInstance] errorMessage] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [av show];
//    
    
}


// Called if push notification comes in and app active, even if suspended in background
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    if (application.applicationState == UIApplicationStateActive) {
        
        NSLog(@"Received Push Notification when app running");
        
        // App was frontmost when received notification and won't be made active
        application.applicationIconBadgeNumber = 0;
        
//        [[SZCPushManager PushManager] processPushNotificationWhileActiveWithPayload:userInfo];
        
        
        
    } else if (application.applicationState == UIApplicationStateInactive) {
        // App wasn't on screen when received push notification but wasn't LAUNCHED upon opening notification because just inactive
//        [[SZCPushManager PushManager] processPushNotificationAfterUnlockWithPayload:userInfo];

    }
}

                 
-(void)stopObserving
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
//    [[SZCSafeGuardServerConnector sharedInstance] clearLastCommand];
//    if (_lockScreenController.view.window) {
//        [_lockScreenController viewWillDisappear:NO];
//    }
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[SZLConcreteApplicationContainer sharedApplicationContainer] didEnterBackground];

    [[UIPasteboard generalPasteboard] setItems:_savedPasteboardItems];
    _savedPasteboardItems = nil;
    [self closePopUps:application.windows];
    [self lockContainer:NO];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    _savedPasteboardItems = [[UIPasteboard generalPasteboard] items];
    [[UIPasteboard generalPasteboard] setItems:[NSArray array]];
    
    [[SZLConcreteApplicationContainer sharedApplicationContainer] willEnterForeground];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
//    if (freshLaunch) {
//        [_rootViewController presentViewController:_lockScreenController animated:NO completion:nil];
//        //[self lockContainer];
//        freshLaunch = NO;
//    }
    
//    NSLog(@"SafeZone Container v%@.",SAFEZONE_VERSION);
    
    [[SZLConcreteApplicationContainer sharedApplicationContainer] didBecomeActive];
        
#if !TARGET_IPHONE_SIMULATOR
//    if (![[SZCSafeGuardServerConnector sharedInstance] isProvisioned]) {
//        SZCProvisioningViewController *pvc = [[SZCProvisioningViewController alloc] initWithNibName:@"SZCProvisioningView" bundle:nil];
//        [_lockScreenController presentModalViewController:pvc animated:NO];
//    }
#endif
    
    // this could also be done in ConcreteApplicationContainer requestToSwitchToContainedApplication:
    // FIXME PUSH should only be set to 0 when opening email app
    application.applicationIconBadgeNumber = 0;

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)wipeContainer
{
    [[SZLConcreteApplicationContainer sharedApplicationContainer] exitToContainerWithCompletion:^{
        
//        [SZLSecureDatabase clearKey];
        NSFileManager *fm = [NSFileManager defaultManager];
        //    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        //    NSArray *databases = [[NSBundle mainBundle] pathsForResourcesOfType:@"sqlite" inDirectory:documentsDirectory];
        
        NSArray *wipeFolders = [NSArray arrayWithObjects:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0],
                                [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0], nil];
        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults removeObjectForKey:@"serviceURL"];
//        [defaults synchronize];

        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
        [[NSUserDefaults standardUserDefaults] synchronize];
//        [[SZLSecureSettings sharedSettings] wipe];
        
        for (NSString *folder in wipeFolders) {
        NSError *err = nil;
        NSArray *files = [fm contentsOfDirectoryAtPath:folder error:&err];
        for (NSString *removePath in files) {
                NSLog(@"Removing %@",removePath);
                err = nil;
                NSString *fullPath = [folder stringByAppendingPathComponent:removePath];
                if (![fm removeItemAtPath:fullPath error:&err]) {
                    NSLog(@"Error removing %@\n%@---",fullPath,err);
                }    
            }
        }
        
        [self lockContainer:NO];
        
#if !TARGET_IPHONE_SIMULATOR
//        SZCProvisioningViewController *pvc = [[SZCProvisioningViewController alloc] initWithNibName:@"SZCProvisioningView" bundle:nil];
//        [_lockScreenController presentModalViewController:pvc animated:NO];
#endif
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Container Wiped" message:@"This SafeZone container has been wiped and will now exit." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av setTag:1];
        [av show];
    }];

}

-(void)lockContainer
{
    [self lockContainer:YES];
}

-(void)lockContainer:(BOOL)animated
{
//    // don't present if already up.
//    [SZCCACManager stopObserving];
//    if (_lockScreenController.view.window) {
//        // lock screen is already presented. dismiss anything showing on top of it. log that this happened.
//        NSLog(@"attempted to lock and lock was already up");
//        if (_lockScreenController.modalViewController) {
//            [_lockScreenController.modalViewController dismissViewControllerAnimated:YES completion:nil];
//        }
//        return;
//    }
//    
//    UIViewController *modalController = self.window.rootViewController.modalViewController;
//    
//    if (!modalController) {
//        [self.window.rootViewController presentViewController:_lockScreenController animated:animated completion:nil];
//        
//    } else {
//        
////        if ([modalController isKindOfClass:[SZCGuardScreenController class]]) {
////            return;
////        }
//
//        if ([modalController isKindOfClass:[SZCProvisioningViewController class]]) {
//            return;
//        }
//        
//        // Traverse the modal controller chain until we are at the frontmost controller.
//        while (modalController.modalViewController) {
//            if (modalController.modalViewController.modalPresentationStyle != UIModalPresentationFullScreen) {
//                [modalController.modalViewController dismissModalViewControllerAnimated:NO];
//            } else {
//                modalController = modalController.modalViewController;
//            }
//        }
//        
////        if ([modalController isKindOfClass:[SZCGuardScreenController class]]) {
////            return;
////        }
//        
//        if ([modalController isKindOfClass:[SZCProvisioningViewController class]]) {
//            return;
//        }
//        
//        if (modalController.modalPresentationStyle != UIModalPresentationFullScreen) {
//            UIViewController *tmp = [modalController presentingViewController];
//            [modalController dismissModalViewControllerAnimated:NO];
//            if (tmp && [tmp presentingViewController]) {
//                modalController = tmp;
//            } else {
//                modalController = nil;
//            }
//        }
//        
//        if (modalController) {
//            [modalController presentViewController:_lockScreenController animated:animated completion:nil];
//        } else {
//            [self.window.rootViewController presentViewController:_lockScreenController animated:animated completion:nil];
//        }
//    }
}

-(void)unlockContainer
{
//    [_lockScreenController dismissViewControllerAnimated:YES completion:^{
//        
//        [[NSNotificationCenter defaultCenter] postNotificationName:SZCContainerDidUnlock object:nil];
//        [SZCCACManager startObserving];
//
//        [[SZCPushManager PushManager] updatePush];        
//    }];
}

-(id <SZLApplicationContainer>)container
{
    return [SZLConcreteApplicationContainer sharedApplicationContainer];
}

-(BOOL)isJailbroken
{
    NSString *checkPathString = @"/bin/ssh /Applications/Cydia.app /Applications/Icy.app /Applications/Rock.app /var/mobile/Library/Caches/com.apple.mobile.installation.plist";
    NSArray *paths = [checkPathString componentsSeparatedByString:@" "];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    for (NSString *path in paths) {
        NSLog(@"Testing: %@",path);
        if ([fm fileExistsAtPath:path]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)closePopUps:(NSArray *)subviews {
    for (UIView * subview in subviews){
        if ([subview isKindOfClass:[UIAlertView class]]){
            [(UIAlertView *)subview dismissWithClickedButtonIndex:-1 animated:NO];
        } else if ([subview isKindOfClass:[UIActionSheet class]]){
            [(UIActionSheet *)subview dismissWithClickedButtonIndex:-1 animated:NO];
        } else {
            [self closePopUps:subview.subviews];
        }
    }
}

#pragma mark - AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) { // Container was just wiped
        exit(0);
    }

}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) { // Container was just wiped
        exit(0);
    }
}

@end
