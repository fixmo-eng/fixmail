/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TraceDelegate.h"

@class ASAccount;
@class TimeFilterSheet;
@class TraceTypeSheet;

@interface TraceViewController : UIViewController <TraceDelegate> {
    ASAccount*                          account;
    
    IBOutlet UIActivityIndicatorView*   activityIndicator;
    
    IBOutlet UIButton*                  emailTimeFilterButton;
    IBOutlet UIButton*                  calendarTimeFilterButton;

    IBOutlet UIButton*                  traceTypeButton;
    IBOutlet UIButton*                  generateReportButton;
    IBOutlet UIButton*                  viewReportButton;
    
    TimeFilterSheet*                    timeFilterSheet;
    TraceTypeSheet*                     traceTypeSheet;
}

// Properties
@property (nonatomic, strong) ASAccount*        account;
@property (nonatomic, strong) TimeFilterSheet*  timeFilterSheet;
@property (nonatomic, strong) TraceTypeSheet*   traceTypeSheet;


// Actions
- (IBAction)emailTimeFilterAction:(id)sender;
- (IBAction)calendarTimeFilterAction:(id)sender;

- (IBAction)reportTypeAction:(id)sender;
- (IBAction)generateReport:(id)sender;
- (IBAction)viewReportAction:(id)sender;

@end
