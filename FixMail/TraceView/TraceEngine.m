/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TraceEngine.h"
#import "StringUtil.h"
#import "WBXMLRequest.h"
#import "SZLLogger.h"
#import "SZLSecureFileHandle.h"

#ifdef USE_SECURE_HANDLE
static SZLSecureFileHandle*     sFileHandle;
#else
static NSFileHandle*            sFileHandle;
#endif

static TraceEngine* sTraceEngine = nil;

void TraceLog(NSString *format, ...)
{
    if(sFileHandle && sTraceEngine) {
        [sTraceEngine lockTrace];
        va_list ap;
        va_start (ap, format);
        if (![format hasSuffix: @"\n"]) {
            format = [format stringByAppendingString: @"\n"];
        }
        NSString *body =  [[NSString alloc] initWithFormat: format arguments: ap];
        va_end (ap);
        
        //FXDebugLog(kFXLogActiveSync, @"%@", body);
        NSData* aData = [body dataUsingEncoding:NSUTF8StringEncoding];
        [sFileHandle writeData:aData];
        [sTraceEngine unlockTrace];
    }
}

@implementation TraceEngine

// Globals
extern BOOL gIsFixTrace;

// Properties
@synthesize fileHandle;
@synthesize traceType;
@synthesize lock;


////////////////////////////////////////////////////////////////////////////////
// Factory
////////////////////////////////////////////////////////////////////////////////
#pragma mark Factory


+ (TraceEngine*)engine
{
    if(!sTraceEngine) {
        sTraceEngine = [[TraceEngine alloc] init];
    }
    return sTraceEngine;
}

+ (NSString*)stringForTraceType:(ETraceType)aTraceType
{
    NSString* aString = @"";
    switch(aTraceType) {
        case kTraceTypeNone:
            aString = @"No Log";
            break;
        case kTraceTypeActiveSync:
            aString = @"Full ActiveSync Log";
            break;
        case kTraceTypeMinimal:
            aString = @"Minimal Log";
            break;
    }
    return aString;
}

////////////////////////////////////////////////////////////////////////////////
// Construct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct

- (id)init
{
    self = [super init];
    if (self) {
        self.lock = [[NSLock alloc] init];
    }
    return self;
}

- (void)dealloc
{
    [self removeTraceLog];
}

- (void)lockTrace
{
    [self.lock lock];
}

- (void)unlockTrace
{
    [self.lock unlock];
}

////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

- (void)setTraceType:(ETraceType)aTraceType
{
    traceType = aTraceType;
    switch(self.traceType) {
        case kTraceTypeNone:
            [WBXMLRequest setTraceWBXMLRequests:FALSE];
            [WBXMLRequest setTraceWBXMLResponses:FALSE];
            break;
        case kTraceTypeActiveSync:
            [WBXMLRequest setTraceWBXMLRequests:TRUE];
            [WBXMLRequest setTraceWBXMLResponses:TRUE];
            break;
        case kTraceTypeMinimal:
            [WBXMLRequest setTraceWBXMLRequests:FALSE];
            [WBXMLRequest setTraceWBXMLResponses:FALSE];
            break;
    }
}
- (NSString*)traceLogPath
{
    NSString* aTraceLogPath = [StringUtil filePathInDocumentsDirectoryForFileName:@"fixTrace.log"];
    return aTraceLogPath;
}

- (void)removeTraceLog
{
    [self close];
	NSString* aTraceLogPath = [self traceLogPath];
    
    // Remove old log if there is one
    //
    NSFileManager* aFileManager = [NSFileManager defaultManager];
	if([aFileManager fileExistsAtPath:aTraceLogPath]) {
		[aFileManager removeItemAtPath:aTraceLogPath error:NULL];
	}
}

- (void)createTraceLog
{
    // Remove old trace log
    [self removeTraceLog];
    
    // Create trace log
	NSString* aTraceLogPath = [self traceLogPath];
   
#ifdef USE_SECURE_HANDLE
    self.fileHandle = [SZLSecureFileHandle fileHandleForWritingAtPath:aTraceLogPath];
#else
    [[NSFileManager defaultManager] createFileAtPath:aTraceLogPath contents:nil attributes:nil];
    self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:aTraceLogPath];
#endif
    if(self.fileHandle) {
        sFileHandle = self.fileHandle;
        gIsFixTrace = TRUE;
    }else{
        FXDebugLog(kFXLogActiveSync, @"Opening log failed: %@", aTraceLogPath);
    }
}

- (void)close
{
    gIsFixTrace = FALSE;
    sFileHandle = nil;
    [self.fileHandle closeFile];
    self.fileHandle = nil;
}

- (NSData*)getData
{
    NSData* aData = nil;
    NSString* aTraceLogPath = [self traceLogPath];
#ifdef USE_SECURE_HANDLE
    self.fileHandle = [SZLSecureFileHandle fileHandleForReadingAtPath:aTraceLogPath];
#else
    self.fileHandle = [NSFileHandle fileHandleForReadingAtPath:aTraceLogPath];
#endif
    if(self.fileHandle) {
        aData = [self.fileHandle readDataToEndOfFile];
    }else{
        
    }
    [self.fileHandle closeFile];
    self.fileHandle = nil;

    return aData;
}

- (void)terminate
{
    [self removeTraceLog];
}

@end
