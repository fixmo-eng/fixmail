//
//  TraceDelegate.h
//  FixMail
//
//  Created by Ed Millard on 9/23/12.
//
//

#import <Foundation/Foundation.h>

@protocol TraceDelegate <NSObject>

- (void)syncComplete;

@end
