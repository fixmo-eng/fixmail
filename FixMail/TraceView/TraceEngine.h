/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */

//#define USE_SECURE_HANDLE
#ifdef USE_SECURE_HANDLE
@class SZLSecureFileHandle;
#endif

#if __cplusplus
extern "C" {
void TraceLog(NSString *format, ...);
}
#else
void TraceLog(NSString *format, ...);
#endif

// Types
typedef enum
{
    kTraceTypeNone                  = 0,
    kTraceTypeActiveSync            = 1,
    kTraceTypeMinimal               = 2
} ETraceType;

@interface TraceEngine : NSObject {
#ifdef USE_SECURE_HANDLE
    SZLSecureFileHandle*        fileHandle;
#else
    NSFileHandle*               fileHandle;
#endif
    ETraceType                  traceType;
}


// Properties
#ifdef USE_SECURE_HANDLE
@property (nonatomic, strong) SZLSecureFileHandle*  fileHandle;
#else
@property (nonatomic, strong) NSFileHandle*         fileHandle;
#endif
@property (nonatomic, readonly) ETraceType          traceType;
@property (nonatomic, strong) NSLock*               lock;


// Factory
+ (TraceEngine*)engine;
+ (NSString*)stringForTraceType:(ETraceType)aTraceType;

// Interface
- (void)setTraceType:(ETraceType)aTraceType;

- (NSString*)traceLogPath;
- (void)removeTraceLog;
- (void)createTraceLog;
- (void)close;

- (NSData*)getData;

- (void)terminate;

- (void)lockTrace;
- (void)unlockTrace;

@end
