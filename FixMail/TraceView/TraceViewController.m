 /*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TraceViewController.h"
#import "AppSettings.h"
#import "ASAccount.h"
#import "ProgressView.h"
#import "TimeFilterSheet.h"
#import "TraceTypeSheet.h"
#import "TraceEngine.h"
#import "composeMailVC.h"

@implementation TraceViewController

// Properties
@synthesize account;
@synthesize timeFilterSheet;
@synthesize traceTypeSheet;

// Constants
static NSString* kLogEmailAddress = @"ed.millard@fixmo.com";

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [account setTraceDelegate:nil];
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [activityIndicator setHidesWhenStopped:TRUE];
    [viewReportButton setHidden:TRUE];
    
    NSString* aEmailTimeFilterString = [account stringForTimeFilter:[account emailFilterType]];
    [emailTimeFilterButton setTitle:aEmailTimeFilterString forState:UIControlStateNormal];
    
    NSString* aCalendarTimeFilterString = [account stringForTimeFilter:[account calendarFilterType]];
    [calendarTimeFilterButton setTitle:aCalendarTimeFilterString forState:UIControlStateNormal];
    
    NSString* aTraceTypeString = [TraceEngine stringForTraceType:[[TraceEngine engine] traceType]];
    [traceTypeButton setTitle:aTraceTypeString forState:UIControlStateNormal];
    
    self.navigationItem.title = @"ActiveSync Debug Logger";
    self.navigationController.toolbarHidden = NO;
    self.navigationController.toolbar.tintColor = [UIColor blackColor];

    UIBarButtonItem* progressItem   = [ProgressView barButtonItem];
	self.toolbarItems = [NSArray arrayWithObjects:progressItem,nil];
}

- (void)viewDidUnload
{
    [account setTraceDelegate:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (IBAction)emailTimeFilterAction:(id)sender
{
    self.timeFilterSheet = [[TimeFilterSheet alloc] initWithView:self.view account:account button:sender folderType:kFolderTypeInbox];
}

- (IBAction)calendarTimeFilterAction:(id)sender
{
    self.timeFilterSheet = [[TimeFilterSheet alloc] initWithView:self.view account:account button:sender folderType:kFolderTypeCalendar];
}

- (IBAction)reportTypeAction:(id)sender
{
    self.traceTypeSheet = [[TraceTypeSheet alloc] initWithView:self.view button:sender];
}

- (IBAction)generateReport:(id)sender
{
    if(account) {
        int anInitialSyncCount = 0;
        NSArray* aFolders = [account foldersToSync];
        for(ASFolder* aFolder in aFolders) {
            if(![aFolder isInitialSynced]) {
                anInitialSyncCount++;
            }
        }
        if(anInitialSyncCount == 0) {
            FXDebugLog(kFXLogActiveSync, @"Reset for initial sync");
            [account resetForInitialSync];
            [[TraceEngine engine] createTraceLog]; 
        }

        TraceEngine* aTraceEngine = [TraceEngine engine];
        ETraceType aTraceType = [aTraceEngine traceType];
        switch(aTraceType) {
            case kTraceTypeNone:
                break;
            case kTraceTypeActiveSync:
            case kTraceTypeMinimal:
                [viewReportButton setHidden:TRUE];
                [account setTraceDelegate:self];
                [activityIndicator startAnimating];
                if([account initialSync]) {
                }else{
                    FXDebugLog(kFXLogActiveSync, @"Trace minimal initial sync failed");
                }
                break;
            default:
                FXDebugLog(kFXLogActiveSync, @"Trace type not implemented: %d", aTraceType);
                break;
        }
    }else{
        FXDebugLog(kFXLogActiveSync, @"Not account to trace");
    }
}

- (IBAction)viewReportAction:(id)sender
{
    TraceEngine* aTraceEngine = [TraceEngine engine];
    NSString* aTraceLogPath = [aTraceEngine traceLogPath];
    NSString* aTraceLog = [NSString stringWithContentsOfFile:aTraceLogPath encoding:NSUTF8StringEncoding error:nil];
    if(aTraceLog.length > 0) {
        composeMailVC* aComposeViewController = [composeMailVC createMailComposerWithTo:[NSArray arrayWithObject:kLogEmailAddress] cc:nil bcc:nil attachments:nil account:self.account];
        NSString* aSubject = [TraceEngine stringForTraceType:[[TraceEngine engine] traceType]];
        [aComposeViewController setSubject:aSubject];
        [aComposeViewController setBody:aTraceLog];
        [self.navigationController pushViewController:aComposeViewController animated:YES];
        [aTraceEngine removeTraceLog];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Trace log is empty");
    }
}

////////////////////////////////////////////////////////////////////////////////
// TraceDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark TraceDelegate

- (void)syncComplete
{
    [account setTraceDelegate:nil];
    [activityIndicator stopAnimating];
    [viewReportButton setHidden:FALSE];
}

@end
