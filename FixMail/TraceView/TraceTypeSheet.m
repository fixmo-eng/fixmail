/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TraceTypeSheet.h"
#import "TraceEngine.h"

@implementation TraceTypeSheet

// Properties
@synthesize button;


////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView button:(UIButton*)aButton
{
    if (self = [super init]) {
        self.button = aButton;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:nil
                                           otherButtonTitles:
                         [TraceEngine stringForTraceType:kTraceTypeActiveSync],
                         [TraceEngine stringForTraceType:kTraceTypeMinimal],
                                                    nil];
        [anActionSheet showInView:aView];
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ETraceType aTraceType = (ETraceType)buttonIndex+1; // Add one to skip kTraceTypeNone

    if(button) {
        NSString* aString = [TraceEngine stringForTraceType:aTraceType];
        [button setTitle:aString forState:UIControlStateNormal];
    }
    
    [[TraceEngine engine] setTraceType:aTraceType];

}

@end