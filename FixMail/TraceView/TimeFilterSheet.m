/*
 *  FIXMO CONFIDENTIAL
 *
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 *
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc.
 */
#import "TimeFilterSheet.h"
#import "ASAccount.h"

@implementation TimeFilterSheet

// Properties
@synthesize account;
@synthesize button;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithView:(UIView*)aView account:(ASAccount*)anAccount button:(UIButton*)aButton folderType:(EFolderType)aFolderType
{
    if (self = [super init]) {
        self.account    = anAccount;
        self.button     = aButton;
        folderType      = aFolderType;
        
        UIActionSheet* anActionSheet;
        anActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                    delegate:self
                                           cancelButtonTitle:nil
                                      destructiveButtonTitle:nil
                                           otherButtonTitles:nil];
        NSArray* aFilterTypeStrings = [account filterTypeStrings];
        for(NSString* aString in aFilterTypeStrings) {
            [anActionSheet addButtonWithTitle:aString];
        }


        [anActionSheet showInView:aView];
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(button) {
        NSString* aString = [account stringForTimeFilter:(EFilterType)buttonIndex];
        switch(folderType) {
            default:
                [account setEmailTimeFilter:aString shouldResync:FALSE];
                break;
            case kFolderTypeCalendar:
            case kFolderTypeUserCalendar:
                [account setCalendarTimeFilter:aString shouldResync:FALSE];
                break;
            case kFolderTypeTasks:
                break;
            case kFolderTypeContacts:
            case kFolderTypeUserContacts:
            case kFolderTypeRecipientInfo:
                break;
        }
        [button setTitle:aString forState:UIControlStateNormal];
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIActionSheetDelegete
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIActionSheetDelegete

- (void)willPresentActionSheet:(UIActionSheet *)anActionSheet
{
    NSArray* aViews = [anActionSheet subviews];
    for(UIView* aView in aViews) {
        if([aView isKindOfClass:[UIButton class]]) {
            UIButton* aButton = (UIButton*)aView;
            EFilterType aFilterType = [account filterTypeForString:[[aButton titleLabel] text]];
            BOOL isSupported        = [account isFilterTypeSupported:aFilterType folderType:folderType];
            [aButton setEnabled:isSupported];
        }
    }
}

@end
