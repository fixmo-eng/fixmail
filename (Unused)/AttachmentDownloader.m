//
//  GmailAttachmentDownloader.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/7/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "AttachmentDownloader.h"
#import "ActivityIndicator.h"
#import "BaseAccount.h"
#import "EmailProcessor.h"
#import "GlobalDBFunctions.h"
#import "StringUtil.h"

@implementation AttachmentDownloader

// Properties
@synthesize delegate;
@synthesize attachment;
@synthesize uid;
@synthesize attachmentNum;
@synthesize folderNum;
@synthesize accountNum;
@synthesize expectedSize;
@synthesize filePath;
@synthesize extension;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


////////////////////////////////////////////////////////////////////////////////////////////
// Static getters
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Static getters

+(NSString*)attachmentDirPath 
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *saveDirectory = [paths objectAtIndex:0];
	
	NSString* attachmentDir = [saveDirectory stringByAppendingPathComponent:@"at"];
	return attachmentDir;
}

+(void)ensureAttachmentDirExists 
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *saveDirectory = [paths objectAtIndex:0];

	NSString* attachmentDir = [saveDirectory stringByAppendingPathComponent:@"at"];

	NSFileManager* fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:attachmentDir]) {
		return;
	}
	
	[fileManager createDirectoryAtPath:attachmentDir withIntermediateDirectories:NO attributes:nil error:nil];
}

+(NSString*)fileNameForAccountNum:(int)accountNum 
                        folderNum:(int)folderNum 
                              uid:(NSString*)uid 
                    attachmentNum:(int)attachmentNum 
                        extension:(NSString*)extension
{
	int combined = [EmailProcessor combinedFolderNumFor:folderNum withAccount:accountNum];
	NSString* fileName = [NSString stringWithFormat:@"%i-%@-%i", combined, uid, attachmentNum];
    if([extension length] > 0) {
        fileName = [fileName stringByAppendingPathExtension:extension];
    }
                      
	return fileName;
}


////////////////////////////////////////////////////////////////////////////////////////////
// ASItemOperationsDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASItemOperationsDelegate

- (void)deliverProgress:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
	if(self.delegate != nil && [self.delegate respondsToSelector:@selector(deliverProgress:)]) {
		[self.delegate performSelectorOnMainThread:@selector(deliverProgress:) withObject:message waitUntilDone:NO];
	}
}

- (void)deliverError:(NSString*)message itemOp:(ASItemOperations *)anItemOp
{
	if(self.delegate != nil && [self.delegate respondsToSelector:@selector(deliverError:)]) {
		[self.delegate performSelectorOnMainThread:@selector(deliverError:) withObject:message waitUntilDone:NO];
	}
}

- (void)deliverFile:(NSString *)aPath itemOp:(ASItemOperations *)anItemOp
{
    [ActivityIndicator off];

	if(self.delegate != nil && [self.delegate respondsToSelector:@selector(deliverAttachment)]) {
		[self.delegate performSelectorOnMainThread:@selector(deliverAttachment) withObject:nil waitUntilDone:NO];
	}
}

- (void)deliverData:(NSData*)aData itemOp:(ASItemOperations *)anItemOp
{
    // Not used when storing attachment to a file
}


////////////////////////////////////////////////////////////////////////////////////////////
// Run loop
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Run loop

-(void)run 
{
	//FXDebugLog(kFXLogActiveSync, @"AttachmentDownloader run: %i %@", attachmentNum, uid);
	
	[NSThread setThreadPriority:0.1];
	
	@autoreleasepool {

		if(![GlobalDBFunctions enoughFreeSpaceForSync]) {
			[self deliverError:[NSString stringWithFormat:NSLocalizedStringFromTable(@"IPHONE/IPOD_DISK_FULL", @"AttachmentDownloader", @"Error when not enough free space on disk"), exp] itemOp:nil];
		} else {
		
			[ActivityIndicator on];
			
			[self deliverProgress:NSLocalizedStringFromTable(@"DOWNLOADING_ATTACHMENT ...", @"AttachmentDownloader", @"Progress notification while downloading attachment") itemOp:nil];
			
			BaseAccount* anAccount = [BaseAccount accountForAccountNumber:self.accountNum];
			[anAccount fetchAttachment:self];
		}
	}

	return;
}

@end
