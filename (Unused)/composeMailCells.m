//
//  composeMailCells.m
//  SafeZone 5
//
//  Created by Colin Biggin on 2013-01-27.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//
#import "composeMailCells.h"
#import "SZCAddressButton.h"
#import "UIView-ViewFrameGeometry.h"

#define kTitlePadding		10.0
#define kTitleTop			15.0
#define kTitleLeft			10.0
#define kContentRight		300.0
#define kMinimumBodyHeight	300.0
#define kMinimumCellHeight	44.0

#pragma mark -

@implementation mailTitleCell

- (void) layoutSubviews
{
	[super layoutSubviews];

	self.titleLabel.left = kTitleLeft;
	self.contentLabel.left = self.titleLabel.right + kTitlePadding;

	// make sure out text field goes to the end of cell
	if (self.contentLabel.right > kContentRight) {
		self.contentLabel.width = (kContentRight - (self.titleLabel.left + kTitlePadding));
	}

	return;
}

#pragma mark Static methods

+ (CGFloat) rowHeight
{
	return kMinimumCellHeight;
}

#pragma mark Getters/Setters

- (void) setTitleString:(NSString *)inString
{
	_titleString = inString;
	self.titleLabel.text = inString;
	[self.titleLabel sizeToFit];

	[self setNeedsLayout];
}

- (void) setContentString:(NSString *)inString
{
	_contentString = inString;
	self.contentLabel.text = inString;
	[self.contentLabel sizeToFit];

	[self setNeedsLayout];
}

@end

#pragma mark -

@implementation mailTextEntryCell

@synthesize contentString = _contentString;

- (void) layoutSubviews
{
	[super layoutSubviews];

	self.titleLabel.left = kTitleLeft;
	self.contentField.left = self.titleLabel.right + kTitlePadding;

	// adjusting the left position might have shifted the field beyond the edge
	// so check for this
	if (self.contentField.right != kContentRight) {
		self.contentField.width = (kContentRight - (self.contentField.left + kTitlePadding));
	}

	return;
}

#pragma mark Static methods

+ (CGFloat) rowHeight
{
	return kMinimumCellHeight;
}

#pragma mark Getters/Setters

- (void) setTitleString:(NSString *)inString
{
	_titleString = inString;
	self.titleLabel.text = inString;
	[self.titleLabel sizeToFit];

	[self setNeedsLayout];
}

- (void) setContentString:(NSString *)inString
{
	_contentString = inString;
	self.contentField.text = inString;

	[self setNeedsLayout];
}

- (NSString *) contentString
{
	return self.contentField.text;
}

#pragma mark UITextFieldDelegate methods

- (BOOL) textFieldShouldBeginEditing:(UITextField *)inTextField
{
	return self.editable;
}

- (void) textFieldDidBeginEditing:(UITextField *)inTextField
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kMailTextEntryActivatedNotification object:self];
}

#pragma mark IBActions

- (IBAction) toggleContactsButton:(id)inSender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kMailTextEntryContactsNotification object:self];
}

@end

#pragma mark -

@implementation mailBodyCell

@synthesize contentString = _contentString;

- (id) initWithStyle:(UITableViewCellStyle)inStyle reuseIdentifier:(NSString *)inReuseIdentifier
{
	self = [super initWithStyle:inStyle reuseIdentifier:inReuseIdentifier];
	if (!self) return nil;

	_calculatedRowHeight = 0;

	return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];

	// our _calculatedRowHeight will have the proper height so just resize
	self.contentView.height = _calculatedRowHeight + 10;

	return;
}

- (CGFloat) rowHeight
{
	if (_calculatedRowHeight < kMinimumBodyHeight) return (kMinimumBodyHeight + 20);
	return (_calculatedRowHeight + 20);
}

#pragma mark Getters/Setters

- (void) setContentString:(NSString *)inString
{
	_contentString = inString;
	self.contentView.text = inString;

	CGFloat newHeight = self.contentView.contentSize.height;
	if (newHeight < kMinimumBodyHeight) newHeight = kMinimumBodyHeight;
	if (newHeight != _calculatedRowHeight) {
		_calculatedRowHeight = newHeight;
		[self setNeedsLayout];
//		[[NSNotificationCenter defaultCenter] postNotificationName:kMailBodySizeChangedNotification object:self];
	}

	return;
}

- (NSString *) contentString
{
	return self.contentView.text;
}

#pragma mark UITextViewDelegate methods

- (void) textViewDidChange:(UITextView *)inTextView
{
	CGFloat newHeight = inTextView.contentSize.height;
	FXDebugLog(kFXLogComposeMailVC, [NSString stringWithFormat:@"textViewDidChange: %d (%d)", (int)newHeight, (int)_calculatedRowHeight]);

	if (newHeight < kMinimumBodyHeight) newHeight = kMinimumBodyHeight;
	if (newHeight != _calculatedRowHeight) {
		_calculatedRowHeight = newHeight;
//		[self setNeedsLayout];
		[[NSNotificationCenter defaultCenter] postNotificationName:kMailBodySizeChangedNotification object:self];
	}

//	_contentString = inTextView.text;
//	[self recalculateRowHeight:true];
}

#pragma mark Private methods

- (void) recalculateRowHeight:(bool)inSendNotification
{
	CGSize theViewSize = self.contentView.frame.size;
	theViewSize.width -= (self.contentView.contentInset.right + self.contentView.contentInset.left);
	theViewSize.height = 10000;

	CGSize theSize = [self.contentString sizeWithFont:self.contentView.font
									constrainedToSize:theViewSize
										lineBreakMode:NSLineBreakByWordWrapping];

	// don't need to do anything... only need to re-layout if we're resizing the view which
	// is only done if it goes above the minimum height.
	// ====================================================================
//	if (theSize.height == _calculatedRowHeight) return;
//	if (theSize.height < kMinimumBodyHeight) return;

	// ok, stuff needs to happen now - basically have to resize ourself and inform the tableview to
	// resize the cell (or the entire tableview maybe)
	// ====================================================================
	if (theSize.height < kMinimumBodyHeight) _calculatedRowHeight = kMinimumBodyHeight;
	else _calculatedRowHeight = theSize.height;
	FXDebugLog(kFXLogComposeMailVC, [NSString stringWithFormat:@"recalculateRowHeight: %d (%d)", (int)_calculatedRowHeight, (int)theSize.height]);

	[self setNeedsLayout];

	if (inSendNotification) {
		[[NSNotificationCenter defaultCenter] postNotificationName:kMailBodySizeChangedNotification object:self];
	}

	return;
}

@end

#pragma mark -

@implementation mailEmailCell

#define kMailEmailBubbleTop				10.0
#define kMailEmailBubbleHeight			25.0
#define kMailEmailCellLineHeight		30.0
#define kMailEmailTopPadding			kTitleTop
#define kMailEmailBottomPadding			10.0

- (void) layoutSubviews
{
	[super layoutSubviews];

	// Our layout will go as follows:
	//
	// To:	<email1> <email2>
	//		<email3> <email4>
	//		[TextField Entry]
	//

	self.titleLabel.origin = CGPointMake(kTitleLeft, kTitleTop);
	CGFloat minX = self.titleLabel.right + kTitlePadding;
	CGFloat maxWidth = kContentRight - minX;
	CGFloat currentY = kMailEmailBubbleTop;

	// get rid of any existing subviews
	[self deleteAllButtons];

	// now go through and create all the addresses, one per line
	for (NSString *theEmailAddress in self.emailAddresses) {
		CGRect theFrame = CGRectMake(minX, currentY, maxWidth, kMailEmailBubbleHeight);
		SZCAddressButton *theButton = [[SZCAddressButton alloc] initWithFrame:theFrame];
		[theButton setTitle:theEmailAddress forState:UIControlStateNormal];
		[theButton sizeToFit];
		if (theButton.width > maxWidth) theButton.width = maxWidth;
		[self.contentView addSubview:theButton];
		currentY += kMailEmailCellLineHeight;
	}

	// now our text field, currentY will be at the proper height now
	if (_editable) {
		self.contentField.origin = CGPointMake(minX, currentY);
		self.contentField.width = maxWidth;
		self.contentField.hidden = false;
		[self.contentField becomeFirstResponder];
	} else {
		self.contentField.hidden = true;
	}

	return;
}

- (void) prepareForReuse
{
	self.titleLabel.text = @"";
	self.contentField.text = @"";
	[self deleteAllButtons];
}

#pragma mark Static methods

+ (CGFloat) rowHeightForList:(NSArray *)inEmailAddresses editing:(bool)inEditing;
{
	CGFloat theHeight = kMailEmailTopPadding + inEmailAddresses.count * kMailEmailCellLineHeight + kMailEmailBottomPadding;
	if (inEditing) theHeight += kMailEmailCellLineHeight;
	if (theHeight < kMinimumCellHeight) theHeight = kMinimumCellHeight;
	return theHeight;
}

#pragma mark Getters/Setters

- (void) setTitleString:(NSString *)inString
{
	_titleString = inString;
	self.titleLabel.text = inString;
	[self.titleLabel sizeToFit];

	[self setNeedsLayout];
}

- (void) setEmailAddresses:(NSMutableArray *)inEmailAddresses
{
	if (_emailAddresses == nil) _emailAddresses = [[NSMutableArray alloc] initWithCapacity:2];
	[_emailAddresses removeAllObjects];
	[_emailAddresses addObjectsFromArray:inEmailAddresses];

	[self setNeedsLayout];
}

- (void) setEditable:(bool)inValue
{
	if (inValue == _editable) return;

	_editable = inValue;
	[self setNeedsLayout];
}

#pragma mark UITextFieldDelegate methods

- (void) textFieldDidBeginEditing:(UITextField *)inTextField
{
}

- (void) textFieldDidEndEditing:(UITextField *)inTextField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)inTextField
{
	if (inTextField.text.length == 0) return NO;

	NSDictionary *theDictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.contentField.text, kEmailCellUserInfoAddress, nil];
	[[NSNotificationCenter defaultCenter] postNotificationName:kEmailCellAddedAddressNotification
															object:self
														  userInfo:theDictionary];	

	return YES;
}

#pragma mark IBActions

- (IBAction) toggleContactsButton:(id)inSender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:kMailTextEntryContactsNotification object:self];
}

#pragma mark Private methods

- (void) deleteAllButtons
{
	NSMutableArray *deleteArray = [[NSMutableArray alloc] initWithCapacity:self.contentView.subviews.count];
	for (UIView *theSubview in self.contentView.subviews) {
		if (![theSubview isKindOfClass:[SZCAddressButton class]]) continue;
		[deleteArray addObject:theSubview];
	}
	if (deleteArray.count > 0) {
		[deleteArray makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}

	return;
}

@end


