//
//  composeMailCells.h
//  SafeZone 5
//
//  Created by Colin Biggin on 2013-01-28.
//  Copyright (c) 2013 Fixmo Inc. All rights reserved.
//

#define kMailTextEntryActivatedNotification		@"kMailTextEntryActivatedNotification"
#define kMailTextEntryContactsNotification		@"kMailTextEntryContactsNotification"
#define kMailBodySizeChangedNotification		@"kMailBodySizeChangedNotification"

#define kEmailCellAddedAddressNotification		@"kEmailCellAddedAddressNotification"
#define kEmailCellUserInfoAddress				@"kEmailCellUserInfoAddress"

@interface mailTitleCell : UITableViewCell

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSString *contentString;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;

+ (CGFloat) rowHeight;

@end

#pragma mark -

@interface mailTextEntryCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSString *contentString;
@property (nonatomic, assign) bool editable;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITextField *contentField;

+ (CGFloat) rowHeight;
- (IBAction) toggleContactsButton:(id)inSender;

@end

#pragma mark -

@interface mailBodyCell : UITableViewCell <UITextViewDelegate>
{
	CGFloat			_calculatedRowHeight;
}

@property (nonatomic, strong) NSString *contentString;

@property (nonatomic, weak) IBOutlet UITextView *contentView;

- (CGFloat) rowHeight;

@end

#pragma mark -

@interface mailEmailCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) NSString *titleString;
@property (nonatomic, strong) NSMutableArray *emailAddresses;
@property (nonatomic, assign) bool editable;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UITextField *contentField;

+ (CGFloat) rowHeightForList:(NSArray *)inEmailAddresses editing:(bool)inEditing;
- (IBAction) toggleContactsButton:(id)inSender;

@end

