//
//  AttachmentViewController.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/7/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "AttachmentViewController.h"
#import "AttachmentDownloader.h"

@implementation AttachmentViewController

// Properties
@synthesize attachment;
@synthesize filename;
@synthesize extension;
@synthesize uid;
@synthesize attachmentNum;
@synthesize accountNum;
@synthesize folderNum;
@synthesize contentType;
@synthesize expectedSize;

@synthesize webWiew;
@synthesize loadingLabel;
@synthesize loadingIndicator;

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    FXDebugLog(kFXLogActiveSync, @"Exception: AttachmentViewController %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
	[super viewDidUnload];
	
	self.webWiew            = nil;
	self.loadingLabel       = nil;
	self.loadingIndicator   = nil;
    
    self.attachment         = nil;
	self.contentType        = nil;
	self.uid                = nil;
}

-(void)viewDidAppear:(BOOL)animated 
{
	[super viewDidAppear:animated];
}

-(void)viewDidLoad 
{
	self.webWiew.delegate = self;
	self.webWiew.scalesPageToFit = YES;
	self.title = NSLocalizedStringFromTable(@"ATTACHMENT", @"AttachmentViewController", @"Attachment field title");
}

-(void)viewWillAppear:(BOOL)animated 
{
	[self.loadingIndicator startAnimating];
}

- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

-(void)doLoad 
{
	[self.loadingLabel setHidden:NO];
	[self.loadingIndicator setHidden:NO];
	[self.loadingIndicator startAnimating];
	
	[AttachmentDownloader ensureAttachmentDirExists];
	
    self.filename = [attachment objectForKey:@"n"];
    self.extension = [filename pathExtension];
	NSString* attachmentName = [AttachmentDownloader fileNameForAccountNum:self.accountNum 
                                                                 folderNum:self.folderNum 
                                                                       uid:self.uid 
                                                             attachmentNum:self.attachmentNum
                                                                 extension:self.extension];
	NSString* attachmentDir = [AttachmentDownloader attachmentDirPath];
	NSString* attachmentPath = [attachmentDir stringByAppendingPathComponent:attachmentName];
	
	// try to find attachment on disk
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if([fileManager fileExistsAtPath:attachmentPath]) {
		// load it, display it
		[self performSelectorOnMainThread:@selector(deliverAttachment) withObject:nil waitUntilDone:NO];
		return;		
	}
	
	// else, fetch from Gmail
	AttachmentDownloader* downloader = [[AttachmentDownloader alloc] init];
    downloader.attachment       = self.attachment;
	downloader.uid              = self.uid;
	downloader.attachmentNum    = self.attachmentNum;
	downloader.delegate         = self;
	downloader.folderNum        = self.folderNum;
	downloader.accountNum       = self.accountNum;
    downloader.expectedSize     = self.expectedSize;
    downloader.extension        = self.extension;

    downloader.filePath         = attachmentPath;
	
	NSThread *driverThread = [[NSThread alloc] initWithTarget:downloader selector:@selector(run) object:nil];
	[driverThread start];
	
}

-(void)deliverProgress:(NSString*)message 
{
	self.loadingLabel.text = message;
}

-(void)deliverError:(NSString*)error 
{
	[self.loadingLabel setHidden:YES];
	[self.loadingIndicator setHidden:YES];
	[self.loadingIndicator stopAnimating];
	
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedStringFromTable(@"ERROR_DOWNLOADING_ATTACHMENT", @"ErrorAlert", @"Alert view title for error downloading attachment")
                                                                message:error
                                                                delegate:self
                                                       cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"ErrorAlert", @"OK button title for alert view")
                                                    otherButtonTitles:nil];
	[alertView show];	
}

-(void)deliverAttachment 
{
    @try {
        NSString* attachmentName = [AttachmentDownloader fileNameForAccountNum:self.accountNum 
                                                                     folderNum:self.folderNum 
                                                                           uid:self.uid 
                                                                 attachmentNum:self.attachmentNum
                                                                     extension:self.extension];
        NSString* attachmentDir = [AttachmentDownloader attachmentDirPath];
        NSString* attachmentPath = [attachmentDir stringByAppendingPathComponent:attachmentName];
        
        FXDebugLog(kFXLogActiveSync, @"Opening filename: %@, content type: %@", attachmentName, contentType);
        
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL fileURLWithPath:attachmentPath isDirectory:NO]];
        [self.webWiew loadRequest:request];
        
        self.loadingLabel.text = NSLocalizedStringFromTable(@"DISPLAYING_...", @"AttachmentViewController", @"Loading label text while displaying attachment");
    }@catch (NSException* e) {
        [self _logException:@"loadSettings" exception:e];
    }
}


////////////////////////////////////////////////////////////////////////////////
// UIWebViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIWebViewDelegate

-(void)webViewDidStartLoad:(UIWebView *)aWebView 
{
#ifdef DEBUG
    NSURLRequest* aRequest = aWebView.request;
	FXDebugLog(kFXLogActiveSync, @"AttachmentViewController webViewDidStartLoad %@ %@", [aRequest URL], [aRequest mainDocumentURL]);
#endif
}

-(void)webViewDidFinishLoad:(UIWebView *)webViewLocal {
    FXDebugLog(kFXLogActiveSync, @"AttachmentViewController didFinishLoad");

	[loadingLabel setHidden:YES];
	[loadingIndicator setHidden:YES];
	[loadingIndicator stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error 
{
	FXDebugLog(kFXLogActiveSync, @"AttachmentViewController didFailLoadWithError: %@", error);
	loadingLabel.text = [NSString stringWithFormat:@"Error: %@", error];
	[loadingIndicator setHidden:YES];
	[loadingIndicator stopAnimating];
}

- (BOOL)webView:(UIWebView *)aWebView shouldStartLoadWithRequest:(NSURLRequest *)aRequest navigationType:(UIWebViewNavigationType)aNavigationType
{
    FXDebugLog(kFXLogActiveSync, @"AttachmentViewController shouldStartLoadWithRequest: %@", [aRequest URL]);
    return TRUE;
}

@end
