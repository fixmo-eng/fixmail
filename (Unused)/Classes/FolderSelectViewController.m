//
//  FolderSelectViewController.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/15/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "FolderSelectViewController.h"
#import "BaseAccount.h"
#import "HomeViewController.h"
#import "ImapName.h"
#import "SyncManager.h"
#import "EmailProcessor.h"
#import "StringUtil.h"
    
@implementation FolderSelectViewController

@synthesize account;
@synthesize utf7Decoder;
@synthesize folderPaths;
@synthesize folderSelected;

@synthesize name;
@synthesize emailAddress;
@synthesize password;
@synthesize server;

@synthesize encryption;
@synthesize port;
@synthesize authentication;

@synthesize accountNum;
@synthesize newAccount;
@synthesize firstSetup;
@synthesize accountType;

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
	[super viewDidUnload];
	
	self.utf7Decoder = nil;
	self.folderPaths = nil;
	self.folderSelected = nil;
	
	self.name = nil;
	self.emailAddress = nil;
	self.password = nil;
	self.server = nil;
}

-(void)viewDidAppear:(BOOL)animated 
{
	[super viewDidAppear:animated];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Folders", nil);
	self.navigationItem.prompt = NSLocalizedString(@"Select folders to download.", nil);
	
	self.utf7Decoder = [NSDictionary dictionaryWithObjectsAndKeys:
						@"À", @"&AMA-",
						@"Á", @"&AME-",
						@"Â", @"&AMI-",
						@"Ã", @"&AMM-",
						@"Ä", @"&AMQ-",
						@"Å", @"&AMU-",
						@"Æ", @"&AMY-",
						@"Ç", @"&AMc-",
						@"È", @"&AMg-",
						@"É", @"&AMk-",
						@"Ê", @"&AMo-",
						@"Ë", @"&AMs-",
						@"Ì", @"&AMw-",
						@"Í", @"&AM0-",
						@"Î", @"&AM4-",
						@"Ï", @"&AM8-",
						@"Ñ", @"&ANE-",
						@"Ò", @"&ANI-",
						@"Ó", @"&ANM-",
						@"Ô", @"&ANQ-",
						@"Õ", @"&ANU-",
						@"Ö", @"&ANY-",
						@"Ø", @"&ANg-",
						@"Ù", @"&ANk-",
						@"Ú", @"&ANo-",
						@"Û", @"&ANs-",
						@"Ü", @"&ANw-",
						@"ß", @"&AN8-",
						@"à", @"&AOA-",
						@"á", @"&AOE-",
						@"â", @"&AOI-",
						@"ã", @"&AOM-",
						@"ä", @"&AOQ-",
						@"å", @"&AOU-",
						@"æ", @"&AOY-",
						@"ç", @"&AOc-",
						@"è", @"&AOg-",
						@"é", @"&AOk-",
						@"ê", @"&AOo-",
						@"ë", @"&AOs-",
						@"ì", @"&AOw-",
						@"í", @"&AO0-",
						@"î", @"&AO4-",
						@"ï", @"&AO8-",
						@"ò", @"&API-",
						@"ó", @"&APM-",
						@"ô", @"&APQ-",
						@"õ", @"&APU-",
						@"ö", @"&APY-",
						@"ù", @"&APk-",
						@"ú", @"&APo-",
						@"û", @"&APs-",
						@"ü", @"&APw-", nil];
	
	self.folderSelected = [NSMutableSet set];
	
	if(!self.newAccount) {
		SyncManager *sm = [SyncManager getSingleton];
		for(int i = 0; i < [sm folderCount:self.accountNum]; i++) {
			if([sm isFolderDeleted:i accountNum:self.accountNum]) {
				continue;
			}
			
			NSString* folderPath = [[sm retrieveState:i accountNum:self.accountNum] objectForKey:@"folderPath"];
			[self.folderSelected addObject:folderPath];
		}
	} else {
		// pre-select Inbox and Sent Messages
		for(NSString* folderPath in self.folderPaths) {
			if([folderPath isEqualToString:@"INBOX"]) {
				[self.folderSelected addObject:folderPath];
			}
			
			int cutaway = 0;
			if([StringUtil stringStartsWith:folderPath subString:@"[Gmail]/"]) {
				cutaway = [@"[Gmail]/" length];
			} else if ([StringUtil stringStartsWith:folderPath subString:@"[Google Mail]/"]) {
				cutaway = [@"[Google Mail]/" length];
			}
			
			if(cutaway > 0) {
				NSString* stringAfterCut = [folderPath substringFromIndex:cutaway];
				
				// include sent folder as well
				if([stringAfterCut isEqualToString:@"Sent Mail"] ||
				   [stringAfterCut isEqualToString:@"Gesendet"] ||
				   [stringAfterCut isEqualToString:@"Messages envoy&AOk-s"]) {
					[self.folderSelected addObject:folderPath];
				}  
			}
			
		}
	}
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return YES;
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Interface
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Interface

-(void)_openHomeView 
{
	// display home screen
	HomeViewController *homeController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
	UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:homeController];
	navController.navigationBarHidden = NO;
	[self.view.window addSubview:navController.view];
	
	// Remove own view from screen stack
	[self.view removeFromSuperview];
}

- (void)modifyAccount:(NSSet*)aFoldersSelected
{
    SyncManager *sm = [SyncManager getSingleton];

    // calculate the delta between current folders and the selected ones
    NSMutableSet* syncedFolders = [NSMutableSet set];
    
    for(int i = 0; i < [sm folderCount:self.accountNum]; i++) {
        if(![sm isFolderDeleted:i accountNum:self.accountNum]) {
            NSString* folderPath = [[sm retrieveState:i accountNum:self.accountNum] objectForKey:@"folderPath"];
            [syncedFolders addObject:folderPath];
        }
    }
    
    NSMutableSet* toDelete = [syncedFolders mutableCopy];
    [toDelete minusSet:aFoldersSelected];
    
    for(int i = 0; i < [sm folderCount:self.accountNum]; i++) {
        if(![sm isFolderDeleted:i accountNum:self.accountNum]) {
            NSString* folderPath = [[sm retrieveState:i accountNum:self.accountNum] objectForKey:@"folderPath"];
            if([toDelete containsObject:folderPath]) {
                [sm markFolderDeleted:i accountNum:self.accountNum];
            }
        }
    }
    
    NSMutableSet* toAdd = [aFoldersSelected mutableCopy];
    [toAdd minusSet:syncedFolders];
    
    for(NSString* folderPath in toAdd) {
        if ([sm folderCount:self.accountNum] < [EmailProcessor folderCountLimit]-2) {
            [account setupFolder:folderPath];
        }
    }
     //mutableCopy actually retains the data in it!
}

- (void)done 
{
    // Must select at least one view
    //
	if([self.folderSelected count] == 0) {
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No Folders Selected", nil)
															 message:NSLocalizedString(@"Need to select at least one folder to download.", nil) 
															delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
	}else{
        if(self.newAccount) {
            [account createAccount:self.folderSelected folderPaths:self.folderPaths firstSetup:self.firstSetup];

            if(self.firstSetup) {
                [self _openHomeView];
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            } 
        } else {
            [self modifyAccount:self.folderSelected];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	return [self.folderPaths count] + 1;
}


-(UITableViewCell*) createNewFolderNameCell 
{ 
	UITableViewCell* cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FolderNameCell"];
	cell.detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
	
	return cell; 
} 

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if(indexPath.row == 0) {
		cell.backgroundColor = [UIColor lightGrayColor];
	}
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	UITableViewCell* cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"FolderNameCell"];
	if (cell == nil) {
		cell = [self createNewFolderNameCell];
	}

	if(indexPath.row == 0) {
		cell.imageView.image = nil;
		cell.textLabel.text = NSLocalizedString(@"Select All", nil);
		
		return cell;
	}
	
	NSString* folderPath = [self.folderPaths objectAtIndex:indexPath.row - 1]; 
	
	BOOL selected = [self.folderSelected containsObject:folderPath];
	
	// Take care of Umlauts
	NSString* display = [ImapName imapFolderNameToDisplayName:folderPath];
	cell.textLabel.text = display;	
	
	if(selected) {
		cell.imageView.image = [UIImage imageNamed:@"checkboxChecked.png"];
	} else {
		cell.imageView.image = [UIImage imageNamed:@"checkbox.png"];
	}
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if(indexPath.row == 0) {
		for(NSString* folderPath in folderPaths) {
			
			int cutaway = 0;
			if([StringUtil stringStartsWith:folderPath subString:@"[Gmail]/"]) {
				cutaway = [@"[Gmail]/" length];
			} else if ([StringUtil stringStartsWith:folderPath subString:@"[Google Mail]/"]) {
				cutaway = [@"[Google Mail]/" length];
			}
			
			if(cutaway > 0) {
				NSString* stringAfterCut = [folderPath substringFromIndex:cutaway];
				
				// include sent folder as well
				if(!([stringAfterCut isEqualToString:@"Sent Mail"] ||
					 [stringAfterCut isEqualToString:@"Gesendet"] ||
					 [stringAfterCut isEqualToString:@"Messages envoy&AOk-s"])) {
					continue;
				}  
			}
			
			BOOL selected = [self.folderSelected containsObject:folderPath];
			
			if(!selected) {
				[self.folderSelected addObject:folderPath];
			}
		}
		[self.tableView reloadData];
	} else {
		int folderIndex = indexPath.row - 1;
		NSString* folderPath = [self.folderPaths objectAtIndex:folderIndex];
		BOOL selected = [self.folderSelected containsObject:folderPath];
		
		if(selected) {
			[self.folderSelected removeObject:folderPath];
		} else {
			[self.folderSelected addObject:folderPath];
		}
		
		[self.tableView reloadData];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
