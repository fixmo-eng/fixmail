#ifdef FEATURE_IMAP
//  ReMailIPhone
//
//  Created by Gabor Cselle on 6/29/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "ImapAccount.h"
#import "ActivityIndicator.h"
#import "AppSettings.h"
#import "AttachmentDownloader.h"
#import "ImapFolder.h"
#import "ImapName.h"
#import "StringUtil.h"
#import "SyncManager.h"

#import "CTCoreAccount.h"
#import "CTCoreFolder.h"
#import "CTCoreMessage.h"
#import "CTBareAttachment.h"
#import "CTCoreAttachment.h"
#import "ImapFolderWorker.h"

@implementation ImapAccount

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccountNum:(int)anAccountNum
{
    if (self = [super initWithAccountNum:anAccountNum]) {
        accountType = AccountTypeActiveSync;
        [self loadSettings];
    }
    
    return self;  
}

- (id)initWithName:(NSString*)aName
          userName:(NSString*)aUserName
          password:(NSString*)aPassword              
          hostName:(NSString*)aHostName
              port:(int)aPort
        encryption:(int)anEncryption
    authentication:(int)anAuthentication
    folderNames:(NSArray *)aFolderNames
{
    if (self = [super initWithName:aName
                          userName:aUserName 
                          password:aPassword 
                          hostName:aHostName 
                              port:aPort 
                        encryption:anEncryption 
                    authentication:anAuthentication 
                       folderNames:aFolderNames]) 
    {
        [self setAccountType:AccountTypeImap];
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

////////////////////////////////////////////////////////////////////////////////
// Load/Store
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Store

- (void)loadSettings
{
    [super loadSettings];
}

- (void)commitSettings
{
    [super commitSettings];
}

////////////////////////////////////////////////////////////////////////////////
// Folder
////////////////////////////////////////////////////////////////////////////////
#pragma mark Folder

- (ImapFolder*)findFolder:(NSString*)aPath
{
    return [folderDictionary objectForKey:aPath];
}

////////////////////////////////////////////////////////////////////////////////
// Overrides
////////////////////////////////////////////////////////////////////////////////
#pragma mark Overrides

- (BaseFolder*)createFolderForFolderType:(EFolderType)aFolderType
{
    return [[ImapFolder alloc] initWithAccount:self];
}

- (void)addFolder:(BaseFolder*)aBaseFolder
{
    ImapFolder* aFolder = (ImapFolder*)aBaseFolder;
    ImapFolder* anExistingFolder = [self findFolder:[aFolder path]];
    if(!anExistingFolder) {
        [folders addObject:aFolder]; [aFolder release];
        [folderDictionary setObject:aFolder forKey:[aFolder path]];
    }else{
        FXDebugLog(kFXLogActiveSync, @"Attempting to add folder twice: %@ %@", aFolder, anExistingFolder);
    }
}

- (void)setupFolder:(NSString*)aFolderPath
{
    NSString* aFolderDisplayName = [ImapName imapFolderNameToDisplayName:aFolderPath];
    if ([StringUtil stringContains:aFolderDisplayName subString:@"&"]) {
        aFolderDisplayName = @"";
    } 
    FXDebugLog(kFXLogGeneral, @"generated folder display name: %@ for %@", aFolderDisplayName, aFolderPath );
    NSMutableDictionary* folderState = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithInt:0],     @"accountNum", 
                                        aFolderDisplayName,             @"folderDisplayName",
                                        aFolderPath,                    @"folderPath",
                                        [NSNumber numberWithBool:NO],   @"deleted", 
                                        nil];
    
    [[SyncManager getSingleton] addFolderState:folderState accountNum:accountNum];
}

- (void)creationComplete
{
}

- (void)fetchAttachment:(AttachmentDownloader*)anAttachmentDownloader
{
    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

    // connect to IMAP server
    //
    [anAttachmentDownloader deliverProgress:NSLocalizedString(@"Logging into Account ...", nil)];
    
	if(userName == nil || [userName length] == 0 || password == nil) {
		[anAttachmentDownloader deliverError:NSLocalizedString(@"Invalid credentials", nil)];
		[pool release];
		return;
	}
	
	// log in 
	CTCoreAccount* account = [[CTCoreAccount alloc] init];
	
	@try   {
        [account connectToServer:[AppSettings server:self.accountNum] 
							port:[AppSettings serverPort:self.accountNum]
				  connectionType:[AppSettings serverEncryption:self.accountNum]
						authType:[AppSettings serverAuthentication:self.accountNum] 
						   login:userName 
						password:password];
    } @catch (NSException *exp) {
		[ActivityIndicator off];
		[account disconnect];
        FXDebugLog(kFXLogActiveSync, @"Connect exception: %@", exp);
		[anAttachmentDownloader deliverError:[ImapFolderWorker decodeError:exp]];
		[pool release];
        return; 
	}
	
	[anAttachmentDownloader deliverProgress:NSLocalizedString(@"Opening Folder ...", nil)];
	
	// figure out name of folder to fetch (i.e. the Gmail name)
	NSSet* aFolders;
	@try {
		aFolders = [account allFolders];
	} @catch (NSException *exp) {
		[ActivityIndicator off];
		[account disconnect];
		[anAttachmentDownloader release];
        FXDebugLog(kFXLogActiveSync, @"Error getting folders: %@", exp);
		[anAttachmentDownloader deliverError:[NSString stringWithFormat:NSLocalizedString(@"List Folders: %@", nil), [ImapFolderWorker decodeError:exp]]];
        return; 
	}
	
	NSString* folderPath = nil;
	if ([AppSettings accountType:self.accountNum] == AccountTypeImap
        || [AppSettings accountType:self.accountNum] == AccountTypeActiveSync) {
		SyncManager* sm = [SyncManager getSingleton];
		
		NSDictionary* folderStatus = [sm retrieveState:anAttachmentDownloader.folderNum accountNum:anAttachmentDownloader.accountNum];
		folderPath = [folderStatus objectForKey:@"folderPath"];
	} else {
		FXDebugLog(kFXLogActiveSync, @"Account type not recognized");
	}
	
	CTCoreFolder *folder;
	@try {
		folder = [account folderWithPath:folderPath];
	} @catch (NSException *exp) {
		[ActivityIndicator off];
		[account disconnect];
        FXDebugLog(kFXLogActiveSync, @"Error getting folder: %@", exp);
		[anAttachmentDownloader deliverError:[NSString stringWithFormat:NSLocalizedString(@"Folder: %@", nil), [ImapFolderWorker decodeError:exp]]];
        return; 
	}
	
	if(folder == nil) {
		[ActivityIndicator off];
		[anAttachmentDownloader deliverError:NSLocalizedString(@"Folder not found", nil)];
		[pool release];
        return; 
	}
	
	[anAttachmentDownloader deliverProgress:NSLocalizedString(@"Fetching Attachment ...", nil)];
	
	CTCoreMessage* message;
	@try  {
		message = [folder messageWithUID:(unsigned int)anAttachmentDownloader.uid];
		
		FXDebugLog(kFXLogActiveSync, @"Subject: %@", message.subject);
		
		[message fetchBodyStructure];
		
		NSArray* attachments = [message attachments];
		
		if([attachments count] <= anAttachmentDownloader.attachmentNum)  {
			[anAttachmentDownloader deliverError:NSLocalizedString(@"Can't find attachment on server", nil)];
			[pool release];
			return;
		}
		
		CTBareAttachment* attachment = [attachments objectAtIndex:anAttachmentDownloader.attachmentNum];
		
		CTCoreAttachment* fullAttachment = [attachment fetchFullAttachment];
		
		NSString* filename = [AttachmentDownloader fileNameForAccountNum:anAttachmentDownloader.accountNum 
                                                               folderNum:anAttachmentDownloader.folderNum 
                                                                     uid:anAttachmentDownloader.uid 
                                                           attachmentNum:anAttachmentDownloader.attachmentNum
                                                               extension:anAttachmentDownloader.extension];
		NSString* attachmentDir = [AttachmentDownloader attachmentDirPath];
		NSString* attachmentPath = [attachmentDir stringByAppendingPathComponent:filename];
		
		[fullAttachment writeToFile:attachmentPath];		
	} @catch (NSException *exp) {
		[anAttachmentDownloader deliverError:[NSString stringWithFormat:NSLocalizedString(@"Fetch error: %@", nil), [ImapFolderWorker decodeError:exp]]];
		[ActivityIndicator off];
		[account disconnect];
		[pool release];
		return;
	}
    [account disconnect];
	[anAttachmentDownloader deliverAttachment];
}


////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
	[string appendString:@"\n{\n"];
    
    [super description:string];

	[string appendString:@"}\n"];
	
	return string;
}

@end
#endif
