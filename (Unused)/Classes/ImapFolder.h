#ifdef FEATURE_IMAP
/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "BaseFolder.h"

@class ImapAccount;

@interface ImapFolder : BaseFolder {
    NSString*   path;

    int         numSynced;
    int         newSyncStart;
    int         oldSyncStart;
    int         seqDelta; 
}

@property (retain) NSString*        path;

@property int numSynced;
@property int newSyncStart;
@property int oldSyncStart;
@property int seqDelta;

// Construct
- (id)initWithAccount:(ImapAccount*)anAccount;

// Load/Store
- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary;


@end
#endif
