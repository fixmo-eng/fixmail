/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/* This code originates from a StackOverflow question on how to get the TTMessageController to work
 * http://stackoverflow.com/questions/5374684/how-to-use-three20-ttmessagecontroller
 */
//#import <AddressBookUI/AddressBookUI.h>

#import "AddressBookDataSource.h"
#import "AddressBookModel.h"
#import "EmailAddress.h"

@implementation AddressBookDataSource

// Properties
@synthesize addressBook = _addressBook;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init 
{
    if (self = [super init]) {
        _addressBook = [AddressBookModel new];
        self.model = _addressBook;
    }
    return self;
}


////////////////////////////////////////////////////////////////////////////////////////////
// TTTableViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark TTTableViewDataSource

- (void)tableViewDidLoadModel:(UITableView*)tableView 
{    
    self.items = [NSMutableArray new];
    NSArray* aResults = ((AddressBookModel *)self.model).searchResults;
    for(EmailAddress* anEmailAddress in aResults) {
        //FXDebugLog(kFXLogActiveSync, @"tableViewDidLoadModel: %@", anEmailAddress);
        NSString* aString = [NSString stringWithFormat:@"%@ %@", [anEmailAddress name], [anEmailAddress address]];
        TTTableItem* item = [TTTableSubtitleItem itemWithText:aString subtitle:nil];
        [_items addObject:item];
    }
} 

- (void)search:(NSString*)text 
{
    [_addressBook search:text];
}

- (NSString*)titleForLoading:(BOOL)reloading 
{
    return @"Searching...";
}

- (NSString*)titleForNoData {
    return @"No names found";
}

@end
