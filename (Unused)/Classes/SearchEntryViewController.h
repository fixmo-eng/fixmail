//
//  SearchEntryViewController.h
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

@interface SearchEntryViewController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate> {
	BOOL autocompleting; // this is YES if autocompleting is currently running ...
	NSArray *autocompletions;
	NSArray *queryHistory;
	NSMutableArray *dates;
	NSArray *types;
	NSString* lastSearchString;
}

-(void)goFTSearch:(NSString*)queryText;
-(void)goSenderSearch:(NSString*)senderName withParams:(NSDictionary*)params;
-(void)doLoad;

@property (assign) BOOL autocompleting;
@property (nonatomic, strong) NSArray *autocompletions;
@property (nonatomic, strong) NSArray *queryHistory;
@property (nonatomic, strong) NSMutableArray *dates;
@property (nonatomic, strong) NSArray *types;
@property (nonatomic, strong) NSString* lastSearchString;
@end
