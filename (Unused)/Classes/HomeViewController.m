//
//  HomeViewController.m
//  Displays home screen to user, manages toolbar UI and responds to sync status updates
//
//  Created by Gabor Cselle on 1/22/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "HomeViewController.h"
#import "AccountViewController.h"
#import "BaseAccount.h"
#import "AppSettings.h"
#import "ErrorViewController.h"
#import "FixMailAppDelegate.h"
#import "GlobalDBFunctions.h"
#import "MailboxViewController.h"
#import "PastQuery.h"
#import "ProgressView.h"
#import "SearchEntryViewController.h"
#import "SearchRunner.h"
#import "SettingsListViewController.h"
#import "StatusViewController.h"
#import "SyncManager.h"


// Globals
extern BOOL gIsFixTrace;

////////////////////////////////////////////////////////////////////////////////
// HomeViewController
////////////////////////////////////////////////////////////////////////////////
#pragma mark HomeViewController

@implementation HomeViewController

@synthesize goToFolders;

static BOOL loaded                              = NO;

// Artwork disclosure:
// Search icon is from:    http://www.icons-land.com/
// Settings icon is from:  http://www.iconspedia.com/icon/settings-1625.html

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct


////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
	[super viewDidUnload];
	
	//self.clientMessage = nil;
	//self.clientMessageButton = nil;
}

-(void)viewDidAppear:(BOOL)animated 
{
	if(!loaded) {
        [self loadIt];
		loaded = YES;
	}
	    
    if(goToFolders) {
        [self foldersClick:self];
        goToFolders = FALSE;
    }
	
	return;
}

-(void)viewWillAppear:(BOOL)animated 
{
	if(loaded && animated) {
		// Cancel SearchRunner for the case where we just came back from "All Mail" and are about to quickly dive back
		// animated will be true in this case because we're diving up
		SearchRunner *sem = [SearchRunner getSingleton];
		[sem cancel];
	}
	
	[self.navigationController setToolbarHidden:NO animated:animated];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.168 green:0.5 blue:0.741 alpha:1.0];
    self.navigationItem.title = @"fixMail";
    
    // Bottom tool bar
    //
	self.navigationController.toolbarHidden = NO;
    self.navigationController.toolbar.tintColor = [UIColor blackColor]; //[UIColor colorWithRed:0.168 green:0.5 blue:0.741 alpha:1.0];

    // Progress view
    //
    UIBarButtonItem* refreshButton  = [AccountViewController refreshButton];
	UIBarButtonItem* progressItem   = [ProgressView barButtonItem];
	UIBarButtonItem* statusButton   = [AccountViewController statusButton];

	self.toolbarItems = [NSArray arrayWithObjects:refreshButton,progressItem,statusButton,nil];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	FXDebugLog(kFXLogActiveSync, @"SplashScreenViewController received memory warning");
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return YES;
}

////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions


-(IBAction)foldersClick:(id)sender 
{
    if(!gIsFixTrace) {
        AccountViewController *vc = nil;
		if (IS_IPAD()) vc = [UIStoryboard storyboardWithName:kStoryboard_Accounts_iPad identifier:@"AccountViewController"];
		else vc = [UIStoryboard storyboardWithName:kStoryboard_Accounts_iPhone identifier:@"AccountViewController"];
		vc.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
        [self.navigationController pushViewController:vc animated:(sender != nil)];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME gIsFixTrace");
    }
}

-(IBAction)accountListClick:(id)sender 
{
	SettingsListViewController *alvc = [[SettingsListViewController alloc] initWithNibName:@"SettingsList" bundle:nil];
	alvc.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
	[self.navigationController pushViewController:alvc animated:YES];
}

-(IBAction)searchClick:(id)sender 
{
	NSArray* nibContents = [[NSBundle mainBundle] loadNibNamed:@"SearchEntryView" owner:self options:NULL];
	NSEnumerator *nibEnumerator = [nibContents objectEnumerator]; 
	SearchEntryViewController *uivc = nil;
	NSObject* nibItem = nil;
    while ( (nibItem = [nibEnumerator nextObject]) != NULL) { 
        if ( [nibItem isKindOfClass: [SearchEntryViewController class]]) { 
			uivc = (SearchEntryViewController*) nibItem;
			break;
		}
	}

	if(uivc == nil) {
		return;
	}
	
	uivc.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
	
	[uivc doLoad];
	[self.navigationController pushViewController:uivc animated:(sender != nil)];
}


- (void)loadIt 
{
    @autoreleasepool {

        loaded = YES;
        
        [GlobalDBFunctions tableCheck];


        if([AppSettings firstSync]) {
            //[ssm requestSyncIfNoneInProgressAndConnected];
            [AppSettings setFirstSync:NO];
        }
    }
}

- (BOOL)shouldLoadExternalURL:(NSURL*)url 
{
	return YES;
}

@end
