//
//  MailViewController.h
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "ASSyncMessageDelegate.h"
#import "SZCSeamlessHeaderWebView.h"

@class BaseEmail;
@class BaseFolder;

@interface MailViewController : SZCSeamlessHeaderWebView <UIActionSheetDelegate, UIAlertViewDelegate, ASSyncMessageDelegate>
{
	BOOL                    loaded;
	
	id                      deleteDelegate; // call emailDeleted:pk on this delegate if an email was deleted
	
	BOOL                    isSenderSearch; // sender will be highlighted if this is YES
	BOOL                    copyMode;
    int                     bodyTop;
    
	NSString*               query; // if !isSenderSearch, terms in here will be highlighted in sender/attachment/body

    NSArray*                attachmentMetadata;
    
    // UI
	UIBarButtonItem*        replyButton;
	IBOutlet UILabel*       fromLabel;
	IBOutlet UILabel*       toLabel;
	IBOutlet UILabel*       ccLabel;
	IBOutlet UILabel*       subjectLabel;
	IBOutlet UILabel*       dateLabel;
	IBOutlet UIImageView*   unreadIndicator;
	IBOutlet UIScrollView*  scrollView;
  
	UILabel*      subjectTTLabel;
	UILabel*      bodyTTLabel;
	UITextView*             subjectUIView;
    
    NSMutableArray*         bodyViews;

    // Activity Indicator
    UIActivityIndicatorView* loadingIndicator;
    UILabel*                loadingLabel;
	
    // Copy Mode
	UIButton*               copyModeButton;
	UILabel*                copyModeLabel;
}


// Properties
@property (nonatomic, strong) BaseFolder*           folder;
@property (nonatomic, strong) BaseEmail*            email;

@property (nonatomic, strong) id                    deleteDelegate;

@property (assign) BOOL                             isSenderSearch; // YES if we're doing senderQuery
@property (assign) BOOL                             copyMode;
@property (nonatomic, strong) NSString*             query;

@property (nonatomic, strong) NSArray*              attachmentMetadata;

// UI Properties
@property (nonatomic, strong) UIBarButtonItem*      replyButton;

@property (nonatomic, strong) UIView*              fromView;
@property (nonatomic, strong) UIView*              toView;
@property (nonatomic, strong) UIView*              ccView;
@property (nonatomic, strong) UIView*              subjectView;

@property (nonatomic, strong) UILabel*              fromLabel;
@property (nonatomic, strong) UILabel*              toLabel;
@property (nonatomic, strong) UILabel*              ccLabel;
@property (nonatomic, strong) UILabel*              subjectLabel;
@property (nonatomic, strong) UILabel*              dateLabel;
@property (nonatomic, strong) UIImageView*          unreadIndicator;
@property (nonatomic, strong) UIScrollView*         scrollView;

@property (nonatomic, strong) UIButton*             copyModeButton;
@property (nonatomic, strong) UILabel*              copyModeLabel;

@property (nonatomic, strong) UILabel*    subjectTTLabel;
@property (nonatomic, strong) UILabel*    bodyTTLabel;
@property (nonatomic, strong) UITextView*           subjectUIView;

// Actions
-(IBAction)replyButtonWasPressed;

// Interface
-(void)composeViewWithSubject:(NSString*)subject body:(NSString*)body to:(NSArray*)to cc:(NSArray*)cc includeAttachments:(BOOL)includeAttachments;

-(NSString*)markupText:(NSString*)text query:(NSString*)query beginDelim:(NSString*)beginDelim endDelim:(NSString*)endDelim;
-(BOOL)matchText:(NSString*)text withQuery:(NSString*)queryLocal;
-(NSString*)massageDisplayString:(NSString*)y;

@end
