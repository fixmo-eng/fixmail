/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/* This code originates from a StackOverflow question on how to get the TTMessageController to work
 * http://stackoverflow.com/questions/5374684/how-to-use-three20-ttmessagecontroller
 */
#import "AddressBookModel.h"
#import "EmailAddress.h"
#import "SearchRunner.h"

@implementation AddressBookModel

// Properties
@synthesize searchResults = _searchResults;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)init 
{
    if (self = [super init]) {
        _delegates = nil;
        _searchResults = nil;
    }
    return self;
}


- (void)_logException:(NSString*)where exception:(NSException*)e
{
	FXDebugLog(kFXLogActiveSync, @"Exception: AddressBookModel %@ %@: %@", where, [e name], [e reason]);
}

////////////////////////////////////////////////////////////////////////////////////////////
// TTModel
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark TTModel

- (NSMutableArray*)delegates 
{
    if (!_delegates) {
        _delegates = TTCreateNonRetainingArray();
    }
    return _delegates;
}

- (BOOL)isLoadingMore 
{
    return NO;
}

- (BOOL)isOutdated 
{
    return NO;
}

- (BOOL)isLoaded 
{
    return YES;
}

- (BOOL)isLoading 
{
    return NO;
}

- (BOOL)isEmpty 
{
    return !_searchResults.count;
}

- (void)load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more 
{
}

- (void)invalidate:(BOOL)erase 
{
}

- (void)cancel 
{
    // TTSearchField doesn't implement modelDidCancelLoad so don't call it, it will throw an exception
    //
    //@try {
    //    [_delegates makeObjectsPerformSelector:@selector(modelDidCancelLoad:) withObject:self];
    //}@catch (NSException* e) {
    //    [self _logException:@"AddressBookModel cancel" exception:e];
    //}
}

- (void)_deliverAutocompleteResult:(NSArray*)aResults
{
    [_delegates makeObjectsPerformSelector:@selector(modelDidStartLoad:) withObject:self];

    NSMutableArray* aResultArray = [NSMutableArray arrayWithCapacity:[aResults count]];

    @try {        
        for(NSDictionary* aResult in aResults) {
            NSString* aName = [aResult objectForKey:@"name"];
            if([aName length] > 0) {
            }else{
                aName = @"";
            }
            NSString* anAddressStrings = [aResult objectForKey:@"emailAddresses"];
            NSArray* anAddressArray = [anAddressStrings componentsSeparatedByString:@","];
            for(__strong NSString* anAddressString in anAddressArray) {
#warning FIXME String cleaning
                // this is awful, clean these strings before they go in to contact database and deliver them properly formatted
                // from SearchRunner
                anAddressString = [anAddressString stringByReplacingOccurrencesOfString:@"'" withString:@""];
                anAddressString = [anAddressString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                anAddressString = [NSString stringWithFormat:@"<%@>", anAddressString];
                EmailAddress* anEmailAdress = [[EmailAddress alloc] initWithName:aName address:anAddressString];
                [aResultArray addObject:anEmailAdress]; 
            }
        }
    }@catch (NSException* e) {
        [self _logException:@"AddressBookModel search" exception:e];
    }
    if(aResultArray.count > 0) {
        self.searchResults = aResultArray;
    }else{
        self.searchResults = nil;
    }
    [_delegates makeObjectsPerformSelector:@selector(modelDidFinishLoad:) withObject:self];
    [_delegates makeObjectsPerformSelector:@selector(modelDidChange:) withObject:self];
}

- (void)deliverAutocompleteResult:(NSArray*)aResults
{
    [self performSelectorOnMainThread:@selector(_deliverAutocompleteResult:) withObject:aResults waitUntilDone:FALSE];
}

- (void)search:(NSString*)aText 
{
    [self cancel];

    @try {
        if (aText.length > 0) {
            NSRange anAtRange = [aText rangeOfString:@"@"];
            if(anAtRange.location != NSNotFound && anAtRange.location < [aText length]-1) {
                // User appears to be typing out the email address himself so stop searching
                // and just put what he is typing in the picker
                //
                [_delegates makeObjectsPerformSelector:@selector(modelDidStartLoad:) withObject:self];
                EmailAddress* anAddress = [[EmailAddress alloc] initWithName:aText address:aText];
                NSArray* aResultArray = [NSArray arrayWithObject:anAddress]; 
                self.searchResults = aResultArray;
                [_delegates makeObjectsPerformSelector:@selector(modelDidFinishLoad:) withObject:self];
                [_delegates makeObjectsPerformSelector:@selector(modelDidChange:) withObject:self];
            }else{
                // Start a search against the contacts database
                //
                [[SearchRunner getSingleton] autocomplete:[NSString stringWithFormat:@"%@*", aText] withDelegate:self];
            }
        } else {
            [_delegates makeObjectsPerformSelector:@selector(modelDidStartLoad:) withObject:self];
            self.searchResults = nil;
            [_delegates makeObjectsPerformSelector:@selector(modelDidFinishLoad:) withObject:self];
            [_delegates makeObjectsPerformSelector:@selector(modelDidChange:) withObject:self];
        }
    }@catch (NSException* e) {
        [self _logException:@"AddressBookModel search" exception:e];
    }
} 

@end