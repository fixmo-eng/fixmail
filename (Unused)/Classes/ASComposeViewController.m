/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ASComposeViewController.h"
#import "ASCompose.h"
#import "ASEmail.h"
//#import "PKMIMEMessage.h"

@implementation ASComposeViewController

@synthesize account;

////////////////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithTo:(NSArray *)aTos cc:(NSArray*)aCCs account:(BaseAccount*)anAccount
{
	if (IS_IPAD()) {
		self = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPad identifier:@"ASComposeViewController"];
	} else {
		self = [UIStoryboard storyboardWithName:kStoryboard_ComposeMail_iPhone identifier:@"ASComposeViewController"];
	}

    if (self) {
		self.toList			= [NSMutableArray arrayWithArray:aTos];
		self.ccList			= [NSMutableArray arrayWithArray:aCCs];
		self.bccList		= [[NSMutableArray alloc] initWithCapacity:1];
        self.account		= (ASAccount*)anAccount;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.toList			= [NSMutableArray array];
		self.ccList			= [NSMutableArray array];
		self.bccList		= [[NSMutableArray alloc] initWithCapacity:1];
		self.account		= nil;
    }
    return self;
}

- (void) addRecipient:(NSString *)inEmailAddress forFieldAtIndex:(NSUInteger)inIndex
{
	if (inEmailAddress.length == 0) return;
	if (inIndex >= self.toList.count) {
		[self.toList addObject:inEmailAddress];
	} else {
		[self.toList insertObject:inEmailAddress atIndex:inIndex];
	}

	return;
}

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [self handleError:[NSString stringWithFormat:@"Exception: %@ %@: %@", where, [e name], [e reason]]];
}

- (void)handleError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:@"Compose" 
                                                          message:anErrorMessage 
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel" 
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// ASComposeDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ASComposeDelegate

- (void)sendMailSuccess
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)sendMailFailed:(NSString*)aMessage
{
    [self handleError:aMessage];
}

////////////////////////////////////////////////////////////////////////////////////////////
// MessageControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MessageControllerDelegate

/*
- (NSString*)stringFromRecipientArray:(NSArray*)anObjects
{
    NSMutableString* aRecipientString = [NSMutableString stringWithCapacity:0];
    NSUInteger aCount = anObjects.count;
    for(NSUInteger i = 0 ; i < aCount ; ++i) {
        NSObject* anObject = [anObjects objectAtIndex:i];
        if([anObject isKindOfClass:[TTTableTextItem class]]) {
            TTTableTextItem* anItem = (TTTableTextItem*)anObject;
            //FXDebugLog(kFXLogActiveSync, @"Recipient: %@", [anItem text]);
            [aRecipientString appendString:[anItem text]];
        }else if([anObject isKindOfClass:[NSString class]]) {
            NSString* aString = (NSString*)anObject;
            [aRecipientString appendString:aString];
        }else{
            FXDebugLog(kFXLogActiveSync, @"stringFromRecipientArray recipient unrecognized: %@", anObject);
        }
        if(i < aCount-1) {
            [aRecipientString appendString:@","];
        }
    }
    return aRecipientString;
}


- (NSString*)getAddressString:(MessageField*)aField
{
    NSString* aRecipientString;

    if([aField isKindOfClass:[MessageRecipientField class]]) {
        MessageRecipientField* aRecipientTextField = (MessageRecipientField*)aField;
        NSArray* anItems = [aRecipientTextField recipients];
        aRecipientString = [self stringFromRecipientArray:anItems];
    }else if([aField isKindOfClass:[MessageCCField class]]) {
        MessageCCField* aRecipientTextField = (MessageCCField*)aField;
        NSArray* anItems = [aRecipientTextField recipients];
        aRecipientString = [self stringFromRecipientArray:anItems];
    }else{
        FXDebugLog(kFXLogActiveSync, @"getAddressString field: %@", aField);
        aRecipientString = @"";
    }
    return aRecipientString;
}

- (void)composeController:(MessageController*)aController didSendFields:(NSArray*)aFields
{
    @try {
        ASEmail* anEmail = [[ASEmail alloc] initWithAccount:account];
        
        anEmail.from = [account name];
        anEmail.replyTo = [account emailAddress];
         
        NSUInteger aCount = [aFields count];
        for (NSUInteger i = 0 ; i < aCount ; ++i) {
            MessageField* aField = [aFields objectAtIndex:i];
            NSString* aFieldTitle = [aField title];
            if([aFieldTitle isEqualToString:@"To:"]) {
                [anEmail setToFromAddressString:[self getAddressString:aField]];
            }else if([aFieldTitle isEqualToString:@"Cc:"]) {
                [anEmail setCCFromAddressString:[self getAddressString:aField]];
            }else if([aFieldTitle isEqualToString:@"Bcc:"]) {
                [anEmail setBCCFromAddressString:[self getAddressString:aField]];
            }else if([aFieldTitle isEqualToString:@"Subject:"]) {
                anEmail.subject = [self subject];
            }else if([aFieldTitle length] == 0) {
                NSString* aBody = [self body];
                anEmail.body = [NSData dataWithBytes:[aBody UTF8String] length:[aBody length]];
            } else{
//               FXDebugLog(kFXLogFIXME, @"FIXME Unknown compose field: %@ %@ %@", [aField title], [self textForFieldAtIndex:i], aField);
            }
        }
        //FXDebugLog(kFXLogActiveSync, @"didSendFields:\n%@", anEmail);
        NSString* aMIMEString = [anEmail asMIMEString];
        
        // Send it
        //
        NSData* aMIMEData = [[NSData alloc] initWithBytes:[aMIMEString UTF8String] length:[aMIMEString length]];
        [ASCompose sendMail:account mime:aMIMEData delegate:self];   
        
    }@catch (NSException* e) {
        [self _logException:@"didSendFields" exception:e];
    }
}

- (void)composeControllerWillCancel:(MessageController*)aController
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (void)composeControllerShowRecipientPicker:(MessageController*)aController
{
}
*/
@end

