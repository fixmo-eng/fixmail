#ifdef FEATURE_IMAP
/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "ImapFolder.h"
#import "ImapAccount.h"
#import "SyncManager.h"

@implementation ImapFolder

// Constants
static NSString* kFolderPath    = @"folderPath";
static NSString* kNumSynced     = @"numSynced";
static NSString* kNewSyncStart  = @"newSyncStart";
static NSString* kOldSyncStart  = @"oldSyncStart";
static NSString* kSeqDelta      = @"seqDelta";

// Properties
@synthesize path;

@synthesize numSynced;
@synthesize newSyncStart;
@synthesize oldSyncStart;
@synthesize seqDelta;

////////////////////////////////////////////////////////////////////////////////
// Construct/Destruct
////////////////////////////////////////////////////////////////////////////////
#pragma mark Construct/Destruct

- (id)initWithAccount:(ImapAccount*)anAccount
{
    if (self = [super initWithAccount:anAccount]) {
        [self setPath:@""];
        numSynced       = 0;
        newSyncStart    = 0;
        oldSyncStart    = 0;
        seqDelta        = 0;
    }
    
    return self;
}

- (void)dealloc
{    
    [super dealloc];
}

////////////////////////////////////////////////////////////////////////////////
// Load/Save
////////////////////////////////////////////////////////////////////////////////
#pragma mark Load/Save

- (void)loadFromStore:(NSDictionary *)aDictionary
{
    [self setPath:[aDictionary objectForKey:kFolderPath]];
    if([path length] == 0) {
        FXDebugLog(kFXLogFIXME, @"FIXME folder path invalid");
    }
    
    numSynced       = [self getInt:aDictionary key:kNumSynced];
    newSyncStart    = [self getInt:aDictionary key:kNewSyncStart];
    oldSyncStart    = [self getInt:aDictionary key:kOldSyncStart];
    seqDelta        = [self getInt:aDictionary key:kSeqDelta];
    
    [super loadFromStore:aDictionary];
}

- (void)commit
{
    SyncManager* aSyncManager = [SyncManager getSingleton];
	NSMutableDictionary* aSyncState = [aSyncManager retrieveState:folderNum accountNum:accountNum];
    
    [self folderAsDictionary:aSyncState];
    [aSyncManager persistState:aSyncState forFolderNum:folderNum accountNum:accountNum];
}

- (void)folderAsDictionary:(NSMutableDictionary*)aDictionary
{
    [aDictionary setObject:path          forKey:kFolderPath];
    
    [aDictionary setObject:[NSNumber numberWithInt:numSynced]    forKey:kNumSynced];
    [aDictionary setObject:[NSNumber numberWithInt:newSyncStart] forKey:kNewSyncStart];
    [aDictionary setObject:[NSNumber numberWithInt:oldSyncStart] forKey:kOldSyncStart];
    [aDictionary setObject:[NSNumber numberWithInt:seqDelta]     forKey:kSeqDelta];
}

////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////
#pragma mark Debug

- (NSString*)description
{
	NSMutableString* string = [NSMutableString stringWithString:[super description]];
    
    [self writeString:string		tag:kFolderPath     value:path];

    [self writeInt:string   tag:kNumSynced      value:numSynced];
    [self writeInt:string   tag:kNewSyncStart   value:newSyncStart];
    [self writeInt:string   tag:kOldSyncStart   value:oldSyncStart];
    [self writeInt:string	tag:kSeqDelta       value:seqDelta];

	[string appendString:@"}\n"];
	
	return string;
}

@end
#endif
