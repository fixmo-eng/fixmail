//
//  MailViewController.m
//  ReMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "MailViewController.h"
#import "AttachmentDownloader.h"
#import "AttachmentViewController.h"
#import "BaseAccount.h"
#import "BaseEmail.h"
#import "BaseFolder.h"
#import "DateUtil.h"
#import "EmailProcessor.h"
#import "MeetingResponseSheet.h"
#import "MemUtil.h"
#import "MeetingRequest.h"
#import "NSString+SBJSON.h"
#import "SearchRunner.h"
#import "SyncManager.h"
#import "WebViewController.h"
#import "composeMailVC.h"

#import "ASAttachment.h"

#import "PKContentType.h"       // by Portable Knowledge
#import "PKMIMEData.h"
#import "PKMIMEMessage.h"

#import "SZCFlowLayoutView.h"
#import "SZCAddressButton.h"

@interface UIView (FU)
@property (nonatomic, readonly) CGFloat bottom;
@property (nonatomic, readonly) CGFloat right;
@property (nonatomic, readonly) CGSize size;
@end

@implementation UIView (FU)

-(CGSize)size
{
    return self.bounds.size;
}

-(CGFloat)right
{
    return self.frame.origin.x + self.frame.size.width;
}

-(CGFloat)bottom
{
    return self.frame.origin.y + self.frame.size.height;
}
@end

////////////////////////////////////////////////////////////////////////////////
// MailViewController (Private)
////////////////////////////////////////////////////////////////////////////////
#pragma mark MailViewController (Private)

@class PersonActionSheetHandler;
@interface MailViewController ()

@property (nonatomic, strong) PersonActionSheetHandler *pash;

@end

////////////////////////////////////////////////////////////////////////////////
// PersonActionSheetHandler
////////////////////////////////////////////////////////////////////////////////
#pragma mark PersonActionSheetHandler

@interface PersonActionSheetHandler : NSObject<UIActionSheetDelegate> {
	NSString* name;
	NSString* address;
	
	MailViewController* mailVC;
}

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* address;
@property (nonatomic, strong) MailViewController* mailVC;
@end

@implementation PersonActionSheetHandler
@synthesize name;
@synthesize address;
@synthesize mailVC;

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == 0) { //Compose Email To
		[self.mailVC composeViewWithSubject:@"" body:@"" to:[NSArray arrayWithObject:self.address] cc:nil includeAttachments:NO];
	} else if (buttonIndex == 1) { // Copy Address
		UIPasteboard* pasteBoard = [UIPasteboard generalPasteboard];
		pasteBoard.string = self.address;
	} else if (buttonIndex == 2) { // Copy Name
		UIPasteboard* pasteBoard = [UIPasteboard generalPasteboard];
		pasteBoard.string = self.name;
	}
}
@end


////////////////////////////////////////////////////////////////////////////////
// MailViewController
////////////////////////////////////////////////////////////////////////////////
#pragma mark MailViewController
@implementation MailViewController

// Properties
@synthesize folder;
@synthesize attachmentMetadata;
@synthesize email;

@synthesize copyMode;
@synthesize isSenderSearch;
@synthesize query;
@synthesize unreadIndicator;
@synthesize scrollView;
@synthesize fromLabel;
@synthesize toLabel;
@synthesize ccLabel;
@synthesize replyButton;
@synthesize subjectLabel;
@synthesize dateLabel;
@synthesize deleteDelegate;

@synthesize subjectTTLabel;
@synthesize bodyTTLabel;
@synthesize subjectUIView;

@synthesize pash;

// Constants
static const int kDefaultAudioAttachmentHeight          = 24;
static const int kDefaultApplicationAttachmentHeight    = 800;
static const int kDefaultVideoAttachmentHeight          = 800;

static const CGFloat kButtonOffsetX                     = 20.0f;
static const CGFloat kButtonOffsetY                     = 5.0f;
static const CGFloat kMeetingViewHeight                 = 40.0f;


////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

-(void)viewDidLoad 
{
    [super viewDidLoad];
	loaded = NO;
	self.copyMode = NO;	
}

- (void)viewDidUnload 
{
	[super viewDidUnload];
	
    self.folder = nil;
	self.email = nil;
	
	self.fromLabel = nil;
	self.toLabel = nil;
	self.ccLabel = nil;
	self.scrollView = nil;
	self.replyButton = nil;
	
	self.copyModeButton = nil;
	self.copyModeLabel = nil;
	
	self.subjectLabel = nil;
	self.dateLabel = nil;
	self.unreadIndicator = nil;
	
	self.subjectTTLabel = nil;
	self.bodyTTLabel = nil;
	self.subjectUIView = nil;

	[bodyViews removeAllObjects];
	bodyViews = nil;

	self.attachmentMetadata = nil;
}

-(void)viewWillAppear:(BOOL)animated 
{
    self.headerView = [[SZCFlowLayoutView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 164)];
    ((SZCFlowLayoutView*)self.headerView).vPadding = 0;
    ((SZCFlowLayoutView*)self.headerView).hPadding = 0;
    ((SZCFlowLayoutView*)self.headerView).vSpacing = 1;
    ((SZCFlowLayoutView*)self.headerView).hSpacing = 0;
    
    self.headerView.backgroundColor = [UIColor darkGrayColor];

    self.fromView = [[SZCFlowLayoutView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    self.toView = [[SZCFlowLayoutView alloc] initWithFrame:CGRectMake(0, 36, self.view.frame.size.width, 35)];
    self.ccView  = [[SZCFlowLayoutView alloc] initWithFrame:CGRectMake(0, 72, self.view.frame.size.width, 35)];
    self.subjectView = [[SZCFlowLayoutView alloc] initWithFrame:CGRectMake(0, 108, self.view.frame.size.width, 55)];
    
    self.fromView.backgroundColor = self.toView.backgroundColor = self.ccView.backgroundColor = self.subjectView.backgroundColor = [UIColor whiteColor];
    
    self.fromLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 22)];
    self.fromLabel.text = @"From:";
    [self.fromLabel sizeToFit];
    
    self.toLabel =  [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 22)];
    self.toLabel.text = @"To:";
    [self.toLabel sizeToFit];
    
    self.ccLabel =  [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 22)];
    self.ccLabel.text = @"CC:";
    [self.ccLabel sizeToFit];
    
    [self.fromView addSubview:self.fromLabel];
    [self.toView addSubview:self.toLabel];
    [self.ccView addSubview:self.ccLabel];
    
    [self.headerView addSubview:self.fromView];
    [self.headerView addSubview:self.toView];
    [self.headerView addSubview:self.ccView];
    [self.headerView addSubview:self.subjectView];
    
	[super viewWillAppear:YES];
    
    if(!bodyViews) {
        bodyViews = [[NSMutableArray alloc] initWithCapacity:2];
    }
	if(!loaded) {
#if 1
		NSThread *driverThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadEmail) object:nil];
		[driverThread start];
		loaded = YES;
#else
        // Transitional hack to use email object if available, instead of realoading from database
        // Not ready for prime time
        //
        if(email) {
            [self validateAndLoadEmail:email folder:folder];
        }else{
            NSThread *driverThread = [[NSThread alloc] initWithTarget:self selector:@selector(loadEmail) object:nil];
            [driverThread start];
            [driverThread release];	
        }
        loaded = YES;
#endif
	}
	
	// hide toolbar so we have more space to display mail
	[self.navigationController setToolbarHidden:YES animated:animated];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
	FXDebugLog(kFXLogActiveSync, @"MailViewController received memory warning");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return YES;
}

- (void)_loadingActivityIndicator:(UIView*)aView
{
    if(!loadingIndicator) {
        loadingIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(100, 160, 20, 20)];
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        loadingIndicator.hidesWhenStopped = NO;
    }
	[aView addSubview:loadingIndicator];
	[loadingIndicator startAnimating];
	
    if(!loadingLabel) {
        loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(127, 160, 90, 20)];
        loadingLabel.font       = [UIFont systemFontOfSize:14];
        loadingLabel.textColor  = [UIColor darkGrayColor];	
        loadingLabel.text       = NSLocalizedString(@"Loading email", nil);
    }
	[aView addSubview:loadingLabel];
}

- (void)_removeActivityIndicator
{
    if(loadingIndicator) {
        [loadingIndicator stopAnimating];
        [loadingIndicator removeFromSuperview];
    }
    if(loadingLabel) {
        [loadingLabel removeFromSuperview];
    }
}

/*
- (void)loadView
{
	self.view = [[[UIView class] alloc] initWithFrame:CGRectMake(0,0,320,480)];
	self.view.backgroundColor = [UIColor blackColor];
    
	// This is our real view, dude
	self.scrollView = [[[UIScrollView class] alloc] initWithFrame:CGRectMake(0,0,320,480)];
	self.scrollView.backgroundColor                 = [UIColor whiteColor];
	self.scrollView.canCancelContentTouches         = NO;
	self.scrollView.showsVerticalScrollIndicator    = YES;
	self.scrollView.showsHorizontalScrollIndicator  = NO;
	self.scrollView.alwaysBounceVertical            = YES;
	[self.view addSubview:self.scrollView];
	
    [self _loadingActivityIndicator:self.scrollView];
	
	self.scrollView.contentSize = CGSizeMake(320, 180);
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	self.title = @"eMail";
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(replyButtonWasPressed)];
}
*/
////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [self handleError:[NSString stringWithFormat:@"Exception: %@ %@: %@", where, [e name], [e reason]]];
}

- (void)handleError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:@"Mail view error"
                                                          message:anErrorMessage 
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel" 
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////////////////
// UIAlertView delegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIAlertView delegate

- (void)alertViewCancel:(UIAlertView *)alertView
{
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

- (UIButton*)copyModeButton
{
    return copyModeButton;
}

- (void)setCopyModeButton:(UIButton*)aCopyModeButton
{
    copyModeButton = aCopyModeButton;
}

- (UILabel*)copyModeLabel
{
    return copyModeLabel;
}

- (void)setCopyModeLabel:(UILabel*)aCopyModeLabel
{
    copyModeLabel = aCopyModeLabel;
}

-(void)personButtonClicked:(SZCAddressButton*)target
{
	NSString *name = [target titleForState:UIControlStateNormal];
	NSString *address = [target titleForState:UIControlStateDisabled];
	
	NSString* title = address;
	if (![name isEqualToString:address]) {
		title = [NSString stringWithFormat:@"%@ <%@>", name, address];
	}
	
    self.pash = [[PersonActionSheetHandler alloc] init]; // this doesn't get released but the actionsheet does not retain it either!
	pash.name = name;
	pash.address = address;
	pash.mailVC = self;
	
	UIActionSheet* aS = [[UIActionSheet alloc] initWithTitle:title delegate:pash cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:nil otherButtonTitles:
							NSLocalizedString(@"Compose Email To",nil), NSLocalizedString(@"Copy Address",nil), NSLocalizedString(@"Copy Name",nil), nil];
	[aS showInView:self.view];
}

- (IBAction)replyButtonWasPressed {
	UIActionSheet* aS = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) destructiveButtonTitle:NSLocalizedString(@"Delete", nil) otherButtonTitles:NSLocalizedString(@"Reply", nil), 
						 NSLocalizedString(@"Reply All", nil), 
						 NSLocalizedString(@"Forward", nil), 
						 nil];
	[aS showInView:self.view];
}

////////////////////////////////////////////////////////////////////////////////////////////
// Reply
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Reply

-(NSMutableArray*)emailAddressesForJson:(NSString*)json 
{
	if(json == nil || [json length] <= 4) {
		return [NSMutableArray array];
	}
	
	NSArray* peopleList = [json JSONValue];
	NSMutableArray* res = [NSMutableArray arrayWithCapacity:[peopleList count]];
	for (NSDictionary* person in peopleList) {
		NSString* address = [person objectForKey:@"e"];
		if (address != nil && [address length] > 0) {
			[res addObject: address];
		}
	}
	return res;
}

- (NSString *)_plainTextFromHTML:(NSString *)anHtml 
{    
    // This isn't a very good HTML flattener, may need to use DTCoreText instead?
    //
    NSScanner* aScanner;
    NSString* aText = nil;
    aScanner = [NSScanner scannerWithString:anHtml];
    
    while ([aScanner isAtEnd] == NO) {
        [aScanner scanUpToString:@"<" intoString:NULL] ; 
        [aScanner scanUpToString:@">" intoString:&aText] ;
        anHtml = [anHtml stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", aText] withString:@""];
    }

    anHtml = [anHtml stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return anHtml;
}

-(NSString*)replyString 
{
	if(self.email.body == nil || [self.email.body length] == 0) {
		return @"";
	}
	
	NSDate* date = self.email.datetime;
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	NSString* dateString = [dateFormatter stringFromDate:date];

	[dateFormatter setDateStyle:NSDateFormatterNoStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	NSString* timeString = [dateFormatter stringFromDate:date];
	
	NSString *headerString;
	if(self.email.from != nil && [self.email.from length] > 0) {
		headerString = [NSString stringWithFormat:@"On %@ at %@, %@ <%@> wrote:", dateString, timeString, self.email.from, self.email.replyTo];
	} else {
		headerString = [NSString stringWithFormat:@"On %@ at %@, %@ wrote:", dateString, timeString, self.email.replyTo];
	}
	
    NSString* quotedBody = NULL;
    
    if([self.email isMIME]) {
        quotedBody = [self bodyAsString:self.email];
    }else if([self.email isHTML]) {
        NSString* aBodyAsString = [[NSString alloc] initWithData:self.email.body encoding:NSUTF8StringEncoding];
        quotedBody = [self _plainTextFromHTML:aBodyAsString];
    }else if([self.email isRTF]) {
        FXDebugLog(kFXLogFIXME, @"FIXME replyString from RTF:\n%@", self.email);
    }else{
        quotedBody = [[NSString alloc] initWithData:self.email.body encoding:NSUTF8StringEncoding];
    }
	
	
    return [NSString stringWithFormat:@"\n\n%@\n%@", headerString, quotedBody];
}

-(NSArray*)replyAllRecipients 
{
	NSMutableArray* recipients = [self emailAddressesForJson:self.email.toJSON];
	[recipients addObjectsFromArray:[self emailAddressesForJson:self.email.ccJSON]];
	[recipients addObjectsFromArray:[self emailAddressesForJson:self.email.bccJSON]];
	
    if([recipients count] == 0) {
        FXDebugLog(kFXLogFIXME, @"FIXME no recipients:\n%@", self.email);
    }
	return recipients;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Action Sheet
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Action Sheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	if (buttonIndex == 0) { // Delete
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete Message?",nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes", @"No", nil];
		[alertView show];	
		return;
	}
	
	BOOL keepSubject = NO;
	if(self.email.subject != nil && [self.email.subject length] >= 3) {
		NSString* subjectStart = [[self.email.subject substringToIndex:3] lowercaseString];

		if([subjectStart isEqualToString:@"r:"] || 
		   [subjectStart isEqualToString:@"re:"] || 
		   [subjectStart isEqualToString:@"aw:"]) {
			// If the subject line already starts with "Re:", don't edit it 
			// (that's equivalent to Gmail's behavior
			keepSubject = YES;
		}
	}
	
	if(buttonIndex == 1) { //Reply
		NSString* newSubject = self.email.subject;
		if(!keepSubject) {
			newSubject = [NSString stringWithFormat:@"Re: %@", self.email.subject];
		}
		
		
		[self composeViewWithSubject:newSubject
								body:[self replyString]
								  to:[NSArray arrayWithObject:self.email.replyTo]
								  cc:nil
				  includeAttachments:NO];
	} else if (buttonIndex == 2) { // Reply All
		NSString* newSubject = self.email.subject;
		if(!keepSubject) {
			newSubject = [NSString stringWithFormat:@"RE: %@", self.email.subject];
		}
		
		[self composeViewWithSubject:newSubject
								body:[self replyString]
								  to:[NSArray arrayWithObject:self.email.replyTo]
								  cc:[self.email ccAsArrayOfAddressStrings]
				  includeAttachments:NO]; //TODO(gabor): Look into In-Reply-To header
	} else if (buttonIndex == 3) { // Forward
		[self composeViewWithSubject:[NSString stringWithFormat:@"Fwd: %@", self.email.subject] 
								body:[self replyString]
								  to:nil 
								  cc:nil
				  includeAttachments:YES];
	} else if (buttonIndex == 0) { // Delete
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Delete Message?",nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes", @"No", nil];
		[alertView show];	
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// Compose
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Compose

-(void)addAttachments:(composeMailVC*)aComposeController
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	for (int i = 0; i < [self.attachmentMetadata count]; i++) {
		NSDictionary* attachment = [self.attachmentMetadata objectAtIndex:i];
		NSString* filename = [attachment objectForKey:@"n"];
		NSString* contentType = [attachment objectForKey:@"t"];
		
		if(filename == nil || [filename length] == 0) {
			continue;
		}
		
		int accountNumDC = [EmailProcessor accountNumForCombinedFolderNum:self.email.folderNum];
		int folderNumDC = [EmailProcessor folderNumForCombinedFolderNum:self.email.folderNum];
		
		NSString* filenameOnDisk = [AttachmentDownloader 
                                    fileNameForAccountNum:accountNumDC 
                                    folderNum:folderNumDC 
                                    uid:self.email.uid 
                                    attachmentNum:i
                                    extension:[filename pathExtension]];
		NSString* attachmentDir = [AttachmentDownloader attachmentDirPath];
		NSString* attachmentPath = [attachmentDir stringByAppendingPathComponent:filenameOnDisk];		
		BOOL fileExists = [fileManager fileExistsAtPath:attachmentPath];
		
		if(!fileExists) {
			continue;
		}
		
		NSData* data = [[NSData alloc] initWithContentsOfFile:attachmentPath];
		
#warning FIXME Compose Mail Add Attachments
        FXDebugLog(kFXLogFIXME, @"FIXME MailViewController addAttachments: %@", contentType);
		//[mailCtrl addAttachmentData:data mimeType:contentType fileName:filename];
        
		
		i++;
	}
}

- (UILabel *) recipientLabel:(NSString *)inName
{
	if (inName.length == 0) return nil;
	UILabel *theLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	theLabel.text = inName;
	theLabel.backgroundColor = [UIColor lightGrayColor];
	theLabel.textColor = [UIColor blackColor];
	[theLabel sizeToFit];

	return theLabel;
	
}

-(void)composeViewWithSubject:(NSString*)subject 
                         body:(NSString*)body 
                           to:(NSArray*)to 
                           cc:(NSArray*)cc 
           includeAttachments:(BOOL)includeAttachments 
{
    BaseAccount* anAccount = NULL;
    if(folder) {
        anAccount = [folder account];
    }else{
        FXDebugLog(kFXLogActiveSync, @"No folder in MailViewController");
        anAccount = [BaseAccount accountForAccountNumber:0];
    }

    NSMutableArray* aTos = NULL;
    if([to count] > 0) {
        aTos = [NSMutableArray array];
        for(NSString* aRecipient in to) {
//            [aTos addObject:[TTTableTextItem itemWithText:aRecipient]];
            [aTos addObject:[self recipientLabel:aRecipient]];
        }
    }
	
    NSMutableArray* aCCs = NULL;
    if([cc count] > 0) {
        aCCs = [NSMutableArray array];
        for(NSString* aRecipient in cc) {
//            [aCCs addObject:[TTTableTextItem itemWithText:aRecipient]];
            [aCCs addObject:[self recipientLabel:aRecipient]];
        }
    }
    
    composeMailVC* aComposeViewController = [[composeMailVC alloc] initWithTo:aTos cc:aCCs account:anAccount];
	[aComposeViewController setSubject:subject];
	[aComposeViewController setBody:body];
    
    if(includeAttachments) {
		[self addAttachments:aComposeViewController];
	}
    
    [self.navigationController pushViewController:aComposeViewController animated:YES];
}

#if 0
-(void)attachmentButtonClicked:(TTButton*)target 
{
	NSArray* attachmentList = [email.attachmentJSON JSONValue];
    NSDictionary* anAttachment = [attachmentList objectAtIndex:target.tag];
    if(anAttachment) {
        NSString* contentType           = [anAttachment objectForKey:@"t"];
        NSString* filename              = [anAttachment objectForKey:@"n"];
        NSNumber* expectedSizeNumber    = [anAttachment objectForKey:@"s"];
        NSUInteger expectedSize = 0;
        if(expectedSizeNumber) {
            expectedSize = [expectedSizeNumber unsignedIntValue];
        }
        
        SyncManager* sm = [SyncManager getSingleton];
        
        AttachmentViewController* avc = [[AttachmentViewController alloc] initWithNibName:@"Attachment" bundle:nil];
        avc.attachment      = anAttachment;
        avc.folderNum       = [EmailProcessor folderNumForCombinedFolderNum:self.email.folderNum];
        avc.accountNum      = [EmailProcessor accountNumForCombinedFolderNum:self.email.folderNum];
        avc.attachmentNum   = target.tag;
        avc.uid             = self.email.uid;
        avc.expectedSize    = expectedSize;
        avc.contentType = [sm correctContentType:contentType filename:filename];
        [avc doLoad];
        [self.navigationController pushViewController:avc animated:YES];
	}else{
        FXDebugLog(kFXLogActiveSync, @"attachmentButtonClicked invalid");
    }
}
#endif

////////////////////////////////////////////////////////////////////////////////////////////
// Handle deletions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Handle deletions

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	if(buttonIndex == 0) {
        @try {
            [self.folder deleteUid:self.email.uid updateServer:TRUE]; 
            [self.navigationController popViewControllerAnimated:YES];
        }@catch (NSException* e) {
            [self _logException:@"delete email" exception:e];
        }
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// Assemble UI
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Assemble UI

-(void)peopleList:(NSString*)title
       addToView:(UIView*)addToView
      peopleList:(NSArray*)peopleList 
             top:(int)top
    highlightAll:(BOOL)highlightAll
  highlightQuery:(NSString*)highlightQuery 
{
	for (NSDictionary* person in peopleList) {
		NSString* name = [person objectForKey:@"n"];
		NSString* address = [person objectForKey:@"e"];
		NSString* display = nil;
		if(name != nil && [name length] > 0) {
			display = name;
		} else if (address != nil && [address length] > 0) {
			display = address;
		}
		
		BOOL highlightMatch = NO;
		if(highlightQuery != nil) {
			highlightMatch = (name != nil && [self matchText:name withQuery:query]) || (address != nil && [self matchText:address withQuery:query]);
		}
		
		SZCAddressButton* button = [[SZCAddressButton alloc] initWithFrame:CGRectZero];
        [button useForViewing];
        [button setTitle:address forState:UIControlStateNormal];
        button.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;

        button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
		[button setTitle:address forState:UIControlStateDisabled]; // using disabled state to store the email address
		[button sizeToFit];
		[addToView addSubview:button];
		
		[button addTarget:self action:@selector(personButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
	}
    
    [(SZCFlowLayoutView*)addToView sizeToFit];
    [(SZCFlowLayoutView*)[addToView superview] sizeToFit];
    CGFloat headerFrameHeight = self.headerView.frame.size.height + 2;
    
    //self.headerView.center = CGPointMake(self.headerView.center.x, 0 - (self.headerView.frame.size.height/2));
    UIEdgeInsets currentContentInset = self.scrollView.contentInset;
    currentContentInset.top = headerFrameHeight;
    self.scrollView.contentInset = currentContentInset;
    self.scrollView.contentOffset = CGPointMake(0, 0-headerFrameHeight);

}

-(int)_addAttachmentListToView:(UIView*)addToView
      attachmentList:(NSArray*)attachmentList 
                 top:(int)top 
      highlightQuery:(NSString*)highlightQuery 
{
    UIImageView* line = NULL;
    
    CGFloat aContentWidth = addToView.size.width;

    @try {
        // produces a list of attachment name buttons
        UIView* toView      = [[UIView alloc] init];
        toView.frame        = CGRectMake(0,top,aContentWidth,100);
    
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        int i = 0;
        for (NSDictionary* attachment in attachmentList) {
            // FIXME encapsulate in ASAttachment
            //
            //ASAttachment* anAttachment = [ASAttachment attachmentFromDictionary:attachment];
            
            NSString* filename = [attachment objectForKey:@"n"];            
            BOOL highlightMatch = NO;
            if(highlightQuery != nil) {
                highlightMatch = (filename != nil && [self matchText:filename withQuery:query]);
            }
            
            int accountNumDC = [EmailProcessor accountNumForCombinedFolderNum:self.email.folderNum];
            int folderNumDC  = [EmailProcessor folderNumForCombinedFolderNum:self.email.folderNum];
            
            NSString* filenameOnDisk = [AttachmentDownloader fileNameForAccountNum:accountNumDC 
                                                                         folderNum:folderNumDC 
                                                                               uid:self.email.uid 
                                                                     attachmentNum:i
                                                                         extension:[filename pathExtension]];
            NSString* attachmentDir  = [AttachmentDownloader attachmentDirPath];
            NSString* attachmentPath = [attachmentDir stringByAppendingPathComponent:filenameOnDisk];		
            BOOL fileExists = [fileManager fileExistsAtPath:attachmentPath];
            
            if(filename != nil && [filename length] > 0) {
#warning FIXME Supported attachments filters
#if 0
                // Do we want to filter supported attachments or gamle, the filter tends to always be too conservative
                // and filter things that should work
                if([[SyncManager getSingleton] isAttachmentViewSupported:contentType filename:filename]) {
#endif
                    UIButton* button;
                    if(highlightMatch && fileExists) {
//                        button = [TTButton buttonWithStyle:@"redRoundButton:" title:filename];
						button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
						button.backgroundColor = [UIColor redColor];
						[button setTitle:filename forState:UIControlStateNormal];
                    } else if(highlightMatch) {
//                        button = [TTButton buttonWithStyle:@"lightRedRoundButton:" title:filename];
						button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
						button.backgroundColor = [UIColor redColor];
						[button setTitle:filename forState:UIControlStateNormal];
                      } else if (fileExists) {
//                        button = [TTButton buttonWithStyle:@"greyRoundButton:" title:filename];
						button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
						button.backgroundColor = [UIColor grayColor];
						[button setTitle:filename forState:UIControlStateNormal];
                      } else {
//                        button = [TTButton buttonWithStyle:@"whiteRoundButton:" title:filename];
						button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
						button.backgroundColor = [UIColor whiteColor];
						[button setTitle:filename forState:UIControlStateNormal];
                      }
                    button.titleLabel.font = [UIFont boldSystemFontOfSize:12];
                    button.tag  = i;
                    [button sizeToFit];
                    [toView addSubview:button];
                    
                    [button addTarget:self action:@selector(attachmentButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
#if 0
                } else {
                    UILabel* label = [[UILabel alloc] init];
                    label.text = filename;
                    label.font = [UIFont boldSystemFontOfSize:12];
                    if(highlightMatch) {
                        label.textColor = [UIColor redColor];
                    } else {
                        label.textColor = [UIColor darkGrayColor];
                    }
                    
                    CGSize size = [filename sizeWithFont:label.font];
                    label.frame = CGRectMake(0, 0, size.width+2, 30);
                    [toView addSubview:label];
                    [label release];
                }
#endif
            }
            i++;
        }
        
        [self.scrollView addSubview:toView];
        
//        TTFlowLayout* flowLayout = [[TTFlowLayout alloc] init];
//        flowLayout.padding = 5;
//        flowLayout.spacing = 2;
        CGSize toSize = [self flowLayoutSubviews:toView.subviews forView:toView padding:5 spacing:2];
        
        toView.frame = CGRectMake(0, top, toSize.width, toSize.height);	
        [addToView addSubview:toView];
    }
    @catch (NSException* e) {
        [self _logException:@"attachmentList" exception:e];
    }
	
	return line.bottom;
}

-(void)toggleCopyMode:(UIButton*)button 
{
	FXDebugLog(kFXLogActiveSync, @"toggleCopyMode");
	
	UIImage* copyIcon = nil;
	if(self.copyMode) {
		self.copyMode = NO;
		copyIcon = [UIImage imageNamed:@"copyModeOff.png"];
		
		[self.subjectTTLabel setHidden:NO];
		[self.bodyTTLabel setHidden:NO];
		
		[self.subjectUIView setHidden:YES];
        for(UIView* aBodyView in bodyViews) {
            [aBodyView setHidden:YES];
        }
		[self.copyModeLabel setHidden:YES];
	} else {
		copyIcon = [UIImage imageNamed:@"copyModeOn.png"];
		self.copyMode = YES;

		[self.subjectTTLabel setHidden:YES];
		[self.bodyTTLabel setHidden:YES];
		
		[self.subjectUIView setHidden:NO];
        for(UIView* aBodyView in bodyViews) {
            [aBodyView setHidden:NO];
        }
		[self.copyModeLabel setHidden:NO];
	}

	[button setImage:copyIcon forState:UIControlStateNormal];
	[button setImage:copyIcon forState:UIControlStateHighlighted];
	[button setImage:copyIcon forState:UIControlStateSelected];
	
}

-(void)subjectBlock:(NSString*)subject
          markedUp:(NSString*)subjectMarkedUp 
              date:(NSDate*)date 
               top:(int)top 
         addToView:(UIView*)addToView
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterShortStyle];
	
	top += 1;
	
    CGFloat aContentWidth = addToView.size.width;
	
	CGRect subjectUIRect = CGRectMake(-3, top, aContentWidth-10, 21);
			
	self.subjectUIView = [[UITextView alloc] initWithFrame:subjectUIRect];
	self.subjectUIView.font                 = [UIFont boldSystemFontOfSize:17];
	self.subjectUIView.textColor            = [UIColor blackColor];
	self.subjectUIView.text                 = subject;
	self.subjectUIView.scrollEnabled        = YES;
	self.subjectUIView.editable             = NO;
	self.subjectUIView.dataDetectorTypes    = UIDataDetectorTypeAll;
	self.subjectUIView.contentInset         = UIEdgeInsetsMake(0,0,0,0);
	self.subjectUIView.autoresizingMask     = UIViewAutoresizingFlexibleWidth;
	[self.subjectUIView setContentOffset:CGPointMake(0,8) animated:NO];
	
	[addToView addSubview:self.subjectUIView];

	UILabel* labelDate = [[UILabel alloc] initWithFrame:CGRectMake(5, top+22, 250, 17)];
	labelDate.font      = [UIFont systemFontOfSize:12];
	labelDate.textColor = [UIColor darkGrayColor];
	labelDate.text      = [dateFormatter stringFromDate:date];
	[addToView addSubview:labelDate];
    
    [(SZCFlowLayoutView*)addToView sizeToFit];
    [(SZCFlowLayoutView*)[addToView superview] sizeToFit];
    CGFloat headerFrameHeight = self.headerView.frame.size.height + 2;
    
    //self.headerView.center = CGPointMake(self.headerView.center.x, 0 - (self.headerView.frame.size.height/2));
    UIEdgeInsets currentContentInset = self.scrollView.contentInset;
    currentContentInset.top = headerFrameHeight;
    self.scrollView.contentInset = currentContentInset;
    self.scrollView.contentOffset = CGPointMake(0, 0-headerFrameHeight);

}

- (void)validateAndLoadEmail:(BaseEmail*)anEmail folder:(BaseFolder*)aFolder
{
    if([anEmail.uid length] > 0) {
        // If there is no body, need to fetch it
        //
        if([anEmail.body length] == 0  && ![anEmail isFetched]) {
            //FXDebugLog(kFXLogActiveSync, @"Fetch message: pk=%d dbnum=%d uid=%@", self.emailPk, self.dbNum, anEmail.uid);
            if(aFolder) {
#warning FIXME move this from account to email or folder
                [[aFolder account] fetchEmail:anEmail folder:aFolder delegate:self];
            }else{
                FXDebugLog(kFXLogActiveSync, @"Fetch message folder invalid");
            }
        }else if([anEmail isFetched]) {
            if(anEmail.estimatedSize <= 1) {
                FXDebugLog(kFXLogActiveSync, @"Email has no body");
            }else{
                FXDebugLog(kFXLogActiveSync, @"Emai body inconsistency estimateSize=%d", anEmail.estimatedSize);
            }
        }
        [self performSelectorOnMainThread:@selector(loadedEmail) withObject:nil waitUntilDone:NO];
        
    }else{
        FXDebugLog(kFXLogActiveSync, @"validateAndLoadEmail uid invalid: %@", self.email);
    }
}

-(void)loadEmail
{
	@autoreleasepool {
    
        @try {
			if (self.email) {
//                self.email = [[SearchRunner getSingleton] loadEmailForFolder:aFolder pk:self.emailPk dbNum:self.dbNum];
                
                [self validateAndLoadEmail:self.email folder:self.email.folder];
			} else {
				BaseFolder* aFolder = NULL;
				if(folder) {
					aFolder = folder;
				}else if(self.email) {
					aFolder = [BaseAccount folderForCombinedFolderNum:self.email.folderNum];
				}
				
				if(aFolder) {
#ifndef FEATURE_DBNUM
                    FXDebugLog(kFXLogActiveEmail, @"FIXME loadEmailForFolder");
#else
					self.email = [[SearchRunner getSingleton] loadEmailForFolder:aFolder pk:self.email.pk dbNum:self.email.dbNum];
#endif
					
					[self validateAndLoadEmail:self.email folder:aFolder];            
				}else{
					FXDebugLog(kFXLogFIXME, @"FIXME no folder to load email");
				}
			}
        }@catch (NSException* e) {
            [self _logException:@"loadEmail" exception:e];
        }
    
	}
}
	
-(void)loadedEmail 
{
    [self _removeActivityIndicator];

	// now that the email was loaded (in a different thread), we can display its contents
	//
    @try {
        if(self.email == nil) {
#warning FIXME Email not found error reporting
            //int bottom = [self bodyBlock:@"Error: Email not found" markedUp:@"Error: Email not found" top:0 addToView:self.scrollView showTTView:NO];
            //self.scrollView.contentSize = CGSizeMake(320, bottom+2);
            return;
        }
        
        NSString *highlightQuery = nil;
        if(!self.isSenderSearch) { // only highlight matching senders when we're not in senderSearch
            highlightQuery = self.query;
        }
        BOOL markupBody = YES; // avoid showing the huge TTView for the body if possible
        if(self.isSenderSearch || [highlightQuery length] == 0) {
            markupBody = NO;
        }
        
        NSDictionary* senderDict = [NSDictionary dictionaryWithObjectsAndKeys:email.replyTo, @"e", email.from, @"n", nil];
        [self peopleList:NSLocalizedString(@"From:",nil) addToView:self.fromView peopleList:[NSArray arrayWithObject:senderDict] top:0 highlightAll:self.isSenderSearch highlightQuery:highlightQuery];
        
        if(email.toJSON != nil && [email.toJSON length] > 4) {
            NSArray* peopleList = [email.toJSON JSONValue];
            [self peopleList:NSLocalizedString(@"To:",nil) addToView:self.toView peopleList:peopleList top:0 highlightAll:NO highlightQuery:highlightQuery];
        }
        
        if(email.ccJSON != nil && [email.ccJSON length] > 4) {
            NSArray* peopleList = [email.ccJSON JSONValue];
            [self peopleList:NSLocalizedString(@"Cc:",nil) addToView:self.ccView peopleList:peopleList top:0 highlightAll:NO highlightQuery:highlightQuery];
        }
        
//        if(email.bccJSON != nil && [email.bccJSON length] > 4) {
//            NSArray* peopleList = [email.bccJSON JSONValue];
//            bottom = [self peopleList:NSLocalizedString(@"Bcc:",nil) addToView:self.scrollView peopleList:peopleList top:bottom highlightAll:NO highlightQuery:highlightQuery];
//        }
        
        NSString* subject = [self massageDisplayString:email.subject];
        if(subject == nil || [subject length] == 0) {
            subject = NSLocalizedString(@"[empty]", nil);
            email.subject = subject;
        }

        NSString* subjectMarkup = [self markupText:subject query:highlightQuery beginDelim:@"<span class=\"yellowBox\">" endDelim:@"</span>"];
        [self subjectBlock:email.subject markedUp:subjectMarkup date:email.datetime top:0 addToView:self.subjectView ];
        
//        self.subjectLabel.text = email.subject;
        
        self.attachmentMetadata = nil;
        NSString* anAttachmentJSON = email.attachmentJSON;
        if(anAttachmentJSON != nil && [anAttachmentJSON length] > 4) {
            NSArray* attachmentList = [anAttachmentJSON JSONValue];
            self.attachmentMetadata = attachmentList;
            [self _addAttachmentListToView:self.subjectView attachmentList:attachmentList top:0 highlightQuery:highlightQuery];
        }
        
        if(email.meetingRequest) {
            [self _addMeetingToView:self.scrollView meetingRequest:email.meetingRequest top:0];
        }
        
    #if 1
        [self bodyBlock:email top:0 addToView:self.scrollView];
		self.scrollView.contentOffset = CGPointZero;
    #else
        NSString* body = [self massageDisplayString:email.body];
        if(body == nil || [body length] == 0 || [body isEqualToString:@"\r\n"]) {
            body = NSLocalizedString(@"[empty]", nil);
        }
        NSString* bodyMarkup = [self markupText:body query:highlightQuery beginDelim:@"<span class=\"yellowBox\">" endDelim:@"</span>"];
        bottom = [self bodyBlock:email.body markedUp:bodyMarkup top:bottom addToView:self.scrollView showTTView:markupBody];
    #endif

        // Resize scroll view to cover content
        //self.scrollView.contentSize = CGSizeMake(320, bottom+2);
        
        FXDebugLog(kFXLogActiveSync, @"Free Memory: %d MB - MailView loadedEmail", [MemUtil freeMemoryAsMB]);


    } @catch (NSException* e) {
        [self _logException:@"loadedEmail" exception:e];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// String massinging and match markup
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark String massinging and match markup

-(NSString*)massageDisplayString:(NSString*)y 
{
	y = [y stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
	y = [y stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
	y = [y stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
	return y;
}

-(NSString*)markupText:(NSString*)text query:(NSString*)queryLocal beginDelim:(NSString*)beginDelim endDelim:(NSString*)endDelim 
{
	if(queryLocal == nil) {
		return text;
	}
	if(text == nil) {
		return @"";
	}
	
	NSMutableString* markedUp = [[NSMutableString alloc] initWithString:text];
	
	NSCharacterSet* beginSet = [NSCharacterSet characterSetWithCharactersInString:@" {}[]()|\"'<\n\r\t^-!;"]; // tries to avoid URL-looking separators
	NSCharacterSet* endSet =   [NSCharacterSet characterSetWithCharactersInString:@" {}[]()|\"'<\n\r\t^-!;.,:&+#_=~/\\@"]; // it's fine if we end with one of there separators though

	NSArray* queryParts = [queryLocal componentsSeparatedByCharactersInSet:endSet];
	
	for(__strong NSString* queryPart in queryParts) {
		
		BOOL star = NO;
		if([queryPart hasSuffix:@"*"]) {
			star = YES;
			queryPart = [queryPart substringToIndex:[queryPart length] - 1];
		}
		
		NSRange searchRange = NSMakeRange(0, [markedUp length]);
		while(YES) {
			NSRange range = [markedUp rangeOfString:queryPart options:NSCaseInsensitiveSearch range:searchRange];

			if(range.location == NSNotFound) {
				break;
			}
			
			BOOL beginOk = NO;
			if(range.location == 0) {
				beginOk = YES;
			} else {
				unichar before = [markedUp characterAtIndex:range.location - 1];
				beginOk = [beginSet characterIsMember:before];
			}
			
			BOOL endOk = NO;
			if([markedUp length] == range.location + range.length) {
				endOk = YES;
			} else if (star) {
				NSRange	foundSpace = [markedUp rangeOfCharacterFromSet:endSet options:0 range:NSMakeRange(range.location, [markedUp length]-range.location)];
				if(foundSpace.location == NSNotFound) {
					foundSpace.location = [markedUp length];
				}
				range = NSMakeRange(range.location, foundSpace.location - range.location);
				endOk = YES;
			} else {
				unichar after = [markedUp characterAtIndex:(range.location + range.length)];
				endOk = [endSet characterIsMember:after];
			}
			
			if(beginOk && endOk) {
				NSString *rangeContents = [markedUp substringWithRange:range];
				[markedUp deleteCharactersInRange:range];
				[markedUp insertString:[NSString stringWithFormat:@"$$$$beginDelim$$$$%@$$$$endDelim$$$$", rangeContents] atIndex:range.location];
			}
			
			searchRange = NSMakeRange(range.location + range.length, [markedUp length] - (range.location + range.length));
		}
	}
	
	
	[markedUp replaceOccurrencesOfString:@"$$$$endDelim$$$$" withString:endDelim options:NSLiteralSearch range:NSMakeRange(0,[markedUp length])];
	[markedUp replaceOccurrencesOfString:@"$$$$beginDelim$$$$" withString:beginDelim options:NSLiteralSearch range:NSMakeRange(0,[markedUp length])];
	NSString* res = [NSString stringWithString:markedUp];
	
	return res;	
}

-(BOOL)matchText:(NSString*)text withQuery:(NSString*)queryLocal 
{
	NSArray* queryParts = [StringUtil split:queryLocal];
	
	NSCharacterSet* divisor = [NSCharacterSet characterSetWithCharactersInString:@" {}[]()|\"'<\n\r\t^-!;.,:&+#_=~/\\@"];
	
	NSScanner *scanner = [[NSScanner alloc] initWithString:text];
	scanner.caseSensitive = NO;
	NSString* dest = nil;
	NSString* prev = nil;
	for(__strong NSString* queryPart in queryParts) {
		BOOL star = NO;
		if([queryPart hasSuffix:@"*"]) {
			star = YES;
			queryPart = [queryPart substringToIndex:[queryPart length] - 1];
		}
		
		scanner.scanLocation = 0;

		BOOL beginOk = NO;
		BOOL endOk = NO;
		
		[scanner scanUpToString:queryPart intoString:&prev];
		if([scanner scanString:queryPart intoString:&dest]) {
			if(prev == nil || [prev length] == 0) {
				beginOk = YES;
			} else {
				unichar c= [prev characterAtIndex:[prev length]-1]; // last character in prev string
				beginOk = [divisor characterIsMember:c];
			}
			
			if (beginOk) {
				if(star) {
					// anything goes if we have a star
					endOk = YES;
				} else {
					// no star -> make sure the word ends
					if([scanner isAtEnd]) {
						endOk = YES;
					} else {
						unichar c = [text characterAtIndex:scanner.scanLocation];
						endOk = [divisor characterIsMember:c];
					}
				}
			}
			
			if(beginOk && endOk) {
				return YES;
			}
		}
	}	
	
	return NO;
}

/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */

////////////////////////////////////////////////////////////////////////////////////////////
// MessageClientDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MessageClientDelegate

- (void)fetchedMessage:(BaseEmail*)anEmail
{
    [self _removeActivityIndicator];
    //FXDebugLog(kFXLogActiveSync, @"fetchedMessage body: %@", bodyMarkup);

#if 1
    int bottom = [self bodyBlock:email top:bodyTop addToView:self.scrollView];
    self.scrollView.contentSize = CGSizeMake(320, bottom+2);
	self.scrollView.contentOffset = CGPointZero;
#else
    NSString* aBody = [anEmail body];
    NSString* highlightQuery = self.query;
    NSString* bodyMarkup = [self markupText:aBody query:highlightQuery beginDelim:@"<span class=\"yellowBox\">" endDelim:@"</span>"];
	[self bodyBlock:email.body markedUp:bodyMarkup top:bodyTop addToView:self.scrollView showTTView:NO/*markupBody*/];
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////
// MIME Rendering
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MIME Rendering 

- (NSString*)_attachmentsPath
{
    // FIXME should we make a sub directory specific to this email to make it easier to clean up
    //
    NSString* anAttachmentPath =  [StringUtil filePathInDocumentsDirectoryForFileName:@"attachments"];
    NSFileManager* aFileManager = [NSFileManager defaultManager];
    BOOL isDirectory = FALSE;
    if(![aFileManager fileExistsAtPath:anAttachmentPath isDirectory:&isDirectory]) {
        NSError* anError;
        [aFileManager createDirectoryAtPath:anAttachmentPath 
                withIntermediateDirectories:TRUE attributes:nil error:&anError];
    }
    
    return anAttachmentPath;
}

- (NSString*)_attachmentsPathWithFileName:(NSString*)aFileName
{
    return [[self _attachmentsPath] stringByAppendingPathComponent:aFileName];
}

- (void)_addTextToView:(UIScrollView*)anAddToView body:(NSString*)aBody top:(int)aTop
{
    [self.messageView loadData:[aBody dataUsingEncoding:NSUTF8StringEncoding] MIMEType:@"text/plain" textEncodingName:@"UTF-8" baseURL:nil];
}

- (void)_addHTMLToView:(UIScrollView*)anAddToView body:(NSString*)aBody top:(int)aTop
{
    [self.messageView loadHTMLString:[NSString stringWithFormat:@"%@",aBody] baseURL:nil];
}

- (UIView*)_addFileToWebView:(UIScrollView*)anAddToView 
                    fileName:(NSString*)aFileName 
                        data:(NSData*)aData 
                         top:(int)aTop
                      height:(int)aHeight   // FIXME should try to get this from the DOM
{
    // Write the file to attachment directory
    //
    NSString* aFilePath = [self _attachmentsPathWithFileName:aFileName];
    [aData writeToFile:aFilePath atomically:FALSE];
    NSURL* aURL = [NSURL URLWithString:aFilePath];
    
    /*
    // Create a WebView
    //
    CGFloat aContentWidth = anAddToView.size.width;
    
    WebViewController* aWebController = [[WebViewController alloc] initWithNibName:@"WebView" bundle:nil];
    
    UIView* aView = [aWebController view];
	aView.autoresizingMask    = UIViewAutoresizingFlexibleWidth;
    
    aView.frame = CGRectMake(-3, aTop, aContentWidth, aHeight+24);
    
    [anAddToView addSubview:aView];
    
    UIWebView* aWebView = [aWebController webView];
    [bodyViews addObject:aWebView];
    
    // Load the file in to the web view
    //
    [aWebController loadURL:aURL];
    
    return aWebView;
     */
    [self.messageView loadRequest:[NSURLRequest requestWithURL:aURL]];
    return nil;
}

- (UIView*)_addImageToView:(UIScrollView*)anAddToView data:(NSData*)aData top:(int)aTop
{        
    UIImage* anImage = [[UIImage alloc] initWithData:aData];
    
    CGFloat height  = anImage.size.height;
    CGFloat width   = anImage.size.width;
    if(width <= 0.0f || height <= 0.0f) {
        FXDebugLog(kFXLogActiveSync, @"_addImageToView image invalid");
    }
    
    UIImageView* anImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, aTop, width, height)];    
    [anImageView setImage:anImage];
    [anAddToView addSubview:anImageView];
    
    return anImageView;
}

- (void)meetingUserResponse:(id)aSender
{
    MeetingResponseSheet* aResponseSheet = [[MeetingResponseSheet alloc] initWithView:self.view email:(ASEmail*)self.email];
    if(aResponseSheet) {
    }
}

- (int)_addMeetingToView:(UIScrollView*)anAddToView meetingRequest:(MeetingRequest*)aMeetingRequest top:(int)aTop
{
    CGFloat aContentWidth = anAddToView.size.width;
    
    UIView* aMeetingView = [[UIView alloc] initWithFrame:CGRectMake(-3, aTop, aContentWidth, kMeetingViewHeight)];
    aMeetingView.autoresizingMask    = UIViewAutoresizingFlexibleWidth;
    
    // Response Button
    //
    CGFloat aViewHeight = kMeetingViewHeight - kButtonOffsetY*2;
    int anOffsetX;
    if([aMeetingRequest isRequest]) {
        UIButton* aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];

        static const int kButtonWidth   = 80;
        aButton.frame = CGRectMake(kButtonOffsetX, kButtonOffsetY, kButtonWidth, aViewHeight);
        [aButton setTitle:@"Response" forState:UIControlStateNormal];
		[aButton addTarget:self action:@selector(meetingUserResponse:) forControlEvents:UIControlEventTouchUpInside];
		[aMeetingView addSubview:aButton];
        anOffsetX = aButton.frame.origin.x + aButton.frame.size.width + kButtonOffsetX;
    }else{
        anOffsetX = kButtonOffsetX;
    }
    
    // Text view for status
    //
    CGFloat aViewWidth = aMeetingView.frame.size.width - anOffsetX;
    UITextView* aTextView = [[UITextView alloc] initWithFrame:CGRectMake(anOffsetX, kButtonOffsetY,
                                                                          aViewWidth, aViewHeight)];
    aTextView.font                = [UIFont systemFontOfSize:14];
    aTextView.textColor           = [UIColor blackColor];
    aTextView.scrollEnabled       = NO;
    aTextView.editable            = NO;
    aTextView.dataDetectorTypes   = UIDataDetectorTypeAll;
    aTextView.autoresizingMask    = UIViewAutoresizingFlexibleWidth;
    [aMeetingView addSubview:aTextView];
    
    if([aMeetingRequest isCanceled]) {
        aTextView.text = @"Canceled";
    }else if([aMeetingRequest isAccepted]) {
        aTextView.text = @"Accepted";
    }else if([aMeetingRequest isTentativeAccepted]) {
        aTextView.text = @"Tentative";
    }else if([aMeetingRequest isDeclined]) {
        aTextView.text = @"Declined";
    }else{
        aTextView.text = @"No response";
    }
    
    // Add meeting view line to Mail View
    //
    [anAddToView addSubview:aMeetingView];
    [bodyViews addObject:aMeetingView];
    
    int aBottom = aTop + aMeetingView.frame.size.height;

#if 0
    UIView* aSeparatorView = [[[UIView alloc] initWithFrame:CGRectMake(0, aBottom, aContentWidth, 1)] autorelease];
    aSeparatorView.backgroundColor = TTSTYLEVAR(messageFieldSeparatorColor);
    aSeparatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [anAddToView addSubview:aSeparatorView];
    [bodyViews addObject:aSeparatorView];
    aBottom += aSeparatorView.frame.size.height;
#endif
    
    return aBottom;
}

- (UIView*)addMIMEToView:(PKContentType*)aPKContentType 
                mimeData:(PKMIMEData*)aMimeData 
                    view:(UIScrollView*)aView 
                     top:(int)aTop
{
    UIView* anAddedView = NULL;
    //FXDebugLog(kFXLogActiveSync, @"addMIMEToView: %@", [aPKContentType string]);
    NSString* aContentTypeString = [aPKContentType string];
    if([aPKContentType isText]) {

        if([aContentTypeString hasPrefix:@"text/plain"]) {
            [self _addTextToView:aView body:[aMimeData decodedString] top:aTop];
            anAddedView = self.messageView;
        }else if([aContentTypeString hasPrefix:@"text/html"]) {
            [self _addHTMLToView:aView body:[aMimeData decodedString] top:aTop];
            anAddedView = self.messageView;
        }else if([aContentTypeString hasPrefix:@"text/calendar"]) {
            FXDebugLog(kFXLogFIXME, @"FIXME calendar MIME part: %@\n%@", aContentTypeString, [aMimeData decodedString]);
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME text unknown MIME part: %@", aContentTypeString);
        }

    }else if([aPKContentType isApplication]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        if([aFileName length] > 0) {
            if([aContentTypeString hasPrefix:@"application/pdf"]
            || [aContentTypeString hasPrefix:@"application/msword"]
            || [aContentTypeString hasPrefix:@"application/vnd.ms-excel"]) {
                anAddedView = [self _addFileToWebView:aView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultApplicationAttachmentHeight];
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME unknown application MIME part may not work in web view: %@", aContentTypeString);
                anAddedView = [self _addFileToWebView:aView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultApplicationAttachmentHeight];
            }
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME application MIME has no file name");
        }
    }else if([aPKContentType isImage]) {
        anAddedView = [self _addImageToView:aView data:[aMimeData rawData] top:aTop];
    }else if([aPKContentType isAudio]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        anAddedView = [self _addFileToWebView:aView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultAudioAttachmentHeight];
    }else if([aPKContentType isVideo]) {
        FXDebugLog(kFXLogFIXME, @"FIXME video MIME part: %@", aContentTypeString);
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        anAddedView = [self _addFileToWebView:aView fileName:aFileName data:[aMimeData rawData] top:aTop height:kDefaultVideoAttachmentHeight];
    }else if([aPKContentType isData]) {
        FXDebugLog(kFXLogFIXME, @"FIXME data MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMessage]) {
        FXDebugLog(kFXLogFIXME, @"FIXME message MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMultipart]) {
        FXDebugLog(kFXLogFIXME, @"FIXME multipart MIME shouldnt get here: %@", aContentTypeString);
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME shouldnt get here: %@", aContentTypeString);
    }
    
    return anAddedView;
}

- (NSString*)mimeBodyAsString:(PKContentType*)aPKContentType mimeData:(PKMIMEData*)aMimeData 
{
    NSString* aString = @"";
    
    //FXDebugLog(kFXLogActiveSync, @"addMIMEToView: %@", [aPKContentType string]);
    NSString* aContentTypeString = [aPKContentType string];
    if([aPKContentType isText]) {
        if([aContentTypeString hasPrefix:@"text/plain"]) {
            aString = [aMimeData decodedString];
        }else if([aContentTypeString hasPrefix:@"text/html"]) {
            aString = [self  _plainTextFromHTML:[aMimeData decodedString]];    
        }else if([aContentTypeString hasPrefix:@"text/calendar"]) {
            FXDebugLog(kFXLogFIXME, @"FIXME calendar MIME part: %@\n%@", aContentTypeString, [aMimeData decodedString]);
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME text unknown MIME part: %@", aContentTypeString);
        }
    }else if([aPKContentType isApplication]) {
        NSString* aFileName = [aPKContentType parameterWithName:@"name"];
        if([aFileName length] > 0) {
            if([aContentTypeString hasPrefix:@"application/pdf"]
               || [aContentTypeString hasPrefix:@"application/msword"]
               || [aContentTypeString hasPrefix:@"application/vnd.ms-excel"]) {
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME unknown application MIME part may not work in web view: %@", aContentTypeString);
            }
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME application MIME has no file name");
        }
    }else if([aPKContentType isImage]) {
        FXDebugLog(kFXLogFIXME, @"FIXME image MIME part: %@", aContentTypeString);
    }else if([aPKContentType isAudio]) {
        FXDebugLog(kFXLogFIXME, @"FIXME audio MIME part: %@", aContentTypeString);
    }else if([aPKContentType isVideo]) {
        FXDebugLog(kFXLogFIXME, @"FIXME video MIME part: %@", aContentTypeString);
    }else if([aPKContentType isData]) {
        FXDebugLog(kFXLogFIXME, @"FIXME data MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMessage]) {
        FXDebugLog(kFXLogFIXME, @"FIXME message MIME part: %@", aContentTypeString);
    }else if([aPKContentType isMultipart]) {
        FXDebugLog(kFXLogFIXME, @"FIXME multipart MIME shouldnt get here: %@", aContentTypeString);
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME shouldnt get here: %@", aContentTypeString);
    }
    
    return aString;
}

////////////////////////////////////////////////////////////////////////////////////////////
// MIME Parsing
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark MIME Parsing 

- (NSString*)cleanContentID:(NSString*)aContentID
{
    NSString* aCleanContentID = aContentID;
    NSRange atRange = [aContentID rangeOfString:@"@" options:0 range:NSMakeRange(0, [aContentID length])];
    if(atRange.location != NSNotFound) {
        aCleanContentID  = [aContentID substringToIndex:atRange.location];
    }
    return aCleanContentID;
}

- (NSMutableString*)stringFromMultipartAlternative:(PKMIMEMessage*)aMIMEMessage contentType:(NSString*)aContentType
{
    NSMutableString* aString = NULL;
    NSArray* anAlternatives = [aMIMEMessage alternatives];
    int aCount = [anAlternatives count];
    for(int i = aCount-1 ; i >= 0 ; --i) {
        PKContentType* anAlternativeContentType = [anAlternatives objectAtIndex:i];
        PKMIMEData* aAlternativeData = [aMIMEMessage dataForAlternativeNumber:i];
        if([anAlternativeContentType isMultipart]) {
            FXDebugLog(kFXLogFIXME, @"FIXME stringFromMultipartAlternative isMultipart");
        }else if([anAlternativeContentType isText]) {
            if([[anAlternativeContentType string] hasPrefix:aContentType]) {
                aString = [NSMutableString stringWithString:[aAlternativeData decodedString]];
                break;
            }else{
            }
        }
    }
    return aString;
}

- (int)handleMultipartRelated:(PKMIMEMessage*)aMIMEMessage 
                  contentType:(PKContentType*)aPKContentType                    
                          top:(int)aTop 
                    addToView:(UIScrollView*)aView 
{
    int aBottom = aTop;
    
    // multipar/related implemented assuming an text/HTML first part which will reference ContentID's in subsequent parts, like images
    // Other variations will probably fail
    //
    NSMutableArray* aContentIDs     = [NSMutableArray arrayWithCapacity:5];
    NSString* anAttachementsPath    = [self _attachmentsPath];
    NSMutableString* anHtmlString   = NULL;
    
    NSUInteger numParts = [aMIMEMessage numParts];
    for(NSUInteger i = 0 ; i < numParts ; ++i) {
        PKMIMEMessage* aPartMessage     = [aMIMEMessage partAtIndex:i];            
        PKContentType* aPKContentType   = [aPartMessage contentType];
        NSString* aContentTypeString    = [aPKContentType string];
        //FXDebugLog(kFXLogActiveSync, @"handleMIMEPart %d: %@", i, aContentTypeString);
        
        if([aContentTypeString hasPrefix:@"text/html"]) {
            PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
            anHtmlString = [NSMutableString stringWithString:[aPartData decodedString]];
            
        }else if([aContentTypeString hasPrefix:@"multipart"]) {
            if([aPartMessage hasAlternatives]) {
                anHtmlString = [self stringFromMultipartAlternative:aPartMessage contentType:@"text/html"];
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME Multipart in Multipart related: %@", aContentTypeString);
            }
        }else {
            PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
            
            // Get the ContentID for this part
            //
            NSString* aContentID = [aPartData contentID];
            [aContentIDs addObject:aContentID];
            NSString* aCleanContentID = [self cleanContentID:aContentID];
            
            
            // Write the CID part's rawData to the attachment directory with a cleaned contentID
            //
            NSString* aFilePath = [anAttachementsPath stringByAppendingPathComponent:aCleanContentID];
            NSData* aRawData = [aPartData rawData];
            //FXDebugLog(kFXLogActiveSync, @"Multipart related contentID = %d %@", [aRawData length], aFilePath);
            
            [aRawData writeToFile:aFilePath atomically:FALSE];
            
            // Debug image
            //NSData* anImageData = [NSData dataWithContentsOfFile:aFilePath];
            //[self _addImageToView:aView data:anImageData top:aTop];
        }
    }
    
    if([anHtmlString length] > 0) {
        // Replace all of the CID src strings in the HTML string with local file references in attachements directory
        //
        for(NSString* aContentID in aContentIDs) {
            NSString* aCIDString = [@"cid:" stringByAppendingString:aContentID];
            NSRange aCIDRange = [anHtmlString rangeOfString:aCIDString options:0 range:NSMakeRange(0, [anHtmlString length])];
            if(aCIDRange.location != NSNotFound) {
                //FXDebugLog(kFXLogActiveSync, @"Replace CID: %@", aCIDString);
                NSString* aCleanContentID = [self cleanContentID:aContentID];
                NSString* aFileURL = aCleanContentID;
                [anHtmlString replaceOccurrencesOfString:aCIDString withString:aFileURL options:0 range:aCIDRange];
            }else{
                FXDebugLog(kFXLogFIXME, @"FIXME CID not found: %@", aCIDString);
            }
        }
        
        // Create a WebView and load the HTML string with attachments
        //
        [self _addHTMLToView:aView body:NULL top:aTop];
        NSURL* aURL = [NSURL fileURLWithPath:anAttachementsPath];
            //FXDebugLog(kFXLogActiveSync, @"multipart/related %@ html: %@", [aURL absoluteString], anHtmlString);

        [self.messageView loadHTMLString:anHtmlString baseURL:aURL];
        
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME handleMultipartRelated no HTML");
    }
#warning FIXME - Need to clean up attachments directory when done
    
    return aBottom;
}

- (int)handleMultipart:(PKMIMEMessage*)aMIMEMessage 
           contentType:(PKContentType*)aPKContentType                    
                   top:(int)aTop 
             addToView:(UIScrollView*)aView 
{
    int aBottom = aTop;
    
    //FXDebugLog(kFXLogActiveSync, @"handleMultipart:\n\tContentType\n%@\n%@", [aPKContentType string], [aPKContentType parameters]);
    
    if([aMIMEMessage hasAlternatives]) {
        // Multipart alternative
        //
        NSArray* anAlternatives = [aMIMEMessage alternatives];
        int aCount = [anAlternatives count];
        for(int i = aCount-1 ; i >= 0 ; --i) {
            PKContentType* anAlternativeContentType = [anAlternatives objectAtIndex:i];
            FXDebugLog(kFXLogActiveSync, @"handleMultipart alternatives %d: %@", i, [anAlternativeContentType string]);
            PKMIMEData* aAlternativeData = [aMIMEMessage dataForAlternativeNumber:i];
            if([anAlternativeContentType isMultipart]) {
                // Warning: this is recursing
                //
                PKMIMEMessage* anAlternativeMIMEMessage = [PKMIMEMessage messageWithData:aAlternativeData];
                aBottom = [self handleMultipart:anAlternativeMIMEMessage contentType:anAlternativeContentType top:aTop addToView:aView];
            }else{
                UIView* anAddedView = [self addMIMEToView:anAlternativeContentType mimeData:aAlternativeData view:aView top:aTop];
                if(anAddedView) {
                    aBottom = anAddedView.bottom;
                }
            }
            if(aBottom > aTop) {
                break;
            }
        }
    }else{
        if([[aPKContentType string] hasPrefix:@"multipart/related"]) {
            // Multipart related
            //
            aBottom = [self handleMultipartRelated:aMIMEMessage contentType:aPKContentType top:aTop addToView:aView];
        }else{
            // Multipart mixed
            //
            NSUInteger numParts = [aMIMEMessage numParts];
            for(NSUInteger i = 0 ; i < numParts ; ++i) {
                PKMIMEMessage* aPartMessage = [aMIMEMessage partAtIndex:i];            
                PKContentType* aPKContentType = [aPartMessage contentType];
                NSString* aContentTypeString = [aPKContentType string];
                FXDebugLog(kFXLogActiveSync, @"handleMIMEPart %d: %@", i, aContentTypeString);
                if([aContentTypeString hasPrefix:@"multipart"]) {
                    aBottom = [self handleMultipart:aPartMessage contentType:aPKContentType top:aTop addToView:aView];
                }else {
                    PKMIMEData* aPartData = [aPartMessage partDataAtIndex:0];
                    UIView* anAddedView = [self addMIMEToView:aPKContentType mimeData:aPartData view:aView top:aTop];
                    if(anAddedView) {
                        aBottom = anAddedView.bottom;
                    }
                }
                aTop = aBottom;
            }
        }
    }
    return aBottom;
}

- (NSString*)bodyAsString:(BaseEmail*)anEmail
{
    NSString* aBodyString = NULL;
    
    NSData* aData = anEmail.body;

    PKMIMEData* aPKMIMEData         = [PKMIMEData dataFromDataWithHeaders:aData];
    PKContentType* aPKContentType   = [aPKMIMEData contentType];
    if([aPKContentType isMultipart]) {
        PKMIMEMessage* aMIMEMessage = [PKMIMEMessage messageWithData:aPKMIMEData];
        if([aMIMEMessage hasAlternatives]) {
            // Multipart alternative
            //
            aBodyString = [self stringFromMultipartAlternative:aMIMEMessage contentType:@"text/plain"];
            if(!aBodyString) {
                 aBodyString = [self stringFromMultipartAlternative:aMIMEMessage contentType:@"text/plain"];
                if(aBodyString) {
                    aBodyString = [self _plainTextFromHTML:aBodyString];
                }
            }
        }else{
            FXDebugLog(kFXLogFIXME, @"FIXME bodyAsString multipart");
            if([[aPKContentType string] hasPrefix:@"multipart/related"]) {
            }else{
            }
        }
    }else{
        aBodyString = [self mimeBodyAsString:aPKContentType mimeData:aPKMIMEData];
    }
    
    return aBodyString;
}

- (int)bodyBlock:(BaseEmail*)anEmail
             top:(int)aTop 
       addToView:(UIScrollView*)aView 
{
    aView = self.messageView.scrollView;
    int aBottom = aTop;
    
    @try {
        NSData* aData = anEmail.body;
        if([aData length] > 0) {
            if(anEmail.isMIME) {
                PKMIMEData* aPKMIMEData         = [PKMIMEData dataFromDataWithHeaders:aData];
                PKContentType* aPKContentType   = [aPKMIMEData contentType];
                PKMIMEMessage* aMimeMessage     = [PKMIMEMessage messageWithData:aPKMIMEData];
                if ([aMimeMessage hasAlternatives]) {
                    PKMIMEData *bestGuess = [aMimeMessage dataForBestAlternative];

                    [self addMIMEToView:[bestGuess contentType] mimeData:bestGuess view:self.scrollView top:0];
                }else if([aPKContentType isMultipart]) {
                    aBottom = [self handleMultipart:aMimeMessage contentType:aPKContentType top:aTop addToView:aView];
                }else{
                    UIView* anAddedView = [self addMIMEToView:aPKContentType mimeData:aPKMIMEData view:aView top:aTop];
                    if(anAddedView) {
                        aBottom = anAddedView.bottom;
                    }
                }
            }else if(anEmail.isHTML) {
                NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
                aDataString = [aDataString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                if([aDataString isEqualToString:@"{no text body}"]) {
                    // Attachments only
                }else{
                    //FXDebugLog(kFXLogActiveSync, @"bodyBlock2 HTML: %@", aDataString);
                    [self _addHTMLToView:aView body:aDataString top:aTop];    
                }
#warning FIXME leak?
                //[aDataString release];
            }else{
                NSString* aDataString = [[NSString alloc] initWithData:aData encoding:NSUTF8StringEncoding];
                if([aDataString isEqualToString:@"{no text body}"]) {
                    // Attachments only
                }else{
                    //FXDebugLog(kFXLogActiveSync, @"bodyBlock Text or RTF: %@", aDataString);
                    [self _addTextToView:aView body:aDataString top:aTop]; 
                }
            }
            
            // If message is unread it is at a point user can read it so change status in the Mail UI and
            // send to the server
            //
            if([anEmail isUnread]) {
                [anEmail changeReadStatus:TRUE];
            }
            
            // DEBUG Force message to reload next time for debugging
            //
            //anEmail.body = NULL;
            //anEmail.flags = 0;
            //[[EmailProcessor getSingleton] updateFlags:anEmail];
            
            [aView setContentOffset:CGPointMake(0.0f, 0.0f)];
        }else{
            [self _loadingActivityIndicator:aView];
        }
    }@catch (NSException* e) {
        [self _logException:@"bodyBlockNew" exception:e];
    }  
    
	return aBottom;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Dead code
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Dead code

#if 0
-(int)bodyBlock:(NSString*)body 
       markedUp:(NSString*)bodyMarkedUp 
            top:(int)top 
      addToView:(UIScrollView*)addToView 
     showTTView:(BOOL)showTTView 
{
	// not showing the TT view is an optimization for when we don't have any markup to show
	if(showTTView) {
		self.bodyTTLabel = [[[TTStyledTextLabel alloc] initWithFrame:CGRectMake(5, top+8, 310, 100)] autorelease];
		self.bodyTTLabel.font = [UIFont systemFontOfSize:14];
		self.bodyTTLabel.textColor = [UIColor blackColor];
	}
	
	CGFloat contentWidth = self.scrollView.size.width;
    
    if([body length] > 0) {
        [self handleMIME:body top:top addToView:addToView];
    }else if([bodyMarkedUp length] > 0) {
        FXDebugLog(kFXLogActiveSync, @"has markup: %@", bodyMarkedUp);
    }
	
	self.bodyUIView = [[[UITextView alloc] initWithFrame:CGRectMake(-3, top, 382, 100)] autorelease];
	self.bodyUIView.font                = [UIFont systemFontOfSize:14];
	self.bodyUIView.textColor           = [UIColor blackColor]; 
	self.bodyUIView.scrollEnabled       = NO;
	self.bodyUIView.editable            = NO;
	self.bodyUIView.dataDetectorTypes   = UIDataDetectorTypeAll;
    
	self.bodyUIView.autoresizingMask    = UIViewAutoresizingFlexibleWidth;
    
	[addToView addSubview:self.bodyUIView];
	
	if(showTTView) {
		self.bodyTTLabel.text = [TTStyledText textFromXHTML:bodyMarkedUp lineBreaks:YES URLs:YES];
		[self.bodyTTLabel sizeToFit];
	}
	
	int bottom = 0;
	if(showTTView) {
		self.bodyUIView.frame = CGRectMake(-3, top, contentWidth, self.bodyTTLabel.bottom-top);
		[self.bodyUIView setHidden:YES];
		bottom = self.bodyTTLabel.bottom;
		[addToView addSubview:self.bodyTTLabel];
	} else {
		CGSize size = [body sizeWithFont:self.bodyUIView.font constrainedToSize:CGSizeMake(300.0f,100000000.0f) lineBreakMode: UILineBreakModeWordWrap];
		self.bodyUIView.frame = CGRectMake(-3, top, contentWidth, size.height+24);
		bottom = self.bodyUIView.bottom;
	}
    
    if([body length] == 0) {
        [self _loadingActivityIndicator:self.bodyUIView];
    }
	
	return bottom;
}
#endif

//
// This is just a copy of TTFlowLayout taken from Three20
//
- (CGSize)flowLayoutSubviews:(NSArray*)subviews forView:(UIView*)view padding:(CGFloat)_padding spacing:(CGFloat)_spacing
{
  CGFloat x = _padding, y = _padding;
  CGFloat maxX = 0.0f, rowHeight = 0.0f;
  CGFloat maxWidth = view.frame.size.width - _padding*2;
  for (UIView* subview in subviews) {
    if (x > _padding && x + subview.frame.size.width > maxWidth) {
      x = _padding;
      y += rowHeight + _spacing;
      rowHeight = 0;
    }
    subview.frame = CGRectMake(x, y, subview.frame.size.width, subview.frame.size.height);
    x += subview.frame.size.width + _spacing;
    if (x > maxX) {
      maxX = x;
    }
    if (subview.frame.size.height > rowHeight) {
      rowHeight = subview.frame.size.height;
    }
  }

  return CGSizeMake(maxX+_padding, y+rowHeight+_padding);
}

@end
