/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
#import "AccountsViewController.h"
#import "AccountViewController.h"
#import "ASComposeViewController.h"
#import "ASSync.h"
#import "BaseFolder.h"
#import "EmailProcessor.h"
#import "MailboxViewController.h"

@implementation AccountsViewController

// Properties
@synthesize accounts;

////////////////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload 
{
    //[account removeDelegate:self];
	[super viewDidUnload];
}

-(void)viewWillAppear:(BOOL)animated 
{	
	[self doLoad];
}

-(void)viewDidLoad 
{
	[super viewDidLoad];
    
    //if(account) {
    //    [account addDelegate:self];
    //}
    
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(composeClick)];
}

- (void)viewDidAppear:(BOOL)animated 
{
	[super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
	return YES;
}

- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	FXDebugLog(kFXLogActiveSync, @"AccountsViewController received memory warning!");
}

////////////////////////////////////////////////////////////////////////////////////////////
// Actions
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Actions

-(void)doLoad 
{
	if(accounts) {
        [accounts removeAllObjects];
    }else{
        accounts = [[NSMutableArray alloc] initWithCapacity:12];
    }
    
    NSArray* anAccounts = [BaseAccount accounts];
    for(BaseAccount* anAccount in anAccounts) {
        [accounts addObject:anAccount];
    }
}

-(IBAction)settingsClick 
{
#warning FIXME settingsClick
}

////////////////////////////////////////////////////////////////////////////////////////////
// AccountDelegate
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark AccountDelegate

- (void)accountAdded:(BaseAccount*)anAccount
{
    if(![accounts containsObject:anAccount]) {
        NSIndexPath* aPath = [NSIndexPath indexPathForRow:[accounts count] inSection:0];
        [accounts addObject:anAccount];
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:aPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)accountDeleted:(BaseAccount*)anAccount
{
    NSUInteger anAccountIndex = [accounts indexOfObject:anAccount];
    if(anAccountIndex != NSNotFound) {
        NSIndexPath* aPath = [NSIndexPath indexPathForRow:anAccountIndex inSection:0];
        [accounts removeObjectAtIndex:anAccountIndex];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:aPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)accountUpdated:(BaseAccount*)anAccount
{
    FXDebugLog(kFXLogFIXME, @"FIXME AccountsViewController accountUpdated");
}

////////////////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section 
{
    return @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [accounts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.section == 0) {
        BaseAccount* anAccount = [accounts objectAtIndex:indexPath.row];
        if(anAccount) {
            cell.textLabel.text = [anAccount name];
        }else{
            cell.textLabel.text = @"";
        }
#warning FIXME put account icon here
        cell.imageView.image = nil;
	}
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
/*	NSArray* nibContents = [[NSBundle mainBundle] loadNibNamed:@"AccountView" owner:self options:NULL];
	NSEnumerator *nibEnumerator = [nibContents objectEnumerator]; 
	AccountViewController* anAccountViewController = nil;
	NSObject* nibItem = NULL;
    while ( (nibItem = [nibEnumerator nextObject]) != NULL) { 
        if ( [nibItem isKindOfClass: [AccountViewController class]]) { 
			anAccountViewController = (AccountViewController*) nibItem;
			break;
		}
	}
*/
	AccountViewController* anAccountViewController = nil;
	if (IS_IPAD()) {
		anAccountViewController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPad identifier:@"AccountViewController"];
	} else {
		anAccountViewController = [UIStoryboard storyboardWithName:kStoryboard_Contacts_iPhone identifier:@"AccountViewController"];
	}
	
	if(anAccountViewController) {
        BaseAccount* anAccount = [accounts objectAtIndex:indexPath.row];        
        anAccountViewController.title   = [anAccount name];
        anAccountViewController.account = anAccount;
        
        anAccountViewController.toolbarItems = [self.toolbarItems subarrayWithRange:NSMakeRange(0, 2)];
        
        [self.navigationController pushViewController:anAccountViewController animated:YES];
    }else{
        FXDebugLog(kFXLogFIXME, @"FIXME no AccountViewController");
    }
    
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
