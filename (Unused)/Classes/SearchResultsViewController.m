#ifdef FEATURE_DBNUM
//
//  SearchResultsViewController.m
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/13/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "SearchResultsViewController.h"
#import "ActivityIndicator.h"
#import "ASEmail.h"
#import "BaseEmail.h"
#import "DateUtil.h"
#import "FMSplitViewDelegate.h"
#import "MailboxCell.h"
#import "StringUtil.h"
#import "SyncManager.h"

@implementation SearchResultsViewController
@synthesize emailData;
@synthesize query;
@synthesize senderSearchParams;
@synthesize isSenderSearch;
@synthesize nResults;

// the following two are expressed in terms of Emails, not Conversations!
int currentDBNum = 0; // current offset we're searching at

BOOL receivedAdditional = NO; // whether we received an additionalResults call
BOOL moreResults = NO; // are there more results after this?

static NSDateFormatter *dateFormatter = nil;

UIImage* imgAttachment = nil;

- (void)dealloc
{

	imgAttachment = nil;
	dateFormatter = nil;
}

////////////////////////////////////////////////////////////////////////////////////////////
// Error Handling
////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Error Handling

- (void)_logException:(NSString*)where exception:(NSException*)e
{
    [self handleError:[NSString stringWithFormat:@"Exception: %@ %@: %@", where, [e name], [e reason]]];
}

- (void)handleError:(NSString*)anErrorMessage
{
    UIAlertView* anAlertView = [[UIAlertView alloc] initWithTitle:@"Folder view error"
                                                          message:anErrorMessage
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:nil];
    [anAlertView show];
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.tableView.rowHeight = 96.0f;
}

- (void)viewDidUnload
{
	[super viewDidUnload];
	
	self.query = nil;
	self.emailData = nil;
}

- (void)updateTitle
{
	if(self.isSenderSearch) {
		// no quotes for sender Search;
		self.title = self.query;
	} else {
		self.title = [NSString stringWithFormat: @"\"%@\"", self.query];
	}
}


-(void)doLoad {
	self.nResults = 0;
	currentDBNum = 0;
	receivedAdditional = NO;
	moreResults = NO;
	
	dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	[dateFormatter setTimeStyle:NSDateFormatterNoStyle];
	
	self.emailData = [[NSMutableArray alloc] initWithCapacity:1];
	[self updateTitle];
	
	imgAttachment = [UIImage imageNamed:@"attachment.png"];
    //	[imgAttachment retain]; // released in "dealloc"
	
	[self runSearchCreateDataWithDBNum:currentDBNum];
}

-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.tableView.rowHeight = 96;
}

-(void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	SearchRunner *sem = [SearchRunner getSingleton];
	[sem cancel];
}

- (void)viewDidAppear:(BOOL)animated {
	// animating to or from - reload unread, server state
    [super viewDidAppear:animated];
	if(animated) {
		[self.tableView reloadData];
	}
    
	[self.navigationController setToolbarHidden:NO animated:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	FXDebugLog(kFXLogActiveSync, @"SearchResultsViewController reveived memory warning - dumping cache");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

////////////////////////////////////////////////////////////////////////////////
// Message Handling
////////////////////////////////////////////////////////////////////////////////
#pragma mark Message Handling

- (void)runSearchCreateDataWithDBNum:(int)dbNum
{
	// run a search with given offset
	SearchRunner* searchManager = [SearchRunner getSingleton];

	receivedAdditional = NO;
	moreResults = NO;
	int nextDBNum = dbNum+1;
	
	if(self.isSenderSearch) {
		NSString* senderAddresses = [self.senderSearchParams objectForKey:@"emailAddresses"];

		int dbMin = [[self.senderSearchParams objectForKey:@"dbMin"] intValue];
		int dbMax = [[self.senderSearchParams objectForKey:@"dbMax"] intValue];

		[searchManager senderSearch:senderAddresses withDelegate:self startWithDB:dbNum dbMin:dbMin dbMax:dbMax];
	} else {
		NSArray* snippetDelims = [NSArray arrayWithObjects:@"$$$$mark$$$$",@"$$$$endmark$$$$", nil];
		[searchManager ftSearch:self.query withDelegate:self withSnippetDelims:snippetDelims startWithDB:dbNum];
	}

	currentDBNum = nextDBNum;
	[self updateTitle];
}

- (void)insertRows:(NSArray*)anEmails
{
    @try {
        NSMutableArray* anIndexPaths = [NSMutableArray arrayWithCapacity:anEmails.count];
        
        NSUInteger aRow = self.emailData.count;
        
        for(BaseEmail* anEmail in anEmails) {
            NSIndexPath* anIndexPath = [NSIndexPath indexPathForRow:aRow inSection:0];
            [anIndexPaths addObject:anIndexPath];
            [self.emailData insertObject:anEmail atIndex:aRow];
            aRow++;
        }
        [self.tableView insertRowsAtIndexPaths:anIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
	} @catch (NSException* e) {
        [self _logException:@"insertRows" exception:e];
	}
}

- (void)loadResults:(NSArray*)anEmails
{
    [self performSelectorOnMainThread:@selector(insertRows:) withObject:anEmails waitUntilDone:NO];
}

- (void)emailDeleted:(NSNumber*)pk
{
#warning FIXME This delete needs to be merged with ActiveSync and ASEmailFolder
	for(int i = 0; i < [emailData count]; i++) {
		NSDictionary* email = [emailData objectAtIndex:i];
		
		NSNumber* emailPk = [email objectForKey:@"pk"];
		
		if([emailPk isEqualToNumber:pk]) {
			[emailData removeObjectAtIndex:i];
			NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
			[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// SearchManagerDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark SearchManagerDelegate

- (void)deliverSearchResults:(NSArray *)anEmails
{
	NSOperationQueue* q = ((SearchRunner*)[SearchRunner getSingleton]).operationQueue;
	NSInvocationOperation *op = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadResults:) object:anEmails];
	[q addOperation:op];
}

- (void)_reloadMoreItems
{
	if(self.emailData != nil) {
		NSIndexPath* indexPath = [NSIndexPath indexPathForRow:[self.emailData count] inSection:0];
		
		[self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
	}
}

- (void)deliverAdditionalResults:(NSNumber*)d
{
	receivedAdditional = YES;
	moreResults = [d boolValue];
		
	[self performSelectorOnMainThread:@selector(_reloadMoreItems) withObject:nil waitUntilDone:NO];
}

- (void) deliverProgressUpdate:(NSNumber *)progressNum
{
	currentDBNum = [progressNum intValue];
	
	[self performSelectorOnMainThread:@selector(_reloadMoreItems) withObject:nil waitUntilDone:NO];
}

- (BOOL)shouldDeliverObject:(NSString *)aUid
{
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
// UITableView
////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	int add = 1;
	return [self.emailData count] + add;
}

/*
- (LoadingCell*)_createConvoLoadingCellFromNib
{
	NSArray* nibContents = [[NSBundle mainBundle] loadNibNamed:@"LoadingCell" owner:self options:nil];
	NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
	LoadingCell* cell = nil;
	NSObject* nibItem = nil;
	while ((nibItem = [nibEnumerator nextObject]) != nil) {
		if([nibItem isKindOfClass: [LoadingCell class]]) {
			cell = (LoadingCell*)nibItem;
			break;
		}
	}
	return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	ASEmail* anEmail;
    
	if (indexPath.row < [self.emailData count]) {
		anEmail = [self.emailData objectAtIndex:indexPath.row];
	} else {
		anEmail = nil; // "More Results" link
	}
	
	if(anEmail == nil) { // "Loading" or "More Results"
		if(!receivedAdditional) {
			static NSString *loadingIdentifier = @"LoadingCell";
			LoadingCell* cell = (LoadingCell*)[tableView dequeueReusableCellWithIdentifier:loadingIdentifier];
			if (cell == nil) {
				cell = [self _createConvoLoadingCellFromNib];
			}
			
			if(![cell.activityIndicator isAnimating]) {
				[cell.activityIndicator startAnimating];
			}
			cell.label.text = [NSString stringWithFormat:NSLocalizedString(@"Loading Mail %i ...", nil), MAX(1,currentDBNum)];
			
			return cell;
		}
		
		UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"More"];
		
		if (cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"More"];
		}
		
		if(moreResults) {
			cell.textLabel.text         = @"More Mail";
			cell.textLabel.textColor    = [UIColor blackColor];
			cell.imageView.image        = [UIImage imageNamed:@"moreResults.png"];
		} else {
			if([self.emailData count] == 0) {
				cell.textLabel.text     = @"No mail";
			} else {
				cell.textLabel.text     = @"No more mail";
			}
			cell.textLabel.textColor    = [UIColor grayColor];
			cell.imageView.image        = nil;
		}
		return cell;
	}
    
    static NSString *cellIdentifier = @"MailCell";
    
    MailboxCell *theCell = (MailboxCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	NSAssert(theCell != nil, @"theCell != nil");
    
	theCell.email       = anEmail;
	//theCell.markingMode = _markingMode;
	
	return theCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	int addPrevious = 0;
	
	if(indexPath.row >= [self.emailData count] + addPrevious) {
		// Clicked "More Results"
        //
		if(moreResults) {
			[self runSearchCreateDataWithDBNum:currentDBNum+1];
			[self.tableView reloadData];
			return;
		} else {
			// clicked "no more results"
			return;
		}
	}
	
	// speed optimization (leads to incorrectness): cancel SearchRunner when user selects a result
    //
	SearchRunner *sem = [SearchRunner getSingleton];
	[sem cancel];
	
    // Create mail viewer and load email in to it
    //
	MailViewController* mailViewController = [[MailViewController alloc] init];
    ASEmail* anEmail = [self.emailData objectAtIndex:indexPath.row-addPrevious];
    mailViewController.email            = anEmail;
    
	int emailPk = anEmail.pk;
	int dbNum   = anEmail.dbNum;
    if(dbNum == 0 || emailPk == 0) {
        FXDebugLog(kFXLogActiveSync, @"Invalid email dbNum: %d emailPk:%d", dbNum, emailPk);
    }
#warning FIXME - switch to using ASEmail and get rid of all this
	mailViewController.email = anEmail;
//    mailViewController.folder           = anEmail.folder;
//	mailViewController.emailPk          = emailPk;
//	mailViewController.dbNum            = dbNum;
	mailViewController.isSenderSearch   = NO;
	mailViewController.query            = nil;
	mailViewController.deleteDelegate   = self;
    
    FMSplitViewDelegate *splitDelegate = (FMSplitViewDelegate*)self.splitViewController.delegate;
    if (splitDelegate)
    {
        splitDelegate.detailViewController = mailViewController;
    }
	else
    {
        [self.navigationController pushViewController:mailViewController animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	ASEmail* anEmail = [emailData objectAtIndex:indexPath.row];
	
	FXDebugLog(kFXLogActiveSync, @"Deleting email with pk: %@ row: %i", anEmail.pk, indexPath.row);
	
	SearchRunner* sm = [SearchRunner getSingleton];
	[sm deleteEmail:anEmail.pk dbNum:anEmail.dbNum];
	
	[emailData removeObjectAtIndex:indexPath.row];
	[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
}
*/
////////////////////////////////////////////////////////////////////////////////
// FolderDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark FolderDelegate

- (void)folderSyncing:(BaseFolder*)aFolder
{
}

- (void)folderSynced:(BaseFolder*)aFolder
{
}

- (void)newObjects:(NSArray*)anObjects
{
    @try {
        // Don't track new object in search results for now, or if you do you have to apply search terms
        //
        //[self performSelector:@selector(loadEmails:) withObject:anObjects];
    }@catch (NSException* e) {
        [self _logException:@"newObjects" exception:e];
    }
}

- (void)changeObject:(BaseObject*)aBaseObject
{
    @try {
        BOOL found = false;
        ASEmail* aChangedEmail = (ASEmail*)aBaseObject;
        NSString* aUid = aChangedEmail.uid;
        for(int i = 0; i < [emailData count]; i++) {
            ASEmail* anEmail = [emailData objectAtIndex:i];
            NSString* anEmailUid = anEmail.uid;
            if([aUid isEqualToString:anEmailUid]) {
#warning FIXME shouldn't this get the original object and not create a new one?
                [self.emailData replaceObjectAtIndex:i withObject:aChangedEmail];
                NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                found = TRUE;
                break;
            }
        }
        if(!found) {
            FXDebugLog(kFXLogActiveSync, @"changeObject failed: %@", aBaseObject);
        }
    }@catch (NSException* e) {
        [self _logException:@"changeObject" exception:e];
    }
}

- (void)deleteUid:(NSString*)aUid
{
    @try {
        BOOL found = false;
        for(int i = 0; i < [emailData count]; i++) {
            ASEmail* email = [emailData objectAtIndex:i];
            NSString* anEmailUid = email.uid;
            if([aUid isEqualToString:anEmailUid]) {
                [emailData removeObjectAtIndex:i];
                NSIndexPath* indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                found = TRUE;
                break;
            }
        }
        if(!found) {
            FXDebugLog(kFXLogActiveSync, @"deleteUid failed: %@", aUid);
        }
    }@catch (NSException* e) {
        [self _logException:@"deleteUid" exception:e];
    }
}

@end
#endif
