//
//  FolderSelectViewController.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/15/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

@class BaseAccount;

@interface FolderSelectViewController : UITableViewController 
{
    BaseAccount*        account;
    
	NSArray* folderPaths;
	NSDictionary* utf7Decoder;
	NSMutableSet* folderSelected;
	
	NSString* name;
	NSString* emailAddress;
	NSString* password;
	NSString* server;
	
	int encryption;
	int port;
	int authentication;
	
	int firstSetup;
	int accountNum;
	BOOL newAccount;
    email_account_type_enum accountType;
}

@property (strong) BaseAccount* account;
@property (nonatomic, strong) NSDictionary* utf7Decoder;
@property (nonatomic, strong) NSArray* folderPaths;
@property (nonatomic, strong) NSMutableSet* folderSelected;

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* emailAddress;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* server;

@property (assign) int encryption;
@property (assign) int port;
@property (assign) int authentication;

@property (assign) int firstSetup;
@property (assign) int accountNum;
@property (assign) BOOL newAccount;
@property (assign) email_account_type_enum accountType;
@end
