//
//  WebViewController.m
//  NextMailIPhone
//
//  Created by Gabor Cselle on 1/16/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#import "WebViewController.h"
#import "AppSettings.h"
#import "StringUtil.h"
#import "Reachability.h"

@implementation WebViewController
@synthesize webView;
@synthesize loadingLabel;
@synthesize serverUrl;
@synthesize loadingIndicator;

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)viewDidUnload {
	[super viewDidUnload];
	
	self.loadingLabel = nil;
	self.loadingIndicator = nil;
	self.webView = nil;
	self.serverUrl  = nil;
}

-(void)viewWillAppear:(BOOL)animated 
{
	[self doLoad];
}

-(void)viewDidAppear:(BOOL)animated 
{
	[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
	FXDebugLog(kFXLogActiveSync, @"WebViewController received memory warning");
}

////////////////////////////////////////////////////////////////////////////////
// View
////////////////////////////////////////////////////////////////////////////////
#pragma mark View

- (void)doLoad 
{
#if 0
	self.loadingLabel.text = NSLocalizedString(@"Loading ...", nil);
	
	
	[loadingLabel setHidden:NO];
	[loadingIndicator setHidden:NO];
	[loadingIndicator startAnimating];

	self.webView.delegate = self;
	
	FXDebugLog(kFXLogActiveSync, @"Start Loading WebView: %@", self.serverUrl);
	NSURL *url = [[NSURL alloc] initWithString:self.serverUrl];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	[self.webView loadRequest:request];
	[url release];
	[request release];
	FXDebugLog(kFXLogActiveSync, @"End loading WebView");
#endif
}

- (void)loadURL:(NSURL*)aURL
{
    self.webView.delegate = self;

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:aURL];
	[self.webView loadRequest:request];
}

- (void)loadHTMLString:(NSString*)aString
{
    [self.webView loadHTMLString:aString baseURL:[NSURL URLWithString:@""]];
}


////////////////////////////////////////////////////////////////////////////////
// UIWebViewDelegate
////////////////////////////////////////////////////////////////////////////////
#pragma mark UIWebViewDelegate

-(void)webViewDidStartLoad:(UIWebView *)aWebView 
{
    //NSURLRequest* aRequest = aWebView.request;
	//FXDebugLog(kFXLogActiveSync, @"webViewDidStartLoad %@ %@", [aRequest URL], [aRequest mainDocumentURL]);
}

-(void)webViewDidFinishLoad:(UIWebView *)aWebView 
{
	[loadingLabel setHidden:YES];
	[loadingIndicator setHidden:YES];
	[loadingIndicator stopAnimating];
    
    //NSString *anHtmlString = [webView stringByEvaluatingJavaScriptFromString: @"document.all[0].innerHTML"];
    //FXDebugLog(kFXLogActiveSync, @"webViewDidFinishLoad: %@", anHtmlString);
    
    //NSURLRequest* aRequest = aWebView.request;
	//FXDebugLog(kFXLogActiveSync, @"WebViewDidFinishLoad %@ %@", [aRequest URL], [aRequest mainDocumentURL]);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error 
{
    NSDictionary* userInfo = [error userInfo];
    //NSString* failingURL = [userInfo objectForKey: NSURLErrorFailingURLStringErrorKey];
    FXDebugLog(kFXLogActiveSync, @"webView didFailLoadWithError: %@\n%@", [error localizedDescription], userInfo);
    
#warning FIXME webView didFailLoadWithError    
    // FIXME this is tripping on multipart related mime load though they render correctly
#if 0
	//if(![StringUtil stringContains:[NSString stringWithFormat:@"%@", error] subString:@"999"]) {
		self.loadingLabel.text = [error localizedDescription];
	//}
	
	[self.loadingIndicator stopAnimating];
	[self.loadingIndicator setHidden:YES];
#endif
}

- (BOOL)webView:(UIWebView *)aWebView shouldStartLoadWithRequest:(NSURLRequest *)aRequest navigationType:(UIWebViewNavigationType)aNavigationType
{
    //FXDebugLog(kFXLogActiveSync, @"webView shouldStartLoadWithRequest: %@", [aRequest URL]);
    return TRUE;
}

@end
