/* 
 *  FIXMO CONFIDENTIAL
 * 
 *  Copyright © 2012 Fixmo Inc. All rights reserved.
 * 
 *  NOTICE:  All information contained herein is, and remains the property of Fixmo Inc.  
 *  The intellectual and technical concepts contained herein are proprietary to Fixmo Inc.
 *  and may be covered by U.S. and Foreign Patents, patents in process, and are 
 *  protected by trade secret or copyright law.
 *  Dissemination of this information or reproduction of this material is strictly
 *  forbidden unless prior written permission is obtained from Fixmo Inc. 
 */
/* This code originates from a StackOverflow question on how to get the TTMessageController to work
 * http://stackoverflow.com/questions/5374684/how-to-use-three20-ttmessagecontroller
 */
#import <Three20/Three20.h>

@interface AddressBookModel : NSObject <TTModel> {
    NSMutableArray* _delegates;
    NSArray*        _searchResults;
}

@property(nonatomic,strong) NSArray* searchResults;

- (void)search:(NSString*)text;

@end
