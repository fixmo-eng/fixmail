//
//  GmailAttachmentDownloader.h
//  ReMailIPhone
//
//  Created by Gabor Cselle on 7/7/09.
//  Copyright 2010 Google Inc.
//  
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//  
//   http://www.apache.org/licenses/LICENSE-2.0
//  
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
#import "ASItemOperationsDelegate.h"

@interface AttachmentDownloader : NSObject <ASItemOperationsDelegate> {
    id              delegate;
    
	int             attachmentNum;
    NSDictionary*   attachment;
    
	NSString*       uid;
	int             folderNum;
	int             accountNum;
    
    NSUInteger      expectedSize;
    NSString*       filePath;
    NSString*       extension;
}


@property (nonatomic, strong) id                delegate;
@property (assign) int                          attachmentNum;
@property (nonatomic, strong) NSDictionary*     attachment;
@property (nonatomic, strong) NSString*         uid;
@property (assign) int                          folderNum;
@property (assign) int                          accountNum;
@property (assign) NSUInteger                   expectedSize;
@property (nonatomic, strong) NSString*         filePath;
@property (nonatomic, strong) NSString*         extension;


// Static getters
+(void)ensureAttachmentDirExists;
+(NSString*)fileNameForAccountNum:(int)accountNum 
                        folderNum:(int)folderNum 
                              uid:(NSString*)uid 
                    attachmentNum:(int)attachmentNum
                        extension:(NSString*)extension;
+(NSString*)attachmentDirPath;

// Run loop
-(void)run;

@end
